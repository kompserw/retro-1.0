object frGadzetyDokLista: TfrGadzetyDokLista
  Left = 640
  Top = 373
  Width = 448
  Height = 269
  Caption = 'Lista dokument'#243'w - przygotowanie'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 4
    Height = 16
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object GroupBox2: TGroupBox
    Left = 16
    Top = 40
    Width = 401
    Height = 105
    Caption = 'Wybierz miesi'#261'ce do wydruku'
    TabOrder = 0
    OnClick = GroupBox2Click
    object CheckBox13: TCheckBox
      Left = 16
      Top = 24
      Width = 97
      Height = 17
      Caption = 'stycze'#324
      TabOrder = 0
      OnClick = GroupBox2Click
    end
    object CheckBox14: TCheckBox
      Left = 16
      Top = 48
      Width = 97
      Height = 17
      Caption = 'luty'
      TabOrder = 1
      OnClick = GroupBox2Click
    end
    object CheckBox15: TCheckBox
      Left = 16
      Top = 72
      Width = 97
      Height = 17
      Caption = 'marzec'
      TabOrder = 2
      OnClick = GroupBox2Click
    end
    object CheckBox16: TCheckBox
      Left = 96
      Top = 24
      Width = 97
      Height = 17
      Caption = 'kwiecie'#324
      TabOrder = 3
      OnClick = GroupBox2Click
    end
    object CheckBox17: TCheckBox
      Left = 96
      Top = 48
      Width = 97
      Height = 17
      Caption = 'maj'
      TabOrder = 4
      OnClick = GroupBox2Click
    end
    object CheckBox18: TCheckBox
      Left = 96
      Top = 72
      Width = 97
      Height = 17
      Caption = 'czerwiec'
      TabOrder = 5
      OnClick = GroupBox2Click
    end
    object CheckBox19: TCheckBox
      Left = 176
      Top = 24
      Width = 97
      Height = 17
      Caption = 'lipiec'
      TabOrder = 6
      OnClick = GroupBox2Click
    end
    object CheckBox20: TCheckBox
      Left = 176
      Top = 48
      Width = 97
      Height = 17
      Caption = 'sierpie'#324
      TabOrder = 7
      OnClick = GroupBox2Click
    end
    object CheckBox21: TCheckBox
      Left = 176
      Top = 72
      Width = 97
      Height = 17
      Caption = 'wrzesie'#324
      TabOrder = 8
      OnClick = GroupBox2Click
    end
    object CheckBox22: TCheckBox
      Left = 264
      Top = 24
      Width = 97
      Height = 17
      Caption = 'pa'#378'dziernik'
      TabOrder = 9
      OnClick = GroupBox2Click
    end
    object CheckBox23: TCheckBox
      Left = 264
      Top = 48
      Width = 97
      Height = 17
      Caption = 'listopad'
      TabOrder = 10
      OnClick = GroupBox2Click
    end
    object CheckBox24: TCheckBox
      Left = 264
      Top = 72
      Width = 97
      Height = 17
      Caption = 'grudzie'#324
      TabOrder = 11
      OnClick = GroupBox2Click
    end
  end
  object JvImgBtn7: TJvImgBtn
    Left = 269
    Top = 166
    Width = 145
    Height = 49
    Caption = 'Drukuj list'#281
    Enabled = False
    TabOrder = 1
    OnClick = JvImgBtn7Click
    Flat = True
    Images = frTowary.ImageList1
    ImageIndex = 6
  end
  object RadioGroup1: TRadioGroup
    Left = 16
    Top = 152
    Width = 185
    Height = 57
    Caption = 'Wydruk z pozycjami czy bez'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'bez pozycji'
      'z pozycjami')
    TabOrder = 2
    Visible = False
  end
end
