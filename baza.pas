unit baza;

{$IFDEF FPC}
  {$MODE Delphi}
{$ENDIF}

interface

uses
{$IFnDEF FPC}
  IBTable, IBCustomDataSet, IBDatabase, ADODB, JvDataSource, JvADOQuery, JvCsvData, IBQuery, Dialogs,
{$ELSE}
  sqldb,
{$ENDIF}
  SysUtils, Classes, DB, Forms;

type
  Tdm = class(TDataModule)
    JvDataSource1: TJvDataSource;
    JvADODataSet1: TJvADODataSet;
    serwer: TIBDatabase;
    TabelaKontrah: TIBTable;
    ibTransakcje: TIBTransaction;
    dsKontrah: TDataSource;
    ibqSQL: TIBQuery;
    PlikCSV: TJvCsvDataSet;
    dsPlikCSV: TJvDataSource;
    dsDostawca: TDataSource;
    dsSzukajObroty: TJvDataSource;
    ibqProwizje: TIBQuery;
    dsProwizje: TDataSource;
    ibqZapisz: TIBQuery;
    dsPokaszKontrah: TJvDataSource;
    ibqZapisImportExcel: TIBQuery;
    ibqKontrahenci: TIBQuery;
    ibqProwizjeZ: TIBQuery;
    ibqDostawca: TIBQuery;
    ibtProwizjeZ: TIBTransaction;
    ibtZapisz: TIBTransaction;
    ibqKartaK: TIBQuery;
    dsKartaK: TDataSource;
    ibtDostawca: TIBTransaction;
    ibtKartaK: TIBTransaction;
    ibqCSV2Firebird: TIBQuery;
    trCSV2Firebird: TIBTransaction;
    dsCSV2Firebird: TDataSource;
    ibqPoliczImport: TIBQuery;
    ibqKasujTabele: TIBQuery;
    ibqFormKontrah: TIBQuery;
    Szukaj: TIBQuery;
    dsFormKontrah: TDataSource;
    ibqSQL1: TIBQuery;
    dsSQL1: TDataSource;
    ibqPracownik: TIBQuery;
    ibqTrasy: TIBQuery;
    ibqTowary: TIBQuery;
    dsTowary: TDataSource;
    ibqGadzetyN: TIBQuery;
    ibqGadzetyP: TIBQuery;
    dsGadzetyN: TDataSource;
    dsGadzetyP: TDataSource;
    ibqSQL3: TIBQuery;
    dsSQL3: TDataSource;
    ibqAktywniD: TIBQuery;
    dsAktywniD: TDataSource;
    ibqAktywniD_P: TIBQuery;
    dsAktywniD_P: TDataSource;
    procedure SzukajOdbiorcy(nazwa,nazwisko,nip: String);
    procedure SzukajDostawcy(nazwa,nazwisko,nip: String);
    procedure PokazObrotySklep(nr_kontrah: Integer);
    procedure PokazObrotyDostawca(nr_kontrah: Integer);
    procedure ZapiszCRM(nr_kontrahenta: Integer;wlasciciel,kanal,tresc: String);
    procedure PokazKontrah(nr_kontrah: Integer;rodzaj,nazwa: String);
    procedure PokazCRM(nr: Integer);
    procedure Trasy(nr_trasy,czynnosc,nr_kto_jezdzi: Integer;nazwa,dlugosc,opis: String);
    procedure Towar(nr_towar,nr_dostawca,czynnosc,vat : Integer; nazwa,data_w : String; cena_n,ilosc : Real; data_zakupu: TDateTime);
    procedure TowaryStany(nr_towar: Integer;ilosc,cena_s: Real);overload;
    procedure Grupy(czynnosc,nr_grupy,r1,r2,r3,r4,r5: Integer;nazwa_grupy: String);
    procedure PokazTrase(nr_trasy,nr_kto_jezdzi: Integer;nazwa: String);overload;
    procedure Pracownicy(nr_pracownik, czynnosc, nr_grupa: Integer;imie,nazwisko,login,haslo: String);
    procedure PokazPracownikow;
    procedure PokazGrupy;
    procedure PokazSklepy(aktywny,nr_trasy: Integer);
    procedure PokazGadzetyP(nr_naglowek: Integer);
    procedure PokazPracownikaTrasy(nr_trasy: Integer);
    procedure SzukajTowar(nr_towar : Integer; nazwa : String);
    procedure PokazTowary;
    procedure NaglowekG(czynnosc,nr_kontrah,nr_naglowek,typ_dok: Integer;netto,brutto: Real);overload;
    procedure PozycjeG(czynnosc,nr_naglowek,nr_pozycja,nr_kontrah: Integer;pozycja: String;ilosc,cena,wartosc: Real);
    procedure OdswiezBazy;
    procedure DataModuleCreate(Sender: TObject);
    procedure KasujPozycje(nr: Integer);
    procedure UsunPozG(nr_naglowek,nr_pozycji: Integer);
    function NrKontrahent() : Integer; overload;
    function Nr_Grupy(nazwa: String) : Integer;
    function NrPrzedstawiciela(imie_nazwisko: String) : Integer;
    function NazwiskoImieKtoJezdzi(nr: Integer) : String;
    function PokazTraseF(nazwa: String) : Integer;
    function PokazPracownikF(nazwa: String) : Integer;
    function PracownikF(nr_trasy: Integer) : String;
    function NrNaglowka(): Integer;
    function SumaPozycji(nr_naglowka: Integer): Real;
    function IleJestTowaru(nr_towar: Integer;nazwa: String): Real;
    function NrTowar(nazwa_towaru: String): Integer;
    function TowarUnikalny(nazwa: String): Boolean;
    function IlePozycji(nr_naglowek: Integer): Integer;
    function NrDokumentu(const typ: Integer): Integer;
  private
    { Private declarations }
  public
    { Public declarations }
    function NrKontrahent(kontrah: String): Integer; overload;
    procedure TowaryStany(nr_towar: Integer;ilosc: Real);overload;
    procedure TowaryStany(nr_towar,nr_dostawca: Integer;ilosc,cena_s: Real);overload;
    procedure Trasowki(nr_pracownik: Integer);
    procedure NaglowekG(nr_kontrah,nr_naglowek,typDokumentow,nr_dokumentu: Integer;wartosc: Real;miesiac_dok,rok_dok,dzien: Word);overload;
    procedure PokazTrase(nr_kto_jezdzi: Integer);overload;
    procedure DrukujListeDokumentowG(rodzaj_dok,zawartosc,mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer);
    procedure AktywniD_Pokaz;
    procedure AktywniDD_Pokaz;
    procedure WstawDoDostawcow(nr_dostawcy: Integer);
    procedure UsunZDD(nr_dostawcy: Integer);
  end;

var
  dm: Tdm;

implementation
uses glowny, obrotyD, obrotyK, towary;
{$IFnDEF FPC}
  {$R *.dfm}
{$ELSE}
  {$R *.lfm}
{$ENDIF}

{ Tdm }

procedure Tdm.OdswiezBazy;
begin
serwer.Close;
serwer.DatabaseName := ExtractFilePath(Application.ExeName) + 'euro.fdb';
serwer.Connected := True;
serwer.Open;
end;

function Tdm.NrKontrahent: Integer;
begin
with Szukaj do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_KONTRAH FROM KONTRAH');
          Open;
          Last;
          Result := FieldValues['NR_KONTRAH'];
     end;

end;

procedure Tdm.PokazKontrah(nr_kontrah: Integer; rodzaj,nazwa: String);
begin
with dm.ibqKontrahenci do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');
          if (nr_kontrah > 0) and (Length(rodzaj) > 0) and (Length(nazwa) = 0) then
             begin
                  SQL.Add('WHERE NR_KONTRAH = :A AND RODZAJ = :B AND AKTYWNY = 1');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kontrah;
                     ParamByName('B').AsString := rodzaj;
                  Prepare;
             end;
          if (nr_kontrah > 0) and (Length(rodzaj) = 0) and (Length(nazwa) = 0) then
             begin
                  SQL.Add('WHERE NR_KONTRAH = :A  AND AKTYWNY = 1');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kontrah;
                  Prepare;
             end;
          if (nr_kontrah = 0) and (Length(rodzaj) > 0) and (Length(nazwa) = 0) then
             begin
                  SQL.Add('WHERE RODZAJ = :A  AND AKTYWNY = 1');
                  Unprepare;
                     ParamByName('A').AsString := rodzaj;
                  Prepare;
             end;
          if (nr_kontrah = 0) and (Length(rodzaj) = 0) and (Length(nazwa) = 0) then
             begin
                  SQL.Add('WHERE AKTYWNY = 1');
             end;
          if (nr_kontrah = 0) and (Length(rodzaj) = 0) and (Length(nazwa) > 0) then
             begin
                  SQL.Add('WHERE NAZWA LIKE :A  AND AKTYWNY = 1');
                  Unprepare;
                     ParamByName('A').AsString := '%' + nazwa + '%';
                  Prepare;
             end;
          if (nr_kontrah = 0) and (Length(rodzaj) > 0) and (Length(nazwa) > 0) then
             begin
                  SQL.Add('WHERE RODZAJ = :B AND NAZWA LIKE :A  AND AKTYWNY = 1');
                  Unprepare;
                     ParamByName('A').AsString := '%' + nazwa + '%';
                     ParamByName('B').AsString := rodzaj;
                  Prepare;
             end;
          Open;
     end;

end;

procedure Tdm.SzukajDostawcy(nazwa, nazwisko, nip: String);
var
   t: String;
begin
t := 'SELECT DISTINCT NR_KONTRAH,RODZAJ,IMIE_NAZWISKO,NAZWA,NIP,ULICA,MIASTO,KOD_P,DATA_PRZYSTAPIENIA,TELEFON,KONCESJA,METRAZ,KATEGORIA,AKTYWNY,UWAGI FROM KONTRAH';
with ibqKontrahenci do
     begin
          Close;
          SQL.Clear;
          SQL.Add(t);
          if Length(nazwa) > 0 then SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = ''D''');
          if Length(nazwisko) > 0 then SQL.Add('WHERE IMIE_NAZWISKO LIKE :A AND RODZAJ = ''D''');
          if Length(nip) > 0 then SQL.Add('WHERE NIP LIKE :A AND RODZAJ = ''D''');
          if (Length(nazwa) = 0) and (Length(nazwisko) = 0) and (Length(nip) = 0) then SQL.Add('WHERE RODZAJ = ''D''');
          Unprepare;
             if Length(nazwa) > 0 then ParamByName('A').AsString := '%' + nazwa + '%';
             if Length(nazwisko) > 0 then ParamByName('A').AsString := '%' + nazwisko + '%';
             if Length(nip) > 0 then ParamByName('A').AsString := '%' + nip + '%';
          Prepare;
          Open;
     end;

end;

procedure Tdm.SzukajOdbiorcy(nazwa, nazwisko, nip: String);
var
   t: String;
begin
t := 'SELECT DISTINCT NR_KONTRAH,RODZAJ,IMIE_NAZWISKO,NAZWA,NIP,ULICA,MIASTO,KOD_P,DATA_PRZYSTAPIENIA,TELEFON,KONCESJA,METRAZ,KATEGORIA,AKTYWNY,UWAGI FROM KONTRAH';
with ibqKontrahenci do
     begin
          Close;
          SQL.Clear;
          SQL.Add(t);
          if Length(nazwa) > 0 then SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = ''O''');
          if Length(nazwisko) > 0 then SQL.Add('WHERE IMIE_NAZWISKO LIKE :A AND RODZAJ = ''O''');
          if Length(nip) > 0 then SQL.Add('WHERE NIP LIKE :A AND RODZAJ = ''O''');
          if (Length(nazwa) = 0) and (Length(nazwisko) = 0) and (Length(nip) = 0) then SQL.Add('WHERE RODZAJ = ''O''');
          Unprepare;
             if Length(nazwa) > 0 then ParamByName('A').AsString := '%' + nazwa + '%';
             if Length(nazwisko) > 0 then ParamByName('A').AsString := '%' + nazwisko + '%';
             if Length(nip) > 0 then ParamByName('A').AsString := '%' + nip + '%';
          Prepare;
          Open;
     end;
end;

procedure Tdm.ZapiszCRM(nr_kontrahenta: Integer; wlasciciel,kanal,tresc: String);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with ibqFormKontrah do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO CRM(NR_KONTRAH,KTO_UTWORZYL,KANAL,NOTATKA)');
          SQL.Add('VALUES(:NR_KONTRAH,:KTO_UTWORZYL,:KANAL,:NOTATKA)');
          Unprepare;
             ParamByName('NR_KONTRAH').AsInteger := nr_kontrahenta;
             ParamByName('KTO_UTWORZYL').AsString := wlasciciel;
             ParamByName('KANAL').AsString := kanal;
             ParamByName('NOTATKA').AsString := tresc;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.PokazCRM(nr: Integer);
begin
with ibqFormKontrah do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM CRM');
          SQL.Add('WHERE NR_KONTRAH = :A');
          //SQL.Add('ORDER BY DATA_UTWORZENIA');
          Unprepare;
             ParamByName('A').AsInteger := nr;
          Prepare;
          Open;
     end;
end;

procedure Tdm.PokazObrotyDostawca(nr_kontrah: Integer);
begin
with frObrotyD.ibqObrotyD do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.IMIE_NAZWISKO AS SKLEP,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.PROW_E_KWOTA,A.PROW_E_PROC,A.OKRES_M,A.OKRES_Q FROM OBROTY A,KONTRAH B');
          SQL.Add('WHERE B.NR_KONTRAH = A.NR_KONTRAH_SKLEP AND A.NR_KONTRACH_DOST = :A');
          SQL.Add('ORDER BY A.OKRES_Q');
          UnPrepare;
                    ParamByName('A').AsInteger := nr_kontrah;
          Prepare;
          Open;
     end;
with frObrotyD.ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(OBROTY) AS OBROTY,SUM(PROWIZJA_KWOTA) AS PROWIZJA,SUM(PROW_E_KWOTA) AS EURO FROM OBROTY A');
          SQL.Add('WHERE A.NR_KONTRACH_DOST = :A');
          UnPrepare;
                    ParamByName('A').AsInteger := nr_kontrah;
          Prepare;
          Open;
          if FieldByName('OBROTY').IsNull then
             begin
                  frObrotyD.lObroty.Caption := 'brak obrot�w';
                  frObrotyD.lProwizja.Caption := 'brak obrot�w';
                  frObrotyD.lProw_E.Caption := 'brak obrot�w';
             end
          else
              begin
                   frObrotyD.lObroty.Caption := FloatToStrF(FieldValues['OBROTY'], ffCurrency, 7, 2);
                   frObrotyD.lProwizja.Caption := FloatToStrF(FieldValues['PROWIZJA'], ffCurrency, 7, 2);
                   frObrotyD.lProw_E.Caption := FloatToStrF(FieldValues['EURO'], ffCurrency, 7, 2);
              end;
     end;
with frObrotyD.grObroty do
     begin
          Columns.Items[0].FieldName := 'SKLEP';
          Columns.Items[0].Title.Caption := 'Sklep';
          Columns.Items[0].Width := 200;
          Columns.Items[1].FieldName := 'OBROTY';
          Columns.Items[1].Title.Caption := 'Obr�t sklepu';
          Columns.Items[1].Width := 95;
          Columns.Items[2].FieldName := 'PROWIZJA_PROC';
          Columns.Items[2].Title.Caption := 'Procent prowizji';
          Columns.Items[2].Width := 95;
          Columns.Items[3].FieldName := 'PROWIZJA_KWOTA';
          Columns.Items[3].Title.Caption := 'Kwota prowizji';
          Columns.Items[3].Width := 95;
          Columns.Items[4].FieldName := 'PROW_E_KWOTA';
          Columns.Items[4].Title.Caption := 'Prowizja Euro';
          Columns.Items[4].Width := 95;
          Columns.Items[5].FieldName := 'PROW_E_PROC';
          Columns.Items[5].Title.Caption := 'Procent Euro';
          Columns.Items[5].Width := 95;
          Columns.Items[6].FieldName := 'OKRES_M';
          Columns.Items[6].Title.Caption := 'Okres miesi�ca';
          Columns.Items[6].Width := 95;
          Columns.Items[7].FieldName := 'OKRES_Q';
          Columns.Items[7].Title.Caption := 'Okres kwarta�';
          Columns.Items[7].Width := 95;
     end;     
end;

procedure Tdm.PokazObrotySklep(nr_kontrah: Integer);
begin
with frObrotyK.ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA_D AS DOSTAWCA,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.OKRES_M,A.OKRES_Q FROM OBROTY A');
          SQL.Add('WHERE A.NR_KONTRAH_SKLEP = :A');
          SQL.Add('ORDER BY A.OKRES_Q');
          UnPrepare;
                    ParamByName('A').AsInteger := nr_kontrah;
          Prepare;
          Open;
     end;
with frObrotyK.ibqSuma do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(OBROTY) AS OBROTY,SUM(PROWIZJA_KWOTA) AS PROWIZJA FROM OBROTY A');
          SQL.Add('WHERE A.NR_KONTRAH_SKLEP = :A');
          UnPrepare;
                    ParamByName('A').AsInteger := nr_kontrah;
          Prepare;
          Open;
          if FieldByName('OBROTY').IsNull then
             begin
                  frObrotyK.lObroty.Caption := 'brak obrot�w';
                  frObrotyK.lProwizja.Caption := 'brak obrot�w';
             end
          else
              begin
                   frObrotyK.lObroty.Caption := FloatToStrF(FieldValues['OBROTY'], ffCurrency, 7, 2);
                   frObrotyK.lProwizja.Caption := FloatToStrF(FieldValues['PROWIZJA'], ffCurrency, 7, 2);
              end;
     end;
with frObrotyK.grObroty do
     begin
          Columns.Items[0].FieldName := 'DOSTAWCA';
          Columns.Items[0].Title.Caption := 'Dostawca';
          Columns.Items[0].Width := 200;
          Columns.Items[1].FieldName := 'OBROTY';
          Columns.Items[1].Title.Caption := 'Obr�t sklepu';
          Columns.Items[1].Width := 80;
          Columns.Items[2].FieldName := 'PROWIZJA_PROC';
          Columns.Items[2].Title.Caption := 'Procent prowizji';
          Columns.Items[2].Width := 80;
          Columns.Items[3].FieldName := 'PROWIZJA_KWOTA';
          Columns.Items[3].Title.Caption := 'Kwota prowizji';
          Columns.Items[3].Width := 80;
          Columns.Items[4].FieldName := 'OKRES_M';
          Columns.Items[4].Title.Caption := 'Okres miesi�ca';
          Columns.Items[4].Width := 80;
          Columns.Items[5].FieldName := 'OKRES_Q';
          Columns.Items[5].Title.Caption := 'Okres kwarta�';
          Columns.Items[5].Width := 80;
     end;
end;

procedure Tdm.DataModuleCreate(Sender: TObject);
var
   baza: String;
begin
baza  := ExtractFilePath(Application.ExeName) + 'euro.fdb';
if FileExists(baza) then
   begin
        serwer.DatabaseName := baza;
        //serwer.Params.Add('user_name=sysdba,password=masterkey');
        serwer.Connected := True;
   end
else
    begin
         ShowMessage('Brak pliku bazy danych');
         Application.Terminate;
    end;
PokazKontrah(0,'','');
end;

procedure Tdm.PokazTrase(nr_trasy,nr_kto_jezdzi: Integer; nazwa: String);
begin
//
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM TRASY');
          if (nr_trasy = 0) and (nr_kto_jezdzi = 0) and (Length(nazwa) > 0) then
             begin
                  SQL.Add('WHERE NAZWA_TRASY LIKE :A');
                  Unprepare;
                     ParamByName('A').AsString := '%' + nazwa + '%';
                  Prepare;
             end;
          if (nr_trasy = 0) and (nr_kto_jezdzi > 0) and (Length(nazwa) = 0) then
             begin
                  SQL.Add('WHERE NR_KTO_JEZDZI = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kto_jezdzi;
                  Prepare;
             end;
          if (nr_trasy > 0) and (nr_kto_jezdzi = 0) and (Length(nazwa) = 0) then
             begin
                  SQL.Add('WHERE NR_TRASY = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_trasy;
                  Prepare;
             end;
          Open;
     end;
end;

procedure Tdm.Trasy(nr_trasy, czynnosc, nr_kto_jezdzi: Integer; nazwa, dlugosc,
  opis: String);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          case czynnosc of
          1: begin
                  SQL.Add('INSERT INTO TRASY(NR_KTO_JEZDZI,NAZWA_TRASY,DLUGOSC_TRASY,OPIS)');
                  SQL.Add('VALUES(:A,:B,:C,:D)');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kto_jezdzi;
                     ParamByName('B').AsString := nazwa;
                     ParamByName('C').AsString := dlugosc;
                     ParamByName('D').AsString := opis;
                  Prepare;
             end;
          2: begin
                  SQL.Add('UPDATE TRASY');
                  SQL.Add('SET NR_KTO_JEZDZI = :A,NAZWA_TRASY = :B,DLUGOSC_TRASY = :C,OPIS = :D');
                  SQL.Add('WHERE NR_TRASY = :E');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kto_jezdzi;
                     ParamByName('B').AsString := nazwa;
                     ParamByName('C').AsString := dlugosc;
                     ParamByName('D').AsString := opis;
                     ParamByName('E').AsInteger := nr_trasy;
                  Prepare;
             end;
          3: begin
                  SQL.Add('DELETE FROM TRASY');
                  SQL.Add('WHERE NR_TRASY = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_trasy;
                  Prepare;
             end;
          end;
     Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.PokazGrupy;
begin
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT DISTINCT NAZWA_GRUPY FROM GRUPY');
          SQL.Add('ORDER BY NAZWA_GRUPY');
          Open;
     end;
end;

procedure Tdm.Pracownicy(nr_pracownik, czynnosc, nr_grupa: Integer;imie,nazwisko,login,haslo: String);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          case czynnosc of
          1: begin
                  SQL.Add('INSERT INTO PRACOWNIK(NR_GRUPA,IMIE,NAZWISKO,LOGIN,HASLO)');
                  SQL.Add('VALUES(:A,:B,:C,:D,:E)');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_grupa;
                     ParamByName('B').AsString := imie;
                     ParamByName('C').AsString := nazwisko;
                     ParamByName('D').AsString := login;
                     ParamByName('E').AsString := haslo;
                  Prepare;
             end;
          2: begin
                  SQL.Add('UPDATE PRACOWNIK');
                  SQL.Add('SET NR_GRUPA = :A,IMIE = :B,NAZWISKO = :C,LOGIN = :D,HASLO = :E)');
                  SQL.Add('WHERE NR_PRACOWNIK = :F');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_grupa;
                     ParamByName('B').AsString := imie;
                     ParamByName('C').AsString := nazwisko;
                     ParamByName('D').AsString := login;
                     ParamByName('E').AsString := haslo;
                     ParamByName('F').AsInteger := nr_pracownik;
                  Prepare;
             end;
          3: begin
                  SQL.Add('DELETE FROM PRACOWNIK');
                  SQL.Add('WHERE NR_PRACOWNIK = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_pracownik;
                  Prepare;
             end;
          end;
     Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

function Tdm.Nr_Grupy(nazwa: String): Integer;
begin
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM GRUPY');
          SQL.Add('WHERE NAZWA_GRUPY = :A');
          Unprepare;
             ParamByName('A').AsString := nazwa;
          Prepare;
          Open;
          Result := FieldValues['NR_GRUPY'];
     end;
end;

procedure Tdm.Grupy(czynnosc, nr_grupy, r1, r2, r3, r4, r5: Integer;
  nazwa_grupy: String);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          case czynnosc of
          1: begin
                  SQL.Add('INSERT INTO GRUPY(NAZWA_GRUPY,ROLA1,ROLA2,ROLA3,ROLA4,ROLA5)');
                  SQL.Add('VALUES(:A,:B,:C,:D,:E,:F)');
                  Unprepare;
                     ParamByName('A').AsString := nazwa_grupy;
                     ParamByName('B').AsInteger := r1;
                     ParamByName('C').AsInteger := r2;
                     ParamByName('D').AsInteger := r3;
                     ParamByName('E').AsInteger := r4;
                     ParamByName('F').AsInteger := r5;
                  Prepare;
             end;
          2: begin
                  SQL.Add('UPDATE GRUPY');
                  SQL.Add('SET NAZWA_GRUPY = :A,ROLA1 = :B,ROLA2 = :C,ROLA3 = :D,ROLA4 = :E,ROLA5 = :F)');
                  SQL.Add('WHERE NR_GRUPY = :G');
                  Unprepare;
                     ParamByName('A').AsString := nazwa_grupy;
                     ParamByName('B').AsInteger := r1;
                     ParamByName('C').AsInteger := r2;
                     ParamByName('D').AsInteger := r3;
                     ParamByName('E').AsInteger := r4;
                     ParamByName('F').AsInteger := r5;
                     ParamByName('G').AsInteger := nr_grupy;
                  Prepare;
             end;
          3: begin
                  SQL.Add('DELETE FROM GRUPY');
                  SQL.Add('WHERE NR_GRUPY = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_grupy;
                  Prepare;
             end;
          end;
     Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.PokazPracownikow;
begin
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT DISTINCT * FROM PRACOWNIK');
          SQL.Add('ORDER BY NAZWISKO');
          Open;
     end;
end;

procedure Tdm.PokazSklepy(aktywny,nr_trasy: Integer);
begin
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');
          if nr_trasy > 0 then
             if aktywny = 0 then SQL.Add('WHERE AKTYWNY = 0')
                else SQL.Add('WHERE NR_TRASY = :A AND AKTYWNY = 1')
          else
             if aktywny = 0 then SQL.Add('WHERE AKTYWNY = 0')
                else SQL.Add('WHERE AKTYWNY = 1');
          SQL.Add('ORDER BY IMIE_NAZWISKO');
          if nr_trasy > 0 then
             begin
                  Unprepare;
                     ParamByName('A').AsInteger := nr_trasy;
                  Prepare;
             end;
          Open;
     end;
end;

function Tdm.NrPrzedstawiciela(imie_nazwisko: String): Integer;
var
   nr: Integer;
begin
Delete(imie_nazwisko,Pos(' ',imie_nazwisko),20);
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_PRACOWNIK FROM PRACOWNIK');
          SQL.Add('WHERE NAZWISKO LIKE :A');
          Unprepare;
            ParamByName('A').AsString := '%' + imie_nazwisko + '%';
          Prepare;
          Open;
          nr := FieldValues['NR_PRACOWNIK'];
          Result := nr;
     end;
end;

function Tdm.NazwiskoImieKtoJezdzi(nr: Integer): String;
begin
with dm.ibqPracownik do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM PRACOWNIK');
          SQL.Add('WHERE NR_PRACOWNIK = :A');
          Unprepare;
             ParamByName('A').AsInteger := nr;
          Prepare;
          Open;
          Result := FieldValues['NAZWISKO'] + ' ' + FieldValues['IMIE'];
     end;
end;

function Tdm.PokazTraseF(nazwa: String): Integer;
var
   nr_trasy: Integer;
begin
with dm.ibqTrasy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT DISTINCT * FROM TRASY');
          SQL.Add('WHERE NAZWA_TRASY = :A');
          Unprepare;
             ParamByName('A').AsString := nazwa;
          Prepare;
          Open;
          nr_trasy := FieldValues['NR_TRASY'];
          Result := nr_trasy;
     end;
end;

function Tdm.PokazPracownikF(nazwa: String): Integer;
begin
Delete(nazwa,Pos(' ',nazwa),20);
with dm.ibqPracownik do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT DISTINCT * FROM PRACOWNIK');
          SQL.Add('WHERE NAZWISKO = :A');
          Unprepare;
             ParamByName('A').AsString := nazwa;
          Prepare;
          Open;
          Result := FieldValues['NR_PRACOWNIK'];
     end;
end;

function Tdm.PracownikF(nr_trasy: Integer): String;
begin
with dm.ibqTrasy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT DISTINCT * FROM TRASY');
          SQL.Add('WHERE NR_TRASY = :A');
          Unprepare;
             ParamByName('A').AsInteger := nr_trasy;
          Prepare;
          Open;
          Result := dm.NazwiskoImieKtoJezdzi(FieldValues['NR_KTO_JEZDZI']);
     end;
end;

procedure Tdm.PokazPracownikaTrasy(nr_trasy: Integer);
begin
with dm.ibqTrasy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM TRASY');
          SQL.Add('WHERE NR_TRASY = :A');
          Unprepare;
             ParamByName('A').AsInteger := nr_trasy;
          Prepare;
          Open;
     end;
end;

procedure Tdm.Towar(nr_towar, nr_dostawca, czynnosc, vat: Integer; nazwa,
  data_w: String; cena_n, ilosc: Real; data_zakupu: TDateTime);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          case czynnosc of
          1: begin
                  SQL.Add('INSERT INTO TOWARY(NR_DOSTAWCA,NAZWA,CENA_N_ZAKUP,VAT,ILOSC,DATA_ZAKUPU,DATA_WAZNOSCI)');
                  SQL.Add('VALUES(:A,:B,:C,:D,:E,:F,:G)');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_dostawca;
                     ParamByName('B').AsString := nazwa;
                     ParamByName('C').AsFloat := cena_n;
                     ParamByName('D').AsInteger := vat;
                     ParamByName('E').AsFloat := 0;
                     ParamByName('F').AsDate := now();
                     ParamByName('G').AsString := data_w;
                  Prepare;
             end;
          2: begin
                  SQL.Add('UPDATE TOWARY');
                  SQL.Add('SET NR_DOSTAWCA = :A,NAZWA = :B,CENA_N_ZAKUP = :C,VAT = :D,DATA_ZAKUPU = :F,DATA_WAZNOSCI = :G');
                  SQL.Add('WHERE NR_TOWARY = :H');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_dostawca;
                     ParamByName('B').AsString := nazwa;
                     ParamByName('C').AsFloat := cena_n;
                     ParamByName('D').AsInteger := vat;
                     //ParamByName('E').AsFloat := ilosc;
                     ParamByName('F').AsDate := now();
                     ParamByName('G').AsString := data_w;
                     ParamByName('H').AsInteger := nr_towar;
                  Prepare;
             end;
          3: begin
                  SQL.Add('DELETE FROM TOWARY');
                  SQL.Add('WHERE NR_TOWARY = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_towar;
                  Prepare;
             end;
          end;
     Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.SzukajTowar(nr_towar: Integer; nazwa: String);
var
   tekst: String;
begin
tekst := 'B.NAZWA AS NAZWA_K,A.NAZWA,A.CENA_N_ZAKUP,A.CENA_B_ZAKUP,A.VAT,A.ILOSC,A.WARTOSC_N,A.WARTOSC_B,A.DATA_ZAKUPU,A.DATA_WAZNOSCI,A.NR_TOWARY';
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT ' + tekst + ' FROM TOWARY A, KONTRAH B');
          if nr_towar = 0 then SQL.Add('WHERE A.NR_DOSTAWCA = B.NR_KONTRAH AND A.NAZWA LIKE :B')
          else SQL.Add('WHERE A.NR_DOSTAWCA = B.NR_KONTRAH AND A.NR_TOWARY = :A');
          Unprepare;
             if nr_towar > 0 then ParamByName('A').AsInteger := nr_towar
             else ParamByName('B').AsString := '%' + nazwa + '%';
          Prepare;
          Open;
     end;
end;

procedure Tdm.NaglowekG(czynnosc, nr_kontrah,nr_naglowek,typ_dok: Integer; netto,
  brutto: Real);
begin
case czynnosc of
     3,4,5: if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
     end;
with dm.ibqGadzetyN do
     begin
          Close;
          SQL.Clear;
          case czynnosc of
          1: begin
                  SQL.Add('SELECT A.DATA_WYSTAWIENIA,A.NUMER,B.IMIE_NAZWISKO,B.NAZWA,B.NIP,A.WARTOSC_NETTO,A.NUMER_DOK,A.MIESIAC_DOK,A.ROK_DOK,A.NR_GADZETY_NAGLOWEK,B.MIASTO,B.ULICA,B.KOD_P,A.TYP_DOK FROM GADZETY_NAGLOWEK A, KONTRAH B');
                  case typ_dok of
                       1: SQL.Add('WHERE A.NR_KONTRAH = B.NR_KONTRAH');
                       2: SQL.Add('WHERE A.NR_KONTRAH = B.NR_KONTRAH AND A.TYP_DOK = ''P''');
                       3: SQL.Add('WHERE A.NR_KONTRAH = B.NR_KONTRAH AND A.TYP_DOK = ''R''');
                  end;
                  SQL.Add('ORDER BY A.DATA_WYSTAWIENIA');
             end;
          2: begin
                  SQL.Add('SELECT * FROM GADZETY_NAGLOWEK');
                  SQL.Add('WHERE NR_KONTRAH = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kontrah;
                  Prepare;
             end;
          3: begin
                  SQL.Add('INSERT INTO GADZETY_NAGLOWEK(NR_KONTRAH,NUMER,TYP_DOK,WARTOSC_NETTO,WARTOSC_BRUTTO)');
                  SQL.Add('VALUES(:A,:D,:E,:B,:C)');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kontrah;
                     ParamByName('B').AsFloat := netto;
                     ParamByName('C').AsFloat := brutto;
                     ParamByName('D').AsInteger := nr_naglowek;
                     case typ_dok of
                          2: ParamByName('E').AsString := 'P';
                          3: ParamByName('E').AsString := 'R';
                     end;
                  Prepare;
             end;
          4: begin
                  SQL.Add('UPDATE GADZETY_NAGLOWEK');
                  SQL.Add('SET NR_KONTRAH = :A,NUMER = :E,TYP_DOK = :F,WARTOSC_NETTO = :B,WARTOSC_BRUTTO = :C');
                  SQL.Add('WHERE NUMER = :D');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kontrah;
                     ParamByName('B').AsFloat := netto;
                     ParamByName('C').AsFloat := brutto;
                     ParamByName('D').AsInteger := nr_naglowek;
                     ParamByName('E').AsInteger := nr_naglowek;
                     case typ_dok of
                          2: ParamByName('F').AsString := 'P';
                          3: ParamByName('F').AsString := 'R';
                     end;
                  Prepare;
             end;
          5: begin
                  SQL.Add('DELETE FROM GADZETY_NAGLOWEK');
                  SQL.Add('WHERE NUMER = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_naglowek;
                  Prepare;
             end;
          end;
          Open;
     end;
case czynnosc of
     3,4,5: if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
     end;
end;

procedure Tdm.PozycjeG(czynnosc, nr_naglowek, nr_pozycja,nr_kontrah: Integer; pozycja: String;
  ilosc, cena, wartosc: Real);
begin
case czynnosc of
     4,5,6: if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
     end;
with dm.ibqGadzetyP do
     begin
          Close;
          SQL.Clear;
          case czynnosc of
          1: begin
                  SQL.Add('SELECT * FROM GADZETY_POZYCJE');
             end;
          2: begin
                  SQL.Add('SELECT * FROM GADZETY_POZYCJE');
                  SQL.Add('WHERE NR_GADZETY_NAGLOWEK = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_naglowek;
                  Prepare;
             end;
          3: begin
                  SQL.Add('SELECT * FROM GADZETY_POZYCJE');
                  SQL.Add('WHERE NR_GADZETY_POZYCJE = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_kontrah;
                  Prepare;
             end;
          4: begin
                  SQL.Add('INSERT INTO GADZETY_POZYCJE(NR_GADZETY_NAGLOWEK,POZYCJA,ILOSC,CENA_NETTO)');
                  SQL.Add('VALUES(:A,:B,:C,:D)');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_naglowek;
                     ParamByName('B').AsString := pozycja;
                     ParamByName('C').AsFloat := ilosc;
                     ParamByName('D').AsFloat := cena;
                  Prepare;
             end;
          5: begin
                  SQL.Add('UPDATE GADZETY_POZYCJE');
                  SQL.Add('SET NR_GADZETY_NAGLOWEK = :A,POZYCJA = :B,ILOSC = :C,CENA_NETTO = :D');
                  SQL.Add('WHERE NR_GADZETY_POZYCJE = :F');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_naglowek;
                     ParamByName('B').AsString := pozycja;
                     ParamByName('C').AsFloat := ilosc;
                     ParamByName('D').AsFloat := cena;
                     ParamByName('F').AsInteger := nr_pozycja;
                  Prepare;
             end;
          6: begin
                  SQL.Add('DELETE FROM GADZETY_POZYCJE');
                  SQL.Add('WHERE NR_GADZETY_POZYCJE = :A');
                  Unprepare;
                     ParamByName('A').AsInteger := nr_pozycja;
                  Prepare;
             end;
          end;
          Open;
     end;
case czynnosc of
     4,5,6: if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
     end;
end;

function Tdm.NrNaglowka(): Integer;
var
   ile_rec,ostatni: Integer;
begin
ile_rec := 0;
ostatni := 0;
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT MAX(NUMER) AS NUMER FROM GADZETY_NAGLOWEK');
          Open;
          ile_rec := RecordCount;

     end;
     if ibqSQL1.FieldByName('NUMER').isNull then ostatni := 0
        else ostatni := ibqSQL1.FieldValues['NUMER'];
     ostatni := ostatni + 1;
if (ibqSQL1.FieldByName('NUMER').IsNull) or (ibqSQL1.FieldValues['NUMER'] = 0) then Result := 1
else Result := ostatni;
end;

procedure Tdm.PokazTowary;
var
   tekst: String;
begin
tekst := 'B.NAZWA AS NAZWA_K,A.NAZWA,A.CENA_N_ZAKUP,A.CENA_B_ZAKUP,A.VAT,A.ILOSC,A.WARTOSC_N,A.WARTOSC_B,A.DATA_ZAKUPU,A.DATA_WAZNOSCI,A.NR_TOWARY';
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT ' + tekst + ' FROM TOWARY A, KONTRAH B');
          SQL.Add('WHERE A.NR_DOSTAWCA = B.NR_KONTRAH');
          Open;
     end;
end;

procedure Tdm.PokazGadzetyP(nr_naglowek: Integer);
begin
with dm.ibqGadzetyP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM GADZETY_POZYCJE');
          SQL.Add('WHERE NR_GADZETY_NAGLOWEK = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_naglowek;
          Prepare;
          Open;
     end;
end;

procedure Tdm.TowaryStany(nr_towar: Integer; ilosc,cena_s: Real);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('UPDATE TOWARY');
          SQL.Add('SET ILOSC = :B,CENA_N_ZAKUP = :C');
          SQL.Add('WHERE NR_TOWARY = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_towar;
              ParamByName('B').AsFloat := ilosc;
              ParamByName('C').AsFloat := cena_s;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

function Tdm.SumaPozycji(nr_naglowka: Integer): Real;
begin
with dm.ibqGadzetyP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(WARTOSC_NETTO) AS SUMA FROM GADZETY_POZYCJE');
          SQL.Add('WHERE NR_GADZETY_NAGLOWEK = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_naglowka;
          Prepare;
          Open;
          if FieldByName('SUMA').IsNull then ShowMessage('Brak pozycji dla nag��wka ' + IntToStr(nr_naglowka))
          else
              Result := FieldValues['SUMA'];
     end;
end;

procedure Tdm.KasujPozycje(nr: Integer);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqGadzetyP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM GADZETY_POZYCJE');
          SQL.Add('WHERE NR_GADZETY_NAGLOWEK = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

function Tdm.NrKontrahent(kontrah: String): Integer;
begin
with Szukaj do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_KONTRAH FROM KONTRAH');
          SQL.Add('WHERE NAZWA LIKE :A');
          Unprepare;
             ParamByName('A').AsString := '%' + kontrah + '%';
          Prepare;
          Open;
          Result := FieldValues['NR_KONTRAH'];
     end;
end;

function Tdm.IleJestTowaru(nr_towar: Integer; nazwa: String): Real;
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT ILOSC FROM TOWARY');
          if nr_towar > 0 then
             SQL.Add('WHERE NR_TOWARY = :A')
          else
              SQL.Add('WHERE NAZWA LIKE :A');
          Unprepare;
          if nr_towar > 0 then
              ParamByName('A').AsInteger := nr_towar
          else
              ParamByName('A').AsString := '%' + nazwa + '%';
          Prepare;
          Open;
          Result := FieldValues['ILOSC'];
     end;
end;

procedure Tdm.TowaryStany(nr_towar, nr_dostawca: Integer; ilosc,cena_s: Real);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('UPDATE TOWARY');
          SQL.Add('SET ILOSC = :B,NR_DOSTAWCA = :C,CENA_N_ZAKUP = :D');
          SQL.Add('WHERE NR_TOWARY = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_towar;
              ParamByName('B').AsFloat := ilosc;
              ParamByName('C').AsInteger := nr_dostawca;
              ParamByName('D').AsFloat := cena_s;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.UsunPozG(nr_naglowek,nr_pozycji: Integer);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqGadzetyP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM GADZETY_POZYCJE');
          SQL.Add('WHERE NR_GADZETY_NAGLOWEK = :A AND NR_GADZETY_POZYCJE = :B');
          Unprepare;
              ParamByName('A').AsInteger := nr_naglowek;
              ParamByName('B').AsInteger := nr_pozycji;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.TowaryStany(nr_towar: Integer; ilosc: Real);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('UPDATE TOWARY');
          SQL.Add('SET ILOSC = :B');
          SQL.Add('WHERE NR_TOWARY = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_towar;
              ParamByName('B').AsFloat := ilosc;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

function Tdm.NrTowar(nazwa_towaru: String): Integer;
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_TOWARY FROM TOWARY');
          SQL.Add('WHERE NAZWA = :A');
          Unprepare;
              ParamByName('A').AsString := nazwa_towaru;
          Prepare;
          Open;
          Result := FieldValues['NR_TOWARY'];
     end;
end;

function Tdm.TowarUnikalny(nazwa: String): Boolean;
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT COUNT(NAZWA) AS ILE FROM TOWARY');
          SQL.Add('WHERE NAZWA = :A');
          Unprepare;
              ParamByName('A').AsString := nazwa;
          Prepare;
          Open;
          if FieldValues['ILE'] > 0 then
             Result := False
          else
              Result := True;
     end;
end;

function Tdm.IlePozycji(nr_naglowek: Integer): Integer;
begin
with dm.ibqGadzetyP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT COUNT(NR_GADZETY_POZYCJE) AS ILE FROM GADZETY_POZYCJE');
          SQL.Add('WHERE NR_GADZETY_NAGLOWEK = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_naglowek;
          Prepare;
          Open;
          Result := FieldValues['ILE'];
     end;
end;

function Tdm.NrDokumentu(const typ: Integer): Integer;
begin
with ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT MAX(NUMER_DOK) AS NUMER FROM GADZETY_NAGLOWEK');
          SQL.Add('WHERE TYP_DOK = :A');
          Unprepare;
              if typ = 2 then ParamByName('A').AsString := 'P'
                 else ParamByName('A').AsString := 'R';
          Prepare;
          Open;
          if FieldByName('NUMER').IsNull then
             Result := 1
          else
              Result := FieldValues['NUMER'] + 1;
     end;
end;

//NaglowekG(nr_kontrah,nr_naglowek,typDokumentow,nr_dokumentu: Integer;wartosc: Real;miesiac_dok,rok_dok,dzien: Word);overload;

procedure Tdm.NaglowekG(nr_kontrah, nr_naglowek,typDokumentow,
  nr_dokumentu: Integer; wartosc: Real; miesiac_dok, rok_dok, dzien: Word);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqGadzetyN do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO GADZETY_NAGLOWEK(NR_KONTRAH,NUMER,TYP_DOK,WARTOSC_NETTO,WARTOSC_BRUTTO,NUMER_DOK,MIESIAC_DOK,ROK_DOK)');
          SQL.Add('VALUES(:A,:D,:E,:B,:C,:F,:G,:H)');
          Unprepare;
              ParamByName('A').AsInteger := nr_kontrah;
              ParamByName('B').AsFloat := wartosc;
              ParamByName('C').AsFloat := wartosc;
              ParamByName('D').AsInteger := nr_naglowek;
              case typDokumentow of
                   2: ParamByName('E').AsString := 'P';
                   3: ParamByName('E').AsString := 'R';
              end;
              ParamByName('F').AsInteger := nr_dokumentu;
              ParamByName('G').AsWord := miesiac_dok;
              ParamByName('H').AsWord := rok_dok;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.Trasowki(nr_pracownik: Integer);
var
   t1,t2,t3: String;
begin
t1 := 'select a.IMIE_NAZWISKO,a.TYDZIEN_1,a.TYDZIEN_2,a.TYDZIEN_3,a.TYDZIEN_4,a.TYDZIEN_5,b.IMIE,b.NAZWISKO,c.NAZWA_TRASY from KONTRAH a,PRACOWNIK b,TRASY c';
t2 := 'where a.NR_TRASY = c.NR_TRASY and b.NR_PRACOWNIK = c.NR_KTO_JEZDZI and b.NR_PRACOWNIK = :A';
t3 := 'order by c.NAZWA_TRASY';
with dm.ibqTrasy do
     begin
          Close;
          SQL.Clear;
          SQL.Add(t1);
          SQL.Add(t2);
          SQL.Add(t3);
          Unprepare;
              ParamByName('A').AsInteger := nr_pracownik;
          Prepare;
          Open;
     end;
end;

procedure Tdm.PokazTrase(nr_kto_jezdzi: Integer);
begin
with dm.ibqSQL3 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM TRASY');
          SQL.Add('WHERE NR_KTO_JEZDZI = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_kto_jezdzi;
          Prepare;
          Open;
     end;
end;

procedure Tdm.DrukujListeDokumentowG(rodzaj_dok, zawartosc, mi1, mi2, mi3,
  mi4, mi5, mi6, mi7, mi8, mi9, mi10, mi11, mi12: Integer);
var
   t1: String;
begin
t1 := 'SELECT B.IMIE_NAZWISKO,B.NAZWA,NR_GADZETY_NAGLOWEK,NUMER,DATA_WYSTAWIENIA,WARTOSC_NETTO,WARTOSC_BRUTTO,TYP_DOK,NUMER_DOK,MIESIAC_DOK,ROK_DOK';
with dm.ibqSQL3 do
     begin
          Close;
          SQL.Clear;
          SQL.Add(t1 + ' FROM GADZETY_NAGLOWEK A, KONTRAH B');
          if rodzaj_dok = 0 then SQL.Add('WHERE B.NR_KONTRAH=A.NR_KONTRAH AND MIESIAC_DOK IN (:M1,:M2,:M3,:M4,:M5,:M6,:M7,:M8,:M9,:M10,:M11,:M12)');
          if rodzaj_dok = 1 then SQL.Add('WHERE B.NR_KONTRAH=A.NR_KONTRAH AND TYP_DOK = :A AND MIESIAC_DOK IN (:M1,:M2,:M3,:M4,:M5,:M6,:M7,:M8,:M9,:M10,:M11,:M12)');
          if rodzaj_dok = 2 then SQL.Add('WHERE B.NR_KONTRAH=A.NR_KONTRAH AND TYP_DOK = :A AND MIESIAC_DOK IN (:M1,:M2,:M3,:M4,:M5,:M6,:M7,:M8,:M9,:M10,:M11,:M12)');
          SQL.Add('ORDER BY NR_GADZETY_NAGLOWEK');
          Unprepare;
              case rodzaj_dok of
                   1: ParamByName('A').AsString := 'P';
                   2: ParamByName('A').AsString := 'R';
              end;
              ParamByName('M1').AsInteger := mi1;
              ParamByName('M2').AsInteger := mi2;
              ParamByName('M3').AsInteger := mi3;
              ParamByName('M4').AsInteger := mi4;
              ParamByName('M5').AsInteger := mi5;
              ParamByName('M6').AsInteger := mi6;
              ParamByName('M7').AsInteger := mi7;
              ParamByName('M8').AsInteger := mi8;
              ParamByName('M9').AsInteger := mi9;
              ParamByName('M10').AsInteger := mi10;
              ParamByName('M11').AsInteger := mi11;
              ParamByName('M12').AsInteger := mi12;
          Prepare;
          Open;
     end;
end;

procedure Tdm.AktywniD_Pokaz;
begin
with dm.ibqAktywniD do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA,A.NR_KONTRAH FROM KONTRAH A');
          SQL.Add('WHERE A.RODZAJ = :A AND AKTYWNY = 1 AND A.NR_KONTRAH NOT IN (SELECT NR_ORG FROM DOSTAWCY)');
          SQL.Add('ORDER BY NAZWA');
          Unprepare;
              ParamByName('A').AsString := 'D';
          Prepare;
          Open;
     end;
end;

procedure Tdm.WstawDoDostawcow(nr_dostawcy: Integer);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqAktywniD_P do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO DOSTAWCY(NR_ORG,AKTYWNY)');
          SQL.Add('VALUES(:A,:B)');
          Unprepare;
              ParamByName('A').AsInteger := nr_dostawcy;
              ParamByName('B').AsInteger := 1;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

procedure Tdm.AktywniDD_Pokaz;
begin
with dm.ibqAktywniD_P do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA,B.NR_ORG FROM KONTRAH A,DOSTAWCY B');
          SQL.Add('WHERE A.NR_KONTRAH = B.NR_ORG');
          SQL.Add('ORDER BY NAZWA');
          Open;
     end;
end;

procedure Tdm.UsunZDD(nr_dostawcy: Integer);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqAktywniD_P do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM DOSTAWCY');
          SQL.Add('WHERE NR_ORG = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_dostawcy;
          Prepare;
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
end;

end.
