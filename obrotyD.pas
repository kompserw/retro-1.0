unit obrotyD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, JvExStdCtrls, JvCombobox, DBCtrls,
  JvGroupBox, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid,
  JvExExtCtrls, JvExtComponent, JvPanel, DB, IBCustomDataSet, IBQuery,
  JvButton, JvCtrls, frxClass, frxDBSet;

type
  TfrObrotyD = class(TForm)
    JvPanel2: TJvPanel;
    grObroty: TJvDBUltimGrid;
    JvPanel1: TJvPanel;
    JvGroupBox1: TJvGroupBox;
    dbeNazwa: TDBText;
    dbeUlica: TDBText;
    dbeMiasto: TDBText;
    dbeKodP: TDBText;
    dbeNIP: TDBText;
    dbeTelefon: TDBText;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    cbKwartal: TJvComboBox;
    leNazwa: TLabeledEdit;
    GroupBox1: TGroupBox;
    lObroty: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    lProwizja: TLabel;
    ibqObrotyD: TIBQuery;
    dsDaneD: TDataSource;
    lProw_E: TLabel;
    Label7: TLabel;
    ibqObroty: TIBQuery;
    JvImgBtn1: TJvImgBtn;
    frxReport1: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    frxDBDataset2: TfrxDBDataset;
    cbMiesiacD: TComboBox;
    procedure zaswewChange(Sender: TObject);
    procedure cbKwartalChange(Sender: TObject);
    procedure leNazwaChange(Sender: TObject);
    procedure UstawSiatke(Sender: TObject);
    procedure WyswietlSumy(Sender: TObject);
    procedure grObrotyDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure zaswewClick(Sender: TObject);
    procedure zaswewSelect(Sender: TObject);
    procedure cbMiesiacDChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frObrotyD: TfrObrotyD;

implementation
uses glowny,baza;
{$R *.dfm}

procedure TfrObrotyD.zaswewChange(Sender: TObject);                           //filtr miesi�ce
var
   nazwa: String;
begin
nazwa := frGlowny.ibqDostawcy.FieldValues['NAZWA'];
with ibqObrotyD do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,B.MIASTO,A.NAZWA_D,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.PROW_E_PROC,A.PROW_E_KWOTA,A.ZYSK,A.OKRES_M,A.OKRES_Q FROM OBROTY A,KONTRAH B');
          if cbMiesiacD.Text <> 'wszystkie' then
             SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_M = :B')
          else
              SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A');
          SQL.Add('ORDER BY A.NAZWA_D,B.NAZWA,A.OKRES_M');
          UnPrepare;
              ParamByName('A').AsString := nazwa;
              if cbMiesiacD.Text <> 'wszystkie' then ParamByName('B').AsString := cbMiesiacD.Text;
          Prepare;
          Open;
     end;

with ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(A.OBROTY) AS OBROT,SUM(A.PROWIZJA_KWOTA) AS PROWIZJA_S,SUM(A.PROW_E_KWOTA) AS PROWIZJA_E,SUM(A.ZYSK) AS ZYSK FROM OBROTY A,KONTRAH B');
          if cbMiesiacD.Text <> 'wszystkie' then
             SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_M = :B')
          else
              SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A');
          UnPrepare;
              ParamByName('A').AsString := frGlowny.ibqDostawcy.FieldValues['NAZWA'];
              if cbMiesiacD.Text <> 'wszystkie' then ParamByName('B').AsString := cbMiesiacD.Text;
          Prepare;
          Open;
     end;
UstawSiatke(Sender);
WyswietlSumy(Sender);
end;

procedure TfrObrotyD.cbKwartalChange(Sender: TObject);                           //filtr kwarta�
begin
with ibqObrotyD do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,B.MIASTO,A.NAZWA_D,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.PROW_E_PROC,A.PROW_E_KWOTA,A.ZYSK,A.OKRES_M,A.OKRES_Q FROM OBROTY A,KONTRAH B');
          SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_Q = :B');
          SQL.Add('ORDER BY A.NAZWA_D,B.NAZWA,A.OKRES_M');
          UnPrepare;
              ParamByName('A').AsString := frGlowny.ibqDostawcy.FieldValues['NAZWA'];
              ParamByName('B').AsString := cbKwartal.Text;
          Prepare;
          Open;
     end;

with ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(A.OBROTY) AS OBROT,SUM(A.PROWIZJA_KWOTA) AS PROWIZJA_S,SUM(A.PROW_E_KWOTA) AS PROWIZJA_E,SUM(A.ZYSK) AS ZYSK FROM OBROTY A,KONTRAH B');
          SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_Q = :B');
          UnPrepare;
              ParamByName('A').AsString := frGlowny.ibqDostawcy.FieldValues['NAZWA'];
              ParamByName('B').AsString := cbKwartal.Text;
          Prepare;
          Open;
     end;
UstawSiatke(Sender);
WyswietlSumy(Sender);
end;

procedure TfrObrotyD.leNazwaChange(Sender: TObject);                             //filtr nazwa sklepu
begin
with ibqObrotyD do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,B.MIASTO,A.NAZWA_D,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.PROW_E_PROC,A.PROW_E_KWOTA,A.ZYSK,A.OKRES_M,A.OKRES_Q FROM OBROTY A,KONTRAH B');
          if cbMiesiacD.Text <> 'wszystkie' then
             SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_M = :B AND B.NAZWA LIKE :C')
          else
              SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND B.NAZWA LIKE :C');
          UnPrepare;
              ParamByName('A').AsString := frGlowny.ibqDostawcy.FieldValues['NAZWA'];
              ParamByName('B').AsString := cbMiesiacD.Text;
              ParamByName('C').AsString := '%' + UpperCase(leNazwa.Text) + '%';
          Prepare;
          Open;
     end;

with ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(A.OBROTY) AS OBROT,SUM(A.PROWIZJA_KWOTA) AS PROWIZJA_S,SUM(A.PROW_E_KWOTA) AS PROWIZJA_E,SUM(A.ZYSK) AS ZYSK FROM OBROTY A,KONTRAH B');
          if cbMiesiacD.Text <> 'wszystkie' then
             SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_M = :B AND B.NAZWA LIKE :C')
          else
              SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND B.NAZWA LIKE :C');
          UnPrepare;
              ParamByName('A').AsString := frGlowny.ibqDostawcy.FieldValues['NAZWA'];
              ParamByName('B').AsString := cbMiesiacD.Text;
              ParamByName('C').AsString := '%' + UpperCase(leNazwa.Text) + '%';
          Prepare;
          Open;
     end;
UstawSiatke(Sender);
WyswietlSumy(Sender);
end;

procedure TfrObrotyD.UstawSiatke(Sender: TObject);
begin
with grObroty.Columns do
     begin
          Items[2].Visible := False;
          Items[0].Title.Caption := 'Nazwa sklepu';
          Items[1].Title.Caption := 'Miasto sklepu';
          Items[3].Title.Caption := 'Obroty sklepu';
          Items[4].Title.Caption := 'Prowizja %';
          Items[5].Title.Caption := 'Prowizja sklepu z�';
          Items[6].Title.Caption := 'Prowizja % Euro';
          Items[7].Title.Caption := 'Prowizja Euro z�';
          Items[8].Title.Caption := 'Zysk Euro-sklep';
          Items[9].Title.Caption := 'Okres miesi�c';
          Items[10].Title.Caption := 'Okres kwarta�';
          Items[0].Width := 250;
          Items[1].Width := 150;
          Items[3].Width := 80;
          Items[4].Width := 80;
          Items[5].Width := 80;
          Items[6].Width := 80;
          Items[7].Width := 80;
          Items[8].Width := 80;
          Items[9].Width := 80;
          Items[10].Width := 80;
     end;
end;

procedure TfrObrotyD.WyswietlSumy(Sender: TObject);
begin
if ibqObroty.FieldByName('OBROT').IsNull = False then
   lObroty.Caption := FloatToStrF(ibqObroty.FieldValues['OBROT'], ffCurrency, 7, 2)
else
    lObroty.Caption := '';
if ibqObroty.FieldByName('PROWIZJA_S').IsNull = False then
   lProwizja.Caption := FloatToStrF(ibqObroty.FieldValues['PROWIZJA_S'], ffCurrency, 7, 2)
else
lProwizja.Caption := '';
if ibqObroty.FieldByName('PROWIZJA_E').IsNull = False then
   lProw_E.Caption := FloatToStrF(ibqObroty.FieldValues['PROWIZJA_E'], ffCurrency, 7, 2)
else
    lProw_E.Caption := '';
//if ibqObroty.FieldByName('ZYSK').IsNull = False then lZysk.Caption := FloatToStrF(ibqObroty.FieldValues['ZYSK'], ffCurrency, 7, 2);
end;

procedure TfrObrotyD.grObrotyDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  if Field.FieldName = 'OBROTY'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'PROWIZJA_KWOTA'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'PROWIZJA_PROC'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 %';
  if Field.FieldName = 'PROW_E_PROC'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 %';
  if Field.FieldName = 'PROW_E_KWOTA'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'ZYSK'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
end;

procedure TfrObrotyD.JvImgBtn1Click(Sender: TObject);
begin
frxReport1.PrepareReport();
frxReport1.ShowReport;
end;

procedure TfrObrotyD.zaswewClick(Sender: TObject);
begin
showmessage('to tu');
end;

procedure TfrObrotyD.zaswewSelect(Sender: TObject);
begin
showmessage('to tu');
end;

procedure TfrObrotyD.cbMiesiacDChange(Sender: TObject);
var
   nazwa: String;
begin
nazwa := dm.ibqKontrahenci.FieldValues['NAZWA'];
with ibqObrotyD do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,B.MIASTO,A.NAZWA_D,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.PROW_E_PROC,A.PROW_E_KWOTA,A.ZYSK,A.OKRES_M,A.OKRES_Q FROM OBROTY A,KONTRAH B');
          if cbMiesiacD.Text <> 'wszystkie' then
             SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_M = :B')
          else
              SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A');
          SQL.Add('ORDER BY A.NAZWA_D,B.NAZWA,A.OKRES_M');
          UnPrepare;
              ParamByName('A').AsString := nazwa;
              if cbMiesiacD.Text <> 'wszystkie' then ParamByName('B').AsString := cbMiesiacD.Text;
          Prepare;
          Open;
     end;

with ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(A.OBROTY) AS OBROT,SUM(A.PROWIZJA_KWOTA) AS PROWIZJA_S,SUM(A.PROW_E_KWOTA) AS PROWIZJA_E,SUM(A.ZYSK) AS ZYSK FROM OBROTY A,KONTRAH B');
          if cbMiesiacD.Text <> 'wszystkie' then
             SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A AND A.OKRES_M = :B')
          else
              SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A');
          UnPrepare;
              ParamByName('A').AsString := nazwa;
              if cbMiesiacD.Text <> 'wszystkie' then ParamByName('B').AsString := cbMiesiacD.Text;
          Prepare;
          Open;
     end;
UstawSiatke(Sender);
WyswietlSumy(Sender);

end;

end.

