object frWydajGadzety: TfrWydajGadzety
  Left = 351
  Top = 237
  Width = 484
  Height = 656
  Caption = 'Wydaj gad'#380'ety'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 16
    Top = 8
    Width = 441
    Height = 105
    Caption = 'Dane sklepu'
    TabOrder = 0
    object DBText1: TDBText
      Left = 16
      Top = 32
      Width = 41
      Height = 13
      AutoSize = True
      DataField = 'IMIE_NAZWISKO'
      DataSource = dm.dsKontrah
    end
    object DBText2: TDBText
      Left = 16
      Top = 72
      Width = 41
      Height = 13
      AutoSize = True
      DataField = 'NAZWA'
      DataSource = dm.dsKontrah
    end
    object DBText3: TDBText
      Left = 280
      Top = 32
      Width = 41
      Height = 13
      AutoSize = True
      DataField = 'NIP'
      DataSource = dm.dsKontrah
    end
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 152
    Width = 441
    Height = 120
    DataSource = dm.dsTowary
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 16
    Top = 352
    Width = 441
    Height = 201
    DataSource = dm.dsGadzetyP
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object JvImgBtn1: TJvImgBtn
    Left = 144
    Top = 288
    Width = 305
    Height = 49
    Caption = 'Dodaj do wydania wybrany gad'#380'et'
    TabOrder = 3
    OnClick = JvImgBtn1Click
    Flat = True
    Images = frGlowny.JvImageList1
    ImageIndex = 39
  end
  object LabeledEdit1: TLabeledEdit
    Left = 144
    Top = 120
    Width = 313
    Height = 21
    CharCase = ecUpperCase
    EditLabel.Width = 124
    EditLabel.Height = 13
    EditLabel.Caption = 'Szukaj po nazwie gad'#380'etu'
    LabelPosition = lpLeft
    TabOrder = 4
    OnChange = LabeledEdit1Change
  end
  object LabeledEdit2: TLabeledEdit
    Left = 16
    Top = 312
    Width = 121
    Height = 21
    EditLabel.Width = 69
    EditLabel.Height = 13
    EditLabel.Caption = 'Wpisz ile sztuk'
    TabOrder = 5
  end
  object JvImgBtn2: TJvImgBtn
    Left = 152
    Top = 552
    Width = 140
    Height = 57
    Caption = 'Zapisz dokument'
    TabOrder = 6
    OnClick = JvImgBtn2Click
    Flat = True
    Images = frGlowny.JvImageList1
    ImageIndex = 3
  end
  object frxReport2: TfrxReport
    Version = '5.5.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Domy'#347'lny'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42747.031989108800000000
    ReportOptions.LastChange = 42789.951427615740000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 272
    Top = 5
    Datasets = <
      item
        DataSet = frxDBDataset2
        DataSetName = 'frxDBDataset2'
      end
      item
        DataSet = frxDBDataset3
        DataSetName = 'frxDBDataset3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 132.283550000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 15.118120000000000000
          Width = 415.748300000000000000
          Height = 83.149660000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 45.354360000000000000
          Top = 7.559060000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8 = (
            'Nazwa firmy:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 434.645950000000000000
          Top = 37.795300000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Ulica:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 434.645950000000000000
          Top = 64.252010000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Miasto:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 434.645950000000000000
          Top = 90.708720000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'NIP')
          ParentFont = False
        end
        object frxDBDataset2NAZWA: TfrxMemoView
          Left = 7.559060000000000000
          Top = 34.015770000000000000
          Width = 389.291590000000000000
          Height = 18.897650000000000000
          DataField = 'NAZWA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."NAZWA"]')
        end
        object frxDBDataset2IMIE_NAZWISKO: TfrxMemoView
          Left = 7.559060000000000000
          Top = 64.252010000000000000
          Width = 389.291590000000000000
          Height = 18.897650000000000000
          DataField = 'IMIE_NAZWISKO'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."IMIE_NAZWISKO"]')
        end
        object Memo12: TfrxMemoView
          Left = 487.559370000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Data wystawienia:')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBDataset2NIP: TfrxMemoView
          Left = 487.559370000000000000
          Top = 90.708720000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'NIP'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."NIP"]')
        end
        object frxDBDataset2MIASTO: TfrxMemoView
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataField = 'MIASTO'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."MIASTO"]')
        end
        object frxDBDataset2KOD_P: TfrxMemoView
          Left = 487.559370000000000000
          Top = 64.252010000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'KOD_P'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."KOD_P"]')
        end
        object frxDBDataset2ULICA: TfrxMemoView
          Left = 487.559370000000000000
          Top = 37.795300000000000000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DataField = 'ULICA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."ULICA"]')
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 253.228510000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset3
        DataSetName = 'frxDBDataset3'
        RowCount = 0
        object Line: TfrxMemoView
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Line]')
          ParentFont = False
        end
        object frxDBDataset3POZYCJA: TfrxMemoView
          Left = 37.795300000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DataField = 'POZYCJA'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[frxDBDataset3."POZYCJA"]')
          ParentFont = False
        end
        object frxDBDataset3ILOSC: TfrxMemoView
          Left = 377.953000000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'ILOSC'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset3."ILOSC"]')
          ParentFont = False
        end
        object frxDBDataset3CENA: TfrxMemoView
          Left = 457.323130000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          DataField = 'CENA'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset3."CENA"]')
          ParentFont = False
        end
        object frxDBDataset3WARTOSC: TfrxMemoView
          Left = 540.472790000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'WARTOSC'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset3."WARTOSC"]')
          ParentFont = False
        end
      end
      object ColumnFooter1: TfrxColumnFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 332.598640000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 457.323130000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Razem:')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          Left = 540.472790000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDBDataset3."WARTOSC">,MasterData1,2)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 377.953000000000000000
        Width = 718.110700000000000000
        object Memo11: TfrxMemoView
          Left = 525.354670000000000000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          HAlign = haRight
          Memo.UTF8 = (
            'Wydruk z programu Retro')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Width = 721.890230000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 173.858380000000000000
        Width = 718.110700000000000000
        object Memo6: TfrxMemoView
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Lp.')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 37.795300000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Nazwa towaru')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 377.953000000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Ilo'#313#8250#196#8225)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 457.323130000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Cena')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 540.472790000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Warto'#313#8250#196#8225)
          ParentFont = False
        end
      end
    end
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IMIE_NAZWISKO=IMIE_NAZWISKO'
      'NAZWA=NAZWA'
      'NIP=NIP'
      'MIASTO=MIASTO'
      'ULICA=ULICA'
      'KOD_P=KOD_P')
    DataSource = dm.dsKontrah
    BCDToCurrency = False
    Left = 304
    Top = 5
  end
  object frxDBDataset3: TfrxDBDataset
    UserName = 'frxDBDataset3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'POZYCJA=POZYCJA'
      'ILOSC=ILOSC'
      'CENA_NETTO=CENA'
      'WARTOSC_NETTO=WARTOSC')
    DataSource = dm.dsGadzetyP
    BCDToCurrency = False
    Left = 344
    Top = 5
  end
end
