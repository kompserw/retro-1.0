unit pracownicy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ImgList, ToolWin, Grids, DBGrids, PageControlEx,
  StdCtrls, JvExStdCtrls, JvButton, JvCtrls, ExtCtrls, DB, DBCtrls;

type
  TfrPracownicy = class(TForm)
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    sb: TStatusBar;
    PageControlEx1: TPageControlEx;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    GroupBox1: TGroupBox;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    ComboBox1: TComboBox;
    Label1: TLabel;
    JvImgBtn1: TJvImgBtn;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    LabeledEdit5: TLabeledEdit;
    LabeledEdit6: TLabeledEdit;
    LabeledEdit7: TLabeledEdit;
    LabeledEdit8: TLabeledEdit;
    ComboBox2: TComboBox;
    JvImgBtn2: TJvImgBtn;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    LabeledEdit9: TLabeledEdit;
    LabeledEdit10: TLabeledEdit;
    LabeledEdit11: TLabeledEdit;
    LabeledEdit12: TLabeledEdit;
    ComboBox3: TComboBox;
    JvImgBtn3: TJvImgBtn;
    TabSheet5: TTabSheet;
    ListBox1: TListBox;
    ListBox2: TListBox;
    procedure JvImgBtn1Click(Sender: TObject);
    procedure PageControlEx1Change(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frPracownicy: TfrPracownicy;

implementation
uses
baza,glowny;
{$R *.dfm}

procedure TfrPracownicy.JvImgBtn1Click(Sender: TObject);
var
   nr_grupy: Integer;
begin
//ZAPIS DANYCH PRACOWNIKA
if Length(ComboBox1.Text) > 0 then nr_grupy := dm.Nr_Grupy(ComboBox1.Text);
dm.Pracownicy(0,1,nr_grupy,LabeledEdit1.Text,LabeledEdit2.Text,LabeledEdit3.Text,LabeledEdit4.Text);
dm.PokazPracownikow;
PageControlEx1.ActivePageIndex := 0;
end;

procedure TfrPracownicy.PageControlEx1Change(Sender: TObject);
var
   imie, nazwisko: String;
begin
if PageControlEx1.Pages[4].CanFocus then
   begin
        dm.PokazPracownikow;
        ListBox1.Clear;
        dm.ibqSQL1.First;
        while not dm.ibqSQL1.Eof do
              begin
                  imie := dm.ibqSQL1.FieldValues['IMIE'];
                  nazwisko := dm.ibqSQL1.FieldValues['NAZWISKO'];
                  dm.ibqSQL1.Next;
                  ListBox1.Items.Add(imie + ' ' + nazwisko);
              end;
   end;
end;

procedure TfrPracownicy.ListBox1Click(Sender: TObject);
var
   nr_pracownika: Integer;
   opis: String;
begin
nr_pracownika := dm.ibqSQL1.FieldValues['NR_PRACOWNIK'];
dm.PokazTrase(nr_pracownika);
ListBox2.Clear;
dm.ibqSQL3.First;
while not dm.ibqSQL3.Eof do
      begin
           opis := dm.ibqSQL3.FieldValues['NAZWA_TRASY'];
           ListBox2.Items.Add(opis);
           dm.ibqSQL3.Next;
      end;
end;

end.
