unit towary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ImgList, PageControlEx, Grids, DBGrids,
  StdCtrls, ExtCtrls, JvExExtCtrls, JvControlBar, DB, ADODB, JvExStdCtrls,
  JvButton, JvCtrls, frxClass, frxDBSet, frxExportPDF, IBCustomDataSet,
  IBQuery, Menus;

type
  TfrTowary = class(TForm)
    sb: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    pc: TPageControlEx;
    ListaTowarow: TTabSheet;
    DodajGratisy: TTabSheet;
    EdytujGratis: TTabSheet;
    UsunGratis: TTabSheet;
    ImageList1: TImageList;
    DBGrid1: TDBGrid;
    ToolButton5: TToolButton;
    ADODataSet1: TADODataSet;
    OpenDialog1: TOpenDialog;
    BazaExcel: TTabSheet;
    DataSource1: TDataSource;
    DBGrid2: TDBGrid;
    GroupBox1: TGroupBox;
    LabeledEdit1: TLabeledEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    DBGrid3: TDBGrid;
    Edit1: TEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    LabeledEdit5: TLabeledEdit;
    data_zakupu_i: TDateTimePicker;
    rg: TRadioGroup;
    Label2: TLabel;
    JvImgBtn1: TJvImgBtn;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    DBGrid4: TDBGrid;
    Edit2: TEdit;
    LabeledEdit6: TLabeledEdit;
    LabeledEdit7: TLabeledEdit;
    LabeledEdit8: TLabeledEdit;
    LabeledEdit9: TLabeledEdit;
    data_zakupu_e: TDateTimePicker;
    rg_e: TRadioGroup;
    JvImgBtn2: TJvImgBtn;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    Edit3: TEdit;
    LabeledEdit10: TLabeledEdit;
    LabeledEdit11: TLabeledEdit;
    LabeledEdit12: TLabeledEdit;
    LabeledEdit13: TLabeledEdit;
    data_zakupu_d: TDateTimePicker;
    rg_d: TRadioGroup;
    JvImgBtn3: TJvImgBtn;
    ToolButton6: TToolButton;
    IBQuery1: TIBQuery;
    frxDBDataset1: TfrxDBDataset;
    frxReport1: TfrxReport;
    frxUserDataSet1: TfrxUserDataSet;
    ToolButton7: TToolButton;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    LabeledEdit14: TLabeledEdit;
    LabeledEdit15: TLabeledEdit;
    JvImgBtn4: TJvImgBtn;
    MenuDrukuj: TPopupMenu;
    Drukujstangdetw1: TMenuItem;
    Drukujdokumentwydaniagadetu1: TMenuItem;
    JvImgBtn5: TJvImgBtn;
    frxReport2: TfrxReport;
    frxDBDataset2: TfrxDBDataset;
    frxDBDataset3: TfrxDBDataset;
    JvImgBtn6: TJvImgBtn;
    rgTypDok: TRadioGroup;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    Poprawdokument1: TMenuItem;
    Usudokument1: TMenuItem;
    N1: TMenuItem;
    JvImgBtn7: TJvImgBtn;
    frxDBDataset4: TfrxDBDataset;
    frxReport3: TfrxReport;
    frxDBDataset5: TfrxDBDataset;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure LabeledEdit1Change(Sender: TObject);
    procedure UstawSiatke(aGrid: TDBGrid);
    procedure UstawSiatkeNaglowki(aGrid: TDBGrid);
    procedure UstawSiatkePozycje;
    procedure Edit1Change(Sender: TObject);
    procedure DBGrid3CellClick(Column: TColumn);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure DBGrid5CellClick(Column: TColumn);
    procedure JvImgBtn5Click(Sender: TObject);
    procedure rgTypDokClick(Sender: TObject);
    procedure JvImgBtn6Click(Sender: TObject);
    procedure JvImgBtn4Click(Sender: TObject);
    procedure DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Usudokument1Click(Sender: TObject);
    procedure JvImgBtn7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frTowary: TfrTowary;

implementation

uses baza, NowyDokGadzety, gadzety_dok_lista;

{$R *.dfm}

procedure TfrTowary.ToolButton1Click(Sender: TObject);
begin
ToolButton2.Visible := True;
ToolButton3.Visible := True;
ToolButton4.Visible := True;
ToolButton6.Visible := True;
pc.ActivePageIndex := 0;
end;

procedure TfrTowary.ToolButton2Click(Sender: TObject);
var
   nazwa: String;
begin
pc.ActivePageIndex := 1;
dm.PokazKontrah(0,'D','');

end;

procedure TfrTowary.ToolButton3Click(Sender: TObject);                           //wywolanie edycji
begin
pc.ActivePageIndex := 2;
Edit2.Text := dm.ibqTowary.FieldValues['NAZWA_K'];
LabeledEdit6.Text := dm.ibqTowary.FieldValues['NAZWA'];
LabeledEdit7.Text := dm.ibqTowary.FieldValues['CENA_N_ZAKUP'];
LabeledEdit8.Text := dm.ibqTowary.FieldValues['ILOSC'];
LabeledEdit9.Text := dm.ibqTowary.FieldValues['DATA_WAZNOSCI'];
data_zakupu_e.Date := dm.ibqTowary.FieldValues['DATA_ZAKUPU'];
case dm.ibqTowary.FieldValues['VAT'] of
     23: rg_e.ItemIndex := 0;
     8: rg_e.ItemIndex := 1;
     0: rg_e.ItemIndex := 2;
     5: rg_e.ItemIndex := 3;
     -1: rg_e.ItemIndex := 4;
end;
end;

procedure TfrTowary.ToolButton4Click(Sender: TObject);                           //wywolanie kasowania gadzetow
begin
pc.ActivePageIndex := 3;
Edit3.Text := dm.ibqTowary.FieldValues['NAZWA_K'];
LabeledEdit10.Text := dm.ibqTowary.FieldValues['NAZWA'];
LabeledEdit11.Text := dm.ibqTowary.FieldValues['CENA_N_ZAKUP'];
LabeledEdit12.Text := dm.ibqTowary.FieldValues['ILOSC'];
LabeledEdit13.Text := dm.ibqTowary.FieldValues['DATA_WAZNOSCI'];
data_zakupu_d.Date := dm.ibqTowary.FieldValues['DATA_ZAKUPU'];
case dm.ibqTowary.FieldValues['VAT'] of
     23: rg_d.ItemIndex := 0;
     8: rg_e.ItemIndex := 1;
     0: rg_e.ItemIndex := 2;
     5: rg_e.ItemIndex := 3;
     -1: rg_e.ItemIndex := 4;
end;
end;

procedure TfrTowary.ToolButton5Click(Sender: TObject);
var
   XLSFile, CStr, tekst, nazwa, data_w : string;
   cena_n, ilosc : Real;
   vat, x : Integer;
begin
   if OpenDialog1.Execute() then 
   begin
       XLSFile := OpenDialog1.FileName;

       CStr := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='   
           +XLSFile+'; Extended Properties=Excel 8.0;Persist Security Info=False';

       ADODataSet1.Active := False;    
       ADODataSet1.ConnectionString := Cstr;
       ADODataSet1.CommandType := cmdTableDirect;
       ADODataSet1.CommandText := 'BIURO$';
       ADODataSet1.Active := True;
       tekst := 'Ilo�� kolumn tabeli ' + IntToStr(ADODataSet1.FieldCount) + ', ilo�� wierszy ' + IntToStr(ADODataSet1.RecordCount);
       sb.SimpleText := tekst;
    end;
with DBGrid2.Columns do
     begin
          Items[0].Width := 20;
          Items[1].Width := 200;
          Items[2].Width := 80;
          Items[3].Width := 80;
          Items[4].Width := 80;
     end;
tekst := 'Czy chcesz wprowadzi� zawarto�� pliku ' + XLSFile + ' do bazy Euro';
if MessageDlg(tekst,mtConfirmation,[mbYes,mbNo],0) = 6 then
   for x:= 1 to ADODataSet1.RecordCount - 1 do
       begin
            nazwa := ADODataSet1.Fields.Fields[1].AsString;
            if not ADODataSet1.Fields.Fields[2].IsNull then data_w := ADODataSet1.Fields.Fields[2].AsString;
            if not ADODataSet1.Fields.Fields[3].IsNull then cena_n := StrToFloat(FormatFloat('0.00',ADODataSet1.Fields.Fields[3].AsFloat));
            if not ADODataSet1.Fields.Fields[4].IsNull then vat := ADODataSet1.Fields.Fields[4].AsInteger;
            if not ADODataSet1.Fields.Fields[6].IsNull then ilosc := StrToFloat(FormatFloat('0.000',ADODataSet1.Fields.Fields[6].AsFloat));
            if Length(ADODataSet1.Fields.Fields[1].AsString) = 0 then Break
            else dm.Towar(0,575,1,vat,nazwa,data_w,cena_n,ilosc,now());
            ADODataSet1.Next;
       end
else
    Close;
end;

procedure TfrTowary.FormClose(Sender: TObject; var Action: TCloseAction);
begin
ToolButton5.Visible := False;
pc.Pages[4].TabVisible := False;
pc.ActivePageIndex := 0;
end;

procedure TfrTowary.FormCreate(Sender: TObject);
begin
pc.Pages[4].TabVisible := False;
pc.ActivePageIndex := 0;
end;

procedure TfrTowary.LabeledEdit1Change(Sender: TObject);
begin
dm.SzukajTowar(0,LabeledEdit1.Text);
UstawSiatke(DBGrid1);
end;

procedure TfrTowary.UstawSiatke(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA_K';
          lColumn.Title.Caption := 'Dostawca';
          lColumn.Width := 150;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa gad�etu';
          lColumn.Width := 300;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'CENA_N_ZAKUP';
          lColumn.Title.Caption := 'Cena';
          lColumn.Width := 80;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ILOSC';
          lColumn.Title.Caption := 'Ilo��';
          lColumn.Width := 80;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'WARTOSC_N';
          lColumn.Title.Caption := 'Warto��';
          lColumn.Width := 80;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'DATA_ZAKUPU';
          lColumn.Title.Caption := 'Data zakupu';
          lColumn.Width := 80;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'DATA_WAZNOSCI';
          lColumn.Title.Caption := 'Data wa�no�ci';
          lColumn.Width := 80;
     end;
end;

procedure TfrTowary.Edit1Change(Sender: TObject);
begin
dm.SzukajDostawcy(Edit1.Text,'','');
end;

procedure TfrTowary.DBGrid3CellClick(Column: TColumn);
begin
Edit1.Text := dm.ibqKontrahenci.FieldValues['NAZWA'];
end;

procedure TfrTowary.JvImgBtn1Click(Sender: TObject);                             //dodaj nowy gadzet
var
   nazwa,data_w: String;
   nr_dost,vat: Integer;
   cena,ilosc: Real;
   data_z: TDateTime;
begin
nazwa := LabeledEdit2.Text;
data_w := LabeledEdit5.Text;
nr_dost := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
case rg.ItemIndex of
     0: vat := 23;
     1: vat := 8;
     2: vat := 0;
     3: vat := 5;
     4: vat := -1;
end;
cena := 0;
ilosc  := 0;
data_z := data_zakupu_i.Date;
if dm.TowarUnikalny(nazwa) then
   begin
        dm.Towar(0,nr_dost,1,0,nazwa,data_w,cena,0,data_z);
        dm.SzukajTowar(0,LabeledEdit1.Text);
        UstawSiatke(DBGrid1);
        pc.ActivePageIndex := 0;
   end
else
    ShowMessage('Taki towar ju� jest w bazie, zmie� nazw� i zapisz ponownie');
end;

procedure TfrTowary.ToolButton6Click(Sender: TObject);
begin
frxReport1.PrepareReport(True);
frxReport1.ShowReport(True);
end;

procedure TfrTowary.JvImgBtn2Click(Sender: TObject);                             //edytuj gadzet
var
   nazwa,data_w: String;
   nr_dost,vat,nr_towar: Integer;
   cena,ilosc: Real;
   data_z: TDateTime;
begin
nr_towar := dm.ibqTowary.FieldValues['NR_TOWARY'];
dm.PokazKontrah(0,'D',Edit2.Text);
nr_dost := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
nazwa := LabeledEdit6.Text;
data_w := LabeledEdit9.Text;
case rg_e.ItemIndex of
     0: vat := 23;
     1: vat := 8;
     2: vat := 0;
     3: vat := 5;
     4: vat := -1;
end;
cena := StrToFloat(LabeledEdit7.Text);
ilosc  := StrToFloat(LabeledEdit8.Text);
data_z := data_zakupu_e.Date;
dm.Towar(nr_towar,nr_dost,2,vat,nazwa,data_w,cena,ilosc,data_z);
dm.SzukajTowar(0,LabeledEdit1.Text);
UstawSiatke(DBGrid1);
Edit2.Text := dm.ibqTowary.FieldValues['NAZWA'];
LabeledEdit6.Text := '';
LabeledEdit7.Text := '';
LabeledEdit8.Text := '';
LabeledEdit9.Text := '';
data_zakupu_e.Date :=now();
pc.ActivePageIndex := 0;
end;

procedure TfrTowary.Edit2Change(Sender: TObject);
begin
dm.SzukajDostawcy(Edit2.Text,'','');
end;

procedure TfrTowary.JvImgBtn3Click(Sender: TObject);                             //usuwanie gadzetu
var
   nr_towar: Integer;
begin
nr_towar := dm.ibqTowary.FieldValues['NR_TOWARY'];
dm.Towar(nr_towar,0,3,0,'','',0,0,now());
dm.SzukajTowar(0,'');
UstawSiatke(DBGrid1);
LabeledEdit13.Text := '';
LabeledEdit10.Text := '';
LabeledEdit11.Text := '';
LabeledEdit12.Text := '';
data_zakupu_d.Date :=now();
pc.ActivePageIndex := 0;
end;

procedure TfrTowary.ToolButton7Click(Sender: TObject);
begin
ToolButton2.Visible := False;
ToolButton3.Visible := False;
ToolButton4.Visible := False;
ToolButton6.Visible := False;
pc.ActivePageIndex := 5;
dm.NaglowekG(1,0,0,1,0,0);
UstawSiatkeNaglowki(DBGrid5);
end;

procedure TfrTowary.UstawSiatkeNaglowki(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'DATA_WYSTAWIENIA';
          lColumn.Title.Caption := 'Data wystawienia';
          lColumn.Width := 80;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'IMIE_NAZWISKO';
          lColumn.Title.Caption := 'Imi� i nazwisko';
          lColumn.Width := 250;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa firmy';
          lColumn.Width := 300;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NIP';
          lColumn.Title.Caption := 'NIP firmy';
          lColumn.Width := 80;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'WARTOSC_NETTO';
          lColumn.Title.Caption := 'Warto�� dokumentu';
          lColumn.Width := 100;
     end;
end;

procedure TfrTowary.DBGrid5CellClick(Column: TColumn);
var
   nr: Integer;
begin
nr := dm.ibqGadzetyN.FieldValues['NUMER'];
dm.PozycjeG(2,nr,0,0,'',0,0,0);
UstawSiatkePozycje;
end;

procedure TfrTowary.UstawSiatkePozycje;
begin
with DBGrid6 do
     begin
          Columns[0].Visible := False;
          Columns[1].Visible := False;
          Columns[2].Width := 250;
          Columns[3].Width := 80;
          Columns[4].Width := 80;
          Columns[5].Width := 80;
          Columns[2].Title.Caption := 'Nazwa gad�etu';
          Columns[3].Title.Caption := 'Ilo��';
          Columns[4].Title.Caption := 'Cena';
          Columns[5].Title.Caption := 'Warto��';
     end;
end;

procedure TfrTowary.JvImgBtn5Click(Sender: TObject);
begin
frxReport2.PrepareReport(True);
frxReport2.ShowReport(True);
end;

procedure TfrTowary.rgTypDokClick(Sender: TObject);
begin
case rgTypDok.ItemIndex of
     0: dm.NaglowekG(1,0,0,1,0,0);
     1: dm.NaglowekG(1,0,0,2,0,0);
     2: dm.NaglowekG(1,0,0,3,0,0);
end;
UstawSiatkeNaglowki(DBGrid5);
end;

procedure TfrTowary.JvImgBtn6Click(Sender: TObject);
begin
// dokument dostawy
dm.PokazKontrah(0,'D','');
frNowyDokG.UstawTypDok(2);
frNowyDokG.UstawSiatkeD(frNowyDokG.DBGrid5);
frNowyDokG.pc.ActivePageIndex := 0;
frNowyDokG.ShowModal;
end;

procedure TfrTowary.JvImgBtn4Click(Sender: TObject);
begin
// dokument wydania
dm.PokazKontrah(0,'O','');
frNowyDokG.UstawTypDok(3);
frNowyDokG.UstawSiatkeO(frNowyDokG.DBGrid5);
frNowyDokG.pc.ActivePageIndex := 0;
frNowyDokG.ShowModal;
end;

procedure TfrTowary.DBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if Column.Field.DataType In [ftFloat, ftCurrency, ftBCD] then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.00 z�';

  with DBGrid5.Canvas do
  begin
    if not((gdFocused in State) or (gdSelected in State)) then
    begin
      Font.Color:=clblack;
      if dm.dsGadzetyN.DataSet.FieldValues['TYP_DOK'] = 'R' then
        Brush.Color:=cl3Dlight
      else
        Brush.Color:=clwhite;
    end;
  DBGrid5.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TfrTowary.DBGrid6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if (Column.Field.DataType In [ftFloat, ftCurrency, ftBCD]) and (Column.Field.FieldName = 'CENA_NETTO') or (Column.Field.FieldName = 'WARTOSC_NETTO') then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.00 z�';
end;

procedure TfrTowary.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if (Column.Field.DataType In [ftFloat, ftCurrency, ftBCD]) and (Column.Field.FieldName = 'CENA_N_ZAKUP') or (Column.Field.FieldName = 'WARTOSC_N') then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.00 z�';
end;

procedure TfrTowary.Usudokument1Click(Sender: TObject);
type
  TGadzetP = record
    nr_pozycji : Integer;
    ilosc : Real;
    pozycja: String;
  end;

var
   nr_naglowek,nr_pozycji,ile_pozycji,nr_towar,x: Integer;
   ListaPozycji: array of TGadzetP;
   typ_dokumentu: String;
   ilosc: Real;
begin
//usuwa dokument przychodu lub rozchodu
if MessageBox(Handle,'Czy chcesz usun�� wybrany dokument ?','Kasowanie dokumentu',MB_YESNO + MB_ICONQUESTION) = IdYes then
   begin
        //dm.UsunPozG(nr_naglowek,nr_pozycji);
        //dm.TowaryStany(nr_towar,ilosc);
        nr_naglowek := dm.ibqGadzetyN.FieldValues['NUMER'];
        typ_dokumentu := dm.ibqGadzetyN.FieldValues['TYP_DOK'];
        ile_pozycji := dm.IlePozycji(nr_naglowek);
        SetLength(ListaPozycji,ile_pozycji);
        dm.PokazGadzetyP(nr_naglowek);
        for x:= 0 to ile_pozycji - 1 do
            begin
                 ListaPozycji[x].nr_pozycji := dm.ibqGadzetyP.FieldValues['NR_GADZETY_POZYCJE'];
                 ListaPozycji[x].ilosc := dm.ibqGadzetyP.FieldValues['ILOSC'];
                 ListaPozycji[x].pozycja := dm.ibqGadzetyP.FieldValues['POZYCJA'];
                 dm.ibqGadzetyP.Next;
            end;
        for x:= 0 to ile_pozycji - 1 do
            begin
                 dm.UsunPozG(nr_naglowek,ListaPozycji[x].nr_pozycji);
                 if typ_dokumentu = 'P' then
                    begin
                         nr_towar := dm.NrTowar(ListaPozycji[x].pozycja);
                         ilosc := dm.IleJestTowaru(nr_towar,'');
                         ilosc := ilosc - ListaPozycji[x].ilosc;
                         dm.TowaryStany(nr_towar,ilosc);
                    end
                 else
                    begin
                         nr_towar := dm.NrTowar(ListaPozycji[x].pozycja);
                         ilosc := dm.IleJestTowaru(nr_towar,'');
                         ilosc := ilosc + ListaPozycji[x].ilosc;
                         dm.TowaryStany(nr_towar,ilosc);
                    end;
                 dm.NaglowekG(5,0,nr_naglowek,0,0,0);
            end;
   dm.NaglowekG(1,0,0,1,0,0);
   UstawSiatkeNaglowki(DBGrid5);
   end;
end;

procedure TfrTowary.JvImgBtn7Click(Sender: TObject);
begin
//DRUKUJE LIST� DOKUMENT�W PRZYJ�CIA LUB WYDANIA GADZETOW
case rgTypDok.ItemIndex of
     0 : frGadzetyDokLista.Label1.Caption := 'Drukuj wszystkie dokumenty (przyj�cie i wydanie)';
     1 : frGadzetyDokLista.Label1.Caption := 'Drukuj dokumenty przyj�cia';
     2 : frGadzetyDokLista.Label1.Caption := 'Drukuj dokumenty wydania';
     end;
frGadzetyDokLista.setRodzajDok(rgTypDok.ItemIndex);
frGadzetyDokLista.ShowModal;
end;

end.


