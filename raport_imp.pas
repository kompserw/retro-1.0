unit raport_imp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery;

type
  TfrRaportImport = class(TForm)
    ibqRaportImport: TIBQuery;
    dsRaportImport: TDataSource;
    ibqRaportImportNAZWA: TIBStringField;
    ibqRaportImportMIASTO: TIBStringField;
    ibqRaportImportDOSTAWCA: TIBStringField;
    ibqRaportImportOBROTY: TIBBCDField;
    ibqRaportImportPROWIZJA_KWOTA: TFloatField;
    ibqRaportImportPROW_E_KWOTA: TIBBCDField;
    ibqRaportImportZYSK: TIBBCDField;
    ibqRaportImportOKRES_M: TIBStringField;
    ibqRaportImportOKRES_Q: TIBStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frRaportImport: TfrRaportImport;

implementation
uses baza,glowny;
{$R *.dfm}

end.
