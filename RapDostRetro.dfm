object frRapDostRetra: TfrRapDostRetra
  Left = 282
  Top = 252
  Width = 1142
  Height = 656
  Caption = 'Raport obroty dostawcy z podzia'#322'em na retra'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ibqDostawca: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT NAZWA, NR_KONTRAH FROM KONTRAH')
    Left = 840
    Top = 16
    object ibqDostawcaNAZWA: TIBStringField
      FieldName = 'NAZWA'
      Origin = 'KONTRAH.NAZWA'
      Required = True
      Size = 250
    end
    object ibqDostawcaNR_KONTRAH: TIntegerField
      FieldName = 'NR_KONTRAH'
      Origin = 'KONTRAH.NR_KONTRAH'
      Required = True
    end
  end
  object ibqPokazObroty: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    DataSource = dsDostawca
    SQL.Strings = (
      
        'SELECT  B.OPIS,A.NAZWA_D,sum(A.OBROT) AS OBROT,sum(A.PROWIZJA_SI' +
        'EC) AS PROWIZJA_SIEC,sum(A.PROWIZJA_SKLEP) AS PROWIZJA_SKLEP,sum' +
        '(A.ZYSK) AS ZYSK FROM P_OBROT_PROW_NR_PROW_1('#39'%'#39') A,PROWIZJE B'
      'where B.NR_PROWIZJE = A.NR_PROWIZJE AND A.NAZWA_D like '#39'%'#39
      'group by A.NAZWA_D,B.OPIS')
    Left = 912
    Top = 16
    object ibqPokazObrotyOPIS: TIBStringField
      FieldName = 'OPIS'
      Origin = 'PROWIZJE.OPIS'
      Required = True
      Size = 50
    end
    object ibqPokazObrotyNAZWA_D: TIBStringField
      FieldName = 'NAZWA_D'
      Origin = 'P_OBROT_PROW_NR_PROW_1.NAZWA_D'
      Size = 250
    end
    object ibqPokazObrotyOBROT: TIBBCDField
      FieldName = 'OBROT'
      Precision = 18
      Size = 2
    end
    object ibqPokazObrotyPROWIZJA_SIEC: TIBBCDField
      FieldName = 'PROWIZJA_SIEC'
      Precision = 18
      Size = 2
    end
    object ibqPokazObrotyPROWIZJA_SKLEP: TIBBCDField
      FieldName = 'PROWIZJA_SKLEP'
      Precision = 18
      Size = 2
    end
    object ibqPokazObrotyZYSK: TIBBCDField
      FieldName = 'ZYSK'
      Precision = 18
      Size = 2
    end
  end
  object dsObrotOpisRetra: TDataSource
    DataSet = ibqPokazObroty
    Left = 976
    Top = 88
  end
  object dsDostawca: TDataSource
    DataSet = ibqDostawca
    Left = 976
    Top = 16
  end
  object DataSource1: TDataSource
    Left = 856
    Top = 200
  end
  object DataSource2: TDataSource
    Left = 928
    Top = 200
  end
  object JvADODataSet1: TJvADODataSet
    Parameters = <>
    DialogOptions.FormStyle = fsNormal
    Left = 848
    Top = 304
  end
end
