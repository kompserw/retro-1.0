program retro;

uses
  Forms,
  glowny in 'glowny.pas' {frGlowny},
  baza in 'baza.pas' {dm: TDataModule},
  kontrahent in 'kontrahent.pas' {frDodajK},
  dodajR in 'dodajR.pas' {frReczny},
  info in 'info.pas' {frInfo},
  obrotyK in 'obrotyK.pas' {frObrotyK},
  rera in 'rera.pas' {frRetra},
  obrotyD in 'obrotyD.pas' {frObrotyD},
  ListaImp in 'ListaImp.pas' {frListaImp},
  LaczKartoteki in 'LaczKartoteki.pas' {frPolaczKartoteki},
  KreaotorRap in 'KreaotorRap.pas' {frKreatorRaportow},
  Rap_Importy in 'Rap_Importy.pas' {rpImporty},
  trasy in 'trasy.pas' {frTrasy},
  pracownicy in 'pracownicy.pas' {frPracownicy},
  grupy in 'grupy.pas' {frGrupy},
  towary in 'towary.pas' {frTowary},
  gadzety_dokument in 'gadzety_dokument.pas' {frGadzetNowy},
  wydaj_gadzety in 'wydaj_gadzety.pas' {frWydajGadzety},
  update in 'update.pas' {frUpdate},
  NowyDokGadzety in 'NowyDokGadzety.pas' {frNowyDokG},
  dodajPGadzet in 'dodajPGadzet.pas' {frDodajPGadzet},
  gadzety_dok_lista in 'gadzety_dok_lista.pas' {frGadzetyDokLista},
  aktywni_d in 'aktywni_d.pas' {frAktywniDostawcy},
  testy in 'testy.pas' {Form1};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrGlowny, frGlowny);
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TfrDodajK, frDodajK);
  Application.CreateForm(TfrReczny, frReczny);
  Application.CreateForm(TfrInfo, frInfo);
  Application.CreateForm(TfrObrotyK, frObrotyK);
  Application.CreateForm(TfrRetra, frRetra);
  Application.CreateForm(TfrObrotyD, frObrotyD);
  Application.CreateForm(TfrListaImp, frListaImp);
  Application.CreateForm(TfrPolaczKartoteki, frPolaczKartoteki);
  Application.CreateForm(TfrKreatorRaportow, frKreatorRaportow);
  Application.CreateForm(TrpImporty, rpImporty);
  Application.CreateForm(TfrTrasy, frTrasy);
  Application.CreateForm(TfrPracownicy, frPracownicy);
  Application.CreateForm(TfrGrupy, frGrupy);
  Application.CreateForm(TfrTowary, frTowary);
  Application.CreateForm(TfrGadzetNowy, frGadzetNowy);
  Application.CreateForm(TfrWydajGadzety, frWydajGadzety);
  Application.CreateForm(TfrUpdate, frUpdate);
  Application.CreateForm(TfrNowyDokG, frNowyDokG);
  Application.CreateForm(TfrDodajPGadzet, frDodajPGadzet);
  Application.CreateForm(TfrGadzetyDokLista, frGadzetyDokLista);
  Application.CreateForm(TfrAktywniDostawcy, frAktywniDostawcy);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.

