object frDodajK: TfrDodajK
  Left = 165
  Top = 200
  Width = 682
  Height = 444
  Caption = 'Dodawanie kontrahenta'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object sb: TStatusBar
    Left = 0
    Top = 386
    Width = 666
    Height = 19
    Panels = <>
  end
  object pc: TPageControlEx
    Left = 0
    Top = 0
    Width = 666
    Height = 386
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 1
    HideHeader = False
    object TabSheet1: TTabSheet
      Caption = 'Modyfikuj kontrahenta'
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 71
        Height = 13
        Caption = 'Imi'#281' i nazwisko'
      end
      object Label2: TLabel
        Left = 8
        Top = 56
        Width = 67
        Height = 13
        Caption = 'Kod pocztowy'
      end
      object Label3: TLabel
        Left = 264
        Top = 56
        Width = 22
        Height = 13
        Caption = 'Ulica'
      end
      object Label4: TLabel
        Left = 152
        Top = 8
        Width = 59
        Height = 13
        Caption = 'Nazwa firmy'
      end
      object Label5: TLabel
        Left = 88
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Miasto'
      end
      object Label6: TLabel
        Left = 144
        Top = 104
        Width = 17
        Height = 13
        Caption = 'NIP'
      end
      object Label7: TLabel
        Left = 8
        Top = 104
        Width = 36
        Height = 13
        Caption = 'Telefon'
      end
      object Label8: TLabel
        Left = 280
        Top = 104
        Width = 46
        Height = 13
        Caption = 'Kategoria'
      end
      object Label9: TLabel
        Left = 416
        Top = 104
        Width = 66
        Height = 13
        Caption = 'Metra'#380' sklepu'
      end
      object Label10: TLabel
        Left = 120
        Top = 152
        Width = 90
        Height = 13
        Caption = 'Data przyst'#261'pienia'
      end
      object Label12: TLabel
        Left = 8
        Top = 264
        Width = 29
        Height = 13
        Caption = 'Uwagi'
      end
      object Label26: TLabel
        Left = 120
        Top = 192
        Width = 60
        Height = 13
        Caption = 'Nazwa trasy'
      end
      object Label29: TLabel
        Left = 120
        Top = 232
        Width = 111
        Height = 13
        Caption = 'Obs'#322'uguj'#261'cy pracownik'
      end
      object edImieNaz: TJvEdit
        Left = 8
        Top = 24
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object edKodP: TJvEdit
        Left = 8
        Top = 72
        Width = 65
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object edUlica: TJvEdit
        Left = 264
        Top = 72
        Width = 273
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object mUwagi: TMemo
        Left = 8
        Top = 280
        Width = 473
        Height = 66
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          '')
        ParentFont = False
        TabOrder = 3
      end
      object edMetraz: TJvEdit
        Left = 416
        Top = 120
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object edNIP: TJvEdit
        Left = 144
        Top = 120
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object edMiasto: TJvEdit
        Left = 88
        Top = 72
        Width = 161
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object edNazwa: TJvEdit
        Left = 152
        Top = 24
        Width = 385
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object edTelefon: TJvEdit
        Left = 8
        Top = 120
        Width = 121
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
      end
      object edKategoria: TJvEdit
        Left = 280
        Top = 120
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
      object rgTypK: TJvRadioGroup
        Left = 488
        Top = 160
        Width = 161
        Height = 113
        Caption = 'Rodzaj kontrahenta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Items.Strings = (
          'Dostawca sieci'
          'Odbiorca - sklepy')
        ParentFont = False
        TabOrder = 10
      end
      object JvImgBtn1: TJvImgBtn
        Left = 488
        Top = 320
        Width = 161
        Height = 25
        Caption = 'Zapisz kontrahenta do bazy'
        TabOrder = 11
        OnClick = JvImgBtn2Click
        Flat = True
        Images = frGlowny.img
        ImageIndex = 18
      end
      object CzyAktywny: TCheckBox
        Left = 552
        Top = 24
        Width = 89
        Height = 17
        Caption = 'Aktywny ?'
        Checked = True
        State = cbChecked
        TabOrder = 12
      end
      object DataPrzystapienia: TDateTimePicker
        Left = 120
        Top = 168
        Width = 186
        Height = 21
        Date = 42614.008854317130000000
        Time = 42614.008854317130000000
        TabOrder = 13
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 152
        Width = 100
        Height = 105
        Caption = 'Rodzaj koncesji'
        TabOrder = 14
        object CheckBox1: TCheckBox
          Left = 8
          Top = 24
          Width = 73
          Height = 17
          Caption = 'typ A'
          TabOrder = 0
        end
        object CheckBox2: TCheckBox
          Left = 8
          Top = 48
          Width = 65
          Height = 17
          Caption = 'typ B'
          TabOrder = 1
        end
        object CheckBox3: TCheckBox
          Left = 8
          Top = 72
          Width = 65
          Height = 17
          Caption = 'typ C'
          TabOrder = 2
        end
      end
      object ComboBox2: TComboBox
        Left = 120
        Top = 208
        Width = 185
        Height = 21
        ItemHeight = 0
        TabOrder = 15
        OnChange = ComboBox2Change
        OnClick = ComboBox2Click
      end
      object ComboBox5: TComboBox
        Left = 120
        Top = 248
        Width = 185
        Height = 21
        ItemHeight = 0
        TabOrder = 16
      end
      object GroupBox3: TGroupBox
        Left = 312
        Top = 160
        Width = 169
        Height = 113
        Caption = 'Harmonogram wizyt'
        TabOrder = 17
        object CheckBox8: TCheckBox
          Left = 8
          Top = 16
          Width = 97
          Height = 17
          Caption = 'I tydzie'#324
          TabOrder = 0
        end
        object CheckBox9: TCheckBox
          Left = 8
          Top = 35
          Width = 97
          Height = 17
          Caption = 'II tydzie'#324
          TabOrder = 1
        end
        object CheckBox10: TCheckBox
          Left = 8
          Top = 53
          Width = 97
          Height = 17
          Caption = 'III tydzie'#324
          TabOrder = 2
        end
        object CheckBox11: TCheckBox
          Left = 8
          Top = 72
          Width = 97
          Height = 17
          Caption = 'IV tydzie'#324
          TabOrder = 3
        end
        object CheckBox12: TCheckBox
          Left = 8
          Top = 91
          Width = 97
          Height = 17
          Caption = 'V tydzie'#324
          TabOrder = 4
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Informacje CMR'
      ImageIndex = 1
      object Label11: TLabel
        Left = 16
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Informacja z ...'
      end
      object Label13: TLabel
        Left = 184
        Top = 120
        Width = 186
        Height = 13
        Caption = 'Maksymalna ilo'#347#263' znak'#243'w 255, zosta'#322'o '
      end
      object Label14: TLabel
        Left = 184
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Tre'#347#263' informacji'
      end
      object ComboBox1: TComboBox
        Left = 16
        Top = 32
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'wybierz z listy'
        Items.Strings = (
          'z telefonu'
          'z e-maila'
          'osobista')
      end
      object Memo1: TMemo
        Left = 184
        Top = 32
        Width = 345
        Height = 89
        Lines.Strings = (
          '')
        TabOrder = 1
        OnChange = Memo1Change
      end
      object Button1: TButton
        Left = 16
        Top = 96
        Width = 145
        Height = 25
        Caption = 'Zapisz informacj'#281
        TabOrder = 2
        OnClick = Button1Click
      end
      object Siatka: TDBGrid
        Left = 16
        Top = 176
        Width = 505
        Height = 177
        DataSource = dm.dsFormKontrah
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object DBNavigator1: TDBNavigator
        Left = 16
        Top = 152
        Width = 168
        Height = 25
        DataSource = dm.dsFormKontrah
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 4
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Dodaj kontrahenta'
      ImageIndex = 2
      object Label15: TLabel
        Left = 8
        Top = 8
        Width = 71
        Height = 13
        Caption = 'Imi'#281' i nazwisko'
      end
      object Label16: TLabel
        Left = 152
        Top = 8
        Width = 59
        Height = 13
        Caption = 'Nazwa firmy'
      end
      object Label17: TLabel
        Left = 8
        Top = 56
        Width = 67
        Height = 13
        Caption = 'Kod pocztowy'
      end
      object Label18: TLabel
        Left = 88
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Miasto'
      end
      object Label19: TLabel
        Left = 264
        Top = 56
        Width = 22
        Height = 13
        Caption = 'Ulica'
      end
      object Label20: TLabel
        Left = 8
        Top = 104
        Width = 36
        Height = 13
        Caption = 'Telefon'
      end
      object Label21: TLabel
        Left = 144
        Top = 104
        Width = 17
        Height = 13
        Caption = 'NIP'
      end
      object Label22: TLabel
        Left = 280
        Top = 104
        Width = 46
        Height = 13
        Caption = 'Kategoria'
      end
      object Label23: TLabel
        Left = 416
        Top = 104
        Width = 66
        Height = 13
        Caption = 'Metra'#380' sklepu'
      end
      object Label24: TLabel
        Left = 120
        Top = 152
        Width = 90
        Height = 13
        Caption = 'Data przyst'#261'pienia'
      end
      object Label25: TLabel
        Left = 8
        Top = 264
        Width = 29
        Height = 13
        Caption = 'Uwagi'
      end
      object Label27: TLabel
        Left = 120
        Top = 192
        Width = 60
        Height = 13
        Caption = 'Nazwa trasy'
      end
      object Label28: TLabel
        Left = 120
        Top = 232
        Width = 111
        Height = 13
        Caption = 'Obs'#322'uguj'#261'cy pracownik'
      end
      object JvEdit1: TJvEdit
        Left = 8
        Top = 24
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object JvEdit2: TJvEdit
        Left = 152
        Top = 24
        Width = 385
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object JvEdit3: TJvEdit
        Left = 8
        Top = 72
        Width = 65
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object JvEdit4: TJvEdit
        Left = 88
        Top = 72
        Width = 161
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object JvEdit5: TJvEdit
        Left = 264
        Top = 72
        Width = 273
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object JvEdit6: TJvEdit
        Left = 8
        Top = 120
        Width = 121
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object JvEdit7: TJvEdit
        Left = 144
        Top = 120
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object JvEdit8: TJvEdit
        Left = 280
        Top = 120
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object JvEdit9: TJvEdit
        Left = 416
        Top = 120
        Width = 121
        Height = 21
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 152
        Width = 100
        Height = 105
        Caption = 'Rodzaj koncesji'
        TabOrder = 9
        object CheckBox4: TCheckBox
          Left = 8
          Top = 24
          Width = 70
          Height = 17
          Caption = 'typ A'
          TabOrder = 0
        end
        object CheckBox5: TCheckBox
          Left = 8
          Top = 48
          Width = 70
          Height = 17
          Caption = 'typ B'
          TabOrder = 1
        end
        object CheckBox6: TCheckBox
          Left = 8
          Top = 72
          Width = 70
          Height = 17
          Caption = 'typ C'
          TabOrder = 2
        end
      end
      object DateTimePicker1: TDateTimePicker
        Left = 120
        Top = 168
        Width = 186
        Height = 21
        Date = 42614.008854317130000000
        Time = 42614.008854317130000000
        TabOrder = 10
      end
      object CheckBox7: TCheckBox
        Left = 552
        Top = 24
        Width = 161
        Height = 17
        Caption = 'Aktywny ?'
        Checked = True
        State = cbChecked
        TabOrder = 11
      end
      object rgTypK_N: TJvRadioGroup
        Left = 488
        Top = 160
        Width = 161
        Height = 113
        Caption = 'Rodzaj kontrahenta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Items.Strings = (
          'Dostawca sieci'
          'Odbiorca - sklepy')
        ParentFont = False
        TabOrder = 12
      end
      object Memo2: TMemo
        Left = 8
        Top = 280
        Width = 473
        Height = 66
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          '')
        ParentFont = False
        TabOrder = 13
      end
      object JvImgBtn4: TJvImgBtn
        Left = 488
        Top = 320
        Width = 161
        Height = 25
        Caption = 'Zapisz kontrahenta do bazy'
        TabOrder = 14
        OnClick = JvImgBtn1Click
        Flat = True
        Images = frGlowny.img
        ImageIndex = 18
      end
      object ComboBox3: TComboBox
        Left = 120
        Top = 208
        Width = 185
        Height = 21
        ItemHeight = 13
        TabOrder = 15
        OnChange = ComboBox3Change
      end
      object ComboBox4: TComboBox
        Left = 120
        Top = 248
        Width = 185
        Height = 21
        ItemHeight = 13
        TabOrder = 16
      end
      object GroupBox4: TGroupBox
        Left = 312
        Top = 160
        Width = 169
        Height = 113
        Caption = 'Harmonogram wizyt'
        TabOrder = 17
        object CheckBox13: TCheckBox
          Left = 8
          Top = 16
          Width = 97
          Height = 17
          Caption = 'I tydzie'#324
          TabOrder = 0
        end
        object CheckBox14: TCheckBox
          Left = 8
          Top = 35
          Width = 97
          Height = 17
          Caption = 'II tydzie'#324
          TabOrder = 1
        end
        object CheckBox15: TCheckBox
          Left = 8
          Top = 53
          Width = 97
          Height = 17
          Caption = 'III tydzie'#324
          TabOrder = 2
        end
        object CheckBox16: TCheckBox
          Left = 8
          Top = 72
          Width = 97
          Height = 17
          Caption = 'IV tydzie'#324
          TabOrder = 3
        end
        object CheckBox17: TCheckBox
          Left = 8
          Top = 91
          Width = 97
          Height = 17
          Caption = 'V tydzie'#324
          TabOrder = 4
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Trasy i pracownicy'
      ImageIndex = 3
    end
  end
end
