unit LaczKartoteki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, JvDataSource, DBCtrls, JvDBControls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid, ExtCtrls, JvExExtCtrls, JvNetscapeSplitter,
  JvExtComponent, JvPanel, ToolWin, ComCtrls, JvExComCtrls, JvToolBar,
  StdCtrls, JvExStdCtrls, JvEdit, JvExGrids, JvStringGrid, IBCustomDataSet, IBQuery;

type
  TfrPolaczKartoteki = class(TForm)
    JvToolBar1: TJvToolBar;
    JvPanel1: TJvPanel;
    JvNetscapeSplitter1: TJvNetscapeSplitter;
    JvPanel2: TJvPanel;
    JvDBUltimGrid1: TJvDBUltimGrid;
    JvDBNavigator1: TJvDBNavigator;
    dsPokazKontrah: TJvDataSource;
    JvEdit1: TJvEdit;
    ToolButton1: TToolButton;
    JvDBUltimGrid2: TJvDBUltimGrid;
    dsPokazTMP: TJvDataSource;
    PokazKontrah: TIBQuery;
    wstawDoTMP: TIBQuery;
    pPokazTMP: TIBQuery;
    procedure JvEdit1Change(Sender: TObject);
    procedure JvDBUltimGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frPolaczKartoteki: TfrPolaczKartoteki;
  x,y : Integer;
implementation
uses glowny,baza;
{$R *.dfm}

procedure TfrPolaczKartoteki.JvEdit1Change(Sender: TObject);
begin
with PokazKontrah do
     begin
          Close;
          //UnPrepare;
          ParamByName('A').AsString := '%' + UpperCase(JvEdit1.Text) + '%';
          //Prepare;
          Open;
     end;
end;

procedure TfrPolaczKartoteki.JvDBUltimGrid1DblClick(Sender: TObject);
begin
with wstawDoTMP do
     begin
          Close;
          //UnPrepare;
          ParamByName('NR_KONTRAH').AsInteger := PokazKontrah.FieldValues['NR_KONTRAH'];
          ParamByName('NAZWA_PUNKTU').AsString := PokazKontrah.FieldValues['NAZWA'];
          ParamByName('ULICA').AsString := PokazKontrah.FieldValues['ULICA'];
          ParamByName('MIASTO').AsString := PokazKontrah.FieldValues['MIASTO'];
          ParamByName('KOD_P').AsString := PokazKontrah.FieldValues['KOD_P'];
          ParamByName('TELEFON').AsString := PokazKontrah.FieldValues['TELEFON'];
          ParamByName('KONCESJA').AsString := PokazKontrah.FieldValues['KONCESJA'];
          ParamByName('METRAZ').AsString := PokazKontrah.FieldValues['METRAZ'];
          ParamByName('KATEGORIA').AsString := PokazKontrah.FieldValues['KATEGORIA'];
          //Prepare;
          Open;
     end;
end;

procedure TfrPolaczKartoteki.FormShow(Sender: TObject);
begin
//
end;

end.
