unit dodajPGadzet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExStdCtrls, JvButton, JvCtrls, ExtCtrls;

type
  TfrDodajPGadzet = class(TForm)
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    JvImgBtn1: TJvImgBtn;
    Label1: TLabel;
    Label2: TLabel;
    procedure UstawTypDok(typ: Integer);
    procedure JvImgBtn1Click(Sender: TObject);
  private
    Typ_Dok: Integer;
  public
    { Public declarations }

  end;

var
  frDodajPGadzet: TfrDodajPGadzet;
  nr_towar,nr_naglowek,nr_kontrah: Integer;

implementation
uses glowny,baza, NowyDokGadzety;
{$R *.dfm}

{procedure Tdm.PozycjeG(czynnosc, nr_naglowek, nr_pozycja,nr_kontrah: Integer; pozycja: String;
  ilosc, cena, wartosc: Real);}

procedure TfrDodajPGadzet.JvImgBtn1Click(Sender: TObject);
var
   nazwa: String;
   ilosc,cena_s,wartosc,ile_jest: Real;

begin
nr_naglowek := nr_naglowek + 1;
nazwa := LabeledEdit1.Text;
ilosc := StrToFloat(LabeledEdit2.Text);
cena_s := StrToFloat(LabeledEdit4.Text);
wartosc := cena_s*ilosc;
ile_jest := dm.IleJestTowaru(nr_towar,'');
if Typ_Dok = 2 then
   begin        //DODAJE TOWARY NA STAN
        dm.PozycjeG(4,nr_naglowek,nr_towar,nr_kontrah,nazwa,ilosc,cena_s,wartosc);
        ilosc := ilosc + ile_jest;
        dm.TowaryStany(nr_towar,nr_kontrah,ilosc,cena_s);
   end
else
    begin
         if ile_jest >= ilosc then
            begin
                 dm.PozycjeG(4,nr_naglowek,nr_towar,nr_kontrah,nazwa,ilosc,cena_s,wartosc);
                 ilosc := ile_jest - ilosc;
                 dm.TowaryStany(nr_towar,ilosc,cena_s);
            end
         else
             begin
                  ilosc := 0;
                  ShowMessage('Uwaga: ilo�� kt�r� chcesz wyda� z magazynu jest wi�ksza ni� ilo�� dost�pna na magazynie');
             end;
   end;
dm.PozycjeG(2,nr_naglowek,0,0,'',0,0,0);
nr_towar := 0;nr_naglowek := 0;nr_kontrah := 0;
LabeledEdit1.Text := '';
LabeledEdit2.Text := '';
LabeledEdit3.Text := '';
LabeledEdit4.Text := '';
Label2.Caption := '';
dm.SzukajTowar(0,'');
frNowyDokG.UstawSiatkeT(frNowyDokG.DBGrid1);
frNowyDokG.UstawSiatkeP(frNowyDokG.DBGrid2);
Close;
end;

procedure TfrDodajPGadzet.UstawTypDok(typ: Integer);
begin
Typ_Dok := typ;
end;

end.
