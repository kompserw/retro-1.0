object frRapObrSklepy: TfrRapObrSklepy
  Left = 468
  Top = 327
  Width = 1142
  Height = 656
  Caption = 'Raport obrot'#243'w sklepu'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ibqObroty: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select NAZWA,sum(OBROTY) AS OBROTY,sum(PROWIZJA_KWOTA) AS PROWIZ' +
        'JA_SKLEPY,sum(PROW_E_KWOTA) AS PROWIZJE_EURO,sum(ZYSK) AS ZYSK f' +
        'rom OBROTY,KONTRAH'
      'where NR_KONTRAH=NR_KONTRAH_SKLEP AND OKRES_M LIKE :A'
      'group by NAZWA'
      'order by NAZWA')
    Left = 888
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'A'
        ParamType = ptUnknown
      end>
    object ibqObrotyNAZWA: TIBStringField
      FieldName = 'NAZWA'
      Origin = 'KONTRAH.NAZWA'
      Required = True
      Size = 250
    end
    object ibqObrotyOBROTY: TIBBCDField
      FieldName = 'OBROTY'
      DisplayFormat = '# ##0.00 z'#322
      Precision = 18
      Size = 2
    end
    object ibqObrotyPROWIZJA_SKLEPY: TFloatField
      FieldName = 'PROWIZJA_SKLEPY'
      DisplayFormat = '# ##0.00 z'#322
    end
    object ibqObrotyPROWIZJE_EURO: TIBBCDField
      FieldName = 'PROWIZJE_EURO'
      DisplayFormat = '# ##0.00 z'#322
      Precision = 18
      Size = 2
    end
    object ibqObrotyZYSK: TIBBCDField
      FieldName = 'ZYSK'
      DisplayFormat = '# ##0.00 z'#322
      Precision = 18
      Size = 2
    end
  end
  object ibqSuma: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 888
    Top = 56
  end
  object dsObroty: TDataSource
    DataSet = ibqObroty
    Left = 944
    Top = 16
  end
end
