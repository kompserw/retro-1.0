unit gadzety_dok_lista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExStdCtrls, JvButton, JvCtrls, ExtCtrls;

type
  TfrGadzetyDokLista = class(TForm)
    GroupBox2: TGroupBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    CheckBox18: TCheckBox;
    CheckBox19: TCheckBox;
    CheckBox20: TCheckBox;
    CheckBox21: TCheckBox;
    CheckBox22: TCheckBox;
    CheckBox23: TCheckBox;
    CheckBox24: TCheckBox;
    Label1: TLabel;
    JvImgBtn7: TJvImgBtn;
    RadioGroup1: TRadioGroup;
    procedure GroupBox2Click(Sender: TObject);
    procedure JvImgBtn7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure setRodzajDok(rodzaj: Integer);
  end;

var
  frGadzetyDokLista: TfrGadzetyDokLista;
  rodzaj_dok: Integer; 
implementation
uses towary, baza;
{$R *.dfm}

procedure TfrGadzetyDokLista.GroupBox2Click(Sender: TObject);
begin
JvImgBtn7.Enabled := True;
end;

procedure TfrGadzetyDokLista.JvImgBtn7Click(Sender: TObject);
var
   mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12,rodzaj_dok,zawartosc: Integer;
   raporty, miesiac: String;
begin
raporty  := ExtractFilePath(Application.ExeName) + '/raporty/lista dokumentow.fr3';
mi1 := 0;mi2 := 0;mi3 := 0;mi4 := 0;mi1 := 0;mi5 := 0;mi6 := 0;mi7 := 0;mi8 := 0;mi9 := 0;mi10 := 0;mi11 := 0;
mi12 := 0;
if CheckBox13.Checked then
   begin
        mi1 := 1;
        miesiac := miesiac + '1 ';
   end;
if CheckBox14.Checked then
   begin
        mi2 := 2;
        miesiac := miesiac + '2 ';
   end;
if CheckBox15.Checked then
   begin
        mi3 := 3;
        miesiac := miesiac + '3 ';
   end;
if CheckBox16.Checked then
   begin
        mi4 := 4;
        miesiac := miesiac + '4 ';
   end;
if CheckBox17.Checked then
   begin
        mi5 := 5;
        miesiac := miesiac + '5 ';
   end;
if CheckBox18.Checked then
   begin
        mi6 := 6;
        miesiac := miesiac + '6 ';
   end;
if CheckBox19.Checked then
   begin
        mi7 := 7;
        miesiac := miesiac + '7 ';
   end;
if CheckBox20.Checked then
   begin
        mi8 := 8;
        miesiac := miesiac + '8 ';
   end;
if CheckBox21.Checked then
   begin
        mi9 := 9;
        miesiac := miesiac + '9 ';
   end;
if CheckBox22.Checked then
   begin
        mi10 := 10;
        miesiac := miesiac + '10 ';
   end;
if CheckBox23.Checked then
   begin
        mi11 := 11;
        miesiac := miesiac + '11 ';
   end;
if CheckBox24.Checked then
   begin
        mi12 := 12;
        miesiac := miesiac + '12 ';
   end;

case RadioGroup1.ItemIndex of
     0: zawartosc := 0;
     1: zawartosc := 1;
     end;
miesiac := Trim(miesiac);
miesiac := StringReplace(miesiac,' ',',',[rfReplaceAll]);
rodzaj_dok := frTowary.rgTypDok.ItemIndex;
if FileExists(raporty) then
   begin
        dm.DrukujListeDokumentowG(rodzaj_dok,zawartosc,mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12);
        frTowary.frxReport3.LoadFromFile(raporty,False);
        frTowary.frxReport3.Variables['okres'] := '''' + ' miesi�ce ' + miesiac + '''';
        frTowary.frxReport3.PrepareReport(True);
        frTowary.frxReport3.ShowReport(True);
   end
else ShowMessage('Brak raportu ' + StringReplace(raporty,'/','\',[rfReplaceAll]));
end;

procedure TfrGadzetyDokLista.setRodzajDok(rodzaj: Integer);
begin
//rodzaj_dok := rodzaj;
end;

end.
