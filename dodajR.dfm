object frReczny: TfrReczny
  Left = 525
  Top = 451
  Width = 683
  Height = 305
  Caption = 'Wprowadzanie obrotu'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object sb: TStatusBar
    Left = 0
    Top = 247
    Width = 667
    Height = 19
    Panels = <>
  end
  object pc: TPageControlEx
    Left = 0
    Top = 0
    Width = 667
    Height = 247
    ActivePage = SzukajSklepu
    Align = alClient
    TabOrder = 1
    HideHeader = True
    object SzukajSklepu: TTabSheet
      Caption = 'SzukajSklepu'
      object GroupBox1: TGroupBox
        Left = 8
        Top = 22
        Width = 649
        Height = 209
        Caption = 'Szukaj sklep lub dodaj nowy sklep'
        TabOrder = 0
        object leNIP: TLabeledEdit
          Left = 8
          Top = 40
          Width = 121
          Height = 21
          EditLabel.Width = 80
          EditLabel.Height = 13
          EditLabel.Caption = 'Podaj NIP sklepu'
          TabOrder = 0
          OnChange = leNIPChange
        end
        object leNazwa: TLabeledEdit
          Left = 144
          Top = 40
          Width = 209
          Height = 21
          EditLabel.Width = 94
          EditLabel.Height = 13
          EditLabel.Caption = 'Podaj nazw'#281' sklepu'
          TabOrder = 1
          OnChange = leNazwaChange
        end
        object leImieN: TLabeledEdit
          Left = 8
          Top = 80
          Width = 121
          Height = 21
          EditLabel.Width = 111
          EditLabel.Height = 13
          EditLabel.Caption = 'Podaj imi'#281' lub nazwisko'
          TabOrder = 2
          OnChange = leImieNChange
        end
        object DBGrid1: TDBGrid
          Left = 8
          Top = 112
          Width = 617
          Height = 89
          DataSource = dm.dsKontrah
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NR_KONTRAH'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'RODZAJ'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'IMIE_NAZWISKO'
              Title.Caption = 'Imi'#281' i nazwisko'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAZWA'
              Title.Caption = 'Nazwa firmy'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NIP'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ULICA'
              Title.Caption = 'Ulica'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MIASTO'
              Title.Caption = 'Miasto'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'KOD_P'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_PRZYSTAPIENIA'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TELEFON'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'KONCESJA'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'METRAZ'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'KATEGORIA'
              Width = -1
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'UWAGI'
              Visible = False
            end>
        end
        object DBNavigator1: TDBNavigator
          Left = 144
          Top = 80
          Width = 208
          Height = 25
          DataSource = dm.dsKontrah
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          TabOrder = 4
        end
        object JvImgBtn1: TJvImgBtn
          Left = 520
          Top = 40
          Width = 105
          Height = 41
          Caption = 'Dalej'
          TabOrder = 5
          OnClick = JvImgBtn1Click
          Flat = True
          Images = frGlowny.JvImageList1
          ImageIndex = 1
        end
        object JvImgBtn2: TJvImgBtn
          Left = 408
          Top = 40
          Width = 107
          Height = 41
          Caption = 'Dodaj sklep'
          TabOrder = 6
          OnClick = JvImgBtn2Click
          Flat = True
          Images = frGlowny.JvImageList1
          ImageIndex = 2
        end
      end
    end
    object SzukajDostawcy: TTabSheet
      Caption = 'SzukajDostawcy'
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 16
        Top = 16
        Width = 641
        Height = 217
        Caption = 'Szukaj dostawcy lub dodaj nowego'
        TabOrder = 0
        object leNIP_D: TLabeledEdit
          Left = 8
          Top = 40
          Width = 121
          Height = 21
          EditLabel.Width = 96
          EditLabel.Height = 13
          EditLabel.Caption = 'Podaj NIP dostawcy'
          TabOrder = 0
          OnChange = leNIP_DChange
        end
        object leNazwaD: TLabeledEdit
          Left = 144
          Top = 40
          Width = 209
          Height = 21
          EditLabel.Width = 110
          EditLabel.Height = 13
          EditLabel.Caption = 'Podaj nazw'#281' dostawcy'
          TabOrder = 1
          OnChange = leNazwaDChange
        end
        object DBNavigator2: TDBNavigator
          Left = 8
          Top = 80
          Width = 208
          Height = 25
          DataSource = dsDostawca
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          TabOrder = 2
        end
        object DBGrid2: TDBGrid
          Left = 8
          Top = 112
          Width = 617
          Height = 89
          DataSource = dsDostawca
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NR_KONTRAH'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'RODZAJ'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'IMIE_NAZWISKO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'NAZWA'
              Title.Caption = 'Nazwa dostawcy'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NIP'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ULICA'
              Title.Caption = 'Ulica'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MIASTO'
              Title.Caption = 'Miasto'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'KOD_P'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_PRZYSTAPIENIA'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TELEFON'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'KONCESJA'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'METRAZ'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'KATEGORIA'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'UWAGI'
              Visible = False
            end>
        end
        object JvImgBtn3: TJvImgBtn
          Left = 392
          Top = 40
          Width = 123
          Height = 41
          Caption = 'Dodaj dostawc'#281
          TabOrder = 4
          OnClick = JvImgBtn3Click
          Flat = True
          Images = frGlowny.JvImageList1
          ImageIndex = 2
        end
        object JvImgBtn4: TJvImgBtn
          Left = 520
          Top = 40
          Width = 105
          Height = 41
          Caption = 'Dalej'
          TabOrder = 5
          OnClick = JvImgBtn1Click
          Flat = True
          Images = frGlowny.JvImageList1
          ImageIndex = 1
        end
      end
    end
    object DodajObrot: TTabSheet
      Caption = 'DodajObrot'
      ImageIndex = 2
      object GroupBox3: TGroupBox
        Left = 16
        Top = 8
        Width = 633
        Height = 225
        Caption = 'Wpisz obr'#243't sklepu i retro Euro i sklepu'
        TabOrder = 0
        object GroupBox4: TGroupBox
          Left = 16
          Top = 120
          Width = 385
          Height = 73
          Caption = 'Dostawca sieci'
          TabOrder = 0
          object DBText6: TDBText
            Left = 16
            Top = 44
            Width = 157
            Height = 13
            AutoSize = True
            DataField = 'ULICA'
            DataSource = dsDostawca
          end
          object DBText7: TDBText
            Left = 200
            Top = 44
            Width = 36
            Height = 13
            AutoSize = True
            DataField = 'MIASTO'
            DataSource = dsDostawca
          end
          object DBText5: TDBText
            Left = 16
            Top = 24
            Width = 94
            Height = 13
            AutoSize = True
            DataField = 'NAZWA'
            DataSource = dsDostawca
          end
        end
        object GroupBox5: TGroupBox
          Left = 16
          Top = 32
          Width = 385
          Height = 73
          Caption = 'Sklep sieci'
          TabOrder = 1
          object DBText4: TDBText
            Left = 288
            Top = 48
            Width = 53
            Height = 13
            AutoSize = True
            DataField = 'MIASTO'
            DataSource = dm.dsKontrah
          end
          object DBText3: TDBText
            Left = 152
            Top = 48
            Width = 83
            Height = 13
            AutoSize = True
            DataField = 'ULICA'
            DataSource = dm.dsKontrah
          end
          object DBText2: TDBText
            Left = 16
            Top = 48
            Width = 110
            Height = 13
            AutoSize = True
            DataField = 'IMIE_NAZWISKO'
            DataSource = dm.dsKontrah
          end
          object DBText1: TDBText
            Left = 16
            Top = 24
            Width = 284
            Height = 13
            AutoSize = True
            DataField = 'NAZWA'
            DataSource = dm.dsKontrah
          end
        end
        object GroupBox6: TGroupBox
          Left = 408
          Top = 32
          Width = 209
          Height = 177
          Caption = 'Obr'#243't sklepu i retro'
          TabOrder = 2
          object lEuro: TLabel
            Left = 16
            Top = 104
            Width = 65
            Height = 13
            Caption = 'Warto'#347#263' Euro'
          end
          object lSklep: TLabel
            Left = 112
            Top = 104
            Width = 67
            Height = 13
            Caption = 'Warto'#347#263' sklep'
          end
          object leObrot: TLabeledEdit
            Left = 16
            Top = 32
            Width = 73
            Height = 21
            EditLabel.Width = 61
            EditLabel.Height = 13
            EditLabel.Caption = 'Obr'#243't sklepu'
            TabOrder = 0
          end
          object leEuro: TLabeledEdit
            Left = 16
            Top = 72
            Width = 73
            Height = 21
            EditLabel.Width = 66
            EditLabel.Height = 13
            EditLabel.Caption = 'Retro Euro %'
            TabOrder = 1
            OnChange = leEuroChange
          end
          object leSklep: TLabeledEdit
            Left = 112
            Top = 72
            Width = 81
            Height = 21
            EditLabel.Width = 68
            EditLabel.Height = 13
            EditLabel.Caption = 'Retro sklep %'
            TabOrder = 2
            OnChange = leSklepChange
          end
          object JvImgBtn5: TJvImgBtn
            Left = 40
            Top = 128
            Width = 121
            Height = 41
            Caption = 'Zapisz do bazy'
            TabOrder = 3
            OnClick = JvImgBtn5Click
            Flat = True
            Images = frGlowny.JvImageList1
            ImageIndex = 3
          end
        end
      end
    end
  end
  object ibqDostawca: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT * FROM KONTRAH'
      'WHERE RODZAJ = '#39'D'#39)
    Left = 552
    Top = 8
  end
  object dsDostawca: TDataSource
    DataSet = ibqDostawca
    Left = 592
    Top = 8
  end
end
