object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 494
  Top = 139
  Height = 569
  Width = 910
  object JvDataSource1: TJvDataSource
    DataSet = JvADODataSet1
    Left = 144
    Top = 120
  end
  object JvADODataSet1: TJvADODataSet
    ConnectionString = 'FILE NAME=C:\Users\kompserw\Documents\Dantex\import.dsn'
    CursorType = ctStatic
    CommandText = 'select * from [lista$]'
    Parameters = <>
    DialogOptions.FormStyle = fsNormal
    Left = 56
    Top = 120
  end
  object serwer: TIBDatabase
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey')
    LoginPrompt = False
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 56
    Top = 72
  end
  object TabelaKontrah: TIBTable
    Database = serwer
    Transaction = ibTransakcje
    ForcedRefresh = True
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'NR_KONTRAH'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'NR_TRASY'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'NR_PRACOWNIK'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'RODZAJ'
        Attributes = [faRequired, faFixed]
        DataType = ftString
        Size = 5
      end
      item
        Name = 'IMIE_NAZWISKO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'NAZWA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 250
      end
      item
        Name = 'NIP'
        Attributes = [faRequired, faFixed]
        DataType = ftString
        Size = 13
      end
      item
        Name = 'ULICA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MIASTO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'KOD_P'
        Attributes = [faRequired]
        DataType = ftString
        Size = 8
      end
      item
        Name = 'DATA_PRZYSTAPIENIA'
        DataType = ftDate
      end
      item
        Name = 'TELEFON'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'KONCESJA'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'METRAZ'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'KATEGORIA'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'AKTYWNY'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'UWAGI'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'TYDZIEN_1'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TYDZIEN_2'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TYDZIEN_3'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TYDZIEN_4'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TYDZIEN_5'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'RDB$PRIMARY4'
        Fields = 'NR_KONTRAH'
        Options = [ixPrimary, ixUnique]
      end>
    IndexFieldNames = 'NR_KONTRAH'
    StoreDefs = True
    TableName = 'KONTRAH'
    Left = 144
    Top = 72
  end
  object ibTransakcje: TIBTransaction
    Active = False
    DefaultDatabase = serwer
    AutoStopAction = saNone
    Left = 144
    Top = 24
  end
  object dsKontrah: TDataSource
    DataSet = ibqKontrahenci
    Left = 432
    Top = 72
  end
  object ibqSQL: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 56
    Top = 24
  end
  object PlikCSV: TJvCsvDataSet
    AutoCalcFields = False
    CsvFieldDef = '7590006383;12300'
    HasHeaderRow = False
    ValidateHeaderRow = False
    Separator = ';'
    AutoBackupCount = 0
    StoreDefs = True
    Left = 16
    Top = 192
  end
  object dsPlikCSV: TJvDataSource
    DataSet = PlikCSV
    Left = 64
    Top = 192
  end
  object dsDostawca: TDataSource
    DataSet = ibqDostawca
    Left = 288
    Top = 264
  end
  object dsSzukajObroty: TJvDataSource
    DataSet = ibqSQL
    Left = 216
    Top = 24
  end
  object ibqProwizje: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT *  FROM PROWIZJE'
      'WHERE NR_KONTRAH = :A')
    Left = 144
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'A'
        ParamType = ptUnknown
      end>
  end
  object dsProwizje: TDataSource
    DataSet = ibqProwizje
    Left = 208
    Top = 312
  end
  object ibqZapisz: TIBQuery
    Database = serwer
    Transaction = ibtZapisz
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT *  FROM PROWIZJE'
      'WHERE NR_KONTRAH = :A')
    Left = 56
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'A'
        ParamType = ptUnknown
      end>
  end
  object dsPokaszKontrah: TJvDataSource
    DataSet = ibqSQL
    Left = 280
    Top = 24
  end
  object ibqZapisImportExcel: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 240
    Top = 120
  end
  object ibqKontrahenci: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      ''
      '')
    Left = 240
    Top = 72
  end
  object ibqProwizjeZ: TIBQuery
    Database = serwer
    Transaction = ibtProwizjeZ
    BufferChunks = 1000
    CachedUpdates = False
    Left = 56
    Top = 264
  end
  object ibqDostawca: TIBQuery
    Database = serwer
    Transaction = ibtDostawca
    BufferChunks = 1000
    CachedUpdates = False
    Left = 144
    Top = 264
  end
  object ibtProwizjeZ: TIBTransaction
    Active = False
    DefaultDatabase = serwer
    AutoStopAction = saNone
    Left = 56
    Top = 328
  end
  object ibtZapisz: TIBTransaction
    Active = False
    DefaultDatabase = serwer
    AutoStopAction = saNone
    Left = 128
    Top = 384
  end
  object ibqKartaK: TIBQuery
    Database = serwer
    Transaction = ibtKartaK
    BufferChunks = 1000
    CachedUpdates = False
    Left = 56
    Top = 440
  end
  object dsKartaK: TDataSource
    DataSet = ibqKartaK
    Left = 176
    Top = 440
  end
  object ibtDostawca: TIBTransaction
    Active = False
    DefaultDatabase = serwer
    AutoStopAction = saNone
    Left = 216
    Top = 264
  end
  object ibtKartaK: TIBTransaction
    Active = False
    DefaultDatabase = serwer
    AutoStopAction = saNone
    Left = 120
    Top = 440
  end
  object ibqCSV2Firebird: TIBQuery
    Database = serwer
    Transaction = trCSV2Firebird
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT NIP, SUM(OBROT) FROM IMPORT'
      'WHERE DATA_CZAS BETWEEN :A AND :B'
      'GROUP BY NIP')
    Left = 152
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'A'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'B'
        ParamType = ptUnknown
      end>
  end
  object trCSV2Firebird: TIBTransaction
    Active = False
    DefaultDatabase = serwer
    AutoStopAction = saNone
    Left = 232
    Top = 192
  end
  object dsCSV2Firebird: TDataSource
    DataSet = ibqCSV2Firebird
    Left = 304
    Top = 192
  end
  object ibqPoliczImport: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 328
    Top = 448
  end
  object ibqKasujTabele: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 328
    Top = 400
  end
  object ibqFormKontrah: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 424
    Top = 400
  end
  object Szukaj: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 488
    Top = 72
  end
  object dsFormKontrah: TDataSource
    DataSet = ibqFormKontrah
    Left = 496
    Top = 400
  end
  object ibqSQL1: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 592
    Top = 80
  end
  object dsSQL1: TDataSource
    DataSet = ibqSQL1
    Left = 648
    Top = 80
  end
  object ibqPracownik: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 496
    Top = 136
  end
  object ibqTrasy: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 496
    Top = 200
  end
  object ibqTowary: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 496
    Top = 264
  end
  object dsTowary: TDataSource
    DataSet = ibqTowary
    Left = 568
    Top = 264
  end
  object ibqGadzetyN: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 640
    Top = 400
  end
  object ibqGadzetyP: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 640
    Top = 464
  end
  object dsGadzetyN: TDataSource
    DataSet = ibqGadzetyN
    Left = 728
    Top = 400
  end
  object dsGadzetyP: TDataSource
    DataSet = ibqGadzetyP
    Left = 728
    Top = 464
  end
  object ibqSQL3: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 736
    Top = 80
  end
  object dsSQL3: TDataSource
    DataSet = ibqSQL3
    Left = 792
    Top = 80
  end
  object ibqAktywniD: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 648
    Top = 160
  end
  object dsAktywniD: TDataSource
    DataSet = ibqAktywniD
    Left = 720
    Top = 160
  end
  object ibqAktywniD_P: TIBQuery
    Database = serwer
    Transaction = ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 656
    Top = 216
  end
  object dsAktywniD_P: TDataSource
    DataSet = ibqAktywniD_P
    Left = 728
    Top = 216
  end
end
