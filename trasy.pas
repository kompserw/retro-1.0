unit trasy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ComCtrls, ImgList, PageControlEx, ToolWin,
  StdCtrls, JvExStdCtrls, JvButton, JvCtrls, ExtCtrls, frxClass, frxDBSet;

type
  TfrTrasy = class(TForm)
    ToolBar1: TToolBar;
    StatusBar1: TStatusBar;
    PageControlEx1: TPageControlEx;
    ImageList1: TImageList;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    siatka: TDBGrid;
    DataSource1: TDataSource;
    GroupBox1: TGroupBox;
    ComboBox1: TComboBox;
    Label1: TLabel;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    Label2: TLabel;
    Memo1: TMemo;
    JvImgBtn1: TJvImgBtn;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ComboBox2: TComboBox;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    Memo2: TMemo;
    JvImgBtn2: TJvImgBtn;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    ComboBox3: TComboBox;
    LabeledEdit5: TLabeledEdit;
    LabeledEdit6: TLabeledEdit;
    Memo3: TMemo;
    JvImgBtn3: TJvImgBtn;
    TabSheet5: TTabSheet;
    ListBox1: TListBox;
    ListBox2: TListBox;
    Label7: TLabel;
    Label8: TLabel;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    frxReport1: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure UstawSiatke;
    procedure FormCreate(Sender: TObject);
    procedure siatkaDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure siatkaCellClick(Column: TColumn);
    procedure ToolButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frTrasy: TfrTrasy;
  nr_trasy: array of Integer;
implementation
uses baza;
{$R *.dfm}

procedure TfrTrasy.ToolButton1Click(Sender: TObject);
begin
ToolButton2.Enabled := True;ToolButton3.Enabled := True;ToolButton4.Enabled := True;
ToolButton6.Enabled := False;
dm.PokazTrase(0,0,'');
frTrasy.UstawSiatke;
frTrasy.Caption := 'Trasy - lista dost�pnych tras';
PageControlEx1.ActivePageIndex := 0;
end;

procedure TfrTrasy.ToolButton2Click(Sender: TObject);
begin
frTrasy.Caption := 'Trasy - dodaj tras�';
PageControlEx1.ActivePageIndex := 1;
ComboBox1.Text := '';
LabeledEdit1.Text := '';
LabeledEdit2.Text := '';
Memo1.Text := '';
ToolButton6.Enabled := False;
end;

procedure TfrTrasy.ToolButton3Click(Sender: TObject);
begin
frTrasy.Caption := 'Trasy - modyfikuj tras�';
PageControlEx1.ActivePageIndex := 2;
ComboBox2.Text := dm.NazwiskoImieKtoJezdzi(dm.ibqSQL1.FieldValues['NR_KTO_JEZDZI']);
LabeledEdit4.Text := dm.ibqSQL1.FieldValues['DLUGOSC_TRASY'];
LabeledEdit3.Text := dm.ibqSQL1.FieldValues['NAZWA_TRASY'];
Memo2.Text := dm.ibqSQL1.FieldValues['OPIS'];
ToolButton6.Enabled := False;
end;

procedure TfrTrasy.ToolButton4Click(Sender: TObject);
begin
frTrasy.Caption := 'Trasy - usu� tras�';
PageControlEx1.ActivePageIndex := 3;
ComboBox3.Text := dm.NazwiskoImieKtoJezdzi(dm.ibqSQL1.FieldValues['NR_KTO_JEZDZI']);
LabeledEdit6.Text := dm.ibqSQL1.FieldValues['DLUGOSC_TRASY'];
LabeledEdit5.Text := dm.ibqSQL1.FieldValues['NAZWA_TRASY'];
Memo3.Text := dm.ibqSQL1.FieldValues['OPIS'];
ToolButton6.Enabled := False;
end;

procedure TfrTrasy.ToolButton5Click(Sender: TObject);
var
   x: Integer;
begin
ToolButton2.Enabled := False;ToolButton3.Enabled := False;ToolButton4.Enabled := False;
PageControlEx1.ActivePageIndex := 4;
x := 0;
dm.PokazTrase(0,0,'');
SetLength(nr_trasy,dm.ibqSQL1.RecordCount);
ListBox1.Clear;
Label8.Caption := '';
Label8.Font.Color := clRed;
while not dm.ibqSQL1.Eof do
      begin
           ListBox1.Items.Add(dm.ibqSQL1.FieldValues['NAZWA_TRASY']);
           nr_trasy[x] := dm.ibqSQL1.FieldValues['NR_TRASY'];
           x := x +1;
           dm.ibqSQL1.Next;
      end;
end;

procedure TfrTrasy.ListBox1Click(Sender: TObject);
begin
dm.PokazSklepy(1,nr_trasy[ListBox1.ItemIndex]);
Label8.Caption := dm.PracownikF(nr_trasy[ListBox1.ItemIndex]);
ListBox2.Clear;
ToolButton6.Enabled := True;
while not dm.ibqSQL1.Eof do
      begin
           ListBox2.Items.Add(dm.ibqSQL1.FieldValues['IMIE_NAZWISKO']);
           dm.ibqSQL1.Next;
      end;
end;

procedure TfrTrasy.JvImgBtn1Click(Sender: TObject);                              //dodawanie nowej trasy
var
   nr_kto_jezdzi: Integer;
begin
nr_kto_jezdzi := dm.NrPrzedstawiciela(ComboBox1.Text);
dm.Trasy(0,1,nr_kto_jezdzi,LabeledEdit1.Text,LabeledEdit2.Text,Memo1.Text);
dm.PokazTrase(0,0,'');
UstawSiatke;
ToolButton3.Enabled := False;ToolButton4.Enabled := False;
PageControlEx1.ActivePageIndex := 0;
end;

procedure TfrTrasy.JvImgBtn2Click(Sender: TObject);                              //modyfikacja trasy
var
   nr_trasy,nr_kto_jezdzi: Integer;
begin
nr_trasy := dm.ibqSQL1.FieldValues['NR_TRASY'];
nr_kto_jezdzi := dm.ibqPracownik.FieldValues['NR_PRACOWNIK'];
dm.Trasy(nr_trasy,2,nr_kto_jezdzi,LabeledEdit3.Text,LabeledEdit4.Text,Memo2.Text);
dm.PokazTrase(0,0,'');
UstawSiatke;
ToolButton3.Enabled := False;ToolButton4.Enabled := False;
PageControlEx1.ActivePageIndex := 0;
end;

procedure TfrTrasy.JvImgBtn3Click(Sender: TObject);                              //kasowanie trasy
var
   nr_trasy: Integer;
begin
nr_trasy := dm.ibqSQL1.FieldValues['NR_TRASY'];
dm.Trasy(nr_trasy,3,0,'','','');
dm.PokazTrase(0,0,'');
UstawSiatke;
ToolButton3.Enabled := False;ToolButton4.Enabled := False;
PageControlEx1.ActivePageIndex := 0;
end;

procedure TfrTrasy.UstawSiatke;
begin
with siatka do
     begin
          Columns[0].Visible := False;
          Columns[1].Visible := False;
          Columns[2].Width := 200;
          Columns[3].Width := 80;
          Columns[4].Width := 250;
          Columns[2].Title.Caption := 'Nazwa trasy';
          Columns[3].Title.Caption := 'D�ugo�� trasy';
          Columns[4].Title.Caption := 'Opis trasy';
     end;
end;

procedure TfrTrasy.FormCreate(Sender: TObject);
begin
//UstawSiatke;
end;

procedure TfrTrasy.siatkaDrawDataCell(Sender: TObject; const Rect: TRect;
  Field: TField; State: TGridDrawState);
begin
if Field.FieldName = 'DLUGOSC_TRASY'  then
    TFloatField(Field).Alignment := taRightJustify;
end;

procedure TfrTrasy.siatkaCellClick(Column: TColumn);
begin
ToolButton2.Enabled := True;ToolButton3.Enabled := True;ToolButton4.Enabled := True;
end;

procedure TfrTrasy.ToolButton6Click(Sender: TObject);
var
   nr: Integer;
begin
nr := dm.ibqSQL1.FieldValues['NR_PRACOWNIK'];
dm.Trasowki(nr);
frxReport1.ShowReport(True);
ToolButton6.Enabled := False;
end;

end.
