unit update;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP;

type
  TfrUpdate = class(TForm)
    IdHTTP1: TIdHTTP;
    Label1: TLabel;
    lblURL: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
     Version = '1.0';
     URL = 'http://komputeryserwis.pl/retro/wersja.ini';
     AppName = 'retro';

var
  frUpdate: TfrUpdate;

implementation

{$R *.dfm}

procedure TfrUpdate.Button1Click(Sender: TObject);
var
   NowaWersja: String;
begin
     IdHTTP1.Host := 'http://komputeryserwis.pl/';
     NowaWersja := IdHTTP1.Get(URL);

     if NowaWersja > Version then
        begin
             ShowMessage('Jest nowsza wersja programu, czy chcesz j� pobra� ?');
             Label1.Visible := True;
             lblURL.Caption := 'http://komputeryserwis.pl/retro/retro.exe';
             lblURL.Visible := True;
        end;
end;

procedure TfrUpdate.FormCreate(Sender: TObject);
begin
     Caption := Caption + ' ' + AppName + '-' + Version;
end;

end.
