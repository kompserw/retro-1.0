unit obrotyK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, JvExStdCtrls, JvGroupBox, ExtCtrls,
  JvExExtCtrls, JvExtComponent, JvPanel, DB, IBCustomDataSet, IBQuery,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls,
  JvDBGridFooter, JvCombobox, JvButton, JvCtrls, frxClass, frxDBSet,
  frxPreview;

type
  TfrObrotyK = class(TForm)
    JvPanel1: TJvPanel;
    JvPanel2: TJvPanel;
    JvGroupBox1: TJvGroupBox;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBText9: TDBText;
    DBText10: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    grObroty: TJvDBUltimGrid;
    ibqObroty: TIBQuery;
    dsObroty: TDataSource;
    ibqSuma: TIBQuery;
    GroupBox1: TGroupBox;
    lObroty: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    lProwizja: TLabel;
    cbMiesiac: TJvComboBox;
    cbKwartal: TJvComboBox;
    leNazwa: TLabeledEdit;
    Label14: TLabel;
    Label15: TLabel;
    JvImgBtn1: TJvImgBtn;
    frxReport1: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    frxDBDataset2: TfrxDBDataset;
    frxPreview1: TfrxPreview;
    procedure cbMiesiacChange(Sender: TObject);
    procedure cbKwartalChange(Sender: TObject);
    procedure leNazwaChange(Sender: TObject);
    procedure grObrotyDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure JvImgBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frObrotyK: TfrObrotyK;

implementation
uses glowny,baza;
{$R *.dfm}

procedure TfrObrotyK.cbMiesiacChange(Sender: TObject);
begin
//kwartal := cKwartal.Text;
with ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA_D AS DOSTAWCA,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.OKRES_M,A.OKRES_Q FROM OBROTY A');
          if cbMiesiac.Text = 'wszystkie' then SQL.Add('WHERE NR_KONTRAH_SKLEP = :A')
          else SQL.Add('WHERE A.NR_KONTRAH_SKLEP = :A AND A.OKRES_M = :B');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
                    if cbMiesiac.Text <> 'wszystkie' then ParamByName('B').AsString := cbMiesiac.Text;
          Prepare;
          Open;
     end;
with ibqSuma do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(OBROTY) AS OBROTY,SUM(PROWIZJA_KWOTA) AS PROWIZJA FROM OBROTY');
          if cbMiesiac.Text = 'wszystkie' then SQL.Add('WHERE NR_KONTRAH_SKLEP = :A')
          else SQL.Add('WHERE NR_KONTRAH_SKLEP = :A AND OKRES_M = :B');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
                    if cbMiesiac.Text <> 'wszystkie' then ParamByName('B').AsString := cbMiesiac.Text;
          Prepare;
          Open;
          if FieldByName('OBROTY').IsNull then
             begin
                  frObrotyK.lObroty.Caption := 'brak obrot�w';
                  frObrotyK.lProwizja.Caption := 'brak obrot�w';
             end
          else
              begin
                   frObrotyK.lObroty.Caption := FloatToStrF(FieldValues['OBROTY'], ffCurrency, 7, 2);
                   frObrotyK.lProwizja.Caption := FloatToStrF(FieldValues['PROWIZJA'], ffCurrency, 7, 2);
              end;
     end;
with grObroty do
     begin
          Columns.Items[0].FieldName := 'DOSTAWCA';
          Columns.Items[0].Title.Caption := 'Dostawca';
          Columns.Items[0].Width := 200;
          Columns.Items[1].FieldName := 'OBROTY';
          Columns.Items[1].Title.Caption := 'Obr�t sklepu';
          Columns.Items[1].Width := 80;
          Columns.Items[2].FieldName := 'PROWIZJA_PROC';
          Columns.Items[2].Title.Caption := 'Procent prowizji';
          Columns.Items[2].Width := 80;
          Columns.Items[3].FieldName := 'PROWIZJA_KWOTA';
          Columns.Items[3].Title.Caption := 'Kwota prowizji';
          Columns.Items[3].Width := 80;
          Columns.Items[4].FieldName := 'OKRES_M';
          Columns.Items[4].Title.Caption := 'Okres miesi�ca';
          Columns.Items[4].Width := 80;
          Columns.Items[5].FieldName := 'OKRES_Q';
          Columns.Items[5].Title.Caption := 'Okres kwarta�';
          Columns.Items[5].Width := 80;
     end;

end;

procedure TfrObrotyK.cbKwartalChange(Sender: TObject);
begin
//kwartal := cbKwartal.Text;
with ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA_D AS DOSTAWCA,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.OKRES_M,A.OKRES_Q FROM OBROTY A');
          if cbKwartal.Text = 'wszystkie' then SQL.Add('WHERE NR_KONTRAH_SKLEP = :A')
          else SQL.Add('WHERE A.NR_KONTRAH_SKLEP = :A AND A.OKRES_Q = :B');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
                    if cbKwartal.Text <> 'wszystkie' then ParamByName('B').AsString := cbKwartal.Text;
          Prepare;
          Open;
     end;
with ibqSuma do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(OBROTY) AS OBROTY,SUM(PROWIZJA_KWOTA) AS PROWIZJA FROM OBROTY');
          if cbKwartal.Text = 'wszystkie' then SQL.Add('WHERE NR_KONTRAH_SKLEP = :A')
          else SQL.Add('WHERE NR_KONTRAH_SKLEP = :A AND OKRES_Q = :B');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
                    if cbKwartal.Text <> 'wszystkie' then ParamByName('B').AsString := cbKwartal.Text;
          Prepare;
          Open;
          if FieldByName('OBROTY').IsNull then
             begin
                  frObrotyK.lObroty.Caption := 'brak obrot�w';
                  frObrotyK.lProwizja.Caption := 'brak obrot�w';
             end
          else
              begin
                   frObrotyK.lObroty.Caption := FloatToStrF(FieldValues['OBROTY'], ffCurrency, 7, 2);
                   frObrotyK.lProwizja.Caption := FloatToStrF(FieldValues['PROWIZJA'], ffCurrency, 7, 2);
              end;
     end;
with grObroty do
     begin
          Columns.Items[0].FieldName := 'DOSTAWCA';
          Columns.Items[0].Title.Caption := 'Dostawca';
          Columns.Items[0].Width := 200;
          Columns.Items[1].FieldName := 'OBROTY';
          Columns.Items[1].Title.Caption := 'Obr�t sklepu';
          Columns.Items[1].Width := 80;
          Columns.Items[2].FieldName := 'PROWIZJA_PROC';
          Columns.Items[2].Title.Caption := 'Procent prowizji';
          Columns.Items[2].Width := 80;
          Columns.Items[3].FieldName := 'PROWIZJA_KWOTA';
          Columns.Items[3].Title.Caption := 'Kwota prowizji';
          Columns.Items[3].Width := 80;
          Columns.Items[4].FieldName := 'OKRES_M';
          Columns.Items[4].Title.Caption := 'Okres miesi�ca';
          Columns.Items[4].Width := 80;
          Columns.Items[5].FieldName := 'OKRES_Q';
          Columns.Items[5].Title.Caption := 'Okres kwarta�';
          Columns.Items[5].Width := 80;
     end;

end;

procedure TfrObrotyK.leNazwaChange(Sender: TObject);
begin
with ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NAZWA_D AS DOSTAWCA,OBROTY,PROWIZJA_PROC,PROWIZJA_KWOTA,OKRES_M,OKRES_Q FROM OBROTY A');
          if cbKwartal.Text = 'wszystkie' then SQL.Add('WHERE NR_KONTRAH_SKLEP = :A  AND NAZWA_D LIKE :C')
          else SQL.Add('WHERE NR_KONTRAH_SKLEP = :A AND OKRES_Q = :B AND NAZWA LIKE :C');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
                    if cbKwartal.Text <> 'wszystkie' then ParamByName('B').AsString := cbKwartal.Text;
                    ParamByName('C').AsString := '%' + UpperCase(leNazwa.Text) + '%';
          Prepare;
          Open;
     end;
with ibqSuma do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(OBROTY) AS OBROTY,SUM(PROWIZJA_KWOTA) AS PROWIZJA FROM OBROTY');
          if cbKwartal.Text = 'wszystkie' then SQL.Add('WHERE NR_KONTRAH_SKLEP = :A  AND NAZWA_D LIKE :C')
          else SQL.Add('WHERE NR_KONTRAH_SKLEP = :A AND OKRES_Q = :B  AND NAZWA LIKE :C');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
                    if cbKwartal.Text <> 'wszystkie' then ParamByName('B').AsString := cbKwartal.Text;
                    ParamByName('C').AsString := '%' + UpperCase(leNazwa.Text) + '%';
          Prepare;
          Open;
          if FieldByName('OBROTY').IsNull then
             begin
                  frObrotyK.lObroty.Caption := 'brak obrot�w';
                  frObrotyK.lProwizja.Caption := 'brak obrot�w';
             end
          else
              begin
                   frObrotyK.lObroty.Caption := FloatToStrF(FieldValues['OBROTY'], ffCurrency, 7, 2);
                   frObrotyK.lProwizja.Caption := FloatToStrF(FieldValues['PROWIZJA'], ffCurrency, 7, 2);
              end;
     end;
with grObroty do
     begin
          Columns.Items[0].FieldName := 'DOSTAWCA';
          Columns.Items[0].Title.Caption := 'Dostawca';
          Columns.Items[0].Width := 200;
          Columns.Items[1].FieldName := 'OBROTY';
          Columns.Items[1].Title.Caption := 'Obr�t sklepu';
          Columns.Items[1].Width := 80;
          Columns.Items[2].FieldName := 'PROWIZJA_PROC';
          Columns.Items[2].Title.Caption := 'Procent prowizji';
          Columns.Items[2].Width := 80;
          Columns.Items[3].FieldName := 'PROWIZJA_KWOTA';
          Columns.Items[3].Title.Caption := 'Kwota prowizji';
          Columns.Items[3].Width := 80;
          Columns.Items[4].FieldName := 'OKRES_M';
          Columns.Items[4].Title.Caption := 'Okres miesi�ca';
          Columns.Items[4].Width := 80;
          Columns.Items[5].FieldName := 'OKRES_Q';
          Columns.Items[5].Title.Caption := 'Okres kwarta�';
          Columns.Items[5].Width := 80;
     end;

end;

procedure TfrObrotyK.grObrotyDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  if Field.FieldName = 'OBROTY'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'PROWIZJA_KWOTA'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'PROWIZJA_PROC'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 %';
end;

procedure TfrObrotyK.JvImgBtn1Click(Sender: TObject);
begin
frxReport1.PrepareReport();
frxReport1.ShowReport;
end;

end.
