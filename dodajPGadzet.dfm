object frDodajPGadzet: TfrDodajPGadzet
  Left = 759
  Top = 369
  Width = 336
  Height = 246
  Caption = 'Dodawanie pozycji'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 112
    Width = 91
    Height = 13
    Caption = 'Ilo'#347#263' na magzaynie'
  end
  object Label2: TLabel
    Left = 16
    Top = 144
    Width = 42
    Height = 16
    Caption = 'Label2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LabeledEdit1: TLabeledEdit
    Left = 16
    Top = 24
    Width = 289
    Height = 21
    EditLabel.Width = 74
    EditLabel.Height = 13
    EditLabel.Caption = 'Nazwa gad'#380'etu'
    TabOrder = 0
  end
  object LabeledEdit2: TLabeledEdit
    Left = 16
    Top = 72
    Width = 81
    Height = 21
    EditLabel.Width = 22
    EditLabel.Height = 13
    EditLabel.Caption = 'Ilo'#347#263
    TabOrder = 1
  end
  object LabeledEdit3: TLabeledEdit
    Left = 128
    Top = 72
    Width = 177
    Height = 21
    EditLabel.Width = 74
    EditLabel.Height = 13
    EditLabel.Caption = 'Cena z karoteki'
    TabOrder = 2
  end
  object LabeledEdit4: TLabeledEdit
    Left = 128
    Top = 120
    Width = 177
    Height = 21
    EditLabel.Width = 90
    EditLabel.Height = 13
    EditLabel.Caption = 'Cena na dokument'
    TabOrder = 3
  end
  object JvImgBtn1: TJvImgBtn
    Left = 128
    Top = 152
    Width = 177
    Height = 41
    Caption = 'Dodaj pozycj'#281' do dokumentu'
    TabOrder = 4
    OnClick = JvImgBtn1Click
    Flat = True
    Images = frGlowny.img
    ImageIndex = 18
  end
end
