unit ListaImp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, JvExComCtrls, JvDateTimePicker, DB, IBCustomDataSet,
  IBQuery, JvDataSource, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, ExtCtrls, DBCtrls, JvDBControls, ToolWin, JvToolBar,
  PageControlEx, StdCtrls, JvExStdCtrls, JvButton, JvCtrls, JvListBox,
  JvExExtCtrls, JvNetscapeSplitter, JvExtComponent, JvPanel, JvgStringGrid,
  JvExGrids, JvStringGrid;

type
  TfrListaImp = class(TForm)
    pc: TPageControlEx;
    tsObrotyLista: TTabSheet;
    TabSheet2: TTabSheet;
    JvToolBar1: TJvToolBar;
    JvDBNavigator1: TJvDBNavigator;
    ToolButton1: TToolButton;
    JvToolBar2: TJvToolBar;
    JvDBUltimGrid2: TJvDBUltimGrid;
    JvDBNavigator2: TJvDBNavigator;
    JvImgBtn1: TJvImgBtn;
    sb: TStatusBar;
    JvImgBtn2: TJvImgBtn;
    JvPanel1: TJvPanel;
    DBGrid1: TDBGrid;
    procedure JvImgBtn1Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure JvDBUltimGrid1DblClick(Sender: TObject);
    procedure sGridCellClick(Column: TColumn);
    procedure ToolButton3Click(Sender: TObject);
    procedure UstawSiatke(aGrid: TDBGrid);
  private
    { Private declarations }
  public
    NR_DOSTAWCA,NUMER_MIESIAC,ROK,NR_PROWIZJI: Integer;
    NAZWA_D: String;
    DATA_ZAPISU: TDateTime;
  end;

var
  frListaImp: TfrListaImp;

implementation
uses baza,glowny;
{$R *.dfm}

procedure TfrListaImp.JvImgBtn1Click(Sender: TObject);
begin
pc.ActivePageIndex := 0;
sb.Panels[1].Text := '';
sb.Panels[2].Text := '';
sb.Panels[3].Text := '';
sb.Panels[4].Text := '';
end;

procedure TfrListaImp.JvImgBtn2Click(Sender: TObject);
var
   t1,t2,t3,t4: String;
begin
with dm.ibqSQL1 do
     begin
          NR_DOSTAWCA := FieldValues['NR_KONTRACH_DOST'];
          NUMER_MIESIAC := FieldValues['NUMER_MIESIAC'];
          DATA_ZAPISU := FieldValues['DATA_ZAPISU'];
          NR_PROWIZJI := FieldValues['NR_PROWIZJE'];
          ROK := FieldValues['ROK'];
          NAZWA_D := FieldValues['NAZWA_D'];
     end;
if MessageBox(Handle,'Czy chcesz usun�� wybrany import z bazy ?', 'Kasowanie obrot�w', MB_YESNO + MB_ICONQUESTION) = IdYes then
  begin
       if not dm.ibtZapisz.InTransaction then dm.ibtZapisz.StartTransaction;
          with dm.ibqSQL1 do
               begin
                    Close;
                    SQL.Clear;
                    SQL.Add('DELETE FROM OBROTY');
                    SQL.Add('WHERE NAZWA_D = :A AND DATA_ZAPISU = :B AND NR_PROWIZJE = :C AND NUMER_MIESIAC = :D AND ROK = :E');
                    UnPrepare;
                       ParamByName('A').AsString := NAZWA_D;
                       ParamByName('B').AsDate := DATA_ZAPISU;
                       ParamByName('C').AsInteger := NR_PROWIZJI;
                       ParamByName('D').AsInteger := NUMER_MIESIAC;
                       ParamByName('E').AsInteger := ROK;
                    Prepare;
                    Open;
               end;
          if dm.ibtZapisz.InTransaction then dm.ibtZapisz.Commit;
  end;
t1 := 'SELECT A.NAZWA_D,A.NR_PROWIZJE,A.DATA_ZAPISU,B.OPIS,SUM(A.OBROTY) AS OBROTY,NR_KONTRACH_DOST,NUMER_MIESIAC,ROK FROM OBROTY A,PROWIZJE B';
t2 := 'WHERE A.NR_PROWIZJE = B.NR_PROWIZJE';
t3 := 'GROUP BY A.DATA_ZAPISU,A.NAZWA_D,A.NR_PROWIZJE,B.OPIS,NR_KONTRACH_DOST,NUMER_MIESIAC,ROK';
t4 := 'ORDER BY A.DATA_ZAPISU DESC';
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add(t1);SQL.Add(t2);SQL.Add(t3);SQL.Add(t4);
          Open;
     end;
UstawSiatke(DBGrid1);
end;

procedure TfrListaImp.JvDBUltimGrid1DblClick(Sender: TObject);
begin
pc.ActivePageIndex := 1;
with dm.ibqSQL1 do
     begin
          Close;
          UnPrepare;
               //ParamByName('A').AsDate := cbData.Date;
               //ParamByName('B').AsString := ibqObrotImp.FieldValues['NAZWA_D'];
          Prepare;
          Open;
     end;

{with ibqSuma do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT sum(OBROTY) AS OBROTY from OBROTY,KONTRAH');
          SQL.Add('WHERE NR_KONTRAH=NR_KONTRAH_SKLEP AND DATA_ZAPISU = :A AND NAZWA_D = :B');
          UnPrepare;
               ParamByName('A').AsDate := cbData.Date;
               ParamByName('B').AsString := ibqObrotImp.FieldValues['NAZWA_D'];
          Prepare;
          Open;
          sb.Panels[1].Text := FloatToStrF(FieldValues['OBROTY'], ffCurrency, 7, 2);
          sb.Panels[1].Alignment := taRightJustify;
          sb.Panels[2].Text := FloatToStrF(FieldValues['PROWIZJA_SKLEPY'], ffCurrency, 7, 2);
          sb.Panels[2].Alignment := taRightJustify;
          sb.Panels[3].Text := FloatToStrF(FieldValues['PROWIZJE_EURO'], ffCurrency, 7, 2);
          sb.Panels[3].Alignment := taRightJustify;
          sb.Panels[4].Text := FloatToStrF(FieldValues['ZYSK'], ffCurrency, 7, 2);
          sb.Panels[4].Alignment := taRightJustify;
     end;  }
end;

procedure TfrListaImp.sGridCellClick(Column: TColumn);
var
   nazwa_dost: String;
   data_zap,czas_zap: TDateTime;
   nr_prowizji: Integer;
begin
nazwa_dost := dm.ibqSQL1.FieldValues['NAZWA_D'];
data_zap := dm.ibqSQL1.FieldValues['DATA_ZAPISU'];
//czas_zap := ibqObrotyLista.FieldValues['CZAS_ZAPISU'];
nr_prowizji := dm.ibqSQL1.FieldValues['NR_PROWIZJE'];
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,sum(A.OBROTY) AS OBROTY,sum(A.PROWIZJA_KWOTA) AS PROWIZJA_SKLEPY,sum(A.PROW_E_KWOTA) AS PROWIZJE_EURO,sum(A.ZYSK) AS ZYSK from OBROTY A,KONTRAH B');
          SQL.Add('WHERE DATA_ZAPISU = :A,CZAS_ZAPISU = :B,NAZWA_D = :C,NR_PROWIZJE= :D');
          SQL.Add('GROUP BY B.NAZWA');
          SQL.Add('ORDER BY NAZWA');
          UnPrepare;
               ParamByName('A').AsDate := data_zap;
               //ParamByName('B').AsDate := czas_zap;
               ParamByName('C').AsString := nazwa_dost;
               ParamByName('D').AsInteger := nr_prowizji;
          Prepare;
          Open;
     end;
end;

procedure TfrListaImp.ToolButton3Click(Sender: TObject);
begin
//ibqObrotyLista.Open;
end;

procedure TfrListaImp.UstawSiatke(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA_D';
          lColumn.Title.Caption := 'Nazwa dostawcy';
          lColumn.Width := 250;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'DATA_ZAPISU';
          lColumn.Title.Caption := 'Data zapisu';
          lColumn.Width := 80;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'OPIS';
          lColumn.Title.Caption := 'Opis retra';
          lColumn.Width := 200;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'OBROTY';
          lColumn.Title.Caption := 'Obr�t';
          lColumn.Width := 80;
     end;
end;

end.
