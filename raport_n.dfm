object frRaport: TfrRaport
  Left = 407
  Top = 215
  Width = 1142
  Height = 656
  Caption = 'Raport'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object IBTable1: TIBTable
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'NR_KONTRAH'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'RODZAJ'
        Attributes = [faRequired, faFixed]
        DataType = ftString
        Size = 5
      end
      item
        Name = 'IMIE_NAZWISKO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'NAZWA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 250
      end
      item
        Name = 'NIP'
        Attributes = [faRequired, faFixed]
        DataType = ftString
        Size = 13
      end
      item
        Name = 'ULICA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MIASTO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'KOD_P'
        Attributes = [faRequired]
        DataType = ftString
        Size = 8
      end
      item
        Name = 'DATA_PRZYSTAPIENIA'
        DataType = ftDate
      end
      item
        Name = 'TELEFON'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'KONCESJA'
        DataType = ftString
        Size = 5
      end
      item
        Name = 'METRAZ'
        Attributes = [faFixed]
        DataType = ftString
        Size = 5
      end
      item
        Name = 'KATEGORIA'
        Attributes = [faFixed]
        DataType = ftString
        Size = 5
      end
      item
        Name = 'AKTYWNY'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'UWAGI'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <
      item
        Name = 'RDB$PRIMARY4'
        Fields = 'NR_KONTRAH'
        Options = [ixPrimary, ixUnique]
      end>
    StoreDefs = True
    TableName = 'KONTRAH'
    Left = 192
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = IBQuery1
    Left = 248
    Top = 8
  end
  object IBQuery1: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'SELECT B.NAZWA,SUM(A.PROWIZJA_KWOTA) AS PROWIZJA,SUM(A.OBROTY) A' +
        'S OBROTY FROM obroty A,KONTRAH B'
      'WHERE B.NR_KONTRAH = A.NR_KONTRAH_SKLEP'
      'GROUP BY B.NAZWA')
    Left = 312
    Top = 8
    object IBQuery1NAZWA: TIBStringField
      FieldName = 'NAZWA'
      Origin = 'KONTRAH.NAZWA'
      Required = True
      Size = 250
    end
    object IBQuery1PROWIZJA: TFloatField
      FieldName = 'PROWIZJA'
      DisplayFormat = '# ##0.00 z'#322
    end
    object IBQuery1OBROTY: TIBBCDField
      FieldName = 'OBROTY'
      DisplayFormat = '# ##0.00 z'#322
      Precision = 18
      Size = 2
    end
  end
end
