object frRaportImport: TfrRaportImport
  Left = 192
  Top = 125
  Width = 1142
  Height = 656
  Caption = 'Raport importu'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ibqRaportImport: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'SELECT B.NAZWA,B.MIASTO,A.NAZWA_D AS DOSTAWCA,A.OBROTY,A.PROWIZJ' +
        'A_KWOTA,A.PROW_E_KWOTA,A.ZYSK,A.OKRES_M,A.OKRES_Q FROM OBROTY A,' +
        'KONTRAH B'
      
        'WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND A.OKRES_M LIKE :B AN' +
        'D A.NAZWA_D LIKE :A'
      'ORDER BY B.NAZWA,B.MIASTO')
    Left = 824
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'B'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'A'
        ParamType = ptUnknown
      end>
    object ibqRaportImportNAZWA: TIBStringField
      FieldName = 'NAZWA'
      Origin = 'KONTRAH.NAZWA'
      Required = True
      Size = 250
    end
    object ibqRaportImportMIASTO: TIBStringField
      FieldName = 'MIASTO'
      Origin = 'KONTRAH.MIASTO'
      Required = True
      Size = 30
    end
    object ibqRaportImportDOSTAWCA: TIBStringField
      FieldName = 'DOSTAWCA'
      Origin = 'OBROTY.NAZWA_D'
      Required = True
      Size = 250
    end
    object ibqRaportImportOBROTY: TIBBCDField
      FieldName = 'OBROTY'
      Origin = 'OBROTY.OBROTY'
      Required = True
      DisplayFormat = '#0.00 z'#322
      currency = True
      Precision = 18
      Size = 2
    end
    object ibqRaportImportPROWIZJA_KWOTA: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'PROWIZJA_KWOTA'
      Origin = 'OBROTY.PROWIZJA_KWOTA'
      ReadOnly = True
      DisplayFormat = '#0.00 z'#322
      currency = True
    end
    object ibqRaportImportPROW_E_KWOTA: TIBBCDField
      FieldKind = fkInternalCalc
      FieldName = 'PROW_E_KWOTA'
      Origin = 'OBROTY.PROW_E_KWOTA'
      ReadOnly = True
      DisplayFormat = '#0.00 z'#322
      currency = True
      Precision = 18
      Size = 2
    end
    object ibqRaportImportZYSK: TIBBCDField
      FieldKind = fkInternalCalc
      FieldName = 'ZYSK'
      Origin = 'OBROTY.ZYSK'
      ReadOnly = True
      DisplayFormat = '#0.00 z'#322
      currency = True
      Precision = 18
      Size = 2
    end
    object ibqRaportImportOKRES_M: TIBStringField
      FieldName = 'OKRES_M'
      Origin = 'OBROTY.OKRES_M'
    end
    object ibqRaportImportOKRES_Q: TIBStringField
      FieldName = 'OKRES_Q'
      Origin = 'OBROTY.OKRES_Q'
    end
  end
  object dsRaportImport: TDataSource
    DataSet = ibqRaportImport
    Left = 872
    Top = 8
  end
end
