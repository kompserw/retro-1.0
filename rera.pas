unit rera;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, StdCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid, ExtCtrls, DBCtrls, ComCtrls,
  PageControlEx, JvExStdCtrls, JvButton, JvCtrls;

type
  TfrRetra = class(TForm)
    ibqRetro: TIBQuery;
    dsRetro: TDataSource;
    pc: TPageControlEx;
    tsTabelaR: TTabSheet;
    tsEdycjaR: TTabSheet;
    GroupBox1: TGroupBox;
    dbRetra: TJvDBUltimGrid;
    DBNavigator1: TDBNavigator;
    GroupBox2: TGroupBox;
    leRetro: TLabeledEdit;
    leRetroS: TLabeledEdit;
    leOpis: TLabeledEdit;
    rgOkres: TRadioGroup;
    JvImgBtn1: TJvImgBtn;
    JvImgBtn2: TJvImgBtn;
    JvImgBtn3: TJvImgBtn;
    ibqKasujR: TIBQuery;
    procedure dbRetraDblClick(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure OdswiezRetra;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frRetra: TfrRetra;
  nr_kontrah : Integer;
implementation
uses baza,glowny;
{$R *.dfm}

procedure TfrRetra.dbRetraDblClick(Sender: TObject);
begin
pc.ActivePageIndex := 1;
nr_kontrah := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
leRetro.Text := ibqRetro.FieldValues['RETRO'];
leRetroS.Text := ibqRetro.FieldValues['RETRO_KLIENT'];
leOpis.Text := ibqRetro.FieldValues['OPIS'];
if Trim(ibqRetro.FieldValues['OKRES']) = 'M' then rgOkres.ItemIndex := 0
   else rgOkres.ItemIndex := 1;
end;

procedure TfrRetra.JvImgBtn1Click(Sender: TObject);
var
   nr : Integer;
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
nr := ibqRetro.FieldValues['NR_PROWIZJE'];
with ibqRetro do
     begin
          Close;
          SQL.Clear;
          SQL.Add('UPDATE PROWIZJE');
          SQL.Add('SET RETRO=:B,OKRES=:C,RETRO_KLIENT=:D,OPIS=:E');
          SQL.Add('WHERE NR_PROWIZJE = :A');
          UnPrepare;
              ParamByName('A').AsInteger := nr;
              ParamByName('B').AsFloat := StrToFloat(leRetro.Text);
              if rgOkres.ItemIndex = 0 then ParamByName('C').AsString := 'M'
              else ParamByName('C').AsString := 'Q';
              ParamByName('D').AsFloat := StrToFloat(leRetroS.Text);
              ParamByName('E').AsString := UpperCase(leOpis.Text);
          Prepare;
          Open;
     end;
     if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
frGlowny.OdswiezBazy;
OdswiezRetra;
pc.ActivePageIndex := 0;
leRetro.Text := '';
leRetroS.Text := '';
leOpis.Text := '';
end;

procedure TfrRetra.JvImgBtn2Click(Sender: TObject);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with ibqRetro do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO PROWIZJE(NR_KONTRAH,RETRO,OKRES,RETRO_KLIENT,OPIS)');
          SQL.Add('VALUES(:A,:B,:C,:D,:E)');
          UnPrepare;
              ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
              ParamByName('B').AsFloat := StrToFloat(leRetro.Text);
              if rgOkres.ItemIndex = 0 then ParamByName('C').AsString := 'M'
              else ParamByName('C').AsString := 'Q';
              ParamByName('D').AsFloat := StrToFloat(leRetroS.Text);
              ParamByName('E').AsString := UpperCase(leOpis.Text);
          Prepare;
          Open;
     end;
     if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
frGlowny.OdswiezBazy;
OdswiezRetra;
pc.ActivePageIndex := 0;
leRetro.Text := '';
leRetroS.Text := '';
leOpis.Text := '';
end;

procedure TfrRetra.JvImgBtn3Click(Sender: TObject);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with ibqKasujR do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM PROWIZJE');
          SQL.Add('WHERE NR_PROWIZJE = :A');
          UnPrepare;
              ParamByName('A').AsInteger := ibqRetro.FieldValues['NR_PROWIZJE'];
          Prepare;
          Open;
     end;
     if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
frGlowny.OdswiezBazy;
OdswiezRetra;
pc.ActivePageIndex := 0;
leRetro.Text := '';
leRetroS.Text := '';
leOpis.Text := '';     
end;

procedure TfrRetra.OdswiezRetra;
begin
with ibqRetro do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT RETRO,OKRES,RETRO_KLIENT,OPIS,NR_PROWIZJE FROM PROWIZJE');
          SQL.Add('WHERE NR_KONTRAH = :A');
          UnPrepare;
                    ParamByName('A').AsInteger := nr_kontrah;
          Prepare;
          Open;
     end;
end;

end.
