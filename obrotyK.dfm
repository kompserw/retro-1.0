object frObrotyK: TfrObrotyK
  Left = 594
  Top = 264
  Width = 887
  Height = 656
  Caption = 'Obroty kontrahenta'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object JvPanel1: TJvPanel
    Left = 0
    Top = 0
    Width = 871
    Height = 241
    Align = alTop
    Color = clWhite
    TabOrder = 0
    object JvGroupBox1: TJvGroupBox
      Left = 16
      Top = 0
      Width = 841
      Height = 193
      Caption = 'Dane kontrahenta'
      TabOrder = 0
      object DBText1: TDBText
        Left = 24
        Top = 51
        Width = 185
        Height = 17
        DataField = 'IMIE_NAZWISKO'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText2: TDBText
        Left = 224
        Top = 51
        Width = 441
        Height = 17
        DataField = 'NAZWA'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText3: TDBText
        Left = 296
        Top = 99
        Width = 249
        Height = 17
        DataField = 'ULICA'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText4: TDBText
        Left = 128
        Top = 99
        Width = 161
        Height = 17
        DataField = 'MIASTO'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText5: TDBText
        Left = 24
        Top = 99
        Width = 65
        Height = 17
        DataField = 'KOD_P'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText6: TDBText
        Left = 560
        Top = 99
        Width = 121
        Height = 17
        DataField = 'NIP'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText7: TDBText
        Left = 701
        Top = 99
        Width = 65
        Height = 17
        DataField = 'TELEFON'
        DataSource = dm.dsKontrah
      end
      object DBText8: TDBText
        Left = 24
        Top = 148
        Width = 65
        Height = 17
        DataField = 'KONCESJA'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText9: TDBText
        Left = 112
        Top = 148
        Width = 65
        Height = 17
        DataField = 'METRAZ'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText10: TDBText
        Left = 213
        Top = 148
        Width = 65
        Height = 17
        DataField = 'KATEGORIA'
        DataSource = dm.dsKontrah
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 24
        Top = 32
        Width = 182
        Height = 16
        Caption = 'Imi'#281' i nazwisko kontrahenta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 224
        Top = 32
        Width = 79
        Height = 16
        Caption = 'Nazwa firmy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 24
        Top = 80
        Width = 91
        Height = 16
        Caption = 'Kod pocztowy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 128
        Top = 80
        Width = 43
        Height = 16
        Caption = 'Miasto'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 296
        Top = 80
        Width = 29
        Height = 16
        Caption = 'Ulica'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 560
        Top = 80
        Width = 21
        Height = 16
        Caption = 'NIP'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 24
        Top = 128
        Width = 58
        Height = 16
        Caption = 'Koncesja'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 112
        Top = 128
        Width = 91
        Height = 16
        Caption = 'Metra'#380' sklepu'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 213
        Top = 130
        Width = 63
        Height = 16
        Caption = 'Kategoria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 700
        Top = 80
        Width = 47
        Height = 16
        Caption = 'Telefon'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 323
        Top = 130
        Width = 106
        Height = 16
        Caption = 'Miesi'#261'c obrot'#243'w'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 488
        Top = 130
        Width = 110
        Height = 16
        Caption = 'Kwarta'#322' obrot'#243'w'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbMiesiac: TJvComboBox
        Left = 323
        Top = 152
        Width = 145
        Height = 21
        TabOrder = 0
        Text = 'wszystkie'
        OnChange = cbMiesiacChange
        Items.Strings = (
          'wszystkie'
          'stycze'#324
          'luty'
          'marzec'
          'kwiecie'#324
          'maj'
          'czerwiec'
          'lipiec'
          'sierpie'#324
          'wrzesie'#324
          'pa'#378'dziernik'
          'listopad'
          'grudzie'#324)
        ItemIndex = 0
      end
      object cbKwartal: TJvComboBox
        Left = 488
        Top = 152
        Width = 145
        Height = 21
        TabOrder = 1
        Text = 'wszystkie'
        OnChange = cbKwartalChange
        Items.Strings = (
          'wszystkie'
          'I kwarta'#322
          'II kwarta'#322
          'III kwarta'#322
          'IV kwarta'#322)
        ItemIndex = 0
      end
      object leNazwa: TLabeledEdit
        Left = 648
        Top = 152
        Width = 177
        Height = 21
        EditLabel.Width = 178
        EditLabel.Height = 16
        EditLabel.Caption = 'Szukaj dostawcy po nazwie'
        EditLabel.Font.Charset = DEFAULT_CHARSET
        EditLabel.Font.Color = clWindowText
        EditLabel.Font.Height = -13
        EditLabel.Font.Name = 'Tahoma'
        EditLabel.Font.Style = [fsBold]
        EditLabel.ParentFont = False
        TabOrder = 2
        OnChange = leNazwaChange
      end
      object JvImgBtn1: TJvImgBtn
        Left = 768
        Top = 8
        Width = 65
        Height = 57
        Hint = 'Drukuj zestawienie'
        TabOrder = 3
        OnClick = JvImgBtn1Click
        Color = clWhite
        Flat = True
        Images = frGlowny.JvImageList1
        ImageIndex = 23
      end
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 200
      Width = 841
      Height = 33
      TabOrder = 1
      object lObroty: TLabel
        Left = 304
        Top = 11
        Width = 89
        Height = 16
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 197
        Top = 9
        Width = 103
        Height = 16
        Caption = 'Suma obrot'#243'w: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 397
        Top = 9
        Width = 98
        Height = 16
        Caption = 'Suma prowizji: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lProwizja: TLabel
        Left = 496
        Top = 10
        Width = 89
        Height = 16
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object frxPreview1: TfrxPreview
        Left = 808
        Top = 32
        Width = 100
        Height = 100
        OutlineVisible = True
        OutlineWidth = 121
        ThumbnailVisible = False
        UseReportHints = True
      end
    end
  end
  object JvPanel2: TJvPanel
    Left = 0
    Top = 241
    Width = 871
    Height = 376
    Align = alClient
    Color = clWhite
    TabOrder = 1
    object grObroty: TJvDBUltimGrid
      Left = 1
      Top = 1
      Width = 869
      Height = 374
      Align = alClient
      BorderStyle = bsNone
      DataSource = dsObroty
      FixedColor = clWhite
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDrawDataCell = grObrotyDrawDataCell
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      WordWrap = True
    end
  end
  object ibqObroty: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 672
    Top = 224
  end
  object dsObroty: TDataSource
    DataSet = ibqObroty
    Left = 728
    Top = 224
  end
  object ibqSuma: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 624
    Top = 224
  end
  object frxReport1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Domy'#347'lny'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42620.616842199100000000
    ReportOptions.LastChange = 42620.955630902780000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 768
    Top = 224
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end
      item
        DataSet = frxDBDataset2
        DataSetName = 'frxDBDataset2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 156.078850000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object frxDBDataset2IMIE_NAZWISKO: TfrxMemoView
          Left = 3.779530000000000000
          Top = 4.897650000000000000
          Width = 226.771800000000000000
          Height = 18.897650000000000000
          DataField = 'IMIE_NAZWISKO'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."IMIE_NAZWISKO"]')
        end
        object frxDBDataset2NAZWA: TfrxMemoView
          Left = 230.551330000000000000
          Top = 4.897650000000000000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          DataField = 'NAZWA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."NAZWA"]')
        end
        object frxDBDataset2ULICA: TfrxMemoView
          Left = 37.795300000000000000
          Top = 35.133890000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DataField = 'ULICA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."ULICA"]')
        end
        object frxDBDataset2KOD_P: TfrxMemoView
          Left = 241.889920000000000000
          Top = 35.133890000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'KOD_P'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset2."KOD_P"]')
          ParentFont = False
        end
        object frxDBDataset2MIASTO: TfrxMemoView
          Left = 302.362400000000000000
          Top = 35.133890000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          DataField = 'MIASTO'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."MIASTO"]')
        end
        object frxDBDataset2NIP: TfrxMemoView
          Left = 3.779530000000000000
          Top = 95.606370000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          DataField = 'NIP'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."NIP"]')
          ParentFont = False
        end
        object frxDBDataset2TELEFON: TfrxMemoView
          Left = 109.606370000000000000
          Top = 95.606370000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          DataField = 'TELEFON'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."TELEFON"]')
          ParentFont = False
        end
        object frxDBDataset2KONCESJA: TfrxMemoView
          Left = 215.433210000000000000
          Top = 95.606370000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          DataField = 'KONCESJA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBDataset2."KONCESJA"]')
          ParentFont = False
        end
        object frxDBDataset2METRAZ: TfrxMemoView
          Left = 347.716760000000000000
          Top = 95.606370000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'METRAZ'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBDataset2."METRAZ"]')
          ParentFont = False
        end
        object frxDBDataset2KATEGORIA: TfrxMemoView
          Left = 483.779840000000000000
          Top = 95.606370000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'KATEGORIA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBDataset2."KATEGORIA"]')
          ParentFont = False
        end
        object frxDBDataset2UWAGI: TfrxMemoView
          Align = baWidth
          Top = 137.181200000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'UWAGI'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Memo.UTF8 = (
            '[frxDBDataset2."UWAGI"]')
        end
        object Memo3: TfrxMemoView
          Left = 3.779530000000000000
          Top = 72.929190000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'NIP')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 109.606370000000000000
          Top = 72.929190000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Telefon')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 215.433210000000000000
          Top = 72.929190000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Posiadane koncesje')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 351.496290000000000000
          Top = 72.929190000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Metra'#313#317' pomieszczenia')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 483.779840000000000000
          Top = 72.929190000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Kategoria')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Top = 118.283550000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Uwagi')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 3.779530000000000000
          Top = 35.133890000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            'Ulica:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 196.535560000000000000
          Top = 31.354360000000000000
          Width = 94.488250000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            'kod i'
            'miasto')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Top = 1.118120000000000000
          Width = 559.370440000000000000
          Height = 64.252010000000000000
          Shape = skRoundRectangle
        end
        object Date: TfrxMemoView
          Left = 638.740570000000000000
          Top = 4.897650000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 275.905690000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
        end
        object frxDBDataset1DOSTAWCA: TfrxMemoView
          Left = 37.795300000000000000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          DataField = 'DOSTAWCA'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."DOSTAWCA"]')
          ParentFont = False
        end
        object frxDBDataset1OBROTY: TfrxMemoView
          Left = 321.260050000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'OBROTY'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."OBROTY"]')
          ParentFont = False
        end
        object frxDBDataset1PROWIZJA_PROC: TfrxMemoView
          Left = 400.630180000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'PROWIZJA_PROC'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBDataset1."PROWIZJA_PROC"]')
          ParentFont = False
        end
        object frxDBDataset1PROWIZJA_KWOTA: TfrxMemoView
          Left = 480.000310000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'PROWIZJA_KWOTA'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDBDataset1."PROWIZJA_KWOTA"]')
          ParentFont = False
        end
        object frxDBDataset1OKRES_M: TfrxMemoView
          Left = 559.370440000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'OKRES_M'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBDataset1."OKRES_M"]')
          ParentFont = False
        end
        object frxDBDataset1OKRES_Q: TfrxMemoView
          Left = 638.740570000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'OKRES_Q'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBDataset1."OKRES_Q"]')
          ParentFont = False
        end
        object Line: TfrxMemoView
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Line#]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 196.535560000000000000
        Width = 718.110700000000000000
        object Memo11: TfrxMemoView
          Left = 37.795300000000000000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Dostawca')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 321.260050000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Obroty')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 400.630180000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Prowizja %')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 480.000310000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Prowizja')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 559.370440000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Okres obrot'#258#322'w')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Lp.')
          ParentFont = False
        end
      end
      object ColumnFooter1: TfrxColumnFooter
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 355.275820000000000000
        Width = 718.110700000000000000
        object Shape3: TfrxShapeView
          Left = 321.260050000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
        end
        object Memo1: TfrxMemoView
          Left = 321.260050000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDBDataset1."OBROTY">,MasterData1,2)]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 480.000310000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDBDataset1."PROWIZJA_KWOTA">,MasterData1,2)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Left = 627.401980000000000000
          Top = 3.779530000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Page] z [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
    end
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DOSTAWCA=DOSTAWCA'
      'OBROTY=OBROTY'
      'PROWIZJA_PROC=PROWIZJA_PROC'
      'PROWIZJA_KWOTA=PROWIZJA_KWOTA'
      'OKRES_M=OKRES_M'
      'OKRES_Q=OKRES_Q')
    DataSet = ibqObroty
    BCDToCurrency = False
    Left = 768
    Top = 273
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    DataSet = dm.ibqKontrahenci
    BCDToCurrency = False
    Left = 816
    Top = 273
  end
end
