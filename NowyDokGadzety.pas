unit NowyDokGadzety;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, PageControlEx, Grids, DBGrids, StdCtrls, ExtCtrls,
  JvExStdCtrls, JvButton, JvCtrls, Menus;

type
  TfrNowyDokG = class(TForm)
    pc: TPageControlEx;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    LabeledEdit14: TLabeledEdit;
    LabeledEdit15: TLabeledEdit;
    Panel1: TPanel;
    DBGrid5: TDBGrid;
    LabeledEdit1: TLabeledEdit;
    Panel2: TPanel;
    LabeledEdit2: TLabeledEdit;
    JvImgBtn4: TJvImgBtn;
    Panel4: TPanel;
    Splitter1: TSplitter;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    JvImgBtn1: TJvImgBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    JvImgBtn2: TJvImgBtn;
    JvImgBtn3: TJvImgBtn;
    pmPozycjeWydania: TPopupMenu;
    Usuwybranpozycj1: TMenuItem;
    Modyfikujwybranpozycj1: TMenuItem;
    procedure UstawTypDok(typ: Integer);
    procedure LabeledEdit14Change(Sender: TObject);
    procedure LabeledEdit15Change(Sender: TObject);
    procedure LabeledEdit1Change(Sender: TObject);
    procedure UstawSiatkeO(aGrid: TDBGrid);
    procedure UstawSiatkeD(aGrid: TDBGrid);
    procedure UstawSiatkeT(aGrid: TDBGrid);
    procedure UstawSiatkeP(aGrid: TDBGrid);
    procedure DBGrid5CellClick(Column: TColumn);
    procedure LabeledEdit2Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure Usuwybranpozycj1Click(Sender: TObject);
    procedure Modyfikujwybranpozycj1Click(Sender: TObject);
    procedure JvImgBtn4Click(Sender: TObject);
  private
    Typ_Dok: Integer;
  public
    { Public declarations }
  end;

var
  frNowyDokG: TfrNowyDokG;
  kontrah,nazwa,nip: String;


implementation
uses glowny,baza,towary,dodajPGadzet;
{$R *.dfm}

{ TfrNowyDokG }

procedure TfrNowyDokG.UstawTypDok(typ: Integer);
begin
Typ_Dok := typ;
end;

procedure TfrNowyDokG.LabeledEdit14Change(Sender: TObject);
begin
// szukanie po nazwisku
if Typ_Dok = 2 then
   begin
        dm.SzukajDostawcy(LabeledEdit14.Text,'','');
        UstawSiatkeD(DBGrid5);
   end
else
    begin
         dm.SzukajOdbiorcy('',LabeledEdit14.Text,'');
         UstawSiatkeO(DBGrid5);
    end;
end;

procedure TfrNowyDokG.LabeledEdit15Change(Sender: TObject);
begin
// szukanie po nip
if Typ_Dok = 2 then
   begin
        dm.SzukajDostawcy('','',LabeledEdit15.Text);
        UstawSiatkeD(DBGrid5);
   end
else
    begin
         dm.SzukajOdbiorcy('','',LabeledEdit15.Text);
         UstawSiatkeO(DBGrid5);
    end;
end;

procedure TfrNowyDokG.LabeledEdit1Change(Sender: TObject);
begin
// szukanie po nazwie firmy
if Typ_Dok = 2 then
   begin
        dm.SzukajDostawcy(LabeledEdit1.Text,'','');
        UstawSiatkeD(DBGrid5);
   end
else
    begin
         dm.SzukajOdbiorcy('',LabeledEdit1.Text,'');
        UstawSiatkeO(DBGrid5);;
   end
end;

procedure TfrNowyDokG.UstawSiatkeD(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa firmy';
          lColumn.Width := 300;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NIP';
          lColumn.Title.Caption := 'NIP';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ULICA';
          lColumn.Title.Caption := 'Ulica';
          lColumn.Width := 100;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'MIASTO';
          lColumn.Title.Caption := 'Miasto';
          lColumn.Width := 100;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'KOD_P';
          lColumn.Title.Caption := 'Kod poczt.';
          lColumn.Width := 100;

     end;
end;

procedure TfrNowyDokG.UstawSiatkeO(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'IMIE_NAZWISKO';
          lColumn.Title.Caption := 'Imi� i nazwisko';
          lColumn.Width := 200;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa firmy';
          lColumn.Width := 300;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NIP';
          lColumn.Title.Caption := 'NIP';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ULICA';
          lColumn.Title.Caption := 'Ulica';
          lColumn.Width := 100;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'MIASTO';
          lColumn.Title.Caption := 'Miasto';
          lColumn.Width := 100;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'KOD_P';
          lColumn.Title.Caption := 'Kod poczt.';
          lColumn.Width := 100;
     end;
end;

procedure TfrNowyDokG.DBGrid5CellClick(Column: TColumn);
begin
kontrah := dm.ibqKontrahenci.FieldValues['IMIE_NAZWISKO'];
nazwa := dm.ibqKontrahenci.FieldValues['NAZWA'];
nip := dm.ibqKontrahenci.FieldValues['NIP'];
pc.ActivePageIndex := 1;
UstawSiatkeT(DBGrid1);
end;

procedure TfrNowyDokG.UstawSiatkeT(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa gad�etu';
          lColumn.Width := 300;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ILOSC';
          lColumn.Title.Caption := 'Ilo��';
          lColumn.Width := 60;
     end;

end;

procedure TfrNowyDokG.LabeledEdit2Change(Sender: TObject);
var
   nazwa: String;
begin
nazwa := LabeledEdit2.Text;
dm.SzukajTowar(0,nazwa);
UstawSiatkeT(DBGrid1);
end;

procedure TfrNowyDokG.DBGrid1DblClick(Sender: TObject);
begin
with frDodajPGadzet do
     begin
          LabeledEdit1.Text := dm.ibqTowary.FieldValues['NAZWA'];
          LabeledEdit2.Text := '1';
          LabeledEdit3.Text := dm.ibqTowary.FieldValues['CENA_N_ZAKUP'];
          LabeledEdit4.Text := dm.ibqTowary.FieldValues['CENA_N_ZAKUP'];
          Label2.Caption := dm.ibqTowary.FieldValues['ILOSC'];
          nr_towar := dm.ibqTowary.FieldValues['NR_TOWARY'];
          nr_naglowek := dm.NrNaglowka;
          nr_kontrah := dm.NrKontrahent(nazwa);
          UstawTypDok(Typ_Dok);
          ShowModal;
     end;
end;

procedure TfrNowyDokG.UstawSiatkeP(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'POZYCJA';
          lColumn.Title.Caption := 'Nazwa gad�etu';
          lColumn.Width := 300;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ILOSC';
          lColumn.Title.Caption := 'Ilo��';
          lColumn.Width := 60;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'CENA_NETTO';
          lColumn.Title.Caption := 'Cena';
          lColumn.Width := 60;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'WARTOSC_NETTO';
          lColumn.Title.Caption := 'Warto��';
          lColumn.Width := 60;
     end;

end;

procedure TfrNowyDokG.JvImgBtn1Click(Sender: TObject);
var
     wartosc: Real;
     nr_nag: Integer;
begin
//ZAPIS NAGLOWKA DOKUMENTU
nr_nag := dm.NrNaglowka + 1;
Label1.Caption := kontrah;
Label2.Caption := nazwa;
Label3.Caption := nip;
wartosc := dm.SumaPozycji(nr_nag);
Label4.Caption := FloatToStr(wartosc);
pc.ActivePageIndex := 2;
end;

procedure TfrNowyDokG.JvImgBtn2Click(Sender: TObject);
var
     nr_nag: Integer;
begin
nr_nag := dm.NrNaglowka;
dm.KasujPozycje(nr_nag);
Close;
end;

procedure TfrNowyDokG.JvImgBtn3Click(Sender: TObject);
var
   nr_kontrah,nr_naglowek,nr_dokumentu,typDok: Integer;
   netto: Real;
   miesiac_dok,rok_dok,dzien: Word;
begin
nr_kontrah := dm.NrKontrahent(nazwa);
nr_naglowek := dm.NrNaglowka + 1;
netto := StrToFloat(Label4.Caption);
typDok := Typ_Dok;
nr_dokumentu := dm.NrDokumentu(typDok);
//typDok := Typ_Dok;
DecodeDate(now,rok_dok,miesiac_dok,dzien);
dm.NaglowekG(nr_kontrah,nr_naglowek,Typ_Dok,nr_dokumentu,netto,miesiac_dok,rok_dok,dzien);
dm.NaglowekG(1,0,0,1,0,0);
Label1.Caption := '';
Label2.Caption := '';
Label3.Caption := '';
Label4.Caption := '';
Close;
end;

procedure TfrNowyDokG.Usuwybranpozycj1Click(Sender: TObject);
var
   towar: String;
   nr_naglowek,nr_pozycji,nr_towar: Integer;
   ilosc_wydana,ilosc: Real;
begin
//Usuwa wybran� pozycj� z listy gadzet�w dokumentu wydania / przyj�cia
towar := dm.ibqGadzetyP.FieldValues['POZYCJA'];
nr_pozycji := dm.ibqGadzetyP.FieldValues['NR_GADZETY_POZYCJE'];
nr_towar := dm.NrTowar(towar);
ilosc := dm.IleJestTowaru(0,towar);
ilosc_wydana := dm.ibqGadzetyP.FieldValues['ILOSC'];
nr_naglowek := dm.NrNaglowka;
if Typ_Dok = 2 then
   ilosc := ilosc - ilosc_wydana
else
    ilosc := ilosc + ilosc_wydana;
if MessageBox(Handle,'Czy chcesz usun�� wybran� pozycje z dokumentu ?','Modyfikacja dokumentu',MB_YESNO + MB_ICONQUESTION) = IdYes then
   begin
        dm.UsunPozG(nr_naglowek,nr_pozycji);
        dm.TowaryStany(nr_towar,ilosc);
   end;

dm.SzukajTowar(0,'');
dm.PozycjeG(2,nr_naglowek,0,0,'',0,0,0);
UstawSiatkeT(frNowyDokG.DBGrid1);
UstawSiatkeP(frNowyDokG.DBGrid2);
end;

procedure TfrNowyDokG.Modyfikujwybranpozycj1Click(Sender: TObject);
begin
//Modyfikuje wybran� pozycj� z listy gadzet�w dokumentu wydania / przyj�cia
end;

procedure TfrNowyDokG.JvImgBtn4Click(Sender: TObject);
begin
//DODAWANIE NOWEGO TOWARU DO BAZY
end;

end.
