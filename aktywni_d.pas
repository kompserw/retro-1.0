unit aktywni_d;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, JvImageList, StdCtrls, JvExStdCtrls, JvButton, JvCtrls,
  Grids, DBGrids, DB;

type
  TfrAktywniDostawcy = class(TForm)
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    JvImgBtn1: TJvImgBtn;
    JvImgBtn2: TJvImgBtn;
    JvImageList1: TJvImageList;
    procedure JvImgBtn1Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frAktywniDostawcy: TfrAktywniDostawcy;

implementation
uses
baza;
{$R *.dfm}

procedure TfrAktywniDostawcy.JvImgBtn1Click(Sender: TObject);
var
   nr_dostawcy: Integer;
   zakladka: TBookmark;
begin
//ZAPISZ DO DOSTAWCOW
zakladka := dm.ibqAktywniD.GetBookmark;
nr_dostawcy := dm.ibqAktywniD.FieldValues['NR_KONTRAH'];
dm.WstawDoDostawcow(nr_dostawcy);
dm.AktywniD_Pokaz;
dm.ibqAktywniD.GotoBookmark(zakladka);
dm.AktywniDD_Pokaz;
end;

procedure TfrAktywniDostawcy.JvImgBtn2Click(Sender: TObject);
var
   nr_dostawcy: Integer;
   zakladka: TBookmark;
begin
//COFINIJ Z DOSTAWCOW
zakladka := dm.ibqAktywniD_P.GetBookmark;
nr_dostawcy := dm.ibqAktywniD_P.FieldValues['NR_ORG'];
dm.UsunZDD(nr_dostawcy);
dm.AktywniD_Pokaz;
dm.AktywniDD_Pokaz;
dm.ibqAktywniD_P.GotoBookmark(zakladka);
end;

procedure TfrAktywniDostawcy.JvImgBtn3Click(Sender: TObject);
begin
//ZAMKNIJ OKIENKO
end;

end.
