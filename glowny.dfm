object frGlowny: TfrGlowny
  Left = 248
  Top = 174
  Width = 1142
  Height = 672
  Caption = 'Import sprzeda'#380'y sklep'#243'w sieci EURO'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object sb: TStatusBar
    Left = 0
    Top = 594
    Width = 1126
    Height = 19
    Panels = <>
  end
  object tbGlowny: TToolBar
    Left = 0
    Top = 0
    Width = 1126
    Height = 49
    ButtonHeight = 38
    ButtonWidth = 39
    Caption = 'tbGlowny'
    Images = JvImageList1
    TabOrder = 1
    object ImpExcel: TToolButton
      Left = 0
      Top = 2
      Hint = 'Import z pliku obrot'#243'w CSV'
      Caption = 'ImpExcel'
      ImageIndex = 8
      ParentShowHint = False
      ShowHint = True
      OnClick = ImpExcelClick
    end
    object KontrahS: TToolButton
      Left = 39
      Top = 2
      Hint = 'Dodaj kontrahenta'
      Caption = 'KontrahS'
      ImageIndex = 11
      ParentShowHint = False
      ShowHint = True
      OnClick = KontrahSClick
    end
    object Prowizje: TToolButton
      Left = 78
      Top = 2
      Hint = 'Prowizje'
      Caption = 'Prowizje'
      ImageIndex = 22
      ParentShowHint = False
      ShowHint = True
      Visible = False
    end
    object ToolButton4: TToolButton
      Left = 117
      Top = 2
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 3
      Style = tbsSeparator
    end
    object KatSklepy: TToolButton
      Left = 125
      Top = 2
      Hint = 'Drukuj raporty'
      ImageIndex = 23
      ParentShowHint = False
      PopupMenu = pmRaporty
      ShowHint = True
      OnClick = KatSklepyClick
    end
    object KartDostwacy: TToolButton
      Left = 164
      Top = 2
      Hint = 'Export do Excela'
      Caption = 'KartDostwacy'
      ImageIndex = 30
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = KartDostwacyClick
    end
    object RaportyButton: TToolButton
      Left = 203
      Top = 2
      Hint = 'Drukuj jako pdf'
      Caption = 'RaportyButton'
      ImageIndex = 31
      ParentShowHint = False
      ShowHint = True
      Visible = False
    end
    object ToolButton9: TToolButton
      Left = 242
      Top = 2
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton20: TToolButton
      Left = 250
      Top = 2
      Hint = 'Poka'#380' trasy'
      Caption = 'ToolButton20'
      ImageIndex = 35
      ParentShowHint = False
      ShowHint = True
      OnClick = rasy1Click
    end
    object ToolButton22: TToolButton
      Left = 289
      Top = 2
      Hint = 'Kartoteka gad'#380'et'#243'w'
      Caption = 'ToolButton22'
      ImageIndex = 38
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton22Click
    end
    object ToolButton8: TToolButton
      Left = 328
      Top = 2
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 12
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 336
      Top = 2
      Hint = 'Koniec'
      Caption = 'ToolButton10'
      ImageIndex = 32
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton10Click
    end
    object ToolButton11: TToolButton
      Left = 375
      Top = 2
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 13
      Style = tbsSeparator
    end
    object ToolButton12: TToolButton
      Left = 383
      Top = 2
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 14
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 391
      Top = 2
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton13: TToolButton
      Left = 399
      Top = 2
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 15
      Style = tbsSeparator
    end
    object ToolButton14: TToolButton
      Left = 407
      Top = 2
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 16
      Style = tbsSeparator
    end
    object jpbPasek: TJvProgressBar
      Left = 415
      Top = 2
      Width = 150
      Height = 38
      TabOrder = 0
    end
    object ToolButton1: TToolButton
      Left = 565
      Top = 2
      Caption = 'ToolButton1'
      ImageIndex = 16
      Visible = False
      OnClick = ToolButton1Click
    end
  end
  object jpcGlowna: TJvPageControl
    Left = 0
    Top = 49
    Width = 1126
    Height = 545
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 2
    OnChange = jpcGlownaChange
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      TabVisible = False
      object JvMemo1: TJvMemo
        Left = 0
        Top = 0
        Width = 1118
        Height = 517
        Align = alClient
        Lines.Strings = (
          'JvMemo1')
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      TabVisible = False
      object JvPanel1: TJvPanel
        Left = 0
        Top = 0
        Width = 1118
        Height = 289
        Align = alTop
        TabOrder = 0
        object JvToolBar1: TJvToolBar
          Left = 1
          Top = 1
          Width = 1116
          Height = 29
          Caption = 'JvToolBar1'
          TabOrder = 0
          object JvDBNavigator1: TJvDBNavigator
            Left = 0
            Top = 2
            Width = 240
            Height = 22
            DataSource = dm.dsKontrah
            TabOrder = 0
          end
        end
        object JvDBUltimGrid1: TJvDBUltimGrid
          Left = 1
          Top = 30
          Width = 1116
          Height = 258
          Align = alClient
          DataSource = dm.dsKontrah
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
        end
      end
      object JvPanel2: TJvPanel
        Left = 0
        Top = 289
        Width = 1118
        Height = 228
        Align = alClient
        TabOrder = 1
        object TuSzukaj: TDBEdit
          Left = 8
          Top = 16
          Width = 33
          Height = 21
          DataField = 'lp'
          TabOrder = 0
        end
        object dbImie_naz: TDBEdit
          Left = 48
          Top = 16
          Width = 121
          Height = 21
          DataField = 'imie nazwisko'
          DataSource = dm.JvDataSource1
          TabOrder = 1
        end
        object dbNazwa: TDBEdit
          Left = 48
          Top = 40
          Width = 400
          Height = 21
          DataField = 'sklep'
          DataSource = dm.JvDataSource1
          TabOrder = 2
        end
        object dbMiasto: TDBEdit
          Left = 184
          Top = 64
          Width = 121
          Height = 21
          DataField = 'miejscowo'#347#263
          DataSource = dm.JvDataSource1
          TabOrder = 3
        end
        object dbUlica: TDBEdit
          Left = 48
          Top = 96
          Width = 121
          Height = 21
          DataField = 'ulica'
          DataSource = dm.JvDataSource1
          TabOrder = 4
        end
        object dbKod_p: TDBEdit
          Left = 48
          Top = 64
          Width = 121
          Height = 21
          DataField = ' '
          DataSource = dm.JvDataSource1
          TabOrder = 5
        end
        object dbNIP: TDBEdit
          Left = 184
          Top = 96
          Width = 121
          Height = 21
          DataField = 'nip'
          DataSource = dm.JvDataSource1
          TabOrder = 6
        end
        object dbData_P: TDBEdit
          Left = 312
          Top = 64
          Width = 121
          Height = 21
          DataField = 'data'
          DataSource = dm.JvDataSource1
          TabOrder = 7
        end
        object dbMetraz: TDBEdit
          Left = 184
          Top = 128
          Width = 121
          Height = 21
          DataField = 'metra'#380
          DataSource = dm.JvDataSource1
          TabOrder = 8
        end
        object dbKategoria: TDBEdit
          Left = 312
          Top = 128
          Width = 121
          Height = 21
          DataField = 'kat'
          DataSource = dm.JvDataSource1
          TabOrder = 9
        end
        object dbTelefon: TDBEdit
          Left = 312
          Top = 96
          Width = 121
          Height = 21
          DataField = 'telefon'
          DataSource = dm.JvDataSource1
          TabOrder = 10
        end
        object dbKoncesja: TDBEdit
          Left = 48
          Top = 128
          Width = 121
          Height = 21
          DataField = 'koncesja'
          DataSource = dm.JvDataSource1
          TabOrder = 11
        end
        object bZapisz: TButton
          Left = 350
          Top = 192
          Width = 91
          Height = 25
          Caption = 'Zapisz do bazy'
          TabOrder = 12
          OnClick = bZapiszClick
        end
        object imie_naz: TJvEdit
          Left = 472
          Top = 40
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 13
          Text = 'imi'#281' i nazwisko'
        end
        object kod_p: TJvEdit
          Left = 472
          Top = 64
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 14
          Text = 'kod pocztowy'
        end
        object Ulica: TJvEdit
          Left = 472
          Top = 88
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 15
          Text = 'ulica'
        end
        object Koncesja: TJvEdit
          Left = 472
          Top = 112
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 16
          Text = 'koncesja'
        end
        object Nazwa: TJvEdit
          Left = 616
          Top = 40
          Width = 345
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 17
          Text = 'nazwa firmy'
        end
        object Miasto: TJvEdit
          Left = 616
          Top = 64
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 18
          Text = 'miasto'
        end
        object NIP: TJvEdit
          Left = 616
          Top = 88
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 19
          Text = 'NIP'
        end
        object Metraz: TJvEdit
          Left = 616
          Top = 112
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 20
          Text = 'metra'#380
        end
        object Kategoria: TJvEdit
          Left = 752
          Top = 88
          Width = 121
          Height = 21
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 21
          Text = 'kategoria'
        end
        object Telefon: TJvEdit
          Left = 752
          Top = 64
          Width = 121
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuHighlight
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 22
          Text = 'telefon'
        end
        object bKopiuj: TButton
          Left = 352
          Top = 152
          Width = 89
          Height = 25
          Caption = 'Kopiuj ->'
          TabOrder = 23
          OnClick = bKopiujClick
        end
        object Uwagi: TMemo
          Left = 472
          Top = 136
          Width = 265
          Height = 89
          Lines.Strings = (
            'Uwagi')
          TabOrder = 24
        end
        object Data_Prz: TJvMonthCalendar
          Left = 888
          Top = 64
          Width = 191
          Height = 154
          Date = 42417.888723576390000000
          TabOrder = 25
        end
        object JvDBNavigator2: TJvDBNavigator
          Left = 48
          Top = 160
          Width = 240
          Height = 25
          DataSource = dm.JvDataSource1
          TabOrder = 26
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Lista kontrahent'#243'w'
      ImageIndex = 2
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 43
        Width = 1118
        Height = 474
        Align = alClient
        DataSource = dm.dsKontrah
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = pmKontrah
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnCellClick = JvDBGrid1CellClick
        OnDblClick = JvDBGrid1DblClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'NR_KONTRAH'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'RODZAJ'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'IMIE_NAZWISKO'
            Title.Caption = 'Imie i nazwisko'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NAZWA'
            Title.Caption = 'Nazwa firmy'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NIP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ULICA'
            Title.Caption = 'Ulica'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MIASTO'
            Title.Caption = 'Miasto'
            Width = 170
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KOD_P'
            Title.Caption = 'Kod pocztowy'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_PRZYSTAPIENIA'
            Title.Caption = 'Data przyst'#261'pienia'
            Width = 110
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFON'
            Title.Caption = 'Telefon'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KONCESJA'
            Title.Caption = 'Koncesja'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'METRAZ'
            Title.Caption = 'Metra'#380
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KATEGORIA'
            Title.Caption = 'Kategoria'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UWAGI'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AKTYWNY'
            Title.Caption = 'Czy aktywny'
            Visible = True
          end
          item
            Expanded = False
            Visible = True
          end>
      end
      object JvToolBar2: TJvToolBar
        Left = 0
        Top = 0
        Width = 1118
        Height = 2
        AutoSize = True
        ButtonHeight = 17
        ButtonWidth = 30
        Caption = 'JvToolBar2'
        Customizable = True
        Flat = True
        List = True
        TabOrder = 1
        object ToolButton2: TToolButton
          Left = 0
          Top = 0
          Width = 8
          Caption = 'ToolButton2'
          Style = tbsSeparator
        end
        object ToolButton3: TToolButton
          Left = 8
          Top = 0
          Width = 8
          Caption = 'ToolButton3'
          ImageIndex = 0
          Style = tbsSeparator
        end
        object ToolButton5: TToolButton
          Left = 16
          Top = 0
          Width = 8
          Caption = 'ToolButton5'
          ImageIndex = 1
          Style = tbsSeparator
        end
        object ToolButton6: TToolButton
          Left = 24
          Top = 0
          Width = 8
          Caption = 'ToolButton6'
          ImageIndex = 2
          Style = tbsSeparator
        end
        object ToolButton21: TToolButton
          Left = 32
          Top = 0
          Width = 8
          Caption = 'ToolButton21'
          ImageIndex = 3
          Style = tbsSeparator
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 2
        Width = 1118
        Height = 41
        Align = alTop
        TabOrder = 2
        object JvDBNavigator3: TJvDBNavigator
          Left = 8
          Top = 0
          Width = 200
          Height = 33
          DataSource = dm.dsKontrah
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          TabOrder = 0
        end
        object jrbTypD: TJvRadioButton
          Left = 224
          Top = 8
          Width = 68
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Dostawca'
          TabOrder = 1
          OnClick = jrbTypDClick
          LinkedControls = <>
        end
        object jrbTypO: TJvRadioButton
          Left = 292
          Top = 8
          Width = 64
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Odbiorca'
          Checked = True
          TabOrder = 2
          TabStop = True
          OnClick = jrbTypOClick
          LinkedControls = <>
        end
        object jeNazwa: TJvEdit
          Left = 364
          Top = 8
          Width = 245
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 3
          Text = 'SZUKAJ PO NAZWIE KONTRAHENTA'
          OnChange = jeNazwaChange
          OnClick = jeNazwaDblClick
        end
        object JvEdit1: TJvEdit
          Left = 617
          Top = 8
          Width = 121
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 4
          Text = 'SZUKAJ PO NUMERZE NIP'
          OnChange = jeNIPChange
          OnClick = jeNIPDblClick
        end
        object jeMiasto: TJvEdit
          Left = 746
          Top = 8
          Width = 121
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 5
          Text = 'WYSZUKAJ MIASTO'
          OnChange = jeMiastoChange
          OnClick = jeMiastoClick
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Zapis obrot'#243'w sklepu sieci Euro'
      ImageIndex = 3
      object JvPanel4: TJvPanel
        Left = 0
        Top = 0
        Width = 1118
        Height = 517
        Align = alClient
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 512
          Top = 160
          Width = 249
          Height = 145
          Caption = 'Zapis obrotu do sklepu'
          TabOrder = 2
          Visible = False
          object DBText1: TDBText
            Left = -32
            Top = -16
            Width = 233
            Height = 17
            DataField = 'Nazwa'
            DataSource = dm.dsSzukajObroty
          end
          object DBText2: TDBText
            Left = 8
            Top = 24
            Width = 233
            Height = 17
            DataField = 'Nazwa'
            DataSource = dm.dsDostawca
          end
          object Label3: TLabel
            Left = 8
            Top = 72
            Width = 97
            Height = 13
            AutoSize = False
          end
          object DBText3: TDBText
            Left = 8
            Top = 96
            Width = 65
            Height = 17
            DataField = 'RETRO'
            DataSource = dm.dsProwizje
          end
          object Label4: TLabel
            Left = 8
            Top = 136
            Width = 95
            Height = 13
            Caption = 'Warto'#347#263' retro klient'
          end
          object Label5: TLabel
            Left = 115
            Top = 136
            Width = 46
            Height = 13
            AutoSize = False
          end
          object Label6: TLabel
            Left = 115
            Top = 160
            Width = 54
            Height = 13
            AutoSize = False
          end
          object Label7: TLabel
            Left = 8
            Top = 160
            Width = 92
            Height = 13
            Caption = 'Warto'#347#263' retro Euro'
          end
          object Button1: TButton
            Left = 8
            Top = 192
            Width = 89
            Height = 25
            Caption = 'Zapisz do bazy'
            TabOrder = 0
            OnClick = Button1Click
          end
        end
        object JvGroupBox2: TJvGroupBox
          Left = 280
          Top = 176
          Width = 513
          Height = 153
          Caption = 'Kartoteka sklep'#243'w'
          TabOrder = 1
          Visible = False
          object dbgDoObrotu: TJvDBUltimGrid
            Left = 2
            Top = 15
            Width = 509
            Height = 136
            Align = alClient
            DataSource = dm.dsSzukajObroty
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Visible = False
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 17
            TitleRowHeight = 17
          end
        end
        object JvGroupBox1: TJvGroupBox
          Left = 176
          Top = 64
          Width = 673
          Height = 321
          Caption = 'Wybierz dostawc'#281
          TabOrder = 0
          Visible = False
          object Label1: TLabel
            Left = 16
            Top = 32
            Width = 126
            Height = 13
            Caption = 'Wprowad'#378' naz'#281' dostawcy'
          end
          object Label2: TLabel
            Left = 216
            Top = 88
            Width = 91
            Height = 13
            Caption = 'Prowizjie dostawcy'
          end
          object JvLabel2: TJvLabel
            Left = 216
            Top = 32
            Width = 117
            Height = 13
            Caption = 'Wybierz miesi'#261'c importu'
          end
          object JvLabel3: TJvLabel
            Left = 376
            Top = 32
            Width = 119
            Height = 13
            Caption = 'Wybierz kwarta'#322' importu'
          end
          object Label18: TLabel
            Left = 528
            Top = 32
            Width = 96
            Height = 13
            Caption = 'Wybierz rok importu'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object JvImgBtn1: TJvImgBtn
            Left = 640
            Top = 192
            Width = 97
            Height = 33
            Caption = 'Wprowad'#378' obr'#243't r'#281'cznie'
            TabOrder = 3
            Visible = False
            OnClick = JvImgBtn1Click
            Flat = True
            Images = JvImageList1
            ImageIndex = 0
          end
          object Edit1: TEdit
            Left = 16
            Top = 56
            Width = 193
            Height = 21
            TabOrder = 0
            OnChange = Edit1Change
          end
          object DBGrid1: TDBGrid
            Left = 16
            Top = 88
            Width = 193
            Height = 217
            Hint = 'Kliknij wybranego kontrahenta '#380'eby wy'#347'wietli'#263' jego retra'
            DataSource = dm.dsDostawca
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDblClick = DBGrid1DblClick
            OnEnter = DBGrid1Enter
            Columns = <
              item
                Expanded = False
                FieldName = 'NAZWA'
                Title.Caption = 'Nazwa dostawcy'
                Visible = True
              end>
          end
          object DBGrid2: TDBGrid
            Left = 216
            Top = 104
            Width = 441
            Height = 201
            Hint = 
              'Kliknij wiersz retra '#380'eby uruchomi'#263' import obrot'#243'w.'#13#10'Sprawd'#378', cz' +
              'y wybra'#322'e'#347' odpowiedni miesi'#261'c lub kwarta'#322'.'
            DataSource = dm.dsProwizje
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnCellClick = DBGrid2CellClick
            Columns = <
              item
                Expanded = False
                FieldName = 'NR_PROWIZJE'
                Visible = False
              end
              item
                Expanded = False
                FieldName = 'NR_KONTRAH'
                Visible = False
              end
              item
                Expanded = False
                FieldName = 'RETRO'
                Title.Caption = 'Retro Euro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OKRES'
                Title.Caption = 'Okres retra'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RETRO_KLIENT'
                Title.Caption = 'Retro sklep'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OPIS'
                Title.Caption = 'Opis retra'
                Visible = True
              end>
          end
          object cMiesiac: TJvComboBox
            Left = 216
            Top = 56
            Width = 145
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            Items.Strings = (
              'stycze'#324
              'luty'
              'marzec'
              'kwiecie'#324
              'maj'
              'czerwiec'
              'lipiec'
              'sierpie'#324
              'wrzesie'#324
              'pa'#378'dziernik'
              'listopad'
              'grudzie'#324)
          end
          object cKwartal: TJvComboBox
            Left = 376
            Top = 56
            Width = 137
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            Items.Strings = (
              'I kwarta'#322
              'II kwarta'#322
              'III kwarta'#322
              'IV kwarta'#322)
          end
          object cRok: TComboBox
            Left = 528
            Top = 56
            Width = 129
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ItemHeight = 16
            ParentFont = False
            TabOrder = 6
            Items.Strings = (
              '2016'
              '2017'
              '2018')
          end
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Karta klienta'
      ImageIndex = 4
      TabVisible = False
      object GroupBox2: TGroupBox
        Left = 8
        Top = 16
        Width = 1105
        Height = 497
        Caption = 'Karta klienta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object JvNetscapeSplitter1: TJvNetscapeSplitter
          Left = 345
          Top = 18
          Height = 477
          Align = alLeft
          Maximized = False
          Minimized = False
          ButtonCursor = crDefault
        end
        object Panel3: TPanel
          Left = 2
          Top = 18
          Width = 343
          Height = 477
          Align = alLeft
          TabOrder = 0
          object PC_KK: TPageControlEx
            Left = 1
            Top = 1
            Width = 341
            Height = 475
            ActivePage = TabSheet8
            Align = alClient
            TabOrder = 0
            HideHeader = False
            object TabSheet8: TTabSheet
              Caption = 'Wyszukaj kontrachenta'
              object Label17: TLabel
                Left = 224
                Top = 354
                Width = 54
                Height = 16
                Caption = 'Kategoria'
              end
              object Label16: TLabel
                Left = 120
                Top = 354
                Width = 79
                Height = 16
                Caption = 'Metra'#380' sklepu'
              end
              object Label15: TLabel
                Left = 8
                Top = 354
                Width = 51
                Height = 16
                Caption = 'Koncesja'
              end
              object Label14: TLabel
                Left = 8
                Top = 314
                Width = 43
                Height = 16
                Caption = 'Telefon'
              end
              object Label13: TLabel
                Left = 96
                Top = 266
                Width = 37
                Height = 16
                Caption = 'Miasto'
              end
              object Label12: TLabel
                Left = 8
                Top = 266
                Width = 78
                Height = 16
                Caption = 'Kod pocztowy'
              end
              object Label11: TLabel
                Left = 8
                Top = 218
                Width = 50
                Height = 16
                Caption = 'Ulica i nr'
              end
              object Label9: TLabel
                Left = 8
                Top = 178
                Width = 88
                Height = 16
                Caption = 'Imi'#281' i nazwisko'
              end
              object Label8: TLabel
                Left = 8
                Top = 138
                Width = 71
                Height = 16
                Caption = 'Nazwa firmy'
              end
              object Label10: TLabel
                Left = 208
                Top = 178
                Width = 19
                Height = 16
                Caption = 'NIP'
              end
              object e10: TJvEdit
                Left = 222
                Top = 370
                Width = 91
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                OnChange = e10Change
              end
              object e05: TJvEdit
                Left = 118
                Top = 370
                Width = 91
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                OnChange = e05Change
              end
              object e04: TJvEdit
                Left = 8
                Top = 370
                Width = 97
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                OnChange = e04Change
              end
              object e09: TJvEdit
                Left = 6
                Top = 330
                Width = 121
                Height = 21
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                OnChange = e09Change
              end
              object e07: TJvEdit
                Left = 96
                Top = 282
                Width = 233
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                OnChange = e07Change
              end
              object e02: TJvEdit
                Left = 8
                Top = 282
                Width = 81
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                OnChange = e02Change
              end
              object e03: TJvEdit
                Left = 8
                Top = 234
                Width = 321
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
                OnChange = e03Change
              end
              object e06: TJvEdit
                Left = 206
                Top = 194
                Width = 121
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                OnChange = e06Change
              end
              object e01: TJvEdit
                Left = 8
                Top = 194
                Width = 121
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                OnChange = e01Change
              end
              object e08: TJvEdit
                Left = 6
                Top = 154
                Width = 323
                Height = 21
                AutoSize = False
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clDefault
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 9
                OnChange = e08Change
              end
              object rgTyp: TJvRadioGroup
                Left = 8
                Top = 56
                Width = 321
                Height = 73
                Caption = 'Typ kontrahenta'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Wszyscy'
                  'Odbiorca'
                  'Dostawca')
                TabOrder = 10
                OnClick = rgTypClick
              end
              object JvBitBtn1: TJvBitBtn
                Left = 256
                Top = 24
                Width = 75
                Height = 25
                Caption = 'Czy'#347#263' filtry'
                TabOrder = 11
                OnClick = JvBitBtn1Click
              end
            end
          end
        end
        object Panel2: TPanel
          Left = 355
          Top = 18
          Width = 748
          Height = 477
          Align = alClient
          TabOrder = 1
          object JvToolBar4: TJvToolBar
            Left = 1
            Top = 1
            Width = 746
            Height = 29
            Caption = 'JvToolBar4'
            TabOrder = 0
          end
          object jPC: TPageControlEx
            Left = 1
            Top = 30
            Width = 746
            Height = 446
            ActivePage = TabSheet6
            Align = alClient
            TabOrder = 1
            HideHeader = True
            object TabSheet6: TTabSheet
              Caption = 'TabSheet6'
              object dbGridPokazK: TJvDBUltimGrid
                Left = 0
                Top = 0
                Width = 746
                Height = 444
                Align = alClient
                DataSource = dm.dsPokaszKontrah
                PopupMenu = pmKontrah2
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -13
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnCellClick = dbGridPokazKCellClick
                OnEnter = dbGridPokazKEnter
                SelectColumnsDialogStrings.Caption = 'Select columns'
                SelectColumnsDialogStrings.OK = '&OK'
                SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
                EditControls = <>
                RowsHeight = 20
                TitleRowHeight = 20
              end
            end
            object TabSheet7: TTabSheet
              Caption = 'TabSheet7'
              ImageIndex = 1
              object JvToolBar5: TJvToolBar
                Left = 0
                Top = 0
                Width = 746
                Height = 29
                Caption = 'JvToolBar5'
                TabOrder = 0
                object JvDBNavigator5: TJvDBNavigator
                  Left = 0
                  Top = 2
                  Width = 240
                  Height = 22
                  DataSource = dm.dsKartaK
                  Flat = True
                  TabOrder = 0
                end
              end
              object jvdbKartaObrotow: TJvDBGrid
                Left = 0
                Top = 29
                Width = 746
                Height = 415
                Align = alClient
                DataSource = dm.dsKartaK
                PopupMenu = pmKontrah2
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -13
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnDrawDataCell = jvdbKartaObrotowDrawDataCell
                SelectColumnsDialogStrings.Caption = 'Select columns'
                SelectColumnsDialogStrings.OK = '&OK'
                SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
                EditControls = <>
                RowsHeight = 20
                TitleRowHeight = 20
              end
            end
          end
        end
      end
    end
    object KartaDostawcy: TTabSheet
      Caption = 'Karta dostawcy'
      ImageIndex = 5
      TabVisible = False
      OnShow = KartaDostawcyShow
      object JvToolBar6: TJvToolBar
        Left = 0
        Top = 0
        Width = 1118
        Height = 29
        ButtonHeight = 21
        Caption = 'JvToolBar6'
        TabOrder = 0
        object ToolButton15: TToolButton
          Left = 0
          Top = 2
          Width = 8
          Caption = 'ToolButton15'
          Style = tbsSeparator
        end
        object JvDBNavigator6: TJvDBNavigator
          Left = 8
          Top = 2
          Width = 240
          Height = 21
          DataSource = dsDostawcy
          TabOrder = 0
        end
        object ToolButton16: TToolButton
          Left = 248
          Top = 2
          Width = 8
          Caption = 'ToolButton16'
          ImageIndex = 0
          Style = tbsSeparator
        end
        object ToolButton18: TToolButton
          Left = 256
          Top = 2
          Width = 8
          Caption = 'ToolButton18'
          ImageIndex = 2
          Style = tbsSeparator
        end
        object ToolButton19: TToolButton
          Left = 264
          Top = 2
          Width = 8
          Caption = 'ToolButton19'
          ImageIndex = 4
          Style = tbsSeparator
        end
        object eSzukajN: TEdit
          Left = 272
          Top = 2
          Width = 263
          Height = 21
          Hint = 'Szukaj po nazwie'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnChange = eSzukajNChange
        end
        object ToolButton17: TToolButton
          Left = 535
          Top = 2
          Width = 8
          Caption = 'ToolButton17'
          ImageIndex = 3
          Style = tbsSeparator
        end
        object eSzukajNIP: TEdit
          Left = 543
          Top = 2
          Width = 121
          Height = 21
          Hint = 'Szukaj po NIP'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnChange = eSzukajNIPChange
        end
      end
      object JvDBGrid2: TJvDBGrid
        Left = 0
        Top = 29
        Width = 1118
        Height = 488
        Align = alClient
        DataSource = dsDostawcy
        PopupMenu = pmDostawca
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Visible = False
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'NR_KONTRAH'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'RODZAJ'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'IMIE_NAZWISKO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NAZWA'
            Title.Caption = 'Nazwa firmy'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NIP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ULICA'
            Title.Caption = 'Ulica'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MIASTO'
            Title.Caption = 'Miasto'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KOD_P'
            Title.Caption = 'Kod pocztowy'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_PRZYSTAPIENIA'
            Title.Caption = 'Data'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TELEFON'
            Title.Caption = 'Telefon'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KONCESJA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'METRAZ'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'KATEGORIA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'UWAGI'
            Width = 300
            Visible = True
          end>
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'TabSheet9'
      ImageIndex = 6
      TabVisible = False
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 1118
        Height = 517
        Align = alClient
        DataSource = DataSource1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Visible = False
      end
    end
  end
  object mm: TMainMenu
    Images = JvImageList1
    Left = 1072
    Top = 8
    object Plik1: TMenuItem
      Caption = 'Plik'
      object Importsprzeday1: TMenuItem
        Caption = 'Import sprzeda'#380'y'
        ImageIndex = 7
        object ZplikuExcel1: TMenuItem
          Caption = 'Z pliku Excel'
          Visible = False
        end
        object Zplikutxt1: TMenuItem
          Caption = 'Z pliku csv'
          ImageIndex = 8
          OnClick = Zplikutxt1Click
        end
      end
      object Importy1: TMenuItem
        Caption = 'Importy'
        Visible = False
        object KontorahenciEur1: TMenuItem
          Caption = 'Kontorahenci Euro'
          OnClick = KontorahenciEur1Click
        end
        object DostawcyEuro1: TMenuItem
          Caption = 'Dostawcy Euro'
        end
      end
      object Po1: TMenuItem
        Caption = 'Po'#322#261'cz z baz'#261' Wapro'
        Visible = False
      end
      object Kartoteki1: TMenuItem
        Caption = 'Kartoteki'
        ImageIndex = 2
        object Dostawcy1: TMenuItem
          Caption = 'Kontrahenci'
          ImageIndex = 16
          OnClick = Dostawcy1Click
        end
        object Prowizje1: TMenuItem
          Caption = 'Prowizje'
          ImageIndex = 22
          OnClick = Prowizje1Click
        end
        object Pracownicy1: TMenuItem
          Caption = 'Pracownicy'
          ImageIndex = 36
          OnClick = Pracownicy1Click
        end
        object rasy1: TMenuItem
          Caption = 'Trasy'
          ImageIndex = 35
          OnClick = rasy1Click
        end
        object Grupy1: TMenuItem
          Caption = 'Grupy'
          ImageIndex = 37
          Visible = False
          OnClick = Grupy1Click
        end
        object Aktywnidostawcy1: TMenuItem
          Caption = 'Aktywni dostawcy'
          ImageIndex = 37
          OnClick = Aktywnidostawcy1Click
        end
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Koniec2: TMenuItem
        Caption = 'Koniec'
        ImageIndex = 32
        OnClick = Koniec2Click
      end
    end
    object Raporty1: TMenuItem
      Caption = 'Raporty'
      OnClick = Raporty1Click
      object Wydruksesjiimportu1: TMenuItem
        Caption = 'Wydruk sesji importu'
        OnClick = Wydruksesjiimportu1Click
      end
      object Wydrukklientwdane1: TMenuItem
        Caption = 'Wydruk klient'#243'w - dane'
      end
      object Wydrukklientwzobrotami1: TMenuItem
        Caption = 'Wydruk klient'#243'w z obrotami'
        object wszystkie1: TMenuItem
          Caption = 'wszystkie'
          Checked = True
          RadioItem = True
        end
        object stycze1: TMenuItem
          Caption = 'stycze'#324
        end
        object luty1: TMenuItem
          Caption = 'luty'
        end
        object marzec1: TMenuItem
          Caption = 'marzec'
        end
        object kwiecie1: TMenuItem
          Caption = 'kwiecie'#324
        end
        object maj1: TMenuItem
          Caption = 'maj'
        end
        object czerwiec1: TMenuItem
          Caption = 'czerwiec'
        end
        object lipiec1: TMenuItem
          Caption = 'lipiec'
        end
        object sierpie1: TMenuItem
          Caption = 'sierpie'#324
        end
        object wrzesie1: TMenuItem
          Caption = 'wrzesie'#324
        end
        object padziernik1: TMenuItem
          Caption = 'pa'#378'dziernik'
        end
        object listopad1: TMenuItem
          Caption = 'listopad'
        end
        object grudzie1: TMenuItem
          Caption = 'grudzie'#324
        end
      end
      object Wydrukklientwbezobrotw1: TMenuItem
        Caption = 'Wydruk klient'#243'w bez obrot'#243'w'
        object wszystkie2: TMenuItem
          Caption = 'wszystkie'
          Checked = True
          RadioItem = True
        end
        object stycze2: TMenuItem
          Caption = 'stycze'#324
        end
        object luty2: TMenuItem
          Caption = 'luty'
        end
        object marzec2: TMenuItem
          Caption = 'marzec'
        end
        object kwiecie2: TMenuItem
          Caption = 'kwiecie'#324
        end
        object maj2: TMenuItem
          Caption = 'maj'
        end
        object czerwiec2: TMenuItem
          Caption = 'czerwiec'
        end
        object lipiec2: TMenuItem
          Caption = 'lipiec'
        end
        object sierpie2: TMenuItem
          Caption = 'sierpie'#324
        end
        object wrzesie2: TMenuItem
          Caption = 'wrzesie'#324
        end
        object padziernik2: TMenuItem
          Caption = 'pa'#378'dziernik'
        end
        object listopad2: TMenuItem
          Caption = 'listopad'
        end
        object grudzie2: TMenuItem
          Caption = 'grudzie'#324
        end
      end
      object Wydrukdostawcwzobrotami1: TMenuItem
        Caption = 'Wydruk dostawc'#243'w z obrotami'
        object wszyscy1: TMenuItem
          Caption = 'wszystkie'
          Checked = True
          RadioItem = True
        end
        object stycze5: TMenuItem
          Caption = 'stycze'#324' + I kwarta'#322
        end
        object luty3: TMenuItem
          Caption = 'luty + I kwarta'#322
        end
        object marzec3: TMenuItem
          Caption = 'marzec + I kwarta'#322
        end
        object kwiecie3: TMenuItem
          Caption = 'kwiecie'#324' + II kwarta'#322
        end
        object maj3: TMenuItem
          Caption = 'maj + II kwarta'#322
        end
        object czerwiec3: TMenuItem
          Caption = 'czerwiec + II kwarta'#322
        end
        object lipiec3: TMenuItem
          Caption = 'lipiec + III kwarta'#322
        end
        object sierpie3: TMenuItem
          Caption = 'sierpie'#324' + III kwarta'#322
        end
        object wrzesie3: TMenuItem
          Caption = 'wrzesie'#324' + III kwarta'#322
        end
        object padziernik3: TMenuItem
          Caption = 'pa'#378'dziernik + IV kwarta'#322
        end
        object listopad3: TMenuItem
          Caption = 'listopad + IV kwarta'#322
        end
        object grudzie3: TMenuItem
          Caption = 'grudzie'#324' + IV kwarta'#322
        end
      end
    end
    object Administracja1: TMenuItem
      Caption = 'Administracja'
      object KonfiguracjapoczeniazbazWapro1: TMenuItem
        Caption = 'Konfiguracja po'#322#261'czenia z baz'#261' Wapro'
        Visible = False
      end
      object Pokalistimportw1: TMenuItem
        Caption = 'Poka'#380' list'#281' import'#243'w'
        OnClick = Pokalistimportw1Click
      end
      object Ustawieniezakadek1: TMenuItem
        Caption = 'Ustawienie zak'#322'adek'
        Visible = False
        object Pierwsza1: TMenuItem
          Caption = 'Pierwsza'
          Checked = True
          OnClick = Pierwsza1Click
        end
        object Druga1: TMenuItem
          Caption = 'Druga'
          Checked = True
        end
      end
      object Czyszczenietabel1: TMenuItem
        Caption = 'Czyszczenie tabel'
        object KasowanietabeliImport1: TMenuItem
          Caption = 'Kasowanie tabeli Import'
          OnClick = KasowanietabeliImport1Click
        end
        object KasowanietabeliObroty1: TMenuItem
          Caption = 'Kasowanie tabeli Obroty'
          OnClick = KasowanietabeliObroty1Click
        end
      end
      object Poczkontrahentw1: TMenuItem
        Caption = 'Po'#322#261'cz kontrahent'#243'w'
        OnClick = Poczkontrahentw1Click
      end
      object Wczprzyciski1: TMenuItem
        Caption = 'W'#322#261'cz przyciski'
        OnClick = Wczprzyciski1Click
      end
      object Aktualizacja1: TMenuItem
        Caption = 'Aktualizacja'
        OnClick = Aktualizacja1Click
      end
    end
    object Koniec1: TMenuItem
      Caption = 'Koniec'
      object Oprogramie1: TMenuItem
        Caption = 'O programie'
      end
      object Koniec3: TMenuItem
        Caption = 'Koniec'
        ImageIndex = 32
        OnClick = Koniec3Click
      end
    end
  end
  object img: TImageList
    ShareImages = True
    Left = 1032
    Top = 8
    Bitmap = {
      494C010114001800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000006000000001002000000000000060
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFDCDCDCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCC
      CCFFCCCCCCFFCCCCCCFFCCCCCCFFCFCFCFFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFF0F0F0FFE9E9E9FFE9E9
      E9FFF0F0F0FF000000FF000000FF000000FF00000000F8F8F8FFE6E6E6FFA2DC
      E9FF9EDBE7FF7BAEE8FF96D6E5FF95D7E7FF90D6E7FF8BD4E6FF87D2E5FF85D2
      E6FF7ECDE3FF70C1DAFF0000000000000000FAFAFAFFF4F4F4FFE2E2E2FF9FD9
      E5FF9CD8E3FF79ABE4FF94D3E1FF93D4E3FF8ED3E3FF89D1E2FF85CFE1FF83CF
      E2FF7DCBE0FF6FBFD7FF54ABCAFF3590B7FF000000FF000000FF000000FF0000
      00FF000000FF000000FFBF9A52FFB78218FFB68114FFB88010FFB9810FFFB87F
      0EFFB67E0FFFB68013FFB78218FFBA8927FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF1C945FFF008747FF0087
      47FF1C945FFF000000FF000000FF000000FF00000000252525FF2B2B2BFF3131
      31FF333333FF333333FF333333FF333333FF333333FF333333FF333333FF3333
      33FF2F2F2FFF233236FF6DBED8FF000000004F4F4FFF2F2F2FFF333333FF3333
      33FF333333FF333333FF333333FF333333FF333333FF333333FF313131FF2B2B
      2BFF282828FF2C3132FF3B5056FF539CB5FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB78218FFF6CD8BFFF1C275FFF9FCFFFF8B8D91FFF9F9
      F9FFF4F8FCFFF0C174FFF6CD8BFFB78218FFD0D0D0FFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFF00874AFF00D99AFF00D9
      9AFF00874AFFBCBCBCFFBDBDBDFFEFEFEFFF00000000919191FF393939FF4747
      47FF4A4A4AFF494949FF4A4A4AFF4A4A4AFF494949FF4A4A4AFF4A4A4AFF4949
      49FF424242FF232323FF0000000000000000232323FF424242FF494949FF4A4A
      4AFF4A4A4AFF494949FF4A4A4AFF4A4A4AFF494949FF4A4A4AFF474747FF3939
      39FF444444FF3A3A3AFF4B5F64FF7BCCE4FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB68116FFF3CB8BFFEBB65CFFF2EDEDFF7F7978FFF1E9
      E2FFEEE9E9FFEAB55BFFF3CB8BFFB68116FFB67E0EFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00823DFF00CD99FF00CD
      99FF00823DFFFFFFFFFFFFFFFFFFE9E9E9FF0000000000000000424242FF5050
      50FF4D4D4DFF4D4D4DFF4F4F4FFF4E4E4EFF4E4E4EFF4E4E4EFF4D4D4DFF4E4E
      4EFF555555FF313D40FF00000000000000005A5A5AFF545454FF4E4E4EFF4E4E
      4EFF4D4D4DFF4E4E4EFF4F4F4FFF4E4E4EFF4D4D4DFF4E4E4EFF505050FF4242
      42FF515151FF45494AFF55686DFF00000000000000FF000000FF000000FF0000
      00FF000000FF000000FFB68115FFF3CE92FFE6AD4FFFEACFA9FFFFFFFFFFFDFF
      FFFFE8CEA7FFE6AD4EFFF3CE92FFB68115FFB47A07FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00AB77FF00C79EFF00C49AFF00C4
      9AFF00C79EFF00AB77FFFFFFFFFFE9E9E9FF0000000000000000C9C4BFFF3736
      36FF595959FF565656FF4D4D4DFF4D4D4DFF4C4C4CFF515151FF5A5A5AFF4A4A
      4AFF5C5C5CFF000000000000000000000000000000004B4B4BFF4A4A4AFF5A5A
      5AFF515151FF4C4C4CFF4D4D4DFF4D4D4DFF565656FF595959FF343434FF4C4C
      4CFF5A5A5AFF404E51FF89C7D6FF00000000000000FF000000FF000000FF0000
      00FF000000FF000000FFB68114FFF4D49EFFE4A641FFE3A43AFFE3A133FFE2A1
      32FFE3A43AFFE3A641FFF4D49EFFB68114FFB47A07FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A371FF00BC98FF00BC
      98FF00A371FFFFFFFFFFFFFFFFFFE9E9E9FF0000000000000000000000000000
      0000000000004F4E4DFF535353FF484848FF4A4A4AFF545454FF6B68AEFF0000
      000000000000000000000000000000000000000000000000000000000000D2CD
      C8FF545454FF4A4A4AFF484848FF535353FF3C3C3CFF505050FF5F5E83FF4C4B
      6CFF00000000000000000000000000000000DCDCDCFFCCCCCCFFCCCCCCFFCCCC
      CCFFCCCCCCFFCCCCCCFFB58013FFF6D9ADFFE19E2EFFE7CAA1FFEBE2E0FFEBE2
      E0FFE7CAA1FFE19E2EFFF6D9ADFFB68014FFB47A07FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF009D70FF009D
      70FFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9FF0000000000000000000000000000
      000000000000000000003E3E3EFF434343FF444444FF797979FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000716F6CFF444444FF434343FF3E3E3EFF434343FF5D5D5DFF7C7C7CFF0000
      000000000000000000000000000000000000BF9A51FFB68114FFB57F11FFB57F
      11FFB57F12FFB68012FFB47E0FFFF8E0BCFFDD9319FFEEE9E9FFEFE6DEFFEFE6
      DEFFEEE9E9FFDD9319FFF8E1BCFFB68013FFB47A07FFFEFFFFFFFDFDFDFFFDFD
      FDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFFFEFEFFFFFFFFFFFFFF
      FFFFFFFEFEFFFDFDFDFFFFFFFFFFE9E9E9FF0000000000000000000000000000
      000000000000CBC6C1FF373737FF3F3F3FFF3C3C3CFF333333FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000323232FF3C3C3CFF3F3F3FFF373737FF373737FF393939FF6F6F6FFFE6E6
      E6FF00000000000000000000000000000000B68113FFFFF1DEFFFFF0DBFFFFF0
      DDFFFFF1DEFFFFF4E2FFB27B0AFFFBEACEFFDA8802FFF0EEECFFB2ADA7FFB2AD
      A7FFF0EEECFFDA8802FFFBEAD0FFB67F12FFB47A07FFF7F7F9FFF7F6F6FFF7F6
      F6FFF7F6F6FFF7F6F6FFF7F6F6FFF7F6F6FFF7F6F6FFF7F6F6FFF7F6F6FFF7F6
      F6FFF7F6F6FFF7F6F6FFFFFFFFFFE9E9E9FF0000000000000000000000000000
      000000000000252525FF353535FF3C3C3CFF3A3A3AFF2F2F2FFF525252FF0000
      000000000000000000000000000000000000000000000000000000000000513A
      25FF2F2F2FFF3A3A3AFF3C3C3CFF353535FF252525FF353535FF3E3E3EFF4D48
      32FF00000000000000000000000000000000B57F10FFFFF7E9FFB07704FFB179
      08FFB17A09FFB37B0BFFB37C0CFFFDEACCFFFBE6C3FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFCE6C3FFFFEBCDFFB68114FFB47A08FFF0F0F1FFF0F0EFFFF0F0
      EFFFF0F0EFFFF0F0EFFFF0F0EFFFF0F0EFFFF0F0EFFFF0F0EFFFF0F0EFFFF0F0
      EFFFF0F0EFFFF0F0EFFFFFFFFFFFE9E9E9FF0000000000000000000000000000
      000000000000262626FF353535FF393939FF373737FF313131FF595959FF0000
      0000000000000000000000000000000000000000000000000000000000005A43
      2DFF313131FF373737FF393939FF353535FF262626FF343434FF414141FF5C5C
      5CFF00000000000000000000000000000000B57E0FFFFFFFFBFFB27907FFEBD1
      8DFFEACF8CFFEBD08DFFC79A3BFFB27C0DFFB27A0AFFB27906FFB27904FFB279
      05FFB27A07FFB37B09FFB37C0BFFB88216FFB47B08FFE9E9E9FFEAE9E8FFEAE9
      E8FFEAE9E8FFEAE9E8FFEAE9E8FFEAE9E8FFEAE9E8FFEAE9E8FFEAE9E8FFEAE9
      E8FFEAE9E8FFEAE9E8FFFFFFFFFFE9E9E9FF0000000000000000000000000000
      000000000000232323FF323232FF353535FF343434FF303030FF383838FF0000
      0000000000000000000000000000000000000000000000000000000000003534
      33FF303030FF343434FF353535FF323232FF232323FF323232FF343434FF5A5A
      5AFF00000000000000000000000000000000B57E0EFFFFFFFFFFB17906FFE8B2
      5EFFE7B15DFFE7B15DFFE8B360FFE9B461FFE9B461FFE9B461FFE9B461FFE9B4
      61FFEBB561FFB37B0AFFFFFFFFFFB67F11FFB47B08FFE1E2E1FFE3E3E1FFE3E3
      E1FFE3E3E1FFE3E3E1FFE3E3E1FFE3E3E1FFE3E3E1FFE3E3E1FFE3E3E1FFE3E3
      E1FFE3E3E1FFE3E3E1FFFFFFFFFFE9E9E9FF0000000000000000000000000000
      000000000000232323FF373737FF313131FF313131FF3E3E3EFF828282FF0000
      0000000000000000000000000000000000000000000000000000000000008585
      85FF3D3D3DFF313131FF313131FF373737FF222222FF373737FF4E4E4EFF4545
      45FF00000000000000000000000000000000B57D0CFFFFFFFFFFB07500FFB178
      04FFB17805FFB17805FFB17906FFB17906FFB27906FFB27906FFB27906FFB279
      06FFB27905FFB07500FFFFFFFFFFB57E0DFFB57B08FFDADAD9FFDCDBD9FFDCDB
      D9FFDCDBD9FFDCDBD9FFDCDBD9FFDCDBD9FFDCDBD9FFDCDBD9FFDCDBD9FFDCDB
      D9FFDCDBD9FFDCDBD8FFFFFFFFFFE9E9E9FF0000000000000000000000000000
      000000000000999999FF414141FF4F4F4FFF4F4F4FFF2B2B2BFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000025292BFF4F4F4FFF4F4F4FFF414141FF474747FF404040FF636363FFA2A2
      A2FF00000000000000000000000000000000B67F0FFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFB67F0FFFB57C0AFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C89F4DFFB67F0FFFB57C0BFFB57C
      0BFFB57C0BFFB57C0BFFB57C0BFFB57C0BFFB57C0BFFB57C0BFFB57C0BFFB57C
      0BFFB57C0BFFB57C0BFFB67F0FFFC89F4DFFB68012FFD99E39FFDAA140FFDAA1
      40FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA1
      40FFDAA140FFDAA03EFFF5DDB8FFE9E9E9FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFB88217FFECCD98FFECCE9AFFECCE
      9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE
      9AFFECCE9AFFECCE9AFFEECF9BFFF2F2F2FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01FEFE
      FE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFE
      FE01FEFEFE01FEFEFE010000000000000000000000FFFCFCFCFFF0F0F0FFA9E6
      F3FFA5E4F1FF80B5F2FF9CDFEFFF99DEEFFF95DDEFFF90DBEEFF8CD9EDFF0000
      00FF000000FF000000FF000000FF000000FF000000FFFBFBFBFFEDEDEDFFA7E3
      F0FFA3E1EEFF7EB3EFFF9ADCECFF98DCEDFF94DCEEFF8FDAEDFF8BD8ECFF0000
      00FF000000FF000000FF000000FF000000FF00000000F9F9F9FFEAEAEAFFA5E0
      EDFFA1DEEBFF7DB1ECFF98D9E9FF97DAEBFF92D9EBFF8DD7EAFF89D5E9FF87D5
      EAFF80D0E7FF71C3DDFF000000000000000000000000F6F6F6097171718E6A6A
      6A956161619E5B5B5BA45B5B5BA45B5B5BA45B5B5BA45B5B5BA49A9A9A65ABAB
      AB54C6C6C639D7D7D728FCFCFC0300000000000000FF575757FF112531FF6D95
      9EFF739DA5FF5B7DA6FF76A4AFFF7DB1BEFF84BECBFF88C8D8FF8AD0E1FF8BD3
      E7FF88D4E9FF000000FF000000FF000000FF000000FF4C4C4CFF081016FF435C
      61FF4D696FFF3D546FFF55767EFF628A94FF6FA0ABFF7AB4C2FF82C4D4FF87CC
      E0FF86D2E6FF000000FF000000FF000000FFDDDDDDFF2E2E2EFF04070AFF1E29
      2BFF222F32FF1B2632FF263538FF2C3E42FF32484CFF375057FF3A585FFF3C5B
      64FF3C5E67FF4D7D8BFF6EC0DBFF00000000000000000000000093DDEFFF8DD9
      EEFF87D6ECFF81D3EAFF7BD0E9FF75CDE7FF63BEDBFF3D9DC2FF69CDE7FFDADA
      DA25FEFEFE01FEFEFE010000000000000000000000FFE1E1E1FF33505DFF2649
      59FF5F8FA1FF8ABAF5FFA8E5F2FFA5E4F2FFA0E3F2FF9CE1F1FF98DFF0FF0000
      00FF000000FF000000FF000000FF000000FF000000FFE0E0E0FF05080AFF000F
      1DFF2C5872FF477CA5FFA6E2EFFFA4E2F0FF9FE2F1FF9BE0F0FF97DEEFFF0000
      00FF000000FF000000FF000000FF000000FF00000000929292FFCECECEFFCECE
      CEFFCECECEFFCECECEFFCECECEFFCECECEFFCECECEFFCECECEFFCECECEFFCECE
      CEFFCECECEFF5A8C9AFF000000000000000000000000000000009CE1F2FF96DE
      F0FF8FDBEEFF89D8EDFF83D4EBFF7ED2E9FF71C9E3FF4EAACCFF8BEDFFFF6CD1
      E9FFD4D4D42B000000000000000000000000000000FF000000FF24343EFF7DCD
      E8FF4789A6FF456D89FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF141D23FF5DA1
      CEFF326B96FF006BADFF4E899EFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF00000000909090FFEAE4DEFFEAE4
      DEFFE9E3DDFFEAE4DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFD7D7D7FF5E8D9AFF0000000000000000000000FF000000FFA5E6F4FF9EE3
      F2FF98DFF1FF92DCEFFF8CD9EDFF85D6ECFF7CCFE7FF59B2D1FF92F4FFFF8FF1
      FFFF67CBE5FFF0F0F00F0000000000000000000000FF000000FF000000FF8BBB
      C7FF50849FFF346385FF23333DFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF09085EFF265D
      87FF17C2FFFF23CAFFFF017AAEFF5691A5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF00000000878787FFEAE4DEFFEAE4
      DEFFE9E3DDFFEAE4DEFFFFFFFFFFADADADFF919191FFFFFFFFFFB0AEE4FF3730
      D5FFD7D7D7FF608D98FF0000000000000000000000FF000000FF9DD3DEFF97D0
      DDFF91CEDBFF8CCBD9FF85C7D8FF80C5D7FF79C0D4FF63AFC7FF4898B5FF3F92
      B2FF3C91B1FF64B7D1ED0000000000000000000000FF000000FF000000FF0000
      00FFAAE7FFFF2E4754FF423830FF1F1F1DFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF548B
      9FFF27A5DAFF31D2FFFF3CD4F8FF1D90B3FF649FB3FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000005A5957FFEAE4DEFFEAE4
      DEFFE9E3DDFFEAE4DEFFFFFFFFFFA4A4A4FF8C8C8CFFFFFFFFFF716DBFFF605D
      A3FFB4B4B4FF618B95FF0000000000000000000000FF000000FFB6EFF9FFB0EC
      F7FFAAE8F5FFA4E5F4FF9DE2F2FF97DFF0FF90DBEFFF86D4EAFF7BCDE5FF73C7
      E1FF69C1DDFF63BEDBFF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF403731FF0A0A0AFF423830FF232423FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF134E6CFF35ACD4FF4CE3FFFF4CD6EEFF379DB3FF75B0C4FF000000FF0000
      00FF000000FF000000FF000000FF000000FF00000000493F3BFFEAE4DEFFEAE4
      DEFFEAE4DEFFEAE4DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFD7D7D7FF628991FF0000000000000000000000FF000000FFBDF2FBFFB8F0
      F9FFB2EDF8FFACEAF6FFA6E6F4FFA0E3F3FF99E0F1FF93DDEFFF8DD9EEFF87D6
      ECFF81D3EAFF79CEE8FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF9AC1F5FF443A33FF0A0A0AFF384A54FF355568FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF1E5B79FF41B2CFFF61EFFFFF4CCADDFF419CADFF85BFD2FF0000
      00FF000000FF000000FF000000FF000000FF00000000565656FFEAE4DEFFEAE4
      DEFFEAE4DEFFE9E3DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFD7D7D7FF63878EFF0000000000000000000000FF000000FFC4F6FCFFBFF3
      FBFFBAF1FAFFB5EEF8FFAFEBF7FFA9E8F5FFA2E4F3FF9CE1F2FF96DEF0FF8FDB
      EEFF89D8EDFF83D4EBFF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFBBE8EDFF6D8D9BFF4C788EFF41372FFF364040FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF2A6886FF49B5CCFF6AF2FEFF33AAC1FF2B7F93FF84B4
      C2FF000000FF000000FF000000FF000000FF00000000514D4CFFCA8547FFCA85
      47FFCA8547FFCA8547FFFFFFFFFF709AC4FF5E81A4FFFFFFFFFFCECECEFFC9B3
      5DFFEFAE00FF63858BFF0000000000000000000000FF000000FFB6E1E5FFB3DF
      E4FFAEDCE3FFAADAE1FFA5D8E1FFA0D5DFFF9AD2DEFF95CFDCFF8FCDDAFF89C9
      D9FF84C6D8FF7EC4D6FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFB2DCE0FF493E37FF0A0A0AFF40372FFF4250
      53FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF377690FF43ACC3FF48CDE0FF394E54FF958E
      89FF90BAC1FF000000FF000000FF000000FF000000004F4845FFD29257FFD292
      57FFD29257FFD29257FFFFFFFFFF7699BCFF6583A1FFFFFFFFFFCFCFCFFFFFFF
      FFFFEFAE00FF638287FF0000000000000000000000FF000000FFCEFBFFFFCBF9
      FEFFC7F8FDFFC3F5FCFFBEF3FBFFB9F0F9FFB3EDF8FFAEEAF6FFA7E7F5FFA1E4
      F3FF9BE1F1FF94DDF0FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFA5CACEFF4B4039FF0A0A0AFF4036
      2EFF53686AFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF36768FFF3E5154FFABA29CFFFFFD
      F7FF444369FF000000FF000000FF000000FF000000006B6B6BFFEAE4DEFFEAE4
      DEFFEAE4DEFFE9E3DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFD7D7D7FF628084FF0000000000000000000000FF000000FFCEFBFFFFCEFB
      FFFFCCFAFFFFC9F8FEFFC5F6FDFFC0F4FBFFBBF1FAFFB6EFF9FFB0ECF7FFAAE8
      F5FFA4E5F4FF9DE2F2FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF99B9BBFF4D413AFF0A0A
      0AFF294E67FF93BFCBFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFB9E1E6FFADA5A0FFE6E6E4FF6D66
      A3FF8575FAFF4351A3FF000000FF000000FF00000000828282FFC4C4C4FFA7A7
      A7FFC4C4C4FFA7A7A7FFC4C4C4FFA7A7A7FFA7A7A7FFA7A7A7FFA7A7A7FFA7A7
      A7FFA7A7A7FF232A54FF0000000000000000000000FF000000FFCEFBFFFFCEFB
      FFFFCEFBFFFFCDFBFFFFCAF9FEFFC7F7FDFFC2F5FCFFBDF2FBFFB8F0F9FFB2ED
      F8FFACEAF6FFA6E6F4FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF88A2A3FFA2CA
      D9FF64A9CFFF597688FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFA3C8CBFF454774FF9787
      FFFFC6B7FAFF42538BFF000000FF000000FF00000000808080FF3E39B6FFBCBC
      BCFF008FB6FFBCBCBCFF008E00FFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFF212A46FF0000000000000000000000FF000000FF131313FF7D6B
      5FFF605375FFB2A69EFF3B3BBBFF786459FF141414FFBBBBBBFFBBBBBBFFB8B8
      B847ABABAB549D9D9D620000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF4A70
      8EFF88B2C8FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF3447
      8FFF6682B6FF000000FF000000FF000000FF00000000ADADADFF35414CFF172D
      42FF131B25FF46467FFF4A445FFF79726BFF61564EFF46467FFF2A2943FF1A23
      47FF33415BFFA0A0A0FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FFF5F5F5FFE5E5
      E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5
      E5FFE5E5E5FFEDEDEDFF000000FF000000FF000000FF000000FFF4F4F4FFE2E2
      E2FFE2E2E2FFE2E2E2FFE2E2E2FFE2E2E2FFE2E2E2FFE2E2E2FFE2E2E2FFE2E2
      E2FFE2E2E2FFEBEBEBFF000000FF000000FF000000000000000000000000E1E1
      E1FFE0E0E0FFDFDFDFFFDFDFDFFFDEDEDEFFDEDEDEFFDFDFDFFFDFDFDFFFE0E0
      E0FFE1E1E1FF0000000000000000000000000000000000000000F3F3F3FFDFDF
      DFFFDEDEDEFFDCDCDCFFDCDCDCFFDBDBDBFFDBDBDBFFDCDCDCFFDCDCDCFFDEDE
      DEFFE0E0E0FF000000000000000000000000000000FF686868FF03070AFF0002
      03FF000103FF000102FF000102FF000102FF000102FF000102FF000103FF0001
      03FF000203FF0F0F0FFF878787FF000000FF000000FF646464FF020507FF0001
      01FF000001FF000001FF000001FF000001FF000001FF000001FF000001FF0000
      01FF000101FF0B0B0BFF848484FF000000FF0000000000000000020507FF0001
      01FF000001FF000000FF000000FF000000FF000000FF000000FF000001FF0000
      01FF000101FF0B0B0BFF000000000000000000000000636363FF020506FF0001
      01FF000001FF000000FF010101FF352C28FF1F1A17FF000000FF000001FF0000
      01FF000101FF0B0B0BFF0000000000000000000000FF000000FF333333FF9E9E
      9EFF404040FF434343FF434343FF434343FF434343FF434343FF434343FF4242
      42FFA7A7A7FF323232FF000000FF000000FF000000FF000000FF1F1F1FFFC6C5
      C5FFDAD9D9FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FF2AFC
      FCFFD1D0D0FF1E1E1EFF000000FF000000FF000000000000000000000000BDBC
      BCFF202076FF1111C1FF1515C5FF1515C5FF1515C5FF1515C5FF0909B9FF1EB5
      B5FFCFCECEFF00000000000000000000000000000000000000001F1F1FFFBCBB
      BBFF483B35FF2B246FFF31295EFF9C8B81FF7C6D64FF1515C3FF4E413BFF306D
      6AFFCDCCCCFF000000000000000000000000000000FF000000FF373737FF7979
      79FF797979FF797979FF797979FF797979FF797979FF797979FF797979FF7979
      79FF797979FF373737FF000000FF000000FF000000FF000000FF202020FF7777
      77FF777777FF777777FF777777FF777777FF777777FF777777FF777777FF7777
      77FF777777FF202020FF000000FF000000FF00000000000000001E1E1EFF1212
      6CFF1212CAFF1616CEFF1616CEFF1616CEFF1616CEFF1616CEFF1616CEFF0A0A
      C2FF565656FF0000000000000000000000000000000000000000000000004F40
      38FFA08C82FF9C887EFF806E65FFA79389FFA9958CFF6A5A51FFA49086FF9F8B
      81FF555454FF000000000000000000000000000000FF000000FF261F1CFFD4D4
      D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4
      D4FFD4D4D4FF261E1BFF000000FF000000FF000000FF000000FF1D1C14FFE1E1
      E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1
      E1FFE1E1E1FF1D1B14FF000000FF000000FF00000000D6D6D6FF070766FF1313
      D4FF1111A4FFA8A8BBFF1414C0FF1717D8FF1717D8FF10109BFF9191B5FF1414
      C2FF0B0BCCFF15140FFF000000000000000000000000000000000A0964FF1917
      C5FFAC958AFFAF988DFFC5AEA3FFD9C2B7FFD0B9AEFFAF988DFFB19A8FFF5946
      3EFF00000000000000000000000000000000000000FF000000FF462E21FFE5E5
      E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5
      E5FFE5E5E5FF462E21FF000000FF000000FF000000FF000000FF545032FF9396
      6BFF93966BFF93966BFF8C8F66FF717453FF717453FF8C8F66FF93966BFF9396
      6BFF93966BFF545032FF000000FF000000FF000000009A9A9AFF1414DEFF1818
      E2FFAFAFC3FFCACACAFF9B9BBDFF1515C9FF1212ABFFCACACAFFCACACAFF1111
      A3FF1818E2FF1212DCFF00000000000000000000000092908EFFA88F84FF927A
      6FFFBDA499FFD9C0B5FFC8AFA4FFC5ACA1FFC8AFA4FFCBB2A7FFC4ABA0FFA991
      86FFA0887DFF6D564CFF0000000000000000000000FF000000FF432714FFDBDB
      DBFFDBDBDBFFDBDBDBFFDBDBDBFFDBDBDBFFDBDBDBFFDBDBDBFFDBDBDBFFDBDB
      DBFFDBDBDBFF4A3324FF000000FF000000FF000000FF000000FF585231FF8E90
      60FF8E9060FF8E9060FF6A6C48FFFFFFFFFFFFFFFFFFF8F8F6FF7B7D53FF8E90
      60FF8E9060FF5A5535FF000000FF000000FF00000000929292FF3838FFFF2222
      F0FF1616D0FFA3A3C6FFD4D4D4FF8484BEFFD4D4D4FFD4D4D4FF1313B0FF1919
      EAFF1919EAFF1616E7FF0000000000000000000000007A6A63FFC1A79BFFBCA2
      96FFCCB2A6FFC8AEA2FF755C52FF8280B3FFA39792FFB9A094FFD0B6AAFFBEA4
      98FFC0A69AFF9E8479FF0000000000000000000000FF000000FF462D15FFDFDF
      DFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDF
      DFFFDFDFDFFF4E3826FF000000FF000000FF000000FF000000FF5F5834FF9C9C
      67FF989865FFBABAA6FF858558FF969663FF75754DFFCDCDBEFFC6C6B6FF9C9C
      67FF9C9C67FF615B39FF000000FF000000FF00000000959595FF3A3AFFFF3A3A
      FFFF3A3AFFFF3737F3FFA2A2DCFFEAEAEAFFE0E0E0FF1414B4FF1B1BEEFF1B1B
      EEFF1B1BEEFF1818EBFF000000000000000000000000000000005046C1FFCCB2
      A6FFD8BEB2FFCDB3A7FF9F9DD3FF00000000000000007B6155FFCAB0A4FFC8AE
      A2FF755C54FF000000000000000000000000000000FF000000FF4A3117FFE3E3
      E3FFE3E3E3FFE3E3E3FFE3E3E3FFE3E3E3FFE3E3E3FFE3E3E3FFE3E3E3FFE3E3
      E3FFE3E3E3FF523E28FF000000FF000000FF000000FF000000FF665E38FFA6A5
      6CFF7C7C51FFFFFFFFFFFFFFFFFF7C7C51FFBEBEA8FF808056FFFCFCFBFFA6A5
      6CFFA6A56CFF68623CFF000000FF000000FF00000000979797FF3E3EFFFF3E3E
      FFFF3E3EFFFF3737D3FFFFFFFFFFFFFFFFFFFFFFFFFFD4D4F6FF3B3BF3FF3E3E
      FFFF3E3EFFFF3B3BFFFF000000000000000000000000908986FF967C70FFD4BC
      AFFFC0A89BFFD2BAADFFC7BBB6FF0000000000000000967C70FFCEB6A9FFCDB5
      A8FFB8A093FF806559FF0000000000000000000000FF000000FF4D3518FFE7E7
      E7FFE7E7E7FFE7E7E7FFE7E7E7FFE7E7E7FFE7E7E7FFE7E7E7FFE7E7E7FFE7E7
      E7FFE7E7E7FF57442BFF000000FF000000FF000000FF000000FF6C633BFFADA9
      70FFADA970FFCAC9B7FFD3D2C3FF827F54FF827F54FFD3D2C3FFCAC9B7FFADA9
      70FFADA970FF6F6740FF000000FF000000FF000000009A9A9AFF4242FFFF4242
      FFFF3737C8FFFFFFFFFFFFFFFFFF3939D3FFDCDCFEFFFFFFFFFFDCDCFEFF3C3C
      E8FF4242FFFF3F3FFFFF00000000000000000000000091847EFFD6C0B3FFE0CA
      BDFFCFB9ACFFD3BDB0FFC7B1A4FF8D7265FF9D8477FFD8C2B5FFB39D90FFDBC5
      B8FFD5BFB2FFA78F82FF0000000000000000000000FF000000FF503A19FFEAEA
      EAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEAEAFFEAEA
      EAFFEAEAEAFF5A492DFF000000FF000000FF000000FF000000FF726A3EFFB1AC
      72FFB1AC72FF9A9563FFF9F9F7FFFFFFFFFFFFFFFFFFF9F9F7FF9A9563FFB1AC
      72FFB1AC72FF756D43FF000000FF000000FF00000000CACACAFF7676FFFF4747
      FFFFA3A3EAFFFFFFFFFF3D3DD3FF4747FFFF4343F3FFE3E3FFFFFFFFFFFF4141
      E8FF5959FFFF494894FF00000000000000000000000000000000907567FF7060
      A2FFE3CFC2FFC8B4A7FFBBA79AFFD5C1B4FFCAB6A9FFAA9689FFDCC8BBFF997F
      71FF846E7DFF705F78FF0000000000000000000000FF000000FF533D1AFFEDED
      EDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDEDEDFFEDED
      EDFFEDEDEDFF5D4D2EFF000000FF000000FF000000FF000000FF776E42FFB5AE
      74FFB5AE74FFB5AE74FFADA66FFF8D875AFF8D875AFFADA66FFFB5AE74FFB5AE
      74FFB5AE74FF797247FF000000FF000000FF0000000000000000564F30FF7B7B
      FFFF4A4AFFFF3F3FC6FF4B4BFFFF4B4BFFFF4B4BFFFF4444E6FF4444E6FF5D5D
      FFFF575799FF746D44FF0000000000000000000000000000000000000000897A
      AAFFEAD8CBFFECDACDFFEBD9CCFFDAC8BBFFDDCBBEFFF2E0D3FFE4D2C5FFBBA5
      97FF00000000000000000000000000000000000000FF000000FF877B64FFFAFA
      FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
      FAFFFAFAFAFF877B64FF000000FF000000FF000000FF000000FF877E5CFFE1E1
      E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1
      E1FFE1E1E1FF877E5CFF000000FF000000FF000000000000000000000000A3A3
      A3FF7F7FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF6161FFFF5F5F
      A8FFD6D6D6FF0000000000000000000000000000000000000000000000009D8E
      85FFEBDACCFFAC9384FF887490FFF1E0D3FFDFCBBEFF7467B6FFEFDED0FFB098
      88FF00000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00001A3047FF8C8CFFFF8D8DFFFF8D8DFFFF8D8DFFFF8D8DFFFF43478CFF2D53
      79FF000000000000000000000000000000000000000000000000000000000000
      000026374AFF000000009488BFFFF3E4D7FFC2AC9DFF00000000545287FF0000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000FAFAFAFFF0F0
      F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0
      F0FFF0F0F0FFF5F5F5FF0000000000000000FEFEFEFFFDFDFDFFF8F8F8FFEEEE
      EEFFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFEEEEEEFFEEEE
      EEFFEEEEEEFFF3F3F3FFFDFDFDFF000000000000000000000000F7F7F7FFEBEB
      EBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEB
      EBFFEBEBEBFFF1F1F1FF0000000000000000000000FF000000FFF6F6F6FFE8E8
      E8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8E8FFE8E8
      E8FFE8E8E8FFEFEFEFFF000000FF000000FF00000000D5D5D5FF122B40FF0232
      5AFF023056FF023056FF023056FF023056FF023056FF023056FF023056FF0230
      57FF02335DFF505050FFE6E6E6FF00000000DFDFDFFF747474FF091620FF011B
      31FF01203AFF011A2FFF01182BFF01182BFF01182BFF01182BFF011E36FF011E
      37FF011A2FFF282828FF909090FFFDFDFDFF00000000707070FF060F16FF000A
      13FF000C15FF000A11FF000910FF000910FF000910FF000910FF000B14FF000B
      14FF000A12FF1D1D1DFF8D8D8DFF00000000000000FF6C6C6CFF040A0FFF0004
      07FF000408FF000406FF000306FF000306FF000306FF000306FF000407FF0004
      07FF000407FF151515FF8A8A8AFF000000FF0000000000000000043661FF99A3
      ACFFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3
      C3FF2F6296FFF5F5F5FF0000000000000000FEFEFEFFE2E2E2FFE6E6E6FF989A
      9CFFC1C1C1FFA2A2A2FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFA8A8A8FFA8A8
      A8FFE6E6E6FFE5E5E5FFBEBEBEFF0000000000000000000000008B8B8BFF6057
      57FF483F3FFF8C7878FF837272FF2D2727FF2F2929FF302B2BFF948585FF2AFC
      FCFF8D8080FF8A8A8AFF0000000000000000000000FF000000FF545454FFD9C2
      C2FFECD7D7FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0E0FF2AFC
      FCFFE4CDCDFF535353FF000000FF000000FF0000000000000000073865FFFAFA
      FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
      FAFF2D5E8DFF00000000000000000000000000000000E0E0E0FFE3E3E3FFB2B2
      B2FFBBBBBBFFB2B2B2FFE2E2E2FFDFDFDFFFE0E0E0FFE4E4E4FFACACACFFACAC
      ACFFE4E4E4FFE1E1E1FFBFBFBFFF000000000000000000000000939393FF5451
      51FF545151FF545151FF545151FF545151FF545151FF545151FF545151FF5451
      51FF545151FF929292FF0000000000000000000000FF000000FF555555FF8376
      76FF837676FF837676FF837676FF837676FF837676FF837676FF837676FF8376
      76FF837676FF555555FF000000FF000000FF00000000000000000A3B69FFF6F6
      F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6
      F6FF2F608FFF00000000000000000000000000000000E1E1E1FFE2E2E2FFE5E5
      E5FFE7E7E7FFE5E5E5FFE1E1E1FFDEDEDEFFDFDFDFFFE3E3E3FFE7E7E7FFE7E7
      E7FFE3E3E3FFE1E1E1FFBFBFBFFF0000000000000000000000008E8E8EFF7E7A
      7AFF979292FF9B9797FFA4A0A0FFA9A5A5FFA9A5A5FFA4A0A0FF9B9797FF9792
      92FF7E7A7AFF8D8D8DFF0000000000000000000000FF000000FF3D312CFFEDE0
      E0FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0E0FFEDE0
      E0FFEDE0E0FF3C302BFF000000FF000000FF00000000000000000E3F6FFFF0F0
      F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0
      F0FF356995FF00000000000000000000000000000000E4E4E4FFE3E3E3FFE3E3
      E3FFE4E4E4FFE3E3E3FFE2E2E2FFE1E1E1FFE1E1E1FFE3E3E3FFE4E4E4FFE4E4
      E4FFE3E3E3FFE3E3E3FFBFBFBFFF0000000000000000000000008E8E8EFF9E99
      99FFA5A0A0FFA49F9FFF9E9999FF9E9999FF9E9999FF9E9999FFA49F9FFFA5A0
      A0FF9E9999FF8E8E8EFF0000000000000000000000FF000000FF704A35FFD67A
      58FFD67A58FFD67A58FFD67A58FFD67A58FFD67A58FFD67A58FFD67A58FFD67A
      58FFD67A58FF704A35FF000000FF000000FF0000000000000000134575FFEBEB
      EBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEB
      EBFF3C7099FF00000000000000000000000000000000E8E8E8FFA2734AFF302E
      2DFF31302EFF31302EFF31302EFF302E2DFFA2734AFFB8B8B8FFB0B0B0FFB8B8
      B8FFCFCFCFFFE8E8E8FFBFBFBFFF00000000000000000000000066482EFFA5A0
      A0FFA6A1A1FFA5A0A0FFA5A0A0FFA5A0A0FFA5A0A0FFA5A0A0FFA5A0A0FFA6A1
      A1FFA5A0A0FF929292FF0000000000000000000000FF000000FF6B3E20FFCF7A
      4EFFCF7A4EFFA7623FFFA15E3CFFA15E3CFFA15E3CFFA15E3CFFA3603DFFCF7A
      4EFFCF7A4EFF765139FF000000FF000000FF00000000000000001A4B7DFFE5E5
      E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5
      E5FF4378A0FF00000000000000000000000000000000EDEDEDFF9F7046FF4545
      45FF5F5F5FFF4D4D4DFF5F5F5FFF484848FF9F7046FFBCBCBCFFD3D3D3FFBCBC
      BCFFCCCCCCFFECECECFFBFBFBFFF00000000000000000000000064462CFFADA8
      A8FFA29D9DFFADA8A8FFADA8A8FFADA8A8FFADA8A8FFADA8A8FFADA8A8FFA7A3
      A3FFADA8A8FF949494FF0000000000000000000000FF000000FF704722FFDB8D
      57FFB77649FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA66B
      42FFDB8D57FF7C5A3CFF000000FF000000FF0000000000000000205285FFE0E0
      E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0
      E0FF4A81A5FF00000000000000000000000000000000F0F0F0FF9C6D3EFFE8A1
      5EFF96714EFF525252FF89694BFFE8A15EFF9C6D3EFFB7B7B7FFBFBFBFFFB7B7
      B7FFD7D7D7FFF0F0F0FFBFBFBFFF000000000000000000000000624427FF817E
      7EFFB5B1B1FFAAA5A5FFB2AEAEFFB5B0B0FFB5B0B0FFB5B0B0FFB6B1B1FFB0AC
      ACFF817E7EFF979797FF0000000000000000000000FF000000FF764E24FFE39D
      5CFFE29D5CFFCFB296FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5BBA3FFD896
      58FFE39D5CFF836340FF000000FF000000FF000000000000000024568AFFD8D8
      D8FFD6D6D6FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD5D5D5FFD7D7
      D7FF5086AAFF00000000000000000000000000000000F5F5F5FF986837FFE29A
      53FF2C2621FF3F3F3FFF2C2722FFE29A53FF986837FFBBBBBBFFBBBBBBFFCBCB
      CBFFBBBBBBFFF5F5F5FFBFBFBFFF0000000000000000000000005F4123FFBEB9
      B9FFBEB9B9FFBEB9B9FFB2ADADFFACA7A7FFACA7A7FFB0ACACFFBBB6B6FFBFBA
      BAFFBEB9B9FF9A9A9AFF0000000000000000000000FF000000FF7B5526FFE8AC
      5FFFE8AC5FFFE6AB5EFFD0B593FFE1D1BCFFFFFFFFFFFFFFFFFFD8A058FFE8AC
      5FFFE8AC5FFF8A6C44FF000000FF000000FF0000000000000000285A90FFD0D0
      D0FFACACACFFB3B3B3FFB1B1B1FFA7A7A7FFB5B5B5FFA2A2A2FFB6B6B6FFC2C2
      C2FF548AACFF00000000000000000000000000000000F9F9F9FF946330FFDD94
      48FF1F1F1EFF333333FF222222FFDD9448FF946330FFF8F8F8FFF8F8F8FFF8F8
      F8FFF8F8F8FFF8F8F8FFBFBFBFFF0000000000000000000000005D3E1EFFC5C0
      C0FFCAC5C5FFD0CCCCFFD0CCCCFFD0CCCCFFD0CCCCFFD0CCCCFFC4BFBFFFBCB7
      B7FFC5C0C0FF9C9C9CFF0000000000000000000000FF000000FF7F5D28FFECBA
      61FFECBA61FFECBA61FFECBA61FFE1B15DFFB18B49FFC29950FFECBA61FFECBA
      61FFECBA61FF8F7447FF000000FF000000FF00000000000000002B5D93FFC8C8
      C8FF827E79FF8D8984FF9A9691FFA39F9AFFABA7A2FF94908BFF8B8782FF7C7C
      7CFF5C94B7FF00000000000000000000000000000000FBFBFBFF92602BFFFFA8
      4BFFAC783FFF393939FF9E713EFFFFA84BFF92602BFFC4C4C4FFA8A8A8FFB3B3
      B3FFA8A8A8FFFBFBFBFFBFBFBFFF0000000000000000000000005C3C1BFFAAA5
      A5FFC0BCBCFF5D5D5DFF5D5D5DFF6F6F6FFF6F6F6FFF3F3F3FFFC1BDBDFFC2BD
      BDFFAAA5A5FF9D9D9DFF0000000000000000000000FF000000FF856229FFEEC5
      62FFEEC562FFEEC562FFEEC562FFEEC562FFEEC562FFEEC562FFEEC562FFEEC5
      62FFEEC562FF957A49FF000000FF000000FF000000000000000032659DFF68A4
      C9FFE1DDD8FFCDC9C4FFDAD6D1FFE8E4DFFFE7E3DEFFD7D3CFFFCCC8C3FF3D5F
      73FF6FAFD7FF00000000000000000000000000000000FEFEFEFFFEFEFEFFFEFE
      FEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFE
      FEFFFEFEFEFFFEFEFEFFBFBFBFFF000000000000000000000000C3C3C3FFE7E5
      E5FFE7E5E5FFE7E5E5FFE7E5E5FFE7E5E5FFE7E5E5FFE7E5E5FFE7E5E5FFE7E5
      E5FFE7E5E5FFC3C3C3FF0000000000000000000000FF000000FFAFA082FFE1E1
      E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1E1FFE1E1
      E1FFE1E1E1FFAFA082FF000000FF000000FF00000000000000008DAECCFF3F78
      B1FF305881FF25415EFF808890FF909395FFC5C3C1FF213B54FF274565FF3F74
      AAFF4079B1FF00000000000000000000000000000000CFCFCFFF6A8299FF2F5A
      85FF244261FF1C3146FF60666CFF6C6E70FF949291FF192C3FFF1D344CFF2F57
      7FFF305B85FFBFBFBFFFEFEFEFFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000FAFAFAFF494949FFA6A2A0FF0B0B0BFFEBEBEBFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFEFFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFFCFCFCFFFDFDFDFF00000000000000000000000000000000FDFDFDFFF9F9
      F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9
      F9FFF9F9F9FFFBFBFBFF00000000000000000000000000000000FCFCFCFFF6F6
      F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6
      F6FFF6F6F6FFF9F9F9FF00000000000000000000000000000000FBFBFBFFF3F3
      F3FFF3F3F3FFF3F3F3FFF3F3F3FFF3F3F3FFF3F3F3FFF3F3F3FFF3F3F3FFF3F3
      F3FFF3F3F3FFF7F7F7FF000000000000000000000000F6F6F6FF96A1AAFF0234
      5EFF02345EFF02345EFF02345EFF02345EFF02345EFF02345EFF02345EFF0234
      5EFF02345EFFCACACAFFFAFAFAFF0000000000000000EDEDEDFF586A78FF0232
      5AFF023056FF023056FF023056FF023056FF023056FF023056FF023056FF0230
      57FF02335DFFA0A0A0FFF5F5F5FF0000000000000000E5E5E5FF34495BFF0232
      5AFF023056FF023056FF023056FF023056FF023056FF023056FF023056FF0230
      57FF02335DFF7F7F7FFFF0F0F0FF0000000000000000DDDDDDFF1F364AFF0234
      5EFF02345EFF02345EFF02345EFF02345EFF02345EFF02345EFF02345EFF0234
      5EFF02345EFF656565FFEBEBEBFF000000000000000000000000043661FF3A75
      B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75
      B2FF3772AFFFFDFDFDFF00000000000000000000000000000000043560FF939C
      A6FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7
      B7FF2D5E91FFFBFBFBFF00000000000000000000000000000000043560FF939C
      A6FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7B7FFB7B7
      B7FF2D5E91FFF9F9F9FF00000000000000000000000000000000043661FF3A75
      B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75B2FF3A75
      B2FF3772AFFFF7F7F7FF00000000000000000000000000000000073865FF3B78
      B2FF3B78B2FF3B78B2FF3C79B3FF3D7AB4FF3D7AB4FF3C79B3FF3B78B2FF3B78
      B2FF3875AFFF0000000000000000000000000000000000000000073057FFFAFA
      FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
      FAFFAEBAC6FF0000000000000000000000000000000000000000073057FFFAFA
      FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
      FAFFAEBAC6FF0000000000000000000000000000000000000000073865FF3B78
      B2FF3B78B2FF3B78B2FF3C79B3FF3D7AB4FF3D7AB4FF3C79B3FF3B78B2FF3B78
      B2FF3875AFFF00000000000000000000000000000000000000000A3B69FF3E7B
      B5FF407DB7FF427FB9FF4481BBFF4582BCFF4582BCFF4481BBFF427FB9FF407D
      B7FF3B78B2FF0000000000000000000000000000000000000000083157FFF6F6
      F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6
      F6FFDCE1E5FF0000000000000000000000000000000000000000083157FFF6F6
      F6FFDDDDDDFFCDCDCDFFDDDDDDFFC5C5C5FFDDDDDDFFD5D5D5FFBCBCBCFFE6E6
      E6FFDCE1E5FF00000000000000000000000000000000000000000A3B69FF3E7B
      B5FF407DB7FF427FB9FF4481BBFF4582BCFF4582BCFF4481BBFF427FB9FF407D
      B7FF3B78B2FF00000000000000000000000000000000000000000E3F6FFF4686
      BCFF4989BFFF4B8BC1FF4E8EC4FF4F8FC5FF4F8FC5FF4E8EC4FF4B8BC1FF4989
      BFFF4383B9FF00000000000000000000000000000000000000000C355EFFF0F0
      F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0
      F0FFC2CBD2FF00000000000000000000000000000000000000000C355EFFF0F0
      F0FFC0C0C0FFB8B8B8FFB8B8B8FFB8B8B8FFE0E0E0FFB8B8B8FFC0C0C0FFE0E0
      E0FFC2CBD2FF00000000000000000000000000000000000000000E3F6FFF4686
      BCFF4989BFFF4B8BC1FF4E8EC4FF4F8FC5FF4F8FC5FF4E8EC4FF4B8BC1FF4989
      BFFF4383B9FF0000000000000000000000000000000000000000134575FF4E8E
      C2FF5292C6FF5595C9FF5898CCFF5999CDFF5999CDFF5898CCFF5595C9FF5292
      C6FF4B8BBFFF0000000000000000000000000000000000000000113D66FFEBEB
      EBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEB
      EBFFA7B5C1FF0000000000000000000000000000000000000000113D66FFEBEB
      EBFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFD3D3D3FFBCBCBCFFB4B4B4FFBCBC
      BCFFA7B5C1FF0000000000000000000000000000000000000000134575FF4E8E
      C2FF5292C6FF5595C9FF5898CCFF5999CDFF5999CDFF5898CCFF5595C9FF5292
      C6FF4B8BBFFF00000000000000000000000000000000000000001A4B7DFF5798
      CAFF5B9CCEFF5FA0D2FF61A2D4FF62A3D5FF62A3D5FF61A2D4FF5FA0D2FF5B9C
      CEFF5495C7FF0000000000000000000000000000000000000000174371FFE5E5
      E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5
      E5FF8DA2B2FF0000000000000000000000000000000000000000174371FFE5E5
      E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5E5FFE5E5
      E5FF8DA2B2FF00000000000000000000000000000000000000001A4B7DFF5798
      CAFF5B9CCEFF5FA0D2FF61A2D4FF62A3D5FF62A3D5FF61A2D4FF5FA0D2FF5B9C
      CEFF5495C7FF0000000000000000000000000000000000000000205285FF60A3
      D1FF64A7D5FF68ABD9FF69ACDAFF6AADDBFF6AADDBFF69ACDAFF68ABD9FF64A7
      D5FF5DA0CEFF00000000000000000000000000000000000000001E4C7CFFE0E0
      E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0E0FFE0E0
      E0FF7A96AAFF00000000000000000000000000000000000000001E4C7CFFE0E0
      E0FFAAAAAAFFAFAFAFFF969696FFAFAFAFFF969696FF9B9B9BFF969696FF9B9B
      9BFF7A96AAFF0000000000000000000000000000000000000000205285FF599C
      CAFF5EA1CFFF5FA2D0FF60A3D1FF63A6D4FF63A6D4FF64A7D5FF65A8D6FF61A4
      D2FF5DA0CEFF000000000000000000000000000000000000000024568AFF66AA
      D5FF69ACD7FF6BADD8FF6DAFDAFF6EB0DBFF6EB0DBFF6DAFDAFF6BAED9FF6AAD
      D8FF64A7D3FF0000000000000000000000000000000000000000225284FFD8D8
      D8FFD6D6D6FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD5D5D5FFD7D7
      D7FF6B8FA8FF0000000000000000000000000000000000000000225284FFD8D8
      D8FFD6D6D6FFD4D4D4FFCBCBCBFFD4D4D4FFD4D4D4FFD4D4D4FFD5D5D5FFD7D7
      D7FF6B8FA8FF000000000000000000000000000000000000000024568AFFCFDF
      E9FF6597B7FF97B8CDFFCADAE4FF9ABACFFF9BBBD0FF6FA0C0FF4386B1FF72A3
      C4FF5C9FCBFF0000000000000000000000000000000000000000285A90FF6AAC
      D6FF5A91B4FF6199BEFF6199BDFF5B90B2FF649DC2FF598DADFF639CC1FF67A4
      CBFF69ACD6FF000000000000000000000000000000000000000027588DFFD0D0
      D0FFACACACFFB3B3B3FFB1B1B1FFA7A7A7FFB5B5B5FFA2A2A2FFB6B6B6FFC2C2
      C2FF608DAAFF000000000000000000000000000000000000000027588DFFD0D0
      D0FFACACACFFB3B3B3FFB1B1B1FFA7A7A7FFB5B5B5FFA2A2A2FFB6B6B6FFC2C2
      C2FF608DAAFF0000000000000000000000000000000000000000285A90FFE0E0
      E0FFB9B9B9FFC0C0C0FFBFBFBFFFB3B3B3FFC2C2C2FFB4B4B4FFCDCDCDFFD5D5
      D5FF599CC7FF00000000000000000000000000000000000000002B5D93FF6CAC
      D5FF827E79FF8D8984FF9A9691FFA39F9AFFABA7A2FF94908BFF8B8782FF466D
      87FF6CAED9FF00000000000000000000000000000000000000002B5D92FFC8C8
      C8FF827E79FF8D8984FF9A9691FFA39F9AFFABA7A2FF94908BFF8B8782FF7C7C
      7CFF5E94B6FF00000000000000000000000000000000000000002B5D92FFC8C8
      C8FF827E79FF8D8984FF9A9691FFA39F9AFFABA7A2FF94908BFF8B8782FF7C7C
      7CFF5E94B6FF00000000000000000000000000000000000000002B5D93FFDBDB
      DBFF827E79FF8D8984FF9A9691FFA39F9AFFABA7A2FF94908BFF8B8782FF8989
      89FF64A6D1FF000000000000000000000000000000000000000032659DFF71B2
      D9FFE1DDD8FFCDC9C4FFDAD6D1FFE8E4DFFFE7E3DEFFD7D3CFFFCCC8C3FF456B
      82FF71B3DBFF000000000000000000000000000000000000000032659DFF68A4
      C9FFE1DDD8FFCDC9C4FFDAD6D1FFE8E4DFFFE7E3DEFFD7D3CFFFCCC8C3FF3D5F
      73FF6FAFD7FF000000000000000000000000000000000000000032659DFF68A4
      C9FFE1DDD8FFCDC9C4FFDAD6D1FFE8E4DFFFE7E3DEFFD7D3CFFFCCC8C3FF3D5F
      73FF6FAFD7FF000000000000000000000000000000000000000032659DFF71B2
      D9FFE1DDD8FFCDC9C4FFDAD6D1FFE8E4DFFFE7E3DEFFD7D3CFFFCCC8C3FF456B
      82FF71B3DBFF0000000000000000000000000000000000000000E0E9F1FF3F78
      B1FF305881FF25415EFF808890FF909395FFC5C3C1FF213B54FF274565FF3F74
      AAFF4079B1FF0000000000000000000000000000000000000000C6D6E5FF3F78
      B1FF305881FF25415EFF808890FF909395FFC5C3C1FF213B54FF274565FF3F74
      AAFF4079B1FF0000000000000000000000000000000000000000B0C6DBFF3F78
      B1FF305881FF25415EFF808890FF909395FFC5C3C1FF213B54FF274565FF3F74
      AAFF4079B1FF00000000000000000000000000000000000000009DB9D3FF3F78
      B1FF305881FF25415EFF808890FF909395FFC5C3C1FF213B54FF274565FF3F74
      AAFF4079B1FF0000000000000000000000000000000000000000000000000000
      000000000000FEFEFEFFC7C7C7FFB8B5B3FF878787FFFBFBFBFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFDFF9B9B9BFFAAA6A4FF474747FFF7F7F7FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FCFCFCFF797979FFA7A3A0FF262626FFF3F3F3FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FBFBFBFF5E5E5EFFA6A2A0FF141414FFEFEFEFFF000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000600000000100010000000000000300000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFC00FF8780030000FC00FF87
      80010000FC00000080030000FC000000C0030001FC000000C0078001FC000000
      F81FE00F00000000FC3FF01F00000000F83FF00F00000000F81FE00F00000000
      F81FE00F00000000F81FE00F00000000F81FE00F00000000F83FF00F00000000
      FFFFFFFF00000000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFC003801F801F8003
      8001800780070001C003801F801F8003C007C3FFC1FF8003C003E1FFC0FF8003
      C003F0FFE07F8003C003F87FF03F8003C003F83FF81F8003C003FC1FFC0F8003
      C003FE0FFE078003C003FF07FF078003C003FF83FF038003C003FFC3FF838003
      C003FFE7FFE78003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC003C003E007C007
      80018001C0038003C003C003E007C007C003C003C007E007C003C0038003C00F
      C003C00380038003C003C00380038003C003C0038003C187C003C00380038183
      C003C00380038003C003C0038003C003C003C003C003E00FC003C003E007E00F
      FFFFFFFFF00FF45FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0030001C003C003
      8001000080018001C0030001C003C003C0078001C003C003C0078001C003C003
      C0078001C003C003C0078001C003C003C0078001C003C003C0078001C003C003
      C0078001C003C003C0078001C003C003C0078001C003C003C0078001C003C003
      C0078001FFFFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC003C003C003C003
      8001800180018001C003C003C003C003C007C007C007C007C007C007C007C007
      C007C007C007C007C007C007C007C007C007C007C007C007C007C007C007C007
      C007C007C007C007C007C007C007C007C007C007C007C007C007C007C007C007
      C007C007C007C007F83FF83FF83FF83F00000000000000000000000000000000
      000000000000}
  end
  object odTxt: TOpenDialog
    Filter = 'Pliki tekstowe csv|*.csv|Pliki tekstowe txt|*.txt'
    Left = 993
    Top = 8
  end
  object JvImageList1: TJvImageList
    Items = <>
    Height = 32
    Width = 32
    Left = 952
    Top = 8
    Bitmap = {
      494C010128002C00040020002000FFFFFFFF2110FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000008000000060010000010020000000000000C0
      0200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FFFBFB
      FBFFF2F2F2FFEAEAEAFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFB
      FBFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFCFCFCFFF4F4F4FFECEC
      ECFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFECECECFFF4F4F4FFFCFCFCFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFFAFAFAFFEFEFEFFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFEFEFEFFFFAFAFAFF000000FF000000FF000000FFF2F2
      F2FFD4D4D4FFC0C0C0FFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2
      F2FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFECECECFFD4D4D4FFC2C2
      C2FFBDBDBDFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBDBDBDFFC2C2C2FFD4D4D4FFECECECFFFBFBFBFF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000FAFAFAFFF1F1F1FFEAEAEAFFE9E9
      E9FFEAEAEAFFF1F1F1FFFAFAFAFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFEFEFEFFFCECECEFFBDBD
      BDFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBDBDBDFFCECECEFFEFEFEFFF000000FF000000FF000000FFEAEA
      EAFF808080FF666666FF666666FF666666FF666666FF666666FF666666FF6667
      67FF666767FF666767FF666666FF666666FF666666FF666767FF666767FF6667
      67FF666666FF666666FF666666FF666666FF666666FF666666FF808080FFEAEA
      EAFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF2F2F2FFD1D1D1FFAE825EFFA553
      0DFFA54F07FFA54F07FFA54E07FFA54E07FFA54E07FFA54E07FFA54E07FFA54F
      07FFA54F07FFA5520DFFAE825EFFD1D1D1FFF2F2F2FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000F8F8F8FFE4E4E4FFCCCCCCFFBFBFBFFFBCBC
      BCFFBFBFBFFFCCCCCCFFE4E4E4FFF8F8F8FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFB3B3B1FFAFAF
      ADFFAEAEACFFAEAFADFFAEAFAEFFAEAFAEFFAEAFAEFFAEAFAEFFAEAEACFFAFAF
      ADFFB1B1AEFFB0B0ADFFB0B0ABFFB0B0ABFFB1B0ABFFB1B0ACFFB1B0ACFFB0B0
      ACFFB0B0ADFFB1B1AFFFAFB0AEFFAEAFADFFAEAFAEFFAEAFAEFFAEAFAEFFAEAF
      AEFFAEAEACFFAFB0ADFFB3B3B1FFE9E9E9FF000000FF000000FF000000FFE9E9
      E9FF656565FF5A5858FF565454FF565454FF676767FF565454FF575555FF5755
      55FF575555FF575555FF575454FF626363FF575454FF575555FF575555FF5755
      55FF575555FF565454FF676767FF565454FF565454FF5A5858FF656565FFE9E9
      E9FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFEAEAEAFFB47A4AFFB66620FFD18D
      50FFE3A56AFFE2A569FFE2A569FFE2A569FFE2A569FFE2A569FFE2A569FFE2A5
      69FFE3A56AFFD49255FFB66521FFB37A4AFFEAEAEAFF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000FAFAFAFFE4E4E4FFBBBBBBFF444444FF1E1E1EFF1D1D
      1DFF1E1E1EFF444444FFBBBBBBFFE5E5E5FFFAFAFAFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAFAFADFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFABAAA8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFABABA8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAFAFADFFE9E9E9FF000000FF000000FF000000FFE9E9
      E9FF656565FF676463FF5E5C5CFF5E5C5CFF666767FF5E5C5CFF5F5D5DFF5F5D
      5DFF5F5D5DFF5F5D5DFF5E5C5CFF666767FF5E5C5CFF5F5D5DFF5F5D5DFF5F5D
      5DFF5F5D5DFF5E5C5CFF666767FF5E5C5CFF5E5C5CFF676463FF656565FFE9E9
      E9FF000000FF000000FF000000FF000000FF000000FFFCFCFCFFF4F4F4FFECEC
      ECFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFD8D8D8FFB85F11FFE1A873FFE2A5
      68FFDE9B59FFDD9B59FFDD9B59FFDD9B59FFDD9A58FFDD9B59FFDD9B59FFDD9B
      59FFDD9B59FFE2A468FFE0A874FFB15F16FFE9E9E9FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000F1F1F1FFC3C3C3FF1E1E1EFF5F5F5FFF929292FF9191
      91FF929292FF5F5F5FFF1E1E1EFFC4C4C4FFF1F1F1FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAEAEABFFFFFF
      FFFFFFFFFFFFD0802BFFC57120FFB95F0FFFAE4B00FFA23800FFFFFFFFFFFFFF
      FFFFA8A8A4FFFFFFFFFF1932FEFF0F2EEFFF0027E0FF0020D3FF0016C4FF0007
      B6FFFFFFFFFFA8A9A6FFFFFFFFFFD2822CFFC57120FFB95F0FFFAE4B00FFA238
      00FFFFFFFFFFFFFFFFFFAEAEABFFE9E9E9FF000000FF000000FF000000FFE9E9
      E9FF646464FF716F6FFF636161FF636162FF656565FF646262FF646262FF6462
      62FF646262FF646362FF656666FF656565FF656565FF646362FF646262FF6462
      62FF646262FF646262FF656565FF636162FF636161FF716F6FFF646464FFE9E9
      E9FF000000FF000000FF000000FF000000FFFBFBFBFFECECECFFD4D4D4FFC2C2
      C2FFBDBDBDFFBCBCBCFFBCBCBCFFBCBCBCFFB7B7B7FFC6640FFFEDBF92FFE2A5
      63FFE2A564FFE2A565FFE2A463FFE2A360FFE2A25EFFE2A360FFE2A463FFE2A5
      65FFE2A564FFE2A563FFE9BD91FFB96A23FFE9E9E9FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000EAEAEAFF494949FF5F6060FF878786FF787473FF7672
      71FF787473FF888988FF5F5F5FFF464646FFDFDFDFFFF9F9F9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAEAEABFFFFFF
      FFFFFBFDFDFFFDFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFDFFFFFFFFFF
      FFFFA8A8A5FFFFFFFFFFFFFFFAFFFFFFFAFFFFFFFBFFFFFFFCFFFFFFFDFFFFFF
      FDFFFFFFFFFFA8A8A6FFFFFFFFFFFEFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFEFF
      FFFFFCFEFFFFFFFFFFFFAEAEABFFE9E9E9FF000000FF000000FF000000FFE9E9
      E9FF636363FF7D7A7AFF696765FF6A6867FF6A6867FF6A6867FF6A6867FF6B69
      67FF686666FF646464FF707071FF575656FF70706FFF636464FF686666FF6B69
      67FF6A6867FF6A6867FF6A6867FF6A6867FF696765FF7D7A7AFF636363FFE9E9
      E9FF000000FF000000FF000000FF000000FFF2F2F2FFD1D1D1FF5794C3FF0577
      CFFF0076D1FF0076D1FF0076D1FF0076D4FF0076E2FFCF6B13FFF1CBA7FFE9AE
      6EFFE8AF70FFE8AF70FFE8AC6BFFE9B782FFEEF0F0FFE8B680FFE8AD6CFFE8AF
      70FFE8AF70FFE8AE6EFFEBC39AFFC88344FFE9E9E9FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000E9E9E9FF1E1E1EFF939393FF6A6767FF6A6665FF6B68
      67FF6A6665FF6A6767FF929292FF171717FFC2C2C2FFE3E3E3FFF9F9F9FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAFAFACFFFFFF
      FFFFFAFAFAFFFBFBFBFFFBFCFCFFFBFDFDFFFBFDFDFFFBFCFCFFFBFCFBFFFFFF
      FEFFAAAAA8FFFFFFFEFFFDFDFAFFFDFDFAFFFEFDFAFFFEFDFAFFFEFDFAFFFEFD
      FBFFFFFFFEFFABABA8FFFFFFFFFFFCFDFCFFFBFCFCFFFBFDFDFFFBFDFDFFFBFC
      FCFFFAFBFAFFFFFFFFFFAFAFACFFE9E9E9FF000000FF000000FF000000FFE9E9
      E9FF626262FF888685FF6D6B6AFF6F6D6CFF6F6D6CFF6F6D6CFF706E6DFF6968
      68FF636464FF626262FF8F8F8FFF323232FFA7A7A5FF616161FF636364FF6968
      68FF706E6DFF6F6D6CFF6F6D6CFF6F6D6CFF6D6B6AFF888685FF626262FFE9E9
      E9FF000000FF000000FF000000FF000000FFEAEAEAFF3E8ECAFF037BD1FF0E8B
      D1FF1190D0FF118FD0FF118FD0FF0E90D2FF0090DEFFD87219FFF5D7BCFFEEBA
      7CFFEDB879FFEDB777FFEDB778FFF2F0EDFFEDF2F5FFE7EAEBFFEDB778FFEEB8
      78FFEDB87AFFEEB877FFF1D6BEFFCE8B4EFFEDEDEDFF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000EAEAEAFF1D1D1DFF929292FF5B5858FF5E5C5CFF1516
      16FF5E5C5CFF5A5858FF919191FF242424FF787878FFC2C2C2FFE3E3E3FFF9F9
      F9FF000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFB0B0AEFFCFCF
      CEFFA7A7A5FFA8A9A8FFA9AAA9FFA9AAA9FFA9AAA8FFA8A9A7FFA9A9A7FFABAC
      A9FFAEAEACFFACACA9FFABABA6FFABABA6FFACABA6FFABAAA6FFA9A9A6FFA9A9
      A7FFABACA9FFAEAEACFFACACAAFFA9AAA8FFA9AAA9FFA9AAA9FFA9AAA8FFA8A9
      A7FFA7A7A5FFCFCFCEFFB0B0AEFFE9E9E9FF000000FF000000FF000000FFEAEA
      EAFF626262FF92908FFF737170FF757372FF757372FF767473FF6B6A68FF6363
      64FF616262FF777777FF7C7C7BFF323233FF919190FF737373FF616162FF6363
      64FF6B6A68FF767473FF757372FF757372FF737170FF92908FFF626262FFEAEA
      EAFF000000FF000000FF000000FF000000FFE9E9E9FF0075D1FF2298D5FF0C91
      D3FF0089D0FF0089D0FF0089D0FF0089D2FF0088DCFFD47B2BFFFAD8BCFFF6D5
      B3FFF3BF7FFFF4C284FFF8F3EBFFF4F7F9FFEFF0EEFFE8EAEAFFE5DFD4FFF3C2
      84FFF4C081FFF4D5B4FFF1D8C0FFC97B32FFF6F6F6FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000ECECECFF1D1D1DFF949494FF514F50FF504E4EFF5250
      50FF504E4EFF514F50FF929292FF131313FFBCBCBCFF707070FFC2C2C2FFE3E3
      E3FFF9F9F9FF0000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAFAFADFFFFFF
      FFFFF2F2F4FFF3F7FBFFF5FAFFFFF6FBFFFFF5FAFEFFF3F4F6FFF3F2F3FFF6F5
      F5FFADADAAFFFAF9F5FFFEFBF2FFFFFEF2FFFFFFF3FFFFFCF3FFF8F5F2FFF4F2
      F2FFF6F5F5FFACADABFFF6F7F9FFF5F8FCFFF5FAFFFFF6FBFFFFF5FAFEFFF3F4
      F6FFF1F1F1FFFFFFFFFFAFAFADFFE9E9E9FF000000FF000000FF000000FFF1F1
      F1FF616262FF9F9D9CFF7A7877FF797776FF7B7878FF7C7A79FF666665FF6262
      62FF5C5C5CFFD0D0D0FF494949FF313131FF70706FFFC3C3C1FF5C5C5DFF6262
      62FF666665FF7C7A79FF7B7878FF797776FF7A7877FF9F9D9CFF616262FFF1F1
      F1FF000000FF000000FF000000FF000000FFE9E9E9FF0073CFFF3CA9DCFF008F
      D7FF0090D7FF0091D7FF0090D7FF008ED7FF008CDDFF40889EFFE78D3EFFFADC
      C2FFFBE6D2FFFFF1DFFFFFFFFFFFFEFAF7FFFBF6F1FFF4F0EBFFE9EAE7FFE9DC
      C7FFFBE7D0FFF2DCC8FFD7975AFFD7BDA6FFE7E7E7FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF0000000000000000000000000000
      000000000000F9F9F9FFDFDFDFFF4A4A4AFF5D5D5DFF7A7979FF444343FF3F3D
      3EFF444343FF797979FF5D5C5CFF515151FF747474FFB5B5B5FF6E6E6EFFC2C2
      C2FFE3E3E3FFF9F9F9FF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAEAEACFFFFFF
      FFFFE9EBEDFFD3832CFFC56F1EFFB65607FFA73E00FFECEFF0FFEBEAEAFFEEED
      ECFFABACA8FFF6F3EBFF1B35FFFF072DE7FF0021D3FF000DBCFFF4F1E9FFECEB
      E9FFEEEDECFFAAABA9FFEFF1F3FFD4842EFFC56F1EFFB65607FFA73E00FFECEE
      F0FFE9E8E7FFFFFFFFFFAEAEACFFE9E9E9FF000000FF000000FF000000FFFBFB
      FBFF838383FF7B797AFFA3A19EFF858280FF787776FF7A7978FF717070FF5F5F
      5FFF6C6C6CFFFFFFFFFFBCBCBBFF2E2E2EFFC2C2C0FFFDFDF8FF6B6B6BFF6060
      60FF717070FF7A7978FF787776FF858280FFA3A19EFF7B797AFF838383FFFBFB
      FBFF000000FF000000FF000000FF000000FFE9E9E9FF0E82D4FF52B5E3FF0097
      DDFF0099DDFF0098DDFF0095DDFF1FA5E0FFF0F0EDFF0FA2E7FF3F8FA4FFD988
      39FFF3BA85FFFFFFFDFFBCDDEEFF64A4C8FF5495B8FF3E82A8FF89A9BAFFEFEC
      E8FFE6B98EFFD38A45FFBDAA98FFBBBBBBFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF0000000000000000000000000000
      0000F9F9F9FFE3E3E3FFC2C2C2FF7B7A7AFF1D1B1AFF5D5A59FF949291FF9392
      91FF949291FF5D5A59FF1E1D1CFF787878FFBCBCBCFF6E6E6EFFB5B5B5FF6F6F
      6FFFC8C8C8FFEEEEEEFF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFEAEAEAFFAEAEACFFFFFF
      FFFFE3E3E4FFE4E7EBFFE6EAEFFFE7ECF0FFE6EAEDFFE4E5E6FFE4E3E2FFE7E6
      E5FFA9A9A6FFEBEAE5FFEFECE1FFF3EFE1FFF4F0E2FFF1EDE3FFE8E6E1FFE5E3
      E2FFE7E6E5FFA8A9A7FFE8E8E9FFE5E9ECFFE6EAEFFFE7ECF0FFE6EAEDFFE4E5
      E5FFE2E2E1FFFFFFFFFFAEAEACFFEAEAEAFF000000FF000000FF000000FF0000
      00FFFAFAFAFF6E6F6FFF797978FFB0AEACFF6B6B6AFF626262FF626262FF5A59
      59FFC8C8C8FFFFFFFFFF62605FFF302F2FFF5F5E5DFFFAF9F4FFB9B9B5FF5C5B
      5CFF626262FF626262FF6B6B6AFFB0AEACFF797978FF797A7AFFFAFAFAFF0000
      00FF000000FF000000FF000000FF000000FFECECECFF1385D5FF67C2EAFF009E
      E3FF009EE3FF009DE3FF009DE2FFFFF7F2FFFFF4EDFFEFEBE7FF009DEAFF009B
      E5FF8D967BFFF79740FF9CB9BFFF68A1BEFF4F829CFF376984FF556B72FFED9A
      49FFCD8524FFB67A06FFB17902FFB27A04FFB37A06FFB47B08FFB47B08FFB47A
      07FFB47B09FFB67E0EFFBA964BFFEAEAEAFF000000000000000000000000F9F9
      F9FFE3E3E3FFC2C2C2FF717070FFBBB8B6FF7B746FFF354C57FF160D08FF160E
      09FF160D08FF354C58FF7B746FFFBBB8B5FF706F6EFFBCBCBCFF6E6E6EFFB6B6
      B6FF717171FFE9E9E9FF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFF2F2F2FFAFAFADFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFD0D0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFCFD0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAFAFADFFF2F2F2FF000000FF000000FF000000FF0000
      00FF000000FFFCFCFCFFA9A9A9FF676766FF757574FF636363FF606060FF6E6D
      6DFFFFFFFFFFFFFFFFFF655F5BFF322C29FF645F5BFFFEFAF3FFF6F3EDFF6D6C
      6CFF616161FF636363FF757574FF676766FFA9A9A9FFFCFCFCFF000000FF0000
      00FF000000FF000000FF000000FF000000FFF6F6F6FF0072CFFF6DC4EBFF4BBE
      EDFF00A3E9FF00A5E8FFDEF0F5FFFFF8F3FFF4F0ECFFF4ECE5FFC8DCE1FF00A5
      EDFF00A6F5FF3791BCFF709EB9FF76B3D3FF6CA7C7FF5C97B7FF325F7BFFCCCD
      D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB67E0EFFE9E9E9FF0000000000000000F9F9F9FFE3E3
      E3FFC2C2C2FF6F6E6EFFB8B4B1FF766C64FF0F71C0FF73C7F2FF72C7F3FF74C9
      F5FF77CBF7FF76CAF5FF1072C1FF766C65FFB8B4B1FF6F6E6EFFBCBCBCFF7070
      70FF727272FFE9E9E9FF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFFBFBFBFFC6C6C5FFB0B0
      ADFFAEAEACFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEACFFAFAF
      ADFFB0B0AEFFAFAFAEFFADAFB0FFADB1B4FFACB1B5FFADB1B4FFADAFB0FFAEAF
      ADFFAFAFADFFB0B0AEFFAFAFADFFAEAEACFFAEAEABFFAEAEABFFAEAEABFFAEAE
      ABFFAEAEACFFB0B0ADFFC6C6C5FFFBFBFBFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFF4F4F4FF949494FF646464FF605F5FFFD8D5
      D3FFC6E4F3FF8CBED7FF69A9CAFF62A2C4FF5394B7FF649AB5FF96B6C3FFC3C0
      BAFF626160FF646464FF949494FFF4F4F4FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFDFDFDFF9AC3E4FF1285D6FF75C7
      EEFF6ECEF4FFB3E7F9FFFFFFFBFFFFFAF5FFFCF6F0FFF8F0E9FFFAEBE1FF9CD3
      E3FF6BD3FCFF4A85B0FF89CAEBFF83C4E4FF79B9D9FF71B0D1FF5E9EC2FF7694
      B3FFFFFFFFFFA4A3A4FFFFFFFFFFFFFFFFFFFFFFFFFFA0A0A2FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB47B09FFE9E9E9FF00000000F9F9F9FFE3E3E3FFC2C2
      C2FF6F6E6EFFB7B4B1FF766B63FF0F6FBDFF6EC0E8FF6CBBE4FF6BBAE4FF6FBE
      E8FF00385DFF74C3EDFF72C3ECFF1070BDFF766B63FFB7B4B1FF6F6E6EFFB1B1
      B1FF727272FFE9E9E9FF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFF9F9F9FFE3E3E3FFC2C2C2FFB77A32FFB67B34FFB77A32FFC2C2C2FFE3E3
      E3FFF9F9F9FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFF5F5F5FFA8A7A6FF5A58
      57FF83C2E0FF74B0CEFF69A0BDFF598EA9FF477A93FF3B6C88FF295E78FF5A5B
      5EFFA9A8A7FFF5F5F5FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFDFDFDFF9FC8E8FF006F
      CEFF45A7E5FFEBFBFFFFCADFE8FF65A5C5FF5596B7FF4183A7FF96ABB5FFD4E7
      EAFF3895D3FF386996FF9ADDFAFF89CAEAFF7EBEDEFF77B5D5FF6EAECFFF1E4F
      7FFFD4DDE5FFADAAA5FFFFFFFFFFFFFFFFFFFFFFFFFFA5A4A1FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB47B08FFE9E9E9FFF9F9F9FFE3E3E3FFC2C2C2FF6F6E
      6EFFB7B4B1FF766B64FF0F6FBDFF6CBCE6FF6AB7E0FF68B4DEFF68B4DEFF6AB7
      E1FF70BCE6FF00395DFF72BFE9FF70BFE9FF1070BDFF766B64FFB7B4B1FF6F6E
      6EFF717171FFDDDDDDFFF9F9F9FF000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF9F9
      F9FFE3E3E3FFC2C2C2FFB57C38FFF6C477FFF4C377FFF6C477FFB57C38FFC2C2
      C2FFE3E3E3FFF9F9F9FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFDFDFDFFE8E8E8FF4C6B
      90FF6EA2C1FF7CBAD8FF79B8DAFF74B2D3FF6FADCFFF5B94B3FF325C77FF335A
      88FFE6E6E6FFFDFDFDFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF0F5
      F9FF539DDAFF0076D6FF6BB2DAFF6CA2BCFF50819AFF3B6A82FF24648AFF0079
      DEFF064A8BFF376F99FFA4E8FFFF92D4F3FF86C7E7FF7EBDDDFF78B8D8FF244C
      73FF37597FFFB7B2AAFFA8A5A1FFA3A29EFFA5A4A0FFA9A7A3FFA5A4A0FFA2A0
      9CFF9F9E9CFFFFFFFFFFB47B08FFE9E9E9FFF0F0F0FFCBCBCBFF6F6F6EFFB8B4
      B1FF766B64FF1070BDFF69BAE5FF67B5E0FF65B3DEFF64B2DDFF64B2DDFF65B3
      DEFF67B5E0FF6DBAE6FF00395EFF6EBDE8FF6CBCE7FF1070BDFF766B64FFB7B4
      B1FF6F6E6EFFC2C2C2FFE3E3E3FFF9F9F9FF0000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF0F0
      F0FFCBCBCBFFB67F3DFFF2C075FFEDBC73FFECBB72FFEDBC73FFF2C075FFB67F
      3DFFCBCBCBFFF0F0F0FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFF7F7F7FFDADADAFF264B
      7AFF79B5D8FF85C5E5FF7CBCDCFF76B4D6FF71AECFFF6EABCCFF629CBFFF2650
      80FFD7D7D7FFF6F6F6FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFEFE
      FEFFEFEFEFFF91A4B7FF69A0C2FF79B4D2FF6DA8C7FF5E98B5FF2B5E82FFC5D3
      DDFF2C557EFF4A89B1FF87C9EAFF8DCEEEFF93D6F5FF87C9E9FF82C4E4FF2D52
      79FF123D71FFBAB3A9FFFFFFFFFFFFFFFCFFFFFFFFFFA6A39FFFFFFFFFFFFFFF
      FBFFFFFFFBFFFFFFFFFFB47B08FFE9E9E9FFF2F2F2FF717170FFB8B5B2FF766C
      64FF1070BDFF67B8E4FF67B5E1FF67B5E1FF64B2DEFF62B0DCFF62B0DCFF62B0
      DCFF63B1DDFF65B3E0FF6AB8E4FF00385CFF68B6E2FF67B8E4FF1070BDFF766B
      64FFB8B4B1FF6F6E6EFFC2C2C2FFE3E3E3FFF9F9F9FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF2F2
      F2FFB88442FFF7CE8FFFF2C98AFFEDC07EFFE9B76DFFEFC381FFF2C98AFFF7CE
      8FFFB88442FFF2F2F2FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFEFEFEFFF8A9AACFF497C
      A7FF94D7F5FF87C8E8FF80BFDFFF7AB9D9FF74B2D3FF70ACCDFF6EABCBFF4577
      A0FF7D8FA5FFEEEEEEFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF9F9
      F9FFDFDFDFFF5E7593FF8ECFEEFF84C5E4FF79B9D9FF72B0D1FF63A1C3FF9BA9
      BAFF093C70FF1E5887FF1C4F7CFF1C4873FF305E87FF72AFD0FF7FC0DFFF1531
      5BFF032F67FFAEACA3FFFFFFFEFFFFFFF9FFFFFFFCFFA5A49CFFFFFFFCFFFFFF
      F8FFFFFFF8FFFFFFFFFFB47B08FFE9E9E9FFF6F6F6FF969391FF786E66FF1170
      BEFF64B5E2FF64B3DFFF67B5E2FF00385CFF63B1DEFF60AEDBFF5FADDAFF5FAD
      DAFF5FADDAFF60AEDBFF62B0DDFF63B1DEFF62AFDCFF61B0DCFF63B4E1FF1070
      BDFF766C64FFB8B4B1FF6F6E6EFFC2C2C2FFE3E3E3FFF9F9F9FF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFBFB
      FBFFCBA679FFB78242FFB37D3CFFEECA95FFE2AD61FFEECA95FFB37D3CFFB782
      42FFCBA679FFFBFBFBFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFEFEFEFFE7E7E7FF476388FF8BCB
      EDFF95D9F9FF8BCDEDFF85C6E6FF7EBEDEFF78B7D9FF73B2D3FF70AECFFF6BA9
      C9FF415F88FFE6E6E6FFFDFDFDFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF1F1
      F1FFA6AFB9FF4275A1FF9ADFFCFF89CAEAFF7EBEDEFF76B6D5FF71B0D1FF2D5A
      87FF0E4174FF195682FF306591FF47729BFF50749CFF2D4E78FF23436CFF1833
      60FF07336BFFA2A39FFFA9A59BFFA3A097FFA4A299FFA8A59DFFA4A299FFA19E
      96FF9E9C94FFFFFFFFFFB47B08FFE9E9E9FFF4F4F4FFD2D2D2FF1272BFFF61B2
      E1FF61B0DEFF65B3E1FF003B5FFF65B3E1FF60AEDCFF5DABD9FF5DABD9FF5DAB
      D9FF5DABD9FF5DABD9FF5DABD9FF5EACDAFF5DABD9FF5EACD9FF5FADDBFF61B1
      E0FF1170BDFF766C64FFB7B4B1FF6F6E6EFFC2C2C2FFE3E3E3FFF9F9F9FF0000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFEAEAEAFFB6803EFFEFD4ACFFD7A04FFFEFD4ACFFB6803EFFEAEA
      EAFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF8F8F8FFDBDBDBFF174A7DFFAEF9
      FFFF9EE9FFFF96DFFFFF8ED3F5FF86C8EAFF7FC3E4FF7ABDE2FF78BBDFFF71B1
      D5FF193F6EFFDADADAFFF7F7F7FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFEBEB
      EBFF4A698BFF4582ABFFA2E8FFFF92D3F3FF86C6E7FF7DBCDDFF79B8D9FF2D51
      78FF104174FF175581FF2D658EFF407099FF4B749CFF436993FF315782FF1735
      63FF03356DFFBCB2A4FFFFFFF8FFFFFFF4FFFFFFF6FFA6A29AFFFFFFF5FFFFFF
      F1FFFFFEF0FFFFFFFFFFB47B08FFE9E9E9FFF9F9F9FF1975BEFF82C3E7FF5CAB
      DAFF5DACDBFF003A5DFF62AFDFFF5DABDAFF5BA9D8FF5AA8D7FF5AA8D7FF5AA8
      D7FF5AA8D7FF5AA8D7FF5AA8D7FF5AA8D7FF5AA8D7FF5BA8D8FF5BA9D7FF5CAA
      D9FF5DAEDDFF1070BEFF766C65FFB7B4B2FF6F6E6EFFC2C2C2FFE3E3E3FFF9F9
      F9FF00000000000000000000000000000000000000FFFAFAFAFFEFEFEFFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFDADADAFFB8803BFFF1DABAFFE9CDA3FFF1DABAFFB8803BFFDADA
      DAFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFEFEFEFFFFAFAFAFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF1F1F1FF9CA8B6FF1B3E5EFF887F
      79FF150C0CFF170D0CFF475B68FF91D9FEFF415461FF857D78FF181010FF2017
      14FF193353FF8F9EB0FFF0F0F0FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE9E9
      E9FF164575FF4D8BB4FF88C9EAFF8DCEEEFF93D6F5FF87C9EAFF83C4E4FF2A4C
      71FF0E386CFF134E7DFF1F5B87FF306790FF376892FF325F8AFF23517CFF0C30
      62FF6283A3FFBFB6A5FFFFFFFBFFFFFFF7FFFFFFFAFFADA89CFFFFFFF4FFFFFF
      EEFFFFFDECFFFFFFFFFFB47B08FFE9E9E9FF00000000F9F9F9FF1B71B6FF88C2
      E4FF59A8D8FF5BA9DAFF5AA8D9FF59A7D7FF58A6D6FF58A6D6FF58A6D6FF58A6
      D6FF58A6D6FF58A6D6FF59A7D7FF59A7D7FF5BA8D9FF5CA9DAFF5AA8D8FF58A6
      D6FF59A8D8FF88C5E9FF1173C1FF746E69FFB5B5B4FF6E6E6EFFC2C2C2FFE3E3
      E3FFF9F9F9FF000000000000000000000000000000FFEFEFEFFFCECECEFFBDBD
      BDFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFB7B7B7FFB59062FFB77B32FFB67B32FFB77B32FFB59062FFB7B7
      B7FFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBDBDBDFFCECECEFFEFEFEFFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFEBEBEBFF4F7294FF2E3F4CFF2C27
      25FFA7A5A4FF2B2626FF2A221FFF9DEDFFFF29221FFF292525FFA8A6A4FF2E2A
      27FF293544FF4F7093FFEBEBEBFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE9E9
      E9FF134676FF205988FF1C4F7CFF194571FF305F86FF72AFD0FF7FBFE0FF1832
      5CFF123C6FFF164477FF0D4B7BFF195884FF205A87FF1B527FFF0C4070FF0230
      66FF282F39FF2D2C2DFF222326FF1D1F22FF202225FF222326FFACA699FFA29D
      90FF9E998DFFFFFFFFFFB47B08FFE9E9E9FF0000000000000000F9F9F9FF1A70
      B5FF8EC4E5FF54A3D6FF54A3D5FF55A3D5FF55A3D5FF55A3D5FF55A3D5FF55A3
      D5FF56A3D6FF57A5D8FF59A7DAFF5AA7DAFF5CAADDFF003B5DFF57A6D9FF54A3
      D6FF8EC4E5FF1971B7FFF9F9F9FFF9F9F9FF706E6DFFB5B5B5FF6E6E6EFFC2C2
      C2FFDDDDDDFFE9E9E9FFEEEEEEFFF9F9F9FF000000FFE9E9E9FFB3B3B1FFAFB0
      ADFFAEAEACFFAEAFADFFAEAFAEFFAEAFADFFAFAFACFFAFAFABFFB0B0ABFFB0B0
      ACFFB0B1ADFFB0B2AFFFB0B3B3FFB0B4B5FFB0B4B6FFAFB3B5FFAEB2B3FFAEB0
      B1FFAEB0B0FFAEAFAEFFADAEACFFADADABFFADADABFFADADABFFADADABFFADAD
      ABFFADADABFFAFAFADFFB3B3B1FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FF1C5284FF473A30FF4238
      32FF3D3430FF8B8580FF3E3530FF312824FF3F3631FF3D3631FF3B342FFF8D88
      82FF473B31FF19497DFFE9E9E9FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFEAEA
      EAFF154678FF1A5782FF306791FF47739CFF52749CFF2D4E78FF23436CFF1934
      61FF0E3A6FFFA8A498FF44688EFF00326DFF003972FF003972FF44698FFFEAE6
      DAFFFFFFFAFF26272AFFFFFFFAFFFFFFF3FFFFFFF9FF212327FFFFFFF2FFFFFE
      E7FFFFFAE5FFFFFFFFFFB47B08FFE9E9E9FF000000000000000000000000F9F9
      F9FF1A70B5FF96C8E6FF50A0D3FF51A0D3FF53A1D3FF53A1D3FF53A1D3FF54A1
      D4FF56A4D6FF5AA8DBFF003E61FF5EABDFFF003E60FF58A7DAFF53A3D5FF96C9
      E7FF1A70B5FFF9F9F9FF0000000000000000F9F9F9FF6E6E6EFFB4B4B4FF6969
      69FFB7B7B7FFBCBCBCFFCDCDCDFFEEEEEEFF000000FFE9E9E9FFAFAFADFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAFAFADFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FF174B7EFF24699AFF2D70
      A1FF3877A6FF4077A7FF336492FF1C4877FF467DA6FF6EAED4FF69A7CDFF2247
      72FF12487EFF154679FFE9E9E9FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF0F0
      F0FF144476FF175581FF2D658EFF406F99FF4B739CFF436993FF315782FF1735
      63FF04346EFFC1B49AFFFFFFEFFFFFFFEEFFFFFFF5FF2F2B2AFFFFFFF8FFFFFF
      F1FFFFFFF7FF26272AFFFFFFF6FFFFFFF0FFFFFFF5FF222428FFFFFFEFFFFFFD
      E4FFFFF9E1FFFFFFFFFFB47B08FFE9E9E9FF0000000000000000000000000000
      0000F9F9F9FF1970B5FF9ECBE8FF4D9ED2FF4E9ED2FF509FD2FF50A0D3FF53A2
      D5FF58A6DAFF004163FF5DACE1FF004063FF55A5DAFF4FA1D5FF9ECCE9FF1970
      B5FFF9F9F9FF00000000000000000000000000000000F9F9F9FF6E6E6EFFB2B2
      B2FFAFAFAFFFB0B0B0FF6F6F6FFFEEEEEEFF000000FFE9E9E9FFAEAEABFFFFFF
      FFFFFFFFFFFFCD7D26FFBA5F0DFFA73D00FFFFFFFFFF1B33FFFF1632F5FF0C2E
      EBFF0128E2FF0024D9FF001ED0FF0019C6FF0012BDFF0007B4FFFFFFFFFFCF7F
      27FFBA5F0DFFA63B00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAEAEABFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FF174A7CFF2C75A2FF3A7E
      AAFF4886B0FF538AB3FF608FB6FF6B8FB6FF4A709AFF2C507AFF2A4E7AFF244E
      80FF17487BFF174678FFE9E9E9FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF9F9
      F9FF47698FFF154F7DFF225D88FF306791FF376892FF325F8AFF214F7CFF092D
      60FF395B7CFFB7AA95FFA99F8EFFA89E8DFFB3A896FF292A2DFF25262AFF1F21
      26FF24262AFF2C2D31FF24262AFF1E2025FF232529FF27292CFFAFA695FFA39A
      8AFF9E9686FFFFFFFFFFB47B09FFE9E9E9FF0000000000000000000000000000
      000000000000F9F9F9FF1970B5FFA5CEE9FF4A9AD0FF4C9BD1FF509ED4FF55A2
      D9FF004164FF5CA9E1FF004164FF53A2D9FF4C9DD4FFA6CEEAFF1970B5FFF9F9
      F9FF000000000000000000000000000000000000000000000000F9F9F9FF6F6F
      6FFF6E6E6EFF6F6F6FFF717171FFF9F9F9FF000000FFE9E9E9FFAEAEABFFFFFF
      FFFFFCFDFEFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFAFFFFFFFAFFFFFF
      FAFFFFFFFBFFFFFFFCFFFFFFFDFFFFFFFDFFFFFFFDFFFFFFFDFFFFFFFFFFFEFF
      FFFFFFFFFFFFFEFFFFFFFCFEFEFFFAFBFAFFFAFAF9FFFAFAF9FFFAFAF9FFFAFA
      F9FFFBFBF9FFFFFFFFFFAEAEABFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFECECECFF154476FF2973A1FF3980
      ACFF4687B0FF5089B2FF598DB5FF5E8BB3FF5882ABFF4975A0FF386491FF2754
      84FF154779FF174678FFECECECFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFEFE
      FEFFD8DEE5FF124275FF16517EFF1E5C86FF215B89FF1B517FFF0B3E6FFF0A38
      6EFFE7E3D1FFADA28EFFFFFDE0FFFFFBDEFFFFFFE8FF222429FFFFFFEEFFFFFF
      E8FFFFFFEEFF26272CFFFFFFEEFFFFFFE8FFFFFFEEFF222429FFFFFFE8FFFFF9
      DDFFFFF6D9FFFFFFFFFFB47B09FFE9E9E9FF0000000000000000000000000000
      00000000000000000000F9F9F9FF1970B5FFADD2EBFF4698CFFF4C9CD2FF003F
      62FF57A6DDFF004164FF50A0D7FF499AD1FFAED3EBFF1970B5FFF9F9F9FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAFAFACFFFFFF
      FFFFFAFAFAFFFBFBFCFFFBFCFCFFFBFCFCFFFBFCFAFFFDFDFAFFFDFDFAFFFEFD
      FAFFFEFDFAFFFEFDFAFFFEFDFAFFFEFDFAFFFEFDFAFFFDFDFAFFFCFCFAFFFBFC
      FCFFFBFCFCFFFBFCFCFFFBFBFAFFFAFAFAFFFAFAF9FFFAFAF9FFFAFAF9FFFAFA
      F9FFFAFAF9FFFFFFFFFFAFAFACFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF4F4F4FF184476FF246897FF3680
      ACFF4183AFFF4A86B0FF5189B2FF5287B0FF4D7FA9FF42739FFF346491FF2353
      83FF134678FF1C4A7AFFF4F4F4FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFFEFEFEFFDBE1E8FF3A5F8AFF0C4179FF043E79FF00356FFF416589FFE6E0
      CCFFFFFFE1FFA89E89FFFFFCDDFFFFFADBFFFFFFE4FF212429FFFFFFEAFFFFFF
      E4FFFFFFEAFF25272BFFFFFFEAFFFFFFE4FFFFFFEAFF212429FFFFFFE4FFFFF8
      DAFFFFF5D6FFFFFFFFFFB47B09FFE9E9E9FF0000000000000000000000000000
      0000000000000000000000000000F9F9F9FF1870B5FFB6D6EDFF4596CFFF4D9B
      D5FF004062FF4D9CD5FF4696D0FFB6D7EDFF1870B5FFF9F9F9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFB0B0AEFFCFCF
      CEFFA7A7A5FFA8A9A8FFA9AAA9FFA9AAA8FFA9AAA7FFAAAAA6FFABABA6FFACAB
      A6FFACABA6FFABAAA6FFAAAAA7FFA9AAA8FFA9AAA9FFA9AAA8FFA8A9A7FFA8A8
      A6FFA8A8A6FFA8A8A6FFA8A8A6FFA8A8A6FFA8A8A6FFA8A8A6FFA8A8A6FFA8A8
      A6FFA7A7A5FFCFCFCEFFB0B0AEFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFCFCFCFF7A92ACFF184C7DFF307C
      A8FF3A81ADFF4284AFFF4786B0FF4883AEFF437BA7FF3A709DFF2C6291FF1C50
      81FF144477FF7B94AEFFFCFCFCFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFE9E9E9FFD08801FFFFFFFFFFAFA085FFAA9D
      86FFA89D87FFA99E88FFA59A85FFA49984FFADA18BFF22242AFF202329FF1D20
      26FF212429FF27292DFF212429FF1D2026FF202329FF22242AFFADA18BFFA298
      83FF9E937EFFFFFFFFFFB47B09FFE9E9E9FF0000000000000000000000000000
      000000000000000000000000000000000000F9F9F9FF1870B5FFBFDBEFFF4193
      CDFF4495CEFF4193CDFFBFDBEFFF1870B5FFF9F9F9FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAFAFADFFFFFF
      FFFFF2F2F4FFF4F7FCFFF5FAFFFFF6FAFDFFF8F8F6FFFEFBF1FFFFFEF1FFFFFF
      F2FFFFFFF3FFFFFDF4FFF8F8F6FFF5F8FCFFF5FAFFFFF5F9FDFFF3F4F6FFF3F2
      F2FFF2F1F2FFF2F1F2FFF2F1F2FFF2F1F2FFF2F1F2FFF2F1F2FFF2F1F2FFF2F1
      F1FFF1F0F0FFFFFFFFFFAFAFADFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFF8F8F8FF114072FF1D58
      88FF317DAAFF3982AFFF3E83AFFF3E7FABFF3877A3FF306C9AFF245F8FFF1446
      78FF144275FFF8F8F8FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFE9E9E9FFBD7F05FFFFFFFFFFFFF4D2FFFFF5
      D4FFFFF9D8FFA49985FFFFF9D8FFFFF6D5FFFFFCDAFFACA18AFFFFFFDFFFFFFF
      DCFFFFFFDFFFAFA38CFFFFFFDFFFFFFFDCFFFFFFDFFFACA18AFFFFFCDAFFFFF5
      D4FFFFF3D1FFFFFFFFFFB47B09FFE9E9E9FF0000000000000000000000000000
      00000000000000000000000000000000000000000000F9F9F9FF1770B5FFC7E0
      F0FF398DC9FFC7E0F0FF1770B5FFF9F9F9FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFE9E9E9FFAEAEACFFFFFF
      FFFFE9EBEDFFD1812AFFBD6311FFAB4100FFF4F5F0FF1B35FFFF0D2FEEFF0027
      DEFF001CCCFF000DBCFFF5F5F0FFD3832BFFBD6311FFAA4000FFECEEF0FFEAEA
      E9FFE9E8E7FFE9E8E7FFE9E8E7FFE9E8E7FFE9E8E7FFE9E8E7FFE9E8E7FFE9E8
      E7FFE8E7E6FFFFFFFFFFAEAEACFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFEFEFEFFDCE1E8FF113E
      72FF174A7BFF246090FF2F73A0FF2E74A0FF286A97FF21598BFF184A7CFF1241
      75FFDCE1E8FFFEFEFEFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFEAEAEAFFB67C08FFFFFFFFFFFFF0CEFFFFF0
      CFFFFFF4D2FF9F947FFFFFF4D2FFFFF1D0FFFFF4D3FFA09580FFFFF5D4FFFFF3
      D1FFFFF5D4FFA19680FFFFF5D4FFFFF3D1FFFFF5D4FFA09580FFFFF4D3FFFFF0
      CFFFFFF0CEFFFFFFFFFFB47B09FFEAEAEAFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F9F9F9FF1770
      B5FFD3E7F4FF1770B5FFF9F9F9FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFEAEAEAFFAEAEACFFFFFF
      FFFFE3E3E4FFE5E8EBFFE6EBEFFFE7EAEDFFE8E9E5FFEEECE1FFF3EFE0FFF5F0
      E1FFF5F0E3FFF1EDE3FFE9E8E5FFE5E9EBFFE6EBEFFFE6EAEDFFE4E5E6FFE3E2
      E1FFE3E2E1FFE3E2E1FFE3E2E1FFE3E2E1FFE3E2E1FFE3E2E1FFE3E2E1FFE3E2
      E0FFE2E1E0FFFFFFFFFFAEAEACFFEAEAEAFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFCFC
      FCFF6E8AA8FF174576FF114072FF123F73FF134073FF184577FF6E8AA8FFFCFC
      FCFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFF2F2F2FFB67E0EFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB67E0EFFF2F2F2FF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F9F9
      F9FF1D73B8FFF9F9F9FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFF2F2F2FFAFAFADFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAFAFADFFF2F2F2FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFFBFBFBFFCAA65BFFB67E0EFFB47B09FFB47B
      08FFB47B09FFB57B09FFB47B09FFB47B08FFB47B09FFB57B09FFB47B09FFB47B
      08FFB47B09FFB57B09FFB47B09FFB47B08FFB47B09FFB57B09FFB47B09FFB47B
      08FFB47B09FFB67E0EFFCAA65BFFFBFBFBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFFBFBFBFFC6C6C5FFB0B0
      ADFFAEAEACFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAE
      ABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAE
      ABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAE
      ABFFAEAEACFFB0B0ADFFC6C6C5FFFBFBFBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFCFCFFF6F6
      F6FFEFEFEFFFEAEAEAFFEEEEEEFFF9F9F9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFF9F9F9FFEDED
      EDFFE0E0E0FFD7D7D7FFDEDEDEFFF3F3F3FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFF6F6F6FFE5E5
      E5FFD2D2D2FFC5C5C5FFCFCFCFFFEDEDEDFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9FFEEEEEEFFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE8E8E8FFE3E3E3FFD7D7
      D7FFC9C9C9FFBFBFBFFFCDCDCDFFEEEEEEFFF3F3F3FFDEDEDEFFD5D5D5FFD5D5
      D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5
      D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5
      D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD3D3D3FFCACACAFFB5B5
      B5FF9E9E9EFF8F8F8FFFA5A5A5FFDEDEDEFFEDEDEDFFCFCFCFFFC3C3C3FFC3C3
      C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3
      C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3
      C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC0C0C0FFB4B4B4FF9999
      99FF7D7D7DFF6B6B6BFF858585FFCFCFCFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F4FFDDDDDDFFD2D2D2FFD2D2
      D2FFD2D2D2FFD2D2D2FFD2D2D2FFCDCDCDFFC2C2C2FFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFB5B7B8FF869C
      AEFF6489A6FF4B7CA4FF69A6DAFFE9E9E9FFE9E9E9FFC0C0C0FFADADADFFADAD
      ADFFADADADFFADADADFFADADADFFA5A5A5FF949494FF8B8B8BFF8B8B8BFF8B8B
      8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B
      8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF818486FF5674
      8EFF477396FF4B7CA4FF69A6DAFFD5D5D5FFDFDFDFFFA6A6A6FF8E8E8EFF8E8E
      8EFF8E8E8EFF8E8E8EFF8E8E8EFF858585FF717171FF666666FF666666FF6666
      66FF666666FF666666FF666666FF666666FF666666FF666666FF666666FF6666
      66FF666666FF666666FF666666FF666666FF666666FF666666FF5D6163FF4264
      81FF426F93FF4B7CA4FF69A6DAFFC3C3C3FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9FF5C5C5CFF5C5C5CFF5C5C
      5CFF5C5C5CFF5C5C5CFF5C5C5CFF5D5C5AFF5D5B59FF5C5A5AFF5A5A5AFF5A5A
      5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A
      5BFF5A5A5BFF5A5A5AFF5C5957FF576570FF536E83FF4E7B9FFF4C7FAAFF4C7F
      A9FF4B7EA8FF5081A8FF67A4D8FFE9E9E9FFF3F3F3FF5C5C5CFF5C5C5CFF5C5C
      5CFF5C5C5CFF5C5C5CFF5C5C5CFF5D5C5AFF5D5B59FF5C5A5AFF5A5A5AFF5A5A
      5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A
      5BFF5A5A5BFF5A5A5AFF5C5957FF576570FF536E83FF4E7B9FFF4C7FAAFF4C7F
      A9FF4B7EA8FF5081A8FF67A4D8FFD5D5D5FFEDEDEDFF5C5C5CFF5C5C5CFF5C5C
      5CFF5C5C5CFF5C5C5CFF5C5C5CFF5D5C5AFF5D5B59FF5C5A5AFF5A5A5AFF5A5A
      5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A5BFF5A5A
      5BFF5A5A5BFF5A5A5AFF5C5957FF576570FF536E83FF4E7B9FFF4C7FAAFF4C7F
      A9FF4B7EA8FF5081A8FF67A4D8FFC3C3C3FF0000000000000000FBFBFBFFF2F2
      F2FFEAEAEAFFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FBFBFBFFF2F2F2FFEAEAEAFFE9E9E9FFEAEA
      EAFFF2F2F2FFFBFBFBFF00000000000000000000000000000000000000000000
      0000000000000000000000000000E9E9E9FF386CA1FF706B66FF6D6B69FF6D6B
      6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B
      6AFF6D6B6AFF6D6A68FF6F6864FF4A86B5FF4C83AEFF4D82ACFF4D81AAFF4D81
      AAFF4C80A9FF5786ACFF66A3D6FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD5D5D5FF386CA1FF706B66FF6D6B69FF6D6B
      6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B
      6AFF6D6B6AFF6D6A68FF6F6864FF4A86B5FF4C83AEFF4D82ACFF4D81AAFF4D81
      AAFF4C80A9FF5786ACFF66A3D6FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF386CA1FF706B66FF6D6B69FF6D6B
      6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B6AFF6D6B
      6AFF6D6B6AFF6D6A68FF6F6864FF4A86B5FF4C83AEFF4D82ACFF4D81AAFF4D81
      AAFF4C80A9FF5786ACFF66A3D6FFC3C3C3FF0000000000000000F2F2F2FFD4D4
      D4FFC0C0C0FFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F2F2F2FFD4D4D4FFC0C0C0FFBCBCBCFFC0C0
      C0FFD4D4D4FFF2F2F2FF00000000000000000000000000000000000000000000
      0000000000000000000000000000E9E9E9FF3C6B9CFF6D6761FF6A6765FF6967
      66FF696766FF696766FF696766FF696766FF696766FF696766FF696766FF6967
      66FF696766FF6A6765FF6C645FFF4B85B4FF4E83AEFF4E82ACFF4E82ACFF4E82
      ACFF4C80ABFF5B8BB1FF64A1D3FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD5D5D5FF3C6B9CFF6D6761FF6A6765FF6967
      66FF696766FF696766FF696766FF696766FF696766FF696766FF696766FF6967
      66FF696766FF6A6765FF6C645FFF4B85B4FF4E83AEFF4E82ACFF4E82ACFF4E82
      ACFF4C80ABFF5B8BB1FF64A1D3FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF3C6B9CFF6D6761FF6A6765FF6967
      66FF696766FF696766FF696766FF696766FF696766FF696766FF696766FF6967
      66FF696766FF6A6765FF6C645FFF4B85B4FF4E83AEFF4E82ACFF4E82ACFF4E82
      ACFF4C80ABFF5B8BB1FF64A1D3FFC3C3C3FF0000000000000000EAEAEAFF7D7B
      7BFF5C5A59FF5C5A5AFF5C5A59FF7D7B7BFFEAEAEAFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EAEAEAFF7D7B7BFF5C5A59FF5C5A5AFF5C5A
      59FF7D7B7BFFEAEAEAFF00000000000000000000000000000000000000000000
      0000000000000000000000000000E9E9E9FF3D6B9CFF6B6660FF686664FF6766
      65FF676665FF676665FF676665FF676665FF676665FF676665FF676665FF6766
      65FF676665FF676664FF69635DFF4B86B6FF4E83AFFF4E83AEFF4E83AEFF4E83
      AEFF4C81ADFF628FB3FF629FD2FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD5D5D5FF3C6B9CFF6B6660FF686664FF6766
      65FF676665FF676665FF676665FF676665FF676665FF676665FF676665FF6766
      65FF676665FF676664FF69635DFF4B86B6FF4E83AFFF4E83AEFF4E83AEFF4E83
      AEFF4C81ADFF628FB3FF629FD2FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF3D6B9CFF6B6660FF686664FF6766
      65FF676665FF676665FF676665FF676665FF676665FF676665FF676665FF6766
      65FF676665FF676664FF69635DFF4B86B6FF4E83AFFF4E83AEFF4E83AEFF4E83
      AEFF4C81ADFF628FB3FF629FD2FFC3C3C3FF00000000FEFEFEFFE6E6E6FF6F6D
      6BFF6F6E6BFF706E6BFF706E6BFF706E6BFFD8D8D8FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFD8D8D8FF706E6BFF706E6BFF706E6BFF6F6E
      6BFF6F6D6BFFE6E6E6FFFEFEFEFF000000000000000000000000000000000000
      0000000000000000000000000000E9E9E9FF3F6D9DFF6A635EFF676462FF6664
      63FF666463FF666463FF666463FF666463FF666463FF666463FF666463FF6664
      63FF666463FF666462FF69615BFF4D87B7FF4F84B0FF4F84AFFF4F84AFFF4F84
      AFFF4D82ADFF6694B8FF619DCFFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD5D5D5FF3E6D9DFF6A635EFF676462FF6664
      63FF666463FF666463FF666463FF666463FF666463FF666463FF666463FF6664
      63FF666463FF666462FF69615BFF4D87B7FF4F84B0FF4F84AFFF4F84AFFF4F84
      AFFF4D82ADFF6694B8FF619DCFFFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF3F6C9DFF6A635EFF676462FF6664
      63FF666463FF666463FF666463FF666463FF666463FF666463FF666463FF6664
      63FF666463FF666462FF69615BFF4D87B7FF4F84B0FF4F84AFFF4F84AFFF4F84
      AFFF4D82ADFF6694B8FF619DCFFFC3C3C3FF00000000F7F7F7FFD8D8D8FF615E
      5DFF625F5EFF625F5EFF625F5EFF635F5EFFB7B7B7FFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFB7B7B7FF635F5EFF625F5EFF625F5EFF625F
      5EFF615E5DFFD8D8D8FFF7F7F7FF000000000000000000000000000000000000
      0000F9F9F9FFF0F0F0FFF2F2F2FFE5E5E5FF3F6E9EFF67615BFF646260FF6462
      61FF646261FF646261FF646261FF646261FF646261FF646261FF646261FF6462
      61FF646261FF64615FFF665E59FF4E8AB9FF5087B2FF5086B1FF5086B1FF5086
      B1FF4D84AFFF6C99BAFF609CCEFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD1D1D1FF3F6E9DFF67615BFF646260FF6462
      61FF646261FF646261FF646261FF646261FF646261FF646261FF646261FF6462
      61FF646261FF64615FFF665E59FF4E8AB9FF5087B2FF5086B1FF5086B1FF5086
      B1FF4D84AFFF6C99BAFF609CCEFFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FFECECECFFE6E6E6FFB3B3B3FF416D9FFF68605CFF646260FF6462
      61FF646261FF646261FF646261FF646261FF646261FF646261FF646261FF6462
      61FF646261FF64615FFF665E59FF4E8AB9FF5087B2FF5086B1FF5086B1FF5086
      B1FF4D84AFFF6C99BAFF609CCEFFC3C3C3FF00000000EDEDEDFFADADADFF5755
      57FF585657FF585658FF585658FF585658FF585658FF585758FF595758FF5957
      58FF595758FF595758FF595758FF595758FF595758FF595758FF595758FF5957
      58FF595758FF595758FF585758FF585658FF585658FF585658FF585658FF5856
      57FF575557FFADADADFFEDEDEDFF000000000000000000000000000000000000
      0000EEEEEEFFD1D1D1FFD1D1D1FFD5D5D5FF416EA1FF655F5AFF62615FFF6261
      60FF626160FF626160FF626160FF626160FF626160FF626160FF626160FF6261
      60FF626160FF62605FFF645D57FF4F8BBCFF5188B5FF5187B3FF5187B3FF5187
      B3FF4E85B1FF729CBFFF5E9ACCFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF3F709EFF65605AFF62615FFF6261
      60FF626160FF626160FF626160FF626160FF626160FF626160FF626160FF6261
      60FF626160FF62605FFF645D57FF4F8BBCFF5188B5FF5187B3FF5187B3FF5187
      B3FF4E85B1FF729CBFFF5E9ACCFFD5D5D5FF000000FF000000FF000000FF0000
      00FFEBEBEBFFC1C1C1FFABABABFF8E8E8EFF446DA4FF665F5BFF62615FFF6261
      60FF626160FF626160FF626160FF626160FF626160FF626160FF626160FF6261
      60FF626160FF62605FFF645D57FF4F8BBCFF5188B5FF5187B3FF5187B3FF5187
      B3FF4E85B1FF729CBFFF5E9ACCFFC3C3C3FF00000000E9E9E9FFACAAA8FFADAA
      A9FFADAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAA
      A9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAA
      A9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFAEAAA9FFADAA
      A9FFADAAA9FFACAAA8FFE9E9E9FF000000000000000000000000000000000000
      0000E9E9E9FF008E4DFF3C9A6CFFBDBDBDFF466CA5FF655D59FF615F5DFF615F
      5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F
      5EFF615F5EFF615E5CFF635B55FF508CBDFF5289B6FF5288B4FF5288B4FF5288
      B4FF4F86B3FF78A1C3FF5C98CAFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFADADADFF40709FFF645D58FF615F5DFF615F
      5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F
      5EFF615F5EFF615E5CFF635B55FF508CBDFF5289B6FF5288B4FF5288B4FF5288
      B4FF4F86B3FF78A1C3FF5C98CAFFD5D5D5FF000000FF000000FF000000FFFCFC
      FCFFD7D7D7FF00723EFF0E824AFF009246FF476CA7FF655D59FF615F5DFF615F
      5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F5EFF615F
      5EFF615F5EFF615E5CFF635B55FF508CBDFF5289B6FF5288B4FF5288B4FF5288
      B4FF4F86B3FF78A1C3FF5C98CAFFC3C3C3FF00000000E9E9E9FFA6A4A2FFA6A4
      A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4
      A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4
      A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4A2FFA6A4
      A2FFA6A4A2FFA6A4A2FFE9E9E9FF00000000FAFAFAFFEFEFEFFFE9E9E9FFE9E9
      E9FFD8D8D8FF008B4BFF00C885FF4A9970FF496AA8FF655A57FF605C5BFF5F5D
      5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D
      5CFF5F5D5CFF5F5C5AFF605853FF528EBFFF538AB8FF5389B6FF5389B6FF5389
      B6FF4D82ADFF7CA5C6FF5B96C8FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF448C66FF4071A0FF625B56FF5F5D5BFF5F5D
      5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D
      5CFF5F5D5CFF5F5C5AFF605853FF528EBFFF538AB8FF5389B6FF5389B6FF5389
      B6FF4D82ADFF7CA5C6FF5B96C8FFD5D5D5FF000000FF000000FFE6E6E6FFD7D7
      D7FFAEAEAEFF007C41FF00C885FF008F45FF486CA8FF645A57FF615C5BFF605D
      5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D5CFF5F5D
      5CFF5F5D5CFF5F5C5AFF605853FF528EBFFF538AB8FF5389B6FF5389B6FF5389
      B6FF4D82ADFF7CA5C6FF5B96C8FFC3C3C3FF00000000E9E9E9FFA19C99FFA09B
      98FF9E9A98FF9D9A98FF9D9A98FF9D9A99FF9D9A99FF9D9B99FF9E9B99FF9E9B
      99FF9E9B99FF9E9B99FF9E9B99FF9E9B99FF9E9B99FF9E9B99FF9E9B99FF9E9B
      99FF9E9B99FF9E9B99FF9D9B99FF9D9A99FF9D9A98FF9D9A98FF9D9A98FF9E9A
      98FFA09B98FFA19C99FFE9E9E9FF00000000EFEFEFFFCECECEFFBDBDBDFFBCBC
      BCFFB7B7B7FF008848FF00E5A6FF00C27DFF157E68FF675358FF60595BFF5D5B
      5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B
      5BFF5D5B5BFF5D5A59FF5E5651FF5390C1FF548CB9FF548BB7FF548BB7FF548B
      B7FF497BA2FF82AACAFF5A95C5FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00B172FF4172A0FF605955FF5D5B5AFF5D5B
      5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B
      5BFF5D5B5BFF5D5A59FF5E5651FF5390C1FF548CB9FF548BB7FF548BB7FF548B
      B7FF497BA2FF82AACAFF5A95C5FFD5D5D5FF000000FFCCCCCCFFAEAEAEFF9797
      97FF2C855AFF00C080FF00E5A5FF008A43FF4E6AAAFF695357FF65555AFF6257
      5BFF5F5A5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B5BFF5D5B
      5BFF5D5B5BFF5D5A59FF5E5651FF5390C1FF548CB9FF548BB7FF548BB7FF548B
      B7FF497BA2FF82AACAFF5A95C5FFC3C3C3FF00000000E9E9E9FFD0C4B9FFCFC6
      BDFFC6C1BDFFC1C0BEFFC0C0BFFFC0C1C0FFC1C1C1FFC2C2C1FFC5C4C2FFC7C5
      C3FFC8C5C4FFC8C6C4FFC8C6C4FFC8C6C4FFC8C6C4FFC8C6C4FFC8C6C4FFC8C5
      C4FFC7C5C3FFC5C4C2FFC2C2C1FFC1C1C1FFC0C0BFFFC0BFBEFFC1BFBDFFC6C1
      BDFFCFC6BDFFD0C4B9FFE9E9E9FF00000000E9E9E9FF0C9053FF008A49FF0088
      47FF008746FF008342FF00DCA1FF00DCA0FF00C07EFF1F764BFF645357FF5E58
      59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A
      59FF5B5A59FF5B5957FF5C554FFF5491C3FF558DBBFF558CB9FF568BB8FF578A
      B7FF457096FF87ADCDFF5892C4FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00C992FF4273A2FF5D5852FF5B5A58FF5B5A
      59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A
      59FF5B5A59FF5B5957FF5C554FFF5491C3FF558DBBFF558CB9FF568BB8FF578A
      B7FF457096FF87ADCDFF5892C4FFD5D5D5FFE5E5E5FF0B854CFF006F3BFF007B
      3FFF00BC81FF00DBA0FF00DCA1FF008440FF008A42FF008D44FF009047FF1A7D
      50FF605759FF5C5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A59FF5B5A
      59FF5B5A59FF5B5957FF5C554FFF5491C3FF558DBBFF558CB9FF568BB8FF578A
      B7FF457096FF87ADCDFF5892C4FFC3C3C3FF00000000E9E9E9FF3DA3FFFF0888
      FFFFFFFFFCFFFFFFFAFFFFECDFFFFFE0D0FFFFE1D1FFFFEADCFFB9BAB3FFBFBF
      B7FFC2C2B9FFC3C4BAFFC3C4BAFFC3C4BAFFC3C4BAFFC3C4BAFFC3C4BAFFC2C2
      B9FFBFC0B7FFB5B5B0FFFFEADBFFFFE0D0FFFFE9DCFFFFFEF8FFFFFFFDFFFFFF
      FCFF0888FFFF3DA3FFFFE9E9E9FF00000000E9E9E9FF008948FF39E8BEFF00DA
      A1FF00D9A1FF00D8A0FF00D39CFF00D39CFF00D79FFF00BE80FF1F754AFF6151
      55FF5B5657FF595857FF595857FF595857FF595857FF595857FF595857FF5958
      57FF595857FF595755FF59524CFF5693C4FF568EBCFF568DBAFF588BB7FF3AA8
      DFFF23B8F7FF8EB1CFFF5791C1FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00C18FFF4275A3FF5B5550FF595856FF5958
      57FF595857FF595857FF595857FF595857FF595857FF595857FF595857FF5958
      57FF595857FF595755FF59524CFF5693C4FF568EBCFF568DBAFF588BB7FF3AA8
      DFFF23B8F7FF8EB1CFFF5791C1FFD5D5D5FFDDDDDDFF00703BFF0E925CFF00BA
      80FF00D6A0FF00D39CFF00D39CFF00D89FFF00DAA1FF00DAA1FF35EABDFF0091
      47FF615257FF5A5757FF595857FF595857FF595857FF595857FF595857FF5958
      57FF595857FF595755FF59524CFF5693C4FF568EBCFF568DBAFF588BB7FF3AA8
      DFFF23B8F7FF8EB1CFFF5791C1FFC3C3C3FF00000000E9E9E9FF73BAFFFF4EA4
      FCFFFFFAE4FFFFF5E6FFFFF5E9FFFCDCC9FFFEDECAFFFFEFE1FFC3C4BFFFCCCD
      C6FF7D7C7BFF7F7E7EFF807F7EFF807F7EFF807F7EFF807F7EFF7F7E7EFF7D7C
      7BFFCCCCC6FFC6C8C3FFFFEEE0FFFEDDC9FFFFF5E8FFFFF4E7FFFFF4E5FFFFF9
      E3FF4DA4FCFF73BAFFFFE9E9E9FF00000000E9E9E9FF008745FF52E5C3FF00CF
      9AFF00CF9BFF00CF9BFF00CD9AFF00CD9AFF00CF9BFF00D39FFF00BC81FF1C79
      4DFF5D5256FF595556FF585656FF585656FF585656FF585656FF585656FF5856
      56FF585656FF585554FF58504BFF5795C7FF5790BEFF588FBBFF5A8CB8FF27B8
      F5FF5482AEFF92B4D2FF5590BEFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00BB8DFF4375A5FF5A534FFF585655FF5856
      56FF585656FF585656FF585656FF585656FF585656FF585656FF585656FF5856
      56FF585656FF585554FF58504BFF5795C7FF5790BEFF588FBBFF5A8CB8FF27B8
      F5FF5482AEFF92B4D2FF5590BEFFD5D5D5FFD7D7D7FF007E42FF00B881FF00D2
      9FFF00CE9BFF00CD9AFF00CD9AFF00CF9BFF00CF9BFF00CF9AFF51E6C3FF008E
      44FF604F55FF5A5556FF585656FF585656FF585656FF585656FF585656FF5856
      56FF585656FF585554FF58504BFF5795C7FF5790BEFF588FBBFF5A8CB8FF27B8
      F5FF5482AEFF92B4D2FF5590BEFFC3C3C3FF00000000E9E9E9FF84C3FFFFA4C7
      EAFFFFEED1FFFFEBD3FFFFECD4FFFDE5D1FFFFF3E4FFE8E0D9FFB9B8B4FFDAD9
      D4FFDEDDD7FFE1E0DAFFE1E0DBFFE1E0DBFFE1E0DBFFE1E0DBFFE1E0DAFFDEDD
      D7FFDAD9D4FFBEBCB9FFE8DFD9FFFFF1E3FFFFF0DEFFFFEAD3FFFFEAD2FFFFEE
      D1FFAECCEAFF84C3FFFFE9E9E9FF00000000E9E9E9FF008744FF6DE6CDFF00C8
      97FF00C898FF00C999FF00C999FF00C99AFF00C999FF06CEA1FF28DFB6FF0093
      47FF5D5053FF575454FF565554FF565554FF565554FF565554FF565554FF5655
      54FF565554FF565452FF564F49FF5896C9FF5891C0FF5990BDFF5C8DBAFF27BA
      F7FF4F7BA4FF97B8D7FF558EBDFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00B88DFF4376A5FF58524CFF565553FF5655
      54FF565554FF565554FF565554FF565554FF565554FF565554FF565554FF5655
      54FF565554FF565452FF564F49FF5896C9FF5891C0FF5990BDFF5C8DBAFF27BA
      F7FF4F7BA4FF97B8D7FF558EBDFFD5D5D5FFDADADAFF008A48FF2CDCB7FF07CD
      A1FF00C999FF00C99AFF00C999FF00C999FF00C898FF00C897FF6BE7CDFF008D
      43FF5E4E53FF585454FF565554FF565554FF565554FF565554FF565554FF5655
      54FF565554FF565452FF564F49FF5896C9FF5891C0FF5990BDFF5C8DBAFF27BA
      F7FF4F7BA4FF97B8D7FF558EBDFFC3C3C3FF00000000EAEAEAFF36A1FFFFFFFC
      F8FFFFFFF3FFFFFFF4FFFFFFF5FFF9F0E7FFCFC9C3FF8B8988FFA19F9DFFE8E7
      E2FF747271FF777574FF787674FF787674FF787674FF787674FF777574FF7472
      71FFE7E7E2FFA7A6A3FF8A8987FFD7CFC8FFF9F1E8FFFFFFF5FFFFFFF4FFFFFF
      F3FFFFFCF8FF37A1FFFFEAEAEAFF00000000E9E9E9FF008744FF87E9D8FF00C3
      97FF00C398FF00C398FF00C498FF00C599FF00C69AFF60E1C7FF00B781FF1977
      4BFF594F53FF555353FF545353FF545353FF545353FF545353FF545353FF5453
      53FF545353FF545251FF534D48FF5997CAFF5992C1FF5A91BEFF5C8EBCFF27B9
      F7FF4A7299FF9DBDD9FF538CBBFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00B48CFF4477A6FF56504BFF545352FF5453
      53FF545353FF545353FF545353FF545353FF545353FF545353FF545353FF5453
      53FF545353FF545251FF534D48FF5997CAFF5992C1FF5A91BEFF5C8EBCFF27B9
      F7FF4A7299FF9DBDD9FF538CBBFFD5D5D5FFE4E4E4FF008948FF00B381FF62E0
      C7FF00C69AFF00C599FF00C498FF00C398FF00C398FF00C397FF86EAD8FF008E
      43FF5B4B52FF565253FF545353FF545353FF545353FF545353FF545353FF5453
      53FF545353FF545251FF534D48FF5997CAFF5992C1FF5A91BEFF5C8EBCFF27B9
      F7FF4A7299FF9DBDD9FF538CBBFFC3C3C3FF00000000F0F0F0FFA79E96FF817B
      78FF7F7C7BFF7F7C7DFF7F7D7DFF817E7EFF838180FF827F7FFFC3C1C0FFFCFB
      F8FFF7F5F2FFF9F7F4FFF9F7F4FFF9F7F4FFF9F7F4FFF9F7F4FFF9F7F4FFF7F5
      F2FFF8F5F2FFBBBABAFF827F7FFF838080FF817E7EFF7F7D7DFF7F7C7DFF7F7C
      7BFF827C78FFA29992FFF0F0F0FF00000000EFEFEFFF008946FF9DF0E6FF4EE9
      D4FF51E9D4FF4DE7D1FF94E6D7FF00C097FF5EDDC6FF00B47FFF1A7547FF5B4A
      4FFF554F51FF535151FF535151FF535151FF535151FF535151FF535151FF5351
      51FF535151FF53504FFF524A45FF5B99CDFF5A93C3FF5A92C1FF5C90BFFF3BA9
      E1FF21B7F6FFA0BFD9FF528ABAFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00AF8AFF4479A7FF554D49FF535050FF5351
      51FF535151FF535151FF535151FF535151FF535151FF535151FF535151FF5351
      51FF535151FF53504FFF524A45FF5B99CDFF5A93C3FF5A92C1FF5C90BFFF3BA9
      E1FF21B7F6FFA0BFD9FF528ABAFFD5D5D5FF000000FF008745FF269E6CFF00AF
      7FFF60DCC6FF00C097FF93E6D7FF4DE7D1FF4FE9D3FF4CEAD4FF9AF2E6FF0091
      46FF594A50FF555051FF535151FF535151FF535151FF535151FF535151FF5351
      51FF535151FF53504FFF524A45FF5B99CDFF5A93C3FF5A92C1FF5C90BFFF3BA9
      E1FF21B7F6FFA0BFD9FF528ABAFFC3C3C3FFFBFBFBFFEBEBEBFFBCBAB9FF9895
      94FF82817EFF807F7DFF817F7DFF81807EFF817F7DFF7E7C7AFFACABAAFFA7A5
      A4FFA5A3A1FFA5A3A1FFA5A3A1FFA5A3A1FFA5A3A1FFA5A3A1FFA5A3A1FFA5A3
      A1FFA7A5A4FFACABABFF7E7D7AFF817F7DFF81807EFF817F7DFF817F7DFF827F
      7EFF939190FFB7B4B3FFEBEBEBFFFBFBFBFFFAFAFAFF0F9255FF008946FF0087
      44FF008642FF00823BFF82E3D6FF58DAC4FF00AF7DFF197346FF59474CFF544D
      4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F
      4FFF514F4FFF514E4DFF504843FF5C9BCEFF5B95C4FF5B94C2FF5C94C1FF5C92
      BFFF588FBEFFA5C3DDFF5089B7FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF50C7B3FF457AA7FF524B46FF514E4DFF514F
      4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F
      4FFF514F4FFF514E4DFF504843FF5C9BCEFF5B95C4FF5B94C2FF5C94C1FF5C92
      BFFF588FBEFFA5C3DDFF5089B7FFD5D5D5FF000000FF000000FF008745FF0085
      43FF00AB7FFF5AD9C4FF81E3D6FF008339FF00893EFF008D42FF009146FF1579
      4CFF564B4EFF524E4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F4FFF514F
      4FFF514F4FFF514E4DFF504843FF5C9BCEFF5B95C4FF5B94C2FF5C94C1FF5C92
      BFFF588FBEFFA5C3DDFF5089B7FFC3C3C3FFF2F2F2FFD3D3D3FFB7B7B7FF9F9D
      9CFF898786FF7B7877FF7C7A79FF7C7A79FF7C7A79FF797776FF959392FF9B9B
      98FF908E8CFF918F8DFF918F8DFF918F8DFF918F8DFF918F8DFF918F8DFF908E
      8CFF9B9B98FF959392FF797776FF7C7A79FF7C7A79FF7C7A79FF7B7978FF8381
      80FF999796FFB5B5B5FFD3D3D3FFF2F2F2FF0000000000000000000000000000
      0000E9E9E9FF008944FF79E3D8FF00AC7CFF16846AFF574548FF514C4DFF4F4E
      4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E
      4EFF4F4E4EFF4F4D4CFF4E4641FF5C9DD1FF5B96C6FF5B95C4FF5B95C4FF5A94
      C3FF5691C2FFA9C6E1FF4F87B6FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF009D71FF467AA9FF504A45FF4F4D4DFF4F4E
      4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E
      4EFF4F4E4EFF4F4D4CFF4E4641FF5C9DD1FF5B96C6FF5B95C4FF5B95C4FF5A94
      C3FF5691C2FFA9C6E1FF4F87B6FFD5D5D5FF000000FF000000FF000000FFFCFC
      FCFF4AA477FF00AA7FFF79E3D7FF008B3FFF5172B1FF574347FF55474CFF544A
      4EFF514D4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E4EFF4F4E
      4EFF4F4E4EFF4F4D4CFF4E4641FF5C9DD1FF5B96C6FF5B95C4FF5B95C4FF5A94
      C3FF5691C2FFA9C6E1FF4F87B6FFC3C3C3FFEAEAEAFF8E8C8BFF74716FFF9C9A
      9AFF8A8887FF757372FF737170FF737170FF737170FF72706FFF7D7B7AFF8B89
      89FF7A7978FF797776FF797777FF797777FF797777FF797777FF797776FF7A79
      78FF8B8989FF7D7B7AFF72706FFF737170FF737170FF737170FF737171FF8583
      80FF979695FF757270FF8E8C8BFFEAEAEAFF0000000000000000000000000000
      0000EEEEEEFF008C4AFF00AC85FF5CAC81FF4F77B2FF504645FF4D4B4BFF4D4C
      4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C
      4CFF4D4C4CFF4D4B4AFF4B443FFF5E9ED3FF5C97C8FF5C96C6FF5C96C6FF5B95
      C6FF5693C4FFADCAE3FF4D85B3FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF549D76FF467CAAFF4D4743FF4D4B4AFF4D4C
      4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C
      4CFF4D4C4CFF4D4B4AFF4B443FFF5E9ED3FF5C97C8FF5C96C6FF5C96C6FF5B95
      C6FF5693C4FFADCAE3FF4D85B3FFD5D5D5FF000000FF000000FF000000FF0000
      00FFEBEBEBFF008646FF00AC85FF008F43FF4E78B2FF504645FF4E4A4AFF4E4B
      4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C4CFF4D4C
      4CFF4D4C4CFF4D4B4AFF4B443FFF5E9ED3FF5C97C8FF5C96C6FF5C96C6FF5B95
      C6FF5693C4FFADCAE3FF4D85B3FFC3C3C3FFEAEAEAFF817E7CFF807D7BFF8885
      84FF8A8888FF6F6D6DFF666464FF656463FF646262FF626161FF616060FF6765
      65FF5F5F5FFF545353FF545353FF545353FF545353FF545353FF545353FF5F5F
      5FFF676565FF616060FF626061FF646262FF656463FF666464FF6A6868FF8584
      83FF888483FF807D7BFF817E7CFFEAEAEAFF0000000000000000000000000000
      0000F9F9F9FF008E4DFF51AF81FFE5E5E5FF4C7AB1FF4E4542FF4C4948FF4C4A
      4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A
      4AFF4C4A4AFF4C4948FF4A423DFF5F9FD4FF5D98C9FF5D97C7FF5D97C7FF5C96
      C7FF5793C5FFB2CFE6FF4C84B1FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD1D1D1FF467DAAFF4D4541FF4C4948FF4C4A
      4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A
      4AFF4C4A4AFF4C4948FF4A423DFF5F9FD4FF5D98C9FF5D97C7FF5D97C7FF5C96
      C7FF5793C5FFB2CFE6FF4C84B1FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF008C4CFF1A965AFF009146FF4D7AB2FF4E4542FF4C4948FF4C4A
      4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A4AFF4C4A
      4AFF4C4A4AFF4C4948FF4A423DFF5F9FD4FF5D98C9FF5D97C7FF5D97C7FF5C96
      C7FF5793C5FFB2CFE6FF4C84B1FFC3C3C3FFF2F2F2FFBEBBBAFFBEBBBAFFBFBC
      BBFF848583FFA7A5A0FFA9A8A3FFC2BEB6FFD1CDC2FFE0DBCFFFF9F2E3FFFFF8
      E8FFFFF8E9FFFFF9E9FFFFF9E9FFFFF9E9FFFFF9E9FFFFF9E9FFFFF8E9FFFFF8
      E8FFFFF8E8FFF9F1E2FFE0DBCFFFD1CDC2FFC2BEB6FFAAA9A3FFA6A5A0FF8586
      84FFC0BCBCFFBEBBBAFFBEBBBAFFF2F2F2FF0000000000000000000000000000
      0000000000000000000000000000E9E9E9FF4B7DAFFF4B4440FF4A4847FF4A49
      49FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A49
      49FF4A4949FF4A4847FF48413CFF60A1D7FF5E9BCBFF5E99C9FF5E99C9FF5D98
      C9FF5895C7FFB6D2E8FF4A82B0FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD5D5D5FF487EABFF4A4440FF4A4847FF4A49
      49FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A49
      49FF4A4949FF4A4847FF48413CFF60A1D7FF5E9BCBFF5E99C9FF5E99C9FF5D98
      C9FF5895C7FFB6D2E8FF4A82B0FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF4D7CB2FF4B4441FF4A4847FF4A49
      49FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A4949FF4A49
      49FF4A4949FF4A4847FF48413CFF60A1D7FF5E9BCBFF5E99C9FF5E99C9FF5D98
      C9FF5895C7FFB6D2E8FF4A82B0FFC3C3C3FFFBFBFBFFAEACABFF8B8886FF8B88
      86FF807F7EFFFFF6E5FFFFF4E4FFFFF0E1FFFFEFE2FFFFEFE0FFFFEDE2FFFFEE
      E2FFFFEEE2FFFFEFE3FFFFEFE4FFFFF0E5FFFFF0E5FFFFF0E6FFFFF1E8FFFFF1
      E9FFFFF2E9FFFFF3EAFFFFF3EAFFFFF4E8FFFFF2E3FFFFF5E5FFF8EFDFFF8180
      7FFF8B8886FF8B8886FFAEACABFFFBFBFBFF0000000000000000000000000000
      0000000000000000000000000000E9E9E9FF497FAEFF48423DFF484645FF4847
      47FF484747FF484747FF484747FF484747FF484747FF484747FF484747FF4847
      47FF484747FF484545FF463E39FF61A3D8FF5F9CCCFF5F9ACAFF5F9ACAFF5E99
      CAFF5996C8FFBBD5EBFF4981ADFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD5D5D5FF487FADFF48423DFF484645FF4847
      47FF484747FF484747FF484747FF484747FF484747FF484747FF484747FF4847
      47FF484747FF484545FF463E39FF61A3D8FF5F9CCCFF5F9ACAFF5F9ACAFF5E99
      CAFF5996C8FFBBD5EBFF4981ADFFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF4B7FB0FF48423EFF484645FF4847
      47FF484747FF484747FF484747FF484747FF484747FF484747FF484747FF4847
      47FF484747FF484545FF463E39FF61A3D8FF5F9CCCFF5F9ACAFF5F9ACAFF5E99
      CAFF5996C8FFBBD5EBFF4981ADFFC3C3C3FF000000000000000000000000FEFE
      FEFFCFCFCFFFD1CABDFFFFEFDDFFFAE5D3FFFAE6D4FFFAE5D4FFF9E6D5FFF9E7
      D6FFF9E7D7FFF9E8D8FFF9E9D9FFFAE9DAFFFAEADCFFFAEBDDFFFAECDFFFFAEC
      E0FFFBEEE2FFFBEDE0FFFAE7D6FFF9E2CFFFFBE2CDFFFFF1DEFFADA9A1FFCFD0
      CFFFFEFEFEFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E9E9E9FF4A7FAEFF463F3BFF464443FF4645
      45FF464545FF464545FF464545FF464545FF464545FF464545FF464545FF4645
      45FF464545FF464343FF433C37FF62A4DAFF609CCEFF609BCCFF609BCCFF5F9A
      CCFF5997CAFFC0D9EDFF477EABFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD5D5D5FF497FAEFF463F3BFF464443FF4645
      45FF464545FF464545FF464545FF464545FF464545FF464545FF464545FF4645
      45FF464545FF464343FF433C37FF62A4DAFF609CCEFF609BCCFF609BCCFF5F9A
      CCFF5997CAFFC0D9EDFF477EABFFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC3C3C3FF4A7FAFFF463F3BFF464443FF4645
      45FF464545FF464545FF464545FF464545FF464545FF464545FF464545FF4645
      45FF464545FF464343FF433C37FF62A4DAFF609CCEFF609BCCFF609BCCFF5F9A
      CCFF5997CAFFC0D9EDFF477EABFFC3C3C3FF0000000000000000000000000000
      0000F4F4F4FF8D8B88FFFFF8E3FFF8E0C9FFF6E2CDFFF6E2CFFFF6E3D0FFF6E5
      D1FFF6E6D4FFF8E6D5FFF8E7D6FFF7E8D7FFF7E8D7FFF6E4D2FFF6E3D0FFF5DF
      CAFFF4DAC1FFF3D8BDFFF3D8BDFFF3D8BDFFF8DEC3FFF5EAD8FF797979FFF4F4
      F4FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EAEAEAFF4980ADFF453E39FF454241FF4543
      42FF454342FF454342FF454342FF454342FF454342FF454342FF454342FF4543
      42FF454342FF444140FF403934FF60A2DBFF5E9BD0FF5F9BCEFF619CCEFF5F9B
      CEFF5A98CCFFC5DCF0FF467DAAFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFD7D7D7FF4981ADFF453E39FF454241FF4543
      42FF454342FF454342FF454342FF454342FF454342FF454342FF454342FF4543
      42FF454342FF444140FF403934FF60A2DBFF5E9BD0FF5F9BCEFF619CCEFF5F9B
      CEFF5A98CCFFC5DCF0FF467DAAFFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC5C5C5FF4980ADFF453E39FF454241FF4543
      42FF454342FF454342FF454342FF454342FF454342FF454342FF454342FF4543
      42FF454342FF444140FF403934FF60A2DBFF5E9BD0FF5F9BCEFF619CCEFF5F9B
      CEFF5A98CCFFC5DCF0FF467DAAFFC3C3C3FF0000000000000000000000000000
      0000FCFCFCFFAEAEAEFFBCB6ACFFFFECD4FFEFD0ACFFEFD0ADFFEED0AEFFEED0
      AEFFEED0AEFFEED0AEFFEED0AEFFEED0AEFFEED0AEFFEED0AEFFEED0AEFFEFD1
      AFFFEFD1AFFFEFD1AFFFEFD1AEFFF1D1ADFFFFF2DDFFA09C96FFAFAFAEFFFCFC
      FCFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F2F2F2FF4980ACFF413730FF423B36FF423C
      37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C
      37FF423C37FF413A35FF3B322AFFBCDCF7FF7AAED9FF5B9ACDFF5F9CCEFF5F9C
      CEFF5A9ACDFFC9DFF2FF457CA7FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFE6E6E6FF4980ACFF413730FF423B36FF423C
      37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C
      37FF423C37FF413A35FF3B322AFFBCDCF7FF7AAED9FF5B9ACDFF5F9CCEFF5F9C
      CEFF5A9ACDFFC9DFF2FF457CA7FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDADADAFF4980ACFF413730FF423B36FF423C
      37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C37FF423C
      37FF423C37FF413A35FF3B322AFFBCDCF7FF7AAED9FF5B9ACDFF5F9CCEFF5F9C
      CEFF5A9ACDFFC9DFF2FF457CA7FFC3C3C3FF0000000000000000000000000000
      000000000000F7F7F7FF7F7F7CFFF1E3D1FFFFEDD0FFF7DBB9FFF2D5B1FFEAC7
      9EFFEAC79EFFE9C79EFFE9C79EFFE9C79EFFE9C79EFFE9C79EFFE9C79EFFEAC7
      9FFFEAC79FFFEECFA8FFF5D9B6FFFFEED0FFDDD2C2FF757675FFF7F7F7FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FBFBFBFF7DA2BFFF4A82AEFF4B83B0FF4B83
      B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83
      B1FF4B83B1FF4A82AFFF447CA9FF80A9CBFFB9D3EAFFA7C9E5FF6CA4D3FF5B9A
      CFFF5999CFFFCDE2F3FF437AA6FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFF7F7F7FF5787ACFF4A82AEFF4B83B0FF4B83
      B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83
      B1FF4B83B1FF4A82AFFF447CA9FF80A9CBFFB9D3EAFFA7C9E5FF6CA4D3FF5B9A
      CFFF5999CFFFCDE2F3FF437AA6FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFF3F3F3FF4C7FA7FF4A82AEFF4B83B0FF4B83
      B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83B1FF4B83
      B1FF4B83B1FF4A82AFFF447CA9FF80A9CBFFB9D3EAFFA7C9E5FF6CA4D3FF5B9A
      CFFF5999CFFFCDE2F3FF437AA6FFC3C3C3FF0000000000000000000000000000
      000000000000FEFEFEFFDBDADAFF777774FFBEB6AAFFDCD0C0FFF0E2CFFFFFFE
      E7FFFFFDE7FFFFFDE7FFFFFDE7FFFFFDE7FFFFFDE7FFFFFDE7FFFFFDE7FFFFFD
      E7FFFFFEE7FFFBEBD7FFDBD0C0FFBFB6AAFF787775FFDCDCDBFFFEFEFEFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FCFCFCFFD0E0EEFFC2DBEEFF9AC2
      E2FF5A9AD0FFCFE3F5FF4177A3FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF9F9F9FFC1D6E8FFC2DBEEFF9AC2
      E2FF5A9AD0FFCFE3F5FF4177A3FFD5D5D5FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF6F6F6FFBCD2E6FFC2DBEEFF9AC2
      E2FF5A9AD0FFCFE3F5FF4177A3FFC3C3C3FF0000000000000000000000000000
      00000000000000000000FEFEFEFFDCDBDBFFB1B0B0FF8F8F8FFF848585FF6968
      69FF686868FF686868FF686868FF686868FF686868FF686868FF686868FF6868
      68FF686869FF848485FF8F8F8FFFB0B0AFFFE7E7E7FFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFEFFF2F5F8FFCADD
      EEFFBDD6EDFFD3E7F7FF4076A2FFEEEEEEFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFDFDFDFFE7ECF2FFC0D7
      EBFFBDD6EDFFD3E7F7FF4076A2FFDEDEDEFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFCFCFCFFDDE5EDFFBFD6
      EAFFBDD6EDFFD3E7F7FF4076A2FFCFCFCFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFE
      FEFFE8EFF5FFCEE3F6FF3E759FFFF9F9F9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFDFD
      FDFFD8E4EEFFCDE3F6FF3E759FFFF3F3F3FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFCFC
      FCFFCCDCE9FFCDE3F6FF3E759FFFEDEDEDFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFEBEBEBFFC4C4C4FFA6A6A6FFA3A3A3FFA3A3A3FFA3A3
      A3FFA3A3A3FFA3A3A3FFA3A3A3FFA3A3A3FFA3A3A3FFA3A3A3FFA3A3A3FFA3A3
      A3FFA3A3A3FFA3A3A3FFA3A3A3FFA4A4A4FFAFAFAFFFBDBDBDFFCFCFCFFFDEDE
      DEFFE7E7E7FFE9E9E9FFEEEEEEFFF9F9F9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFA0A0A0FF9B9B9BFF969696FF969696FF9B9B9BFFA0A0A0FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FBFBFBFFF2F2F2FFEAEAEAFFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FAFAFAFFEFEFEFFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF000000FF000000FF000000FF0000
      00FF000000FF000000FFC4C4C4FF656565FF3E3E3EFF373737FF373737FF3737
      37FF373737FF373737FF373737FF373737FF373737FF373737FF373737FF3737
      37FF373737FF373737FF373737FF3A3A3AFF474747FF5F5F5FFF777777FF9090
      90FFA8A8A8FFB8B8B8FFCDCDCDFFEEEEEEFF000000FF000000FF000000FFF9F9
      F9FFEEEEEEFFE9E9E9FFB3B3B3FF5C5C5CFF393939FF323232FF323232FF3232
      32FF323232FF313131FF2D2D2DFF292929FF292929FF2D2D2DFF313131FF3232
      32FF323232FF323232FF323232FF353535FF414141FF575757FF6D6D6DFF8686
      86FFA4A4A4FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F2F2F2FFD4D4D4FFC0C0C0FFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEFFFCECECEFFBDBDBDFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF000000FF000000FF000000FF0000
      00FF000000FF000000FFA6A6A6FFA47A4AFFB5834DFFB38149FFB38149FFB381
      49FFB38149FFB38149FFB38149FFB58148FFC28441FF2A7ED2FF367EC9FF387D
      C7FF397CC7FF397CC7FF387DC7FF387DC8FF387DC8FF387DC7FF397CC7FF397C
      C7FF387DC7FF397FC8FF3A82CAFFEBEBEBFF000000FF000000FF000000FFEEEE
      EEFFCDCDCDFFBCBCBCFF7A7A7AFF795A37FF856139FF845F36FF845F36FF845F
      36FF845F36FF815D35FF4A7EA0FF318BCEFF318BCEFF2A7EC0FF275B91FF295C
      93FF2A5B93FF2A5B93FF295C93FF295C93FF295C93FF295C93FF2A5B93FF2E64
      A0FF3475BAFF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000F9F9F9FFEEEEEEFFEEEEEEFFF9F9
      F9FF000000000000000000000000EAEAEAFF64986DFF378444FF348241FF3382
      41FF348241FF348241FF348241FF348241FF348241FF348241FF348241FF3382
      41FF348241FF378444FF64986DFFEAEAEAFF0000000000000000000000000000
      000000000000000000000000000000000000F9F9F9FFEEEEEEFFEEEEEEFFF9F9
      F9FF00000000E9E9E9FFB3B3B1FFAFAFADFFAEAEABFFADADABFFADADABFFADAD
      ABFFADADABFFADADABFFADADABFFADADABFFADADABFFADADABFFADADABFFADAD
      ABFFAEAEABFFAFAFADFFB6B6B5FFEAEAEAFF000000FF000000FF000000FF0000
      00FF000000FF000000FFA3A3A3FFB5834CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF247BCDFF4EE0FFFF55E3
      FFFF56E1FFFF56E2FFFF55E4FFFF53E7FFFF53E7FFFF55E4FFFF56E2FFFF56E1
      FFFF55E3FFFF53E2FFFF3B86CDFFF0F0F0FF000000FF000000FF000000FFEEEE
      EEFF525051FF525051FF525051FF525051FF525051FF525051FF525051FF5250
      51FF534F4FFF554A44FF3895DCFF65E4FCFF65E4FCFF3895DCFF554A44FF534F
      4FFF525051FF525051FF525051FF525051FF525051FF525051FF525051FF5250
      51FF4FD4EEFF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000EEEEEEFFCDCDCDFFC8C8C8FFE3E3
      E3FFF9F9F9FF0000000000000000E9E9E9FF378444FFE0ECE2FFDCE8DDFFDCE8
      DDFFDCE7DCFFDBE7DCFFDCE7DCFFDCE8DDFFDDE8DDFFDDE8DDFFDCE8DDFFDCE8
      DDFFDCE8DDFFE0ECE2FF378444FFE9E9E9FF0000000000000000000000000000
      000000000000000000000000000000000000EEEEEEFFCDCDCDFFC8C8C8FFE3E3
      E3FFF9F9F9FFE9E9E9FFB0B0AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB0B0ADFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FF000000FFA3A3A3FFB48148FFFFFFFFFFFFF7EBFFFFF6EAFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF7EBFFFFFCECFF7AA6D3FF6FB6E6FF4FD9
      FFFF4AD4FFFF4CD4FFFF4AD8FFFF8A6557FF8A6557FF4AD8FFFF4CD4FFFF4AD4
      FFFF51DAFFFF7BBCE7FF5F8FC0FFEFEFEFFF000000FF000000FF000000FFF9F9
      F9FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B
      78FF7F7A77FF82756DFF4098DFFF91F7FFFF91F7FFFF4098DFFF82756DFF7F7A
      77FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B78FF7E7B
      78FF4FD5F9FF000000FF000000FF000000FF0000000000000000000000000000
      0000FCFCFCFFF4F4F4FFEBEBEBFFE9E9E9FFD8D8D8FFBA851CFFB88419FFC2C2
      C2FFE3E3E3FFF9F9F9FF00000000E9E9E9FF348242FFDCE8DDFF47B06BFF44AE
      68FF41AC65FF40AC65FF41AD66FF44AE68FF47AF6BFF45AE69FF42AD67FF44AE
      68FF47B06BFFDCE8DDFF348242FFE9E9E9FF0000000000000000000000000000
      0000FCFCFCFFF4F4F4FFEBEBEBFFE9E9E9FFD8D8D8FFBA851CFFB88319FFC2C2
      C2FFE3E3E3FFE3E3E3FFAFAFAEFFFFFFFFFFF9F8FEFFFFFFFEFFFFFFFEFFFFFF
      FDFFFFFFFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFE
      FDFFFFFFFEFFFFFFFFFFAEAEABFFE9E9E9FFEBEBEBFFC4C4C4FFA6A6A6FFA3A3
      A3FFA3A3A3FFA3A3A3FF6D6D6DFFB58146FFFFFFFFFFFFEEDAFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFF1DCFFFFEFD9FF377ECAFF9DE2
      FFFF3DCCFFFF42CCFFFF42D1FFFF40D9FFFF40D9FFFF42D1FFFF42CCFFFF3DCC
      FFFFA2E5FFFF3E82CAFF5D6E7EFFD8D8D8FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFFCEBD8FF80B1D9FF4B9DE2FF4C9DE3FF80B3D9FF367DC8FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000000000000000000000FAFA
      FAFFEAEAEAFFD3D3D3FFC2C2C2FFBCBCBCFFB7B7B7FFB78217FFFCDFB2FFB580
      14FFC2C2C2FFE3E3E3FFF9F9F9FFE9E9E9FF338241FFDDE8DDFF42AA64FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF73BF8CFFC4E4CEFFFFFFFFFFFFFF
      FFFF42AA64FFDDE8DDFF348241FFE9E9E9FF000000000000000000000000FAFA
      FAFFEAEAEAFFD3D3D3FFC2C2C2FFBCBCBCFFB7B7B7FFB78217FFFCDFB1FFB57F
      11FFC2C2C2FFCDCDCDFFAFB1B1FFFFFFFFFF301EEDFF4433EEFFFBFBFBFFFFFF
      FBFFFCFCFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFB
      FBFFFCFCFCFFFFFFFFFFADADABFFE9E9E9FFC4C4C4FF656565FF3E3E3EFF3737
      37FF373737FF373737FF2D2D2DFFB78145FFFFFFFFFFFFE6CAFFFFE6CBFFFFE6
      CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE7CCFFFFECCCFFA6B8C9FF5699
      D8FF8FE3FFFF35C7FFFF38CFFFFF64483EFF64483EFF38CFFFFF35C7FFFF91E4
      FFFF61A1DBFF284D98FF1C326EFFB8B8B8FF000000FF000000FF000000FF0000
      00FF000000FF000000FF2C2C2CFFAE7A41FFEAEAEAFFE9D2B9FFE9D2B9FFE9D2
      BAFFE9D2BAFFE9D2BAFFD8C3ADFF7F7269FF4F433EFFD8C8ADFF98A8B8FF4F8C
      C5FF83CFE9FF30B6E9FF33BDE9FF5C4239FF5F443BFF37CCFBFF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000FCFCFCFFEAEA
      EAFFCACACAFFB59B65FFB78319FFB68218FFB68116FFB37E11FFF6D6A2FFF2C6
      81FFB68015FFC2C2C2FFE3E3E3FFE3E3E3FF328242FFDDE9DEFF42A560FFC8E5
      D1FFFFFFFFFFFFFFFFFFB7DDC3FF2D9A4EFFBDE0C8FFFFFFFFFFFFFFFFFFC8E5
      D1FF42A560FFDDE9DEFF348241FFE9E9E9FF0000000000000000FCFCFCFFEAEA
      EAFFCACACAFFB59B65FFB78319FFB68218FFB68116FFB37E11FFF6D6A2FFF2C6
      81FFB57E0FFFB1B1B1FFAEB2B8FFFFFFFFFF6557F1FFA098F4FF6759F1FFFFFF
      FAFFFDFEF9FFFAFAF9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFFAFAF9FFFAFB
      F9FFFBFBFAFFFFFFFFFFADADABFFE9E9E9FFA4A4A4FF777777FF808080FF7E7E
      7EFF7E7E7EFF7F8081FF7D8186FFB98144FFFFFFFFFFFFDFB7FFFFDFB8FFFFDF
      B9FFFFDFB9FFFFDFB9FFFFDFB9FFFFDFB9FFFFDFB9FFFFE1B9FFFFE7B8FF5A8E
      C1FF9BCCF0FF69D5FFFF2AC9FFFF69564CFF69564CFF2AC9FFFF69D5FFFF9DCE
      F1FF3D78B5FF897F75FF777374FFA2A2A2FF000000FF000000FF000000FF0000
      00FF000000FF000000FF777A7FFF9A6B39FFC0C0C0FFBCA487FFBCA488FFBCA4
      88FFBCA488FFBCA488FFB7A085FF727275FF434449FFB7A185FFBCAA88FF4269
      8EFF7296B1FF4D9DBCFF1F94BCFF4F4139FF57473FFF28BFF2FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFF1F1F1FFE5E5E5FFCECE
      CEFFB88829FFCB993AFFEDBC6EFFEFBF72FFEFBE71FFEEBE70FFE6AD4FFFE6AE
      50FFF3C986FFB88115FFC2C2C2FFCDCDCDFF2F8244FFDEEADFFF44A361FF3FA0
      5DFFC4E1CCFFFFFFFFFFFFFFFFFFEEF6F0FFFFFFFFFFFFFFFFFFC4E1CCFF40A0
      5DFF44A361FFDEEADFFF348242FFE9E9E9FFFBFBFBFFF1F1F1FFE5E5E5FFCECE
      CEFFB88829FFCB993AFFEDBC6EFFEFBF72FFEFBE71FFEEBE70FFE6AD4FFFE6AE
      50FFF2C884FFB57C07FFACB3BDFFFFFFFFFFFFFFF9FF968EF1FF3522EDFFBBB5
      F4FFFFFFF7FFFEFEF7FFFBFAF7FFFAF9F7FFFBFAF7FFFDFCF7FFFFFFF7FFFFFF
      F7FFFFFFF8FFFFFFFFFFAEAEABFFE9E9E9FF9A9A9AFF808080FFEAE7E6FFE2DF
      DEFFE2E0DFFFE6E4E4FF757578FFBD8444FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFA
      FFFF3883CFFFBBEDFFFF3ECCFFFF6C5B53FF6C5B53FF3DCCFFFFBAECFFFF3681
      CCFFC7CED6FFEEE8E4FF81807FFF9A9A9AFF989898FF797979FFD7D4D3FFCFCC
      CBFFCFCDCCFFD2D0D0FF646467FFAB7C46FFB6844CFFB48149FFB48149FFB481
      49FFB48149FFB48148FFB68148FFB98348FFBA8448FFB78248FFB48148FFB481
      48FFB48148FFB48148FFB48149FFB6844CFF99734AFF34AEDAFFAAD8E9FF3176
      BAFFB6BCC4FFDAD5D1FF7A7979FF989898FFF1F1F1FFD2D2D2FFBCBCBCFFB49B
      69FFC8922FFFECB861FFE9B35AFFE7B157FFE7B156FFE7B257FFE2A540FFE3A7
      43FFE5A843FFF5CD8DFFBD8215FFB1B1B1FF2A8244FFDEEAE1FF45A05FFF48A1
      61FF3B9A56FFBBDCC4FFFFFFFFFFFFFFFFFFFFFFFFFFBBDCC4FF3B9A56FF48A1
      61FF45A05FFFDFEAE0FF348242FFE9E9E9FFF1F1F1FFD2D2D2FFBCBCBCFFB49B
      69FFC8922FFFECB861FFE9B35AFFE7B157FFE7B156FFE7B257FFE2A540FFE3A7
      43FFE5A842FFF4C983FFB27300FFFFFFFFFFFAFBF8FFFFFFF6FFFAFBF6FF240F
      ECFF998FF2FFC2BDF4FFFFFFF7FFFFFFF6FFFFFFF7FFFFFFF7FFB9B2F3FF867C
      F2FF8177F2FFFFFFFFFFAFAFABFFE9E9E9FF959595FF7E7E7EFFE3E1E0FFD6D4
      D2FFD6D4D2FFDAD9D7FF959494FF6B6B6DFF6B6A6BFF6A6868FF7C7A78FF7C7A
      78FF7B7A78FF7B7A78FF7B7A78FF7B7A78FF7B7A78FF7C7A78FF7E7B78FF857D
      76FF4E79A4FF70AFE6FFB2F0FFFF6D5B52FF6D5B52FFB1EFFFFF6BABE2FF759F
      CAFFE0D8D1FFE5E2E0FF7E7E7EFF959595FF8D8D8DFF696969FFABA9A9FF9E9C
      9BFF9E9C9BFFA1A09FFF6B6A6AFFB78348FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB78248FF7FACB7FF4F7EA7FF5675
      95FFA59F9AFFACAAA9FF696969FF8D8D8DFFEAEAEAFFBA913FFFB78114FFB681
      16FFE3A94AFFE7AB4AFFEFC98AFFF6DAADFFF7DAADFFF6DAADFFF1D19DFFE29E
      32FFF1CA8AFFB07704FFB57906FFC17C06FF257F40FFDDEAE0FF419B5BFF459C
      5CFF379651FFBBDBC3FFFFFFFFFFFFFFFFFFFFFFFFFFBBDBC3FF379651FF459C
      5CFF419B5BFFE0EBE1FF348242FFE9E9E9FFEAEAEAFFBA913FFFB78114FFB681
      16FFE3A94AFFE7AB4AFFEFC98AFFF6DAADFFF7DAADFFF6DAADFFF1D19DFFE29F
      32FFF2CB8AFFB27700FFAAB0B9FFFFFFFFFFF6F5F5FFF8F7F4FFFFFFF4FF867A
      F0FF8B81F0FFB7B2F1FF7265EFFF776BEFFF5849EEFF2712EDFF1602EBFF6A5C
      EFFF5040EEFFFFFFFFFFAFAFABFFE9E9E9FF959595FF7D7E7EFFE3E0DFFFCFCD
      CBFFCFCDCBFFD1CFCDFFD7D5D3FF8E8C8AFF61605FFF636160FF626160FF6260
      5FFF62605FFF62605FFF62605FFF62605FFF62605FFF62605FFF63605FFF6561
      5DFF64615DFF2877C6FF9CD5FAFF90E2FFFF90E1FFFF9AD4FAFF2B79C8FFD0CD
      CAFFD4D0CBFFE3E1DFFF7D7E7EFF959595FF898989FF868686FF959595FF9494
      94FF949494FF959797FF95999EFFB88043FFFFFFFFFFFFF0DDFFFFF0DDFFFFF0
      DEFFFFF0DEFFFFF0DEFFFFF0DEFFFFF0DDFFFFF0DDFFFFF0DDFFFFF0DDFFFFF0
      DDFFFFF0DDFFFFF0DCFFFFF0DCFFFFFFFFFFB88043FF95999EFF959797FF9494
      94FF949494FF959595FF868686FF898989FFE9E9E9FFB67F11FFFFFFFFFFB37C
      0CFFE5A43BFFEFC887FFE7C07CFFB47A05FFB07703FFB07906FFF4D29DFFF1CC
      90FFAE7300FFFFFFFFFFFFFFFFFFFFFFFFFF227A35FFDDEADFFF3B9553FF3892
      4FFFC2DDC9FFFFFFFFFFFFFFFFFFEEF7F1FFFFFFFFFFFFFFFFFFC2DDC9FF3892
      4FFF3B9553FFDFEBE1FF348242FFE9E9E9FFE9E9E9FFB67F11FFFFFFFFFFB37C
      0CFFE5A43BFFEFC887FFE7C07CFFB47A05FFB07703FFB07906FFF4D29DFFF1CD
      90FFB07500FFF6FDFFFFA9ABADFFFFFFFFFFF3F3F2FFF3F3F2FFF8F8F2FFFFFF
      F3FF230EEDFFFBFDF3FFFFFFF3FF695BEFFF776BEFFFF8F9F1FFD4D0F1FFF1F1
      F1FFFEFFF2FFFFFFFFFFAEAEABFFE9E9E9FF959595FF7E7E7EFFE3E2E1FFCBC8
      C7FFCBC8C7FFCCC8C8FFCECBCAFFD2CECDFFD5D1D1FFD6D3D2FFD6D3D2FFD6D3
      D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD7D3
      D1FFDBD3CEFFA3B6CAFF3F8CD5FFC1EFFFFFC1EFFFFF3E8BD4FF9FB3C6FFD3CB
      C6FFCDC8C7FFE3E2E1FF7E7E7EFF959595FF888888FF959595FFE5E4E2FFDEDD
      DCFFDFDEDDFFE3E3E2FF757476FFBD8444FFFFFFFFFFFFEBC6FFFFECC8FFFFEC
      C8FFFFECC8FFFFECC8FFFFECC8FFFFECC8FFFFECC8FFFFECC8FFFFECC8FFFFEC
      C8FFFFECC8FFFFECC8FFFFEBC6FFFFFFFFFFBD8444FF757476FFE3E3E2FFDFDE
      DDFFDEDDDCFFE5E4E2FF959595FF888888FFE9E9E9FFB57D0CFFFFFFFFFFB179
      07FFE29B29FFF6DAAFFFB07804FFDFCEC4FFFFFFFFFFB17703FFF6D39DFFAE74
      00FFDBC7B8FFFFFFFFFFFFFFFFFFFFFFFFFF207730FFDBE9DEFF328D49FFC4DE
      CAFFFFFFFFFFFFFFFFFFBEDBC5FF298841FFBEDBC5FFFFFFFFFFFFFFFFFFC4DE
      CAFF328D49FFDFEBE0FF348242FFE9E9E9FFE9E9E9FFB57D0CFFFFFFFFFFB179
      07FFE29B29FFF6DAAFFFB07804FFDFCEC4FFFFFFFFFFB17703FFF6D39DFFAF74
      00FFDECABAFFF4F7FCFFA8A9A8FFFFFFFFFFF1F1F0FFF1F1F0FFF3F3F0FFFBFC
      F0FF8278EEFFABA4EFFF8A7FEFFF8379EEFFFFFFF0FFF8F9F0FFF6F7F0FFF4F5
      F0FFF3F3F0FFFFFFFFFFADADABFFE9E9E9FF959595FF7E7E7EFFE5E3E3FFC5C2
      C1FFC6C3C2FFC6C3C2FFC8C5C4FFCBC8C7FFCDCAC9FFCECBCAFFCECBCAFFCECB
      CAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECB
      CAFFD1CCCAFFD8CEC7FF518FCBFF74BAF0FF73B9EFFF4D8BC7FFD2C8C1FFC9C4
      C2FFC6C2C1FFE5E3E3FF7E7E7EFF959595FF888888FF949494FFE0DFDEFFD5D4
      D2FFD5D4D2FFD9D8D6FF959494FF6C6B6DFF6C6B6CFF6C6A6CFF6C6B6DFF6C6B
      6DFF6C6B6DFF6C6B6DFF6C6B6DFF6C6B6DFF6C6B6DFF6C6B6DFF6C6B6DFF6C6B
      6DFF6C6B6DFF6C6B6DFF6C6A6CFF6C6B6CFF6C6B6DFF959494FFD9D8D6FFD5D4
      D2FFD5D4D2FFE0DFDEFF949494FF888888FFE9E9E9FFB57C0BFFFFFFFFFFB077
      04FFE39F2EFFF7DCB1FFB27803FFDDC9BAFFFFFFFFFFAE7100FFAE7200FFFFFF
      FFFFD8C0A9FFFFFFFFFFFFFFFFFFFFFFFFFF207630FFDAE9DCFF2C853FFFFFFF
      FFFFFFFFFFFFC3DCC9FF348A47FF3B8E4EFF348A47FFC3DCC9FFFFFFFFFFFFFF
      FFFF2F8742FFDEEBE1FF348242FFE9E9E9FFE9E9E9FFB57C0BFFFFFFFFFFB077
      04FFE39F2EFFF7DCB1FFB27803FFDDC9BAFFFFFFFFFFAE7100FFAE7200FFFFFF
      FFFFDBC3ACFFF3F3F3FFA8A8A7FFFFFFFFFFF0EFEFFFF0EFEFFFF1F0EFFFF5F5
      EFFFF1F2EEFF3928EDFF3726EDFFFFFFEFFFF5F5EFFFF1F0EFFFF0EFEFFFF0EF
      EFFFF0EFEFFFFFFFFFFFADADABFFE9E9E9FF959595FF7D7E7EFFE8E6E6FFBFBD
      BBFFC1BFBDFFC3C1BFFFC7C5C3FF979593FF7B7977FF7D7B79FF7D7B79FF7D7B
      79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B
      79FF7E7B79FF817C77FF7C7D7FFF2885DAFF2482D7FFBDBEC0FFC7C2BEFFC1BF
      BDFFBFBDBBFFE8E6E6FF7D7E7EFF959595FF888888FF949494FFE0DFDDFFCECD
      CBFFCECDCBFFD0CFCDFFD6D5D3FF8E8C8AFF63605FFF646161FF646261FF6462
      61FF646261FF646261FF646261FF646261FF646261FF646261FF646261FF6462
      61FF646261FF646261FF646161FF63605FFF8E8C8AFFD6D5D3FFD0CFCDFFCECD
      CBFFCECDCBFFE0DFDDFF949494FF888888FFE9E9E9FFB57C0AFFFFFFFFFFB075
      00FFFBE2BBFFFBE2BAFFB27801FFDDC8B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFD8BFA4FFFFFFFFFFFFFFFFFFFFFFFFFF1D7730FFD8E8DCFF6CA877FF2B82
      3DFF2F8440FF358846FF3C8C4DFF3F8D4FFF3C8C4DFF358846FF308542FF3186
      43FF318542FFDFECE1FF348342FFE9E9E9FFE9E9E9FFB57C0AFFFFFFFFFFB075
      00FFFBE2BBFFFBE2BAFFB27801FFDDC8B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFDBC2A6FFF4F3F1FFA8A9A8FFFFFFFFFFEDECECFFEEEDEDFFEEEDEDFFF1F0
      EDFFFDFDEDFF5749EDFFA69EEDFFF9F9EDFFF0EFEDFFEEEDEDFFEEEDEDFFEEED
      EDFFEDECECFFFFFFFFFFADADABFFE9E9E9FF959595FF7D7D7DFFECEAEAFFBAB7
      B5FFBCB9B7FFC1BDBBFF8F8D8DFF767473FF797776FF797777FF797877FF7978
      77FF797877FF797877FF797877FF797877FF797877FF797877FF797877FF7978
      77FF7A7877FF7B7876FF7D7874FF7F7771FF7D746EFF938E8BFFC2BEBBFFBDB9
      B7FFBAB7B5FFECEAEAFF7D7D7DFF959595FF888888FF949494FFE0E0DFFFCAC7
      C7FFCAC8C7FFCBC8C8FFCDCBCAFFD0CECDFFD3D1D1FFD5D3D2FFD5D3D2FFD5D3
      D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3
      D2FFD5D3D2FFD5D3D2FFD5D3D2FFD3D1D1FFD0CECDFFCDCBCAFFCBC8C8FFCAC8
      C7FFCAC7C7FFE0E0DFFF949494FF888888FFE9E9E9FFB57C0AFFFFFFFFFFBF91
      3DFFAF7300FFAF7400FFC29441FFDCC5AEFFD8BFA3FFD7BDA0FFD7BDA0FFD8BE
      A1FFD9C0A4FFD8BDA0FFD8BC9FFFE0C0A4FF68945CFF8FBD9BFFD8E8DBFF6EA7
      78FF30813FFF338342FF348443FF358444FF348443FF338442FF338341FF3183
      40FF2C7F3CFFDEECE1FF348342FFE9E9E9FFE9E9E9FFB57C0AFFFFFFFFFFBF91
      3DFFAF7300FFAF7400FFC29441FFDCC5AEFFD8BFA3FFD7BDA0FFD7BDA0FFD8BE
      A2FFDBC2A5FFD1BDA5FFA9AAA9FFFFFFFFFFEBEBEAFFECECEBFFECECEBFFEEEF
      EBFFF9FBEBFF5545ECFFC1BCECFFF3F4EBFFEDEDEBFFECECEBFFECECEBFFEBEB
      EAFFEBEBEAFFFFFFFFFFADADABFFE9E9E9FF959595FF7D7D7DFFEFEEEDFFB2AE
      ACFFB4B0AEFFBAB7B5FF676564FF6E6C6BFF706E6DFF706E6DFF706E6DFF706E
      6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E
      6DFF706E6DFF706E6DFF706E6DFF716E6CFF6F6C6AFF676563FFBAB7B5FFB4B0
      AEFFB2AEACFFEFEEEDFF7D7D7DFF959595FF888888FF949494FFE3E1E1FFC4C1
      C1FFC5C3C2FFC5C3C2FFC7C5C4FFCAC8C7FFCCCAC9FFCDCBCAFFCDCBCAFFCDCB
      CAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCB
      CAFFCDCBCAFFCDCBCAFFCDCBCAFFCCCAC9FFCAC8C7FFC7C5C4FFC5C3C2FFC5C3
      C2FFC4C1C1FFE3E1E1FF949494FF888888FFE9E9E9FFB47B08FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFD7BEA1FFFFFFFDFFFFFFF8FFFFFFF8FFFFFF
      FCFFD6BD9DFFFFFFFCFFFFFFF9FFFFFFFBFFFFFFFFFF68935BFF91BD9CFFDAE9
      DDFFDDEADFFFDEEBE0FFDEECE1FFDFECE2FFE0EDE3FFE1EDE3FFE1EDE3FFE0ED
      E2FFDFECE1FFE1EDE3FF378444FFEFEFEFFFE9E9E9FFB47B08FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFD7BEA1FFFFFFFDFFFFFFF8FFFFFFF8FFFFFF
      FCFFD9BF9FFFF3F2EDFFA8AAA8FFFFFFFFFFEAE9E8FFEBEAE9FFEBEAE9FFEDED
      E9FFF9F9E9FF2410EEFFBEB9EAFFF1F1E9FFEBEAE8FFEAE8E7FFEAE9E7FFE9E8
      E7FFE9E8E7FFFFFFFFFFADADABFFE9E9E9FF959595FF7D7D7DFFF2F1F0FFECEC
      EBFFEDEDECFFF5F4F3FF797776FF807E7DFF817F7EFF817F7EFF817F7EFF817F
      7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F
      7EFF817F7EFF817F7EFF817F7EFF817F7EFF807E7DFF797776FFF5F4F3FFEDED
      ECFFECECEBFFF2F1F0FF7D7D7DFF959595FF888888FF949494FFE6E3E4FFBFBD
      BBFFC1BFBDFFC3C1BFFFC7C5C3FF979593FF7D7977FF7E7B79FF7E7B79FF7E7B
      79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B
      79FF7E7B79FF7E7B79FF7E7B79FF7D7977FF979593FFC7C5C3FFC3C1BFFFC1BF
      BDFFBFBDBBFFE6E3E4FF949494FF888888FFE9E9E9FFB47A08FFFFFFFFFFFFFC
      F3FFFFFCF2FFFFFCF3FFFFFFF7FFD6BA9AFFFFFFF6FFFFFBF1FFFFFBF1FFFFFF
      F6FFD6BA99FFFFFFF6FFFFFBF1FFFFFCF2FFFFFFF9FFE4C1A4FF4B9358FF1F77
      31FF207832FF217832FF247A36FF2A7F3FFF308242FF338342FF348342FF3483
      42FF348342FF378444FF478E54FFFAFAFAFFE9E9E9FFB47A08FFFFFFFFFFFFFC
      F3FFFFFCF2FFFFFCF3FFFFFFF7FFD6BA9AFFFFFFF6FFFFFBF1FFFFFBF1FFFFFF
      F6FFD8BC9BFFF2F0E8FFA8A9A8FFFFFFFFFFE8E8E7FFE9E9E8FFE9E9E8FFEBEC
      E8FFF5F7E8FF6254EBFFBCB7E9FFEFEFE7FFF3F3F1FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAEAEABFFE9E9E9FF959595FF7D7D7DFFD7D5D4FFC8C5
      C4FFC8C6C5FFCECDCCFF969391FF9C9997FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9C9997FF969391FFCECDCCFFC8C6
      C5FFC8C5C4FFD7D5D4FF7D7D7DFF959595FF888888FF939494FFEAE7E8FFB9B7
      B5FFBBB9B7FFBFBDBBFF918D8DFF777473FF7A7776FF7A7777FF7B7877FF7B78
      77FF7B7877FF7B7877FF7B7877FF7B7877FF7B7877FF7B7877FF7B7877FF7B78
      77FF7B7877FF7B7877FF7A7777FF7A7776FF777473FF918D8DFFBFBDBBFFBBB9
      B7FFB9B7B5FFEAE7E8FF939494FF888888FFE9E9E9FFB47A08FFFFFFFFFFFFFD
      F1FFFFFCF0FFFFFDF2FFFFFFF6FFD6BB9AFFFFFFF6FFFFFDF2FFFFFDF2FFFFFF
      F6FFD6BB9AFFFFFFF6FFFFFDF2FFFFFDF2FFFFFFF7FFDCBD9EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7E15FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47A08FFFFFFFFFFFFFD
      F1FFFFFCF0FFFFFDF2FFFFFFF6FFD6BB9AFFFFFFF6FFFFFDF2FFFFFDF2FFFFFF
      F6FFD9BD9BFFF3F0E7FFA8AAA9FFFFFFFFFFE7E6E4FFE8E7E6FFE8E7E6FFEAE9
      E6FFF1F1E6FF3B2AECFF978EE8FFECECE4FFFFFFFFFFCBCBCAFFA8A8A5FFA7A7
      A5FFA5A5A3FFFFFFFFFFAFAFADFFEFEFEFFF959595FF7D7D7DFFEAE9E8FFCECC
      CBFFCFCDCCFFD5D3D2FF868382FF8C8988FF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8C8988FF868382FFD5D3D2FFCFCD
      CCFFCECCCBFFEAE9E8FF7D7D7DFF959595FF888888FF939393FFEDECEBFFB1AE
      ACFFB4B0AEFFBAB7B5FF686564FF6F6C6BFF716E6DFF716E6DFF716E6DFF716E
      6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E
      6DFF716E6DFF716E6DFF716E6DFF716E6DFF6F6C6BFF686564FFBAB7B5FFB4B0
      AEFFB1AEACFFEDECEBFF939393FF888888FFE9E9E9FFB47B08FFFFFFFFFFD4B7
      92FFD4B893FFD5B895FFD7BB98FFD8BD9BFFD7BB98FFD5B895FFD5B895FFD7BB
      98FFD8BD9BFFD7BB98FFD5B895FFD5B895FFD7BB98FFD9BD9BFFD9BC9AFFD8BA
      98FFD7BA96FFD7B995FFFFFFFFFFBB7B07FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47B08FFFFFFFFFFD4B7
      92FFD4B893FFD5B895FFD7BB98FFD8BD9BFFD7BB98FFD5B895FFD5B895FFD7BB
      98FFDABE9CFFD0BB9FFFA9AAAAFFFFFFFFFFE4E3E2FFE6E5E4FFE6E5E4FFE7E6
      E4FFEAE9E4FFEEEEE4FFECECE3FFE7E6E2FFFFFFFFFFA8A8A5FFF7F7F6FFDFDE
      DDFFFFFFFFFFE9E9E9FFB5B5B3FFFAFAFAFF979797FF7C7D7DFFF9F9F8FFD3D1
      D0FFD4D2D1FFDCDAD9FF797675FF807D7CFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF807D7CFF797675FFDCDAD9FFD4D2
      D1FFD3D1D0FFF9F9F8FF7C7D7DFF979797FF8A8A8AFF939393FFF0EFEEFFECEB
      EBFFEDEDECFFF5F4F3FF7A7776FF817E7DFF827F7EFF827F7EFF827F7EFF827F
      7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F
      7EFF827F7EFF827F7EFF827F7EFF827F7EFF817E7DFF7A7776FFF5F4F3FFEDED
      ECFFECEBEBFFF0EFEEFF939393FF8A8A8AFFE9E9E9FFB47A08FFFFFFFFFFFFFB
      EAFFFFFAEAFFFFFBECFFFFFEF0FFD6BA96FFFFFEF0FFFFFBECFFFFFBECFFFFFE
      F0FFD6BA96FFFFFEF0FFFFFBECFFFFFBECFFFFFEF0FFD6BA96FFFFFEF0FFFFFB
      ECFFFFFAEAFFFFFBEAFFFFFFFFFFB57A07FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47A08FFFFFFFFFFFFFB
      EAFFFFFAEAFFFFFBECFFFFFEF0FFD6BA96FFFFFEF0FFFFFBECFFFFFBECFFFFFE
      F0FFD9BC98FFF3EFE3FFA8AAAAFFFFFFFFFFE2E1E1FFE3E2E2FFE3E2E3FFE4E3
      E3FFE4E3E3FFE5E4E3FFE4E3E2FFE2E1E1FFFFFFFFFFA7A7A5FFDFDEDDFFFFFF
      FFFFE7E7E8FFB5B5B2FFFAFAFAFF00000000B3B3B3FF7E7E7EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF6A6868FF6E6C6EFF545657FF535454FF535354FF5353
      54FF535354FF535354FF535354FF535354FF535354FF535354FF535354FF5353
      54FF535354FF535354FF535454FF545657FF6E6C6EFF6A6868FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF7E7E7EFFB3B3B3FFA4A4A4FF949494FFD5D3D2FFC7C5
      C3FFC8C6C4FFCECCCAFF969391FF9C9997FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9C9997FF969391FFCECCCAFFC8C6
      C4FFC7C5C3FFD5D3D2FF949494FFA4A4A4FFE9E9E9FFB47A08FFFFFFFFFFFFF7
      E4FFFFF7E4FFFFF7E5FFFFFBEAFFD6B792FFFFFBEAFFFFF7E5FFFFF7E5FFFFFB
      EAFFD6B792FFFFFBEAFFFFF7E5FFFFF7E5FFFFFBEAFFD6B792FFFFFBEAFFFFF7
      E5FFFFF7E4FFFFF7E4FFFFFFFFFFB47A08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF7
      E4FFFFF7E4FFFFF7E5FFFFFBEAFFD6B792FFFFFBEAFFFFF7E5FFFFF7E5FFFFFB
      EAFFD8B994FFF2EDDEFFA8AAAAFFFFFFFFFFE0DFDEFFE1E0DFFFE1E0DFFFE1E0
      DFFFE1E0DFFFE1E0DFFFE1E0DFFFE0DFDEFFFFFFFFFFA5A5A3FFFFFFFFFFE7E7
      E8FFB4B4B2FFFAFAFAFF0000000000000000D9D9D9FF7F7F7FFF7E7E7EFF7C7C
      7CFF7C7C7CFF7E7F80FF808387FFC08646FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC08646FF808387FF7E7F80FF7C7C
      7CFF7C7C7CFF7E7E7EFF7F7F7FFFD9D9D9FFC6C6C6FF939393FFE8E7E5FFCECB
      CAFFCFCDCCFFD5D3D3FF868482FF8C8A88FF8D8B89FF8D8B89FF8D8B89FF8D8B
      89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B
      89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8C8A88FF868482FFD5D3D3FFCFCD
      CCFFCECBCAFFE8E7E5FF939393FFC6C6C6FFE9E9E9FFB47A08FFFFFFFFFFFFF8
      E4FFFFF8E4FFFFF9E5FFFFFCE9FFD6B892FFFFFCE9FFFFF9E5FFFFF9E5FFFFFC
      E9FFD6B892FFFFFCE9FFFFF9E5FFFFF9E5FFFFFCE9FFD6B892FFFFFCE9FFFFF9
      E5FFFFF8E4FFFFF8E4FFFFFFFFFFB47A08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF8
      E4FFFFF8E4FFFFF9E5FFFFFCE9FFD6B892FFFFFCE9FFFFF9E5FFFFF9E5FFFFFC
      E9FFD9BA94FFF2ECDDFFA9ABACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9FFB5B5
      B2FFFAFAFAFF000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FFDEDEDEFF6A6B6EFFBA8344FFFFFFFFFFFFFCF6FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFCF6FFFFFFFFFFBA8344FF6A6B6EFFDEDEDEFF0000
      00FF000000FF000000FF000000FF000000FFDEDEDEFF929292FFF7F7F6FFD3D0
      CFFFD4D2D1FFDCDAD9FF787775FF7F7D7BFF807E7CFF7F7D7BFF7F7D7BFF7F7D
      7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D
      7BFF7F7D7BFF7F7D7BFF7F7D7BFF807E7CFF7F7D7BFF787775FFDCDAD9FFD4D2
      D1FFD3D0CFFFF7F7F6FF929292FFDEDEDEFFE9E9E9FFB47B08FFFFFFFFFFD4B3
      8AFFD5B58CFFD5B68EFFD7B890FFD8BA93FFD7B890FFD5B68EFFD5B68EFFD7B8
      90FFD8BA93FFD7B890FFD5B68EFFD5B68EFFD7B890FFD8BA93FFD7B890FFD5B6
      8EFFD5B58CFFD4B38AFFFFFFFFFFB47B08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47B08FFFFFFFFFFD4B3
      8AFFD5B58CFFD5B68EFFD7B890FFD8BA93FFD7B890FFD5B68EFFD5B68EFFD7B8
      90FFD9BB94FFD8B993FFB8AFA3FFA7A9AAFFA7A9A9FFA8AAAAFFA7A8A9FFA6A7
      A8FFA6A7A7FFA6A7A7FFA8AAABFFABAEB1FFADAFB0FFAFAFAEFFB5B5B4FFFAFA
      FAFF00000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF8E8F90FF7C7E81FFB88144FFFFFFFFFFFFF7ECFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF7ECFFFFFFFFFFB88144FF7C7E81FF8E8F90FF0000
      00FF000000FF000000FF000000FF000000FFEEEEEEFF949494FFFFFFFFFFFDFD
      FDFFFDFDFEFFFFFFFFFF696867FF6C6C6CFF717374FF6F7070FF6F6F70FF6F6F
      70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F
      70FF6F6F70FF6F6F70FF6F7070FF717374FF6C6C6CFF696867FFFFFFFFFFFDFD
      FEFFFDFDFDFFFFFFFFFF949494FFEEEEEEFFE9E9E9FFB47A08FFFFFFFFFFFFF6
      DEFFFFF6DFFFFFF7E1FFFFFAE4FFD5B78EFFFFFAE4FFFFF7E1FFFFF7E1FFFFFA
      E4FFD5B78EFFFFFAE4FFFFF7E1FFFFF7E1FFFFFAE4FFD5B78EFFFFFAE4FFFFF7
      E1FFFFF6DFFFFFF6DEFFFFFFFFFFB47A08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF6
      DEFFFFF6DFFFFFF7E1FFFFFAE4FFD5B78EFFFFFAE4FFFFF7E1FFFFF7E1FFFFFA
      E4FFD6B78EFFFFFBE4FFFFF8E0FFFFF8DFFFFFFBE1FFD9B78BFFFFFBE1FFFFF8
      DEFFFFF7DDFFFFF8DDFFFFFFFFFFB67600FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF57575AFFB78145FFFFFFFFFFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFFFFFFFB78145FF57575AFF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFB2B2B2FF949494FF9292
      92FF929292FF959697FF989C9FFFBE8545FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBE8545FF989C9FFF959697FF9292
      92FF929292FF949494FFB2B2B2FFFBFBFBFFE9E9E9FFB47A08FFFFFFFFFFFFF3
      D8FFFEF2D8FFFFF3DAFFFFF7DEFFD5B48AFFFFF7DEFFFFF3DAFFFFF3DAFFFFF7
      DEFFD5B48AFFFFF7DEFFFFF3DAFFFFF3DAFFFFF7DEFFD5B48AFFFFF7DEFFFFF3
      DAFFFEF2D8FFFFF3D8FFFFFFFFFFB47A08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF3
      D8FFFEF2D8FFFFF3DAFFFFF7DEFFD5B48AFFFFF7DEFFFFF3DAFFFFF3DAFFFFF7
      DEFFD5B48AFFFFF7DEFFFFF4DAFFFFF4DAFFFFF7DEFFD5B58AFFFFF7DEFFFFF4
      DAFFFFF3D8FFFFF3D8FFFFFFFFFFB57902FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF67686AFFB48147FFFFFFFFFFFFEEDAFFFFEEDAFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDAFFFFEEDAFFFFFFFFFFB48147FF67686AFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFE7E7E7FF68686CFFB98244FFFFFFFFFFFFF9F0FFFFF8EFFFFFF8
      EFFFFFF8EFFFFFF8EFFFFFF8EFFFFFF8EFFFFFF8EFFFFFF8EFFFFFF8EFFFFFF8
      EFFFFFF8EFFFFFF8EFFFFFF9F0FFFFFFFFFFB98244FF68686CFFE7E7E7FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FFB47B09FFFFFFFFFFFFF4
      D7FFFFF5D8FFFFF6DAFFFFF8DDFFD7B58AFFFFF8DDFFFFF6DAFFFFF6DAFFFFF8
      DDFFD7B58AFFFFF8DDFFFFF6DAFFFFF6DAFFFFF8DDFFD7B58AFFFFF8DDFFFFF6
      DAFFFFF5D8FFFFF4D7FFFFFFFFFFB47B09FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47B09FFFFFFFFFFFFF4
      D7FFFFF5D8FFFFF6DAFFFFF8DDFFD7B58AFFFFF8DDFFFFF6DAFFFFF6DAFFFFF8
      DDFFD7B58AFFFFF8DDFFFFF6DAFFFFF6DAFFFFF8DDFFD7B58AFFFFF8DDFFFFF6
      DAFFFFF5D8FFFFF4D7FFFFFFFFFFB47A07FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF979898FFB38048FFFFFFFFFFFFEBD2FFFFEBD3FFFFEB
      D4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEB
      D4FFFFEBD4FFFFEBD3FFFFEBD2FFFFFFFFFFB38048FF979898FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFF8F8F8FF7C7D80FFB88144FFFFFFFFFFFFF1E1FFFFF1E1FFFFF1
      E1FFFFF1E1FFFFF1E1FFFFF1E1FFFFF1E1FFFFF1E1FFFFF1E1FFFFF1E1FFFFF1
      E1FFFFF1E1FFFFF1E1FFFFF1E1FFFFFFFFFFB88144FF7C7D80FFF8F8F8FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FFB57B09FFFFFFFFFFDEB2
      7DFFE3B47FFFE3B681FFE1B784FFE0B888FFE1B784FFE3B681FFE3B681FFE1B7
      84FFE0B888FFE1B784FFE3B681FFE3B681FFE1B784FFE0B888FFE1B784FFE3B6
      81FFE3B47FFFDEB27DFFFFFFFFFFB57B09FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB57B09FFFFFFFFFFDEB2
      7DFFE3B47FFFE3B681FFE1B784FFE0B888FFE1B784FFE3B681FFE3B681FFE1B7
      84FFE0B888FFE1B784FFE3B681FFE3B681FFE1B784FFE0B888FFE1B784FFE3B6
      81FFE3B47FFFDEB27DFFFFFFFFFFB57B08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFA3A3A3FFB38048FFFFFFFFFFFFE6C9FFFFE7CBFFFFE7
      CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7
      CCFFFFE7CCFFFFE7CBFFFFE6C9FFFFFFFFFFB38048FFA3A3A3FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF959595FFB78145FFFFFFFFFFFFEBD4FFFFEBD5FFFFEB
      D6FFFFEBD6FFFFEBD6FFFFEBD6FFFFEBD6FFFFEBD6FFFFEBD6FFFFEBD6FFFFEB
      D6FFFFEBD6FFFFEBD5FFFFEBD4FFFFFFFFFFB78145FF959595FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FFB67B08FFFFFFFFFF4FC6
      FFFF58C9FFFF59CAFFFF53CBFFFFECBA80FF53CBFFFF59CAFFFF59CAFFFF53CB
      FFFFECBA80FF53CBFFFF59CAFFFF59CAFFFF53CBFFFFECBA80FF53CBFFFF59CA
      FFFF58C9FFFF4FC6FFFFFFFFFFFFB67B08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB67B08FFFFFFFFFF4FC6
      FFFF58C9FFFF59CAFFFF53CBFFFFECBA80FF53CBFFFF59CAFFFF59CAFFFF53CB
      FFFFECBA80FF53CBFFFF59CAFFFF59CAFFFF53CBFFFFECBA80FF53CBFFFF59CA
      FFFF58C9FFFF4FC6FFFFFFFFFFFFB67B08FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFA3A3A3FFB38048FFFFFFFFFFFFE1BFFFFFE2C2FFFFE2
      C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2
      C3FFFFE2C3FFFFE2C2FFFFE1BFFFFFFFFFFFB38048FFA3A3A3FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF959595FFB48147FFFFFFFFFFFFE5C6FFFFE5C8FFFFE6
      C8FFFFE6C8FFFFE6C8FFFFE6C8FFFFE6C8FFFFE6C8FFFFE6C8FFFFE6C8FFFFE6
      C8FFFFE6C8FFFFE5C8FFFFE5C6FFFFFFFFFFB48147FF959595FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FFB67B07FFFFFFFFFF55C5
      FFFF60C7FFFF61C9FFFF59C9FFFFF1B97AFF59C9FFFF61C9FFFF61C9FFFF59C9
      FFFFF1B97AFF59C9FFFF61C9FFFF61C9FFFF59C9FFFFF1B97AFF59C9FFFF61C9
      FFFF60C7FFFF55C5FFFFFFFFFFFFB67B07FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB67B07FFFFFFFFFF55C5
      FFFF60C7FFFF61C9FFFF59C9FFFFF1B97AFF59C9FFFF61C9FFFF61C9FFFF59C9
      FFFFF1B97AFF59C9FFFF61C9FFFF61C9FFFF59C9FFFFF1B97AFF59C9FFFF61C9
      FFFF60C7FFFF55C5FFFFFFFFFFFFB67B07FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFA6A6A6FFB38149FFFFFFFFFFFFDEB6FFFFDEB7FFFFDE
      B8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDE
      B8FFFFDEB8FFFFDEB7FFFFDEB6FFFFFFFFFFB38149FFA6A6A6FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF989898FFB38149FFFFFFFFFFFFDDB8FFFFDEB9FFFFDE
      BAFFFFDEBAFFFFDEBAFFFFDEBAFFFFDEBAFFFFDEBAFFFFDEBAFFFFDEBAFFFFDE
      BAFFFFDEBAFFFFDEB9FFFFDDB8FFFFFFFFFFB38149FF989898FF000000FF0000
      00FF000000FF000000FF000000FF000000FFEAEAEAFFB67C09FFFFFFFFFF4EC3
      FFFF56C5FFFF57C6FFFF51C6FFFFEAB578FF51C6FFFF57C6FFFF57C6FFFF51C6
      FFFFEAB578FF51C6FFFF57C6FFFF57C6FFFF51C6FFFFEAB578FF51C6FFFF57C6
      FFFF56C5FFFF4EC3FFFFFFFFFFFFB67C09FFEAEAEAFF00000000000000000000
      000000000000000000000000000000000000EAEAEAFFB67C09FFFFFFFFFF4EC3
      FFFF56C5FFFF57C6FFFF51C6FFFFEAB578FF51C6FFFF57C6FFFF57C6FFFF51C6
      FFFFEAB578FF51C6FFFF57C6FFFF57C6FFFF51C6FFFFEAB578FF51C6FFFF57C6
      FFFF56C5FFFF4EC3FFFFFFFFFFFFB67C09FFEAEAEAFF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFC4C4C4FFB5834DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5834DFFC4C4C4FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFBABABAFFB5834DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5834DFFBABABAFF000000FF0000
      00FF000000FF000000FF000000FF000000FFF2F2F2FFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFB67E0EFFF0F0F0FF00000000000000000000
      000000000000000000000000000000000000F2F2F2FFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFB67E0EFFF0F0F0FF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFEBEBEBFFB48551FFB5834CFFB38149FFB38049FFB380
      49FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB380
      49FFB38049FFB38049FFB38149FFB5834CFFB48551FFEBEBEBFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFE7E7E7FFB48551FFB5834CFFB38149FFB38049FFB380
      49FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB380
      49FFB38049FFB38049FFB38149FFB5834CFFB48551FFE7E7E7FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFCAA65BFFB67E0EFFB67C
      09FFB67B07FFB67B07FFB67B08FFB57B08FFB67B08FFB67B07FFB67B07FFB67B
      08FFB57B08FFB67B08FFB67B07FFB67B07FFB67B08FFB57B08FFB67B08FFB67B
      07FFB67B07FFB67C09FFB67E0EFFC19438FFFAFAFAFF00000000000000000000
      000000000000000000000000000000000000FBFBFBFFCAA65BFFB67E0EFFB67C
      09FFB67B07FFB67B07FFB67B08FFB57B08FFB67B08FFB67B07FFB67B07FFB67B
      08FFB57B08FFB67B08FFB67B07FFB67B07FFB67B08FFB57B08FFB67B08FFB67B
      07FFB67B07FFB67C09FFB67E0EFFC19438FFFAFAFAFF00000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFF7F7F7FFE6E6E6FFD7D7D7FFD5D5D5FFD5D5D5FFD5D5
      D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5
      D5FFD5D5D5FFD5D5D5FFD5D5D5FFD6D6D6FFDDDDDDFFE6E6E6FFF0F0F0FFF9F9
      F9FFFEFEFEFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFF3F3F3FFDADADAFFC5C5C5FFC3C3C3FFC3C3C3FFC3C3
      C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3C3FFC3C3
      C3FFC3C3C3FFC3C3C3FFC3C3C3FFC4C4C4FFD2D2D2FFE2E2E2FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFEFEFEFFFCFCFCFFFB5B5B5FFB2B2B2FFB2B2B2FFB2B2
      B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2
      B2FFB2B2B2FFB2B2B2FFB2B2B2FFB3B3B3FFC0C0C0FFCFCFCFFFE2E2E2FFF3F3
      F3FFFDFDFDFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFE6E6E6FFB0B0B0FF919191FF8B8B8BFF8B8B8BFF8B8B
      8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B
      8BFF8B8B8BFF8B8B8BFF8B8B8BFF8E8E8EFF9C9C9CFFB5B5B5FFCBCBCBFFDFDF
      DFFFF1F1F1FFFCFCFCFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFDADADAFF929292FF6D6D6DFF666666FF666666FF6666
      66FF666666FF666666FF666666FF666666FF666666FF666666FF666666FF6666
      66FF666666FF666666FF666666FF6B6B6BFF828282FFACACACFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFCFCFCFFF797979FF525252FF4B4B4BFF4B4B4BFF4B4B
      4BFF4B4B4BFF4B4B4BFF4B4B4BFF4B4B4BFF4B4B4BFF4B4B4BFF4B4B4BFF4B4B
      4BFF4B4B4BFF4B4B4BFF4B4B4BFF4F4F4FFF606060FF818181FFA2A2A2FFC3C3
      C3FFE4E4E4FFF9F9F9FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD7D7D7FFA98154FFB5834DFFB38149FFB38149FFB381
      49FFB38149FFB38149FFB38149FFB38149FFB38149FFB48149FFB9834CFFC786
      52FF9B8954FF3D8A4DFF008A49FF008949FF008949FF008A49FF2E9364FF96B2
      A5FFD2D2D2FFEDEDEDFFFCFCFCFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFC5C5C5FFA57D4EFFB68754FFB58651FFB58750FFB689
      4EFFB78A4CFFB88B4AFFB98B49FFBA8948FFBC8748FFBD8448FFBF8348FFBF83
      49FFBF844BFFBF864DFFBE874FFFBC8952FF81804CFF007F43FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB5B5B5FFA47B4BFFB5834DFFB38149FFB38149FFB381
      49FFB38149FFB38149FFB38149FFB38149FFB38149FFB48149FFB88549FFC28D
      48FFA17C67FF5A569DFF2739C2FF2A3ABEFF2B3BBEFF2B3BBFFF28489EFF6175
      87FFADADADFFDCDCDCFFF9F9F9FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFB2814CFFAA7A45FFA47643FFA476
      43FFA47643FFA47643FFA47643FFA47643FFA47643FFA47643FFA87A43FFB181
      42FF93715EFF534F90FF2536B8FF2939BBFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD5D5D5FFB5834CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF71C4
      A1FF008F4EFF00B678FF00CD90FF00D194FF00D194FF00CD90FF00B679FF0091
      52FF4B9C77FFD2D2D2FFF1F1F1FFFEFEFEFF000000FF000000FF000000FF0000
      00FF000000FF000000FFC3C3C3FFB58754FFFFEEB3FFFAE1C5FFF4D7D5FFEECC
      E4FFE8C3F1FFE2B8FFFFDCB2FFFFD1C6FFFFC6DAFFFFBAEEFFFFAFFFFFFFACFF
      FFFFAFEFF8FFB1E0E9FFB0D0D8FFABBCC8FFBC8953FF00BB84FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB2B2B2FFB5834CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B96
      E6FF2339CDFF4C60EEFF6377FFFF697CFFFF697CFFFF6578FFFF5164EBFF3344
      C7FF305193FFADADADFFE4E4E4FFFDFDFDFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFF666F
      AAFF1A2A97FF3948B3FF5263D4FF6476F2FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD5D5D5FFB48148FFFFFFFFFFFFF7EBFFFFF6EAFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF7EDFFFFFDF5FF6BB88EFF008F
      4AFF00CA8CFF00CC8FFF00C98AFF00C685FF00C685FF00C98AFF00CD8FFF00CB
      8FFF009656FF4E9F7AFFDFDFDFFFF9F9F9FF000000FF000000FF000000FF0000
      00FF000000FF000000FFC3C3C3FFB48654FFFEE7A4FFD77F1FFFC6634FFFB545
      7AFFA427A2FF9307CCFF8000EFFF6031F3FF3E6BF7FF1DA6FDFF00E4FFFF00DC
      E8FF00AEB9FF00818BFF004E58FFA3B0B9FFBF8A52FF00B87EFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB2B2B2FFB48148FFFFFFFFFFFFF7EBFFFFF6EAFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF9EBFFFFFFECFF858AD3FF2538
      C9FF5C6FFFFF6074FFFF6073FDFF5F72FCFF5F72FCFF6073FDFF6174FEFF6072
      FEFF3546CAFF335596FFC3C3C3FFF3F3F3FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFEAEAEAFF94928FFF7D7D7DFF7B7B
      7BFF7B7B7BFF7B7B7BFF7B7B7BFF7B7B7BFF7B7B7BFF7B7B7BFF7B7B7BFF7B7B
      7BFF7B7B7BFF7D7D7DFF6F7494FF5769E7FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF7F7F7FFE6E6E6FFD7D7D7FFD5D5
      D5FFD5D5D5FFD5D5D5FFB7B7B7FFB58146FFFFFFFFFFFFEEDAFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEFDCFFFFF1E0FFCCD9BCFF008842FF00C6
      89FF00C98BFF00C788FF00C380FFFFFFFFFFFFFFFFFF00C380FF00C788FF00CA
      8CFF00C98DFF009251FF9AB6A9FFECECECFF000000FF000000FF000000FF0000
      00FF000000FF000000FFA7A7A7FFB48656FFFCDD8BFFD98427FFC86A57FFB84E
      7FFFA832A5FF9813CDFF8603EEFF683BF2FF4871F5FF28A9FBFF07E5FFFF00DE
      E7FF07B1BBFF0C868EFF08545DFF8E9DA4FFC18C53FF00B275FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF3F3F3FFDADADAFFC5C5C5FFC3C3
      C3FFC3C3C3FFC3C3C3FF8D8D8DFFB58146FFFFFFFFFFFFEEDAFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEFDBFFFFF5DBFFD4C9D5FF1D2FC4FF556A
      FBFF5A6FFBFF596EFAFF596DF9FF596DF9FF596DF9FF596DF9FF596EFAFF5B6F
      FCFF5B6FFCFF3242C7FF6B8092FFDEDEDEFFEFEFEFFFCFCFCFFFB5B5B5FFB2B2
      B2FFB2B2B2FFB2B2B2FF818181FFA57640FFD8D8D8FF7D7D7DFFE5E2E1FFE0DE
      DDFFDFDCDBFFDFDCDBFFDFDCDBFFDFDCDBFFDFDCDBFFDFDCDBFFDFDCDBFFDFDC
      DBFFE0DEDDFFE5E2E1FF7D7D7DFF4B5CD3FF5164E4FF5164E4FF5165E4FF5365
      E6FF5365E6FF2E3DB7FF66798BFFDBDBDBFFE6E6E6FFB0B0B0FF919191FF8B8B
      8BFF8B8B8BFF8B8B8BFF838383FFB78145FFFFFFFFFFFFE6CAFFFFE6CBFFFFE6
      CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE7CDFFFFECD5FF43A36CFF00AF6DFF00C8
      89FF00C586FF00C383FF00BE79FFFFFFFFFFFFFFFFFF00BE79FF00C383FF00C5
      86FF00C88AFF00B374FF2A9061FFDEDEDEFF000000FF000000FF000000FF0000
      00FF000000FF000000FF787878FFB58757FFFBD472FFD9862AFFC96B58FFB950
      80FFA934A6FF9915CDFF8705EEFF693CF2FF4A73F5FF2BAAFBFF09E6FFFF02DE
      E8FF0AB2BBFF0E8890FF0C575FFF778890FFC38D54FF00AE6FFF000000FF0000
      00FF000000FF000000FF000000FF000000FFDADADAFF929292FF6D6D6DFF6666
      66FF666666FF666666FF565656FFB78145FFFFFFFFFFFFE6CAFFFFE6CBFFFFE6
      CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE8CCFFFFF2CCFF6569C2FF3D52E6FF546A
      FAFF5267F8FF5167F7FF5166F7FF5166F7FF5166F7FF5166F7FF5167F7FF5268
      F8FF566BFAFF475BE6FF2949A1FFCCCCCCFFCFCFCFFF797979FF525252FF4B4B
      4BFF4B4B4BFF4B4B4BFF3F3F3FFF875F33FFB7B7B7FF7B7C7CFFD4D1CFFFCECC
      C9FFCDCBC9FFCDCBC9FFCDCBC9FFCDCBC9FFCDCBC9FFCDCBC9FFCDCBC9FFCDCB
      C9FFCECCC9FFD4D1CFFF7B7C7CFF3A49B1FF3C4BB6FF3C4BB6FF3C4CB6FF3C4D
      B7FF3F4FB8FF3545ADFF223D86FFC2C2C2FFD7D7D7FF7E7E7EFF808080FF7E7E
      7EFF7E7E7EFF7F8081FF7D8186FFB98144FFFFFFFFFFFFDFB7FFFFDFB8FFFFDF
      B9FFFFDFB9FFFFDFB9FFFFDFB9FFFFE1BAFFFFE7C4FF007F3AFF00C384FF00C3
      82FF00BE7CFF00BC77FF00B86FFFFFFFFFFFFFFFFFFF00B86FFF00BC77FF00BE
      7CFF00C383FF00C587FF008A4AFFD6D6D6FFD4D4D4FF787878FF757575FF7373
      73FF737373FF747576FF6A6D72FFB68757FFF8CB58FFD9862CFFC96C59FFB950
      80FFA934A6FF9915CDFF8705EEFF693CF2FF4A73F5FF2BAAFBFF09E6FFFF02DE
      E8FF0AB2BBFF0F8890FF0E5961FF5E727BFFC68E54FF009C5EFF00AC6DFF00AE
      71FF00B278FF00B57CFF008346FFD3D3D3FFC3C3C3FF777777FF808080FF7E7E
      7EFF7E7E7EFF7F8081FF7D8186FFB98144FFFFFFFFFFFFDFB7FFFFDFB8FFFFDF
      B9FFFFDFB9FFFFDFB9FFFFDFB9FFFFE1B9FFFFEEB9FF1428BEFF4A60F6FF4A61
      F5FF4159F3FF3D55F2FF3C54F2FF3C54F2FF3C54F2FF3C54F2FF3D55F2FF4159
      F3FF4A61F5FF4F64F6FF2D3CBFFFC1C1C1FFB3B3B3FF777777FF818181FF8080
      80FF808080FF808080FF818282FF848485FF858686FF7D7D7DFFC9C6C4FFC4C1
      BFFFC3C0BEFFC3C0BEFFC3C0BEFFC3C0BEFFC3C0BEFFC3C0BEFFC3C0BEFFC3C0
      BEFFC4C1BFFFC9C6C4FF7D7D7DFF858686FF848485FF818282FF808080FF8080
      80FF808080FF818181FF656987FFB1B1B1FFD5D5D5FF808080FFEAE7E6FFE2DF
      DEFFE2E0DFFFE6E4E4FF757578FFBD8444FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00823BFF17CC94FF00BD
      7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF00BD7AFF1BCD96FF008949FFD5D5D5FFCACACAFF6A6A6AFFB0AEADFFA7A4
      A4FFA7A5A4FFAAA8A8FF545456FFB78756FFF6C13FFFDA872EFFC96C59FFB950
      80FFA934A6FF9915CDFF8705EEFF693CF2FF4A73F5FF2BAAFBFF09E6FFFF02DE
      E8FF0AB2BBFF0F8890FF0F5A62FF475D66FFC98F53FFB7B7B7FFBCBCBCFFBCBC
      BCFF008B5AFF149A71FF00723DFFCACACAFFB9B9B9FF808080FFEAE7E6FFE2DF
      DEFFE2E0DFFFE6E4E4FF757578FFBD8444FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF192BBFFF5C71F8FF3B53
      F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF3B53F2FF5F74F8FF2C3BBFFFB9B9B9FFA9A9A9FF818181FFD2CFCEFFCAC8
      C7FFCAC7C6FFCAC8C7FFCECCCBFF7C7A79FF807E7DFF7E7C7BFFC1BEBCFFBDBA
      B8FFBCB9B7FFBCB9B7FFBCB9B7FFBCB9B7FFBCB9B7FFBCB9B7FFBCB9B7FFBCB9
      B7FFBDBAB8FFC1BEBCFF7E7C7BFF807E7DFF7C7A79FFCECCCBFFCAC8C7FFCAC7
      C6FFCAC8C7FFD2CFCEFF818181FFA9A9A9FFD5D5D5FF7E7E7EFFE3E1E0FFD6D4
      D2FFD6D4D2FFDAD9D7FF959494FF6B6B6DFF6B6A6BFF6A6868FF7C7A78FF7C7A
      78FF7B7A78FF7B7A78FF7B7A78FF7E7A79FF8C7B7FFF00873FFF36D3A0FF00BB
      76FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF00BB77FF39D3A2FF008947FFD5D5D5FFC3C3C3FF868686FF959595FF9494
      94FF949494FF959797FF959AA1FFB98857FFF5B822FFDB882EFFCA6C59FFBA4F
      80FFAA32A7FF9A13CFFF8704F0FF693CF4FF4973F7FF29ABFDFF07E7FFFF00E0
      E9FF08B3BDFF0E8991FF105B64FF2E4750FFCC9152FF989CA0FF959797FF9494
      94FF949494FF959595FF698979FFC3C3C3FFB2B2B2FF7E7E7EFFE3E1E0FFD6D4
      D2FFD6D4D2FFDAD9D7FF959494FF6B6B6DFF6B6A6BFF6A6868FF7C7A78FF7C7A
      78FF7B7A78FF7B7A78FF7B7A78FF7D7B77FF878373FF1E2FC3FF6E82F9FF334E
      F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF334EF0FF7084F8FF2A3ABFFFB2B2B2FFA3A3A3FF7F8080FFD2CFCEFFC2C0
      BFFFC2C0BFFFC3C1C0FFC6C4C3FF908E8DFF737170FF747271FF858381FF8381
      7FFF83817FFF83817FFF83817FFF83817FFF83817FFF83817FFF83817FFF8381
      7FFF83817FFF858381FF747271FF737170FF908E8DFFC6C4C3FFC3C1C0FFC2C0
      BFFFC2C0BFFFD2CFCEFF7F8080FFA3A3A3FFD5D5D5FF7D7E7EFFE3E0DFFFCFCD
      CBFFCFCDCBFFD1CFCDFFD7D5D3FF8E8C8AFF61605FFF636160FF626160FF6260
      5FFF62605FFF62605FFF62605FFF646060FF6E5F64FF00873EFF6BDBB7FF00BB
      78FF00B974FF00B771FF00B268FFFFFFFFFFFFFFFFFF00B268FF00B771FF00B9
      74FF00BB78FF6DDCB8FF008946FFD5D5D5FFC3C3C3FF959595FFE5E4E2FFDEDD
      DCFFDFDEDDFFE3E3E2FF757579FFBF8A54FFFBB002FFE48A2BFFD16B57FFC04C
      81FFAE2DABFF9D0CD6FF8800FAFF6737FFFF4573FFFF23AFFFFF00EEFFFF00E7
      F3FF00B8C3FF058B94FF095B64FF102F37FFCD9151FF777678FFE3E3E2FFDFDE
      DDFFDEDDDCFFE5E4E2FF959595FFC3C3C3FFB2B2B2FF7D7E7EFFE3E0DFFFCFCD
      CBFFCFCDCBFFD1CFCDFFD7D5D3FF8E8C8AFF61605FFF636160FF626160FF6260
      5FFF62605FFF62605FFF62605FFF63615EFF6A6659FF1C2EC5FF909FF7FF334D
      EDFF2F4AECFF2C47EBFF2B47EBFF2B47EBFF2B47EBFF2B47EBFF2C47EBFF2F4A
      ECFF334EEDFF93A1F7FF2737C2FFB2B2B2FFA3A3A3FF7F7F7FFFD5D2D3FFBFBD
      BCFFBFBDBCFFBFBDBCFFC1BFBEFFC6C4C3FF898786FF656362FF656362FF6563
      62FF656362FF656362FF656362FF656362FF656362FF656362FF656362FF6563
      62FF656362FF656362FF656362FF898786FFC6C4C3FFC1BFBEFFBFBDBCFFBFBD
      BCFFBFBDBCFFD5D2D3FF7F7F7FFFA3A3A3FFD5D5D5FF7E7E7EFFE3E2E1FFCBC8
      C7FFCBC8C7FFCCC8C8FFCECBCAFFD2CECDFFD5D1D1FFD6D3D2FFD6D3D2FFD6D3
      D2FFD6D3D2FFD6D3D2FFD6D3D2FFD8D3D3FFE1D3D7FF329D6BFF5AC59BFF28C8
      94FF00BA78FF00B978FF00B46EFFFFFFFFFFFFFFFFFF00B46EFF00B978FF00BA
      78FF29C894FF5DC79EFF26875BFFD5D5D5FFC3C3C3FF949494FFE0DFDEFFD5D4
      D2FFD5D4D2FFD9D8D6FF959495FF6D6E73FF6F727BFF6F757BFF717878FF737A
      74FF757D70FF76806DFF787F6AFF7C7C69FF7F7768FF827268FF846E68FF856E
      6BFF85716EFF857573FF837876FF7D7778FF737171FF969495FFD9D8D6FFD5D4
      D2FFD5D4D2FFE0DFDEFF949494FFC3C3C3FFB2B2B2FF7E7E7EFFE3E2E1FFCBC8
      C7FFCBC8C7FFCCC8C8FFCECBCAFFD2CECDFFD5D1D1FFD6D3D2FFD6D3D2FFD6D3
      D2FFD6D3D2FFD6D3D2FFD6D3D2FFD7D4D1FFDED9CFFF5661C6FF7D8AE6FF5970
      F0FF324DEBFF3550EBFF3651EBFF3751EBFF3751EBFF3651EBFF3550EBFF324D
      EBFF5970F1FF808CE8FF4751ADFFB2B2B2FFA3A3A3FF7E7F7FFFDAD7D6FFBCBA
      B8FFBDBBB9FFBDBBB9FFBDBBB9FFC0BEBBFFC3C1BFFFC5C3C1FFC6C4C2FFC6C4
      C2FFC6C4C2FFC6C4C2FFC6C4C2FFC6C4C2FFC6C4C2FFC6C4C2FFC6C4C2FFC6C4
      C2FFC6C4C2FFC6C4C2FFC5C3C1FFC3C1BFFFC0BEBBFFBDBBB9FFBDBBB9FFBDBB
      B9FFBCBAB8FFDAD7D6FF7E7F7FFFA3A3A3FFD5D5D5FF7E7E7EFFE5E3E3FFC5C2
      C1FFC6C3C2FFC6C3C2FFC8C5C4FFCBC8C7FFCDCAC9FFCECBCAFFCECBCAFFCECB
      CAFFCECBCAFFCECBCAFFCECBCAFFCFCBCBFFD6CCCEFFA1BCAEFF00934FFF8AE2
      C4FF1DC48BFF00B774FF00B36DFFFFFFFFFFFFFFFFFF00B36DFF00B774FF1CC4
      8AFF89E2C3FF00934FFF6B8378FFD5D5D5FFC3C3C3FF949494FFE0DFDDFFCECD
      CBFFCECDCBFFD0CFCDFFD6D6D4FF8E8D8BFF636162FF656363FF666463FF6664
      62FF666562FF666561FF676561FF676561FF686460FF686360FF686260FF6962
      61FF696362FF696362FF686462FF666261FF8F8D8BFFD6D6D4FFD0CFCDFFCECD
      CBFFCECDCBFFE0DFDDFF949494FFC3C3C3FFB2B2B2FF7E7E7EFFE5E3E3FFC5C2
      C1FFC6C3C2FFC6C3C2FFC8C5C4FFCBC8C7FFCDCAC9FFCECBCAFFCECBCAFFCECB
      CAFFCECBCAFFCECBCAFFCECBCAFFCFCCCAFFD4D0CAFFABACC7FF2E3ECBFFA3B0
      F7FF4D63EEFF2D48E9FF314BE9FF334DE9FF334DE9FF314BE9FF2D48E9FF4D63
      EDFFA2AFF6FF2E3FCAFF74778EFFB2B2B2FFA3A3A3FF7E7E7EFFDFDDDCFFB9B7
      B5FFBBB9B7FFBBB9B7FFBDBBB9FFBFBDBBFFC1BFBDFFC2C0BEFFC2C0BEFFC2C0
      BEFFC2C0BEFFC2C0BEFFC2C0BEFFC2C0BEFFC2C0BEFFC2C0BEFFC2C0BEFFC2C0
      BEFFC2C0BEFFC2C0BEFFC2C0BEFFC1BFBDFFBFBDBBFFBDBBB9FFBBB9B7FFBBB9
      B7FFB9B7B5FFDFDDDCFF7E7E7EFFA3A3A3FFD5D5D5FF7D7E7EFFE8E6E6FFBFBD
      BBFFC1BFBDFFC3C1BFFFC7C5C3FF979593FF7B7977FF7D7B79FF7D7B79FF7D7B
      79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF807C7AFF897C7EFF258558FF0A9B
      5BFF95E4C8FF5CD5ACFF0BBC7FFF00B068FF00B068FF0BBC7FFF5CD4ABFF92E2
      C6FF049655FF5DB38CFF8C8186FFD5D5D5FFC3C3C3FF949494FFE0E0DFFFCAC7
      C7FFCAC8C7FFCBC8C8FFCDCBCAFFD0CECDFFD3D1D1FFD5D3D2FFD5D3D2FFD5D3
      D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3D2FFD5D3
      D2FFD5D3D2FFD5D3D2FFD5D3D2FFD3D1D1FFD0CECDFFCDCBCAFFCBC8C8FFCAC8
      C7FFCAC7C7FFE0E0DFFF949494FFC3C3C3FFB2B2B2FF7D7E7EFFE8E6E6FFBFBD
      BBFFC1BFBDFFC3C1BFFFC7C5C3FF979593FF7B7977FF7D7B79FF7D7B79FF7D7B
      79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7F7D79FF858276FF4650AAFF394B
      D0FFA8B4F8FF7C8EF1FF3E57E9FF2542E5FF2542E5FF3E57E9FF7C8EF1FFA6B2
      F6FF3345CAFF7B84D4FF88877DFFB2B2B2FFA3A3A3FF7E7E7EFFE4E2E2FFB8B5
      B3FFBAB7B5FFBCB9B7FFC0BCBAFF959391FF7D7B79FF7E7C7AFF7E7C7AFF7E7C
      7AFF7E7C7AFF7E7C7AFF7E7C7AFF7E7C7AFF7E7C7AFF7E7C7AFF7E7C7AFF7E7C
      7AFF7E7C7AFF7E7C7AFF7E7C7AFF7D7B79FF959391FFC0BCBAFFBCB9B7FFBAB7
      B5FFB8B5B3FFE4E2E2FF7E7E7EFFA3A3A3FFD5D5D5FF7D7D7DFFECEAEAFFBAB7
      B5FFBCB9B7FFC1BDBBFF8F8D8DFF767473FF797776FF797777FF797877FF7978
      77FF797877FF797877FF797877FF7A7877FF7A7877FF7D7778FF85767BFF2C82
      5AFF009752FF59C59AFF92E2C6FF9BE8CEFF9BE7CEFF91E1C4FF55C296FF008F
      4BFF459A71FFFCEDF3FF827F80FFD5D5D5FFC3C3C3FF949494FFE3E1E1FFC4C1
      C1FFC5C3C2FFC5C3C2FFC7C5C4FFCAC8C7FFCCCAC9FFCDCBCAFFCDCBCAFFCDCB
      CAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCBCAFFCDCB
      CAFFCDCBCAFFCDCBCAFFCDCBCAFFCCCAC9FFCAC8C7FFC7C5C4FFC5C3C2FFC5C3
      C2FFC4C1C1FFE3E1E1FF949494FFC3C3C3FFB2B2B2FF7D7D7DFFECEAEAFFBAB7
      B5FFBCB9B7FFC1BDBBFF8F8D8DFF767473FF797776FF797777FF797877FF7978
      77FF797877FF797877FF797877FF7A7877FF7A7876FF7C7A75FF817D70FF4A52
      A2FF3042CFFF7886E7FFA5B1F7FFAFBAFAFFAFBAF9FFA4B0F5FF7482E4FF283A
      C7FF626BB9FFF8F4E9FF81817DFFB2B2B2FFA3A3A3FF7E7E7EFFE9E8E7FFB5B2
      B0FFB7B4B2FFBBB8B6FF8D8B8BFF757372FF787675FF797776FF797776FF7977
      76FF797776FF797776FF797776FF797776FF797776FF797776FF797776FF7977
      76FF797776FF797776FF797776FF787675FF757372FF8D8B8BFFBBB8B6FFB7B4
      B2FFB5B2B0FFE9E8E7FF7E7E7EFFA3A3A3FFD5D5D5FF7D7D7DFFEFEEEDFFB2AE
      ACFFB4B0AEFFBAB7B5FF676564FF6E6C6BFF706E6DFF706E6DFF706E6DFF706E
      6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF716E6DFF736E6EFF786C
      6FFF567263FF137F4FFF008A40FF00893FFF00893FFF00863CFF269160FF89A6
      97FFBFB1B3FFF4EFF0FF7E7D7EFFD5D5D5FFC3C3C3FF949494FFE6E3E4FFBFBD
      BBFFC1BFBDFFC3C1BFFFC7C5C3FF979593FF7D7977FF7E7B79FF7E7B79FF7E7B
      79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B79FF7E7B
      79FF7E7B79FF7E7B79FF7E7B79FF7D7977FF979593FFC7C5C3FFC3C1BFFFC1BF
      BDFFBFBDBBFFE6E3E4FF949494FFC3C3C3FFB2B2B2FF7D7D7DFFEFEEEDFFB2AE
      ACFFB4B0AEFFBAB7B5FF676564FF6E6C6BFF706E6DFF706E6DFF706E6DFF706E
      6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF726F6BFF7571
      67FF60617CFF3844A8FF1D30C8FF1E30C7FF1D2FC6FF1A2CC4FF4B56BAFF9495
      B0FFBBB7ABFFF3F1EDFF7E7E7DFFB2B2B2FFA3A3A3FF7D7D7DFFEEEDECFFB0AC
      AAFFB2AEACFFB7B4B2FF656362FF6B6968FF6C6A69FF6D6A69FF6D6A69FF6D6A
      69FF6D6A69FF6D6A69FF6D6A69FF6D6A69FF6D6A69FF6D6A69FF6D6A69FF6D6A
      69FF6D6A69FF6D6A69FF6D6A69FF6C6A69FF6B6968FF656362FFB7B4B2FFB2AE
      ACFFB0ACAAFFEEEDECFF7D7D7DFFA3A3A3FFD5D5D5FF7D7D7DFFF2F1F0FFECEC
      EBFFEDEDECFFF5F4F3FF797776FF807E7DFF817F7EFF817F7EFF817F7EFF817F
      7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7FFF827F7FFF837F
      7FFF867E7FFF887D80FF8B7B80FF8B7B80FF8A7A7FFF847579FFFFF4F8FFF5EE
      F0FFEFECECFFF3F1F0FF7D7D7DFFD5D5D5FFC3C3C3FF939494FFEAE7E8FFB9B7
      B5FFBBB9B7FFBFBDBBFF918D8DFF777473FF7A7776FF7A7777FF7B7877FF7B78
      77FF7B7877FF7B7877FF7B7877FF7B7877FF7B7877FF7B7877FF7B7877FF7B78
      77FF7B7877FF7B7877FF7A7777FF7A7776FF777473FF918D8DFFBFBDBBFFBBB9
      B7FFB9B7B5FFEAE7E8FF939494FFC3C3C3FFB2B2B2FF7D7D7DFFF2F1F0FFECEC
      EBFFEDEDECFFF5F4F3FF797776FF807E7DFF817F7EFF817F7EFF817F7EFF817F
      7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF82807EFF8280
      7DFF84817BFF858278FF868376FF878375FF868274FF807C6FFFFCFAF0FFF3F1
      EBFFEEEDEBFFF3F2F0FF7D7D7DFFB2B2B2FFA3A3A3FF7D7D7DFFF2F1F0FFEDEC
      EBFFEDEDECFFF3F2F1FFABACACFFB0B1B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2
      B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2
      B2FFB2B2B2FFB2B2B2FFB2B2B2FFB2B2B2FFB0B1B2FFABACACFFF3F2F1FFEDED
      ECFFEDECEBFFF2F1F0FF7D7D7DFFA3A3A3FFD5D5D5FF7D7D7DFFD7D5D4FFC8C5
      C4FFC8C6C5FFCECDCCFF969391FF9C9997FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9E9A98FF9E9A98FF9F9998FF9F9998FF9E9997FF989391FFD0CDCCFFC9C6
      C5FFC8C5C5FFD7D5D4FF7D7D7DFFD5D5D5FFC3C3C3FF939393FFEDECEBFFB1AE
      ACFFB4B0AEFFBAB7B5FF686564FF6F6C6BFF716E6DFF716E6DFF716E6DFF716E
      6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E6DFF716E
      6DFF716E6DFF716E6DFF716E6DFF716E6DFF6F6C6BFF686564FFBAB7B5FFB4B0
      AEFFB1AEACFFEDECEBFF939393FFC3C3C3FFB2B2B2FF7D7D7DFFD7D5D4FFC8C5
      C4FFC8C6C5FFCECDCCFF969391FF9C9997FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9E9A97FF9E9B97FF9E9B97FF9E9B96FF9D9A96FF97948FFFCFCDCBFFC9C7
      C5FFC8C6C4FFD7D5D4FF7D7D7DFFB2B2B2FFA3A3A3FF7D7D7DFFD7D5D4FFC8C5
      C4FFC8C6C5FFCDCBCAFF93908EFF999593FF999694FF999694FF999694FF9996
      94FF999694FF999694FF999694FF999694FF999694FF999694FF999694FF9996
      94FF999694FF999694FF999694FF999694FF999593FF93908EFFCDCBCAFFC8C6
      C5FFC8C5C4FFD7D5D4FF7D7D7DFFA3A3A3FFD5D5D5FF7D7D7DFFEAE9E8FFCECC
      CBFFCFCDCCFFD5D3D2FF868382FF8C8988FF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8C8988FF868382FFD5D3D2FFCFCD
      CCFFCECCCBFFEAE9E8FF7D7D7DFFD5D5D5FFC3C3C3FF939393FFF0EFEEFFECEB
      EBFFEDEDECFFF5F4F3FF7A7775FF817E7CFF827F7DFF827F7DFF827F7DFF827F
      7DFF827F7DFF827F7DFF827F7DFF827F7DFF827F7DFF827F7DFF827F7DFF827F
      7DFF827F7DFF827F7DFF827F7DFF827F7DFF817E7CFF7A7775FFF5F4F3FFEDED
      ECFFECEBEBFFF0EFEEFF939393FFC3C3C3FFB2B2B2FF7D7D7DFFEAE9E8FFCECC
      CBFFCFCDCCFFD5D3D2FF868382FF8C8988FF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8C8988FF868382FFD5D3D2FFCFCD
      CCFFCECCCBFFEAE9E8FF7D7D7DFFB2B2B2FFA3A3A3FF7D7D7DFFEAE9E8FFCECC
      CBFFCFCDCCFFD5D3D2FF868281FF8C8888FF8D8A89FF8D8A89FF8D8A89FF8D8A
      89FF8D8A89FF8D8A89FF8D8A89FF8D8A89FF8D8A89FF8D8A89FF8D8A89FF8D8A
      89FF8D8A89FF8D8A89FF8D8A89FF8D8A89FF8C8888FF868281FFD5D3D2FFCFCD
      CCFFCECCCBFFEAE9E8FF7D7D7DFFA3A3A3FFD7D7D7FF7C7D7DFFF9F9F8FFD3D1
      D0FFD4D2D1FFDCDAD9FF797675FF807D7CFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF807D7CFF797675FFDCDAD9FFD4D2
      D1FFD3D1D0FFF9F9F8FF7C7D7DFFD7D7D7FFC4C4C4FF949494FFD5D3D2FFC7C5
      C3FFC8C6C4FFCECCCAFF969391FF9C9997FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9C9997FF969391FFCECCCAFFC8C6
      C4FFC7C5C3FFD5D3D2FF949494FFC4C4C4FFB4B4B4FF7C7D7DFFF9F9F8FFD3D1
      D0FFD4D2D1FFDCDAD9FF797675FF807D7CFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF807D7CFF797675FFDCDAD9FFD4D2
      D1FFD3D1D0FFF9F9F8FF7C7D7DFFB4B4B4FFA5A5A5FF7C7C7DFFF9F9F8FFD3D1
      CFFFD4D2D1FFDCDAD9FF797574FF807D7CFF827F7EFF827F7EFF827F7EFF827F
      7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F7EFF827F
      7EFF827F7EFF827F7EFF827F7EFF827F7EFF807D7CFF797574FFDCDAD9FFD4D2
      D1FFD3D1CFFFF9F9F8FF7C7C7DFFA5A5A5FFE6E6E6FF7E7E7EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF6A6868FF6E6C6EFF545657FF535454FF535354FF5353
      54FF535354FF535354FF535354FF535354FF535354FF535354FF535354FF5353
      54FF535354FF535354FF535454FF545657FF6E6C6EFF6A6868FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF7E7E7EFFE6E6E6FFD2D2D2FF939393FFE8E7E5FFCECB
      CAFFCFCDCCFFD5D3D3FF868482FF8C8A88FF8D8B89FF8D8B89FF8D8B89FF8D8B
      89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8D8B
      89FF8D8B89FF8D8B89FF8D8B89FF8D8B89FF8C8A88FF868482FFD5D3D3FFCFCD
      CCFFCECBCAFFE8E7E5FF939393FFD2D2D2FFC7C7C7FF7E7E7EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF6A6868FF6E6C6EFF545657FF535454FF535354FF5353
      54FF535354FF535354FF535354FF535354FF535354FF535354FF535354FF5353
      54FF535354FF535354FF535454FF545657FF6E6C6EFF6A6868FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF7E7E7EFFC7C7C7FFBDBDBDFF7E7E7EFFFFFFFFFFFEFD
      FDFFFEFDFDFFFFFFFFFF6A6665FF716D6CFF5A5B5BFF595A5BFF595A5BFF595A
      5BFF595A5BFF595A5BFF595A5BFF595A5BFF595A5BFF595A5BFF595A5BFF595A
      5BFF595A5BFF595A5BFF595A5BFF5A5B5BFF716D6CFF6A6665FFFFFFFFFFFEFD
      FDFFFEFDFDFFFFFFFFFF7E7E7EFFBDBDBDFFF7F7F7FF8B8B8BFF7E7E7EFF7C7C
      7CFF7C7C7CFF7E7F80FF808387FFC08646FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC08646FF808387FF7E7F80FF7C7C
      7CFF7C7C7CFF7E7E7EFF8B8B8BFFF7F7F7FFE3E3E3FF929292FFF7F7F6FFD3D0
      CFFFD4D2D1FFDCDAD9FF787775FF7F7D7BFF807E7CFF7F7D7BFF7F7D7BFF7F7D
      7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D7BFF7F7D
      7BFF7F7D7BFF7F7D7BFF7F7D7BFF807E7CFF7F7D7BFF787775FFDCDAD9FFD4D2
      D1FFD3D0CFFFF7F7F6FF929292FFE3E3E3FFDFDFDFFF858585FF7E7E7EFF7C7C
      7CFF7C7C7CFF7E7F80FF808387FFC08646FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC08646FF808387FF7E7F80FF7C7C
      7CFF7C7C7CFF7E7E7EFF858585FFDFDFDFFFDCDCDCFF7F7F7FFF979797FF9393
      93FF939393FF969696FF9C9C9DFF9E9E9EFFC3C1BFFFC3C0BDFFC2BFBDFFC2BF
      BDFFC2BFBDFFC2BFBDFFC2BFBDFFC2BFBDFFC2BFBDFFC2BFBDFFC2BFBDFFC2BF
      BDFFC2BFBDFFC2BFBDFFC3C0BDFFC3C1BFFF9E9E9EFF9C9C9DFF969696FF9393
      93FF939393FF979797FF7F7F7FFFDCDCDCFF000000FF000000FF000000FF0000
      00FF000000FFDEDEDEFF6A6B6EFFBA8344FFFFFFFFFFFFFCF6FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFCF6FFFFFFFFFFBA8344FF6A6B6EFFDEDEDEFF0000
      00FF000000FF000000FF000000FF000000FFF2F2F2FF949494FFFFFFFFFFFDFD
      FDFFFDFDFEFFFFFFFFFF696867FF6C6C6CFF717374FF6F7070FF6F6F70FF6F6F
      70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F70FF6F6F
      70FF6F6F70FF6F6F70FF6F7070FF717374FF6C6C6CFF696867FFFFFFFFFFFDFD
      FEFFFDFDFDFFFFFFFFFF949494FFF2F2F2FF000000FF000000FF000000FF0000
      00FF000000FFEEEEEEFF6A6B6EFFBA8344FFFFFFFFFFFFFCF6FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFCF6FFFFFFFFFFBA8344FF6A6B6EFFEEEEEEFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF636467FF696766FFE3E1E0FFDFDDDCFFDDDBDBFFDDDB
      DAFFDDDBDAFFDDDBDAFFDEDCDBFFDEDCDBFFDEDCDBFFDEDCDBFFDDDBDAFFDDDB
      DAFFDDDBDAFFDEDCDBFFDFDDDCFFE3E1E0FF696766FF636467FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFF3F3F3FF7C7E81FFB88144FFFFFFFFFFFFF7ECFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF7ECFFFFFFFFFFB88144FF7C7E81FFF3F3F3FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFB2B2B2FF949494FF9292
      92FF929292FF959697FF989C9FFFBE8545FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBE8545FF989C9FFF959697FF9292
      92FF929292FF949494FFB2B2B2FFFBFBFBFF000000FF000000FF000000FF0000
      00FF000000FF919293FF7C7E81FFB88144FFFFFFFFFFFFF7ECFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF7ECFFFFFFFFFFB88144FF7C7E81FF919293FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF797B7EFF807E7CFF7D7B79FF7A7876FF777573FF7371
      6FFF73716EFF757370FF787674FF7B7976FF7B7976FF787674FF757370FF7371
      6FFF747270FF787674FF7B7977FF7D7B79FF807E7CFF797B7EFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD5D5D5FFB78145FFFFFFFFFFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFFFFFFFB78145FFD5D5D5FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFEDEDEDFF68686CFFB98244FFFFFFFFFFFFFBF2FFFFFAF1FFFFFA
      F1FFFFFAF1FFFFFAF1FFFFFAF1FFFFFAF1FFFFFAF1FFFFFAF1FFFFFAF1FFFFFA
      F1FFFFFAF1FFFFFAF1FFFFFBF2FFFFFFFFFFB98244FF68686CFFEDEDEDFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF5F5F63FFB78145FFFFFFFFFFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFFFFFFFB78145FF5F5F63FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFE9DDCFFFA1A0A0FFECEA
      EAFFE1DFDEFFE6E4E3FF7F7F80FFE3D7CAFFE3D7CAFF7F7F80FFE6E4E3FFE1DF
      DEFFEDEBECFF7E7F7FFFE9DDCFFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD5D5D5FFB48147FFFFFFFFFFFFEEDAFFFFEEDAFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDAFFFFEEDAFFFFFFFFFFB48147FFD5D5D5FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFF7F7F7FF7C7D7FFFB88144FFFFFFFFFFFFF4E5FFFFF3E5FFFFF3
      E5FFFFF3E5FFFFF3E5FFFFF3E5FFFFF3E5FFFFF3E5FFFFF3E5FFFFF3E5FFFFF3
      E5FFFFF3E5FFFFF3E5FFFFF4E5FFFFFFFFFFB88144FF7C7D7FFFF7F7F7FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF717274FFB48147FFFFFFFFFFFFEEDAFFFFEEDAFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDAFFFFEEDAFFFFFFFFFFB48147FF717274FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFE9D9C7FF9D9E9EFFEBEA
      E9FFD6D4D3FFDCDAD9FF7D7D7DFFCCBEAFFFCCBEAFFF838484FFDCDAD9FFD6D4
      D3FFEDECEBFF7D7D7DFFE9D9C7FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD5D5D5FFB38048FFFFFFFFFFFFEBD2FFFFEBD3FFFFEB
      D4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEB
      D4FFFFEBD4FFFFEBD3FFFFEBD2FFFFFFFFFFB38048FFD5D5D5FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFFEFEFEFFB5B6B6FFB88144FFFFFFFFFFFFEEDAFFFFEEDAFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDAFFFFEEDAFFFFFFFFFFB88144FFB5B6B6FFFEFEFEFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFA5A6A6FFB38048FFFFFFFFFFFFEBD2FFFFEBD3FFFFEB
      D4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEB
      D4FFFFEBD4FFFFEBD3FFFFEBD2FFFFFFFFFFB38048FFA5A6A6FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFEDDAC3FF8D8D8DFFF9F8
      F8FFD1CECDFFD3D0CFFF989797FF9F968CFF9F968CFF989797FFD2D0CFFFD1CF
      CDFFFAF9F9FF7D7D7DFFEDDAC3FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD5D5D5FFB38048FFFFFFFFFFFFE6C9FFFFE7CBFFFFE7
      CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7
      CCFFFFE7CCFFFFE7CBFFFFE6C9FFFFFFFFFFB38048FFD5D5D5FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFC3C3C3FFB68145FFFFFFFFFFFFE9D0FFFFE9D1FFFFE9
      D2FFFFE9D2FFFFE9D2FFFFE9D2FFFFE9D2FFFFE9D2FFFFE9D2FFFFE9D2FFFFE9
      D2FFFFE9D2FFFFE9D1FFFFE9D0FFFFFFFFFFB68145FFC3C3C3FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB2B2B2FFB38048FFFFFFFFFFFFE6C9FFFFE7CBFFFFE7
      CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7
      CCFFFFE7CCFFFFE7CBFFFFE6C9FFFFFFFFFFB38048FFB2B2B2FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFF6DEC2FF80807FFFDBDA
      DAFFEEEDEDFFD3D1D0FFBEBCBBFF8A8989FF8A8989FFBDBCBBFFD3D1D0FFEEED
      EDFFDCDADBFF818180FFF6DEC2FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD5D5D5FFB38048FFFFFFFFFFFFE1BFFFFFE2C2FFFFE2
      C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2
      C3FFFFE2C3FFFFE2C2FFFFE1BFFFFFFFFFFFB38048FFD5D5D5FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFC3C3C3FFB48047FFFFFFFFFFFFE4C4FFFFE4C6FFFFE5
      C7FFFFE5C7FFFFE5C7FFFFE5C7FFFFE5C7FFFFE5C7FFFFE5C7FFFFE5C7FFFFE5
      C7FFFFE5C7FFFFE4C6FFFFE4C4FFFFFFFFFFB48047FFC3C3C3FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB2B2B2FFB38048FFFFFFFFFFFFE1BFFFFFE2C2FFFFE2
      C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2
      C3FFFFE2C3FFFFE2C2FFFFE1BFFFFFFFFFFFB38048FFB2B2B2FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFDDFBEFFD3BFA9FF8484
      84FFDAD9D9FFF8F8F7FFF9F9F8FFFBFBFAFFFBFBFAFFF9F9F8FFF8F8F7FFDAD9
      D9FF848484FFD3BFA9FFFDDFBEFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD7D7D7FFB38149FFFFFFFFFFFFDEB6FFFFDEB7FFFFDE
      B8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDE
      B8FFFFDEB8FFFFDEB7FFFFDEB6FFFFFFFFFFB38149FFD7D7D7FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFC5C5C5FFB38149FFFFFFFFFFFFDEB7FFFFDEB8FFFFDE
      B9FFFFDEB9FFFFDEB9FFFFDEB9FFFFDEB9FFFFDEB9FFFFDEB9FFFFDEB9FFFFDE
      B9FFFFDEB9FFFFDEB8FFFFDEB7FFFFFFFFFFB38149FFC5C5C5FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFB5B5B5FFB38149FFFFFFFFFFFFDEB6FFFFDEB7FFFFDE
      B8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDE
      B8FFFFDEB8FFFFDEB7FFFFDEB6FFFFFFFFFFB38149FFB5B5B5FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFFDDCB6FFD6BF
      A5FF818080FF7D7D7DFF7D7D7DFF7D7D7DFF7D7D7DFF7D7D7DFF7D7D7DFF8180
      80FFD6BFA5FFFDDCB6FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFE6E6E6FFB5834DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5834DFFE6E6E6FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFDADADAFFB5834DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5834DFFDADADAFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFCFCFCFFFB5834DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5834DFFCFCFCFFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFF7F7F7FFBA9060FFB5834CFFB38149FFB38049FFB380
      49FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB380
      49FFB38049FFB38049FFB38149FFB5834CFFBA9060FFF7F7F7FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFF3F3F3FFB68855FFB5834CFFB38149FFB38049FFB380
      49FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB380
      49FFB38049FFB38049FFB38149FFB5834CFFB68855FFF3F3F3FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFEFEFEFFFB58652FFB5834CFFB38149FFB38049FFB380
      49FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB380
      49FFB38049FFB38049FFB38149FFB5834CFFB58652FFEFEFEFFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FBFBFBFFF2F2F2FFEAEAEAFFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F2F2F2FFD4D4D4FFC0C0C0FFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFA350
      83FF9D4C7FFF924776FF8B4370FF8A436FFF8A436FFF8B4370FF924776FF9448
      79FF94497BFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF9E5518FF914E15FF8446
      19FF934955FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EAEAEAFFB99974FFB5834DFFB38149FFB38149FFB381
      49FFB38149FFB38149FFB38149FFB38149FFB38149FFB38149FFB38149FFB381
      49FFB38149FFB38149FFB38149FFB5834DFFB99974FFEAEAEAFF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFD279C0FFC16E
      B0FFA55E96FF894E7DFF79456FFF75436BFF75436BFF79456FFF874D7BFF9E57
      8DFFAA5A92FFA75086FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF9D7147FF8C6144FF9345
      77FFAD5B95FFCA6FB2FFD479C0FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFEAEAEAFF808080FF656666FF6666
      66FF666666FF666666FF666666FF666666FF666666FF656565FF666666FF6666
      66FF666666FF666666FF656666FF666666FF656666FF7F7F7FFFDADADAFFE9E9
      E9FFE9E9E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E9E9E9FFB5834CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5834CFFE9E9E9FF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFAB5E9AFF975489FF7743
      6CFF463F66FF26429BFF2B3BBFFF2B3BBEFF2B3BBEFF2B3BBFFF26429BFF4C43
      6CFF8D5583FFA55D93FFA44E85FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF5B5F59FF6D6E75FF745C
      82FF9E5F93FFA45D97FFA05991FFA95E98FFAE5F9DFF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FF656565FF5D5B5BFF5856
      56FF5F5E5EFF595656FF595757FF595656FF5C5B5BFF636363FF5C5B5BFF5956
      56FF595757FF595656FF636262FF585656FF5D5B5BFF656565FFB7B7B7FFBCBC
      BCFFBCBCBCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E9E9E9FFB48148FFFFFFFFFFFFF7EBFFFFF6EAFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EAFFFFF7EBFFFFFFFFFFB48148FFE9E9E9FF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFA55C94FF945385FF714066FF2642
      89FF3344C7FF5164EBFF6578FFFF697CFFFF697CFFFF6578FFFF5164EBFF3344
      C7FF29438AFF7F5476FF95467AFFA3A3A3FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF787E77FF6B4F78FF5E51
      4CFF3F5569FF4F4B74FF683B5DFF8B4F7EFF975488FF915182FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FF646465FF706E6CFF6360
      60FF656464FF646262FF646262FF646262FF646464FF626363FF646464FF6462
      62FF646262FF646262FF656464FF636060FF706E6DFF656565FF656666FF6666
      66FF656666FF666666FF656666FF808080FFEAEAEAFF00000000000000000000
      000000000000000000000000000000000000FBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFD8D8D8FFB58146FFFFFFFFFFFFEEDAFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDAFFFFFFFFFFB58146FFD8D8D8FFE9E9E9FFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF959595FF9D7018FF98690CFF9666
      07FF966506FF966607FF98690CFF9D711BFF6A6A68FF9D701BFF98690CFF9666
      07FF966506FF966506FF966506FF966506FF916306FF7B5406FF274482FF3446
      CBFF6072FEFF6174FEFF6073FDFF5F72FCFF5F72FCFF6073FDFF6174FEFF6072
      FEFF3546CAFF2C4982FF864572FF9F9F9FFF929292FF936916FF8B600BFF895D
      06FF895C05FF895D06FF8B600BFF936A19FF666664FF936919FF855C0AFF4363
      78FF607682FF6FBEEEFF2C6D9AFF4D4C33FF704C05FF6B4905FF243E77FF3040
      B9FF5868E8FF5B6DEEFF5D6FF4FF5A6CEFFF5869E8FF5869E7FF596AE8FF5868
      E8FF3040B9FF29447AFF834470FF000000FFECECECFF636363FF817E7EFF6A68
      68FF6C6A6AFF6D6B6BFF686767FF636464FF626262FF5F5F5EFF626262FF6464
      64FF686767FF6D6B6BFF6C6A6AFF6A6868FF817E7FFF636464FF585656FF5956
      56FF636262FF585656FF5D5B5BFF656565FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000F2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFB7B7B7FFB78145FFFFFFFFFFFFE6CAFFFFE6CBFFFFE6
      CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE6CCFFFFE6
      CCFFFFE6CCFFFFE6CBFFFFE6CAFFFFFFFFFFB78145FFB7B7B7FFBCBCBCFFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF878787FF775209FF8C8C8CFF8B8B
      8BFF8B8B8BFF8B8B8BFF8C8C8CFF775105FF525350FF775105FF8C8C8CFF8B8B
      8BFF8B8B8BFF8B8B8BFF8B8B8BFF8B8B8BFF898989FF606B72FF2C3FCEFF5A6F
      FDFF5B6FFCFF596EFAFF596DF9FF596DF9FF596DF9FF596DF9FF596EFAFF5B6F
      FCFF5B6FFCFF3242C7FF52456FFFA4A4A4FF7F7F7FFF604207FF686868FF6666
      66FF666666FF666666FF686868FF614204FF484946FF614204FF676767FF5C70
      72FF6DC5F6FFA7E4FFFF81CDFCFF4B96D1FF306CA8FF454D52FF202E98FF4252
      BBFF4352BBFF4859CBFF4F61DDFF4A5BCFFF4352BBFF4250B8FF4251B8FF4352
      BAFF4352BBFF2835A1FF4D4168FF000000FFF6F6F6FF636363FF8B8A89FF7876
      75FF757372FF6E6D6CFF636363FF606060FF8A8A88FF9C9C9AFF878786FF6060
      60FF636363FF6E6D6CFF757372FF787675FF8B8A89FF636363FF646161FF6462
      62FF656464FF636060FF706E6CFF646465FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000EAEAEAFF969696FF808080FF7E7E
      7EFF7E7E7EFF7F8081FF7D8186FFB98144FFFFFFFFFFFFDFB7FFFFDFB8FFFFDF
      B9FFFFDFB9FFFFDFB9FFFFDFB9FFFFDFB9FFFFDFB9FFFFDFB9FFFFDFB9FFFFDF
      B9FFFFDFB9FFFFDFB8FFFFDFB7FFFFFFFFFFB98144FF7D8186FF7F8081FF7E7E
      7EFF7E7E7EFF808080FF969696FFEAEAEAFF7E7E7EFFB48118FFB67E0EFFB47B
      09FFB47A08FFB47B09FFB67E0EFFB47F17FF7C612BFFB47F17FFB67E0EFFB47B
      09FFB47A08FFB47A08FFB47A08FFB67C07FFC18400FF534E8AFF4257EBFF556B
      FBFF5268F8FF5167F7FF5166F7FF5166F7FF5166F7FF5166F7FF5167F7FF5268
      F8FF566BFAFF475BE6FF28459FFFAAAAAAFF737373FFB48118FFB67E0EFFB47B
      09FFB47A08FFB47B09FFB67E0EFFB47E17FF624C22FFB37F16FFBA7E09FFC07A
      00FF6592ABFFF0FEFFFFA9DEFBFF56B6FCFF0F9AFFFF2D6AABFFCB8504FFBD80
      0AFFB87F0FFFAD7E25FF4051C4FF8F704BFFB67E0EFFB47B09FFB47A08FFB47B
      09FFB67E0EFFAF7F23FF253F91FF000000FFFDFDFDFFBEBEBEFF6C6D6BFF9B9A
      98FF878584FF7E7C7BFF616161FF59595AFFDFDFDEFF91918FFFD7D7D5FF5A5A
      5BFF616161FF7E7C7BFF878483FF9A9997FF6B6B6AFF636363FF686767FF6D6A
      6BFF6C6A6AFF6A6868FF817E7EFF636363FFECECECFF00000000000000000000
      000000000000000000000000000000000000E9E9E9FF808080FFEAE7E6FFE2DF
      DEFFE2E0DFFFE6E4E4FF757578FFBD8444FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBD8444FF757578FFE6E4E4FFE2E0
      DFFFE2DFDEFFEAE7E6FF808080FFE9E9E9FF7D7D7DFFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFB47B08FF7B7B7BFFB47B08FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF172BC3FF4B61F7FF4A61
      F5FF4159F3FF3D55F2FF3C54F2FF3C54F2FF3C54F2FF3C54F2FF3D55F2FF4159
      F3FF4A61F5FF4F64F6FF2D3CBFFFB1B1B1FF727272FFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFB47B08FF555555FFB47B08FFFFFFFFFFFFFF
      FFFFAFCBDFFF73B6DAFFEAFBFFFF5DBCFFFF1297FFFF119AFFFF2A65A1FFFFFF
      FFFFFFFFFFFFB67C09FF2A3AA8FFB47B08FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB67E0EFF2937AFFF000000FF00000000FDFDFDFFB5B5B5FF6C6A
      6AFF757576FF6C6B6BFF5E5D5DFF838383FFFFFFFFFF8C8B88FFFCFBF7FF807F
      7EFF5E5F5EFF6B6A6AFF727272FF666565FF727272FF616161FF636363FF6E6D
      6CFF757372FF787675FF8B8A89FF636363FFF6F6F6FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FF7E7E7EFFE3E1E0FFD6D4
      D2FFD6D4D2FFDAD9D7FF959494FF6B6B6DFF6B6A6BFF6A6868FF7C7A78FF7C7A
      78FF7B7A78FF7B7A78FF7B7A78FF7B7A78FF7B7A78FF7B7A78FF7B7A78FF7B7A
      78FF7C7A78FF7C7A78FF6A6868FF6B6A6BFF6B6B6DFF959494FFDAD9D7FFD6D4
      D2FFD6D4D2FFE3E1E0FF7E7E7EFFE9E9E9FF828282FFB47B09FFFFFFFFFFFFFF
      FFFFFEFEFDFFFFFFFFFFFFFFFFFFC5973DFFC79C46FFC5973DFFFFFFFFFFFFFF
      FFFFFEFEFDFFFEFEFDFFFEFEFDFFFFFFFDFFFFFFFFFF1628BBFF5C71F8FF3B53
      F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF3B53F2FF5F74F8FF2C3BBFFFB7B7B7FF777777FFB47B09FFFFFFFFFFFFFF
      FFFFFEFEFDFFFFFFFFFFFFFFFFFFC5973DFFC79C46FFC5973DFFFFFFFFFFFFFF
      FFFFFFFFFEFF136AC1FF76DAFFFFDDF5FFFF62BDFFFF1497FFFF0F97FFFF265E
      95FFFFFFFFFFCE9C3FFFC99D47FFC5973DFFFFFFFFFFFFFFFFFFFEFEFDFFFFFF
      FFFFFFFFFFFFB47B09FF2836AFFF000000FF0000000000000000FEFEFEFFDBDB
      DBFF696969FF626262FF5B5A59FFE4E1DFFFFFFFFFFF918B86FFFFFBF5FFD4D1
      CCFF5C5B5AFF5C5C5CFF6F6F6FFF868685FFD9D9D7FF5B5B5BFF616161FF7E7C
      7BFF878584FF9B9A98FF6C6C6BFFBEBDBEFFFDFDFDFF00000000000000000000
      000000000000000000000000000000000000E9E9E9FF7D7E7EFFE3E0DFFFCFCD
      CBFFCFCDCBFFD1CFCDFFD7D5D3FF8E8C8AFF61605FFF636160FF626160FF6260
      5FFF62605FFF62605FFF62605FFF62605FFF62605FFF62605FFF62605FFF6260
      5FFF62605FFF626160FF636160FF61605FFF8E8C8AFFD7D5D3FFD1CFCDFFCFCD
      CBFFCFCDCBFFE3E0DFFF7D7E7EFFE9E9E9FF858585FFB47A07FFFFFFFFFFFDFC
      F8FFFCFBF6FFFDFCF8FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFD
      F9FFFDFCF8FFFCFBF6FFFCFAF5FFFFFDF6FFFFFFFAFF1426BAFF6D80F8FF334E
      F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF334EF0FF7084F8FF2A3ABFFFBCBCBCFF7A7A7AFFB47A07FFFFFFFFFFFDFC
      F8FFFCFBF6FFFDFCF8FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFD
      FAFFFFFFF9FFFFFFFAFF176BC1FF77DAFFFFDEF6FFFF62BDFFFF1397FFFF0F96
      FFFF265E95FFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFAFFFCFAF6FFFCFAF5FFFDFC
      F8FFFFFFFFFFB47A07FF2635AFFF000000FFFBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE0E0E0FF7B7878FF938D89FFC5E7F6FF6BAACAFF5694B5FF3E7C9EFF88A7
      B5FF86817DFF7A7877FFF6F5F3FF8F8E8CFFFDFCF9FF80807EFF5F5F5FFF6C6B
      6BFF757475FF6B696AFFAAA9A9FFE7E7E7FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFFE9E9E9FF7E7E7EFFE3E2E1FFCBC8
      C7FFCBC8C7FFCCC8C8FFCECBCAFFD2CECDFFD5D1D1FFD6D3D2FFD6D3D2FFD6D3
      D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3D2FFD6D3
      D2FFD6D3D2FFD6D3D2FFD6D3D2FFD5D1D1FFD2CECDFFCECBCAFFCCC8C8FFCBC8
      C7FFCBC8C7FFE3E2E1FF7E7E7EFFE9E9E9FF8C8C8CFFB47A07FFFFFFFFFFFDFB
      F6FFFEFEFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFEFEFAFFFCFAF4FFFEFBF2FFFFFFF5FF1023BAFF8E9DF6FF324D
      EDFF2F4AECFF2C47EBFF2B47EBFF2B47EBFF2B47EBFF2B47EBFF2C47EBFF2F4A
      ECFF334EEDFF93A2F7FF2A3AC0FFC6C6C6FF808080FFB47A07FFFFFFFFFFFDFB
      F6FFFEFEFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFBFFFFFFF7FF176CC1FF77DAFFFFDEF6FFFF62BDFFFF1397
      FFFF0F96FFFF255D92FFFFFFF9FFFFFEF5FFFDFAF2FFFBF8F1FFFBF8F1FFFCF9
      F3FFFFFFFFFFB47A07FF2635AFFF000000FFF2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBBBBBBFF7C8A9BFF5385ABFF79B9D8FF6AA6C6FF5B96B6FF2A58
      7BFF33465DFFE8E2DEFFFFFFFFFF928C87FFFFFBF5FFD4D1CCFF5D5B5BFF615E
      60FF6B6669FFAEAEAEFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FFE9E9E9FF7E7E7EFFE5E3E3FFC5C2
      C1FFC6C3C2FFC6C3C2FFC8C5C4FFCBC8C7FFCDCAC9FFCECBCAFFCECBCAFFCECB
      CAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECBCAFFCECB
      CAFFCECBCAFFCECBCAFFCECBCAFFCDCAC9FFCBC8C7FFC8C5C4FFC6C3C2FFC6C3
      C2FFC5C2C1FFE5E3E3FF7E7E7EFFE9E9E9FF9A9A9AFFB47A08FFFFFFFFFFFDFD
      F8FFE7CFBBFFAB5812FF9E4000FF9E4100FF9E4100FF9E4100FF9E4100FF9E40
      00FFAB5812FFE7CFBBFFFDFCF6FFFDF9EFFFFFFFF0FF606BCDFF7A87E4FF5970
      F0FF324DEBFF3550EBFF3651EBFF3751EBFF3751EBFF3651EBFF3550EBFF324D
      EBFF5A71F1FF838FE6FF1E489DFFDADADAFF8D8D8DFFB47A08FFFFFFFFFFFDFD
      F8FFE7CFBBFFAB5812FF9E4000FF9E4100FF9E4100FF9E4100FF9E4100FF9E40
      00FFAB5812FFE9D0BCFFFFFFF7FFFFFFF1FF186CC2FF78DBFFFFDEF6FFFF62BD
      FFFF1397FFFF0F96FFFF255C91FFFFFFF2FFFFFCEFFFFCF8EEFFFBF7EDFFFBF7
      EFFFFFFFFFFFB47A08FF1B428FFF000000FFEAEAEAFF88B69EFF6DB290FF6DB3
      90FF6FB892FF75BF93FF28587EFF8BCEEEFF84C5E5FF79B9DAFF72B0D1FF66A3
      C7FF2C5381FFC9E8F4FF6CABCBFF5694B6FF3E7C9EFF87A5B4FF87807DFF616F
      67FF6BAD8DFF6DB893FF6DB692FF6DB290FF6CB18FFF6CB08EFF6CAF8EFF6CAF
      8EFF6CAF8EFF6DB08FFF88B59EFFEAEAEAFFE9E9E9FF7D7E7EFFE8E6E6FFBFBD
      BBFFC1BFBDFFC3C1BFFFC7C5C3FF979593FF7B7977FF7D7B79FF7D7B79FF7D7B
      79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B79FF7D7B
      79FF7D7B79FF7D7B79FF7D7B79FF7B7977FF979593FFC7C5C3FFC3C1BFFFC1BF
      BDFFBFBDBBFFE8E6E6FF7D7E7EFFE9E9E9FFA1A1A1FFB47A08FFFFFFFFFFFDFF
      FBFFAD5609FFD38E4FFFE1A368FFE0A369FFE0A368FFE0A368FFE0A369FFE1A3
      68FFD38F4FFFAD570BFFFDFFFAFFFCF9EFFFFFFFF0FFD1D2E4FF2738C5FFA1AE
      F6FF4D63EDFF2D48E9FF314BE9FF334DE9FF334DE9FF314BE9FF2D48E9FF4D63
      EEFFA3B0F8FF3545CCFF076353FFE7E7E7FF939393FFB47A08FFFFFFFFFFFDFF
      FBFFAD5609FFD38E4FFFE1A368FFE0A369FFE0A368FFE0A368FFE0A369FFE1A3
      68FFD38F4FFFAD570BFFFFFFFAFFFFFCEFFFFFFFF0FF1B70C5FF79DCFFFFDEF6
      FFFF62BDFFFF1397FFFF0F96FFFF255D91FFFFFFF0FFFFFCEFFFFDF8EDFFFAF6
      EDFFFFFFFFFFB47A08FF065A4CFF000000FFE9E9E9FF6CAF8DFFC9F3DAFFC4F2
      D7FFC8F7D9FFAAD7C5FF3C6DA0FF98DCFCFF89C9E9FF7EBEDEFF77B6D7FF70AF
      D0FF3D6E99FF5788ACFF7BBADAFF6AA7C6FF5B95B6FF29557AFF6D9BA3FFC9FC
      DBFFC5F5D8FFC4F3D7FFC3F2D6FFC3F1D6FFC3F1D6FFC4F1D6FFC4F2D7FFC4F2
      D7FFC3F1D6FFC9F3DAFF6CAF8DFFE9E9E9FFE9E9E9FF7D7D7DFFECEAEAFFBAB7
      B5FFBCB9B7FFC1BDBBFF8F8D8DFF767473FF797776FF797777FF797877FF7978
      77FF797877FF797877FF797877FF797877FF797877FF797877FF797877FF7978
      77FF797877FF797877FF797777FF797776FF767473FF8F8D8DFFC1BDBBFFBCB9
      B7FFBAB7B5FFECEAEAFF7D7D7DFFE9E9E9FFA3A3A3FFB47A08FFFFFFFFFFFCFE
      F7FFBA681EFFE6B789FFDF9E5CFFDF9C59FFDF9A55FFDF9A55FFDF9C59FFDF9E
      5DFFE7B88AFFB1580AFFFDFFF8FFFCF8ECFF838485FFC2C0B1FF555FBAFF3142
      C8FFA5B1F5FF7B8DF1FF3E57E9FF2542E5FF2542E5FF3E57E9FF7B8DF1FFA5B1
      F6FF3347D0FF6D5C6FFF006835FF000000FF959595FFB47A08FFFFFFFFFFFCFE
      F7FFBA681EFFE6B789FFDF9E5CFFDF9C59FFDF9A55FFDF9A55FFDF9C59FFDF9E
      5DFFE7B88AFFB1580AFFFDFFF8FFFDF9ECFF878585FFC8BCB0FF1B6FC4FF78DB
      FFFFDEF6FFFF62BDFFFF1397FFFF0F96FFFF285F94FFCBBFB1FFBAB5AEFFFCF6
      E9FFFFFFFFFFB47A08FF005F30FF000000FFE9E9E9FF69AC8AFFC8F2DAFF64A8
      85FF6AAF88FF3F747DFF30699AFFA0E5FFFF90D2F1FF85C5E5FF7EBDDDFF78B8
      D8FF1E4B78FF5589B1FF87C9E8FF7AB9DBFF72B0D1FF66A1C6FF24527EFF6FB6
      8BFF68AD88FF65AB87FF65AB87FF65AB87FF66AB88FF68AC8AFF66A987FF66AA
      88FF64A784FFC8F2DAFF69AC8AFFE9E9E9FFE9E9E9FF7D7D7DFFEFEEEDFFB2AE
      ACFFB4B0AEFFBAB7B5FF676564FF6E6C6BFF706E6DFF706E6DFF706E6DFF706E
      6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E6DFF706E
      6DFF706E6DFF706E6DFF706E6DFF706E6DFF6E6C6BFF676564FFBAB7B5FFB4B0
      AEFFB2AEACFFEFEEEDFF7D7D7DFFE9E9E9FFA4A4A4FFB47A08FFFFFFFFFFFAFA
      F1FFC67B37FFEDC9A7FFEAAD6BFFEAAF6FFFF6FAFEFFECF0F1FFEAB06FFFEBAE
      6CFFEECBAAFFBB6516FFFBFCF3FFF9F5E7FFFDF6E7FFFFFAE7FFFFFFE7FF7E85
      CFFF2436C4FF7280E3FFA3AFF5FFAEB9F8FFAEB9F8FFA3AFF5FF7280E3FF2336
      C5FF8896EEFFC78901FF006835FF000000FF969696FFB47A08FFFFFFFFFFFAFA
      F1FFC67B37FFEDC9A7FFEAAD6BFFEAAF6FFFF6FAFEFFECF0F1FFEAB06FFFEBAE
      6CFFEECBAAFFBB6516FFFBFCF3FFF9F5E7FFFDF6E7FFFFF9E7FFFFFDE5FF196D
      C4FF78DBFFFFDEF6FFFF62BDFFFF1497FFFF1197FFFF286096FFFFFFE9FFFFF7
      E6FFFFFFFFFFB47A08FF005F30FF000000FFE9E9E9FF68AA87FFC9F4DEFF5B9F
      79FF66A87EFF3A7177FF3C7AA9FFADF4FFFF9DE2FFFF91D4F3FF86C7E8FF82C4
      E2FF1A4672FF548DB6FF8FD0EFFF7FC0E0FF77B6D7FF70AFD0FF3C6B9BFFA1D0
      C0FFBAECCFFFB6E8CDFFB6E7CDFFB6E7CDFFB6E8CEFFBBECD2FF5B9E79FF5FA1
      7CFF5B9E78FFC9F4DEFF68AA87FFE9E9E9FFE9E9E9FF7D7D7DFFF2F1F0FFECEC
      EBFFEDEDECFFF5F4F3FF797776FF807E7DFF817F7EFF817F7EFF817F7EFF817F
      7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F7EFF817F
      7EFF817F7EFF817F7EFF817F7EFF817F7EFF807E7DFF797776FFF5F4F3FFEDED
      ECFFECECEBFFF2F1F0FF7D7D7DFFE9E9E9FFACACACFFB47A08FFFFFFFFFFF8F6
      EAFFCF8A47FFF3DCC8FFF6BF7CFFFFF1DFFFFFFEFEFFF5F4F3FFECDDC8FFF8C1
      7FFFF4DECBFFC57020FFF9F8ECFFF8F2E2FFFBF3E2FFFCF4E3FFFFF7E3FFFFFE
      E3FFCDCBD9FF626CCCFF1527C1FF1729C1FF1729C1FF1427C0FF606ACAFFCBC9
      D9FFFFFFFFFFB97F07FF006836FF000000FF9D9D9DFFB47A08FFFFFFFFFFF8F6
      EAFFCF8A47FFF3DCC8FFF6BF7CFFFFF1DFFFFFFEFEFFF5F4F3FFECDDC8FFF8C1
      7FFFF4DECBFFC57020FFF9F8ECFFF8F2E2FFFBF3E2FFFDF4E3FFFFF7E2FFFFFC
      E1FF196EC4FF78DBFFFFDEF6FFFF62BDFFFF1397FFFF0C97FFFF1F5E9AFFFFFD
      E8FFFFFFFFFFB57C09FF005F31FF000000FFE9E9E9FF65A985FFCCF3DFFF5398
      6EFF5A9D6DFF174770FF1C5789FF1F5381FF194673FF40729AFF82C4E3FF77B2
      D2FF0E3262FF255786FF99DCFBFF87C7E7FF7EBDDDFF78B8D8FF1C497AFF6694
      A0FFB5EACBFFAFE3C8FFAFE2C8FFAFE3C8FFAFE3C8FFB3E6CCFF60A07AFF5093
      6AFF52976DFFCCF3DFFF65A985FFE9E9E9FFE9E9E9FF7D7D7DFFD7D5D4FFC8C5
      C4FFC8C6C5FFCECDCCFF969391FF9C9997FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9D9A
      98FF9D9A98FF9D9A98FF9D9A98FF9D9A98FF9C9997FF969391FFCECDCCFFC8C6
      C5FFC8C5C4FFD7D5D4FF7D7D7DFFE9E9E9FFB0B0B0FFB47B08FFFFFFFFFFF6F1
      E2FFD79452FFE8BD94FFFFEAD3FFB3D9EFFF5DA1C5FF3C7FA4FF799CB0FFFFEE
      D6FFE8BE95FFD79554FFF7F3E3FFF9F1DFFF808287FFB4B2ADFFB3B1ACFFB4B2
      ABFFFFF9DFFF8F8F8AFFC5C1AFFFC6C1AEFFC6C1AEFFC4C0AEFFBEBAABFFFFF5
      DDFFFFFFFFFFB57C08FF6C9280FF000000FFA1A1A1FFB47B08FFFFFFFFFFF6F1
      E2FFD79452FFE8BD94FFFFEAD3FFB3D9EFFF5DA1C5FF3C7FA4FF799CB0FFFFEE
      D6FFE8BE95FFD79554FFF7F3E3FFF9F1DFFF808287FFB4B2ADFFB4B2ACFFB8B2
      ABFFFFF8DCFF186DC4FF78DBFFFFDEF6FFFF5FBFFFFF0999FFFF85786DFF6C68
      66FFFFFFFFFFB97F0AFF638575FF000000FFE9E9E9FF63A783FFCDF2E0FF62A6
      80FFC2F6D5FF06326FFF2C74A2FF4785AFFF5A8CB4FF567CA4FF30527CFF2547
      72FF1A487AFF0D396DFFA6EDFFFF93D5F4FF86C7E8FF82C4E3FF194575FF588C
      95FFB4EAC9FFADE2C6FFAEE2C8FFAFE3C9FFAFE2C8FFADE2C7FFB0E4CAFFB5E8
      CFFF60A480FFCDF2E0FF63A783FFE9E9E9FFE9E9E9FF7D7D7DFFEAE9E8FFCECC
      CBFFCFCDCCFFD5D3D2FF868382FF8C8988FF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B
      8AFF8E8B8AFF8E8B8AFF8E8B8AFF8E8B8AFF8C8988FF868382FFD5D3D2FFCFCD
      CCFFCECCCBFFEAE9E8FF7D7D7DFFE9E9E9FFB2B2B2FFB47B08FFFFFFFFFFF2EC
      D8FFFFFDFEFFE8B481FFE9A258FFBAC1B8FF6099B8FF437997FF909187FFECA5
      5CFFE8B581FFFFFEFFFFF2ECD9FFF5EDD8FFF8EFDAFFF8F0DAFFF7EFDAFFF7EF
      D9FFF7EFD9FFFAF1DAFFFBF2DBFFFBF2DBFFFBF2DBFFFAF2DAFFF8EFD8FFF4EC
      D6FFFFFFFFFFB47B08FFB0B0B0FF000000FFA3A3A3FFB47B08FFFFFFFFFFF2EC
      D8FFFFFDFEFFE8B481FFE9A258FFBAC1B8FF6099B8FF437997FF909187FFECA5
      5CFFE8B581FFFFFEFFFFF2ECD9FFF5EDD8FFF8EFDAFFF8F0DAFFF7EFDAFFF8EF
      D9FFFBF0D8FFFFF5D6FF186EC5FF76DCFFFFD7F9FFFFAB9E93FF9F9895FF6E6C
      6DFF6C7072FFC08705FF999999FFF9F9F9FFE9E9E9FF61A580FFCFF2E1FF5DA2
      7AFFBCF1D0FF06316DFF2971A0FF4387B1FF528CB3FF5A8AB3FF4D7CA6FF3764
      91FF184679FF123F72FF1E4C78FF41739AFF82C4E3FF77B2D2FF0D3264FF1D4E
      77FFB4EBCAFFADE2C8FF71B290FF65A987FF72B290FFACE0C8FFAADFC6FFADE2
      C9FF5AA079FFCFF2E1FF61A580FFE9E9E9FFEAEAEAFF7C7D7DFFF9F9F8FFD3D1
      D0FFD4D2D1FFDCDAD9FF797675FF807D7CFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E7DFF817E
      7DFF817E7DFF817E7DFF817E7DFF817E7DFF807D7CFF797675FFDCDAD9FFD4D2
      D1FFD3D1D0FFF9F9F8FF7C7D7DFFEAEAEAFFB2B2B2FFB47B08FFFFFFFFFFF0E8
      D0FFFCF2E9FFFFF7EFFFEDE3DAFF75A7BFFF77B9DDFF69AACEFF5B8AA2FFFEEF
      E3FFFFF7EFFFFCF2EAFFF1E8D2FFF3EAD4FFF6EDD5FFF6EDD5FFF5ECD5FFF7ED
      D5FFF6EDD5FFF6ECD5FFF5ECD5FFF7EDD5FFF7EDD6FFF6EDD5FFF4EBD4FFF2E9
      D2FFFFFFFFFFB47B08FFB2B2B2FF000000FFA3A3A3FFB47B08FFFFFFFFFFF0E8
      D0FFFCF2E9FFFFF7EFFFEDE3DAFF75A7BFFF77B9DDFF69AACEFF5B8AA2FFFEEF
      E3FFFFF7EFFFFCF2EAFFF1E8D2FFF3EAD4FFF6EDD5FFF6EDD5FFF5ECD5FFF7ED
      D5FFF7EDD5FFFCEFD4FFFFF5D4FF1772CCFFB7ACA6FFE9E5E2FFC3C2C0FF9A9B
      99FF797E76FF9867DAFF8C8C8CFFEEEEEEFFE9E9E9FF5FA47EFFD2F2E4FF599E
      76FFB4EDCCFF0D3870FF185588FF3883B0FF4487B1FF4985AFFF3F77A2FF2D61
      90FF144376FF154375FF5D90B8FF577DA5FF30527CFF254772FF19477BFF0835
      70FFB2ECC9FFABE3C8FF5B9D78FF5FA07CFF5B9D78FFA8E1C7FFA5DEC3FFA8E1
      C6FF589C75FFD2F2E4FF5FA47EFFE9E9E9FFF2F2F2FF7E7E7EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF6A6868FF6E6C6EFF545657FF535454FF535354FF5353
      54FF535354FF535354FF535354FF535354FF535354FF535354FF535354FF5353
      54FF535354FF535354FF535454FF545657FF6E6C6EFF6A6868FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF7E7E7EFFF2F2F2FFB2B2B2FFB47B08FFFFFFFFFFEFE6
      CDFFF6E1CCFFFFE8D1FF687A93FF91DBFEFF85C6E7FF76B5D7FF64A6CBFF7D8B
      9DFFFFE9D1FFF6E1CDFFF0E7CFFFF4E9D0FF81838AFFB2AFA8FFF6ECD2FF8385
      8CFFB3B0A9FFB1AEA7FFF6EBD2FF83858CFFB3B0A9FFB2B0A9FFB0ADA6FFF2E8
      CEFFFFFFFFFFB47B08FFB2B2B2FF000000FFA3A3A3FFB47B08FFFFFFFFFFEFE6
      CDFFF6E1CCFFFFE8D1FF687A93FF91DBFEFF85C6E7FF76B5D7FF64A6CBFF7D8B
      9DFFFFE9D1FFF6E1CDFFF0E7CFFFF4E9D0FF81838AFFB2AFA8FFF6ECD2FF8385
      8CFFB3B0A9FFB2AFA7FFFCEFD2FF8E8C8DFFA39F9DFFD7D5D4FFE6E8E5FF868A
      81FFCA8CC9FFC083BCFF986ACAFFEEEEEEFFE9E9E9FF5EA17CFFD3F4E7FF569B
      72FFACE6C8FF6EA3A4FF052F6BFF1D598CFF307AA8FF3379A7FF2B6D9AFF1A4F
      81FF144072FF306D9BFF548FB6FF5A8BB4FF4D7CA6FF376491FF164579FF0936
      71FFAEEAC7FFA6E0C5FF5FA079FF509269FF5FA079FFA5DEC4FFA2DCC1FFA5DF
      C4FF559A72FFD3F4E7FF5EA17CFFE9E9E9FFFBFBFBFFA5A5A5FF7E7E7EFF7C7C
      7CFF7C7C7CFF7E7F80FF808387FFC08646FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC08646FF808387FF7E7F80FF7C7C
      7CFF7C7C7CFF7E7E7EFFA5A5A5FFFBFBFBFFB2B2B2FFB47B08FFFFFFFFFFEFE5
      CBFFF2D2B4FFFFDCB9FF002E62FFA9F3FFFF96D9F7FF85C7E6FF78BCDDFF0022
      57FFFFDDBBFFF2D2B5FFEFE6CEFFF1E6CDFFF4E9D0FFF4E9CFFFF3E8CEFFF4E9
      CFFFF4E9CFFFF3E8CFFFF3E8CFFFF4E9D0FFF5E9D0FFF4E9CFFFF2E7CDFFF0E4
      CAFFFFFFFFFFB47B08FFB2B2B2FF000000FFA3A3A3FFB47B08FFFFFFFFFFEFE5
      CBFFF2D2B4FFFFDCB9FF002E62FFA9F3FFFF96D9F7FF85C7E6FF78BCDDFF0022
      57FFFFDDBBFFF2D2B5FFEFE6CEFFF1E6CDFFF4E9D0FFF4E9CFFFF3E8CEFFF4E9
      CFFFF4E9CFFFF4E8CFFFF4E8CFFFF7EBD0FFF9EDD1FF9E9E9DFF969992FFDBA1
      DCFFD099CFFFC88FC4FF9E70CCFFF9F9F9FFE9E9E9FF5CA079FFD7F5EAFF5298
      6EFFA5E0C4FFA8E3C3FF75ADA9FF154275FF04306BFF08326DFF09346FFF0936
      6EFF175083FF3A85B2FF4589B2FF4985AFFF3F76A2FF2C6191FF114076FF103E
      74FFA9E5C3FFA0DBC0FFA2DCC2FFA3DDC3FFA2DCC2FF9FDABFFF9FDABFFFA2DD
      C3FF52976EFFD7F5EAFF5CA079FFE9E9E9FF0000000000000000000000000000
      000000000000EEEEEEFF6A6B6EFFBA8344FFFFFFFFFFFFFCF6FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFBF4FFFFFB
      F4FFFFFBF4FFFFFBF4FFFFFCF6FFFFFFFFFFBA8344FF6A6B6EFFEEEEEEFF0000
      000000000000000000000000000000000000B2B2B2FFB47B08FFFFFFFFFFEDE4
      C9FFEDC29AFFFFCFA1FF00316AFF1E5E8FFF5488B2FF7CB3D4FF6399BDFF001E
      57FFFFD1A3FFEDC39CFFEDE5CCFFEFE5CCFFF0E7D0FFF0E7D0FFF0E7CFFFF0E6
      CDFFF0E6CDFFF0E7D0FFF0E7D0FFF0E7CFFFF0E7CFFFF0E7CEFFEFE5CCFFEDE2
      C7FFFFFFFFFFB47B08FFB2B2B2FF000000FFA3A3A3FFB47B08FFFFFFFFFFEDE4
      C9FFEDC29AFFFFCFA1FF00316AFF1E5E8FFF5488B2FF7CB3D4FF6399BDFF001E
      57FFFFD1A3FFEDC39CFFEDE5CCFFEFE5CCFFF0E7D0FFF0E7D0FFF0E7CFFFF0E6
      CDFFF0E6CDFFF0E7D0FFF0E7D0FFF0E7CFFFF2E9D0FFF6EFD0FFBA7FDAFFE3AF
      E6FFD9A5DBFFAA79D9FFA3A3A3FF000000FFE9E9E9FF5A9E77FFDCF6ECFF5094
      6BFFA2DDC4FFA0DCC1FFA2DEC1FFA5E2C2FFA7E4C2FFA8E5C3FFABE8C4FF6DA4
      A3FF07316DFF1E5A8DFF317BA9FF3479A8FF2C6D9BFF184D82FF0C3872FF5690
      8DFFA2DEC1FF9BD8BDFF9BD7BDFF9BD7BDFF9BD7BDFF9CD7BEFF9ED9C0FFA1DD
      C4FF50946BFFDCF6ECFF5A9E77FFE9E9E9FF0000000000000000000000000000
      000000000000F9F9F9FF7C7E81FFB88144FFFFFFFFFFFFF7ECFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6EBFFFFF6
      EBFFFFF6EBFFFFF6EBFFFFF7ECFFFFFFFFFFB88144FF7C7E81FFF9F9F9FF0000
      000000000000000000000000000000000000B2B2B2FFB47B08FFFFFFFFFFECE3
      C6FFE7B381FFF9BF88FF003973FF2B7BAFFF4E8EBDFF517FACFF274877FF0022
      5EFFF8C18AFFE7B482FFEDE5CAFFEEE5CBFFAD6E00FFD0AC65FFCFAB62FFCEA9
      60FFEFE6CEFFAD6E00FFD0AC65FFCFAB63FFCFAB62FFCFAB62FFCEA85DFFECE1
      C4FFFFFFFFFFB47B08FFB2B2B2FF000000FFA3A3A3FFB47B08FFFFFFFFFFECE3
      C6FFE7B381FFF9BF88FF003973FF2B7BAFFF4E8EBDFF517FACFF274877FF0022
      5EFFF8C18AFFE7B482FFEDE5CAFFEEE5CBFFAD6E00FFD0AC65FFCFAB62FFCEA9
      60FFEFE6CEFFAD6E00FFD0AC65FFCFAB63FFCFAB62FFD1AE62FFD3B25DFFB77F
      D8FFB37EDCFFB98400FFA3A3A3FF000000FFE9E9E9FF589C75FFE0F7EFFF61A5
      80FF69AC89FF71B290FF9CD9BFFF9AD7BDFF9AD7BCFF9AD7BCFF9CD9BDFFA2E0
      C0FF72ABA7FF124072FF093571FF0E3874FF0D3775FF124072FF448071FF9EDC
      BEFF9BD8BDFF98D5BCFF98D5BBFF98D5BBFF98D5BCFF9BD7BEFF71B18FFF69AC
      89FF61A580FFE0F7EFFF589C75FFE9E9E9FF0000000000000000000000000000
      00000000000000000000E9E9E9FFB78145FFFFFFFFFFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFF2
      E3FFFFF2E3FFFFF2E3FFFFF2E3FFFFFFFFFFB78145FFE9E9E9FF000000000000
      000000000000000000000000000000000000B3B3B3FFB47B09FFFFFFFFFFEBE2
      C4FFE1A268FFEEAD6FFF344E6CFF1870A9FF3785B9FF3777A9FF154374FF3448
      68FFEEAE70FFE1A369FFECE4C7FFECE1C4FFEEE3C7FFEEE3C7FFEDE2C6FFEDE1
      C5FFEDE1C5FFEEE3C8FFEEE3C7FFEDE2C7FFEDE2C6FFEDE2C6FFECE0C3FFEBDE
      BEFFFFFFFFFFB47B09FFB3B3B3FF000000FFA4A4A4FFB47B09FFFFFFFFFFEBE2
      C4FFE1A268FFEEAD6FFF344E6CFF1870A9FF3785B9FF3777A9FF154374FF3448
      68FFEEAE70FFE1A369FFECE4C7FFECE1C4FFEEE3C7FFEEE3C7FFEDE2C6FFEDE1
      C5FFEDE1C5FFEEE3C8FFEEE3C7FFEDE2C7FFEDE2C6FFEDE3C6FFEEE3C3FFEEE4
      BEFFFFFFFFFFB67E07FFA4A4A4FF000000FFE9E9E9FF559B72FFE3F9F2FF599E
      78FF5FA27DFF5EA07BFF9BD9C1FF99D7BEFF98D7BDFF98D7BDFF99D7BEFF9AD9
      BFFF9FDEC1FF95D6B4FF6DAF82FF5B9F6EFF5B9E6EFF6CAE83FF92D3B4FF9CDB
      C1FF99D7BEFF98D7BDFF98D7BDFF98D7BDFF99D7BEFF9BD9C1FF5EA07BFF5FA2
      7DFF599E78FFE3F9F2FF559B72FFE9E9E9FF0000000000000000000000000000
      00000000000000000000E9E9E9FFB48147FFFFFFFFFFFFEEDAFFFFEEDAFFFFEE
      DBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEEDBFFFFEE
      DBFFFFEEDBFFFFEEDAFFFFEEDAFFFFFFFFFFB48147FFE9E9E9FF000000000000
      000000000000000000000000000000000000B9B9B9FFB47B09FFFFFFFFFFEADF
      BDFFD88C46FFE0944CFFBC8250FF214365FF014884FF00417CFF213F61FFBC84
      52FFE0944CFFD88D46FFEBE0C1FFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADC
      BBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADBB9FFE9DA
      B7FFFFFFFFFFB47B09FFB9B9B9FF000000FFA9A9A9FFB47B09FFFFFFFFFFEADF
      BDFFD88C46FFE0944CFFBC8250FF214365FF014884FF00417CFF213F61FFBC84
      52FFE0944CFFD88D46FFEBE0C1FFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADC
      BBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCB9FFE9DB
      B7FFFFFFFFFFB57B09FFA9A9A9FF000000FFEAEAEAFF549870FFE7F9F4FF478D
      61FF4D9267FF4C9066FF478C60FF468B5EFF468B5EFF468B5EFF468B5EFF468B
      5EFF478C5FFF498E61FF4B9063FF4D9165FF4D9165FF4B9063FF488D61FF478B
      5FFF468B5EFF468B5EFF468B5EFF468B5EFF468B5EFF478C60FF4C9066FF4D92
      67FF478D61FFE7F9F4FF549870FFEAEAEAFF0000000000000000000000000000
      00000000000000000000E9E9E9FFB38048FFFFFFFFFFFFEBD2FFFFEBD3FFFFEB
      D4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEBD4FFFFEB
      D4FFFFEBD4FFFFEBD3FFFFEBD2FFFFFFFFFFB38048FFE9E9E9FF000000000000
      000000000000000000000000000000000000BFBFBFFFB57B09FFFFFFFFFFE7D9
      B4FFE9DCB9FFEBDFBCFFF1E2BBFFF8E4BAFFFCE5B8FFFCE5B9FFF8E5BAFFF1E2
      BCFFEBDFBCFFE9DDBAFFE8DAB5FFE8D8B3FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8
      B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D7B2FFE7D7
      B1FFFFFFFFFFB57B09FFC1C1C1FF000000FFAFAFAFFFB57B09FFFFFFFFFFE7D9
      B4FFE9DCB9FFEBDFBCFFF1E2BBFFF8E4BAFFFCE5B8FFFCE5B9FFF8E5BAFFF1E2
      BCFFEBDFBCFFE9DDBAFFE8DAB5FFE8D8B3FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8
      B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D7B2FFE7D7
      B1FFFFFFFFFFB57B09FFB1B1B1FF000000FFF2F2F2FF53986EFFE8F8F4FFE3F5
      F0FFE4F5F0FFE4F5F0FFE3F5EFFFE3F5EFFFE3F5EFFFE3F5EFFFE3F5EFFFE3F5
      EFFFE3F5EFFFE4F5F0FFE4F5F0FFE4F6F0FFE4F6F0FFE4F5F0FFE4F5F0FFE3F5
      EFFFE3F5EFFFE3F5EFFFE3F5EFFFE3F5EFFFE3F5EFFFE3F5EFFFE4F5F0FFE4F5
      F0FFE3F5F0FFE8F8F4FF53986EFFF2F2F2FF0000000000000000000000000000
      00000000000000000000E9E9E9FFB38048FFFFFFFFFFFFE6C9FFFFE7CBFFFFE7
      CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7CCFFFFE7
      CCFFFFE7CCFFFFE7CBFFFFE6C9FFFFFFFFFFB38048FFE9E9E9FF000000000000
      000000000000000000000000000000000000CCCCCCFFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB67E0EFFD2D2D2FF000000FFBFBFBFFFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB67E0EFFC7C7C7FF000000FFFBFBFBFF85B297FF7EC0A1FF9AD9
      BFFF96D6BBFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5
      BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5
      BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF95D5BAFF96D6
      BBFF9AD9BFFF7EC0A1FF85B297FFFBFBFBFF0000000000000000000000000000
      00000000000000000000E9E9E9FFB38048FFFFFFFFFFFFE1BFFFFFE2C2FFFFE2
      C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2C3FFFFE2
      C3FFFFE2C3FFFFE2C2FFFFE1BFFFFFFFFFFFB38048FFE9E9E9FF000000000000
      000000000000000000000000000000000000E1E1E1FFB88319FFB67E0EFFB57B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB57B
      09FFB67E0EFFB48524FFE2E2E2FF000000FFDDDDDDFFB88318FFB67E0EFFB57B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB57B
      09FFB67E0EFFB4821BFFDEDEDEFF000000FF00000000FCFCFCFF86B197FF93CA
      ACFFBDEED4FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBEC
      D2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBEC
      D2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBBECD2FFBDEE
      D4FF93CAACFF86B197FFFCFCFCFF000000000000000000000000000000000000
      00000000000000000000EAEAEAFFB38149FFFFFFFFFFFFDEB6FFFFDEB7FFFFDE
      B8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDEB8FFFFDE
      B8FFFFDEB8FFFFDEB7FFFFDEB6FFFFFFFFFFB38149FFEAEAEAFF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000FBFBFBFF86B3
      96FF509269FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF4F91
      68FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF4F91
      68FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF4F9168FF5092
      69FF86B396FFFBFBFBFF00000000000000000000000000000000000000000000
      00000000000000000000F2F2F2FFB5834DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5834DFFF2F2F2FF000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FBFBFBFFCAA984FFB5834CFFB38149FFB38049FFB380
      49FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB38049FFB380
      49FFB38049FFB38049FFB38149FFB5834CFFCAA984FFFBFBFBFF000000000000
      000000000000000000000000000000000000FEFEFEFFF9F9F9FFF0F0F0FFEAEA
      EAFFE9E9E9FFE8E8E8FFE4E4E4FFDBDBDBFFD6D6D6FFD5D5D5FFD5D5D5FFD1D1
      D1FFCACACAFFC4C4C4FFC9C9C9FFD0D0D0FFD1D1D1FFD0D0D0FFCECECEFFCBCB
      CBFFC6C6C6FFC7C7C7FFD0D0D0FFD6D6D6FFD6D6D6FFD4D4D4FFD9D9D9FFE4E4
      E4FFEAEAEAFFF0F0F0FFF9F9F9FFFEFEFEFF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CECE
      CEFFC1C1C1FFB6B6B6FFB8B8B8FFBEBEBEFFBFBFBFFFBEBEBEFFBCBCBCFFB9B9
      B9FFB5B5B5FFB6B6B6FFBEBEBEFFC4C4C4FFC4C4C4FFC2C2C2FFC7C7C7FFD4D4
      D4FFE0E0E0FFECECECFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F8F8F8FFE3E3E3FFCBCBCBFFBFBF
      BFFFBCBCBCFFB7B7B7FFA4A4A4FF8F8F8FFF838383FF808080FF7F7F7FFF7878
      78FF696969FF616161FF666666FF6E6E6EFF6D6D6DFF666666FF626262FF5B5B
      5BFF565656FF5D5D5DFF6E6E6EFF787878FF787878FF767676FF808080FF9797
      97FFABABABFFBFBFBFFFDEDEDEFFF7F7F7FF0000000000000000000000000000
      00000000000000000000000000000000000000000000000000007C7C7CFF6C6C
      6CFF565656FF4B4B4BFF4C4C4CFF515151FF505050FF4B4B4BFF484848FF4343
      43FF3F3F3FFF454545FF515151FF585858FF585858FF575757FF5F5F5FFF7575
      75FF8D8D8DFFACACACFFD7D7D7FFF6F6F6FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EEEEEEFFC4B8ADFFA76630FFB363
      1DFFB1601AFFB1601AFFB1601AFFB1601AFFB1601AFFB1601AFFB1601AFFB160
      1AFFB1601AFFB3631DFF9A4C09FF844611FF844620FF9B4B6BFFB55B98FFB35A
      96FFB35996FFB25996FFB35A96FFB45A97FFB35A96FFB25996FFB35996FFB35A
      96FFB55B98FF9C5A84FFAEA1A9FFE7E7E7FF0000000000000000A4642FFFAB5F
      1CFFA55918FFA25818FFA25818FFA25818FFA25818FFA15718FF985216FF8949
      1AFF944A56FF9C4D6FFFA55085FFA55085FFA55085FFA55085FFA55085FFA550
      85FFA55085FFA55085FFA55085FFA55085FFA55085FFA55085FFA55085FF9C4C
      7FFF96497BFF7A4566FF9A8E95FFE2E2E2FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A450
      84FFA14E82FF9B4B7DFF97497AFF97497AFF97497AFF97497AFF9B4B7DFF984A
      7CFF95497BFF000000000000000000000000E7E7E7FFAD5A12FFDA9A60FFE1A4
      68FFE0A165FFE0A165FFE0A165FFE0A165FFE0A165FFE0A165FFE0A165FFE0A1
      65FFE0A165FFE1A468FFDA9B5EFFAA5B0AFFAB5292FFDC81CAFFCA76BAFFBE6E
      AEFFCA75BAFFD87CC6FFBF6EAFFFBE6DAEFFBF6EAFFFD87CC6FFCA75BAFFBE6E
      AEFFCA76BAFFDC81C8FFAD538DFFD3D3D3FFE6E6E6FFA85711FFC58B57FFB987
      56FFAD7C4EFFA7784BFFA5774AFFA5774AFFA5774AFFA5774AFFA2704FFFAC51
      8BFFBE64A4FFD173B9FFD67AC2FFD77BC5FFD77BC4FFD77BC4FFD87BC4FFD87B
      C5FFD87BC5FFD87BC5FFD87BC5FFD77BC4FFD77BC4FFD87BC5FFD67AC2FFCF72
      B8FFBE64A4FFAB528AFF8C4372FFC6C6C6FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D57AC2FFCC74
      BAFFBD6CACFFAC629DFFA25C94FF9F5B91FF9F5B91FFA25C94FFAA619AFFB564
      A1FFB45F9BFFA95188FF0000000000000000DDDDDDFFB25F15FFE7B687FFE09E
      5CFFDF9E5DFFDF9E5DFFDF9D5CFFDF9B59FFDF9A56FFDF9B59FFDF9D5CFFDF9E
      5DFFDF9E5DFFE09E5CFFE8B785FFB2610AFFB2519DFFEA9ADCFFB365A1FFE381
      CDFFE17FCCFFE27FCDFFC96EB6FFAF5F9EFFCA6EB6FFE27FCDFFE17FCCFFE381
      CDFFB365A2FFEA9ADAFFB35493FFC3C3C3FFD8D8D8FF9D5413FFAC8866FF7F6A
      57FF73685EFF6A6766FF6A6766FF6A6766FF696765FF646962FFA45588FFCD72
      B6FFE489D3FFD77AC5FFC06AAEFFC06AADFFBF69ADFFBF69ADFFC06AADFFC16B
      AEFFC26BAFFFC16BAFFFC06AADFFBF69ADFFBF69ADFFC06AADFFC06AAEFFD378
      C1FFE489D3FFCC73B5FFAB528CFFB2B2B2FFD4D4D4FF934F12FF9D7C5DFF7461
      4FFF695F56FF615E5DFF615E5DFF636160FF656361FF5E625CFF964E7CFFBB68
      A6FFD07DC1FFC46FB4FFAF619FFFAF619EFFAF609EFFAF609EFFAF619EFFB062
      9FFFB162A0FFB564A4FFB866A6FFB463A3FFAF609FFFAF619EFFAF619FFFC16E
      B0FFD07DC1FFBF6CAAFFA85089FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD5F9CFFA35A93FF9151
      83FF68566DFF217754FF008A49FF008949FF008949FF008A49FF207653FF725D
      77FFAB679FFFB2649EFFA64F87FF00000000C8C8C8FFC57E3DFFEBC29CFFE5A8
      68FFE5A86AFFE5A869FFE5A665FFE7B37DFFEDEFEFFFE5B27BFFE5A665FFE5A8
      6AFFE5A86AFFE5A868FFECC49CFFB76711FFC568B6FFEDA9E1FFD078BBFFE788
      D0FFE687CFFFE684CFFFE796D4FFB8BDB7FFE595D2FFE684CFFFE687CFFFE788
      D0FFD079BCFFEDABE0FFB95698FFB5B5B5FFBBBBBBFF956437FF706D6CFF807E
      7CFF8D8B89FF989694FF9B9997FF9A9996FF999996FF959D94FFB84F95FFE898
      D8FFE481CDFFC16DAEFFB968A6FFE080CBFFE380CCFFE380CCFFE481CDFFC16D
      AEFFB1649EFFBE6CAAFFE481CEFFE380CCFFE380CCFFE481CDFFC46FB0FFBB69
      A9FFE481CDFFE799D8FFB25392FFA5A5A5FFAFAFAFFF78512CFF535150FF5E5D
      5BFF686665FF706F6DFF737170FF7D7C7AFF868684FF797F78FF883B6EFFAB70
      9FFFA85F97FF8E5080FF884D7AFFA55E96FFA75E96FFA75E96FFA85F97FF8E50
      80FF834A75FF9A588AFFC972B6FFBA69A7FFA95F98FFA85F97FF915282FF8A4D
      7DFFA96098FFBB7CAEFFA74E89FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A65D95FF9D588DFF8A4E7CFF2A69
      52FF009152FF00B679FF00CD90FF00D194FF00D194FF00CD90FF00B679FF0091
      52FF326D59FF9A668FFF9E4A81FFA4A4A4FFBABABAFFCB8749FFEFD1B4FFECB3
      72FFEBB374FFEBB272FFECB273FFF3F6F7FFEEF3F6FFE8EAEAFFEBB372FFECB2
      73FFEBB374FFECB373FFF0D2B3FFBE6F18FFCC6DBBFFF0BAE8FFED90D6FFEB90
      D5FFEB8ED4FFEC8FD5FFF4F9F4FFF0F7F0FFE9EDE6FFEB8FD6FFEC8FD5FFEC91
      D5FFED90D6FFF0BBE7FFBF58A0FFB7B7B7FFAAAAAAFF77706AFF8F8D8CFFA2A0
      9EFF989694FF959391FF949290FF949290FF92938FFF978990FFC35FA5FFE99C
      D9FFE685D0FFB66AA5FFE385CEFFE586D0FFE385CFFFE385CEFFE586D0FFE384
      CEFFB76BA6FFDF83CAFFE586D0FFE385CEFFE385CEFFE486CFFFE687D1FFBA6D
      A9FFE283CDFFE99DD9FFBF63A3FFA7A7A7FF9B9B9BFFB3801CFFB67E0EFFB47B
      09FFB47A08FFB47B09FFB67E0EFFB2811FFF737370FFB2801FFFB67E0EFFB47B
      09FFB47A08FFB47A08FFB47A08FFB47A08FFB47A08FFB47A08FFB47A08FFB47B
      09FFB67E0EFFB78022FFB56AA4FFB57D2EFFB67E0EFFB47B09FFB47A08FFB47B
      09FFB67E0EFFB88221FFAF5A95FF00000000989898FFA8781AFFA6730DFFA470
      08FFA46F07FFA47008FFA6730DFFA7791DFF6E6E6CFFA7781DFFA6730DFFA470
      08FFA46F07FFA46F07FFA46F07FFA46F07FFA26E07FF956507FF36752FFF0096
      56FF00CB8FFF00CD8FFF00C98AFF00C685FF00C685FF00C98AFF00CD8FFF00CB
      8FFF009656FF387936FF994F82FFA3A3A3FFB8B8B8FFC5772FFFEFD4BAFFF2D1
      AEFFF2BD7DFFF2BF81FFF8F2EAFFF5F7F8FFEFF0EEFFE8EAEAFFE5DFD5FFF2BF
      81FFF3BD7EFFF3D1AEFFF1D4B7FFC47B29FFC458ADFFF1BDE7FFF3B8E5FFF297
      DAFFF29ADAFFF8EDF4FFF5F8F4FFEFF0EDFFE8ECE5FFE5DBDDFFF29ADAFFF398
      DBFFF3B8E5FFF0BFE6FFC65CA7FFCBCBCBFFA8A8A8FF777472FFAFAEACFF9C9A
      98FF9B9997FF9B9997FF9B9997FF9B9997FF999B96FFA18A97FFCA67ADFFEBA4
      DCFFE489CEFFE58ACFFFE98DD2FFE78CD1FFE78CD1FFE78CD1FFE78BD1FFE889
      D1FFE988D2FFE98AD1FFE78BD1FFE78CD1FFE78CD1FFE78CD1FFE88DD2FFE58A
      CFFFE489CEFFEAA5DCFFC66BABFFB9B9B9FF9A9A9AFFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFB47B08FF6A6C68FFB47B08FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB47B08FFA06091FFB47B08FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB67E0EFFB5629CFF00000000909090FF93660BFFBDBDBDFFBCBC
      BCFFBCBCBCFFBCBCBCFFBDBDBDFF926406FF5D5F5BFF926406FFBDBDBDFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBBBBBBFF8DA68FFF009254FF00C9
      8DFF00CA8CFF00C788FF00C380FFFFFFFFFFFFFFFFFF00C380FF00C788FF00CA
      8CFF00C98DFF009251FF735A72FFAEAEAEFFB5B5B5FF4F6D7CFFD18E4BFFF0D9
      C3FFF8E2CEFFFFF0DEFFFFFFFFFFFEFAF7FFFBF6F1FFF4F0EBFFE9EAE7FFE8D9
      C6FFFAE4D0FFF2D9C0FFD68C42FFA68F70FFA681A0FFD767B9FFF2C2E7FFF8CD
      EFFFFEE6F6FFFFFFFCFFFDFAF5FFFBF5F0FFF3F0E9FFE7EBE1FFE8CFDDFFFAD0
      F0FFF1C5E7FFD26CB6FFBD58A0FFDDDDDDFFA5A5A5FF817E7DFFB7B6B5FFA19F
      9DFFA2A09EFFA2A09EFFA2A09EFFA2A09EFFA0A29EFFA7909EFFCE6AB1FFEFAE
      E1FFEC90D5FFEB92D5FFEB92D5FFEB92D5FFEB92D5FFEB91D5FFEB8DD4FFEB9E
      D8FFEDF0EBFFEA9DD7FFEB8DD5FFEB91D5FFEB92D5FFEB92D5FFEB92D5FFEB92
      D5FFEC90D5FFEEAFE1FFCA6EAFFFCACACAFF979797FFB47B09FFFFFFFFFFFFFF
      FFFFFEFEFDFFFFFFFFFFFFFFFFFFC5973DFFC79C46FFC5973DFFFFFFFFFFFFFF
      FFFFFEFEFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFEFDFFFFFF
      FFFFFFFFFFFFC5973DFFC79C46FFC5973DFFFFFFFFFFFFFFFFFFFEFEFDFFFFFF
      FFFFFFFFFFFFB47B09FFB965A0FF000000008A8A8AFFB48017FFB67E0EFFB47B
      09FFB47A08FFB47B09FFB67E0EFFB58019FF9D7B37FFB58019FFB67E0EFFB47B
      09FFB47A08FFB47A08FFB47A08FFB77B08FFC67D0AFF338637FF00B375FF00C8
      8AFF00C586FF00C383FF00BE79FFFFFFFFFFFFFFFFFF00BE79FF00C383FF00C5
      86FF00C88AFF00B374FF247A56FFB9B9B9FF000000000075CEFF568A9AFFD488
      3CFFE9BF97FFFFFFFFFFBCDDEFFF62A4C7FF5496B8FF3E82A8FF88A9BBFFF1EB
      E6FFEAB988FFD38739FFAB8F74FF8F9495FF919791FFAE839FFFD75EB4FFEFA1
      D8FFFFFEFFFFD9F0F2FF6EACCAFF5395B7FF4687A9FFAEC5C5FFF1E8E7FFEE9C
      D5FFDC60B5FFDE98CDFFC45BA6FF00000000A4A4A4FF8F8B8AFFC1BFBEFFA8A6
      A4FFA9A7A5FFA9A7A5FFA9A7A5FFA9A7A5FFA7A9A5FFAE96A4FFD36FB6FFF2BF
      E8FFF095D7FFEF97D8FFEF98D8FFEF98D8FFEF97D8FFEF94D7FFEFA2DCFFF1F5
      F0FFEDF5ECFFE8ECE7FFEEA1D9FFF094D8FFEF97D8FFEF98D8FFEF98D8FFEF97
      D8FFF095D7FFF1C0E8FFD073B5FFD4D4D4FF969696FFB47A07FFFFFFFFFFFDFC
      F8FFFCFBF6FFFDFCF8FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFD
      F9FFFDFCF8FFFCFBF6FFFCFAF5FFFCFAF5FFFCFAF5FFFCFAF5FFFCFAF5FFFCFA
      F6FFFEFDFAFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFAFFFCFAF6FFFCFAF5FFFDFC
      F8FFFFFFFFFFB47A07FFBE69A5FF00000000898989FFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFB47B08FFB1B1B1FFB47B08FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00823FFF00C485FF00C3
      83FF00BE7CFF00BC77FF00B86FFFFFFFFFFFFFFFFFFF00B86FFF00BC77FF00BE
      7CFF00C383FF00C587FF008A4AFFC2C2C2FFBABABAFF0071CBFF2A98CCFF077E
      BAFF8E896DFFEC9948FF99B9C2FF68A1BEFF4F819BFF376984FF556C72FFF09D
      4AFFCA9865FFA39D97FFACAEAFFFEEF0EFFFADAEABFFA3A69EFF9B959DFFA560
      A6FFF36AC4FFBCA3D5FF6CA9C3FF4F829AFF32667DFF9479ABFFF96FC9FF9B58
      A4FFB08DADFFBD61A4FF6D7E80FFE8E8E8FFAAAAAAFF939190FFCBCAC9FFAEAC
      AAFFB0AEACFFB0AEACFFB0AEACFFB0AEACFFAEAFABFFADACA9FFD160B1FFF6CF
      F0FFF2B9E5FFF29ADBFFF29CDCFFF29CDCFFF29ADBFFF29BDBFFF6F2F3FFF3F6
      F1FFEFF0EBFFEAEEE7FFE4E8E0FFF39CDCFFF39BDCFFF29DDCFFF29CDCFFF29A
      DBFFF2B9E5FFF6D1F0FFCE6AB2FFDBDBDBFF9B9B9BFFB47A07FFFFFFFFFFFDFB
      F6FFFEFEFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFEFEFAFFFCFAF4FFFBF8F1FFFBF8F1FFFBF8F1FFFBF8F1FFFBF8
      F1FFFBF8F2FFFCF9F3FFFCF9F3FFFCF9F3FFFBF8F2FFFBF8F1FFFBF8F1FFFCF9
      F3FFFFFFFFFFB47A07FFBC61A3FF000000008E8E8EFFB47B09FFFFFFFFFFFFFF
      FFFFFEFEFDFFFFFFFFFFFFFFFFFFC5973DFFC79C46FFC5973DFFFFFFFFFFFFFF
      FFFFFEFEFDFFFEFEFDFFFEFEFDFFFFFFFFFFFFFFFFFF007E38FF17CB93FF00BD
      7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF00BD7AFF1BCD96FF008949FFC8C8C8FFBFBFBFFF0B73BCFF388BB2FF006E
      A3FF006D9FFF13638BFF70A0BDFF76B3D3FF6CA8C7FF5C98B7FF36617FFF9291
      96FFAFAFAFFFACACAEFFF6F6F6FFF3F3F1FFEDEDE8FFBBB6AFFF0D4173FF0038
      6CFF203F77FF7686B4FF75B4D1FF70AFD1FF5A97B5FF516291FF26447BFF013D
      6FFF093972FF49707CFF007A3DFFE3E3E3FFAFAFAFFF9A9795FFD5D4D3FFB5B3
      B0FFB6B4B2FFB7B5B3FFB7B5B3FFB7B5B3FFB4B3B1FFBFC3BCFFDC89C4FFDC82
      C3FFF7D9F2FFF6CAECFFF6ADE3FFF69FDFFFF7A1DFFFFBF2F6FFFCFDF8FFFBF8
      F2FFF9F4EEFFF4F1EBFFECEDE4FFE6DADEFFF7A1E0FFF8A0E0FFF6ADE4FFF6CA
      ECFFF8DCF4FFDD8BC7FF9C6491FFDFDFDFFFA0A0A0FFB47A08FFFFFFFFFFFDFD
      F8FFE7CFBBFFAB5812FF9E4000FF9E4100FF9E4100FF9E4100FF9E4100FF9E40
      00FFAB5812FFE7CFBBFFFDFCF6FFFBF8EFFFFBF7EEFFFBF7EEFFFBF7EDFFFBF7
      EDFFFBF7EEFFFBF7EDFFFBF7EDFFFBF7EEFFFBF7EEFFFBF7EEFFFBF7EDFFFBF7
      EFFFFFFFFFFFB47A08FF8F5B84FF00000000929292FFB47A07FFFFFFFFFFFDFC
      F8FFFCFBF6FFFDFCF8FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFD
      F9FFFDFCF8FFFCFBF6FFFCFAF5FFFFFCF8FFFFFFFFFF007D36FF35D19FFF00BB
      76FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF00BB77FF39D3A2FF008948FFCDCDCDFFC2C2C2FF0B6AADFF1077C7FF0079
      DBFF007AE0FF0757A0FF8BCDEEFF84C5E5FF79B9D9FF72B0D2FF61A1C5FF6279
      96FFC7C1BCFFF4F4F2FFF8F7F6FFF1F0EDFFEFEDE9FFCFD0D1FF00326AFF1342
      76FF174274FF5286AEFF81C2E3FF75B4D5FF6DADCDFF467CA2FF1D4879FF1946
      79FF113A77FF1B8C5CFF007039FFDFDFDFFFB7B7B7FF908D8BFFE0E0E0FFD0CF
      CEFFBCB9B7FFBDBBB8FFBDBBB9FFBCBAB8FFBCBAB8FFF4F7F4FFF4F4F1FFDC85
      C1FFDA6CBBFFF3C0E6FFFCE6F8FFFBD2F0FFFFF7FDFFF8FFFBFFBFDBE6FF88B7
      CFFF679FBCFF70A1B9FF9EBBC6FFDAE2DBFFE2D8D9FFFBD4F0FFFDE7F9FFF3C4
      E7FFDB77BFFF946D94FF006E38FF00000000A7A7A7FFB47A08FFFFFFFFFFFDFF
      FBFFAD5609FFD38E4FFFE1A368FFE0A369FFE0A368FFE0A368FFE0A369FFE1A3
      68FFD38F4FFFAD570BFFFDFFFAFFFBF8EEFFFDF9EEFFFDF9EFFFFCF8EDFFFBF7
      EDFFFDF9EEFFFCF8EEFFFBF7EDFFFDF9EFFFFDF9EFFFFDF9EEFFFBF7EDFFFAF6
      EDFFFFFFFFFFB47A08FF006533FF00000000999999FFB47A07FFFFFFFFFFFDFB
      F6FFFEFEFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFEFEFAFFFCFAF4FFFFFAF4FFFFFFFFFF007C33FF68D9B5FF00BB
      78FF00B974FF00B771FF00B268FFFFFFFFFFFFFFFFFF00B268FF00B771FF00B9
      74FF00BB78FF6EDCB9FF008947FFD2D2D2FFCDCDCDFF0075D0FF118FD3FF0B91
      D4FF0778B9FF42709BFF9ADEFBFF89CAEAFF7EBEDEFF76B6D5FF6FAFD0FF1F51
      81FFB9C1CBFFFFFFFFFFFFFBF7FFFBF6F1FFF8F2ECFFFFF8ECFF01346CFF1743
      76FF0F3D71FF7EBDDFFF87C9E9FF7CBADBFF74B2D2FF63A1C3FF163C6DFF1E47
      7BFF183A78FF14C58EFF008C49FFE4E4E4FFCACACAFF6E8799FFB1AFAEFFE9EA
      E9FFDEDDDDFFCBCAC8FFC1BEBCFFC4C1BFFFF8F6F3FFFFFEFAFFFDFCF5FFFBFE
      F2FFF1CADDFFE671BFFFE870C2FFF7AFE0FFFFFFFFFFE5F8F7FF70B3D0FF6AA7
      C7FF5B96B4FF47809DFF2A6686FFB9C8C4FFE4EEDCFFF1BADDFFEB73C5FFD761
      B5FF4A4283FF14C48DFF0000000000000000B9B9B9FFB47A08FFFFFFFFFFFCFE
      F7FFBA681EFFE6B789FFDF9E5CFFDF9C59FFDF9A55FFDF9A55FFDF9C59FFDF9E
      5DFFE7B88AFFB1580AFFFDFFF8FFFCF8ECFF808285FFB5B4B0FFB3B1AEFFFDF8
      EBFF808285FFB3B2AFFFFEF8ECFF818285FFB6B4B0FFB4B3B0FFB2B0ADFFFAF5
      E9FFFFFFFFFFB47A08FF008043FF00000000A9A9A9FFB47A08FFFFFFFFFFFDFD
      F8FFE7CFBBFFAB5812FF9E4000FF9E4100FF9E4100FF9E4100FF9E4100FF9E40
      00FFAB5812FFE7CFBBFFFDFCF6FFFDF8F0FFFFFEF8FF3CA673FF57C298FF28C8
      94FF00BA78FF00B978FF00B46EFFFFFFFFFFFFFFFFFF00B46EFF00B978FF00BA
      78FF2AC895FF63C7A0FF008345FFDFDFDFFFD3D3D3FF0074D0FF2DA2DAFF0090
      D8FF05538EFF467EA5FFA3E8FFFF92D3F3FF86C6E7FF7DBCDDFF78B8D8FF2449
      72FF6985A6FFCBE6F1FF65A6C7FF5496B8FF4184A9FF9BB4BEFF3C618BFF103D
      71FF316B98FFA3E7FFFF8FD1F0FF83C2E3FF7AB8D9FF76B6D6FF294B72FF1A41
      78FF16577DFF37D7ABFF008B47FFE9E9E9FF000000000072CDFF7A939FFFA5A1
      A1FFDADADBFFF3F3F3FFE6E5E5FFFEFBFBFFECF5F8FFA2C8DBFF86B6CDFF73A9
      C1FF79ACC1FF3E7695FF444584FFA559A8FFF462C3FFDE96D5FF79BAD4FF6498
      B3FF51819AFF417189FF2A6076FFCC81BFFFF967C8FFA75AAAFF364681FF0D35
      62FF155275FF000000000000000000000000C1C1C1FFB47A08FFFFFFFFFFFAFA
      F1FFC67B37FFEDC9A7FFEAAD6BFFEAAF6FFFF6FAFEFFECF0F1FFEAB06FFFEBAE
      6CFFEECBAAFFBB6516FFFBFCF3FFF9F5E7FFFCF6E7FFFCF6E7FFFBF5E6FFFAF4
      E5FFFCF5E7FFFBF5E6FFFBF5E6FFFCF6E7FFFCF6E7FFFCF5E7FFFAF3E5FFF8F2
      E4FFFFFFFFFFB47A08FF007F41FF00000000B0B0B0FFB47A08FFFFFFFFFFFDFF
      FBFFAD5609FFD38E4FFFE1A368FFE0A369FFE0A368FFE0A368FFE0A369FFE1A3
      68FFD38F4FFFAD570BFFFDFFFAFFFCF9EFFFFFFDF4FFC8E2CCFF008C49FF88E0
      C2FF1CC38AFF00B774FF00B36DFFFFFFFFFFFFFFFFFF00B36DFF00B774FF1CC4
      8BFF8AE2C6FF069458FF00783EFFE8E8E8FFD5D5D5FF0C81D3FF46AFE1FF009A
      E1FF0F4476FF4D89B1FF88C9E9FF8DCEEEFF93D6F5FF87C9EAFF82C4E4FF2548
      6EFF133A6CFF9AC2D5FF6BA2BEFF4F819AFF3A6A83FF557384FF7D8495FF0C3B
      6FFF4D8CB5FF86C8E8FF8ACBEBFF90D2F2FF84C5E5FF7EC0E0FF294970FF1435
      6DFF078F84FF55DEB9FF008946FFE9E9E9FF000000000000000046AEE0FF2A96
      C7FF939291FFA3A19FFFD0CECCFFFFFFFFFF9CCBE1FF71ADCDFF69A5C4FF5C97
      B5FF4F88A3FF154A78FF043C6FFF063F71FF174476FF625391FF79B5CEFF70AC
      CAFF71AFD1FF5D98B7FF396C84FF534E86FF1C467AFF094172FF084072FF093C
      6BFF07857BFF000000000000000000000000C3C3C3FFB47A08FFFFFFFFFFF8F6
      EAFFCF8A47FFF3DCC8FFF6BF7CFFFFF1DFFFFFFEFEFFF5F4F3FFECDDC8FFF8C1
      7FFFF4DECBFFC57020FFF9F8ECFFF8F2E2FFFBF3E2FFFBF4E3FFFAF3E2FFFAF2
      E1FFF9F2E1FFFBF3E2FFFBF4E2FFFBF3E2FFFBF3E2FFFBF3E2FFF9F1E1FFF7F0
      DFFFFFFFFFFFB47A08FF007D40FF00000000B2B2B2FFB47A08FFFFFFFFFFFCFE
      F7FFBA681EFFE6B789FFDF9E5CFFDF9C59FFDF9A55FFDF9A55FFDF9C59FFDF9E
      5DFFE7B88AFFB1580AFFFDFFF8FFFCF8ECFF848387FFC6B9BAFF349567FF0193
      53FF91E1C5FF5BD4AAFF0ABC7EFF00B068FF00B068FF0ABC7EFF5BD4AAFF91E1
      C6FF04975CFF538630FF00723AFF00000000D8D8D8FF1185D5FF5DBDE8FF00A3
      EAFF133C6BFF205786FF1C4F7CFF194571FF305F86FF72AFD0FF7FBFDFFF1630
      5AFF0B3468FF75A7C4FF78B4D3FF6DA8C7FF5E98B7FF3D6584FF15A88AFF103F
      74FF205988FF1C4F7CFF194671FF2F5E85FF6FACCDFF7CBBDBFF19315CFF1634
      6EFF00CD8EFF74E6C7FF008946FFECECECFF0000000000000000000000000000
      0000133C6BFF376287FF7B838DFFA89E97FF93BED2FF74ADCCFF5F91ACFF5281
      9AFF49778FFF0C3C6FFF124175FF1B487AFF1D4979FF2A5382FF85C7E6FF7AB9
      DAFF73B1D3FF6EABCDFF65A4C4FF274F7BFF224C7CFF1C497BFF164678FF0F3C
      6EFF00BF85FF000000000000000000000000C5C5C5FFB47B08FFFFFFFFFFF6F1
      E2FFD79452FFE8BD94FFFFEAD3FFB3D9EFFF5DA1C5FF3C7FA4FF799CB0FFFFEE
      D6FFE8BE95FFD79554FFF7F3E3FFF9F1DFFF808287FFB4B2ADFFB3B0ACFFB1AF
      ABFFFAF2DFFF828489FFB4B2ADFFB3B1ACFFB3B1ACFFB2B0ACFFB0AEAAFFF7EE
      DCFFFFFFFFFFB47B08FF007D40FF00000000B4B4B4FFB47A08FFFFFFFFFFFAFA
      F1FFC67B37FFEDC9A7FFEAAD6BFFEAAF6FFFF6FAFEFFECF0F1FFEAB06FFFEBAE
      6CFFEECBAAFFBB6516FFFBFCF3FFF9F5E7FFFDF6E7FFFFF8EAFFFFFBF1FF60B4
      87FF008B48FF53C095FF90E0C4FF9BE7CDFF9BE7CDFF90E0C4FF53BF95FF008B
      49FF6BC5A7FFCC7F0EFF00723AFF00000000E1E1E1FF0072CFFF67C1ECFF48C5
      F6FF123B6CFF195580FF306791FF47739CFF52749CFF2D4E78FF23436CFF1933
      5FFF0C3467FF8ED2F2FF84C5E5FF79B9D9FF73B0D2FF69A2C6FF1A928CFF1240
      75FF1A5682FF316791FF47739CFF52749CFF2D4F78FF24436CFF1B3360FF1433
      6EFF57EEBFFF7FE4C6FF008947FFF6F6F6FF0000000000000000000000000000
      00000000000019557FFF2D6087FF1D4774FF5984A4FF76AFCBFF72ADCCFF6DA7
      C7FF69A5C2FF1E4F7DFF174477FF224C7CFF234978FF69A1C7FF89CAEAFF7DBE
      DEFF77B5D6FF70AED0FF6DAACAFF5285ACFF2B4F7FFF254E7EFF1D487AFF133E
      70FF53E2B5FF000000000000000000000000CECECEFFB47B08FFFFFFFFFFF2EC
      D8FFFFFDFEFFE8B481FFE9A258FFBAC1B8FF6099B8FF437997FF909187FFECA5
      5CFFE8B581FFFFFEFFFFF2ECD9FFF5EDD8FFF8EFDAFFF8F0DAFFF7EFDAFFF7EE
      D9FFF6EED9FFF8EFDAFFF8EFDAFFF8EFDAFFF8EFDAFFF7EFDAFFF6EDD8FFF3EB
      D6FFFFFFFFFFB47B08FF007D41FF00000000BCBCBCFFB47A08FFFFFFFFFFF8F6
      EAFFCF8A47FFF3DCC8FFF6BF7CFFFFF1DFFFFFFEFEFFF5F4F3FFECDDC8FFF8C1
      7FFFF4DECBFFC57020FFF9F8ECFFF8F2E2FFFBF3E2FFFDF4E3FFFFF5E6FFFFF8
      EBFFC2DBC0FF3DA771FF008238FF008239FF008239FF008138FF3BA670FFC0DA
      BFFFFFFFFFFFBB7D0BFF00723BFF00000000E7E7E7FF6A93B4FF097FD5FF76CF
      F8FF113969FF17537EFF2D658EFF406F99FF4B739CFF436993FF315782FF1834
      61FF083065FF9CE2FFFF8ACBEBFF7EBEDEFF76B6D5FF73B1D2FF325C8AFF1140
      73FF175581FF2D658EFF406F99FF4B739CFF436A93FF325783FF1A3563FF1232
      71FF91F0CCFF0B9758FF8DBFA7FFFDFDFDFF0000000000000000000000000000
      00000000000016517BFF27587BFF1E446FFF69A2C8FF86C8E9FF7DBDDEFF78B6
      D7FF76B5D4FF326693FF194477FF254170FF27517FFF9BE0FDFF8ACCECFF82C2
      E2FF7BBADBFF75B3D4FF6FADCEFF6EADCDFF234672FF1B4678FF204A7BFF133C
      70FF8DE9C6FF000000000000000000000000D3D3D3FFB47B08FFFFFFFFFFF0E8
      D0FFFCF2E9FFFFF7EFFFEDE3DAFF75A7BFFF77B9DDFF69AACEFF5B8AA2FFFEEF
      E3FFFFF7EFFFFCF2EAFFF1E8D2FFF3EAD4FFF6EDD5FFF6EDD5FFF5ECD5FFF7ED
      D5FFF6EDD5FFF6ECD5FFF5ECD5FFF7EDD5FFF7EDD6FFF6EDD5FFF4EBD4FFF2E9
      D2FFFFFFFFFFB47B08FF81AF99FF00000000C1C1C1FFB47B08FFFFFFFFFFF6F1
      E2FFD79452FFE8BD94FFFFEAD3FFB3D9EFFF5DA1C5FF3C7FA4FF799CB0FFFFEE
      D6FFE8BE95FFD79554FFF7F3E3FFF9F1DFFF808287FFB4B2ADFFB3B1ACFFB5B0
      ADFFFFF5E4FF928A92FFC9BAB9FFCBB9B9FFCAB9B9FFC8B8B8FFC1B5B3FFFFF2
      E1FFFFFFFFFFB57B09FF76A08CFF0000000000000000AEAEAEFF9FC8E8FF0078
      D6FF15558FFF154C78FF225D87FF306791FF376892FF33608AFF24507CFF1030
      5FFF3D73A1FFA4EAFFFF92D3F3FF86C6E7FF7DBCDDFF7AB8D9FF2F5278FF103F
      73FF144F7DFF225D88FF306791FF376892FF33608AFF24507DFF143165FF195E
      76FF009146FF75A48DFFE7E7E7FF000000000000000000000000000000000000
      000015558EFF13466FFF19466BFF4C7FAAFF97DBF9FF89CAE9FF80C0E0FF7ABA
      DBFF78B7D7FF5088AFFF133E71FF214370FF5591B9FF9CE1FEFF8FD0F0FF86C7
      E7FF80BFE0FF79B8D9FF74B1D2FF71B0D0FF3C678EFF1A4072FF1A4879FF1751
      6EFF009046FF000000000000000000000000D5D5D5FFB47B08FFFFFFFFFFEFE6
      CDFFF6E1CCFFFFE8D1FF687A93FF91DBFEFF85C6E7FF76B5D7FF64A6CBFF7D8B
      9DFFFFE9D1FFF6E1CDFFF0E7CFFFF4E9D0FF81838AFFB2AFA8FFF6ECD2FF8385
      8CFFB3B0A9FFB1AEA7FFF6EBD2FF83858CFFB3B0A9FFB2B0A9FFB0ADA6FFF2E8
      CEFFFFFFFFFFB47B08FFD3D3D3FF00000000C3C3C3FFB47B08FFFFFFFFFFF2EC
      D8FFFFFDFEFFE8B481FFE9A258FFBAC1B8FF6099B8FF437997FF909187FFECA5
      5CFFE8B581FFFFFEFFFFF2ECD9FFF5EDD8FFF8EFDAFFF8F0DAFFF7EFDAFFF7EF
      D9FFF7EFDAFFFAF0DCFFFBF1DCFFFCF1DCFFFBF1DCFFFBF0DCFFF8EEDAFFF4EC
      D7FFFFFFFFFFB47B08FFC1C1C1FF00000000000000000000000000000000C7CC
      D0FF05579AFF104176FF144F7BFF1D5A84FF215B87FF1E537FFF154472FF0E39
      6CFF4986B0FF88CAEAFF8DCEEEFF93D6F5FF87C9EAFF83C4E4FF2A4C71FF123B
      6EFF0F4671FF144F7CFF1D5A84FF215B87FF1E537FFF154473FF113E6FFF0865
      5DFFDCE2DFFF0000000000000000000000000000000000000000000000000000
      0000055596FF0E3866FF0E396BFF64A1C6FF98DCFBFF8CCDEDFF85C5E5FF7FBE
      DEFF7ABAD9FF75B2D3FF144074FF224D7BFF8ACEF0FFA4E9FFFF97DBF9FF8ED0
      F0FF86C7E7FF7FBEDDFF78B7D7FF75B4D4FF578AACFF1F3C69FF184679FF075C
      55FF00000000000000000000000000000000D5D5D5FFB47B08FFFFFFFFFFEFE5
      CBFFF2D2B4FFFFDCB9FF002E62FFA9F3FFFF96DAF7FF86C7E6FF78BCDDFF0022
      57FFFFDDBBFFF2D2B5FFEFE6CEFFF1E6CDFFF4E9D0FFF4E9CFFFF3E8CEFFF4E9
      CFFFF4E9CFFFF3E8CFFFF3E8CFFFF4E9D0FFF5E9D0FFF4E9CFFFF2E7CDFFF0E4
      CAFFFFFFFFFFB47B08FFD5D5D5FF00000000C3C3C3FFB47B08FFFFFFFFFFF0E8
      D0FFFCF2E9FFFFF7EFFFEDE3DAFF75A7BFFF77B9DDFF69AACEFF5B8AA2FFFEEF
      E3FFFFF7EFFFFCF2EAFFF1E8D2FFF3EAD4FFF6EDD5FFF6EDD5FFF5ECD5FFF7ED
      D5FFF6EDD5FFF6ECD5FFF5ECD5FFF7EDD5FFF7EDD6FFF6EDD5FFF4EBD4FFF2E9
      D2FFFFFFFFFFB47B08FFC3C3C3FF00000000000000000000000000000000D4D4
      D4FF0F3C6AFF305F85FF1D4B7CFF07376BFF093A6EFF093A6DFF174475FF0F44
      74FF205988FF1C4F7CFF194571FF305F86FF72AFD0FF7FBFE0FF18325CFF163F
      71FF0C365CFF1C4C7DFF07376BFF093A6EFF093A6DFF174476FF112F5BFF3B5D
      82FFF7F7F7FF0000000000000000000000000000000000000000000000000000
      00000E3964FF20486DFF0F3E72FF74B3D8FF9CE0FEFF91D3F3FF89CAEAFF83C3
      E3FF7FBEDEFF7EC0DEFF1E4777FF235784FF70B1D5FF5E9AC0FF619CC1FF89C9
      EAFF92D5F4FF87C8E7FF7FBFDEFF7BBCDCFF467296FF2A416EFF154578FF3554
      76FFF6F6F6FF000000000000000000000000D5D5D5FFB47B08FFFFFFFFFFEDE4
      C9FFEDC29AFFFFCFA1FF00316AFF1E5E8FFF5488B2FF7CB3D4FF6399BDFF001E
      57FFFFD1A3FFEDC39CFFEDE5CCFFEFE5CCFFF0E7D0FFF0E7D0FFF0E7CFFFF0E6
      CDFFF0E6CDFFF0E7D0FFF0E7D0FFF0E7CFFFF0E7CFFFF0E7CEFFEFE5CCFFEDE2
      C7FFFFFFFFFFB47B08FFD5D5D5FF00000000C3C3C3FFB47B08FFFFFFFFFFEFE6
      CDFFF6E1CCFFFFE8D1FF687A93FF91DBFEFF85C6E7FF76B5D7FF64A6CBFF7D8B
      9DFFFFE9D1FFF6E1CDFFF0E7CFFFF4E9D0FF81838AFFB2AFA8FFF6ECD2FF8385
      8CFFB3B0A9FFB1AEA7FFF6EBD2FF83858CFFB3B0A9FFB2B0A9FFB0ADA6FFF2E8
      CEFFFFFFFFFFB47B08FFC3C3C3FF00000000000000000000000000000000D7D6
      D4FF113D67FF234B75FF94D6F5FF8DCEECFF81C2E0FF7AB8D7FF6FABCBFF0E3F
      71FF195581FF306791FF47739CFF52749CFF2D4E78FF23436CFF1B3561FF153E
      6FFF193E69FF94D6F5FF8DCEECFF81C2E0FF79B8D7FF6DAACAFF1E406EFFB3B8
      BDFFF8F8F8FF0000000000000000000000000000000000000000000000000000
      000010385FFF143E6AFF144A79FF8ACDEEFFA4EAFFFF99DDFBFF90D3F2FF89CA
      EAFF83C4E4FF84C4E4FF0E3568FF26608CFF1F5784FF225583FF21517EFF1742
      6EFF2F5D84FF70ACCDFF8BCFEDFF7FC0DFFF224067FF2F4673FF184779FFA1A6
      AAFFF6F6F6FF000000000000000000000000D5D5D5FFB47B08FFFFFFFFFFECE3
      C6FFE7B381FFF9BF88FF003973FF2B7BAFFF4E8EBDFF507FACFF274877FF0022
      5EFFF8C18AFFE7B482FFEDE5CAFFEEE5CBFFAD6E00FFD0AC65FFCFAB62FFCEA9
      60FFEFE6CEFFAD6E00FFD0AC65FFCFAB63FFCFAB62FFCFAB62FFCEA85DFFECE1
      C4FFFFFFFFFFB47B08FFD5D5D5FF00000000C3C3C3FFB47B08FFFFFFFFFFEFE5
      CBFFF2D2B4FFFFDCB9FF002E62FFA9F3FFFF96D9F7FF85C7E6FF78BCDDFF0022
      57FFFFDDBBFFF2D2B5FFEFE6CEFFF1E6CDFFF4E9D0FFF4E9CFFFF3E8CEFFF4E9
      CFFFF4E9CFFFF3E8CFFFF3E8CFFFF4E9D0FFF5E9D0FFF4E9CFFFF2E7CDFFF0E4
      CAFFFFFFFFFFB47B08FFC3C3C3FF00000000000000000000000000000000D3D2
      D1FF103862FF4276A2FF9BE0FDFF8ACCEBFF80C0E0FF77B7D6FF76B4D3FF0A39
      6CFF15537FFF2D658EFF406F99FF4B739CFF436993FF325883FF1A3663FF113B
      6EFF4176A3FF9BE0FDFF8ACCEBFF80C0E0FF77B7D6FF74B3D3FF325F8BFF9BA3
      ADFFF1F1F1FF0000000000000000000000000000000000000000000000000000
      00000F335AFF154174FF17517FFF72B2D5FF609BC0FF639DC2FF8CCCECFF94D8
      F6FF8CCDEEFF7AB8DAFF0E3568FF336F9BFF3F7FACFF4988B5FF538DBBFF5D92
      BDFF49749EFF294E79FF2D5077FF25446CFF304772FF314774FF18477AFF7A88
      96FFF0F0F0FF000000000000000000000000D5D5D5FFB47B09FFFFFFFFFFEBE2
      C4FFE1A268FFEEAD6FFF344E6CFF1870A9FF3785B9FF3777A9FF154374FF3448
      68FFEEAE70FFE1A369FFECE4C7FFECE1C4FFEEE3C7FFEEE3C7FFEDE2C6FFEDE1
      C5FFEDE1C5FFEEE3C8FFEEE3C7FFEDE2C7FFEDE2C6FFEDE2C6FFECE0C3FFEBDE
      BEFFFFFFFFFFB47B09FFD5D5D5FF00000000C3C3C3FFB47B08FFFFFFFFFFEDE4
      C9FFEDC29AFFFFCFA1FF00316AFF1E5E8FFF5488B2FF7CB3D4FF6399BDFF001E
      57FFFFD1A3FFEDC39CFFEDE5CCFFEFE5CCFFF0E7D0FFF0E7D0FFF0E7CFFFF0E6
      CDFFF0E6CDFFF0E7D0FFF0E7D0FFF0E7CFFFF0E7CFFFF0E7CEFFEFE5CCFFEDE2
      C7FFFFFFFFFFB47B08FFC3C3C3FF00000000000000000000000000000000D1D0
      CFFF0D3968FF4582ABFFA2E8FFFF92D3F3FF86C6E7FF7DBCDDFF7BB9D9FF2C5F
      8DFF114C7AFF225D87FF306791FF376892FF33608AFF24517CFF133463FF0F3D
      71FF4482ABFFA2E8FFFF92D3F3FF86C6E7FF7DBCDDFF7AB9D9FF2E5278FF4867
      89FFEBEBEBFF0000000000000000000000000000000000000000000000000000
      00000C345FFF164276FF1A5884FF1F5785FF1E5481FF1F4F7CFF16416DFF2F5D
      84FF74B0D0FF96DAF7FF0A366CFF336B96FF3E83B2FF488CBAFF5190BDFF5893
      C0FF5B90BDFF5785B0FF4C719BFF41628CFF37517DFF304774FF174678FF435F
      7FFF00000000000000000000000000000000D5D5D5FFB47B09FFFFFFFFFFEADF
      BDFFD88C46FFE0944CFFBC8250FF214365FF014884FF00417CFF213F61FFBC84
      52FFE0944CFFD88D46FFEBE0C1FFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADC
      BBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADBB9FFE9DA
      B7FFFFFFFFFFB47B09FFD5D5D5FF00000000C3C3C3FFB47B08FFFFFFFFFFECE3
      C6FFE7B381FFF9BF88FF003973FF2B7BAFFF4E8EBDFF517FACFF274877FF0022
      5EFFF8C18AFFE7B482FFEDE5CAFFEEE5CBFFAD6E00FFD0AC65FFCFAB62FFCEA9
      60FFEFE6CEFFAD6E00FFD0AC65FFCFAB63FFCFAB62FFCFAB62FFCEA85DFFECE1
      C4FFFFFFFFFFB47B08FFC3C3C3FF00000000000000000000000000000000D5D3
      D2FF104071FF4D8BB4FF88C9EAFF8DCEEEFF93D6F5FF87C9E9FF83C4E4FF2A4F
      76FF0E3E72FF15517EFF1F5C86FF235D88FF205580FF174674FF143F71FF0F3F
      71FF4D8CB4FF88C9EAFF8DCEEEFF93D6F5FF87C9EAFF83C4E4FF294C71FF143C
      6EFFE9E9E9FF0000000000000000000000000000000000000000000000000000
      00000F3A67FF174376FF165B8AFF256E9EFF377CADFF4586B4FF558CB9FF4973
      9FFF294E79FF2E5179FF0F3B6EFF285B89FF3D85B6FF458BB9FF4B8DBBFF508E
      BCFF548EBBFF4E83B0FF47739FFF3E608BFF354E7AFF294675FF144576FF1339
      69FF00000000000000000000000000000000D6D6D6FFB57B09FFFFFFFFFFE7D9
      B4FFE9DCB9FFEBDFBCFFF1E2BBFFF8E4BAFFFCE5B8FFFCE5B9FFF8E5BAFFF1E2
      BCFFEBDFBCFFE9DDBAFFE8DAB5FFE8D8B3FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8
      B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D7B2FFE7D7
      B1FFFFFFFFFFB57B09FFD6D6D6FF00000000C4C4C4FFB47B09FFFFFFFFFFEBE2
      C4FFE1A268FFEEAD6FFF344E6CFF1870A9FF3785B9FF3777A9FF154374FF3448
      68FFEEAE70FFE1A369FFECE4C7FFECE1C4FFEEE3C7FFEEE3C7FFEDE2C6FFEDE1
      C5FFEDE1C5FFEEE3C8FFEEE3C7FFEDE2C7FFEDE2C6FFEDE2C6FFECE0C3FFEBDE
      BEFFFFFFFFFFB47B09FFC4C4C4FF00000000000000000000000000000000D7D6
      D5FF134676FF205988FF1C4F7CFF194571FF305F86FF72AFD0FF7FC0DFFF1832
      5CFF163E70FF114070FF134477FF144678FF134476FF134173FF2A5478FF1447
      77FF205A88FF1C4F7CFF194571FF305F86FF72AFD0FF7FBFE0FF18325CFF1741
      73FFE9E9E9FF0000000000000000000000000000000000000000000000000000
      000012416DFF164175FF185B8AFF2774A6FF3982B2FF4489B8FF528FBDFF5B91
      BCFF5483AFFF446A95FF204979FF184879FF387FAEFF4188B8FF458AB9FF488B
      BAFF4A8AB8FF477FACFF41719DFF395E8AFF314B76FF1C4677FF133865FF1740
      72FF00000000000000000000000000000000DDDDDDFFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB67E0EFFDDDDDDFF00000000CACACAFFB47B09FFFFFFFFFFEADF
      BDFFD88C46FFE0944CFFBC8250FF214365FF014884FF00417CFF213F61FFBC84
      52FFE0944CFFD88D46FFEBE0C1FFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADC
      BBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADCBBFFEADBB9FFE9DA
      B7FFFFFFFFFFB47B09FFCACACAFF00000000000000000000000000000000D5D4
      D3FF154678FF1A5782FF306791FF47739CFF52749CFF2D4E78FF23436CFF1B35
      61FF194272FF11426AFF000000000000000000000000000000004B6A8FFF1546
      78FF1A5782FF306791FF47739CFF52749CFF2D4E78FF23436CFF1B3561FF1842
      73FFEAEAEAFF0000000000000000000000000000000000000000000000000000
      0000144373FF144174FF16507FFF2476AAFF337FB1FF3F85B5FF4789B8FF4D89
      B8FF487EABFF3D6C99FF2C517CFF0F3E71FF224F7FFF3B82B2FF408ABAFF438B
      BAFF4286B5FF3F7AA8FF3A6C99FF335B86FF264574FF124276FF1A345EFF0000
      000000000000000000000000000000000000E5E5E5FFB38F44FFB67E0EFFB57B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB57B
      09FFB67E0EFFB38F44FFE5E5E5FF00000000D1D1D1FFB57B09FFFFFFFFFFE7D9
      B4FFE9DCB9FFEBDFBCFFF1E2BBFFF8E4BAFFFCE5B8FFFCE5B9FFF8E5BAFFF1E2
      BCFFEBDFBCFFE9DDBAFFE8DAB5FFE8D8B3FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8
      B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D8B2FFE7D7B2FFE7D7
      B1FFFFFFFFFFB57B09FFD2D2D2FF00000000000000000000000000000000D7D6
      D4FF144476FF175581FF2D658EFF406F99FF4B739CFF436993FF325883FF1B37
      64FF174174FF103A68FF00000000000000000000000000000000476C93FF1544
      76FF175581FF2D658EFF406F99FF4B739CFF436993FF325883FF1B3764FF1641
      74FFF0F0F0FF0000000000000000000000000000000000000000000000000000
      0000144375FF134775FF164475FF1C6D9FFF2A7CAFFF3680B1FF3C85B5FF3F83
      B3FF3B78A7FF326694FF25507CFF153665FF104074FF194879FF2C6593FF3877
      A4FF3974A1FF356D98FF2A5C8AFF1C4777FF114275FF2D527EFF1B3764FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DADADAFFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB67E0EFFDDDDDDFF00000000000000000000000000000000DBDA
      D9FF406288FF154F7DFF225D88FF306791FF376892FF33608AFF24517CFF1435
      64FF1A4F80FF2B6C9AFF0000000000000000000000000000000036668FFF1945
      74FF154F7DFF225D88FF306791FF376892FF33608AFF24517CFF143564FF4265
      8AFFF9F9F9FF0000000000000000000000000000000000000000000000000000
      000000000000144D7AFF134073FF154978FF2072A4FF2B7DAFFF317FB2FF327C
      ADFF2E6F9EFF255E8CFF1A4874FF143765FF144376FF1D4E7CFF114073FF113E
      72FF123F73FF124073FF114174FF1F4E7DFF325F88FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E5E5E5FFB78521FFB67E0EFFB57B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B
      09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB47B09FFB57B
      09FFB67E0EFFB38F44FFE6E6E6FF00000000000000000000000000000000FEFE
      FEFFD8DEE5FF124275FF16527EFF1F5C86FF235D88FF205580FF174674FF133D
      70FF1C5E8DFF206390FF00000000000000000000000000000000235D87FF1D4F
      7AFF124275FF16527EFF1F5C86FF235D88FF205580FF174674FF133D70FFD8DE
      E5FFFEFEFEFF0000000000000000000000000000000000000000000000000000
      000000000000124275FF154D7AFF134073FF144575FF195989FF1F6796FF2065
      92FF1E5C89FF184F7DFF164172FF134377FF144D7AFF1F5C85FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000AFAFAFFF989EA5FF2C517BFF134477FF144678FF134476FF124073FF113E
      71FF134173FF0000000000000000000000000000000000000000000000001344
      76FF274F79FF284E79FF134477FF144678FF134476FF2C537DFF989EA5FFAFAF
      AFFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000134376FF114173FF124073FF134073FF1441
      74FF144174FF124174FF124072FF164172FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFF1F1F1FFF5F5F5FFECEC
      ECFFDEDEDEFFDBDBDBFFE4E4E4FFEAEAEAFFEAEAEAFFE8E8E8FFEDEDEDFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFFEFEFEFFF9F9F9FFF0F0F0FFEAEAEAFFE9E9E9FFE9E9E9FFE5E5
      E5FFDDDDDDFFD6D6D6FFD5D5D5FFD5D5D5FFD6D6D6FFDCDCDCFFE1E1E1FFDEDE
      DEFFD9D9D9FFDADADAFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFFBFBFBFFF4F4F4FFEDEDEDFFEAEAEAFFE9E9E9FFDDDD
      DDFFC2C2C2FFAFAFAFFFACACACFFACACACFFAFAFAFFFC2C2C2FFDADADAFFD6D6
      D6FFBEBEBEFFC0C0C0FFDDDDDDFFEEEEEEFFDFDFDFFF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFADADADFFB7B7B7FFBFBFBFFFAAAA
      AAFF8E8E8EFF8E8E8EFFA3A3A3FFB2B2B2FFB2B2B2FFAFAFAFFFBDBDBDFFE0E0
      E0FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFABABABFFA9A9A9FFACACACFFAFAFAFFF9B9B
      9BFF828282FF828282FF959595FFA3A3A3FFA3A3A3FFA0A0A0FFADADADFFCDCD
      CDFFE4E4E4FFF0F0F0FFF9F9F9FFFEFEFEFF000000FF000000FF000000FF0000
      00FF000000FFF8F8F8FFDFDFDFFFC2C2C2FFB2B2B2FFADADADFFACACACFFA3A3
      A3FF8F8F8FFF818181FF7F7F7FFF7E7E7EFF7D7D7DFF7F7F7FFF838383FF7C7C
      7CFF747474FF7E7E7EFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFECECECFFE3E3E3FFD2D2D2FFC0C0C0FFB4B4B4FFAEAEAEFFACACACFFA1A1
      A1FF2B7251FF006536FF006434FF006434FF006535FF2A704FFF969696FF9090
      90FF296F4EFF006A3AFFB0B0B0FFBBBBBBFFAFAFAFFF008547FF008849FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFE8E8E8FFD9D9D9FFC3C3C3FFB0B0B0FFA4A4A4FF9F9F9FFF9D9D9DFF9393
      93FF27684AFF005C31FF005B30FF005B30FF005A2FFF225B40FF587467FF1A7F
      50FF008A49FF008949FF008949FF008A49FF208556FF006A39FF00703CFFB4B4
      B4FFE0E0E0FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF00592FFF00502AFF1B4933FF42574DFF135E
      3BFF006636FF006536FF006536FF006636FF18623FFF004E2AFF00532CFF8585
      85FFA8A8A8FFC6C6C6FFE3E3E3FFF8F8F8FF000000FF000000FF000000FF0000
      00FF000000FFCBCBCBFF9C9085FF9A5923FFB3631DFFB1601AFFB1601AFFB160
      1AFFB1601AFFB1601AFFB1601AFFB1601AFFB1601AFFB1601AFFB3631DFF7F4B
      0FFF174E25FF00582FFF005C31FF005D31FF165A3AFF004726FF004C28FF7A7A
      7AFF9A9A9AFFBABABAFFDEDEDEFFF7F7F7FF000000FF000000FF000000FF0000
      00FFC0C0C0FFA8A8A8FF918C88FF97663CFF9C5A1FFFA44F07FFA44E06FFA44E
      06FFA44E06FFA44E06FFA44E06FFA44E06FFA44E06FFA44E06FFA44E06FFA44E
      06FFA44E06FFA44F07FF9C5A1FFF946339FF096531FF15CD9AFF17E3ABFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFE5E5E5FFCBCB
      CBFFADADADFF8A8A8AFF706C69FF714C2DFF734217FF793A05FF793A04FF793A
      04FF793A04FF793A04FF793A04FF793A04FF773904FF30682FFF009253FF00B6
      79FF00CD90FF00D194FF00D194FF00CD90FF00B679FF009152FF07945EFF006E
      38FFB7B7B7FFE2E2E2FF000000FF000000FFFDFDFDFFF1F1F1FFD8D8D8FFBABA
      BAFF9E9E9EFF7E7E7EFF666360FF674529FF693C15FF6F3505FF6F3504FF6F35
      04FF6F3504FF6F3504FF723704FF6F3504FF693215FF854C6CFFB55B98FFB35A
      96FFB35996FFB25996FFB35A96FFB45A97FFB35A96FFB25996FFB35996FFB35A
      96FFB55B98FFA3618BFFC0B3BBFFEEEEEEFF000000FF000000FF000000FF0000
      00FF000000FF737373FFAC5911FFDA9A60FFE1A468FFE0A165FFE0A165FFE0A1
      65FFE0A165FFE0A165FFE0A165FFE0A165FFE0A165FFE0A165FFE1A468FFDA9B
      5FFFAC580BFF80406CFF84426FFF85426FFF84426FFF83426FFF84426FFF8442
      6FFF884472FF824D6FFFAB9FA6FFE7E7E7FF000000FF000000FF000000FF0000
      00FF9B5D25FF864107FFAB550EFFBE722FFFD0894AFFDD9D5FFFE0A264FFE0A1
      63FFE0A163FFE0A163FFE0A163FFE0A163FFE0A163FFE0A163FFE0A163FFE0A1
      63FFE0A264FFDD9D5FFFD0894AFFBE722FFFAB550EFF159E6EFF00C993FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFBCB8B4FF966B
      46FF79471BFF874308FF9A4C08FFA44F07FFA44E06FFA44E06FFA44E06FFA44E
      06FFA44E06FFA44E06FFA74D06FFB64B03FF4C702CFF009858FF00CC8FFF00CD
      8FFF00C98AFF00C685FF00C685FF00C98AFF00CD8FFF00CB8FFF009656FF1397
      66FF00753BFFBDBDBDFF000000FF000000FFF2F2F2FFC8C8C8FF96928FFF7050
      34FF593414FF643106FF723806FF793A05FF793A04FF793A04FF793A04FF793A
      04FF793A04FF7B3A04FF863E05FF953D02FFB95086FFDE81C8FFCA76BAFFBE6E
      AEFFCA75BAFFD87CC6FFBF6EAFFFBE6DAEFFBF6EAFFFD87CC6FFCA75BAFFBE6E
      AEFFCA76BAFFDC81C8FFAD538DFFE4E4E4FF000000FF000000FF000000FF0000
      00FF000000FF5B2D05FFB25F15FFE7B687FFE09E5CFFDF9E5DFFDF9E5DFFDF9D
      5CFFDF9B59FFDF9A56FFDF9B59FFDF9D5CFFDF9E5DFFDF9E5DFFE09E5CFFE7B7
      85FFB2610AFFB258A3FFB35A99FFB45A97FFB35A96FFB25996FFB35996FFB35A
      96FFB55B98FFA25087FF8D4372FFD5D5D5FF000000FF000000FF000000FF0000
      00FFBE7D44FFAE5F1AFFCD894AFFE3A76FFFDE9E5FFFDD9A59FFDC9857FFDC98
      57FFDC9857FFDC9857FFDC9857FFDC9857FFDC9857FFDC9857FFDC9857FFDC98
      57FFDC9857FFDD9A59FFDE9E5FFFE3A76FFFCD894AFF9E631EFF00BD8DFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFA0500DFF9759
      22FFAB550EFFBE722FFFD0894AFFDD9D5FFFE0A264FFE0A163FFE0A163FFE0A1
      63FFE0A163FFE1A163FFE9A263FFB49B5CFF009454FF00CA8EFF00CA8CFF00C7
      88FF00C380FFFFFFFFFFFFFFFFFF00C380FF00C788FF00CA8CFF00C98DFF0092
      51FF3CB18FFF007F40FF000000FF000000FFDBDBDBFF7E878EFF1C659EFF007A
      D0FF007AD0FF007AD0FF007AD0FF007AD0FF007AD0FF007AD0FF007AD0FF007A
      D0FF007AD0FF007AD0FF2C74ACFF7B6E47FFC5508CFFED9AD9FFB365A1FFE381
      CDFFE17FCCFFE27FCDFFC96EB6FFAF5F9EFFCA6EB6FFE27FCDFFE17FCCFFE381
      CDFFB365A2FFEA9ADAFFB35493FFD9D9D9FF000000FF000000FF000000FF0000
      00FF000000FF006FBEFFC67E3BFFEBC29CFFE5A868FFE5A86AFFE5A869FFE5A6
      65FFE7B37DFFEDEFEFFFE5B27BFFE5A665FFE5A86AFFE5A86AFFE5A868FFECC3
      9BFFB76710FFD97BD3FFBF6EB2FFBE6DAEFFBF6EAFFFD87CC6FFCA75BAFFBE6E
      AEFFCA76BAFFDC81C8FFAD528DFFC6C6C6FF000000FF000000FF000000FF0000
      00FFCB9057FFB36017FFE7B485FFE2A160FFE09F5FFFE09F5FFFE09F5FFFE09F
      5FFFE09F5FFFE09F5FFFE09F5FFFE09F5FFFE09F5FFFE09F5FFFE09F5FFFE09F
      5FFFE09F5FFFE09F5FFFE09F5FFFE2A160FFE6B485FFB26017FF00B88CFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFBA7D44FFAE60
      1BFFCD894AFFE3A76FFFDE9E5FFFDD9A59FFDC9857FFDC9857FFDC9857FFDC98
      57FFDC9857FFDF9857FFED9857FF3B8F4DFF00B374FF00C88AFF00C586FF00C3
      83FF00BE79FFFFFFFFFFFFFFFFFF00BE79FF00C383FF00C586FF00C88AFF00B3
      74FF008F56FF78E0CEFF000000FF000000FFCACACAFF0175CFFF128ED2FF0F8F
      CFFF0E8FD0FF0E8FD0FF0E8FD0FF0D8ED0FF0D8ED0FF0D8ED0FF0E8FD0FF0E8F
      D0FF0E8FD0FF0F8FCFFF0F8FD3FF0474C1FFD968A6FFF0A9DFFFD078BBFFE788
      D0FFE687CFFFE684CFFFE796D4FFB8BDB7FFE595D2FFE684CFFFE687CFFFE788
      D0FFD079BCFFEDABE0FFB95698FFD9D9D9FFC9C9C9FF0172CAFF1186C6FF0E83
      BEFF0D83BEFF0C79B0FFD38843FFF0D1B2FFECB372FFEBB374FFEBB272FFECB2
      73FFF3F6F7FFEEF3F6FFE8EAEAFFEBB372FFECB273FFEBB374FFECB373FFEFD2
      B2FFBC6F16FFE47FDAFFCA6EB9FFAF5F9EFFCA6EB6FFE27FCDFFE17FCCFFE381
      CDFFB365A2FFEA9ADAFFB35493FFC6C6C6FF000000FF000000FF000000FF0000
      00FFCD9157FFBC702AFFE9BC92FFE3A665FFE3A667FFE3A667FFE3A667FFE3A6
      67FFE3A667FFE3A667FFE3A667FFE3A666FFE3A667FFE3A667FFE3A667FFE3A6
      67FFE3A667FFE3A667FFE3A667FFE3A665FFE8B88AFFC27A39FF00B38BFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFD2A47AFFB260
      17FFE6B485FFE2A160FFE09F5FFFE09F5FFFE09F5FFFE09F5FFFE09F5FFFE09F
      5FFFE09F5FFFE39F5FFFF5A060FF008848FF00C587FF00C383FF00BE7CFF00BC
      77FF00B86FFFFFFFFFFFFFFFFFFF00B86FFF00BC77FF00BE7CFF00C383FF00C5
      87FF008A4AFF007D41FF000000FF000000FFC4C4C4FF0074D0FF2DA1D9FF008B
      D2FF008CD3FF008CD3FF008BD2FF0089D2FF0088D2FF0089D2FF008BD3FF008C
      D3FF008CD3FF008BD2FF2BA2DAFF0075D3FFE06EADFFF3BAE6FFED90D6FFEB90
      D5FFEB8ED4FFEC8FD5FFF4F9F4FFF0F7F0FFE9EDE6FFEB8FD6FFEC8FD5FFEC91
      D5FFED90D6FFF0BBE7FFBF58A0FFE6E6E6FFBFBFBFFF0067B9FF2480ADFF0068
      9DFF00679CFF006497FFDA781FFFF6D4B6FFF3D1AEFFF2BD7DFFF2BF81FFF8F2
      EAFFF5F7F8FFEFF0EEFFE8EAEAFFE5DFD5FFF2BF81FFF3BD7EFFF2D1AEFFEED4
      B5FFC57828FFE883DAFFE796D5FFB8BDB7FFE595D2FFE684CFFFE687CFFFE788
      D0FFD079BCFFEDABE0FFB95698FFD2D2D2FF000000FF000000FF000000FF0000
      00FFCF985EFFC68040FFEBC5A1FFE7AC6BFFE7AD6DFFE7AD6EFFE7AD6EFFE7AD
      6EFFE7AD6EFFE7AC6CFFE7AB69FFE7AA68FFE7AB69FFE7AC6CFFE7AD6EFFE7AD
      6EFFE7AD6EFFE7AD6EFFE7AD6EFFE7AD6CFFEAC095FFC68040FF00AF89FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFD5AC85FFBF75
      31FFE9BC92FFE3A665FFE3A667FFE3A667FFE3A667FFE3A667FFE3A667FFE3A6
      67FFE3A667FFE7A667FFFAA768FF008746FF17CD95FF00BD7AFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00BD7AFF1BCD
      96FF008949FF679980FF000000FF000000FFC3C3C3FF0C81D3FF46AEE0FF0093
      D9FF0094D9FF0094D9FF0091D8FF1FA1DDFFF4EFEBFF1E9FDAFF0091D9FF0094
      D9FF0094D9FF0093D9FF44AFE1FF0074D3FFD05A9FFFF3BEE5FFF3B8E5FFF297
      DAFFF29ADAFFF8EDF4FFF5F8F4FFEFF0EDFFE8ECE5FFE5DBDDFFF29ADAFFF398
      DBFFF3B8E5FFF0BFE6FFC65CA7FFF1F1F1FFB6B6B6FF0867ABFF0C75C3FF007A
      D0FF007AD2FF0079D8FF497D99FFE68E3DFFF7D9BEFFFAE3CDFFFFF0DEFFFFFF
      FFFFFEFAF7FFFBF6F1FFF4F0EBFFE9EAE7FFE8DAC6FFFAE4CFFFEFD8BEFFCE8B
      3BFFE0879BFFED8FDCFFF4F9F5FFF0F7F0FFE9EDE6FFEB8FD6FFEC8FD5FFEC91
      D5FFED90D6FFF0BBE7FFBF58A0FFDFDFDFFF000000FF000000FF000000FF0000
      00FFD39E64FFCA8546FFEECEB1FFEBB373FFEBB475FFEBB476FFEBB476FFEBB4
      76FFEBB374FFEBB170FFEBBC86FFEDEEEEFFEABB85FFEBB270FFEBB475FFEBB4
      76FFEBB476FFEBB476FFEBB475FFEBB373FFEEC8A2FFCA8547FF00A988FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFD7B492FFC680
      3FFFEBC5A0FFE7AC6BFFE7AD6DFFE7AD6EFFE7AD6EFFE7AD6EFFE7AD6EFFE7AC
      6CFFE7AB69FFEBAA68FFFEAD6CFF008543FF35D2A1FF00BB76FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00BB77FF39D3
      A2FF008948FF007741FF000000FF000000FFC5C5C5FF1185D5FF5EBCE6FF009B
      E0FF009BE0FF0099DFFF009AE0FFFFF7F2FFFFF4EDFFF2ECE4FF009AE0FF009A
      E0FF009BE0FF009BE0FF5CBDE8FF0073D4FF48867AFFDA6AB8FFF3C4E8FFF9CE
      EFFFFEE6F6FFFFFFFCFFFDFAF5FFFBF5F0FFF3F0E9FFE7EBE1FFE8CFDDFFFAD0
      F0FFF1C5E7FFD26CB6FFB89BB2FFEFEFEFFFB4B4B4FF0076D0FF128ED2FF0F8F
      CFFF0E8FD0FF0A8FD2FF008DD9FF508A9BFFD88638FFF4BF8FFFFFFFFCFFBCDD
      EEFF62A4C7FF5496B8FF3E82A9FF88A9BBFFF0ECE6FFE7BB86FFD28A3AFFE791
      A1FFF49AE3FFF9EEF7FFF5F8F4FFEFF0EDFFE8ECE5FFE5DBDDFFF29ADAFFF398
      DBFFF3B8E5FFF0BFE6FFC65CA7FFE7E7E7FF000000FF000000FF000000FF0000
      00FFD8A56BFFCF8D4FFFF1D8C0FFF0B978FFEFBA7CFFEFBB7EFFEFBB7EFFEFBB
      7DFFEFB979FFEFC28BFFF1F3F5FFEDF2F3FFE8EBEAFFEEBF89FFF0B979FFEFBB
      7DFFEFBB7EFFEFBB7EFFEFBA7CFFF0BA79FFF1D5BDFFCE8D4FFF8BD4CEFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFD9BCA2FFCA85
      46FFEECEB1FFEBB373FFEBB475FFEBB476FFEBB476FFEBB476FFEBB374FFEBB1
      70FFEBBC86FFF0EFEEFFFFBE87FF00823CFF69DAB7FF00BB78FF00B974FF00B7
      71FF00B268FFFFFFFFFFFFFFFFFF00B268FF00B771FF00B974FF00BB78FF6EDC
      B9FF008947FF009871FF000000FF000000FFCDCDCDFF0072CFFF67C0EAFF47BB
      EBFF00A1E7FF00A3E7FFDEEFF6FFFFF8F3FFF4F0ECFFF5ECE4FFCADBE0FF00A3
      E8FF00A2E8FF46BBEBFF61BFEDFF0074D3FF747162FF9A6F88FFD75FB5FFEFA2
      D9FFFFFEFFFFD9F0F2FF6EACCAFF5395B7FF4687A9FFAEC5C5FFF1E8E7FFEE9C
      D5FFDC60B5FF4A8184FF419C70FF000000FFBBBBBBFF0074D0FF2DA1D9FF008B
      D2FF008CD3FF008CD3FF008BD5FF0088D9FF0086C9FF868C75FFF6983FFF9CB9
      C0FF68A1BEFF4F819BFF376A84FF566E73FFF1A24BFFDB7F6BFFEFBBDBFFFACE
      F7FFFFE6F8FFFFFFFCFFFDFAF5FFFBF5F0FFF3F0E9FFE7EBE1FFE8CFDDFFFAD0
      F0FFF1C5E7FFD26CB6FF708183FFE9E9E9FF000000FF000000FF000000FF0000
      00FFE2B075FFCC823BFFF6E4D3FFF2D4B0FFF2C080FFF2C183FFF2C184FFF2C0
      81FFF2C082FFF6F4F0FFF3F5F5FFEFEFEDFFEBECEBFFE5E7E5FFF4C082FFF3C1
      82FFF2C284FFF2C183FFF2C080FFF2D4B0FFF6E4D3FFCE8845FF00803FFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFDDC6B0FFCF8D
      4FFFF1D8C0FFF0B978FFEFBA7CFFEFBB7EFFEFBB7EFFEFBB7DFFEFB978FFEFC2
      8BFFF1F3F5FFF0F2F4FFF9EEEDFF379C61FF57C399FF28C894FF00BA78FF00B9
      78FF00B46EFFFFFFFFFFFFFFFFFF00B46EFF00B978FF00BA78FF2AC895FF63C7
      A0FF0D8D51FF21BEA8FF000000FF000000FFD6D6D6FF7E7E7AFF087DD3FF71C4
      EDFF69CCF3FFB2E7F9FFFFFFFBFFFFFAF5FFFCF6F0FFF8F0E9FFFAECE1FF9DD1
      E0FF69CDF5FF6BC3F0FF007AD9FF5E89A9FFA19B92FF9C9D94FF928C95FFA560
      A4FFF46AC4FFBCA3D5FF6CA9C3FF4F829AFF32667DFF9479ABFFF96FC9FF9758
      A2FF166A49FF1FB19DFF000000FF000000FFC4C4C4FF0C81D3FF46AEE0FF0093
      D9FF0094D9FF0094D9FF0091D8FF1DA1DEFFECEFF0FF0EA0E4FF0578AFFF729E
      B9FF76B3D3FF6CA8C7FF5D99B8FF386481FF696966FF876B7AFFD85EB7FFF0A3
      DBFFFFFEFFFFD9F0F2FF6EACCAFF5395B7FF4687A9FFAEC5C5FFF1E8E7FFEE9C
      D5FFDC60B5FF5D909FFF008945FF000000FF000000FF000000FF000000FF0000
      00FFEEBD7EFFD38F4CFFDDA976FFF8ECE2FFF6DFC8FFF6CE9CFFF7C586FFF7C7
      8AFFFDF7EEFFFDFCFDFFFCF8F4FFF9F4EFFFF5F1ECFFEEEDE9FFE8E2D5FFF8C8
      8CFFF8C687FFF6CE9CFFF6DFC8FFF8ECE2FFDDA976FFD69D67FFFBFBFBFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFE8D7C7FFCC82
      3BFFF6E4D3FFF2D4B0FFF2C080FFF2C183FFF2C184FFF2C081FFF2C082FFF6F8
      F9FFF3F4F5FFF0EFEDFFF4EEEEFFB5D2C1FF008C48FF88E0C2FF1CC48AFF00B7
      74FF00B36DFFFFFFFFFFFFFFFFFF00B36DFF00B774FF1DC48BFF8DE2C6FF0C94
      57FF35A77FFFA6ECEBFF000000FF000000FF000000FFCA813BFF91AFC5FF0070
      CEFF44A6E4FFEAFBFFFFCADFE8FF65A5C5FF5596B7FF4183A7FF96ABB4FFD2E5
      E8FF309FE6FF006DD0FF5E89A8FFA0968EFF9A9590FFA19D95FF0D4172FF0039
      6DFF203F77FF7687B4FF75B4D1FF70AFD1FF5A97B5FF516291FF26447BFF013E
      6FFF02406EFF98D8D7FF000000FF000000FFD0D0D0FF1185D5FF5EBCE6FF009B
      E0FF009BE0FF0099DFFF009AE0FFFFF7F2FFFFF6EFFFF4F2ECFF005F9FFF8ACB
      ECFF84C4E4FF79B9D9FF72B0D2FF64A4C8FF415A74FF77786CFF706B72FF9E5A
      9FFFF46AC5FFBCA3D5FF6CA9C3FF4F829AFF32667DFF9479ABFFF86FC9FF9454
      9DFF0F355EFF8ECAC9FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FFF2CB9AFFD89651FFDA9B5DFFF1DBC3FFFAF4F1FFFAE8D3FFFFFC
      F4FFE9F4FBFFA1C7DCFF87B5CDFF689FBDFF74A3BDFF7FA7BCFFBBCDD5FFE5DF
      D3FFFBE9D5FFFAF5F1FFF1DBC3FFDA9B5DFF8B9751FF499F76FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFDAA674FFD499
      61FFDDA976FFF8ECE2FFF6DFC8FFF6CE9CFFF7C586FFF7C78AFFFDF7EEFFFDFC
      FCFFFCF8F4FFFAF4EFFFF9F1EDFFFFF1EFFF4AAA7AFF039454FF92E2C6FF5BD4
      ABFF0BBC7FFF00B068FF00B068FF0BBC7FFF5DD4ACFF98E4C9FF149A5FFF5DA8
      84FF4AA67CFF000000FF000000FF000000FF000000FF000000FF000000FFC895
      64FF4881ADFF0074D6FF6BB3DAFF6CA2BCFF50819AFF3B6A82FF24648BFF0076
      DDFF3182C0FF9A9EA2FFAAA199FFB3AEAAFFF3F1EEFFA5A6A7FF00346DFF1443
      76FF174274FF5286AEFF81C2E3FF75B4D5FF6DADCDFF467CA2FF1D4879FF1947
      78FF104275FF138C55FF000000FF000000FFE0E0E0FF0072CFFF66BFEAFF47BB
      EBFF00A1E7FF00A3E7FFDEEFF6FFFFF8F3FFFBF5F0FFBBC1C9FF326898FF98DD
      FAFF89CAEAFF7EBEDEFF76B6D6FF71B1D2FF285A89FF858B90FF154876FF003A
      6DFF204077FF7687B4FF75B4D1FF70AFD1FF5A97B5FF516291FF25447BFF013D
      6EFF003D6DFF11804EFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFF9E7D2FFEED9BFFFD5904AFFDB9A58FFEECBA8FFFFFF
      FFFF99CBE5FF70ADCEFF69A5C4FF5C96B5FF4A83A0FF387291FF43728EFFE7EB
      EBFFE9CEB2FFDC9A58FFD58F48FF3A833FFF00DEA2FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFD799
      5CFFD79B60FFDA9B5DFFF1DBC3FFFAF4F1FFFAE8D3FFFFFCF4FFE9F4FBFFA1C7
      DCFF87B5CDFF68A0BDFF75A3BDFF84A8BEFFDCD9E0FF5AB088FF008F4DFF54C2
      9AFF92E2C7FF9DE7CEFF9EE7CEFF95E2C7FF5FC49CFF0A9456FF1D915AFFF1DD
      E5FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFD698
      5CFFCA915AFF7C6C5CFF69A0C2FF79B4D2FF6DA8C7FF5F98B6FF2C6083FF7C8E
      A2FFBFB3A9FFB3ADA7FFB0ACAAFFF7F6F5FFF6F5F2FFFBF7EDFF00326CFF1642
      76FF0F3D71FF7EBDDFFF87C9E9FF7CBADBFF74B2D2FF63A1C3FF163C6EFF1D49
      7BFF184678FFE9D8DFFF000000FF000000FFE7E7E7FF6C95B6FF1184D6FF70C4
      EDFF69CCF3FFB2E7F9FFFFFFFBFFFFFBF6FFFFFDF5FF5A7697FF3B7AA5FFA1E7
      FFFF92D3F3FF86C6E7FF7DBCDDFF79B8D9FF2A5078FF36577DFF0A3D73FF1545
      77FF174274FF5286AEFF81C2E3FF75B4D5FF6DADCDFF467CA2FF1D4879FF1947
      78FF104275FFD5C5CCFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF98CAE4FF82A5B1FFBD9766FFEF98
      3FFF9DBEC5FF73ADCDFF5F91ACFF518099FF44738DFF36657FFF5C7176FFF79F
      47FFA49B53FF299755FF008744FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFD78F4AFFD59450FFD68F48FFDB9A58FFEECBA8FFFFFFFFFF98CBE5FF70AD
      CEFF69A5C4FF5C96B5FF4A83A0FF387291FF567E98FFF7EFF2FFBBB483FF388B
      46FF008845FF008845FF008845FF008946FF009B5DFF00C08AFF00B275FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE8D6
      C3FFBC7D41FF535864FF8ECFEEFF84C5E4FF79B9D9FF72B0D1FF63A1C4FF6A7D
      96FFC6BFB8FFBEBAB7FFF2F2F0FFF8F7F6FFF3F1EEFFFAF6EDFF325984FF0E3B
      70FF316B98FFA3E7FFFF8FD1F0FF83C2E3FF7AB8D9FF76B6D6FF294C72FF1A46
      79FF325884FFBBB2B6FF000000FF000000FF000000FFB1B1B1FF9EC7E7FF006F
      CEFF44A6E4FFEAFBFFFFCADFE8FF66A6C6FF5C9CBCFF0A3D70FF4888B1FF87C9
      E9FF8DCEEEFF93D6F5FF87C9EAFF83C4E4FF284B70FF1B4171FF114175FF1945
      77FF0F3D71FF7EBDDFFF87C9E9FF7CBADBFF74B2D2FF63A1C3FF163C6EFF1D49
      7BFF184678FFAEA5A9FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF73ACCCFF5987A1FF204B
      73FF61849CFF75AFCCFF72ADCDFF6CA7C7FF65A0BFFF4A7E9CFF2D526EFF0D66
      76FF00C091FF00CE9CFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FFD0975EFFE69442FFCB9E67FFEF983EFF9DBEC4FF73AD
      CDFF5F91ABFF51809AFF44738DFF36657FFF5B7276FFFAA149FFB78453FF386D
      6DFF00BF90FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF0F0
      F0FFA5AEB8FF4275A1FF9ADFFCFF89CAEAFF7EBEDEFF76B6D5FF6FAFD0FF2052
      82FFAEB7C1FFF7F3EFFFFFFFFFFFFFFAF6FFFDF7F1FFFFF8EFFF98A7B7FF0738
      6CFF4C8BB4FF86C8E8FF8ACBEBFF90D2F2FF84C5E5FF7EC0E0FF294A70FF133D
      6FFF3A7C7BFFE5DCE0FF000000FF000000FF000000FF000000FF000000FFE2E7
      EBFF3683C3FF0074D6FF6BB3DAFF6DA3BDFF56869EFF0A3C6FFF1E5787FF1C4F
      7CFF194571FF305F86FF72AFD0FF7FBFDFFF17315CFF123C6FFF486C93FF103D
      71FF316B98FFA3E7FFFF8FD1F0FF83C2E3FF7AB8D9FF76B6D6FF294C72FF1A46
      79FF1F4C78FFDCD3D7FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF72AAC6FF6396B2FF2B57
      80FF68A2C9FF86C8E9FF7DBDDEFF77B6D7FF72AFD1FF6EACCDFF4D83A8FF276E
      82FF1EB693FF1CC9A2FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF567291FF5B7B92FF2B5881FF5986A6FF75AF
      CCFF72ADCDFF6CA7C6FF65A0BFFF4A7E9CFF2A506CFF2F5D87FF487A9DFF276E
      81FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE7E7
      E7FF49688AFF4582ABFFA2E8FFFF92D3F3FF86C6E7FF7DBCDDFF78B8D8FF244A
      73FF597799FFFFFFFFFFC1DEECFF63A4C6FF5596B8FF4587ABFF93ADB9FF093E
      71FF1E5887FF1C4F7CFF194671FF2F5E85FF6FACCDFF7CBBDBFF18325CFF1640
      71FF00722DFF000000FF000000FF000000FF000000FF000000FF000000FFE6E6
      E6FF446181FF285D88FF69A0C2FF7AB5D3FF74AFCCFF0B3C70FF195581FF3067
      91FF47739CFF52749CFF2D4E78FF23436CFF193360FF0E396CFFAEBBC8FF0738
      6CFF4C8BB4FF86C8E8FF8ACBEBFF90D2F2FF84C5E5FF7EC0E0FF294A70FF133D
      6FFF0E5B4CFFEADEE3FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF68A1C8FF7BB8D7FF5284A2FF4B7F
      AAFF97DBF9FF89CAE9FF80C0E0FF7AB9DAFF74B2D3FF70ADCEFF6EACCBFF3B6C
      98FF5FA0A1FF8AD8CAFF49E7CFFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF5A728FFF5A8CADFF305D86FF68A2C9FF86C8
      E8FF7DBDDEFF77B6D7FF72AFD1FF6EACCDFF4C81A6FF305D86FF5F95B0FF3969
      94FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE4E4
      E4FF164575FF4D8BB4FF88C9EAFF8DCEEEFF93D6F5FF87C9EAFF82C4E4FF2548
      6FFF0C3669FFB6A99EFF91BBD1FF69A1BDFF4F819AFF3B6B84FF5B7986FF0D40
      74FF195582FF306791FF47739CFF52749CFF2D4F78FF24436CFF1B3561FF1741
      73FF4EA57CFF000000FF000000FF000000FF000000FF000000FF000000FFDFDF
      DFFF133C66FF2E567FFF8ECFEEFF86C6E6FF81C2E0FF08366AFF15527EFF2D64
      8EFF406F99FF4B739CFF436993FF325782FF173462FF08346AFFFFFFFFFF063C
      6FFF1D5887FF1C4F7CFF194671FF2F5E85FF6FACCDFF7CBBDBFF18325CFF1640
      71FF479771FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF93D5F2FF76AEC9FF164275FF64A1
      C6FF98DCFBFF8CCDEDFF85C5E5FF7FBEDEFF78B7D7FF72B1D2FF70AECFFF4C7F
      A4FF12446EFF006929FF007A33FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF79818BFF45759DFF6093AFFF4B7FAAFF97DBF9FF89CA
      E9FF80C0E0FF7AB9DAFF74B2D3FF70ADCEFF6EACCBFF3B6C98FF4B7A99FF4675
      96FF12436DFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE4E4
      E4FF134676FF205988FF1C4F7CFF194571FF305F86FF72AFD0FF7FBFDFFF1731
      5CFF0E396EFF537E97FF70A3C1FF77B3D3FF6DA8C7FF5F99B7FF3F6A86FF0D3D
      71FF165480FF2D658EFF406F99FF4B739CFF436A93FF325883FF1B3764FF1641
      73FFF0F0F0FF000000FF000000FF000000FF000000FF000000FF000000FFD7D7
      D7FF0E3861FF4275A1FF9ADFFCFF8ACCEBFF85C6E4FF2C5E8DFF0F4877FF205C
      87FF306790FF376892FF33608AFF23507CFF0F2F5FFF2C608EFF6BAAC6FF0B3D
      71FF185581FF306791FF47739CFF52749CFF2D4F78FF24436CFF1B3561FF1741
      73FFDCDCDCFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF90D0EDFF5587A9FF0F3E72FF74B3
      D8FF9CE0FEFF91D3F3FF89CAEAFF83C3E3FF7DBCDCFF76B6D5FF73B2D2FF5587
      A9FF0E3466FF889AACFFF0F0F0FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF143A67FF568BABFF174376FF64A1C6FF98DCFBFF8CCD
      EDFF85C5E5FF7FBEDEFF78B7D7FF72B1D2FF70AECFFF4C7FA3FF194778FF4973
      90FF0E3263FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE5E5
      E5FF154678FF1A5782FF306791FF47739CFF52749CFF2D4E78FF23436CFF1A34
      61FF133C6EFF3A6589FF8BCEEFFF84C5E5FF79B9D9FF73B1D2FF6AA7C8FF1642
      76FF134D7BFF225D87FF306791FF376892FF33608AFF24517CFF143564FF496C
      92FFF9F9F9FF000000FF000000FF000000FF000000FF000000FF000000FFD3D3
      D3FF0D3968FF4582ABFFA2E8FFFF92D3F3FF89CAEAFF6DABCEFF0F3D70FF124D
      7AFF1E5B85FF235D89FF1F5480FF134271FF103C6EFF6095B3FF5A8BA2FF0A3A
      6EFF15537FFF2D648EFF406F99FF4B739CFF436A93FF325883FF1B3764FF1641
      73FFEAEAEAFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF90CEEAFF31618BFF144A79FF8ACD
      EEFFA4EAFFFF99DDFBFF90D3F2FF89CAEAFF82C2E2FF7CBADBFF79B7D8FF6095
      B6FF0F2E5DFF415F81FFEBEBEBFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF0E3B6CFF47789DFF0F3E72FF74B3D8FF9CE0FEFF91D3
      F3FF89CAEAFF83C3E3FF7DBCDCFF76B6D5FF73B2D2FF5587A9FF10386AFF3664
      86FF0E2B58FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE9E9
      E9FF144476FF175581FF2D658EFF406F99FF4B739CFF436993FF325883FF1A36
      63FF113A6EFF4176A2FF9ADFFCFF89CAEAFF7EBEDEFF76B6D6FF74B2D2FF2F5C
      88FF0E3F72FF15517EFF1F5C86FF235D88FF205580FF174674FF133D70FFD4D6
      DEFFFEFEFEFF000000FF000000FF000000FF000000FF000000FF000000FFD5D5
      D5FF104071FF4D8BB4FF88C9EAFF8DCFEFFF94D7F6FF8CCEEDFF70AED1FF1B49
      79FF114376FF134678FF114374FF164475FF669BBCFF7EB9D7FF72ADCBFF2758
      87FF104978FF215C87FF306790FF376892FF33608AFF24517CFF143564FF3F63
      8AFFF8F8F8FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF96D6E9FF174577FF17527FFF72B2
      D5FF609BC0FF639DC2FF8CCDECFF94D8F6FF8ACBEAFF82C2E3FF80C0E1FF4F7E
      A0FF0E2857FF19477AFFE7E7E7FF007B2EFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF124470FF2F608AFF144A79FF8ACDEEFFA4EAFFFF99DD
      FBFF90D3F2FF89CAEAFF82C2E2FF7CBADBFF79B7D8FF6094B6FF0F2E5DFF234D
      78FF0D2550FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF1F0
      EEFF32547AFF154F7DFF225D88FF306791FF376892FF33608AFF24517CFF1334
      63FF0F3D71FF4482ABFFA2E8FFFF92D3F3FF86C6E7FF7DBCDDFF7AB9D9FF2F52
      78FF0F3969FF113F70FF134477FF144678FF134476FF39608AFFD6D7DFFFF1EF
      EEFF000000FF000000FF000000FF000000FF000000FF000000FF000000FFDCDB
      D9FF134676FF205988FF1C4F7CFF194571FF305F86FF74B0D1FF81C0E1FF1A33
      5CFF184071FF3D7498FF8ECCE0FF436F94FF8FD1F1FF85C6E6FF7CBCDBFF649E
      C2FF0E3D70FF124D7BFF1E5B85FF235D88FF205580FF174674FF133D70FFCCD1
      D7FFFEFEFEFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF588EAFFF164276FF1A5884FF1F57
      85FF1D5480FF1F4F7CFF16416DFF2F5D85FF73AECFFF8FD2F1FF84C5E5FF2748
      6EFF0F2B59FF1B497BFFC0BDBEFFEFE6EAFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF154A74FF164476FF17527FFF72B2D5FF609CC0FF639D
      C2FF8CCDECFF94D8F6FF8ACBEAFF82C2E3FF80C0E1FF4F7EA0FF0D2857FF1946
      78FF0E2751FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFF3F2
      F1FF878B90FF124275FF16527EFF1F5C86FF235D88FF205580FF174674FF133E
      70FF0F3F71FF4D8CB4FF88C9EAFF8DCEEEFF93D6F5FF87C9EAFF83C4E4FF2A4C
      71FF143C6EFF194370FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFDFDE
      DDFF154678FF1A5782FF306791FF47739CFF52749CFF2D4E78FF23436CFF1B35
      61FF194272FF43799CFF5D90AFFF4176A2FF9ADFFCFF89CBEAFF7FC0DFFF7ABA
      D9FF639EC2FF184576FF114276FF134678FF124476FF385E88FFCED3D9FFEEEC
      EBFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF1B4D76FF184476FF165B8AFF276E
      9FFF377CADFF4686B4FF558CB9FF49759FFF294D78FF2B4E76FF20406AFF1E37
      64FF122E5CFF1C497CFFBEC3CAFFF3F0F0FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF185079FF164276FF1A5884FF1F5785FF1D5380FF1F4F
      7CFF16416DFF2F5D85FF73AECFFF8ED2F0FF84C5E5FF27486EFF0F2B59FF1A49
      7BFF102A54FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF999997FF154073FF134172FF134477FF144678FF134476FF134476FF1B46
      6FFF144777FF205A88FF1C4F7CFF194571FF305F86FF72AFD0FF7FC0E0FF1832
      5CFF174073FF1A4371FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE3E2
      E1FF144476FF175581FF2D658EFF406F99FF4B739CFF436993FF325883FF1B37
      64FF174173FF1C4E76FF103A6AFF4582ABFFA2E8FFFF92D3F3FF86C6E7FF7DBD
      DEFF7CBBDAFF30547AFF466284FFE0DDDDFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF3373A0FF164375FF185B8AFF2774
      A5FF3882B2FF4489B8FF538FBDFF5B91BCFF5482AFFF436A95FF315580FF233E
      6BFF132E5BFF1A487AFFDEDDDCFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF14537EFF184476FF165B8AFF276E9FFF377CADFF4686
      B4FF558CB9FF49759FFF284D78FF335A81FF204069FF1E3763FF122E5CFF1A49
      7BFF112A53FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF407B
      A5FF154678FF1A5782FF306791FF47739CFF52749CFF2D4E78FF23436CFF1B35
      61FF184272FF184270FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFE9E7
      E6FF305277FF154F7DFF225D88FF306791FF376892FF33608AFF24517CFF1435
      64FF144273FF174E74FF114172FF4D8BB4FF88C9EAFF8DCEEEFF93D6F5FF87C9
      EAFF83C4E5FF2A4C71FF143C6EFF8D8D8EFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF377CAAFF154375FF165080FF2576
      AAFF337FB1FF3F85B5FF4789B8FF4D8AB8FF487EACFF3C6C99FF2C537FFF1F3C
      67FF153766FF1C4C7BFFE5E4E3FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF165480FF164375FF185B8AFF2774A5FF3882B2FF4489
      B8FF538FBDFF5B90BCFF5482AEFF436994FF315580FF223E6BFF14315EFF1948
      7AFF13335EFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF4081
      ADFF154476FF175581FF2D658EFF406F99FF4B739CFF436993FF325883FF1B37
      64FF164174FF1A4874FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFEBEA
      E9FF81868DFF124275FF16527EFF1F5C86FF235D88FF205580FF174674FF133D
      70FF134172FF154D75FF134676FF205988FF1C4F7CFF194571FF305F86FF72AF
      D0FF7FBFE0FF18325CFF174173FFDBDAD9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF327EAFFF255B8CFF164476FF1C6D
      9FFF2A7CAFFF3680B1FF3D85B5FF3F83B3FF3B78A7FF326694FF25507DFF1635
      62FF174172FF738BA5FF99999AFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF154D79FF144275FF165080FF2576AAFF337FB1FF3F86
      B5FF4789B8FF4D8AB8FF487EABFF3C6C99FF2C537FFF1D3B67FF153766FF1645
      78FF163E6DFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF3E83
      B1FF1D4E7FFF154F7DFF225D88FF306791FF376892FF33608AFF24517CFF1435
      64FF144071FF7088A1FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFB2B1B0FF184474FF124172FF134477FF144678FF134476FF174B7DFF3777
      A6FF1D4E7FFF134873FF154678FF1A5782FF306791FF47739CFF52749CFF2D4E
      78FF23436CFF1B3561FF184272FFAEADADFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF357CACFF134173FF1649
      7AFF2072A4FF2B7DAFFF317FB2FF327CADFF2E6F9FFF255E8DFF1A4874FF1437
      66FF144477FFE1E0DFFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF164576FF165182FF164677FF1C6D9FFF2C7CAFFF3680
      B1FF3D85B5FF3F83B3FF3B78A7FF326694FF25507DFF163462FF174172FF123B
      6AFF144376FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF367F
      B0FF3578A7FF124275FF16527EFF1F5C86FF235D88FF205580FF174674FF133D
      70FF134072FFE0DFDEFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF113E6EFF154476FF175581FF2D658EFF406F99FF4B739CFF4369
      93FF325883FF1B3764FF164174FFDAD9D8FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF2B7DAEFF2C74A6FF1240
      73FF144575FF1A5989FF1F6796FF206592FF1E5C89FF194F7DFF154172FF1443
      77FFC2C6CDFFE0DFDEFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF144775FF134173FF16497AFF2072A4FF2B7D
      AFFF317FB2FF327CADFF2E6F9EFF255E8CFF1A4874FF143766FF144477FF1341
      74FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF317FB1FF2D72A2FF174678FF134477FF144678FF134476FF124174FF123F
      72FFC1C5CCFF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF2C6F9EFF123F70FF154F7DFF225D88FF306791FF376892FF3360
      8AFF24517CFF143564FF42658BFFDCDBDAFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF2064
      90FF144979FF124073FF134173FF144174FF144174FF194678FF6E8BA9FFFCFC
      FCFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF134073FF134272FF124073FF144575FF1A59
      89FF1F6796FF206392FF1E5C89FF184F7DFF164172FF134377FF607E9EFFFBFB
      FBFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF206391FF1B5582FF124275FF16527EFF1F5C86FF235D88FF2055
      80FF174674FF133D70FFD8DEE5FFFEFEFEFF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF164274FF113E71FF1240
      73FF134173FF144174FF144174FF174476FF4F6C8AFFAEAEAEFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF144174FF164273FF1C4675FF134477FF144678FF1344
      76FF2C537DFF989EA5FFAFAFAFFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFFCFCFCFFF6F6
      F6FFEEEEEEFFEAEAEAFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFEAEAEAFFEEEE
      EEFFF6F6F6FFFCFCFCFF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFBFB
      FBFFF2F2F2FFF0F0F0FFF9F9F9FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBFB
      FBFFF2F2F2FFEAEAEAFFE9E9E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF0000
      000000000000000000000000000000000000F9F9F9FFEEEEEEFFEEEEEEFFF9F9
      F9FF00000000000000000000000000000000F9F9F9FFEEEEEEFFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFEFEFEFFFFAFA
      FAFF00000000000000000000000000000000000000000000000000000000FAFA
      FAFFEFEFEFFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFEEEEEEFFF9F9F9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFFEFEFEFFF9F9F9FFEFEFEFFFE4E4E4FFD6D6
      D6FFC7C7C7FFBEBEBEFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBEBEBEFFC7C7
      C7FFD6D6D6FFE4E4E4FFEFEFEFFFF9F9F9FFFEFEFEFF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFFCFCFCFFEAEA
      EAFFD0D0D0FFD1D1D1FFEEEEEEFF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F2F2
      F2FFD4D4D4FFC0C0C0FFBCBCBCFFBCBCBCFFC0C0C0FFD4D4D4FFEFEFEFFF0000
      0000000000000000000000000000F9F9F9FFE3E3E3FFC8C8C8FFC8C8C8FFE3E3
      E3FFF9F9F9FF000000000000000000000000EEEEEEFFCDCDCDFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBDBDBDFFCECECEFFEFEF
      EFFF00000000000000000000000000000000000000000000000000000000EFEF
      EFFFCECECEFFBDBDBDFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFCDCDCDFFEEEEEEFF000000FF000000FF000000FF0000
      00FF000000FF000000FFFCFCFCFFF2F2F2FFDFDFDFFFCACACAFFB5B5BBFF7980
      B8FF4652BCFF2D3CC0FF2D3CC0FF2D3DC0FF2E3DC0FF2E3DC0FF2E3EC1FF4753
      BCFF7A81B8FFB5B5BBFFCACACAFFDFDFDFFFF2F2F2FFFCFCFCFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FFFCFCFCFFEBEBEBFFCDCD
      CDFF38986CFF008E4EFFE9E9E9FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000FBFBFBFFF4F4
      F4FFEDEDEDFFEAEAEAFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFDADA
      DAFF3A9A6EFF008949FF008747FF008747FF008948FF39986BFFCBCBCBFFC3C3
      C3FF37966AFF00000000E4E4E4FFE3E3E3FFC2C2C2FF008949FF008949FFC2C2
      C2FFE3E3E3FFF9F9F9FF0000000000000000E9E9E9FFB47A07FFB47A07FFB47A
      08FFB47B08FFB47A08FFB47A07FFB47A07FFB47B09FFB67E0EFFB98723FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB98725FFB67E0EFFB47B09FFB47A07FFB47A07FFB47A07FFB47A07FFB47A
      07FFB47A08FFB47B08FFB47A08FFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FFFAFAFAFFE9E9E9FFD1D1D1FFAAACBBFF4450BDFF2E3EC3FF394F
      D3FF3D56DEFF405DE9FF3D5BEBFF3958EAFF3654E8FF3351E9FF304DE5FF2D46
      DBFF2C42D0FF2E3EC2FF4652BDFFAAACBBFFD1D1D1FFE9E9E9FFFAFAFAFF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFCFCFCFFEBEBEBFFCDCDCDFF3E98
      6DFF00C487FF008C4BFFD8D8D8FFE9E9E9FFEDEDEDFFF5F5F5FFFCFCFCFF0000
      00FF000000FF000000FF000000FF000000FFFEFEFEFFF7F7F7FFE6E6E6FFD2D2
      D2FFC5C5C5FFBEBEBEFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFB7B7
      B7FF008744FF0FE9ACFF00E1A1FF00E1A1FF0FE8ABFF009D5BFF979797FF337D
      59FF00B17AFF008447FFBFBFBFFFB1B1B1FF008644FF18E8AFFF18E8AFFF0086
      44FFC2C2C2FFE3E3E3FFF9F9F9FF00000000E9E9E9FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB67E0EFFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB67E0EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9FF000000FF000000FF000000FF0000
      00FFFAFAFAFFE6E6E6FFC7C7C7FF656EBBFF2D3DC3FF445CDEFF5370F0FF627B
      F5FF738AFCFF8396FFFF8599FFFF8498FFFF8395FFFF8295FFFF7D8FFFFF677D
      FAFF4C64F1FF2E4BE7FF2A43D8FF2E3FC2FF676FBBFFC7C7C7FFE6E6E6FFFAFA
      FAFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFFCFCFCFFEBEBEBFFCDCDCDFF3E986CFF00BD
      81FF00DEA7FF008947FFB7B7B7FFBDBDBDFFC4C4C4FFD3D3D3FFE6E6E6FFF5F5
      F5FFFDFDFDFF000000FF000000FF000000FFF9F9F9FFE1E1E1FFC1BCB8FFAA79
      4FFFA56328FFA44F07FFA44E06FFA44E06FFA34D04FFA34900FFA54200FFFAC5
      B1FF00843FFF2CE3B4FF00D59BFF00D49AFF2CE1B2FF009855FFF9D0C0FF5F5D
      26FF07A479FF006F39FF8A8A8AFF008441FF3DE6BCFF00D69DFF00D69DFF3DE6
      BCFF008643FFC2C2C2FFE3E3E3FFF9F9F9FFE9E9E9FFFFFFFFFFFFFFFFFFFFFF
      FFFFD7C0A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB47B09FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB47B09FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFD7C0A6FFFFFFFFFFE9E9E9FF000000FF000000FF000000FFFAFA
      FAFFE6E6E6FFC5C5C5FF3946BDFF3B4DCEFF5672EFFF7089F9FF889DFFFF7E90
      FFFF7083FFFF6779FFFF6275FEFF6375FEFF6376FEFF6376FEFF6879FFFF6E81
      FFFF7689FFFF7A8CFFFF546BF4FF2643E3FF2B3FCCFF3C49BDFFC5C5C5FFE6E6
      E6FFFAFAFAFF000000FF000000FF000000FFFBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE2E2E2FFCCCCCCFF3E976CFF00B880FF00D5
      A0FF00D5A1FF008442FF008846FF008947FF138C52FF579D7CFFC0C0C0FFD4D4
      D4FFE5E5E5FFF0F0F0FFFAFAFAFF000000FFEBEBEBFFBBB1A8FFAB550EFFBE72
      2FFFD0894AFFDD9D5FFFE0A264FFE0A163FFE09E5EFFF7CCA9FFFFE9D7FFFFE7
      D5FF00823DFF49E3BFFF00CF99FF00CF98FF49E2BDFF009654FFFFDCC8FFFED4
      BFFFEBBBA1FF2F744EFF00823BFF5CE9C9FF00D19BFF00CF9AFF00CF9AFF00D1
      9BFF5EEACAFF008744FFC4C4C4FFEEEEEEFFE9E9E9FFD6BDA0FFD6BDA1FFD7BF
      A3FFD9C1A7FFD7BFA3FFD6BDA1FFD6BCA0FFD5BCA0FFFFFFFFFFB47B08FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB47B08FFFFFFFFFFD5BCA0FFD6BCA0FFD6BDA0FFD6BDA0FFD6BDA0FFD6BD
      A1FFD7BFA3FFD9C1A7FFD7BFA3FFE9E9E9FF000000FF000000FFFCFCFCFFE9E9
      E9FFC7C7C7FF3947BDFF4B62DDFF6582F5FF859CFFFF8196FFFF6577FDFF6073
      FDFF6174FDFF6174FDFF6275FDFF6275FDFF6275FDFF6275FDFF6275FDFF6275
      FDFF6274FDFF6577FEFF7789FFFF6E82FEFF304CE8FF2941D5FF3D4BBEFFC7C7
      C7FFE9E9E9FFFCFCFCFF000000FF000000FFF2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFB8B8B8FF3E9263FF00B57EFF00D09FFF00CC
      9CFF00CD9CFF00D09FFF00D1A1FF00D1A1FF00D3A3FF00B37AFF008D4CFF67A0
      83FFBBBBBBFFD0D0D0FFF0F0F0FF000000FFDDDDDDFFAD6322FFCC894AFFE3A7
      6DFFDE9E5FFFDD9A59FFDC9857FFDC9755FFDE934FFFFFE4D2FF00833CFF0084
      3EFF00813CFF6CE5CAFF00CA98FF00C998FF6CE4C9FF009554FF00833DFF0084
      3CFFFFD8C5FF00823AFF79F1DBFF00CE9FFF00CA9AFF00C999FF00C999FF00CA
      9AFF00CF9FFF83F4E0FF008B49FFEEEEEEFFE9E9E9FFFFFFFFFFFFFFFFFFFFFF
      FFFFD7BEA3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB47A08FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB47A08FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFD7BEA3FFFFFFFFFFE9E9E9FF000000FFFEFEFEFFF2F2F2FFD1D1
      D1FF3846BEFF4F65DEFF6E8AF7FF8EA4FFFF7286FEFF5C6FFCFF5D70FCFF5F71
      FCFF5F72FCFF5F72FCFF5F72FCFF5F72FCFF5F72FCFF5F72FCFF5F72FCFF5F72
      FCFF5F72FCFF5F72FCFF5E71FCFF6B7EFFFF798CFFFF3551EAFF2941D5FF3D4A
      BFFFD1D1D1FFF2F2F2FFFEFEFEFF000000FFEAEAEAFFB8B8B8FFB2B2B2FFB0B0
      B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0
      B0FFB0B0B0FFAFAFAFFFB0AEAFFFEFE1E7FF007E34FF52E0C4FF00C89AFF00C7
      99FF00C799FF00C799FF00C89AFF00C89AFF00C89BFF00CB9EFF00C99CFF0097
      58FF54A980FFF7E8EEFFE8E8E8FFFEFEFEFFD6D6D6FFB26017FFE6B485FFE2A1
      60FFE09F5FFFE09F5FFFE09F5FFFE09E5DFFE19A57FFFFE5D3FF008139FF79EC
      DBFF00C89EFF00C59BFF00C59AFF00C49AFF00C59AFF00C79DFF79EBDBFF0080
      38FFFFEBDEFF00833DFF008642FF00823EFF89E9D8FF00C498FF00C498FF89E9
      D8FF00833EFF008947FF008D4CFFF8F8F8FFE9E9E9FFFFFDF8FFFFFEF9FFFFFF
      FFFFD6BC9EFFFFFFFFFFFFFEF9FFFFFDF8FFFFFEFBFFFFFFFFFFB47A07FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB47A07FFFFFFFFFFFFFEFBFFFFFDF8FFFFFDF8FFFFFDF8FFFFFDF8FFFFFE
      F9FFFFFFFFFFD6BC9EFFFFFFFFFFE9E9E9FF000000FFF9F9F9FFE0E0E0FF6871
      BEFF3F52D0FF728DF7FF91A7FFFF6D81FCFF5A6DFBFF5C6FFBFF5D70FBFF5D70
      FBFF5D70FBFF5D70FBFF5D70FBFF5D70FBFF5D70FBFF5D70FBFF5D70FBFF5D70
      FBFF5D70FBFF5D70FBFF5D70FBFF5C6FFBFF697BFDFF7A8CFFFF304CE8FF2B3F
      CBFF6A73BEFFE0E0E0FFF9F9F9FF000000FFE9E9E9FFB2B2B2FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF51B082FF01BC8FFF72E0CBFF00C0
      95FF70DFC9FF72E0CCFF72E1CDFF71E1CCFF6BDFC9FF35D2B3FF00C69EFF00C9
      9FFF00985AFF7CB799FFDFDFDFFFFAFAFAFFD5D5D5FFBF7531FFE9BC92FFE3A6
      65FFE3A667FFE3A667FFE3A667FFE3A666FFE3A362FFF6CDA8FFFFE7D3FF007D
      34FF92EADEFF00C099FF00C099FF00C099FF00BF99FF92EADFFF007D34FFFFE6
      D0FFFFECDFFFFFD5C3FF5EB9A9FF008641FF9BEDE0FF00BF96FF00BF96FF9BED
      E0FF008642FF71A78CFF0000000000000000E9E9E9FFFFFCF5FFFFFDF6FFFFFF
      FBFFD6BB9CFFFFFFFBFFFFFDF6FFFFFCF5FFFFFDF7FFFFFFFFFFB57A07FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB47A07FFFFFFFFFFFFFDF7FFFFFCF5FFFFFCF5FFFFFCF5FFFFFCF5FFFFFD
      F6FFFFFFFBFFD6BB9CFFFFFFFBFFE9E9E9FF000000FFEFEFEFFFB8B9C9FF2D3D
      C2FF718CF5FF8FA7FFFF7085FDFF576BFAFF596DFAFF5A6EFAFF5A6EFAFF5A6E
      FAFF5A6EFAFF5A6EFAFF5A6EFAFF5A6EFAFF5A6EFAFF5A6EFAFF5A6EFAFF5A6E
      FAFF5A6EFAFF5A6EFAFF5A6EFAFF596EFAFF596DFAFF697CFEFF6F82FEFF2643
      E3FF2E3FC2FFB8BAC9FFEFEFEFFF000000FFE9E9E9FFB0B0B0FFFFFFFFFFF4F4
      F3FFF4F4F3FFF5F5F5FFF6F7F7FFF6F8F8FFF6F7F7FFF5F5F5FFF4F5F4FFF4F4
      F3FFF4F4F3FFF5F4F3FFF9F7F7FFFFFAFBFFFFFFFFFF50B185FF07B990FF8DE5
      D6FF8DE7D9FF00904FFF009151FF0B9A5EFF2AB589FF79DFCBFF88E4D5FF1ACA
      AAFF00BF98FF008E4DFFB7C6BEFFF1F1F1FFD5D5D5FFC6803FFFEBC5A0FFE7AC
      6BFFE7AD6DFFE7AD6EFFE7AD6EFFE7AD6EFFE7AB6BFFE7A865FFFACEA8FFFFE6
      D1FF007B30FF96E9DFFF00B999FF00BA99FF96E9E0FF007C33FFFFE8D5FFFACF
      A9FFEDB98AFFDA7A37FF007943FF008743FF99E9E0FF00B995FF00B995FF99E9
      E0FF008743FF008247FF0000000000000000E9E9E9FFFFFFF8FFFFFFF9FFFFFF
      FDFFD6BD9FFFFFFFFEFFFFFFFBFFFFFFFAFFFFFFFBFFFFFFFFFFBC7B07FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB47A08FFFFFFFFFFFFFFF9FFFFFFF8FFFFFFF8FFFFFFF8FFFFFFF8FFFFFF
      F9FFFFFFFDFFD6BD9FFFFFFFFDFFE9E9E9FFFCFCFCFFE4E4E4FF4451BEFF5C74
      E5FF88A1FDFF869CFFFF5467F9FF576BF9FF586CF9FF586CF9FF586CF9FF586C
      F9FF586CF9FF586CF9FF586CF9FF586CF9FF586CF9FF586CF9FF586CF9FF586C
      F9FF586CF9FF586CF9FF586CF9FF586CF9FF586CF9FF566AF9FF7587FFFF546C
      F5FF2A43D8FF4854BFFFE4E4E4FFFCFCFCFFE9E9E9FFB0B0B0FFFFFFFFFFEBEA
      EAFFEBEBEBFFEDEDEEFFD3C5BBFFC1AA99FFD3C5BBFFEDEDEEFFECEBEBFFECEA
      EBFFECEAEBFFF5F1F3FFFFFDFFFFFFFEFFFFFFFEFFFFFFFFFFFF4EAF83FF0BB8
      93FFB2F1EBFF008138FFC7D3C6FFA9C5AEFF4BA476FF00833CFF4EBB96FFB0EE
      E6FF3AD1B8FF00AB7EFF429D71FFEBEBEBFFD5D5D5FFCA8546FFEECEB1FFEBB3
      73FFEBB475FFEBB476FFEBB476FFEBB476FFEBB374FFEBB06DFFECB980FFFDFA
      FCFFFFECDAFF007A2FFF92E5DFFF91E5E0FF007C32FFFFEBD8FFFCD3B0FFECAF
      6BFFF1C69EFFDE8445FFACB6ABFF008946FF9BEBE7FF98E7E1FF98E7E1FF9BEB
      E7FF008946FF00A278FF0000000000000000E9E9E9FFD5BA99FFD5BA9AFFD7BD
      9DFFD9BFA0FFDCBFA0FFE4C0A3FFECC2A6FFEEC2A6FFFFFFFFFFD17C07FFDDDD
      DDFFF8F8F8FFFEFEFEFF0000000000000000000000000000000000000000E9E9
      E9FFB47B08FFFFFFFFFFD4B998FFD4BA98FFD5BA99FFD5BA99FFD5BA99FFD5BA
      9AFFD7BD9DFFD8BFA0FFD7BD9DFFE9E9E9FFF6F6F6FFCECFD5FF2D3CC2FF819D
      FCFF98AFFFFF576CF8FF5368F7FF556AF7FF556AF7FF556AF7FF556AF7FF556A
      F7FF556AF7FF556AF7FF556AF7FF556AF7FF556AF7FF556AF7FF556AF7FF556A
      F7FF556AF7FF556AF7FF556AF7FF556AF7FF556AF7FF5469F7FF586DF8FF7B8D
      FFFF2E4BE8FF2E3EC2FFCFCFD5FFF6F6F6FFEBEBEBFFB0B0B0FFFFFFFFFFE2E1
      E0FFE3E2E1FFE5E6E6FFC2AC9CFFF1CFB1FFC2AC9CFFE5E6E6FFE3E2E1FFE3E1
      E0FFE5E2E2FFEFF0EFFF109458FF007B31FF4EAA7FFFFFFBFFFFFFFBFFFF4CAD
      81FF16BB9DFF008138FFFFE4E6FFFFF3EBFFFFE5E4FFECEEEDFF36A270FF29A7
      74FFBCF2EDFF23C6AFFF008A46FFE9E9E9FFD8D8D8FFD08D4FFFF1D8C0FFF0B9
      78FFEFBA7CFFEFBB7EFFEFBB7EFFEFBB7DFFEFB979FFEFC28BFFF1F3F2FFEEEF
      F0FFFBF5F6FFFFEFDDFF007B30FF007C33FFFFEFDDFFFFD9B5FFF0B675FFF0B8
      76FFF4D5BCFFE08D50FFE6CECFFF4FAA7CFF008946FF008743FF008743FF0089
      46FF37A879FF22C3ACFF0000000000000000E9E9E9FFFFFEF3FFFFFEF4FFFFFF
      F8FFD9BC9CFFFFFFFFFF4FB383FF007F39FF007F3AFF00813EFF1D863BFFC2C2
      C2FFE0E0E0FFF5F5F5FFFEFEFEFF00000000000000000000000000000000E9E9
      E9FFB47A08FFFFFFFFFFFFFEF3FFFFFEF3FFFFFEF3FFFFFEF3FFFFFEF3FFFFFE
      F4FFFFFFF8FFD6BC9BFFFFFFF8FFE9E9E9FFEEEEEEFF848BC4FF5166D9FF8CA6
      FEFF8299FEFF4E63F5FF5267F6FF5368F6FF5368F6FF5368F6FF5368F6FF5368
      F6FF5368F6FF5368F6FF5368F6FF5368F6FF5368F6FF5368F6FF5368F6FF5368
      F6FF5368F6FF5368F6FF5368F6FF5368F6FF5368F6FF5368F6FF5166F6FF7185
      FFFF4C65F1FF2D42D0FF868DC4FFEEEEEEFFEEEEEEFFB2B2B2FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFC3AD9DFFF7D8BEFFC3AD9DFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF007E35FF0AEAA9FF00B670FF83C3A4FFFFFFFFFFFFFF
      FFFF55B98FFF007F33FFFFE5E6FF007F34FF4AA276FFFFFFFFFFFFFFFFFF7AC5
      A1FF46B994FFA7EDECFF008945FFEFEFEFFFE1E1E1FFCC823BFFF6E3D3FFF2D2
      B0FFF2C080FFF2C183FFF2C184FFF2C081FFF2C082FFF6F4F0FFF3F4F5FFEFEF
      ECFFEBEAE8FFF9F4F3FFFFEFDAFFFFEFDDFFFFDDB9FFF3BE7DFFF2BF7EFFF2D2
      AFFFF7E3D3FFD78846FFF1D8D9FF000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFEF9ECFFFFFAEDFFFFFD
      F2FFD9BA99FFFFFFFFFF007C35FF52F1C7FF00E09DFF00E19FFF00D291FF088E
      51FFA8B7B0FFDADADAFFF2F2F2FFFDFDFDFF000000000000000000000000E9E9
      E9FFB47A08FFFFFFFFFFFFFAEDFFFEF9ECFFFEF9ECFFFEF9ECFFFEF9ECFFFFFA
      EDFFFFFDF2FFD5B897FFFFFDF2FFE9E9E9FFEAEAEAFF4753BFFF6B82EAFF96AE
      FFFF697EF9FF4D63F5FF4F66F5FF4F66F5FF4E64F5FF4D64F5FF4D63F5FF4D63
      F5FF4D63F5FF4D63F5FF4D63F5FF4D63F5FF4D63F5FF4D63F5FF4D63F5FF4D63
      F5FF4D63F5FF4D63F5FF4D64F5FF4E64F5FF4F66F5FF5066F5FF4E64F5FF6175
      FAFF687EFAFF2D47DBFF4A57C0FFEAEAEAFFECECECFFB9B9B9FFB1B1B1FFAFAF
      AFFFAFAFAFFFAEB1B2FFC4AF9EFFFCDDC8FFC4AF9EFFAEB1B2FFAFAFAFFFAEAE
      AEFFB0AEAFFFF0E1E7FF007E35FF30E5B1FF07E1A3FF009F5AFF399C6DFFD8D6
      D5FFF8E3ECFFF6E4EDFFFFE5E6FF00823BFF00BF78FF4AA278FFF3E0E7FFF0E0
      E6FF4BA87DFF15995DFF149052FFFAFAFAFFE8E8E8FFC68E57FFDDA976FFF8EC
      E1FFF6DFC8FFF6CE9CFFF7C586FFF7C78AFFFDF7EEFFFDFCFDFFFCF7F4FFF8F4
      EEFFF4F0EBFFEEEBE7FFEADED0FFFAC686FFF9C483FFF6CD9BFFF6DFC7FFF8EC
      E1FFDEA976FFD69863FFFBE1E2FF000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFEF8E9FFFFF9EAFFFFFC
      EFFFD9BA97FFFFFFFCFF007D35FF6AF2D1FF60ECC8FF44E6BAFF00DA9EFF00D3
      95FF009858FF88AD9BFFD5D5D5FFEEEEEEFFFCFCFCFF0000000000000000E9E9
      E9FFB47A08FFFFFFFFFFFFF9EAFFFEF8E9FFFEF8E9FFFEF8E9FFFEF8E9FFFFF9
      EAFFFFFCEFFFD5B995FFFFFCEFFFE9E9E9FFE9E9E9FF2837BFFF87A2FBFF9DB6
      FFFF5166F4FF4C62F4FF4D64F4FF4A61F4FF435BF3FF3D56F3FF3B54F3FF3B53
      F3FF3B53F3FF3B53F3FF3B53F3FF3B53F3FF3B53F3FF3B53F3FF3B53F3FF3B53
      F3FF3B53F3FF3B54F3FF3D56F3FF435BF3FF4A61F4FF4D64F4FF4D63F4FF5166
      F5FF7F91FFFF304DE6FF2E3DC0FFE9E9E9FFE9E9E9FFB6B6B6FFB1B1B1FFAFAF
      AFFFAEAFAFFFAEB0B2FFC4AE9DFFFFFFFFFFC4AE9DFFAEB0B2FFAEAFAFFFAEAE
      AEFFAEADAEFFDAD0D4FF45A277FF2BC391FF30E1B2FF00D99CFF00B371FF0084
      3EFF4FA67DFFA7C6B6FFC8CEBFFF00833FFF00DFA3FF00B776FF4BA378FFF4E0
      E8FFEDE0E6FFFBE8F0FFE9E9E9FF000000FF00000000B3B3B3FFC7915CFFDA9B
      5DFFF1DBC4FFFAF4F1FFFAE8D3FFFFFCF4FFE9F4FBFFA1C7DCFF86B5CDFF7EAC
      C4FF74A2BCFF7FA7BCFFBACBD2FFE5DED2FFFBE9D4FFFAF4F1FFF1DBC4FFDA9B
      5DFFA48F53FFA4C3B3FF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFFFBECFFFFFBEDFFFFFE
      F1FFD9BA97FFFFFFFAFF4FB281FF007F39FF007E38FF31C293FF55E8C3FF08D9
      A3FF00D49AFF00A162FF6AA388FFCDCDCDFFE8E8E8FFFAFAFAFF00000000E9E9
      E9FFB47A08FFFFFFFFFFFFFBEBFFFFFAEBFFFFFBECFFFFFBECFFFFFBECFFFFFB
      EDFFFFFEF1FFD6BA96FFFFFEF1FFE9E9E9FFE9E9E9FF2736BEFF90ADFFFFA0B8
      FFFF4359F2FF495FF3FF495FF3FF4057F2FFC8CEFBFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFC8CEFBFF4057F2FF495FF3FF4A60F3FF475D
      F2FF8597FFFF3351E9FF2E3DC0FFE9E9E9FFE9E9E9FFB2B2B2FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFC2AB99FFFFECE0FFC2AB99FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFDEF0E7FF028F4FFF6FE7CAFF0BD5A2FF00D39BFF00CF
      98FF00B072FF009B59FF008844FF00823EFF00D6A0FF00D49DFF00B375FF5BB3
      88FFFFFFFFFFC3B9BDFFE9E9E9FF000000FF0000000000000000FEFEFEFFEEDB
      C8FFD8904AFFDB9A57FFEFCBA9FFFFFFFFFF99CBE5FF70ADCEFF69A5C4FF5B96
      B4FF4A83A0FF387291FF43738EFFE7EBEBFFE9CEB2FFDC9A58FFD18F47FF3AB9
      7FFF00AF72FF0000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFD5B692FFD5B692FFD7B9
      95FFD9BB99FFDCBB98FFE4BC9BFFECBE9EFFF4BF9FFF56B48CFF19AD79FF5EE8
      C9FF1EDAABFF00D6A0FF00A96EFF3D966CFFC6C6C6FFE4E4E4FFF9F9F9FFE8E8
      E8FFB57B08FFFFFFFFFFD4B48FFFD4B691FFD5B692FFD5B692FFD5B692FFD5B6
      92FFD7B995FFD8BB98FFD7B995FFE9E9E9FFE9E9E9FF2735BEFF93AFFFFFA1BA
      FFFF4057F1FF475EF2FF455DF2FF3750F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3750F1FF455DF2FF475EF2FF445B
      F1FF8698FFFF3654E9FF2E3DC0FFE9E9E9FFE9E9E9FFB0B0B0FFFFFFFFFFFEFE
      FEFFFEFEFEFFFFFFFFFFB6A99DFFC1AA98FFB6A99DFFFFFFFFFFFEFEFEFFFDFD
      FDFFFEFDFDFFFFFFFFFFFFFFFFFF76C19EFF41AF80FF84E8D1FF19D3A8FF00CC
      98FF00CE9BFF00CF9DFF00D09EFF00D09EFF00CE9CFF00CD9BFF00CE9CFF00B0
      73FF56B488FFE7DEE2FFE9E9E9FF000000FF0000000000000000000000000000
      0000FDFDFDFFF3E6D8FFD1985EFFEF9840FF9DBEC5FF73ADCDFF5F91ACFF5080
      99FF44738DFF34657FFF5B7277FFF79F48FFB59B57FF92D4B4FF19D2A7FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFFFAE7FFFFFAE8FFFFFD
      ECFFD7B993FFFFFEECFFFFFBE9FFFFFBE9FFFFFCEAFFFFFFFFFF647B17FF12A5
      70FF67E3C7FF2FDAB3FF00D09EFF00B77FFF128C51FFC3C3C3FFDFDFDFFFDDDD
      DDFFB67A07FFFFFFFFFFFFF9E6FFFFFAE6FFFFFAE7FFFFFAE7FFFFFAE7FFFFFA
      E8FFFFFDECFFD6B993FFFFFDECFFE9E9E9FFE9E9E9FF2635BEFF97B2FFFFA2BB
      FFFF3E54EEFF455BF0FF435BF0FF354EEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF354EEEFF435BF0FF455CF0FF4158
      EFFF879AFFFF3A58EAFF2D3DC0FFE9E9E9FFE9E9E9FFAFAFAFFFFFFFFFFFFBFB
      FBFFFBFBFBFFFCFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFBFBFBFFFAFA
      FAFFFAFAFAFFFDFCFCFFFFFFFFFFFFFFFFFF57B388FF45AF82FF90E7D4FF55DD
      C0FF23D2AAFF1DCFA7FF00CEA3FF00CEA3FF00C698FF00C799FF00C89BFF58E2
      C6FF007D31FFECE0E5FFE9E9E9FF000000FF0000000000000000000000000000
      000000000000FBFCFCFFEEEEEEFF567292FF61849CFF75AFCCFF72ADCDFF6CA7
      C7FF65A0BFFF4A7E9CFF2D526EFF5B7696FF51A77FFF45AE81FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFFF5E0FFFFF6E1FFFFF9
      E6FFD5B590FFFFF9E6FFFFF6E1FFFFF5E0FFFFF6E1FFFFFFFFFFCA7800FFB8D0
      C4FF0E9A60FF69DEC2FF42DCBAFF00CDA0FF00C291FF078E50FFADB7B3FFC2C2
      C2FFBA7A06FFFFFFFFFFFFF6E0FFFEF5E0FFFFF5E0FFFFF5E0FFFFF5E0FFFFF6
      E1FFFFF9E6FFD5B590FFFFF9E6FFE9E9E9FFEAEAEAFF2635BEFF9BB6FFFFA3BC
      FFFF3A52EDFF4159EFFF4159EFFF3851EEFFC5CCFAFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CCFAFF3851EEFF4159EFFF425AEFFF3E56
      EEFF899BFFFF3E5CEBFF2D3CC0FFEAEAEAFFE9E9E9FFAFAFAFFFFFFFFFFFFAFA
      FAFFFCFCFCFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFCFC
      FCFFFAFAFAFFFCFCFCFF969696FFCDC8CAFFFFFFFFFF6AAC8EFF1B985EFF53BB
      94FF90EADAFF96EBDCFF4AE9D1FF4DE8D1FF6CDEC7FF00C297FF5BDCC3FF00A9
      73FF55B487FFE7DEE1FFE9E9E9FF000000FF0000000000000000000000000000
      000000000000F7F7F7FFDDDDDDFF5D7593FF68A2C9FF86C8E9FF7DBDDEFF77B6
      D7FF72AFD1FF6EACCDFF4D83A8FF4F6684FFDDDDDDFF67A78AFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFFF4DEFFFFF5DFFFFFF8
      E3FFD5B68EFFFFF8E3FFFFF5DFFFFFF3DEFFFFF5DEFFFFFFFFFFC37A04FFE8E8
      E8FFE1EEE8FF099154FF67D7BCFF5FE0C7FF00C99FFF00C397FF00975BFF7F98
      70FFC47903FFFFFFFFFFFFF5DEFFFFF4DEFFFFF4DEFFFFF4DEFFFFF4DEFFFFF5
      DFFFFFF8E3FFD5B68EFFFFF8E3FFE9E9E9FFEEEEEEFF2736BFFF97B2FFFFA4BE
      FFFF455CEFFF3E56EEFF4058EEFF3D56EEFF364FEDFF304AECFF2E48ECFF2D48
      ECFF2D48ECFF2D48ECFF2D48ECFF2D48ECFF2D48ECFF2D48ECFF2D48ECFF2D48
      ECFF2D48ECFF2E48ECFF304AECFF364FEDFF3D56EEFF4058EEFF3F57EEFF465C
      EFFF8699FFFF415DE9FF2D3CC0FFEEEEEEFFE9E9E9FFAFAFAFFFFFFFFFFFFAFA
      FAFFCFCFCFFFB8B8B8FFBBBBBBFFBBBBBBFFBBBBBBFFBBBBBBFFB8B8B8FFCFCF
      CFFFFAFAFAFFF9F9F9FFFBFBFBFFFBFBFBFFFDFAFBFFFFFFFFFFFFFDFDFF74C0
      9DFF139558FF007B30FF007E35FF007F36FF81E4D4FF55D8C1FF00A571FF54AE
      82FFFFFFFFFFBCB8BAFFE9E9E9FF000000FF0000000000000000000000000000
      0000CECECEFFAAAAAAFF79828CFF4B7FAAFF97DBF9FF89CAE9FF80C0E0FF7AB9
      DAFF74B2D3FF70ADCEFF6EACCBFF3B6C98FFA4AAB5FFEAEAEAFFFDFBFBFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFFF7E0FFFFF7E0FFFFFA
      E4FFD5B78EFFFFFAE4FFFFF7E0FFFFF6DFFFFFF6DEFFFFFFFFFFBB7A06FFE9E9
      E9FF00000000FAFAFAFF199154FF52C4A0FF77E3D1FF0DC9A6FF00C39CFF009C
      64FF777C16FFFFFFFFFFFFF8E2FFFFF8E1FFFFF7E1FFFFF7E0FFFFF7E0FFFFF7
      E0FFFFFAE4FFD5B78EFFFFFAE4FFE9E9E9FFF6F6F6FF4855C3FF7A91EDFFA3BE
      FFFF5F78F4FF3952ECFF3D56EDFF3D57EDFF3C55EDFF3B55EDFF3B54EDFF3B54
      EDFF3B54EDFF3B54EDFF3B54EDFF3B54EDFF3B54EDFF3B54EDFF3B54EDFF3B54
      EDFF3B54EDFF3B54EDFF3B55EDFF3C55EDFF3D57EDFF3D57EDFF3B54ECFF586E
      F5FF768CFDFF3D56DEFF4D5AC4FFF6F6F6FFE9E9E9FFB0B0B0FFFFFFFFFFFAFA
      FAFFB7B7B7FF6B6B6BFF6F6F6FFF6E6E6EFF6E6E6EFF6F6F6FFF6B6B6BFFB7B7
      B7FFFAFAFAFFF7F7F7FFF9F9F9FFF9F9F9FFF8F7F8FFF9F7F8FFFFFAFCFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF008037FF72DFD3FF00A272FF53AE81FFFFFF
      FFFFFFFFFFFFB0AFB0FFE9E9E9FF000000FF0000000000000000000000000000
      0000B2B2B2FF5C5C5CFF153C6AFF64A1C6FF98DCFBFF8CCDEDFF85C5E5FF7FBE
      DEFF78B7D7FF72B1D2FF70AECFFF4C7FA3FF244C7AFFD5D3D4FFF7F2F4FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFD4B38AFFD4B48BFFD6B6
      8EFFD7B890FFD6B68EFFD4B48BFFD3B38AFFD3B187FFFFFFFFFFB77B08FFE9E9
      E9FF0000000000000000FCFCFCFF52AA7FFF38B185FF7FE4D6FF27CDB1FF00C3
      A0FF009A62FF5CB78FFFF5BC96FFECBB95FFE3B992FFD9B58DFFD5B48BFFD4B4
      8BFFD6B68EFFD7B890FFD6B68EFFE9E9E9FFFCFCFCFF959BD5FF5D72DDFFA3BE
      FFFF829BFBFF344DEAFF3A53ECFF3C55ECFF3C55ECFF3C55ECFF3C55ECFF3C55
      ECFF3C55ECFF3C55ECFF3C55ECFF3C55ECFF3C55ECFF3C55ECFF3C55ECFF3C55
      ECFF3C55ECFF3C55ECFF3C55ECFF3C55ECFF3C55ECFF3B54ECFF3750EBFF7287
      FCFF647CF6FF3A4FD4FF969DD5FFFCFCFCFFE9E9E9FFB0B0B0FFFFFFFFFFF8F8
      F8FFB6B6B6FF6D6D6DFF717171FFBAB9B9FFBAB9B9FF717171FF6D6D6DFFB6B6
      B6FFF9F9F9FFF6F6F7FF989898FFC0C0C0FFBFBFBFFFF8F8F8FF9A9A9AFFC2C1
      C2FFC2C0C1FFC4C0C2FFFFFFFFFF007F35FF00A277FF52AE81FFFFFFFFFFFAF5
      F7FFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      0000ACACACFF43505FFF0F3E72FF74B3D8FF9CE0FEFF91D3F3FF89CAEAFF83C3
      E3FF7DBCDCFF76B6D5FF73B2D2FF5587A7FF0E3466FF8496A8FF919191FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFFF5DCFFFFF5DDFFFFF8
      E0FFD5B58CFFFFF8E0FFFFF5DDFFFFF4DBFFFFF3D9FFFFFFFFFFB57A08FFE9E9
      E9FF000000000000000000000000FDFDFDFF8CC5A9FF26A672FF86E1D4FF3CD0
      B9FF00BE9CFF00A779FF00823AFF00823AFF4FB07CFFFFFAE3FFFFF5DDFFFFF5
      DDFFFFF8E0FFD5B58CFFFFF8E0FFE9E9E9FF000000FFE6E7EDFF2D3CC3FFA7C2
      FFFFA9C2FFFF3C56EBFF354FEAFF3852EBFF3953EBFF3953EBFF3953EBFF3953
      EBFF3953EBFF3953EBFF3953EBFF3953EBFF3953EBFF3953EBFF3953EBFF3953
      EBFF3953EBFF3953EBFF3953EBFF3953EBFF3953EBFF3650EAFF3C55EBFF8CA0
      FFFF5470F0FF2E3EC3FFE7E7EDFF000000FFE9E9E9FFB0B0B0FFFFFFFFFFF7F6
      F4FFB2B2B0FFB7B6B5FF6C6A68FFE5E2DFFFE5E2DFFF6C6A68FFB7B6B5FFB2B2
      B0FFF8F6F5FFF5F4F3FFF9F8F6FFF9F8F7FFF8F6F5FFF7F5F4FFF8F6F5FFF9F8
      F7FFF8F6F5FFF8F5F5FFFFFFFFFF007B2EFF4FB082FFFFFFFFFFF9F3F5FFF2F0
      EFFFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      0000A4A4A2FF3B5A7BFF144A79FF8ACDEEFFA4EAFFFF99DDFBFF90D3F2FF89CA
      EAFF82C2E2FF7CBADBFF79B7D8FF6095B5FF0F2E5DFF4F6C8EFFE5E3E2FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFDF1D4FFFEF2D5FFFFF5
      D9FFD4B387FFFFF5D9FFFEF2D5FFFDF0D3FFFEF1D3FFFFFFFFFFB47A08FFE9E9
      E9FF00000000000000000000000000000000FEFEFEFFB4D8C6FF159A61FF81DB
      CDFF53D4C3FF00B89CFF00B99EFF5CDACDFF007E36FFFFF9DFFFFFF2D6FFFEF2
      D5FFFFF5D9FFD4B387FFFFF5D9FFE9E9E9FF000000FFF9F9F9FF4956C5FF788E
      EAFFA6C1FFFF8AA3FCFF2D47E7FF344EE8FF3751E9FF3751E9FF3751E9FF3751
      E9FF3751E9FF3751E9FF3751E9FF3751E9FF3751E9FF3751E9FF3751E9FF3751
      E9FF3751E9FF3751E9FF3751E9FF3751E9FF354FE8FF304AE7FF798FFDFF728B
      F9FF455DDFFF4D59C6FFF9F9F9FF000000FFE9E9E9FFB0B0B0FFFFFFFFFFF4F3
      F2FFADABA9FFFFFFFFFF90A2C6FF85ADD5FF85ADD5FF91A3C6FFFFFFFFFFADAB
      AAFFF5F4F3FFF4F3F1FF9A9A9AFFC1C1C0FFBFC0BFFFBEBEBDFFF5F4F3FF9A9A
      9AFFBFBFBEFFF4F2F2FFD5D1D3FFF1E8ECFFEEE6E9FFF7F2F4FFF0EEEDFFEFED
      ECFFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      00009E9C9AFF1A4577FF17517FFF72B2D5FF609BC0FF639DC2FF8CCCECFF94D8
      F6FF8ACBEAFF82C2E3FF80C0E1FF4F7EA0FF0D2857FF1D497AFFE0DFDEFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFDF0D2FFFEF1D3FFFFF4
      D7FFD4B286FFFFF4D7FFFEF1D3FFFDEFD1FFFEF0D1FFFFFFFFFFB47A08FFE9E9
      E9FF0000000000000000000000000000000000000000FEFEFEFFDFEDE6FF0C92
      56FF75D6C7FF90E5DFFF83E2D9FF73DDD7FF007E36FFFFF8DDFFFFF1D4FFFEF1
      D3FFFFF4D7FFD4B286FFFFF4D7FFE9E9E9FF000000FFFEFEFEFFDBDCECFF2D3C
      C2FFA2BCFFFFA8C3FFFF607AF2FF2C46E6FF314BE8FF344EE8FF344EE8FF344E
      E8FF344EE8FF344EE8FF344EE8FF344EE8FF344EE8FF344EE8FF344EE8FF344E
      E8FF344EE8FF344EE8FF344EE8FF324CE8FF2E48E6FF5870F2FF889FFFFF5773
      EFFF2D3DC2FFDBDDECFFFEFEFEFF000000FFE9E9E9FFB0B0B0FFFFFFFFFFF1F0
      EFFFA9A8A6FFFFFFFFFF425F96FFC5EEFFFFC6EEFFFF34538EFFFFFFFFFFA9A9
      A7FFF2F1F0FFF1F0EFFFF4F3F2FFF5F4F2FFF3F2F1FFF2F1F0FFF3F2F1FFF4F3
      F2FFF3F2F1FFF2F0EFFFF2F0EFFFF4F1F1FFF3F0F0FFEFEEEDFFEDECEBFFECEB
      EAFFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      00009A9A98FF164276FF1A5884FF1F5785FF1E5481FF1F4F7CFF16416DFF2F5D
      84FF73AECFFF8FD2F1FF84C5E5FF27476CFF0F2B59FF1A497BFFDEDDDCFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFFFF3D4FFFFF3D4FFFFF6
      D8FFD7B486FFFFF6D8FFFFF3D4FFFFF2D2FFFFF2D1FFFFFFFFFFB47B09FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FF0A863BFF00823BFF008239FF00823AFF51B078FFFFF9DBFFFFF4D5FFFFF3
      D4FFFFF6D8FFD7B486FFFFF6D8FFE9E9E9FF000000FF000000FFFCFCFCFF7B84
      D2FF4D60D3FFA9C4FFFFAAC4FFFF5771EFFF2943E5FF2F49E6FF314BE7FF324C
      E7FF324CE7FF324CE7FF324CE7FF324CE7FF324CE7FF324CE7FF324CE7FF324C
      E7FF324CE7FF324BE7FF304AE6FF2B45E5FF5169F0FF92A8FFFF6683F5FF3B4D
      CEFF7C85D2FFFCFCFCFF000000FF000000FFE9E9E9FFB0B0B0FFFFFFFFFFEFED
      ECFFA7A6A4FFFFFFFFFF244585FFC9F2FFFFC9F2FFFF254686FFFFFFFFFFA8A6
      A4FFF0EFEEFFEFEEEDFF9B9B9BFFBFBFBFFFBDBDBDFFF1EFEEFF9C9C9CFFBFBF
      BFFFBEBEBFFFBCBDBDFFF0EFEEFF9A9A9BFFBDBDBDFFEDECEBFFEBEAE9FFEAE9
      E8FFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      0000999896FF174376FF165B8AFF256E9EFF377CADFF4686B4FF558CB9FF4973
      9FFF294D78FF2B4E76FF20406AFF1E3763FF122E5CFF1A497BFF8F8F8FFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FFE4B57FFFE3B680FFE1B7
      83FFE0B887FFE1B783FFE3B680FFE3B47EFFDEB27CFFFFFFFFFFB57B09FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFC57D09FFFFFFFFFFF7BB88FFF9BD89FFF3BB86FFE9B781FFE4B67FFFE3B6
      80FFE1B783FFE0B887FFE1B783FFE9E9E9FF000000FF000000FF000000FFFAFA
      FAFF3B48C3FF7186E7FFA8C3FFFFAAC4FFFF5D77F1FF243FE3FF2A45E5FF2D48
      E6FF2E49E6FF2F4AE6FF2F4AE6FF2F4AE6FF2F4AE6FF2F4AE6FF2F4AE6FF2E49
      E6FF2D48E6FF2B46E5FF2641E4FF5871F1FF95ABFFFF6F8AF7FF4C62DDFF3E4C
      C4FFFAFAFAFF000000FF000000FF000000FFE9E9E9FFB0B0B0FFFFFFFFFFECEB
      EAFFA3A2A2FFFFFFFFFF6D84AFFF254786FF254786FF6E85AFFFFFFFFFFFA4A2
      A2FFEEEDEBFFECEBEAFFEFEEEDFFEFEEEDFFEEEDECFFEFEEEDFFF0EFEEFFF0EF
      EDFFEFEEEDFFEEEDECFFEFEEEDFFF0EFEEFFEEEDECFFEBEAE9FFE9E8E7FFE8E7
      E5FFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      0000979696FF164175FF185B8AFF2774A6FF3982B2FF4489B8FF538FBDFF5B91
      BCFF5482AFFF436A95FF315580FF223E6BFF132E5BFF19487AFFDEDDDCFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FF58C9FFFF56C9FFFF51CA
      FFFFEBB97FFF51CAFFFF56C9FFFF56C8FFFF4DC5FFFFFFFFFFFFB67B08FFE9E9
      E9FF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB87C08FFFFFFFFFF51C7FFFF59C9FFFF5AC9FFFF59C9FFFF58C9FFFF56C9
      FFFF51CAFFFFEBB97FFF51CAFFFFE9E9E9FF000000FF000000FF000000FF0000
      00FFFAFAFAFF3C48C3FF7186E7FFA9C4FFFFA8C3FFFF89A2FBFF304CE5FF233F
      E3FF2742E3FF2A45E4FF2B46E5FF2B46E5FF2B46E5FF2B46E5FF2A45E4FF2843
      E3FF2540E3FF304CE6FF7D95FCFF92AAFFFF738EF8FF4F65DFFF3F4DC4FFFAFA
      FAFF000000FF000000FF000000FF000000FFE9E9E9FFB0B0B0FFFFFFFFFFE8E7
      E6FFBBBAB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBBBA
      BAFFEAE9E8FFEAE9E8FF9B9B9BFFBDBCBCFFECEBEAFF9C9D9DFFBEBDBDFFBDBC
      BDFFBCBBBBFFECEBEAFF9B9B9CFFBEBDBDFFBCBBBBFFE9E8E7FFE7E6E5FFE5E4
      E3FFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      0000B3B2B1FF1A4677FF16507EFF2476AAFF337FB1FF3F85B5FF4789B8FF4D89
      B7FF487EABFF3C6C99FF2C537FFF1E3C67FF153766FF194779FFB6B5B5FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E9E9E9FF56C6FFFF55C6FFFF4FC6
      FFFFE9B579FF4FC6FFFF55C6FFFF54C5FFFF4CC3FFFFFFFFFFFFB67C09FFEAEA
      EAFF00000000000000000000000000000000000000000000000000000000E9E9
      E9FFB67C09FFFFFFFFFF4CC3FFFF54C5FFFF56C6FFFF56C6FFFF56C6FFFF55C6
      FFFF4FC6FFFFE9B579FF4FC6FFFFE9E9E9FF000000FF000000FF000000FF0000
      00FF000000FFFAFAFAFF3C4AC4FF4E60D3FFA2BCFFFFA7C2FFFFAAC3FFFF7D97
      F9FF516CEDFF2F4BE5FF1D3AE1FF1E3BE1FF1E3BE1FF1E3AE1FF2E4BE5FF4F6A
      EEFF7790F9FF9CB2FFFF8AA2FEFF718DF5FF3F52D0FF3F4DC4FFFAFAFAFF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FFB0B0B0FFFFFFFFFFE4E3
      E1FFE5E4E3FFE4E3E2FFE4E3E1FFE5E4E1FFE5E4E1FFE4E3E1FFE4E3E2FFE6E5
      E3FFE6E5E3FFE6E5E4FFE8E7E6FFE7E6E5FFE7E6E5FFE8E7E6FFE8E7E6FFE8E7
      E6FFE7E6E5FFE7E6E5FFE8E7E6FFE8E7E6FFE7E6E5FFE5E4E3FFE4E3E2FFE3E2
      E1FFFFFFFFFFB0B0B0FFE9E9E9FF000000FF0000000000000000000000000000
      0000E2E1E0FF6F85A0FF164677FF1C6D9FFF2A7CAFFF3680B1FF3C85B5FF3F83
      B3FF3B78A7FF326694FF25507BFF163562FF174172FF7189A3FFE5E4E3FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EEEEEEFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB67E0EFFF2F2
      F2FF00000000000000000000000000000000000000000000000000000000EFEF
      EFFFB67E0EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEFF000000FF000000FF000000FF0000
      00FF000000FF000000FFFCFCFCFF7E87D6FF2D3CC2FF788EEAFFA7C2FFFFA4BF
      FFFFA5BFFFFFA7BFFFFFA6BFFFFFA5BEFFFFA4BDFFFFA3BAFFFFA0B8FFFF99B1
      FFFF8DA7FFFF829DFDFF5D74E5FF2D3DC2FF7F88D6FFFCFCFCFF000000FF0000
      00FF000000FF000000FF000000FF000000FFEAEAEAFFB0B0B0FFFFFFFFFFE0DF
      DEFFE1E0DFFFE1E0DFFFE1E0DEFFE1E0DEFFE1E0DEFFE1E0DEFFE1E0DFFFE1E0
      DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0
      DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DEFFE0DF
      DEFFFFFFFFFFB0B0B0FFEAEAEAFF000000FF0000000000000000000000000000
      000000000000DBDAD9FF134173FF154978FF2072A4FF2B7DAFFF317FB2FF327C
      ADFF2E6F9EFF255E8CFF1A4874FF143766FF144477FFDBDAD9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9FFB67B07FFB67B07FFB67B
      08FFB57B08FFB67B08FFB67B07FFB67B07FFB67C09FFB67E0EFFCAA65BFFFBFB
      FBFF00000000000000000000000000000000000000000000000000000000FAFA
      FAFFBD8B29FFB67E0EFFB67C09FFB67B07FFB67B07FFB67B07FFB67B07FFB67B
      07FFB67B08FFB57B08FFB67B08FFF9F9F9FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFFEFEFEFFE1E3F2FF4C59C8FF2D3CC3FF5D72
      DDFF7A91EEFF97B2FFFF9BB7FFFF98B3FFFF94AFFFFF91ADFFFF88A3FBFF6B83
      EAFF5166D9FF2D3CC2FF4E5BC8FFE1E3F2FFFEFEFEFF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF2F2F2FFB2B2B2FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB2B2B2FFF2F2F2FF000000FF0000000000000000000000000000
      000000000000FEFEFEFFDCE1E8FF134073FF144575FF195989FF1F6796FF2065
      92FF1C5C89FF184F7EFF144171FF134377FFDCE1E8FFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFF2F3F9FF9FA5
      DFFF4D5BC8FF2736BFFF2635BEFF2635BEFF2735BEFF2736BEFF2837BFFF4F5B
      C8FF9FA6DFFFF2F3F9FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFC7C7C7FFB2B2B2FFB0B0
      B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0
      B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0
      B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0B0FFB0B0
      B0FFB2B2B2FFC7C7C7FFFBFBFBFF000000FF0000000000000000000000000000
      0000000000000000000000000000AEAEAEFF4F6B89FF164375FF134073FF1441
      74FF144174FF164476FF506C8AFFAEAEAEFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FAFAFAFFF0F0F0FFEAEAEAFFE9E9E9FFE9E9E9FFEAEAEAFFECECECFFEBEB
      EBFFEAEAEAFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFEAEAEAFFECECECFFECECECFFEDEDEDFFEBEBEBFFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFE
      FEFFF9F9F9FFF0F0F0FFEAEAEAFFE9E9E9FFE9E9E9FFEAEAEAFFF0F0F0FFF9F9
      F9FFFEFEFEFF000000000000000000000000FAFAFAFFEFEFEFFFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFEFEFEFFFFAFAFAFF0000000000000000000000000000
      0000F0F0F0FFD0D0D0FFBEBEBEFFBCBCBCFFBCBCBCFFBFBFBFFFC8C8C8FFCCCC
      CCFFC4C4C4FFBDBDBDFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBFBFBFFFC8C8C8FFCECECEFFCACACAFFC0C0C0FFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF0000000000000000000000000000
      00000000000000000000000000000000000000000000FBFBFBFFF2F2F2FFEAEA
      EAFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFCFCFFF1F1
      F1FFDFDFDFFFCBCBCBFFBFBFBFFFBCBCBCFFBCBCBCFFBFBFBFFFCBCBCBFFDFDF
      DFFFF1F1F1FFFCFCFCFF0000000000000000EFEFEFFFCECECEFFBDBDBDFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBDBDBDFFCECECEFFEFEFEFFF0000000000000000000000000000
      0000EAEAEAFFB6B6B6FFB2B2B2FFB0B0B0FFB0B0B0FFB1B1B1FFB0B0B0FFFFFF
      FFFFAFAFAFFFB1B1B1FFB0B0B0FFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAF
      AFFFAFAFAFFFB0B0B0FFB1B1B1FFB0B0B0FFFFFFFFFFB1B1B1FFB1B1B1FFB0B0
      B0FFB0B0B0FFB2B2B2FFB8B8B8FFEAEAEAFF0000000000000000000000000000
      00000000000000000000000000000000000000000000F2F2F2FFD4D4D4FFC0C0
      C0FFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FCFCFCFFEDEDEDFFD2D2
      D2FF9FA2BCFF4E5BBAFF2B3BBFFF2B3BBEFF2B3BBEFF2B3BBFFF4E5BBAFF9FA2
      BCFFD2D2D2FFEDEDEDFFFCFCFCFF00000000E9E9E9FFB98723FFB67E0EFFB47B
      09FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A
      07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A
      07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A
      07FFB47B09FFB67E0EFFB98723FFE9E9E9FF0000000000000000000000000000
      0000E9E9E9FFB2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFACACACFFB1B1
      B1FFACACACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFACACACFFB1B1B1FFACACACFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB2B2B2FFE9E9E9FF0000000000000000000000000000
      000000000000FBFBFBFFF2F2F2FFEAEAEAFFE9E9E9FFDADADAFFB4B4B4FFAFAF
      ADFFAEAEABFFADADABFFADADABFFADADABFFADADABFFAFAEACFFB0AFADFFAFAE
      ADFFAEAEACFFADADABFFADADABFFADADABFFADADABFFADADABFFADADABFFADAD
      ABFFAEAEABFFAFAFADFFB6B6B5FFEAEAEAFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFF1F1F1FFD2D2D2FF656E
      BBFF3344C7FF5164EBFF6578FFFF697CFFFF697CFFFF6578FFFF5164EBFF3344
      C7FF656EBBFFD2D2D2FFF1F1F1FFFEFEFEFFE9E9E9FFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB67E0EFFE9E9E9FF0000000000000000000000000000
      0000E9E9E9FFB0B0B0FFFFFFFFFFFEFEFEFFFEFEFEFFFFFFFFFFA9A9A9FFABAB
      ABFFA9A9A9FFFFFFFFFFFEFEFEFFFDFDFDFFFEFEFEFFFEFEFEFFFEFEFEFFFEFE
      FEFFFEFEFEFFFFFFFFFFFFFFFFFFAAAAAAFFACACACFFAAAAAAFFFFFFFFFFFFFF
      FFFFFEFEFEFFFFFFFFFFB0B0B0FFE9E9E9FF0000000000000000000000000000
      000000000000F2F2F2FFD4D4D4FFC0C0C0FFBCBCBCFFB7B7B7FFAFAFADFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB0B0ADFFE9E9E9FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9FFDFDFDFFF6972BFFF3546
      CAFF6072FEFF6174FEFF6073FDFF5F72FCFF5F72FCFF6073FDFF6174FEFF6072
      FEFF3546CAFF6972BFFFDFDFDFFFF9F9F9FFE9E9E9FFB47B09FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB47B09FFE9E9E9FF0000000000000000000000000000
      0000E9E9E9FFAFAFAFFFFFFFFFFFFBFBFBFFFBFBFBFFFDFDFDFFFFFFFFFFFFFF
      FFFFFFFFFFFFFDFDFDFFFBFBFBFFFBFBFBFFFCFCFCFFFEFEFEFFFEFEFEFFFDFD
      FDFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
      FDFFFCFCFCFFFFFFFFFFAFAFAFFFE9E9E9FF00000000FBFBFBFFF1F1F1FFEAEA
      EAFFE9E9E9FFDADADAFFB4B4B4FFAFAFADFFAEAEACFFAEAEACFFACACA9FFFFFF
      FFFFFFFFFFFFFEFEFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF00792DFF4FB0
      83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFE
      FFFFFFFFFFFFFFFFFFFFAEAEABFFE9E9E9FFFBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE0E0E0FFA6AAC6FF3242C8FF5B6F
      FCFF5B6FFCFF596EFAFF596DF9FF596DF9FF596DF9FF596DF9FF596EFAFF5B6F
      FCFF5B6FFCFF3242C7FFABAEC9FFF0F0F0FFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FFFFFFFFFEFFFFFFFEFFFFFFFFFFFFFFFFFF9E9E9DFFFFFFFFFFFFFFFFFFFFFF
      FEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFEFFFFFF
      FEFFFFFFFEFFFFFFFFFFFFFFFFFF9E9E9DFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
      FEFFFFFFFFFFFFFFFFFFB47A07FFE9E9E9FF0000000000000000000000000000
      0000E9E9E9FFAFB0B1FFFFFFFFFFFAFAFAFFFCFCFCFFFEFEFEFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFEFEFEFFFDFDFDFFFDFDFDFF969696FFC1C1C1FFFEFE
      FEFF979797FFC2C2C2FFC2C2C2FFC1C1C1FFFEFEFEFF979797FFC3C3C3FFC0C0
      C0FFFBFBFBFFFFFFFFFFAFAFAFFFE9E9E9FF00000000F1F1F1FFD2D2D2FFBFBF
      BFFFBCBCBCFFB7B7B7FFAFAFADFFFFFFFFFFFFFFFFFFFFFFFFFFA8A8A5FFFFFF
      FFFFFDFDFCFFFCFCFBFFFEFDFCFFFFFFFFFFFFFFFFFF00792FFF00DE9FFF007E
      36FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFDFFFCFC
      FBFFFDFDFCFFFFFFFFFFADADABFFE9E9E9FFF2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFB8B8B8FF4B5AC3FF465AE8FF566B
      FAFF5268F8FF5167F7FF5166F7FF5166F7FF5166F7FF5166F7FF5167F7FF5268
      F8FF566BFAFF475BE6FF535FC0FFEAEAEAFFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FEFFFFFEFBFFFFFEFBFFFFFFFCFFFFFFFFFFA1A0A0FFFFFFFFFFFFFFFCFFFFFE
      FBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFE
      FBFFFFFEFBFFFFFFFCFFFFFFFFFFA1A0A0FFFFFFFFFFFFFFFCFFFFFEFBFFFFFE
      FBFFFFFFFEFFFFFFFFFFB47A07FFE9E9E9FFFBFBFBFFF1F1F1FFEAEAEAFFE9E9
      E9FFD8D8D8FFAFB1B4FFFFFFFFFFFAFAFAFFCFCFCFFFB8B8B8FFBBBBBBFFBBBB
      BBFFBBBBBBFFBBBBBBFFB8B8B8FFCFCFCFFFFCFCFCFFFBFBFBFFFBFBFBFFF9F9
      F9FFFBFBFBFFFBFBFBFFFBFBFBFFFAFAFAFFF9F9F9FFFBFBFBFFFBFBFBFFF9F9
      F9FFF8F8F8FFFFFFFFFFAFAFAFFFE9E9E9FF00000000EAEAEAFFB5B5B4FFAFAF
      ADFFAEAEACFFAEAEACFFACACA9FFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A4FFFFFF
      FFFFFBFBFBFFFCFBFBFFFFFEFFFFFFFFFFFF00792FFF00D59DFF00D6A0FF0081
      3CFF007F39FF007E36FF007C34FF078E4DFF7BC19FFFFFFCFCFFFFFFFFFFFDFB
      FCFFFBFBFBFFFFFFFFFFADADABFFE9E9E9FFEAEAEAFFBA964BFFB77F0FFFB67D
      0AFFB67C09FFB67C09FFB67C09FFB67C09FFB67C09FFB67C09FFB67C09FFB67C
      09FFB67C09FFB67C09FFB67C09FFB97E07FFC68700FF1D32CAFF4C63F8FF4A61
      F5FF4159F3FF3D55F2FF3C54F2FF3C54F2FF3C54F2FF3C54F2FF3D55F2FF4159
      F3FF4A61F5FF4F64F6FF2D3CBFFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FCFFFFFDF9FFFFFDF9FFFFFEFAFFFFFFFFFFA2A19EFFFFFFFFFFFFFEFAFFFFFD
      F9FFFFFDF9FFFFFDF9FFFFFDF9FFFFFDF9FFFFFDF9FFFFFDF9FFFFFDF9FFFFFD
      F9FFFFFDF9FFFFFEFAFFFFFFFFFFA2A19EFFFFFFFFFFFFFEFAFFFFFDF9FFFFFD
      F9FFFFFFFCFFFFFFFFFFB47A07FFE9E9E9FFF1F1F1FFD2D2D2FFBFBFBFFFBCBC
      BCFFB7B7B7FFAFB2B9FFFFFFFFFFFAFAFAFFB7B7B7FF6B6B6BFF6F6F6FFF6E6E
      6EFF6E6E6EFF6F6F6FFF6B6B6BFFB7B7B7FFFCFCFCFFF9F9F9FFF9F9F9FFF8F8
      F8FFF7F7F7FFF9F9F9FFF9F9F9FFF8F8F8FFF7F7F7FFF6F6F6FFF5F5F5FFF4F4
      F4FFF4F4F4FFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFB0B0ADFFFFFF
      FFFFFFFFFFFFFFFFFFFFA8A8A5FFFFFFFFFFFEFEFDFFFFFFFFFFA6A6A3FFFFFF
      FFFFFBFAFBFFFFFCFEFFFFFFFFFF00792EFF00CF9CFF00CE9CFF00CE9CFF00D0
      9EFF00D19EFF00D09EFF00D09EFF00C592FF00A56AFF008844FFDBEAE2FFFFFE
      FFFFFCFBFBFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB77F0FFFF7FFFFFFF3F8
      FFFFF2F7FFFFF2F7FFFFF2F7FFFFF2F7FFFFF2F7FFFFF2F7FFFFF2F7FFFFF2F7
      FFFFF2F7FFFFF2F7FFFFF2F7FFFFF5F9FFFFFFFFFFFF192CC1FF5D71F8FF3B53
      F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF3B53F2FF5F74F8FF2C3BBFFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FBFFFFFDF8FFFFFDF8FFFFFEF9FFFFFFFFFFA2A19DFFFFFFFFFFFFFEF9FFFFFD
      F8FFFFFDF8FFFFFDF8FFFFFDF8FFFFFDF8FFFFFDF8FFFFFDF8FFFFFDF8FFFFFD
      F8FFFFFDF8FFFFFEF9FFFFFFFFFFA2A19DFFFFFFFFFFFFFEF9FFFFFDF8FFFFFD
      F8FFFFFFFBFFFFFFFFFFB47A07FFE9E9E9FFEAEAEAFFB99240FFB78217FFB680
      11FFB68B35FFAEB2BAFFFFFFFFFFF8F8F8FFB6B6B6FF6D6D6DFF717171FFBAB9
      B9FFBAB9B9FF717171FF6D6D6DFFB7B7B7FFFCFCFCFF999999FFC0C0C0FFBFBF
      BFFFF8F8F8FF99999AFFC1C1C1FFC0C0C0FFBEBEBEFFF5F5F5FFF3F3F3FFF2F2
      F2FFF2F2F2FFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFAEAEABFFFFFF
      FFFFFFFFFFFFFFFFFFFFA6A6A4FFFFFFFFFFFCFCFCFFFFFFFFFFA6A6A4FFFFFF
      FFFFFAF8F9FFFFFCFFFF007627FF47DFC1FF00C99BFF00C799FF00C698FF10CC
      A4FF10CDA4FF00C599FF00C69AFF00C69BFF00C89DFF00BC8BFF00853FFFFEF9
      FAFFFFFAFCFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67D0AFFF4FBFFFFECED
      F0FFECECEDFFECECEDFFECECEDFFECECEDFFECECEDFFECECEDFFECECEDFFECEC
      EDFFECECEDFFEBECEDFFEBEBEDFFEEEEEDFFFEFCEFFF1628BCFF6D81F8FF334E
      F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF334EF0FF7084F8FF2A3ABFFFEAEAEAFFE9E9E9FFB47B08FFFFFFFFFFFFFF
      FCFFFFFFFBFFFFFFFBFFFFFFFDFFFFFFFFFFA5A39FFFFFFFFFFFFFFFFDFFFFFF
      FBFFFFFFFBFFFFFFFBFFFFFFFBFFFFFFFBFFFFFFFBFFFFFFFBFFFFFFFBFFFFFF
      FBFFFFFFFBFFFFFFFDFFFFFFFFFFA5A39FFFFFFFFFFFFFFFFDFFFFFFFBFFFFFF
      FBFFFFFFFCFFFFFFFFFFB47B08FFE9E9E9FFE9E9E9FFB78218FFF6CD8AFFF5C7
      7EFFE4C189FFACB0B7FFFFFFFFFFF5F6F4FFB2B2B0FFB7B6B5FF6C6A68FFE5E2
      DFFFE5E2DFFF6C6A68FFB7B6B5FFB2B2B1FFF9F9F8FFF8F8F7FFF8F8F7FFF6F6
      F5FFF5F5F4FFF6F6F5FFF8F8F7FFF7F7F6FFF5F5F4FFF5F5F4FFF4F4F3FFF2F2
      F1FFF0F0EFFFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFADADABFFFFFF
      FFFFFEFEFDFFFFFFFFFFA6A6A3FFFFFFFFFFFBFBFBFFFFFFFFFFA6A6A4FFFFFF
      FFFFF7F7F7FFFEF9FBFFFFFFFFFF00792DFF6EE2CEFF00C197FF70DFCAFF70E1
      CCFF70E2CDFF75E1CFFF76E0CEFF49D5BAFF06C4A0FF00C29DFF00A16AFF79C0
      9CFFFFFCFFFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C09FFF4F9FFFFEAE9
      EBFFEBE8E8FFEBE9E9FFEBE9E9FFEBE8E9FFEBE9E9FFEBE9E9FFEBE9E9FFEAE8
      E8FFEAE8E8FFE9E6E6FFE8E6E6FFEAE8E6FFF9F5E9FF1124BAFF8E9DF6FF324D
      EDFF2F4AECFF2C47EBFF2B47EBFF2B47EBFF2B47EBFF2B47EBFF2C47EBFF2F4A
      ECFF334EEDFF93A2F7FF2A3AC0FFF0F0F0FFE9E9E9FFB47B08FFFFFFFFFF9E9D
      98FFA09F99FFA19F99FFA1A09AFFA4A39DFFA8A6A0FFA4A39DFFA1A09AFFA19F
      99FFA19F99FFA19F99FFA19F99FFA19F99FFA19F99FFA19F99FFA19F99FFA19F
      99FFA19F99FFA1A09AFFA4A39DFFA8A6A0FFA4A39DFFA1A09AFFA19F99FFA09F
      99FF9E9D98FFFFFFFFFFB47B08FFE9E9E9FFE9E9E9FFB68116FFF3CA85FFEFC0
      71FFE0BC7FFFACAFB6FFFFFFFFFFF4F3F2FFADABA9FFFFFFFFFF90A2C6FF85AD
      D5FF85ADD5FF91A3C6FFFFFFFFFFADACAAFFF8F7F6FF9B9B9BFFC1C1C0FFBFC0
      BFFFBEBEBDFFF5F4F3FF9A9A9AFFBFBFBEFFF5F4F3FF99999AFFC0C0BFFFBDBE
      BDFFF1F0EFFFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFADADABFFFFFF
      FFFFFCFCFCFFFFFFFFFFA6A6A4FFFFFFFFFFFAF8F9FFFEFCFDFFA6A7A4FFFFFF
      FFFFF6F6F5FFFAF7F7FFFFFAFDFFFFFFFFFF00792DFF8BE8DAFF8DE8DBFF007F
      36FF007F35FF0A985CFF0AA16DFF51CEB4FF95E6D9FF13C4A7FF00B28AFF078E
      4EFFFFFDFFFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C09FFF4FAFFFFE9E9
      E9FFEBEAE9FFEEEDECFFEEEDEBFFECEBE9FFEDECEBFFEEEDECFFEDECEBFFEAE9
      E8FFE7E6E5FFFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFF626ED1FF7986E3FF5870
      F0FF324DEBFF3550EBFF3651EBFF3751EBFF3751EBFF3651EBFF3550EBFF324D
      EBFF5A71F0FF848FE6FF5966C9FFF9F9F9FFE9E9E9FFB47B08FFFFFFFFFFFFFF
      F7FFFFFFF6FFFFFFF6FFFFFFF7FFFFFFFBFFA4A39DFFFFFFFBFFFFFFF7FFFFFF
      F6FFFFFFF6FFFFFFF6FFFFFFF6FFFFFFF6FFFFFFF6FFFFFFF6FFFFFFF6FFFFFF
      F6FFFFFFF6FFFFFFF7FFFFFFFBFFA4A39DFFFFFFFBFFFFFFF7FFFFFFF6FFFFFF
      F6FFFFFFF7FFFFFFFFFFB47B08FFE9E9E9FFE9E9E9FFB68116FFF1C986FFEEBC
      6BFFE0BA7BFFABAFB6FFFFFFFFFFF1F0EFFFA9A8A6FFFFFFFFFF425F96FFC5EE
      FFFFC6EEFFFF34538EFFFFFFFFFFAAA9A8FFF5F4F3FFF5F4F3FFF5F4F2FFF3F2
      F1FFF2F1F0FFF3F2F1FFF4F3F2FFF3F2F1FFF2F1F0FFF3F2F1FFF4F3F2FFF2F1
      F0FFEEEDECFFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFADADABFFFFFF
      FFFFFBFBFBFFFFFFFFFFA6A6A4FFFFFFFFFFF7F7F8FFFBFBFCFFA7A7A4FFFFFF
      FFFFFBF6F8FFFFF9FEFFFFFCFFFFFFFFFFFFFFFFFFFF007B2FFFADF0EDFF007E
      34FFFFFBFFFFC4DED1FFC8DFD2FF3EA572FF4CC3A7FF89E0D7FF00B79DFF007E
      33FFFFFCFFFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C09FFF4FAFFFFE7E7
      E8FFECEBEBFF4F4E4EFF919090FFEEEDEDFF8F8E8FFF919090FF8F8E8EFFE9E8
      E8FFE6E5E5FFECEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFD2D5F2FF2434C1FFA0AD
      F4FF4C63EDFF2D48E9FF314BE9FF334DE9FF334DE9FF314BE9FF2D48E9FF4D64
      EEFFA5B2F6FF3947C7FFC9CDE7FFFEFEFEFFE9E9E9FFB47A08FFFFFFFFFFFFFB
      F1FFFFFAEFFFFFFAEFFFFFFBF0FFFFFFF5FFA19F98FFFFFFF5FFFFFBF0FFFFFA
      EFFFFFFAEFFFFFFAEFFFFFFAEFFFFFFAEFFFFFFAEFFFFFFAEFFFFFFAEFFFFFFA
      EFFFFFFAEFFFFFFBF0FFFFFFF5FFA19F98FFFFFFF5FFFFFBF0FFFFFAEFFFFFFA
      EFFFFFFBF1FFFFFFFFFFB47A08FFE9E9E9FFE9E9E9FFB68116FFF2C987FFEDB9
      64FFDFB876FFACAFB6FFFFFFFFFFEFEDECFFA7A6A4FFFFFFFFFF244585FFC9F2
      FFFFC9F2FFFF254686FFFFFFFFFFA9A7A5FFF3F2F1FF9B9C9CFFBFBFBFFFBDBD
      BDFFF1EFEEFF9C9C9CFFBFBFBFFFBEBEBFFFBDBDBDFFF1F0EFFF9B9B9BFFBDBD
      BDFFEDEBEAFFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFADADABFFFFFF
      FFFFFAF8F9FFFEFCFDFFA6A7A4FFFFFFFFFFF6F6F5FFFAFAF9FFA7A7A5FFFFFF
      FFFFEFF1EFFF109458FF007D34FF007C34FF56AE85FFFFFFFFFF007A2EFF4AAC
      7FFFFFFDFFFFF9F5F5FFFFFFFFFFC8DFD2FF0F9C66FFBCF1F0FF9DE9E6FF007E
      33FFF5F3F2FFFFFFFFFFAEADABFFE9E9E9FFE9E9E9FFB67C09FFF4FAFFFFE5E5
      E6FFE9E8E8FFF1F0F0FFF1F0F0FFEEEDEDFFEDECECFFEDECECFFECEBEBFFE7E6
      E6FFE4E3E3FFDEDDDBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF707BD5FF2E40
      C5FFA5B0F5FF7B8DF0FF3E57E9FF2542E5FF2542E5FF3E57E9FF7C8DF1FFA7B3
      F8FF3B4DCFFF7781D2FFFCFCFCFF00000000E9E9E9FFB47A08FFFFFFFFFFFFF9
      EEFFFEF8ECFFFEF8ECFFFFF9EDFFFFFFF2FFA19D96FFFFFFF2FFFFF9EDFFFEF8
      ECFFFEF8ECFFFEF8ECFFFEF8ECFFFEF8ECFFFEF8ECFFFEF8ECFFFEF8ECFFFEF8
      ECFFFEF8ECFFFFF9EDFFFFFFF2FFA19D96FFFFFFF2FFFFF9EDFFFEF8ECFFFEF8
      ECFFFFF9EEFFFFFFFFFFB47A08FFE9E9E9FFE9E9E9FFB68116FFF2C989FFEBB7
      60FFDDB572FFACB0B7FFFFFFFFFFECEBEAFFA3A2A2FFFFFFFFFF6D84AFFF2547
      86FF254786FF6E85AFFFFFFFFFFFA4A3A2FFF0EFEEFFF0EFEEFFEFEEEDFFEEED
      ECFFEFEEEDFFF0F0EFFFF0F0F0FFEFEFEFFFEFEEEFFFEFEFF0FFF1F0F0FFEEEE
      EDFFEAE9E8FFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFADADABFFFFFF
      FFFFF7F7F8FFFBFBFCFFA7A7A4FFFFFFFFFFF5F4F4FFF9F8F8FFA7A7A5FFFFFF
      FFFFF5F0F1FF007D34FF10EAABFF00E5A1FF009D55FFCCDDD3FFFFFDFFFFFFFB
      FFFFFAF3F6FF139459FF007C32FFFFFFFFFF50AD82FF007D31FF007E32FF0C93
      56FFEEEEEEFFFFFFFFFFAEADABFFE9E9E9FFE9E9E9FFB67C09FFF4FAFFFFE2E2
      E4FFE7E6E6FF4D4D4DFF919090FF8F8E8EFF8D8C8CFFE9E8E8FF898888FFE5E4
      E4FFE3E2E3FFD1D0CDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF828C
      DBFF2334C2FF7280E1FFA3AEF4FFAEB9F8FFAEB9F8FFA3AEF4FF7281E3FF283C
      CDFF6C5A6FFFE9E9E9FF0000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF8
      EAFFFEF7E9FFFEF7E9FFFFF8EAFFFFFEEFFFA19C94FFFFFEEFFFFFF8EAFFFEF7
      E9FFFEF7E9FFFEF7E9FFFEF7EAFFFFF8EBFFFFF8EBFFFEF7EAFFFEF7E9FFFEF7
      E9FFFEF7E9FFFFF8EAFFFFFEEFFFA19C94FFFFFEEFFFFFF8EAFFFEF7E9FFFEF7
      E9FFFFF8EAFFFFFFFFFFB47A08FFE9E9E9FFE9E9E9FFB68116FFF2CB8BFFEAB3
      5AFFDEB36FFFACB0B7FFFFFFFFFFE8E7E6FFBBBAB9FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFBBBABAFFECEBEAFF9B9B9CFFBDBCBCFFECEB
      EAFF9D9E9FFFBFC0C5FFC0C2CAFFBFC3CDFFEFF3FDFF9EA4AFFFC1C5CEFFBEC0
      C6FFE8E8E9FFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFADADABFFFFFF
      FFFFF6F6F5FFFAFAF9FFA7A7A4FFFFFFFFFFF3F3F3FFF7F7F7FFA7A7A5FFFFFF
      FFFFFFF7FEFF007C34FF3AE6B5FF08DEA1FF00BE7CFF49A477FFCADCD2FFC6DC
      D0FFCFDFD7FF00813BFF00E0A2FF007D34FFFFFEFFFFFFFBFFFFFFF8FEFFFFF5
      FAFFF6F1F3FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C09FFF5FBFFFFE1DF
      E1FFE4E1E1FFE7E5E5FFE8E6E6FFE7E5E5FFE6E4E4FFE4E2E2FFE4E2E2FFE3E1
      E1FFE3E1E1FFC5C3C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFD9DCF8FF6773D7FF1527BFFF1729C0FF1729C0FF1427BFFF5964CAFFCBD3
      F7FFC58701FFE9E9E9FF0000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF9
      E9FFFFF8E8FFFFF8E8FFFFF9E9FFFFFFEEFFA19D93FFFFFFEEFFFFF9E9FFFFF8
      E8FFFFF9E8FFFFF9E9FFFFFBEDFFFFFFF2FFFFFFF2FFFFFBEDFFFFF9E9FFFFF9
      E8FFFFF8E8FFFFF9E9FFFFFFEEFFA19D93FFFFFFEEFFFFF9E9FFFFF8E8FFFFF8
      E8FFFFF9E9FFFFFFFFFFB47A08FFE9E9E9FFE9E9E9FFB68116FFF3CC8EFFE9B1
      54FFDCB16AFFACB0B8FFFFFFFFFFE4E3E1FFE5E4E3FFE4E3E2FFE4E3E1FFE5E4
      E1FFE5E4E1FFE4E3E1FFE4E3E2FFE6E5E4FFE7E6E5FFE8E7E6FFE8E7E6FFE7E7
      E8FFEAECF1FFDDD2BBFFC59E50FFB37901FFB37903FFB57B05FFB57A02FFC8A4
      5BFFE6E8EDFFFFFFFFFFB0B0B0FFE9E9E9FF00000000E9E9E9FFADADABFFFFFF
      FFFFF5F4F4FFF9F8F8FFA7A7A5FFFFFFFFFFF2F2F1FFF6F6F5FFA7A7A5FFFFFF
      FFFFFFF5F9FF028C4CFF47CBA1FF32E0B3FF00D49AFF00C082FF009856FF0094
      52FF008642FF00813EFF00D7A1FF00D69EFF007B32FFFFF9FFFFFDF3F5FFF3F0
      F0FFF0EEEDFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C09FFF5FBFFFFDEDE
      DFFFDFDEDDFFE0DFDEFFE0E0DEFFE0DFDEFFE0DFDEFFE0DFDEFFE0DFDDFFE0DF
      DDFFE1E0DFFFB2B2B3FFB1B2B2FFB0B1B1FFB0B0B0FFB0B0B0FFB1B1B1FFB3B3
      B2FFB9B8B3FFBEBDB5FFC3C1B6FFC4C3B6FFC4C2B6FFC1C0B4FFECEBE2FFFCFF
      FFFFB97F08FFE9E9E9FF0000000000000000E9E9E9FFB47B08FFFFFFFFFFFFFC
      EAFFFFFCEBFFFFFCEBFFFFFEECFFFFFFF0FFA4A095FFFFFFF0FFFFFEECFFFFFD
      EBFFFFFDECFFFFFFF0FFFFFFF9FFAD712AFFAD712AFFFFFFF9FFFFFFF0FFFFFD
      ECFFFFFDEBFFFFFEECFFFFFFF0FFA4A095FFFFFFF0FFFFFEECFFFFFCEBFFFFFC
      EBFFFFFCEAFFFFFFFFFFB47B08FFE9E9E9FFE9E9E9FFB68115FFF2CE92FFE8AE
      4EFFDBAF67FFACB1B9FFFFFFFFFFE0DFDEFFE1E0DFFFE1E0DFFFE1E0DEFFE1E0
      DEFFE1E0DEFFE1E0DEFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE2E1E0FFE3E4
      E8FFD5C6A6FFBD8413FFE6B563FFF2C67DFFF3C77EFFF5C981FFF6C97FFFB275
      00FFE3E7EEFFFFFFFFFFB0B0B0FFEAEAEAFF00000000E9E9E9FFADADABFFFFFF
      FFFFF3F3F3FFF7F7F7FFA7A7A5FFFFFFFFFFF1EFEFFFF5F4F4FFA7A7A5FFFFFF
      FFFFFAF2F4FF71B997FF1B9D64FF7EEAD1FF0ED3A3FF00CF99FF00D19BFF00D2
      9DFF00D09EFF00D09EFF00CE9CFF00CE9CFF00D09CFF007A2FFFFFF6FCFFF4EF
      F1FFEDEDECFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C09FFF5FBFFFFDCDC
      DDFFDDDCDBFFDDDCDBFFDDDCDBFFDDDCDBFFDDDCDBFFDDDCDBFFDDDCDBFFDDDC
      DBFFDEDDDCFFDFDEDDFFDFDEDDFFDFDEDDFFDFDEDDFFDFDEDDFFDFDEDDFFDFDE
      DDFFE0DFDDFFE1E0DEFFE2E1DEFFE2E1DEFFE2E1DEFFE1E0DDFFDFDEDDFFF6FC
      FFFFB67D09FFE9E9E9FF0000000000000000E9E9E9FFB47B08FFFFFFFFFF9E99
      8DFFA09B8FFFA19B90FFA19C90FFA49F93FFA8A296FFA49F93FFA19C90FFA19C
      91FFA39F96FFA4A49FFFB07530FFF6C478FFF6C478FFB07530FFA4A49FFFA39F
      96FFA19C91FFA19C90FFA49F93FFA8A296FFA49F93FFA19C90FFA19B90FFA09B
      8FFF9E998DFFFFFFFFFFB47B08FFE9E9E9FFE9E9E9FFB68115FFF3CF95FFE7AB
      49FFD9AF65FFADB3BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFBC8821FFEBB964FFEDBB6BFFEDBB6AFFE3B059FFB17804FFB37700FFD6B1
      68FFFFFFFFFFFFFFFFFFB2B2B2FFF2F2F2FF00000000E9E9E9FFADADABFFFFFF
      FFFFF2F2F1FFF6F6F5FFA7A7A5FFFFFFFFFFF0EEEDFFF4F3F2FFA7A8A5FFFFFF
      FFFFF2EDEEFFEFEBEDFF00853FFF5BB991FF8BEAD5FF4DDCBDFF1AD1A8FF1ED1
      A8FF00CEA3FF00CEA3FF00C698FF00C89AFF00CA9CFF50E2C5FF007829FFF8EF
      F3FFEDEBEBFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C09FFF5FBFFFFDADA
      DAFFDBDAD9FFDCDBDAFFDCDBDAFFDCDBDAFFDBDBDAFFDCDBDAFFDCDBDAFFDBDA
      D9FFDAD9D8FFD9D8D7FFD8D7D6FFD8D7D5FFD8D7D5FFD8D7D5FFD8D7D5FFD8D7
      D5FFD8D7D5FFD8D7D5FFD8D7D5FFD8D7D5FFD8D7D6FFD9D8D6FFD9D9D9FFF5FB
      FFFFB67C09FFE9E9E9FF0000000000000000E9E9E9FFB47B08FFFFFFFFFFFFFA
      E5FFFFFAE5FFFFFAE6FFFFFCE7FFFFFFEBFFA49E91FFFFFFEBFFFFFCE8FFFFFF
      EDFFFFFFF8FFB27732FFF0BD71FFECBB72FFECBB72FFF0BD71FFB27732FFFFFF
      F8FFFFFFEDFFFFFCE8FFFFFFEBFFA49E91FFFFFFEBFFFFFCE7FFFFFAE6FFFFFA
      E5FFFFFAE5FFFFFFFFFFB47B08FFE9E9E9FFE9E9E9FFB68115FFF3D099FFE5A9
      46FFE3A94AFFBCB3A4FFACB3BEFFABB1BAFFABB0B9FFABB0B9FFABB0B9FFABB0
      B9FFABB0B9FFABB0B9FFABB0B9FFABB0B9FFABB0B9FFAAAFB6FFA9ADB4FFEEF5
      FFFFB17803FFEBB65DFFE8B35AFFF0C989FFB57C0AFFF0FAFFFFF1F7FFFFB0B4
      BBFFB1B2B5FFB2B2B2FFC7C7C7FFFBFBFBFF00000000E9E9E9FFADADABFFFFFF
      FFFFF1EFEFFFF5F4F4FFA7A7A5FFFFFFFFFFEDEDECFFF1F2F1FFA8A8A5FFFFFF
      FFFFECEAE9FFF6EEEFFFCBDCD4FF00853FFF209D63FF71CDAFFF94E5D4FF98EC
      DDFF4BE9D2FF4CE8D0FF93E7D8FF00C298FF55DDC4FF007A2EFFFFF2F8FFF2EC
      EDFFEBE9E8FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C0AFFF6FBFFFFD9D8
      D9FFDCDAD9FFE0DEDDFFE0DEDDFFDFDDDCFFDDDBDAFFDFDCDBFFDEDCDBFFDCDA
      D8FFD8D6D5FFFBFBFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFD7D5D6FFF5FB
      FFFFB67C0AFFE9E9E9FF0000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF5
      DFFFFFF4DFFFFFF4DFFFFFF5E0FFFFFAE5FFA19C8EFFFFFBE6FFFFF9E6FFFFFF
      F1FFAE742FFFE7B567FFE6B569FFE4B369FFE4B369FFE6B569FFE7B567FFAE74
      2FFFFFFFF1FFFFF9E6FFFFFBE6FFA19C8EFFFFFAE5FFFFF5E0FFFFF4DFFFFFF4
      DFFFFFF5DFFFFFFFFFFFB47A08FFE9E9E9FFE9E9E9FFB68115FFF3D49EFFE3A6
      40FFE4A741FFE5A53BFFE5A130FFE49E29FFE49D26FFE49D26FFE49D26FFE49D
      26FFE49D26FFE49D26FFE49D26FFE49D26FFE49D26FFE59D26FFFFF2D9FFFFF5
      E2FFB27B0BFFE9AF51FFE6AD4DFFF3CD91FFB27907FFB7B7B7FFCDCDCDFFEEEE
      EEFF0000000000000000000000000000000000000000E9E9E9FFADADABFFFFFF
      FFFFF0EEEDFFF4F3F2FFA7A8A5FFFFFFFFFFECEBEBFFF0EFF0FFA8A8A5FFFFFF
      FFFFE8E8E7FFECEAEAFFF4EDEFFFEDEAE9FF6DB895FF008C4BFF007E33FF007E
      34FF008037FF007F37FF7FE4D4FF4FD8C1FF007A2DFFFFF1F6FFF1EBEDFFE9E8
      E8FFE7E7E6FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB67C0AFFF6FBFFFFD8D6
      D7FFDDDBDAFF4A4949FF8A8988FF878685FFDFDDDCFF868584FF858583FFDBD9
      D8FFD6D4D3FFE9E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E8E8FFD5D3D3FFF6FB
      FFFFB67C0AFFE9E9E9FF0000000000000000E9E9E9FFB47A08FFFFFFFFFFFFF4
      DCFFFEF3DBFFFEF3DCFFFFF4DDFFFFF9E2FFA19B8DFFFFFDE7FFFFFFEDFFAD72
      2CFFE1AB5CFFE0AC60FFDFAD62FFDFAD63FFDFAD63FFDFAD62FFE0AC60FFE1AB
      5CFFAD722CFFFFFFEDFFFFFDE7FFA19B8DFFFFF9E2FFFFF4DDFFFEF3DCFFFEF3
      DBFFFFF4DCFFFFFFFFFFB47A08FFE9E9E9FFE9E9E9FFB68114FFF5D5A2FFE2A3
      3CFFE2A53FFFE1A136FFF4D9B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB07500FFB27A
      09FFB27E12FFE5A843FFE2A53FFFEEC37DFFB27C0EFFB47B09FFB67D08FFEEEE
      EEFF0000000000000000000000000000000000000000E9E9E9FFADADABFFFFFF
      FFFFEDEDECFFF1F2F1FFA8A8A5FFFFFFFFFFEBEAE9FFEFEEEDFFA8A8A6FFFFFF
      FFFFE7E6E4FFE8E7E6FFEAE8E7FFEEE9EAFFF5ECEEFFFBEEF2FFFEEFF4FFFFF1
      F7FFC1D7CBFF008137FF6BDFD2FF007A2DFFFFFAFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAEAEABFFE9E9E9FFE9E9E9FFB67D0AFFF6FCFFFFD5D4
      D6FFDBD9D9FFE2E0E0FFE3E1E1FFE1DFDFFFDFDDDDFFE0DDDEFFDEDCDCFFD9D7
      D7FFD5D3D3FFD8D7D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D6D5FFD3D2D4FFF6FB
      FFFFB67D0AFFE9E9E9FF0000000000000000E9E9E9FFB47A08FFFFFFFFFFFEF3
      DAFFFDF2D9FFFDF2DAFFFEF3DBFFFFF8E0FFA29B8CFFFFFFEAFFAF732BFFF0D4
      ABFFEFD4ACFFE5C28DFFD8A458FFD9A65BFFD9A65BFFD8A458FFE5C28DFFEFD4
      ACFFF0D4ABFFAF732BFFFFFFEAFFA29B8CFFFFF8E0FFFEF3DBFFFDF2DAFFFDF2
      D9FFFEF3DAFFFFFFFFFFB47A08FFE9E9E9FFE9E9E9FFB68114FFF5D8A7FFE1A0
      36FFE0A139FFDF9C2DFFFEFCFFFFFCF3F1FFFCF2EDFFFBF1ECFFFCF2EDFFFCF2
      EDFFFBF1ECFFFBF1EDFFFCF2EDFFFCF2EDFFFCF3EFFFFFF8FBFFAE7300FFF9DE
      B2FFE39E31FFE19F35FFE09F34FFE09E32FFE39E2FFFFDE2BAFFB88216FFF9F9
      F9FF0000000000000000000000000000000000000000E9E9E9FFADADABFFFFFF
      FFFFECEBEBFFF0EFF0FFA8A8A5FFFFFFFFFFE9E9E8FFEDEDECFFA8A8A6FFFFFF
      FFFFE6E4E3FFE7E6E5FFE7E6E5FFE8E6E5FFE9E7E6FFEAE7E7FFEBE8E8FFEEE9
      EAFFFAEDF1FF45A77AFF007C2EFFFFEDF3FFFFFFFFFFCDCCCBFFA7A7A5FFA7A7
      A4FFA5A5A2FFFFFFFFFFAFAFADFFEFEFEFFFE9E9E9FFB67D0AFFF6FCFFFFD3D3
      D4FFD9D8D7FF494848FF888787FF868586FF868585FF868585FF838282FFD7D6
      D5FFD4D4D2FFC7C6C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C6C5FFD2D2D2FFF6FC
      FFFFB67D0AFFE9E9E9FF0000000000000000E9E9E9FFB47B09FFFFFFFFFFFFF3
      D9FFFEF2D9FFFEF3DAFFFFF4DBFFFFF9DFFFA29B8CFFFFFFE9FFB0752EFFB279
      36FFB27A37FFE9CDA2FFD29D50FFD3A054FFD3A054FFD29D50FFE9CDA2FFB27A
      37FFB27936FFB0752EFFFFFFE9FFA29B8CFFFFF9DFFFFFF4DBFFFEF3DAFFFEF2
      D9FFFFF3D9FFFFFFFFFFB47B09FFE9E9E9FFE9E9E9FFB68014FFF6DAAEFFDF9E
      30FFDF9F33FFDE9926FFFFFDFFFFFFF7F1FFFFF5EDFFFEF4EDFFFFF6EEFFFFF5
      EEFFFEF4EDFFFEF4ECFFFFF6EEFFFFF6EEFFFFF5EEFFFFF7F3FFFFFFFFFFAE74
      00FFEFC582FFDE9722FFDD9824FFDE9722FFFAE2BCFFB58014FFF9F9F9FF0000
      00000000000000000000000000000000000000000000E9E9E9FFADADABFFFFFF
      FFFFEBEAE9FFEFEEEDFFA8A8A6FFFFFFFFFFE8E7E5FFECEBE9FFA8A8A6FFFFFF
      FFFFE3E2E2FFE5E4E4FFE5E4E4FFE5E4E4FFE5E4E4FFE5E4E4FFE5E4E4FFE6E4
      E5FFEAE6E7FFF2E9ECFFF3E9EDFFEBE5E7FFFFFFFFFFA7A7A5FFFFFFFFFFF4F2
      F2FFFFFFFFFFE9E9E9FFB5B5B3FFFAFAFAFFE9E9E9FFB67D0AFFF6FCFFFFD0D0
      D1FFD4D3D2FFD8D7D6FFD8D7D6FFD8D7D6FFD8D7D6FFD7D7D5FFD6D5D4FFD4D3
      D2FFD4D3D2FFBAB7B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B7B4FFD1D1D2FFF7FC
      FFFFB67D0AFFE9E9E9FF0000000000000000E9E9E9FFB67E0EFFFFFFFFFFFFFB
      E8FFFFFAE7FFFFFBE8FFFFFCE9FFFFFFECFFA4A097FFFFFFF0FFFFFFF4FFFFFF
      FBFFB37B39FFE6CAA1FFCC9547FFCD974CFFCD974CFFCC9547FFE6CAA1FFB37B
      39FFFFFFFBFFFFFFF4FFFFFFF0FFA4A097FFFFFFECFFFFFCE9FFFFFBE8FFFFFA
      E7FFFFFBE8FFFFFFFFFFB67E0EFFE9E9E9FFE9E9E9FFB68014FFF8DDB3FFDE9B
      2AFFDE9D2EFFDD9720FFFFFFFFFF474444FFA8A29CFFFFF8F1FF474442FFAAA3
      9DFFA6A09AFFFFF7F0FF474442FFABA59FFFA9A29DFFA5A09BFFFFFCFFFFFCEE
      D7FFAE7704FFF4D8AAFFDB8F11FFF6DAADFFB47F11FFF9F9F9FF000000000000
      00000000000000000000000000000000000000000000E9E9E9FFADADABFFFFFF
      FFFFE9E9E8FFEDEDECFFA8A8A6FFFFFFFFFFE7E5E4FFEBE9E8FFA8A8A6FFFFFF
      FFFFE2E1E0FFE3E2E1FFE4E3E1FFE4E3E1FFE4E3E1FFE4E3E1FFE4E3E1FFE4E3
      E2FFE4E3E2FFE6E3E3FFE6E3E2FFE3E1E0FFFFFFFFFFA7A7A4FFF4F2F3FFFFFF
      FFFFE7E7E7FFB5B5B2FFFAFAFAFF00000000E9E9E9FFB67D0AFFF6FCFFFFCECD
      CEFFD1CFCEFFD2CFCEFFD2D0CFFFD2CFCEFFD2CFCEFFD2CFCEFFD1CFCEFFD1CF
      CEFFD3D0CFFFA3A3A4FFA2A2A2FFA0A1A1FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0
      A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A1A1FFA2A2A2FFA2A3A3FFD0CFCFFFF7FC
      FFFFB67D0AFFE9E9E9FF0000000000000000E9E9E9FFB88114FFB27804FFAF74
      00FFAF7400FFAF7400FFAF7400FFB17600FFB27700FFB17600FFB07600FFB279
      02FFB57D3BFFE4C9A1FFC58D3BFFC68E3FFFC68E3FFFC58D3BFFE4C9A1FFB57D
      3BFFB27902FFB07600FFB17600FFB27700FFB17600FFAF7400FFAF7400FFAF74
      00FFAF7400FFB27804FFB88114FFE9E9E9FFE9E9E9FFB68013FFF8E0B9FFDD98
      24FFDE9A29FFDC941BFFFFFFFFFFFFFBF4FFFFFAF1FFFFF8F0FFFFFAF2FFFFFA
      F1FFFFF7EFFFFFF6EEFFFFF9F1FFFFFDF4FFFFFCF3FFFFF8F0FFFFFDFFFFDB8F
      0EFFFEF2E2FFAF7806FFF7DBADFFB27A09FFE9E9E9FF00000000000000000000
      00000000000000000000000000000000000000000000E9E9E9FFADAEABFFFFFF
      FFFFE8E7E5FFECEBE9FFA8A8A6FFFFFFFFFFE4E3E3FFE8E7E7FFA8A9A6FFFFFF
      FFFFE0DFDEFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0
      DFFFE1E0DFFFE1E0DFFFE1E0DFFFE0DFDEFFFFFFFFFFA5A5A2FFFFFFFFFFE7E7
      E7FFB4B4B1FFFAFAFAFF0000000000000000E9E9E9FFB67D0AFFF6FCFFFFCAC9
      CBFFCCCACAFFCCCACAFFCCCACAFFCCCACAFFCCCACAFFCCCACAFFCCCACAFFCCCA
      CAFFCDCBCBFFCECDCCFFCECDCDFFCECDCDFFCECDCDFFCECDCDFFCECDCDFFCECD
      CDFFCECDCDFFCECDCDFFCECDCDFFCECDCDFFCECDCDFFCDCCCCFFCBCACCFFF6FC
      FFFFB67D0AFFE9E9E9FF0000000000000000E9E9E9FFB78012FFF9E9D5FFF3DE
      C0FFF3DEBFFFF3DEBFFFF3DEBFFFF3DEBFFFF3DEC0FFF3DEBFFFF3DEC0FFF6E3
      C4FFB37B38FFE1C79FFFDFC49AFFE0C59BFFE0C59BFFDFC49AFFE1C79FFFB37B
      38FFF6E3C4FFF3DEC0FFF3DEBFFFF3DEC0FFF3DEBFFFF3DEBFFFF3DEBFFFF3DE
      BFFFF3DEC0FFF9E9D5FFB78012FFE9E9E9FFE9E9E9FFB68013FFF9E4C1FFDE97
      20FFE09A25FFDD9417FFFFFFFFFF474544FFAAA29DFFA8A09BFFA7A09BFFA7A0
      9BFFA79F9BFFA59D99FFFFF6EEFF4A4745FF4B4947FFA7A09DFFFFFFFFFFDC91
      11FFE0961AFFFFF5E3FFB07804FFF3ECDBFFE9E9E9FF00000000000000000000
      00000000000000000000000000000000000000000000E9E9E9FFAEAEABFFFFFF
      FFFFE7E5E4FFEBE9E8FFA8A8A6FFFFFFFFFFE3E1E0FFE7E6E5FFA9AAA7FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9FFB4B4
      B2FFFAFAFAFF000000000000000000000000E9E9E9FFB67D0CFFF4FDFFFFF2F7
      FFFFF2F8FFFFF3F8FFFFF3F8FFFFF3F8FFFFF3F8FFFFF3F8FFFFF3F8FFFFF3F8
      FFFFF3F8FFFFF3F9FFFFF3F9FFFFF3F9FFFFF3F9FFFFF3F9FFFFF3F9FFFFF3F9
      FFFFF3F9FFFFF3F9FFFFF3F9FFFFF3F9FFFFF3F9FFFFF3F8FFFFF2F7FFFFF4FD
      FFFFB67D0CFFE9E9E9FF0000000000000000E9E9E9FFB67F10FFF7E6CFFFECD0
      A5FFECCFA4FFECD0A5FFECD0A5FFECD0A5FFECD0A5FFECD0A5FFECD0A5FFEED4
      AAFFCA9C62FFB37B37FFB37C39FFB37C39FFB37C39FFB37C39FFB37B37FFCA9C
      62FFEED4AAFFECD0A5FFECD0A5FFECD0A5FFECD0A5FFECD0A5FFECD0A5FFECCF
      A4FFECD0A5FFF7E6CFFFB67F10FFE9E9E9FFE9E9E9FFB68012FFFBE7C8FFE095
      19FF6C470BFFDF9211FFFFFFFFFFFFF8F0FFFFF8EEFFFFF5ECFFFFF4EBFFFFF4
      EBFFFFF7EDFFFFF5ECFFFEF3EAFFFFF7EEFFFFFAF0FFFFF5EEFFFFFFFFFFDF91
      0FFF6C4508FFE09415FFFBE7C6FFB47C0BFFE9E9E9FF00000000000000000000
      00000000000000000000000000000000000000000000E9E9E9FFAEAEABFFFFFF
      FFFFE4E3E3FFE8E7E7FFA8A9A6FFFFFFFFFFE1DFDEFFE4E3E2FFB4B4B2FFA9A9
      A7FFA9A9A6FFA9A9A6FFA9A9A6FFA9A9A6FFA9A9A6FFA9A9A6FFA9A9A6FFA9A9
      A6FFA9A9A6FFAAAAA7FFABABA9FFADADABFFAEAEABFFAFAFADFFB5B5B4FFFAFA
      FAFF00000000000000000000000000000000E9E9E9FFB67F0FFFFAE3C2FFE3AB
      52FFE3AC55FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD
      56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD
      56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AD56FFE3AC55FFE3AB52FFFAE3
      C2FFB67F0FFFE9E9E9FF0000000000000000E9E9E9FFB67F0FFFF6E6CFFFE8C5
      91FFE8C692FFE8C693FFE8C693FFE8C693FFE8C693FFE8C693FFE8C693FFE9C8
      95FFEBCA98FFECCC9BFFEDCD9BFFEDCD9CFFEDCD9CFFEDCD9BFFECCC9BFFEBCA
      98FFE9C895FFE8C693FFE8C693FFE8C693FFE8C693FFE8C693FFE8C693FFE8C6
      92FFE8C591FFF6E6CFFFB67F0FFFE9E9E9FFE9E9E9FFB57F12FFFCEACFFFDE92
      12FF654006FFDE8F0AFFFFFFFFFF444241FFA7A099FFA49D97FFA29C95FFFDF2
      E9FF454240FFA69F98FFA29C95FFFDF3E9FF45423FFFA39D98FFFFFFFFFFDE8F
      0AFF654006FFDE9212FFFCEACFFFB57F11FFE9E9E9FF00000000000000000000
      00000000000000000000000000000000000000000000E9E9E9FFAEAEABFFFFFF
      FFFFE3E1E0FFE7E5E5FFA9A9A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6
      F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F6FFF7F7F6FFF7F7
      F7FFF8F8F7FFF9F9F8FFD6D6D5FFC6C6C5FFFCFCFCFF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB68012FFF5DDB8FFD99E
      39FFDAA13FFFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA1
      40FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA1
      40FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA13FFFD99E39FFF5DD
      B8FFB68012FFE9E9E9FF0000000000000000E9E9E9FFB67F0FFFF7E7D0FFE5BC
      81FFE5BD83FFE5BD84FFE5BD84FFE5BD84FFE5BD84FFE5BD84FFE5BD84FFE5BE
      84FFE5BE85FFE5BE85FFE6BE85FFE6BE85FFE6BE85FFE6BE85FFE5BE85FFE5BE
      85FFE5BE84FFE5BD84FFE5BD84FFE5BD84FFE5BD84FFE5BD84FFE5BD84FFE5BD
      83FFE5BC81FFF7E7D0FFB67F0FFFE9E9E9FFEAEAEAFFB57F12FFFDEDD5FFD989
      04FFDC8C09FFD88500FFFFFFFFFFF9EFE7FFF9EEE5FFF8EEE4FFF7EDE3FFF7ED
      E3FFF9EFE6FFF9EFE6FFF7EDE4FFF7EDE3FFF9EFE5FFF7EEE6FFFFFFFFFFD885
      00FFDC8C09FFD98904FFFDEDD5FFB57F12FFEAEAEAFF00000000000000000000
      00000000000000000000000000000000000000000000EAEAEAFFAEAEACFFFFFF
      FFFFE1DFDEFFE3E2E1FFBFBFBDFFA8A8A5FFA7A7A5FFA7A7A5FFA8A8A6FFA9A9
      A7FFA9A9A7FFAAAAA7FFAAAAA7FFAAAAA7FFAAAAA7FFAAAAA8FFACACAAFFAFAF
      ADFFB0B0AEFFB1B1AFFFC7C7C5FFFBFBFBFF0000000000000000000000000000
      000000000000000000000000000000000000EAEAEAFFB68114FFF1D6A9FFD18B
      17FFD28E1BFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E
      1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E
      1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1BFFD18B17FFF1D6
      A9FFB68114FFEAEAEAFF0000000000000000EAEAEAFFB67F10FFF7E8D1FFDFB1
      6DFFDFB26FFFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFE0B3
      70FFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFE0B3
      70FFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFE0B370FFDFB2
      6FFFDFB16DFFF7E8D1FFB67F10FFEAEAEAFFF2F2F2FFB78115FFFFEBCEFFFDEA
      CCFFFDEACDFFFCE7C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7
      C6FFFDEACDFFFDEACCFFFFEBCEFFB78115FFF0F0F0FF00000000000000000000
      00000000000000000000000000000000000000000000F2F2F2FFAFAFADFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECECEDFFC1C1
      BFFFFBFBFBFF0000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F2F2F2FFB88217FFEECF9BFFECCD
      98FFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE
      9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE
      9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCD98FFEECF
      9BFFB88217FFF2F2F2FF0000000000000000F2F2F2FFB78114FFFAEBD8FFF7E8
      D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8
      D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8
      D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8D1FFF7E8
      D1FFF7E8D1FFFAEBD8FFB78114FFF2F2F2FFFBFBFBFFCAA65DFFB78115FFB67F
      12FFB57F12FFB57E0FFFB57C0BFFB57C09FFB57B09FFB57C09FFB57C09FFB57C
      09FFB57C09FFB57C09FFB57C09FFB57C09FFB57C09FFB57C09FFB57C0CFFB57E
      0FFFB57F12FFB67F12FFB78115FFC1943AFFFAFAFAFF00000000000000000000
      00000000000000000000000000000000000000000000FBFBFBFFC6C6C5FFB0B0
      ADFFAEAEACFFAEAEACFFAEAEACFFAEAEACFFAFAFACFFAFAFACFFAFAFACFFAFAF
      ACFFAFAFACFFAFAFACFFAFAFACFFAFAFACFFAFAFACFFAFAFADFFC6C6C5FFFBFB
      FBFF000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FBFBFBFFCBA65EFFB88317FFB782
      16FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB781
      16FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB781
      16FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB78216FFB883
      17FFCBA65EFFFBFBFBFF0000000000000000FBFBFBFFCAA65CFFB78114FFB67F
      10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F
      10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F
      10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F10FFB67F
      10FFB67F10FFB78114FFCAA65CFFFBFBFBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F9F9F9FFEFEFEFFFEFEFEFFFFAFAFAFF0000
      000000000000000000000000000000000000FAFAFAFFEFEFEFFFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFEFEFEFFFFAFA
      FAFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFF9F9F9FFEEEEEEFFEEEEEEFFF9F9F9FF0000
      00FF000000FF000000FF000000FF000000FFFAFAFAFFF0F0F0FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFEEEEEEFFF7F7F7FFFAFAFAFFF4F4
      F4FFECECECFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFEAEAEAFFF0F0F0FFFAFAFAFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EEEEEEFFCECECEFFCACACAFFE5E5E5FFFAFA
      FAFF00000000000000000000000000000000EFEFEFFFCECECEFFBDBDBDFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBDBDBDFFCECECEFFEFEF
      EFFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFEEEEEEFFCDCDCDFFC8C8C8FFE3E3E3FFF9F9
      F9FF000000FF000000FF000000FF000000FFF0F0F0FFD0D0D0FFBEBEBEFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFC5C5C5FFD8D8D8FFDFDFDFFFD3D3
      D3FFC3C3C3FFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBEBEBEFFD0D0D0FFF0F0F0FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E9E9E9FF008E4EFF0E8F53FFC6C6C6FFE8E8
      E8FFFCFCFCFF000000000000000000000000E9E9E9FF50AD8EFF48AA89FF48AA
      89FF47AA89FF47AA89FF47AA89FF47AA89FF47AA89FF47AA89FF47AA89FF47AA
      89FF47AA89FF47AA89FF47AA89FF47AA89FF48AA89FF48AA89FF50AD8EFFE9E9
      E9FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFD8D8D8FFBA8546FFB88445FFC2C2C2FFE3E3
      E3FFF9F9F9FF000000FF000000FF000000FFEAEAEAFFBA8D34FFB78218FFB681
      16FFB58013FFB37A06FFD7CFCFFFD5CBC5FFDAD1D0FFB5AB97FFB8AD98FFB59C
      6AFFD9D1CFFFD5CAC2FFD4C9C0FFD4C9C0FFD4C9C0FFD5CAC2FFD7CFCFFFB37A
      06FFB58013FFB68116FFB78218FFBA8D34FFEAEAEAFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F9F9F9FFEEEEEEFFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFD8D8D8FF008B4BFF00C786FF138D53FFCCCC
      CCFFEBEBEBFFFCFCFCFF0000000000000000E9E9E9FF48AA89FF61E6BEFF5EE8
      BEFF5DE9BFFF5DEAC0FF5DEAC0FF5DEAC0FF5DEAC0FF5DEAC0FF5DEAC0FF5DEA
      C0FF5DEAC0FF5DEAC0FF5DEAC0FF5DE9BFFF5EE8BEFF61E6BEFF48AB89FFE9E9
      E9FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFF2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFB7B7B7FFB78344FFFBCC81FFB58142FFC2C2
      C2FFE3E3E3FFF9F9F9FF000000FF000000FFE9E9E9FFB78218FFF6CD8BFFF3C8
      82FFF2C77FFFF1C274FFF4F6F4FFF3F1E8FFFFFCF4FF575961FF8E8D90FF8888
      88FFFCFAF2FFF2F0E6FFF2EEE4FFF2EEE4FFF2EEE4FFF2EFE6FFF4F6F4FFF1C2
      74FFF2C77FFFF3C882FFF6CD8BFFB78218FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000FBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFDDDDDDFFC8C8C8FFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFB7B7B7FF008848FF00E5A6FF00C080FF3E96
      6CFFCDCDCDFFEBEBEBFFFCFCFCFF00000000E9E9E9FF48A989FF5EE8BEFF856B
      71FF827174FF827275FF827275FF827275FF827275FF827275FF827275FF8272
      75FF827275FF827275FF827275FF827174FF856B71FF5EE8BEFF47AC85FFD8D8
      D8FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFEFEFEFFFFAFAFAFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFEAEAEAFFBA9669FFB78242FFB682
      42FFB78243FFB78244FFB78244FFB68243FFB37F41FFF0BF75FFF1C176FFB581
      42FFC2C2C2FFE3E3E3FFF9F9F9FF000000FFE9E9E9FFB68116FFF3C985FFEEBF
      74FFEDBE71FFECBA67FFF2F3F1FFF0ECE1FFFFFBF3FF565455FF8E8A84FF8682
      7CFFFCF8F1FFF0EBDFFFEEEADEFFEEEADEFFEEEADEFFEEEAE0FFF2F3F1FFECBA
      67FFEDBE71FFEEBF74FFF3C985FFB68116FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000F2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFB7B7B7FF008949FF008847FF008847FF008847FF0088
      47FF008847FF008847FF008847FF008746FF008342FF00DCA1FF00DBA0FF00BC
      81FF3E986CFFCDCDCDFFEBEBEBFFFBFBFBFFE9E9E9FF47AA89FF5EE9BFFF7F6D
      71FF7C7576FF7B7677FF7B7677FF7B7677FF7B7677FF7B7677FF7B7677FF7B76
      77FF7B7677FF7B7677FF7B7677FF7C7576FF7F6D71FF5DEBBEFF47B081FFB7B7
      B7FFBCBCBCFFBCBCBCFFBCBCBCFFBDBDBDFFCECECEFFEFEFEFFF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE9E9E9FFB78140FFF9E2BDFFEAB7
      6BFFEAB86DFFEAB86FFFEAB86FFFEAB86EFFE9B76DFFE7B56CFFE7B56CFFEBB9
      6FFFB68243FFC2C2C2FFE3E3E3FFF9F9F9FFE9E9E9FFB68116FFF1C986FFECBC
      6EFFEBBB6CFFEAB762FFF3F3F2FFEEE8DEFFFFFDF5FF525151FF8D877FFF847E
      77FFFEF9F3FFEEE7DCFFEDE6DCFFEDE6DCFFEDE6DCFFEDE6DDFFF3F3F2FFEAB7
      62FFEBBB6CFFECBC6EFFF1C986FFB68116FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000EAEAEAFFBA974CFFB67E0EFFB47B
      09FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A07FFB47A
      07FFB77B08FFC97C08FF008746FF35E8BDFF00DAA1FF00DAA2FF00DAA2FF00DA
      A2FF00DAA2FF00DAA2FF00DAA2FF00D9A2FF00D8A0FF00D39CFF00D39CFF00D6
      A0FF00BA80FF3E986CFFD1D1D1FFF2F2F2FFE9E9E9FF47A989FF5EE8BFFF7A71
      72FF7A7272FF797575FF797675FF797675FF797675FF797675FF797675FF7976
      75FF797675FF797675FF797575FF7A7272FF7A7172FF5DE9BDFF45AF7EFF4B4F
      FBFF4956F3FF4957F0FF4957F0FF4A58F0FF5361EDFFE9E9E9FF000000FF0000
      00FF000000FF000000FF000000FF000000FFFBFBFBFFF2F2F2FFEAEAEAFFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFD8D8D8FFB67F3BFFF4DBB4FFE0AD
      60FFE0AE64FFE1AF65FFE1AF65FFE1AF65FFE0AF64FFE0AE64FFE0AE64FFE2B0
      65FFE5B367FFB68141FFC8C8C8FFEEEEEEFFE9E9E9FFB68116FFF1C988FFEBB9
      67FFEAB865FFE9B45BFFF5F4F5FFEDE5DAFFFFFFF8FF4D4C4CFF8A837CFF807B
      73FFFFFDF6FFEDE4D9FFECE4D9FFECE4D9FFECE4D9FFEBE4DAFFF5F4F6FFE9B4
      5BFFEAB865FFEBB967FFF1C988FFB68116FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB67E0EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF007F39FF4FE4C2FF00CF9AFF00CF9BFF00CF9BFF00CF
      9BFF00CF9BFF00CF9BFF00CF9BFF00CF9BFF00CF9BFF00CD9AFF00CD9AFF00CE
      9BFF00D29FFF00B881FF3F9C70FFEDEDEDFFE9E9E9FF48A988FF5FE5BDFF67B3
      9AFF7A6A6DFF787171FF777372FF777473FF777473FF777473FF777473FF7774
      73FF777473FF777372FF787171FF7A6A6DFF67B39AFF5EE6BCFF43AD80FF9CB4
      FFFF98B8FFFF98B8FFFF98B8FFFF9BBAFFFF4656F4FFD8D8D8FFE9E9E9FFE9E9
      E9FFE9E9E9FFE9E9E9FFEEEEEEFFF9F9F9FFF2F2F2FFD4D4D4FFC0C0C0FFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFBCBCBCFFBCBCBCFFB7B7B7FFB67C36FFEFD5AEFFDAA5
      59FFDBA75DFFDBA85EFFDBA85EFFDBA85EFFDBA85EFFDBA85EFFDBA85EFFDBA8
      5DFFDCA75BFFF2DAB4FFB88241FFEEEEEEFFE9E9E9FFB68116FFF1C98AFFE9B7
      63FFE9B661FFE8B156FFF7F6F9FFECE2D6FFFFFFFCFF464344FF857E77FF7C75
      6EFFFFFFFAFFEDE2D6FFECE2D6FFECE2D6FFECE2D6FFEBE2D6FFF7F6F9FFE8B1
      56FFE9B661FFE9B763FFF1C98AFFB68114FFE9E9E9FF00000000000000000000
      000000000000000000000000000000000000E9E9E9FFB47B09FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF007C33FF68E4CBFF00C897FF00C898FF00C899FF00C8
      99FF00C899FF00C899FF00C899FF00C899FF00C999FF00C999FF00C99AFF00C9
      99FF07CDA1FF2CDCB7FF008A48FFF2F2F2FFE9E9E9FF48A888FF60E2BBFF5CDC
      B3FF65B198FF76666AFF756C6EFF74706FFF747271FF747271FF747271FF7472
      71FF74706FFF756C6EFF76666AFF65B198FF5CDCB3FF5FE3BBFF42AD86FF8271
      6DFF7D766DFF7C766BFF7C7462FF96B7FFFF3E51FAFFB7B7B7FFBCBCBCFFBCBC
      BCFFBCBCBCFFBCBCBCFFCDCDCDFFEEEEEEFFEAEAEAFFB6B6B5FFAFAFADFFAEAE
      ABFFADADABFFADADABFFADADABFFADADABFFADADABFFADADABFFADADABFFADAD
      ABFFADADABFFADADABFFADADABFFAEAFAEFFAEB3B8FFB57A32FFECD1AAFFD59F
      50FFD5A055FFD6A156FFD6A156FFD6A156FFD6A156FFD6A256FFD6A256FFD5A0
      54FFD59F51FFEED6AFFFB88241FFF9F9F9FFE9E9E9FFB68116FFF2CB8BFFE8B3
      5DFFE8B35CFFE7AE51FFF9F9FDFFEBDED3FFFCF5EEFFA69D92FF49494AFFA69C
      92FFFBF5EDFFEBDFD2FFEBDED2FFEBDFD3FFEBDED2FFEADED3FFF9F9FEFFE7AE
      51FFE8B35CFFE8B35DFFF2CB8BFFB67F10FFD8D8D8FFE9E9E9FFE9E9E9FFE9E9
      E9FFE9E9E9FFEAEAEAFFF2F2F2FFFBFBFBFFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF007B31FF83E7D6FF00C397FF00C398FF00C399FF00C3
      99FF00C399FF00C399FF00C399FF00C399FF00C398FF00C498FF00C599FF00C6
      9AFF61E0C7FF00B381FF4CAA7DFFFBFBFBFFE9E9E9FF48A888FF60E1BAFF5ED5
      AEFF5CD9B1FF5FCFAAFF6B9284FF746669FF726C6DFF726F6FFF726F6FFF726C
      6DFF74666AFF6B9284FF5FCFAAFF5CD9B1FF5ED5AEFF5FE2BBFF43AE89FF8071
      72FF7B7674FF7A7672FF797366FF96B7FFFF3B4EFCFFDDAC5BFFD2A666FFCFA4
      68FFCFA468FFCFA569FFD0A66CFFE9E9E9FFE9E9E9FFB0B0ADFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2772EFFE8CDA3FFCF97
      46FFCF9949FFCF9A4BFFCF9A4BFFCF9A4BFFCF9A4BFFD09A4CFFD09B4DFFD09A
      4BFFEAD2ABFFB6813FFFF9F9F9FF000000FFE9E9E9FFB68116FFF3CC8EFFE7B1
      58FFE7B157FFE6AD4DFFFCFEFFFFE8DCD1FFEBDDD0FFF0E2D5FFF2E5D7FFEFE2
      D5FFEBDDD0FFE9DBCFFFE9DBCFFFE9DBCFFFE8DBCFFFE8DCD1FFFCFEFFFFE6AD
      4DFFE7B157FFE7B158FFF3CC8CFFB67D09FFB7B7B7FFBCBCBCFFBCBCBCFFBCBC
      BCFFBCBCBCFFC0C0C0FFD4D4D4FFF2F2F2FFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF007A2FFF94EBDFFF49E7D1FF4DE7D2FF4EE7D2FF4EE7
      D2FF4EE7D2FF4EE7D2FF4EE7D2FF4FE7D2FF4CE6D1FF93E6D7FF00BF97FF5DDC
      C6FF00AF80FF51A97FFFFCFCFCFF00000000E9E9E9FF48A888FF60E1BAFF5ED2
      ABFF5ED3ACFF5DD6ADFF5CDBB1FF5FCBA6FF716667FF706C6BFF706C6BFF7166
      68FF5FCBA7FF5CDBB1FF5DD6ADFF5ED3ACFF5ED2ABFF60E2BBFF43AE8AFF7E70
      72FF7A7574FF78746FFF78746DFF96B6FFFF3C4EFBFFFFF2AFFFFFECB8FFFFEA
      B9FFFFE8B8FFFBE7BAFFCFA569FFE9E9E9FFE9E9E9FFAEAEACFFFFFFFFFFFEFD
      FDFFFDFCFCFFFDFCFCFFFDFCFCFFFDFCFCFFFDFCFCFFFDFCFCFFFDFCFCFFFDFC
      FCFFFDFCFCFFFDFCFCFFFDFCFCFFFEFEFFFFFFFFFFFFB0742AFFE7CCA2FFE6CB
      A1FFE6C9A1FFE5C99FFFE3C89DFFE3C79CFFE2C69AFFDBB57FFFC89140FFE7CD
      A5FFB7803EFFF6F6F6FF000000FF000000FFE9E9E9FFB68115FFF2CE92FFE6AF
      52FFE6AF53FFE5AD4DFFE7CBA5FFFCFFFFFFFCFEFFFFFDFEFFFFFDFEFFFFFDFE
      FFFFFCFCFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFCFFFFFCFDFFFFE7CBA5FFE5AD
      4DFFE6AF53FFE6AF52FFF2CD90FFB47A04FFAEB3BCFFAEAFAEFFADADABFFADAD
      ABFFAEAEABFFAFAFADFFB6B6B5FFEAEAEAFFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF4CAF82FF007A2FFF007B31FF007B31FF007B31FF007B
      31FF007B31FF007B31FF007C32FF007E35FF007F36FF80E2D5FF55D8C2FF00A9
      7DFF488530FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E1BBFF5ED1
      A8FF5ED1A9FF5ED2A9FF5DD6ACFF5DD7ADFF6E6365FF6D6A69FF6D6A69FF6E63
      65FF5ED0A8FF5DD6ACFF5ED2A9FF5ED1A9FF5ED1A8FF60E2BBFF43AE8AFF7A6D
      6EFF76716DFF746F63FF8394D8FF96B6FFFF4151FBFF787569FF737273FF7070
      74FF696A72FFFFE8B8FFCFA468FFE9E9E9FFE9E9E9FFAEAEABFFFFFFFFFFFAFA
      FAFFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9
      F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9FAFBFFFDFFFFFFCDA97BFFB0742AFFB176
      2DFFB1762EFFB1762EFFB2762FFFB27932FFB37B37FFE3C69CFFE4C69CFFB67D
      37FFCFCFCFFFF2F2F2FF000000FF000000FFE9E9E9FFB68115FFF3CF95FFE5AB
      4CFFE5AC4FFFE5AC4DFFE4AA48FFE4A742FFE3A640FFE3A640FFE3A640FFE3A6
      40FFE3A640FFE3A640FFE3A640FFE3A640FFE3A741FFE4A743FFE4AA48FFE5AC
      4DFFE5AC4FFFE5AB4CFFF2CE92FFB17700FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFB0B0ADFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008036FF74E0D3FF00A576FF5CBB
      97FFCF7F0DFFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E1BBFF5ECE
      A7FF5ECFA8FF5ED0A9FF5DD7ADFF659483FF6C6265FF6B6768FF6B6768FF6C63
      65FF678C7EFF5DD7ADFF5ED1A9FF5ECFA8FF5ECEA7FF60E2BBFF43AE88FF7767
      64FF746E69FF7F92D8FF85A1FFFF96B5FFFF4353FBFF7D7869FF7A7774FF7675
      75FF6C6C71FFFFEAB9FFCFA468FFE9E9E9FFE9E9E9FFAEAEACFFFFFFFFFFF8F8
      F7FFF7F7F6FFF7F7F6FFF7F7F6FFF7F7F6FFF7F7F6FFF7F7F6FFF7F7F6FFF7F7
      F6FFF7F7F6FFF7F7F6FFF7F7F6FFF7F7F6FFF8FAFAFFFBFEFFFFFCFFFFFFFDFF
      FFFFFDFFFFFFFDFFFFFFFDFFFFFFFFFFFFFFB37932FFE0C195FFB3772FFFB0A7
      99FFB6BABEFFEBEBEBFF000000FF000000FFE9E9E9FFB68115FFF3D099FFE4A9
      48FFE4AB4BFFE4AB4BFFE4AA49FFE3A947FFE3A947FFE3A947FFE3A947FFE3A9
      47FFE3A947FFE3A947FFE3A947FFE3A947FFE3A947FFE3A947FFE4AA49FFE4AB
      4BFFE4AB4BFFE4A948FFF2CF96FFB07500FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAEAEABFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007F35FF00A379FF19985DFFFFFF
      FFFFBD7D0BFFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E1BBFF5ECD
      A6FF5ECEA7FF5ED1A9FF5EC9A3FF695A5FFF686364FF686666FF686666FF6863
      64FF695B5FFF5EC9A3FF5ED1A9FF5ECEA7FF5ECDA6FF5FE2BAFF43AD83FF7E7F
      B7FF839BFFFF839DFFFF829BFFFF97B4FFFF4454FBFF7B7668FF777472FF7270
      72FF7A7876FFFFE8B8FFCFA468FFE9E9E9FFE9E9E9FFAEAEACFFFEFEFDFFF6F5
      F5FFF5F4F4FFF6F5F5FFF6F5F6FFF5F4F5FFF5F5F5FFF6F5F6FFF6F5F6FFF6F5
      F6FFF6F5F6FFF6F5F6FFF6F5F6FFF6F5F6FFF6F6F6FFF7F6F7FFF7F7F8FFF7F7
      F9FFF7F7F8FFF6F6F7FFF7F7F9FFFEFFFFFFB27428FFB17429FFF2E8DBFFFFFF
      FFFFAFB1B0FFE9E9E9FF000000FF000000FFE9E9E9FFB68115FFF3D49EFFE3A6
      40FFE3A744FFE3A642FFE1A33BFFE0A035FFE09F33FFE09F33FFE09F33FFE09F
      33FFE09F33FFE09F33FFE09F33FFE09F33FFE09F33FFE0A035FFE1A33BFFE3A6
      42FFE3A744FFE3A640FFF2D29AFFB07600FFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
      FEFFFDFDFDFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00792BFF13965AFFFFFFFFFFFFFF
      FFFFB67B09FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E1BBFF5ECC
      A4FF5ECEA5FF5ED3A9FF61A189FF675D5EFF666362FF666463FF666463FF6663
      62FF675E5EFF61A189FF5ED3A9FF5ECEA5FF5ECCA4FF5FE2BAFF42AB7EFF8696
      FFFF8199FFFF8098FFFF8097FFFF97B5FFFF4454FBFF777264FF706E6DFF716E
      70FFE3C99CFFFBE5B7FFCEA368FFE9E9E9FFE9E9E9FFAEAEACFFFDFCFCFFF4F3
      F4FFF5F5F5FFF7F8FAFFF7F8FAFFF6F6F7FFF9FAFBFFFAFCFEFFFBFDFFFFFBFD
      FFFFFBFDFFFFFBFDFFFFFBFDFFFFFBFDFFFFFBFDFFFFFBFDFFFFFBFDFFFFFBFC
      FFFFFAFBFCFFF5F4F5FFF4F3F4FFFCFDFEFFA8ACAEFFFFFFFFFFFFFFFFFFFFFF
      FFFFAEAEACFFE9E9E9FF000000FF000000FFE9E9E9FFB68114FFF5D5A2FFE2A3
      3CFFE2A53FFFE0A137FFF4DAB7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4DAB7FFE0A1
      37FFE2A53FFFE2A33CFFF4D39FFFB27701FFB9BEC8FFB6B7B9FFB5B5B5FFB3B3
      B3FFFEFCFCFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFFEFF
      FFFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFD
      FDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFD
      FDFFFDFDFDFFFDFDFDFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFB47A07FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E1BBFF5ECA
      A1FF5ECCA3FF5ED4A9FF645C5DFF645D5FFF646161FF646161FF646161FF6461
      61FF645D5FFF645C5DFF5ED4A9FF5ECCA3FF5ECAA1FF5FE2B9FF42AB7CFF8392
      FFFF7F95FFFF7E95FFFF7D94FFFF97B5FFFF4252FBFF6E6A5FFF817C73FFE2C8
      99FFF9D9A1FFF7E2B6FFCEA368FFE9E9E9FFE9E9E9FFAEAEACFFFCFCFBFFF1F2
      F1FFF4F6F7FFA77C5DFFA77A5BFFF5F8F9FFBE9F8AFFA77B5CFFA87D5EFFA87D
      5FFFA87D5FFFA87D5FFFA87D5FFFA87D5FFFA87D5FFFA87D5FFFA87D5FFFA87C
      5EFFA67959FFF3F5F4FFF1F1F1FFFBFBFBFFA7A8A6FFFFFFFFFFFBFCFCFFFFFF
      FFFFAEAEABFFE9E9E9FF000000FF000000FFE9E9E9FFB68114FFF5D8A7FFE1A0
      36FFE0A139FFDF9C2DFFFEFCFFFFFCF3F1FFFCF2EDFFFBF1ECFFFCF2EDFFFCF2
      EDFFFBF1ECFFFBF1EDFFFCF2EDFFFCF2EDFFFBF2EDFFFCF3F0FFFDFBFFFFDF9B
      2DFFE0A139FFE1A036FFF4D6A4FFB27700FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFBFBFBFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFFBFA
      FCFFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9
      F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9F9FFFAF9
      F9FFFAF9F9FFFAF9F9FFFAF9F9FFFBF9FAFFFDFAFBFFFDFAFBFFFCFBFDFFFFFF
      FFFFB47A07FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E1BBFF5EC9
      A1FF5ECCA3FF5ED5A9FF615357FF615D5DFF615F5FFF615F5FFF615F5FFF615F
      5FFF615D5DFF615358FF5ED5A9FF5ECCA3FF5EC9A1FF5FE2BAFF43AB7DFF8190
      FFFF7D93FFFF7C93FFFF7B92FFFF96B4FFFF3D4EF9FFCCB380FFFFDE9FFFF7D7
      9EFFF4D49CFFF6E1B5FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFBFBFAFFEFEE
      EFFFF2F3F5FFA87C5DFFF4F8FAFFF1F2F4FFF2F3F5FFF3F6F8FFF4F6F9FFF4F7
      FAFFF4F7FAFFF4F7FAFFF4F7FAFFF4F7FAFFF4F7FAFFF4F7FAFFF4F7FAFFF4F6
      F9FFF3F4F6FFF0F0F0FFEFEEEEFFFAFAF9FFA7A8A5FFFDFDFCFFF9F9F8FFFFFF
      FFFFAEAEACFFE9E9E9FF000000FF000000FFE9E9E9FFB68014FFF6DAAEFFDF9E
      30FFDF9F33FFDE9926FFFFFDFFFFFFF7F1FFFFF5EDFFFEF4EDFFFFF6EEFFFFF5
      EEFFFEF4EDFFFEF4ECFFFFF6EEFFFFF6EEFFFFF5EDFFFEF4EFFFFFFBFFFFDE99
      26FFDF9F33FFDF9E30FFF5D9ABFFB37700FFBABFC9FFB7B8B9FFECECECFF9A9A
      9AFFFBF9FAFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFF6F7
      F8FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6
      F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6
      F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F6F5FFF6F7F8FFFFFF
      FFFFB47A07FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E2BBFF5EC8
      9EFF5ECBA1FF5ED4A7FF5F5256FF5F5B5CFF5F5D5DFF5F5D5DFF5F5D5DFF5F5D
      5DFF5F5B5CFF5F5256FF5EC29DFF5ECBA1FF5EC89EFF60E3BAFF43AC7FFF7A83
      E9FF7A90FFFF7990FFFF778EFFFF95B4FFFF394BF8FFFFE092FFF7D69AFFF3D3
      9AFFF2D299FFF5E1B6FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFCFAFBFFECEC
      EBFFEFF0F0FFF2F6F7FFF1F4F5FFEFF0F0FFEFF1F1FFF1F3F4FFF2F4F6FFF2F4
      F6FFF2F4F6FFF2F4F6FFF2F4F6FFF2F4F6FFF2F4F6FFF2F4F6FFF2F4F6FFF1F4
      F5FFF0F2F2FFEEEEEDFFECEBEAFFFAF9FAFFA8A8A6FFFBFAFAFFF7F6F6FFFEFE
      FDFFAEAEACFFE9E9E9FF000000FF000000FFE9E9E9FFB68014FFF8DDB3FFDE9B
      2AFFDE9D2EFFDD9720FFFFFFFFFF474444FFA8A29CFFFFF8F1FF474442FFAAA3
      9DFFA6A09AFFFFF7F0FF474442FFABA59FFFA9A29DFFA5A09BFFFFFDFFFFDD97
      20FFDE9D2EFFDE9B2AFFF7DBB0FFB27700FFFFFFFFFFFEFFFFFFFDFDFCFFFBFB
      FAFFF8F8F7FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47A07FFFFFFFFFFF3F3
      F4FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2
      F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2
      F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F2F2FFF3F3F4FFFFFF
      FFFFB47A07FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E2BBFF5EC5
      9DFF5EC8A0FF5ED1A6FF5D4D52FF5D5758FF5D5A5AFF5D5A5AFF5D5A5AFF5D5A
      5AFF5D5758FF5D4D52FF5ED2A6FF5EC9A0FF5EC59DFF60E3BBFF44AE84FF6F6D
      A0FF798FFFFF778DFFFF758BFFFF95B4FFFF384BF8FFFFDB8EFFF5D295FFF2D0
      97FFF2D096FFF5E1B6FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFBFBFAFFEAEA
      EAFFEEEFF0FFAA7E60FFA87C5EFFEFF0F1FFC2A693FFA97C5EFFAA7E60FFAA7E
      60FFAA7E60FFAA7E60FFAA7E60FFAA7E60FFAA7E60FFAA7E60FFAA7E60FFAA7E
      60FFA77B5BFFECEDEDFFEAEAE9FFF9FAF9FFA8A9A6FFF9F8F9FFF5F4F5FFFDFC
      FCFFAEAEACFFE9E9E9FF000000FF000000FFE9E9E9FFB68013FFF8DFB9FFDD97
      22FFDD9927FFDC9319FFFFFFFFFFFFFBF4FFFFFAF1FFFFF8F0FFFFFAF2FFFFFA
      F1FFFFF7EFFFFFF6EEFFFFF9F1FFFFFDF4FFFFFCF3FFFFF8F1FFFFFFFFFFDC94
      1AFFDE9A29FFDD9824FFF7DEB6FFB37700FFBABFC9FFB8B9BBFFB8B8B8FFB5B5
      B5FFF7F6F6FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47A08FFFFFFFFFFEEEF
      F0FFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEF
      EEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEF
      EEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEFEFEEFFEEEFF0FFFFFF
      FFFFB47A08FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E2BCFF5EC4
      9BFF5EC79DFF5ECEA2FF5B6C64FF5A5254FF5A5657FF5A5757FF5A5757FF5A56
      57FF5A5254FF5B6C64FF5ECEA2FF5EC79DFF5EC49BFF60E3BCFF45AF88FF6353
      48FF778DFFFF758AFFFF7388FFFF96B5FFFF384BF8FFFFDB8BFFF5D293FFF2CF
      94FFF2CF92FFF5E1B6FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFBFBFAFFE7E8
      E6FFEAEDECFFA97D5EFFEDF1F2FFEAECECFFEAEDECFFECEFEFFFECF0F0FFECF0
      F0FFECF0F0FFECF0F0FFECF0F0FFECF0F0FFECF0F0FFECF0F0FFECF0F0FFECEF
      F0FFEBEDEDFFE8E9E8FFE7E7E5FFFAFAF9FFA9AAA7FFE1D6CDFFF2F3F2FFFCFD
      FCFFAEAEACFFE9E9E9FF000000FF000000FFE9E9E9FFB68012FFF9E2BEFFDB91
      16FFDC931AFFDB8E0EFFFFFFFFFF474544FFAAA29DFFA8A09BFFA7A09BFFA7A0
      9BFFA79F9BFFA59D99FFFFF6EEFF4A4745FF4B4947FFA7A09DFFFFFFFFFFDD93
      16FFE09A25FFDE9720FFF9E2BEFFB27700FFFDFFFFFFFAFBFCFFFAFAF9FFF8F8
      F7FFF4F5F3FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47B08FFFFFFFFFFEAEB
      ECFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEB
      EAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEB
      EAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEBEBEAFFEAEBECFFFFFF
      FFFFB47B08FFE9E9E9FF0000000000000000E9E9E9FF48A888FF60E2BCFF5EC3
      9AFF5EC59CFF5EC99EFF5DB692FF574F51FF574C50FF584E51FF584E51FF574C
      50FF574F51FF5DB692FF5EC99EFF5EC59CFF5EC39AFF60E3BDFF45B08AFF6152
      49FF758BFFFF7388FFFF7186FFFF97B6FFFF3B4DF9FFE5C27EFFF8D290FFF2CE
      91FFF2CE8FFFF5E2B6FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFBFBFAFFE5E4
      E3FFE7E7E8FFEAECEEFFEAEDEEFFEAECEDFFEAECEDFFEAECEDFFEAECEEFFEAEC
      EEFFEAECEEFFEAECEEFFEAECEEFFEAECEEFFEAECEEFFEAECEEFFEAECEEFFEAEB
      ECFFE8E9E9FFE6E5E5FFE5E4E3FFFAFAFAFFA9AAA7FFF4F3F4FFF0EFEFFFFBFC
      FBFFAFAFADFFE9E9E9FF000000FF000000FFE9E9E9FFB67F11FFFAE3C1FFFCEB
      CEFFD6D1C8FFFCEACAFFFFFFFFFFFFF9F2FFFFF8EEFFFFF5ECFFFFF4EBFFFFF4
      EBFFFFF7EDFFFFF5ECFFFEF3EAFFFFF7EEFFFFFAF0FFFFF5EEFFFFFFFFFFDF91
      10FF6C470BFFE09519FFFBE5C5FFB27700FFB9BEC9FFE7E8EAFF9E9E9EFFB6B6
      B6FFF2F2F1FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47B08FFFFFFFFFFE7E6
      E7FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7
      E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7
      E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE8E7E6FFE7E6E7FFFFFF
      FFFFB47B08FFE9E9E9FF0000000000000000E9E9E9FF48A888FF61E2BCFF5FC0
      96FF5EC197FF5EC298FF5EC69BFF5FC399FF5B987EFF5A8874FF5A8874FF5B98
      7EFF5FC399FF5EC69BFF5EC298FF5EC197FF5FC096FF60E3BDFF45B08AFF5F50
      46FF6E7DE8FF7085FFFF6E82FFFF98B6FFFF3F50FBFFA38F66FFFCD390FFF3CD
      8FFFF2CB8DFFF5E2B7FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFCFBFCFFE3E2
      E0FFE5E6E5FFC8B6A8FFB69B84FFB79D86FFB79D86FFB79D86FFB79D86FFB79D
      86FFB79D86FFB79D86FFB79D86FFB79D86FFB79D86FFB79D86FFB69D86FFB69B
      84FFC7B6A8FFE5E5E5FFE3E2E0FFFCFBFBFFAAAAA8FFECE9E7FFEEEDECFFFCFB
      FBFFAFAFADFFE9E9E9FF000000FF000000FFE9E9E9FFB67F11FFFCE9CCFFC180
      08FFB27C0CFFB27A08FFD4B069FF47484BFFA7A19BFFA49E98FFA39D97FFFEF4
      EBFF454341FFA69F98FFA29C95FFFDF3E9FF45423FFFA39D98FFFFFFFFFFDE8F
      0AFF654006FFDE9212FFFCE9CCFFB27700FFF9FCFFFFF7F6F8FFF7F6F6FFF5F4
      F4FFF1F0F0FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47B08FFFFFFFFFFE2E3
      E3FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4
      E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4
      E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE4E4E2FFE2E3E3FFFFFF
      FFFFB47B08FFE9E9E9FF0000000000000000EEEEEEFF48AA89FF62E4BDFF60E2
      BCFF60E2BCFF60E3BCFF60E3BCFF60E6BDFF60E8BDFF60E9BEFF60E9BEFF60E8
      BDFF60E6BDFF60E4BCFF60E4BDFF60E4BDFF60E4BDFF61E7BEFF46B28AFF5B4C
      41FF7085FFFF6E82FFFF6C7FFFFF98B7FFFF4353FCFF52534BFFFFD58EFFF4CC
      8CFFF2CA8AFFF5E2B7FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFDFDFCFFDFE0
      DEFFE3E5E5FFB39177FFB7987FFFB79A82FFB89A82FFB89A82FFB89A82FFB89A
      82FFB89A82FFB89A82FFB89A82FFB89A82FFB89A82FFB89A82FFB79A82FFB798
      7FFFB39177FFE3E5E5FFDFE0DEFFFCFCFBFFAAAAA8FFDCD1C8FFECEBEAFFFBFB
      FAFFAFAFADFFE9E9E9FF000000FF000000FFEAEAEAFFB67F12FFFFEED7FFB27B
      0BFFE8AA43FFE9AA44FFB37A05FFFDF6F5FFFAF0E7FFF9F0E9FFFAF3EFFFFAF3
      EFFFFBF1EAFFF9EFE6FFF7EDE3FFF7EDE3FFF9EFE5FFF7EEE6FFFFFFFFFFD885
      00FFDC8C09FFD98904FFFDECD2FFB37700FFBBC0CAFFB9BABCFFB9B9B9FFB6B6
      B6FFEFEFEEFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB47B08FFFFFFFFFFDFDF
      DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0
      DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0
      DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFE1E0DFFFDFDFDFFFFFFF
      FFFFB47B08FFE9E9E9FF0000000000000000F9F9F9FF49AB8BFF48AA89FF48A9
      88FF48A888FF48A987FF47AB84FF46AD80FF44AD7DFF44AD7DFF44AF7CFF45AE
      7DFF45AE7FFF46AE85FF46B08AFF46B18CFF46B18CFF47B38CFF47B185FF5E58
      71FF6E81FFFF6C7FFFFF697CFFFF99B8FFFF4455FDFF53514AFFFFD68BFFF5CC
      89FFF2C986FFF5E2B7FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFEFDFDFFDDDC
      DCFFE0E1E1FFC2AB99FFB1896EFFB18B71FFB28C71FFB28C71FFB28C71FFB28C
      71FFB28C71FFB28C71FFB28C71FFB28C71FFB28C71FFB28C71FFB18B71FFB189
      6EFFC2AB99FFE0E1E1FFDDDCDCFFFDFCFDFFAAAAA8FFECECEAFFE8E8E6FFFBFB
      FAFFAFAFADFFE9E9E9FF000000FF000000FFF1F1F1FFB78113FFFFECCFFFB27A
      0AFFF8DEB6FFE39F2FFFB27A08FFFFFFFFFFFFFFFFFFFFFFFFFFB07400FFB074
      00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBE5
      C3FFFCE8C9FFFCE8C9FFFEE8C8FFB37700FFF6FAFFFFF4F4F6FFF3F2F2FFF1F0
      F0FFEEECECFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB57B08FFFFFFFFFFDADB
      DBFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDC
      DAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDC
      DAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDCDCDAFFDADBDBFFFFFF
      FFFFB57B08FFE9E9E9FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFE9E9E9FF4B4CFEFF9DB3FFFF6B73FFFF6C75FFFF6D75
      FFFF6E75FFFF518783FF57463EFF574842FF574842FF57483FFF56676FFF6875
      F3FF6A7CFFFF697AFFFF6777FFFF99B8FFFF4555FDFF4F4F48FFEAC281FFF5CA
      86FFF2C783FFF5E2B8FFCEA368FFE9E9E9FFE9E9E9FFAFAFADFFFFFEFFFFDAD9
      D7FFDCDCDAFFDEDFDEFFDFE1E1FFDFE2E2FFE0E2E2FFE0E2E2FFE0E2E2FFE0E2
      E2FFE0E2E2FFE0E2E2FFE0E2E2FFE0E2E2FFE0E2E2FFE0E2E2FFDFE2E2FFDFE1
      E1FFDEDFDEFFDCDCDAFFDAD9D7FFFFFEFEFFAAAAA8FFE9E8E7FFE5E4E3FFFBFB
      FAFFAFAFADFFE9E9E9FF000000FF000000FFFBFBFBFFC59B49FFDDC592FFB37B
      0AFFF7DCB1FFE19C2BFFB47E0EFFD9BF87FFD9BC81FFDBBF87FFB37B09FFF5D3
      9DFFAF7500FFD9BC81FFB07200FFB07200FFB07200FFB07200FFB07300FFB174
      00FFB17600FFB27600FFB27600FFC08E2BFFA1A5ACFFBBBBBCFFB9BABAFFB7B7
      B7FFEBECEAFFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB57B08FFFFFFFFFFD5D5
      D5FFD6D5D4FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6
      D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6
      D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D6D5FFD6D5D4FFD5D5D5FFFFFF
      FFFFB57B08FFE9E9E9FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFE9E9E9FF4953F3FF9AB7FFFF6273FFFF6475FFFF6475
      FFFF6575FFFF6576FFFF616BD9FF5E66C1FF5E66C1FF606CDAFF6578FFFF6477
      FFFF6476FFFF6375FFFF6274FFFF99B8FFFF4354FDFF494B45FFFFD286FFF4C9
      84FFF2C681FFF5E3B8FFCEA368FFE9E9E9FFEAEAEAFFAFAFADFFFFFFFFFFD7D6
      D3FFD8D7D4FFD8D7D5FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8
      D6FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8D6FFD8D8
      D6FFD8D7D5FFD8D7D4FFD7D6D3FFFFFFFFFFAAAAA8FFE6E5E3FFE3E2E0FFFCFB
      FCFFAFAFADFFE9E9E9FF000000FF000000FF0000000000000000EDEDEDFFB47D
      0EFFF9E0BAFFE4A43BFFE7C486FFB67E11FFB37C0EFFB37D0FFFB27D0FFFF3D2
      9CFFF2CD91FFB07703FFF1FAFFFFF0F6FFFFF1F5FEFFF1F4FCFFF0F3FCFFEFF3
      FCFFF1F5FDFFF1F5FDFFF1F4FCFFF0F3F8FFF0F0F2FFF1F0EFFFF0EFEEFFEEED
      ECFFEAE9E8FFFFFFFFFFADADABFFE9E9E9FFE9E9E9FFB57C0AFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFB57C0AFFE9E9E9FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFEFEFEFFF4A58F0FF9CBAFFFF9AB8FFFF9AB8FFFF9AB8
      FFFF99B8FFFF98B8FFFF98B8FFFF98B8FFFF98B8FFFF98B8FFFF98B7FFFF97B7
      FFFF97B7FFFF97B6FFFF97B6FFFF97B8FFFF3E51FFFF8D7B56FFFCCC81FFF4C6
      81FFF2C37EFFF5E3B8FFCEA368FFE9E9E9FFF2F2F2FFB0B0AEFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABABA9FFE3E3E1FFE0E0DEFFFDFD
      FCFFAFAFADFFE9E9E9FF000000FF000000FF0000000000000000F7F7F7FFB682
      19FFEECB91FFE6AC4BFFE6AD4DFFE7AF50FFE8B052FFE8B052FFE8B052FFE2A3
      3AFFE2A339FFF1C989FFB17600FFE3E8F4FFA1A4A9FFBBBCBEFFB9BABCFFEDED
      EEFFA0A1A3FFBBBCBEFFBABBBDFFB9BABBFFE1E0DFFFA0A0A1FFBABBBBFFB8B8
      B8FFE8E8E7FFFFFFFFFFAEAEABFFE9E9E9FFE9E9E9FFB57E0FFFF8E1C1FFE1A9
      51FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB
      54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB
      54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1AB54FFE1A951FFF8E1
      C1FFB57E0FFFE9E9E9FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFFAFAFAFF5765EFFF4A58EFFF4957EEFF4957EEFF4856
      F0FF4454F3FF3E50F8FF394CF9FF384BFAFF384CFBFF384CFBFF384CFAFF384C
      FAFF384CFAFF384CFAFF384CFAFF374CFDFF5B64E9FFF5C573FFF7C77DFFF2C4
      7CFFF2C279FFF5E3B8FFCEA368FFE9E9E9FFFBFBFBFFC6C6C5FFB0B0AEFFAFAF
      ADFFAEAEACFFADADABFFABACA9FFABABA9FFABACA9FFACACA9FFACACA9FFACAC
      A9FFACACA9FFACACA9FFACACA9FFACACA9FFACACA9FFACACA9FFACACA9FFACAC
      A9FFACACA9FFACACA9FFACACAAFFACACAAFFB7B6B5FFE0DFDEFFDDDCDBFFFEFD
      FDFFAFAFADFFE9E9E9FF000000FF000000FF0000000000000000FEFEFEFFE2D3
      B6FFC7912AFFFCEACDFFE9B35CFFE9B35CFFE9B35CFFE9B35CFFE9B35DFFE4A9
      45FFE4A947FFE6AA45FFF3C77FFFB37500FFEEF0F4FFECEBEBFFEBEAE9FFEBEA
      E9FFECEBEAFFECEBEAFFECEBE9FFECEBEAFFECEBEAFFECEBEAFFECEBE9FFEBE9
      E8FFE7E5E4FFFFFFFFFFAEAEABFFE9E9E9FFE9E9E9FFB68012FFF5DDB8FFD99E
      39FFDAA03EFFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA1
      40FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA1
      40FFDAA140FFDAA140FFDAA140FFDAA140FFDAA140FFDAA03EFFD99E39FFF5DD
      B8FFB68012FFE9E9E9FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFE9E9E9FFE2AF55FFFFEDADFFFFCA69FFFFCB6AFFFFCB6AFFFFCB6AFFFFCB
      6AFFFFCB6AFFFFCB6AFFFFCB6AFFFFCB6BFFF1BF79FFF8C573FFF4C276FFF3C1
      76FFF3C074FFF6E3B8FFCEA368FFE9E9E9FF000000FF000000FF000000FF0000
      00FFE9E9E9FFB0B0AEFFEEEDEDFFD2D1CEFFD2D1CFFFD3D1CFFFD3D1CFFFD3D1
      CFFFD3D1CFFFD3D1CFFFD3D1CFFFD3D1CFFFD3D1CFFFD3D1CFFFD3D1CFFFD3D1
      CFFFD3D1CFFFD3D1CFFFD3D2CFFFD2D1CFFFDCDBD9FFDCDBD9FFDAD9D7FFFFFE
      FFFFAFAFADFFE9E9E9FF000000FF000000FF000000000000000000000000FDFD
      FDFFDFCDA7FFCC9735FFF4D4A1FFFBE5C1FFFAE5C0FFFAE4BFFFF9E4C0FFEDC2
      7BFFE7AD51FFF0C47DFFB27600FFB4BAC6FFB9BCBFFFE8E7E7FF9E9E9FFFE9E7
      E6FFA0A0A0FFB9B9B9FFE8E7E6FFA0A0A0FFB3B4B4FFB9B9B9FFE8E7E6FF9D9E
      9EFFE4E3E2FFFFFFFFFFAEAEABFFE9E9E9FFEAEAEAFFB68114FFF1D6A9FFD18B
      17FFD28E1BFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E
      1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E
      1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1CFFD28E1BFFD18B17FFF1D6
      A9FFB68114FFEAEAEAFF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFEFEFEFFFD5A863FFFBE7B8FFF8E4B6FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
      B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF6E3B8FFF5E3B9FFF5E3
      B9FFF6E3B8FFF8E5BAFFCEA469FFEFEFEFFF000000FF000000FF000000FF0000
      00FFEAEAEAFFAFAFADFFFFFFFFFFD8D7D5FFD9D8D6FFD9D8D6FFD9D8D6FFD9D8
      D6FFD9D8D6FFD9D8D6FFD9D8D6FFD9D8D6FFD9D8D6FFD9D8D6FFD9D8D6FFD9D8
      D6FFD9D8D6FFD9D8D6FFD9D8D6FFD9D8D6FFD8D7D5FFD8D7D4FFD7D6D3FFFFFF
      FFFFAFAFADFFEAEAEAFF000000FF000000FF0000000000000000000000000000
      000000000000F7F5F1FFCBA761FFB67F12FFB57F12FFB57E11FFB27B0CFFF5D5
      A0FFF0C176FFAE7300FFE5EBF6FFE5E6E9FFE4E3E3FFE3E2E1FFE4E2E1FFE3E2
      E1FFE4E3E2FFE4E3E1FFE3E2E1FFE4E3E2FFE5E4E2FFE4E3E1FFE3E2E1FFE3E2
      E1FFE1E0DFFFFFFFFFFFAEAEACFFEAEAEAFFF2F2F2FFB88217FFEECF9BFFECCD
      98FFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE
      9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE
      9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCE9AFFECCD98FFEECF
      9BFFB88217FFF2F2F2FF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFFAFAFAFFD2AB73FFCFA469FFCEA368FFCEA368FFCEA368FFCEA368FFCEA3
      68FFCEA368FFCEA368FFCEA368FFCEA368FFCEA368FFCEA368FFCEA368FFCEA3
      68FFCEA368FFCEA469FFD1AA74FFFAFAFAFF000000FF000000FF000000FF0000
      00FFF2F2F2FFB0B0AEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFB0B0AEFFF2F2F2FF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000000000000000000000EEEEEEFFB78115FFFDDE
      AEFFB27600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFAFAFADFFF2F2F2FFFBFBFB00CBA65EFFB88317FFB782
      16FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB781
      16FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB781
      16FFB78116FFB78116FFB78116FFB78116FFB78116FFB78116FFB78216FFB883
      17FFCBA65EFFFBFBFBFF0000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FFFBFBFBFFC6C6C5FFB0B0AEFFAFAFADFFAFAFACFFAFAFADFFAFAFADFFAFAF
      ADFFAFAFADFFAFAFADFFAFAFADFFAFAFADFFAFAFADFFAFAFADFFAFAFADFFAFAF
      ADFFAFAFADFFAFAFADFFAFAFADFFAFAFADFFAFAFADFFAFAFACFFAFAFADFFB0B0
      AEFFC6C6C5FFFBFBFBFF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000000000000000000000F9F9F9FFB9851AFFB97F
      0DFFB0B8C4FFAFB2B4FFAEAFADFFAEAEACFFAEAEABFFAEAEABFFAEAEABFFAEAE
      ABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAEABFFAEAE
      ABFFAEAEACFFB0B0ADFFC6C6C5FFFBFBFBFF424D3E000000000000003E000000
      2800000080000000600100000100010000000000001600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000E000000FFF8000FFFFFFFFFF80000000
      E000000FFF00007FFF01FFFF80000000E000000FFF00007FFE00FFFF80000000
      E000000FFF00007FFC007FFF80000000E000000F8000007FFC007FFF80000000
      E000000F0000007FFC003FFF80000000E000000F0000007FFC001FFF80000000
      E000000F0000007FFC000FFF80000000E000000F0000007FFC0007FF80000000
      E000000F00000000F80003FF80000000E000000F00000000F00003FF80000000
      F000001F00000000E00003FF80000000F800003F00000000C00003FF80000000
      FE0000FF00000000800003FFFFF007FFFF8003FF80000000000001FFFFE003FF
      FF8003FFE0000000000000FFFFE003FFFF8003FFE00000000000007FFFE003FF
      FF8003FFE00000000000003FFFE003FFFF0001FFE00000000000001FFFF80FFF
      FF0001FFE00000000000000F80000000FF0001FFE00000008000000780000000
      FF0001FFE0000000C000000080000000FF0001FFE0000000E000030080000000
      FF0001FFE0000000F000078080000000FF0001FFE0000000F8000FC080000000
      FF0001FFE0000000FC001FFF80000000FF0001FFF0000000FE003FFF80000000
      FF0001FFFE000000FF007FFF80000000FF8003FFFE000000FF80FFFF80000000
      FF8003FFFE000000FFC1FFFF80000000FFE00FFFFE000000FFE3FFFF80000000
      FFFFFFFFFE000000FFFFFFFF80000000FFFFFFC0FFFFFFC0FFFFFFC0FFFFFFFF
      000000000000000000000000FFFFFFFF000000000000000000000000FFFFFFFF
      000000000000000000000000C07FFE03FE000000FE000000FE000000C07FFE03
      FE000000FE000000FE000000C07FFE03FE000000FE000000FE00000080000001
      FE000000FE000000FE00000080000001F0000000FE000000F800000080000001
      F0000000FE000000F000000080000001F0000000FE000000E000000080000001
      00000000FE000000C00000008000000100000000FE0000008000000080000001
      00000000FE000000000000008000000100000000FE0000000000000080000001
      00000000FE000000000000008000000100000000FE0000000000000080000001
      00000000FE000000000000008000000100000000FE0000008000000000000000
      00000000FE000000C000000000000000F0000000FE000000E000000000000000
      F0000000FE000000F000000000000000F0000000FE000000F800000000000000
      FE000000FE000000FE00000000000000FE000000FE000000FE000000E0000007
      FE000000FE000000FE000000F000000FFE000000FE000000FE000000F000000F
      FE000000FE000000FE000000F800001FFE000000FE000000FE000000F800001F
      FFFFFF00FFFFFF00FFFFFF00FC00003FFFFFFF80FFFFFF80FFFFFF80FFFFFFFF
      FFFFFFE0FFFFFFE0FFFFFFE0FFFFFFFFFC000000FFF81FFFFFFE0000FFF80000
      FC000000E0000007FFFE0000FFF80000FC000000E0000007FF0E0000FF080000
      FC000000E0000007FF060000FF000000FC000000E0000007F0020000F0000000
      00000000FFF81FFFE0000000E000000000000000FC00003FC0000000C0000000
      00000000FC00003F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000007F0000000000000000000000000000007F00000000
      00000000000000000000007F0000000100000000000000000000007F00000003
      00000000000000000000007F00000007F800001F000000000000007F0000000F
      F800001F000000000000007F0000007FFC00003F000000000000007F0000007F
      FC00003FF800001F0000007F0000007FFC00003FF800001F0000007F0000007F
      FC00003FFC00003F0000007F0000007FFC00003FFC00003F0000007F0000007F
      FC00003FFC00003F0000007F0000007FFC00003FFC00003F0000007F0000007F
      FC00003FFC00003F0000007F0000007FFC000007FC00003FFC000007FFFFFFFF
      FC000003FC00003FFC000003FFFFFFFFFC000001FC00003FFC000001FF0000FF
      FC000000FC00003FFC000000FF0000FFFC000000FC00003FFC000000FF0000FF
      00000000FC00003F000000000000000000000000FC00003F0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000F800001F00000000F800001FFC00003F
      F800001F00000000F800001FFC00003FFC00003FF800001FFC00003FFF8001FF
      FC00003FF800001FFC00003FFF8001FFFC00003FF800001FFC00003FFF8001FF
      FC00003FFC00003FFC00003FFF8001FFFC00003FFC00003FFC00003FFF8001FF
      FC00003FFC00003FFC00003FFFC003FFFC00003FFC00003FFC00003FFFFFFFFF
      FC00003FFC00003FFC00003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00003F
      FFFFFFFFFFFFFFFF00001FFFFC00003FFFFFE007FF87FFFF00001FFFFC00003F
      FFFFC003FF81FFFF0000007FFC00003FFFFF8001FF807FFF0000007FFC00003F
      FFFF0000FF803FFF0000007F0000000000000000000000010000007F00000000
      00000000000000010000007F0000000000000000000000010000007F00000000
      00000000000000018000007F000000000000000000000001C000007F00000000
      0000000000000001000000000000000000000000000000010000000000000000
      0000000000000001000000000000000000000000000000010000000000000000
      0000000100000001000000000000000000000001000000010000000000000000
      0000000100000001000000000000000000000001000000010000000000000000
      0000000100000000000000000000000000000001000000000000000000000000
      00000001000000000000000000000000000000010000000000000000F800001F
      000000010000000100000000F800001F000000010000000100000000FC00003F
      000000010000000100000000FC00003F000000010000000100000000FC00003F
      000000010000000100000000FC00003F000000010000000100000000FC00003F
      000000010000000180000001FC00003FFFFFFFFFFFFFFFFFC0000003FC00003F
      FFFFFFFFFFFFFFFFFFFFFFFFFC00003F00000000FFE00003FFFFFFFFFFFFFFFF
      00000000FFC00000FFFFFFFFFFFFFFFF00000000C0000000FFFFFFFFFFFFE007
      0000000000000000FFFFFFFFFFFFC003000000000000000000000001FFFF8001
      000000000000000000000001FFFF000000000000000000000000000100000000
      0000000000000000000000010000000000000000000000000000000100000000
      8000000100000000000000010000000000000000000000000000000100000000
      0000000000000000000000010000000000000000000000010000000100000000
      0000000000000003000000010000000000000000800000070000000100000000
      00000000C0000007000000010000000100000000F00000070000000100000001
      00000000F8000007000000010000000100000000F80000070000000100000001
      80000001F00000070000000100000001E0000007F000000F0000000100000001
      E0000007F00000070000000100000001E0000007F00000070000000100000001
      E0000007F00000070000000100000001E0000007F000000F0000000100000001
      E0000007F000000F0000000100000001E0000007F000000F0000000100000001
      E003C007F000001F0000000100000001E003C007F000001FFFFFFFFF00000001
      E003C007F800007FFFFFFFFF00000001E003C007F8003FFFFFFFFFFFFFFFFFFF
      F007E00FFE00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF801FFFFFFFFFF80003FF
      FC00007FFFFF000FFFFE0000F80003FFF000001FF0000007FFFE0000F8000000
      F000001FC000000300000000F8000000F000001FC000000300000000F8000000
      F000001FC000000300000000F8000000F000001FC00000030000000000000000
      F000001FC00000030000000000000000F000001FC00000030000000000000000
      F000001FC00000030000000000000000F000001FC00000030000000100000000
      F000001FC00000030000000300000001F000001FC00000038000000300000003
      F800003FC0000007E000000300000003FC00007FE000000FE000000300000003
      FF0001FFF000001FE000000380000003FF8003FFFC0007FFE0000003E0000003
      FF8003FFFE000FFFE0000007E0000003FF0001FFFE000FFFE0000007E0000007
      FF0001FFFC0007FFE0000007E0000007FF0001FFFC0007FFE0000007E0000007
      FF0001FFFC0007FFE0000007E0000007FF0000FFFC0007FFE000000FE0000007
      FF0000FFFC0007FFE00003FFE000000FFF0000FFFC0007FFF00003FFE00000FF
      FF0001FFFC0007FFFFE003FFE00000FFFF0001FFFC0007FFFFE003FFE00000FF
      FF0001FFFC0007FFFFE003FFF00000FFFF8003FFFC0007FFFFE003FFFFF800FF
      FF8003FFFE000FFFFFF007FFFFF800FFFFE00FFFFE000FFFFFFFFFFFFFF800FF
      FFFFFFFFFF803FFFFFFFFFFFFFFC01FFFFFFFFFFFFC003FFFFFFE1FFFFE01F0F
      000FE000FE00007FFFFFC1FFFFE01E07000FE000FC00003FFFFF81FFC0000403
      000FE000F800001FFFFF001F00000001000FE000F000000FFFFE000700000000
      000FE000E00000070000000100000000000FE000C00000030000000100000000
      000FE000800000010000000000000000000FE000800000010000000000000003
      000FE000800000010000000000000003000FE000000000000000000000000003
      0003E0000000000000000000000000030001E0000000000000000000000001FF
      0000E0000000000000000000000001FF000060000000000000000001800003FF
      000020000000000000000001C00007FF000000000000000000000001F0001FFF
      000000000000000000000001F8003FFF000000000000000000000001F8003FFF
      000000000000000000000001F0001FFF000800000000000000000001F0001FFF
      000C00000000000000000001F0001FFF000E00008000000100000001F0001FFF
      000F00008000000100000001F0001FFF000F80008000000100000001F0001FFF
      000FE000C000000300000001F0001FFF000FE000E000000700000001F0001FFF
      000FE000F000000F00000001F0001FFF000FE000F800001F00000001F0001FFF
      000FE000FC00003F00000001F8003FFF000FE000FE00007F00000001F8003FFF
      FFFFFFFFFFC003FF00000001FE00FFFFF0000000FFFFFFFFFFFFE00700000000
      F0000000FF800000FFFFC00300000000F0000000FF800000FFFF800100000000
      F0000000F8000000FFFF000000000000F0000000F8000000FFFF000000000000
      F0000000800000000000000000000000F0000000800000000000000000000000
      0000000080000000000000000000000000000000800000000000000000000000
      0000000080000000000000000000000000000000800000000000000000000000
      0000000080000000000000000000000000000000800000000000000000000000
      0000000080000000000000010000000000000000800000000000000300000000
      0000000080000000000000030000000000000000800000000000000300000000
      0000000080000000000000030000000000000000800000000000000300000000
      000000008000000000000003000000000000000F800000000000000300000000
      0000000F8000000000000003000000000000000F800000000000000300000000
      0000001F8000000000000003000000000000003F800000010000000300000000
      0000007F8000000300000003000000000000007F800000070000000300000000
      0000007F8000000F00000003000000000000007F8000007F0000000300000000
      0000007F800000FF00000003000000000000007F800007FF0000000300000000
      0000007F80000FFF0000000300000000FFFFFE1F00000FFFFFFFFE1F0000007F
      FFFFFE0F00000FFFFFFFFE0F0000007FFFFFFE0700000FFFFFFF00070000007F
      FFFC000300000FFFFFFF00030000007F000000010000003FFFFF00010000007F
      000000000000003FFFFF00000000007F000000000000003F000000000000007F
      0000000000000000000000000000007F00000000000000000000000000000000
      0000000000000000000000010000000000000001000000000000000300000000
      0000000300000000000000030000000000000003000000000000000300000000
      0000000300000000000000030000000000000003000000000000000300000000
      0000000300000000000000030000000000000003000000000000000300000000
      0000000300000000000000030000000000000003000000000000000300000000
      0000000300000000000000030000000000000003000000000000000300000000
      0000000300000000000000030000000000000003000000000000000300000000
      0000000300000000000000030000000000000003FC0000000000000300000000
      00000003FC00000000000003C000000000000003FC00000000000003C0000000
      00000003FC00000000000003C000000000000003FFF00000F0000003E0000000
      00000003FFF00000F0000003F800000000000003FFF00000F0000003FF800000
      00000003FFFFFFFFF0000003FF80000000000000000000000000000000000000
      000000000000}
  end
  object pmKontrah: TPopupMenu
    OnPopup = pmKontrahPopup
    Left = 872
    Top = 8
    object Poprawdanekontrahenta1: TMenuItem
      Caption = 'Popraw dane kontrahenta'
      OnClick = Poprawdanekontrahenta1Click
    end
    object DodajCMR1: TMenuItem
      Caption = 'Dodaj CMR'
      OnClick = DodajCMR1Click
    end
    object Usudanekontrahenta1: TMenuItem
      Caption = 'Usu'#324' dane kontrahenta'
      OnClick = Usudanekontrahenta1Click
    end
    object Pokaobrotykontrahenta1: TMenuItem
      Caption = 'Poka'#380' obroty kontrahenta'
      OnClick = Pokaobrotykontrahenta1Click
    end
    object pmTabelaR: TMenuItem
      Caption = 'Poka'#380' tabele retra dostawcy'
      Visible = False
      OnClick = pmTabelaRClick
    end
    object Dodajgadety1: TMenuItem
      Caption = 'Dodaj gad'#380'ety'
      Visible = False
      OnClick = Dodajgadety1Click
    end
    object Drukujkartkontrahenta1: TMenuItem
      Caption = 'Drukuj kart'#281' kontrahenta'
      OnClick = Drukujkartkontrahenta1Click
    end
  end
  object ibqDostawcy: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'SELECT NR_KONTRAH, NAZWA, NIP, KOD_P, MIASTO, RODZAJ, TELEFON, U' +
        'LICA, UWAGI FROM KONTRAH'
      'WHERE RODZAJ = '#39'D'#39)
    Left = 868
    Top = 41
  end
  object dsDostawcy: TDataSource
    DataSet = ibqDostawcy
    Left = 916
    Top = 41
  end
  object pmDostawca: TPopupMenu
    Left = 840
    Top = 8
    object Pokaobrotydostawcy1: TMenuItem
      Caption = 'Poka'#380' obroty dostawcy'
      OnClick = Pokaobrotydostawcy1Click
    end
    object Drukujobrotydostawcy1: TMenuItem
      Caption = 'Drukuj obroty dostawcy'
      object wszystkie3: TMenuItem
        Caption = 'wszystkie'
        Checked = True
        RadioItem = True
      end
      object stycze3: TMenuItem
        Caption = 'stycze'#324
        RadioItem = True
      end
      object luty4: TMenuItem
        Caption = 'luty'
        RadioItem = True
      end
      object marzec4: TMenuItem
        Caption = 'marzec'
        RadioItem = True
      end
      object kwiecie4: TMenuItem
        Caption = 'kwiecie'#324
        RadioItem = True
      end
      object maj4: TMenuItem
        Caption = 'maj'
        RadioItem = True
      end
      object czerwiec4: TMenuItem
        Caption = 'czerwiec'
        RadioItem = True
      end
      object lipiec4: TMenuItem
        Caption = 'lipiec'
        RadioItem = True
      end
      object sierpie4: TMenuItem
        Caption = 'sierpie'#324
        RadioItem = True
      end
      object wrzesie4: TMenuItem
        Caption = 'wrzesie'#324
        RadioItem = True
      end
      object padziernik4: TMenuItem
        Caption = 'pa'#378'dziernik'
        RadioItem = True
      end
      object listopad4: TMenuItem
        Caption = 'listopad'
        RadioItem = True
      end
      object grudzie4: TMenuItem
        Caption = 'grudzie'#324
        RadioItem = True
      end
    end
  end
  object pmKontrah2: TPopupMenu
    OnPopup = pmKontrahPopup
    Left = 904
    Top = 8
    object MenuItem3: TMenuItem
      Caption = 'Poka'#380' obroty kontrahenta'
      OnClick = MenuItem3Click
    end
    object MenuItem4: TMenuItem
      Caption = 'Drukuj obroty kontrahenta'
      object wszystkie: TMenuItem
        Caption = 'wszystkie'
      end
      object styczen: TMenuItem
        Caption = 'stycze'#324
      end
      object luty: TMenuItem
        Caption = 'luty'
      end
      object marzec: TMenuItem
        Caption = 'marzec'
      end
      object kwiecien: TMenuItem
        Caption = 'kwiecie'#324
      end
      object maj: TMenuItem
        Caption = 'maj'
      end
      object czerwiec: TMenuItem
        Caption = 'czerwiec'
      end
      object lipiec: TMenuItem
        Caption = 'lipiec'
      end
      object sierpien: TMenuItem
        Caption = 'sierpie'#324
      end
      object wrzesien: TMenuItem
        Caption = 'wrzesie'#324
      end
      object pazdziernik: TMenuItem
        Caption = 'pa'#378'dziernik'
      end
      object listopad: TMenuItem
        Caption = 'listopad'
      end
      object grudzien: TMenuItem
        Caption = 'grudzie'#324
      end
    end
  end
  object pmRaporty: TPopupMenu
    Left = 808
    Top = 8
  end
  object raporty: TOpenDialog
    Left = 672
    Top = 16
  end
  object DataSource1: TDataSource
    DataSet = dm.PlikCSV
    Left = 748
    Top = 9
  end
end
