unit dodajR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DBCtrls, JvExControls, JvDBLookup,
  DB, IBCustomDataSet, IBQuery, Grids, DBGrids, PageControlEx,
  JvExStdCtrls, JvButton, JvCtrls;

type
  TfrReczny = class(TForm)
    sb: TStatusBar;
    ibqDostawca: TIBQuery;
    dsDostawca: TDataSource;
    pc: TPageControlEx;
    SzukajSklepu: TTabSheet;
    SzukajDostawcy: TTabSheet;
    GroupBox1: TGroupBox;
    leNIP: TLabeledEdit;
    leNazwa: TLabeledEdit;
    leImieN: TLabeledEdit;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    JvImgBtn1: TJvImgBtn;
    JvImgBtn2: TJvImgBtn;
    GroupBox2: TGroupBox;
    leNIP_D: TLabeledEdit;
    leNazwaD: TLabeledEdit;
    DBNavigator2: TDBNavigator;
    DBGrid2: TDBGrid;
    JvImgBtn3: TJvImgBtn;
    JvImgBtn4: TJvImgBtn;
    DodajObrot: TTabSheet;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText5: TDBText;
    GroupBox5: TGroupBox;
    DBText4: TDBText;
    DBText3: TDBText;
    DBText2: TDBText;
    DBText1: TDBText;
    GroupBox6: TGroupBox;
    leObrot: TLabeledEdit;
    leEuro: TLabeledEdit;
    leSklep: TLabeledEdit;
    lEuro: TLabel;
    lSklep: TLabel;
    JvImgBtn5: TJvImgBtn;
    procedure leNIPChange(Sender: TObject);
    procedure leNazwaChange(Sender: TObject);
    procedure leImieNChange(Sender: TObject);
    procedure JvDBLookupList1Click(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure leNIP_DChange(Sender: TObject);
    procedure leNazwaDChange(Sender: TObject);
    procedure leEuroChange(Sender: TObject);
    procedure leSklepChange(Sender: TObject);
    procedure JvImgBtn5Click(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frReczny: TfrReczny;

implementation
uses baza,glowny,kontrahent;
{$R *.dfm}

procedure TfrReczny.leNIPChange(Sender: TObject);
begin
with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE NIP LIKE :A AND RODZAJ = ''O''');
    Unprepare;
      ParamByName('A').AsString := '%' + UpperCase(leNIP.Text) + '%';
    Prepare;
    Open;

  end;
end;

procedure TfrReczny.leNazwaChange(Sender: TObject);
begin
with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = ''O''');
    Unprepare;
      ParamByName('A').AsString := '%' + UpperCase(leNazwa.Text) + '%';
    Prepare;
    Open;

  end;
end;

procedure TfrReczny.leImieNChange(Sender: TObject);
begin
with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE IMIE_NAZWISKO LIKE :A AND RODZAJ = ''O''');
    Unprepare;
      ParamByName('A').AsString := '%' + UpperCase(leImieN.Text) + '%';
    Prepare;
    Open;

  end;
end;

procedure TfrReczny.JvDBLookupList1Click(Sender: TObject);
var
   imieN,ulica,miasto : String;
begin
imieN := dm.ibqKontrahenci.FieldValues['IMIE_NAZWISKO'];
ulica := dm.ibqKontrahenci.FieldValues['ULICA'];
miasto := dm.ibqKontrahenci.FieldValues['MIASTO'];
sb.SimpleText := 'Kontrahent o imieniu i nazwisku ' + imieN + ' z ulicy ' + ulica + ' i miasta ' + miasto;
end;

procedure TfrReczny.JvImgBtn1Click(Sender: TObject);
begin
pc.SelectNextPage(True);
end;

procedure TfrReczny.leNIP_DChange(Sender: TObject);
begin
with ibqDostawca do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE NIP LIKE :A AND RODZAJ = ''D''');
    Unprepare;
      ParamByName('A').AsString := '%' + UpperCase(leNIP_D.Text) + '%';
    Prepare;
    Open;
  end;
end;

procedure TfrReczny.leNazwaDChange(Sender: TObject);
begin
with ibqDostawca do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = ''D''');
    Unprepare;
      ParamByName('A').AsString := '%' + UpperCase(leNazwaD.Text) + '%';
    Prepare;
    Open;
  end;
end;

procedure TfrReczny.leEuroChange(Sender: TObject);
var
   obrot,retroE : Real;
begin
obrot := StrToFloat(leObrot.Text);
retroE := StrToFloat(leEuro.Text);
lEuro.Caption := FloatToStr(obrot * (retroE/100));

end;

procedure TfrReczny.leSklepChange(Sender: TObject);
var
   obrot,retroS : Real;
begin
obrot := StrToFloat(leObrot.Text);
retroS := StrToFloat(leSklep.Text);
lSklep.Caption := FloatToStr(obrot * (retroS/100));
end;

procedure TfrReczny.JvImgBtn5Click(Sender: TObject);
var
     nr_sklep,nr_dostawca : Integer;
     nazwa_dostawca : String;
begin
nr_sklep := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
nr_dostawca := ibqDostawca.FieldValues['NR_KONTRAH'];
nazwa_dostawca := ibqDostawca.FieldValues['NAZWA'];
if not dm.ibtZapisz.InTransaction then dm.ibtZapisz.StartTransaction;
with dm.ibqZapisz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO OBROTY(NR_KONTRAH_SKLEP,NR_KONTRACH_DOST,NAZWA_D,DATA_ZAPISU,OBROTY,PROWIZJA_PROC,PROW_E_PROC)');
          SQL.Add('VALUES(:NR_KONTRAH_SKLEP,:NR_KONTRACH_DOST,:NAZWA_D,CURRENT_DATE,:OBROTY,:PROWIZJA_PROC,:PROW_E_PROC)');
          UnPrepare;
               ParamByName('NR_KONTRAH_SKLEP').AsInteger := nr_sklep;
               ParamByName('NR_KONTRACH_DOST').AsInteger := nr_dostawca;
               ParamByName('NAZWA_D').AsString := nazwa_dostawca;
               ParamByName('OBROTY').asFloat := StrToFloat(leObrot.Text);
               ParamByName('PROWIZJA_PROC').asFloat := StrToFloat(leSklep.Text);
               ParamByName('PROW_E_PROC').asFloat := StrToFloat(leEuro.Text);
          Prepare;
          Open;
     end;
     if dm.ibtZapisz.InTransaction then dm.ibtZapisz.Commit;
leEuro.Text := '0,00';
leSklep.Text := '0,00';
leObrot.Text := '0,00';
pc.ActivePageIndex := 0;
Close;
end;

procedure TfrReczny.JvImgBtn3Click(Sender: TObject);
begin
frDodajK.ShowModal;
end;

procedure TfrReczny.JvImgBtn2Click(Sender: TObject);
begin
frDodajK.ShowModal;
end;

end.
