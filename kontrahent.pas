unit kontrahent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, JvExExtCtrls, JvRadioGroup, ComCtrls,
  JvExComCtrls, JvMonthCalendar, JvExStdCtrls, JvEdit, JvButton, JvCtrls,
  Buttons, DBCtrls, Grids, DBGrids, PageControlEx, Menus;

type
  TfrDodajK = class(TForm)
    sb: TStatusBar;
    pc: TPageControlEx;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    edImieNaz: TJvEdit;
    edKodP: TJvEdit;
    edUlica: TJvEdit;
    mUwagi: TMemo;
    edMetraz: TJvEdit;
    edNIP: TJvEdit;
    edMiasto: TJvEdit;
    edNazwa: TJvEdit;
    edTelefon: TJvEdit;
    edKategoria: TJvEdit;
    rgTypK: TJvRadioGroup;
    JvImgBtn1: TJvImgBtn;
    CzyAktywny: TCheckBox;
    DataPrzystapienia: TDateTimePicker;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    ComboBox1: TComboBox;
    Label11: TLabel;
    Memo1: TMemo;
    Button1: TButton;
    Siatka: TDBGrid;
    DBNavigator1: TDBNavigator;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    TabSheet3: TTabSheet;
    Label15: TLabel;
    JvEdit1: TJvEdit;
    Label16: TLabel;
    JvEdit2: TJvEdit;
    JvEdit3: TJvEdit;
    Label17: TLabel;
    JvEdit4: TJvEdit;
    Label18: TLabel;
    JvEdit5: TJvEdit;
    Label19: TLabel;
    JvEdit6: TJvEdit;
    Label20: TLabel;
    Label21: TLabel;
    JvEdit7: TJvEdit;
    JvEdit8: TJvEdit;
    Label22: TLabel;
    JvEdit9: TJvEdit;
    Label23: TLabel;
    GroupBox2: TGroupBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    Label24: TLabel;
    CheckBox7: TCheckBox;
    rgTypK_N: TJvRadioGroup;
    Memo2: TMemo;
    Label25: TLabel;
    JvImgBtn4: TJvImgBtn;
    TabSheet4: TTabSheet;
    ComboBox2: TComboBox;
    Label26: TLabel;
    Label27: TLabel;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    Label28: TLabel;
    Label29: TLabel;
    ComboBox5: TComboBox;
    GroupBox3: TGroupBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    GroupBox4: TGroupBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    procedure UstawSiatke;
    procedure JvImgBtn1Click(Sender: TObject);
    procedure DataPrzystapieniaClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBox2Change(Sender: TObject);
    procedure ComboBox2Click(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nr: Integer;
  end;

var
  frDodajK: TfrDodajK;
  nr: Integer;
implementation
uses baza,glowny;
{$R *.dfm}

procedure TfrDodajK.JvImgBtn1Click(Sender: TObject);
var
   koncesja,a,b,c,t1,t2,t3,t4: String;
   nr_trasy,nr_pracownik: Integer;
begin
if CheckBox4.Checked then a := 'A'; if CheckBox5.Checked then b := 'B'; if CheckBox6.Checked then c := 'C';
koncesja := a + b + c;
t1 := 'NR_TRASY,NR_PRACOWNIK,RODZAJ,IMIE_NAZWISKO,NAZWA,NIP,ULICA,MIASTO,KOD_P,DATA_PRZYSTAPIENIA,';
t2 := 'TELEFON,KONCESJA,METRAZ,KATEGORIA,UWAGI,AKTYWNY,TYDZIEN_1,TYDZIEN_2,TYDZIEN_3,TYDZIEN_4,TYDZIEN_5';
t3 := ':NR_TRASY,:NR_PRACOWNIK,:RODZAJ,:IMIE_NAZWISKO,:NAZWA,:NIP,:ULICA,:MIASTO,:KOD_P,:DATA_PRZYSTAPIENIA,';
t4 := ':TELEFON,:KONCESJA,:METRAZ,:KATEGORIA,:UWAGI,:AKTYWNY,:T1,:T2,:T3,:T4,:T5';
nr_trasy := dm.PokazTraseF(ComboBox3.Text);
nr_pracownik := dm.PokazPracownikF(ComboBox4.Text);
if not dm.ibtZapisz.InTransaction then dm.ibtZapisz.StartTransaction;
with dm.ibqZapisz do
begin
     Close;
     SQL.Clear;
     SQL.Add('INSERT INTO KONTRAH(' + t1 + t2 + ')');
     SQL.Add('VALUES(' + t3 + t4 + ')');
     UnPrepare;
          case rgTypK_N.ItemIndex of
               0: ParamByName('RODZAJ').AsString:='D';
               1: ParamByName('RODZAJ').AsString:='O';
          end;
          ParamByName('NR_TRASY').AsInteger := nr_trasy;
          ParamByName('NR_PRACOWNIK').AsInteger := nr_pracownik;
          ParamByName('IMIE_NAZWISKO').AsString := UpperCase(JvEdit1.Text);
          ParamByName('NAZWA').AsString := UpperCase(JvEdit2.Text);
          ParamByName('NIP').AsString := JvEdit7.Text;
          ParamByName('ULICA').AsString := UpperCase(JvEdit5.Text);
          ParamByName('MIASTO').AsString := UpperCase(JvEdit4.Text);
          ParamByName('KOD_P').AsString := JvEdit3.Text;
          ParamByName('DATA_PRZYSTAPIENIA').AsDate := DateTimePicker1.Date;
          ParamByName('TELEFON').AsString := JvEdit6.Text;
          ParamByName('KONCESJA').AsString := koncesja;
          ParamByName('METRAZ').AsString := JvEdit9.Text;
          ParamByName('KATEGORIA').AsString := UpperCase(JvEdit8.Text);
          ParamByName('UWAGI').AsString := Memo2.Text;
          if CheckBox13.Checked then ParamByName('T1').AsString := 'T';
          if CheckBox14.Checked then ParamByName('T2').AsString := 'T';
          if CheckBox15.Checked then ParamByName('T3').AsString := 'T';
          if CheckBox16.Checked then ParamByName('T4').AsString := 'T';
          if CheckBox17.Checked then ParamByName('T5').AsString := 'T';
          if CheckBox7.Checked = True then ParamByName('AKTYWNY').AsInteger := 1
          else ParamByName('AKTYWNY').AsInteger := 0;
     Prepare;
     Open;
     dm.PokazKontrah(0,'','');
end;

if dm.ibtZapisz.InTransaction then dm.ibtZapisz.Commit;

with dm.ibqZapisz do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    Open;
  end;
ComboBox3.Clear;ComboBox4.Clear;
dm.serwer.Connected := False;
dm.serwer.Connected := True;
Close;
end;

procedure TfrDodajK.DataPrzystapieniaClick(Sender: TObject);
begin
DataPrzystapienia.Date := now();
end;

procedure TfrDodajK.UstawSiatke;
begin
with Siatka do
     begin
          //Columns.Items[0].FieldName := 'NR_CRM';
          Columns.Items[0].Visible := False;
          //Columns.Items[1].FieldName := 'NR_KONTRAH';
          Columns.Items[1].Visible := False;
          //Columns.Items[2].FieldName := 'DATA_UTWORZENIA';
          Columns.Items[2].Title.Caption := 'Data notatki';
          Columns.Items[2].Width := 80;
          //Columns.Items[3].FieldName := 'KTO_UTWORZYL';
          Columns.Items[3].Title.Caption := 'Kto wpisa�';
          Columns.Items[3].Width := 80;
          //Columns.Items[4].FieldName := 'KANAL';
          Columns.Items[4].Title.Caption := 'Informacja z ...';
          Columns.Items[4].Width := 80;
          //Columns.Items[5].FieldName := 'NOTATKA';
          Columns.Items[5].Title.Caption := 'Notatka';
          //Columns.Items[5].Width := 200;
     end;
end;

procedure TfrDodajK.Button1Click(Sender: TObject);
var
   nr_kontrahenta: Integer;
begin
nr_kontrahenta := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
dm.ZapiszCRM(nr_kontrahenta,'admin',ComboBox1.Text,Memo1.Text);
dm.OdswiezBazy;
dm.PokazCRM(nr_kontrahenta);
dm.PokazKontrah(0,'','');
UstawSiatke;
Memo1.Clear;
Close;
end;

procedure TfrDodajK.Memo1Change(Sender: TObject);
var
   tresc: String;
begin
nr := nr - 1;
tresc := 'Maksymalna ilo�� znak�w 255, zosta�o ';
label13.Caption := tresc + IntToStr(nr);
end;

procedure TfrDodajK.JvImgBtn2Click(Sender: TObject);
var
   koncesja,a,b,c: String;
   nr,nr_trasy,nr_pracownik: Integer;
begin

if CheckBox1.Checked then a := 'A'; if CheckBox2.Checked then b := 'B'; if CheckBox3.Checked then c := 'C';
koncesja := a + b + c;
nr := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
nr_trasy := dm.PokazTraseF(ComboBox2.Text);
nr_pracownik := dm.PokazPracownikF(ComboBox5.Text);
if not dm.ibtZapisz.InTransaction then dm.ibtZapisz.StartTransaction;
with dm.ibqZapisz do
begin
     Close;
     SQL.Clear;
     SQL.Add('UPDATE KONTRAH');
     SQL.Add('SET NR_TRASY=:NR_TRASY,NR_PRACOWNIK=:NR_PRACOWNIK,RODZAJ=:RODZAJ,IMIE_NAZWISKO=:IMIE_NAZWISKO,NAZWA=:NAZWA,NIP=:NIP,ULICA=:ULICA,MIASTO=:MIASTO,');
     SQL.Add('KOD_P = :KOD_P,DATA_PRZYSTAPIENIA = :DATA_PRZYSTAPIENIA,TELEFON = :TELEFON,KONCESJA = :KONCESJA,');
     SQL.Add('METRAZ = :METRAZ,KATEGORIA = :KATEGORIA,UWAGI = :UWAGI,AKTYWNY = :AKTYWNY,TYDZIEN_1 = :T1,TYDZIEN_2 = :T2,TYDZIEN_3 = :T3,TYDZIEN_4 = :T4,TYDZIEN_5 = :T5');
     SQL.Add('WHERE NR_KONTRAH = :A');

     UnPrepare;
          case rgTypK.ItemIndex of
               0: ParamByName('RODZAJ').AsString:='D';
               1: ParamByName('RODZAJ').AsString:='O';
          end;
          ParamByName('NR_TRASY').AsInteger := nr_trasy;
          ParamByName('NR_PRACOWNIK').AsInteger := nr_pracownik;
          ParamByName('A').AsInteger := nr;
          ParamByName('IMIE_NAZWISKO').AsString := UpperCase(edImieNaz.Text);
          ParamByName('NAZWA').AsString := UpperCase(edNazwa.Text);
          ParamByName('NIP').AsString := edNIP.Text;
          ParamByName('ULICA').AsString := UpperCase(edUlica.Text);
          ParamByName('MIASTO').AsString := UpperCase(edMiasto.Text);
          ParamByName('KOD_P').AsString := edKodP.Text;
          ParamByName('DATA_PRZYSTAPIENIA').AsDate := DataPrzystapienia.Date;
          ParamByName('TELEFON').AsString := edTelefon.Text;
          ParamByName('KONCESJA').AsString := koncesja;
          ParamByName('METRAZ').AsString := edMetraz.Text;
          ParamByName('KATEGORIA').AsString := UpperCase(edKategoria.Text);
          if CheckBox8.Checked then ParamByName('T1').AsString := 'T';
          if CheckBox9.Checked then ParamByName('T2').AsString := 'T';
          if CheckBox10.Checked then ParamByName('T3').AsString := 'T';
          if CheckBox11.Checked then ParamByName('T4').AsString := 'T';
          if CheckBox12.Checked then ParamByName('T5').AsString := 'T';
          ParamByName('UWAGI').AsString := mUwagi.Text;
          if CzyAktywny.Checked = True then ParamByName('AKTYWNY').AsInteger := 1
          else ParamByName('AKTYWNY').AsInteger := 0;
     Prepare;
     Open;
if dm.ibtZapisz.InTransaction then dm.ibtZapisz.Commit;
dm.OdswiezBazy;
dm.PokazKontrah(0,'','');
ComboBox2.Clear; ComboBox5.Clear;
frDodajK.Close;

end;

end;

procedure TfrDodajK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
CheckBox1.Checked := False;CheckBox2.Checked := False;CheckBox3.Checked := False;
ComboBox2.Clear; ComboBox5.Clear;
end;

procedure TfrDodajK.ComboBox2Change(Sender: TObject);
var
   pracownik: String;
begin
dm.PokazPracownikaTrasy(dm.PokazTraseF(ComboBox2.Text));
if not dm.ibqTrasy.FieldByName('NR_TRASY').IsNull then
   begin
        pracownik := dm.PracownikF(dm.ibqTrasy.FieldValues['NR_TRASY']);
        ComboBox5.Items.Add(pracownik);
   end
else
      ComboBox5.Items.Text := 'Brak pozycji';

end;

procedure TfrDodajK.ComboBox2Click(Sender: TObject);
begin
ComboBox5.Clear;
end;

procedure TfrDodajK.ComboBox3Change(Sender: TObject);
var
   pracownik: String;
begin
dm.PokazPracownikaTrasy(dm.PokazTraseF(ComboBox3.Text));
if not dm.ibqTrasy.FieldByName('NR_TRASY').IsNull then
   begin
        pracownik := dm.PracownikF(dm.ibqTrasy.FieldValues['NR_TRASY']);
        ComboBox4.Items.Add(pracownik);
   end
else
      ComboBox4.Items.Text := 'Brak pozycji';

end;

end.
