object frRaportObrSklepu: TfrRaportObrSklepu
  Left = 192
  Top = 125
  Width = 1142
  Height = 656
  Caption = 'Obroty sklepu'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ibqPokazObrot: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from P_OBR_SKLEP_OKRESY(:A)'
      'where OKRES_M like :B or OKRES_Q like :C')
    Left = 936
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'A'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'B'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'C'
        ParamType = ptUnknown
      end>
    object ibqPokazObrotDOSTAWCA: TIBStringField
      FieldName = 'DOSTAWCA'
      Origin = 'OBROTY.NAZWA_D'
      Required = True
      Size = 250
    end
    object ibqPokazObrotOBROTY: TIBBCDField
      FieldName = 'OBROTY'
      Origin = 'OBROTY.OBROTY'
      Required = True
      DisplayFormat = '# ##0.00 z'#322
      Precision = 18
      Size = 2
    end
    object ibqPokazObrotPROWIZJA_PROC: TFloatField
      FieldName = 'PROWIZJA_PROC'
      Origin = 'OBROTY.PROWIZJA_PROC'
      Required = True
      DisplayFormat = '0.00 %'
    end
    object ibqPokazObrotPROWIZJA_KWOTA: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'PROWIZJA_KWOTA'
      Origin = 'OBROTY.PROWIZJA_KWOTA'
      ReadOnly = True
      DisplayFormat = '# ##0.00 z'#322
    end
    object ibqPokazObrotOKRES_M: TIBStringField
      FieldName = 'OKRES_M'
      Origin = 'OBROTY.OKRES_M'
    end
    object ibqPokazObrotOKRES_Q: TIBStringField
      FieldName = 'OKRES_Q'
      Origin = 'OBROTY.OKRES_Q'
    end
  end
  object dsPokazObrot: TDataSource
    DataSet = ibqPokazObrot
    Left = 1008
    Top = 16
  end
end
