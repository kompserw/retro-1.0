unit KreaotorRap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExStdCtrls, JvButton, JvCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid, ExtCtrls, ComCtrls, PageControlEx, StrUtils,
  FileCtrl, JvDriveCtrls, JvListBox, JvCombobox;

type
  TfrKreatorRaportow = class(TForm)
    PageControlEx1: TPageControlEx;
    sb: TStatusBar;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    JvImgBtn1: TJvImgBtn;
    JvImgBtn2: TJvImgBtn;
    JvImgBtn4: TJvImgBtn;
    JvImgBtn5: TJvImgBtn;
    JvImgBtn6: TJvImgBtn;
    TabSheet4: TTabSheet;
    JvDriveCombo1: TJvDriveCombo;
    JvDirectoryListBox1: TJvDirectoryListBox;
    JvFileListBox1: TJvFileListBox;
    JvImgBtn7: TJvImgBtn;
    JvImgBtn3: TJvImgBtn;
    Edit1: TEdit;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    GroupBox2: TGroupBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    CheckBox18: TCheckBox;
    CheckBox19: TCheckBox;
    CheckBox20: TCheckBox;
    CheckBox21: TCheckBox;
    CheckBox22: TCheckBox;
    CheckBox23: TCheckBox;
    CheckBox24: TCheckBox;
    CheckBox25: TCheckBox;
    CheckBox26: TCheckBox;
    RadioGroup1: TRadioGroup;
    procedure leSzukajNazwaChange(Sender: TObject);
    procedure leSzukajNazwiskoChange(Sender: TObject);
    procedure leSzukajNIPChange(Sender: TObject);
    procedure leSzukajNazwaClick(Sender: TObject);
    procedure leSzukajNazwiskoClick(Sender: TObject);
    procedure rgMiesiaceClick(Sender: TObject);
    procedure rgKwartalyClick(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure JvImgBtn4Click(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvImgBtn7Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox25Click(Sender: TObject);
    procedure CheckBox26Click(Sender: TObject);
  private
    { Private declarations }
    okres: Integer;
  public
    { Public declarations }
    procedure UstawSiatke(aGrid: TDBGrid);
    procedure DrukujWszystkie(miesiac: String;mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer);
    procedure DrukujWszystkieExcel(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer);
  end;

var
  frKreatorRaportow: TfrKreatorRaportow;
  //okres: Integer;
implementation
uses glowny,baza,Rap_Importy,testy;
{$R *.dfm}

procedure TfrKreatorRaportow.leSzukajNazwaChange(Sender: TObject);
var
   pole: TLabeledEdit;
begin
   pole := TLabeledEdit(Sender);
if pole.Name = 'LabeledEdit1' then
   begin
        dm.SzukajDostawcy(UpperCase(pole.Text),'','');

   end
else
    begin
         //dm.SzukajOdbiorcy(UpperCase(leSzukajNazwa.Text),'','');
         //UstawSiatke(2);
    end;
end;

procedure TfrKreatorRaportow.leSzukajNazwiskoChange(Sender: TObject);
var
   pole: TLabeledEdit;
begin
   pole := TLabeledEdit(Sender);
if pole.Name = 'LabeledEdit2' then
   begin
        dm.SzukajDostawcy('',UpperCase(pole.Text),'');

   end
else
    begin
         //dm.SzukajOdbiorcy('',UpperCase(leSzukajNazwisko.Text),'');
         //UstawSiatke(1);
    end;
end;

procedure TfrKreatorRaportow.leSzukajNIPChange(Sender: TObject);
var
   pole: TLabeledEdit;
begin
   pole := TLabeledEdit(Sender);
if pole.Name = 'LabeledEdit3' then
   begin
        dm.SzukajDostawcy('','',pole.Text);

   end
else
    begin
         //dm.SzukajOdbiorcy('','',leSzukajNIP.Text);
         //UstawSiatke(3);
    end;
end;

procedure TfrKreatorRaportow.leSzukajNazwaClick(Sender: TObject);
begin
{leSzukajNIP.Text := '';
leSzukajNazwisko.Text := '';
LabeledEdit2.Text := '';
LabeledEdit3.Text := '';   }
end;

procedure TfrKreatorRaportow.leSzukajNazwiskoClick(Sender: TObject);
begin
{leSzukajNazwa.Text := '';
leSzukajNIP.Text := '';
LabeledEdit1.Text := '';
LabeledEdit3.Text := ''; }
end;

procedure TfrKreatorRaportow.rgMiesiaceClick(Sender: TObject);
begin

okres := 1;
end;

procedure TfrKreatorRaportow.rgKwartalyClick(Sender: TObject);
begin

okres := 2;
end;

procedure TfrKreatorRaportow.JvImgBtn1Click(Sender: TObject);                    //drukuj raporty
var
   miesiac,kwartal,raporty: String;
   odbiorca,x,y,mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer;

begin
raporty  := ExtractFilePath(Application.ExeName) + '/raporty/obroty sklep�w r�ne miesi�ce.fr3';
mi1 := 0;mi2 := 0;mi3 := 0;mi4 := 0;mi1 := 0;mi5 := 0;mi6 := 0;mi7 := 0;mi8 := 0;mi9 := 0;mi10 := 0;mi11 := 0;
mi12 := 0;
if CheckBox1.Checked then
   begin
        mi1 := 1;
        miesiac := miesiac + '1 ';
   end;
if CheckBox2.Checked then
   begin
        mi2 := 2;
        miesiac := miesiac + '2 ';
   end;
if CheckBox3.Checked then
   begin
        mi3 := 3;
        miesiac := miesiac + '3 ';
   end;
if CheckBox4.Checked then
   begin
        mi4 := 4;
        miesiac := miesiac + '4 ';
   end;
if CheckBox5.Checked then
   begin
        mi5 := 5;
        miesiac := miesiac + '5 ';
   end;
if CheckBox6.Checked then
   begin
        mi6 := 6;
        miesiac := miesiac + '6 ';
   end;
if CheckBox7.Checked then
   begin
        mi7 := 7;
        miesiac := miesiac + '7 ';
   end;
if CheckBox8.Checked then
   begin
        mi8 := 8;
        miesiac := miesiac + '8 ';
   end;
if CheckBox9.Checked then
   begin
        mi9 := 9;
        miesiac := miesiac + '9 ';
   end;
if CheckBox10.Checked then
   begin
        mi10 := 10;
        miesiac := miesiac + '10 ';
   end;
if CheckBox11.Checked then
   begin
        mi11 := 11;
        miesiac := miesiac + '11 ';
   end;
if CheckBox12.Checked then
   begin
        mi12 := 12;
        miesiac := miesiac + '12 ';
   end;
miesiac := Trim(miesiac);
miesiac := StringReplace(miesiac,' ',',',[rfReplaceAll]);
case RadioGroup1.ItemIndex of
     0: DrukujWszystkie(miesiac,mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12);
     1: DrukujWszystkieExcel(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12);
     else
         if FileExists(raporty) then
            begin
                 rpImporty.DrukujObrotyOdbiorcy(odbiorca,mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12);
                 rpImporty.frxReport1.LoadFromFile(raporty,False);
                 rpImporty.frxReport1.PrepareReport(True);
                 if not dm.ibqKontrahenci.FieldByName('IMIE_NAZWISKO').IsNull then
                    begin
                      rpImporty.frxReport1.Variables['sklep'] := '''' + dm.ibqKontrahenci.FieldValues['IMIE_NAZWISKO'] + '''';
                      rpImporty.frxReport1.Variables['okres'] := '''' + 'Obroty za miesi�ce ' + miesiac + '''';
                    end
                 else
                     rpImporty.frxReport1.Variables['sklep'] := '''' + 'brak' + '''';
                 rpImporty.frxReport1.ShowReport(True);
            end
         else
             ShowMessage('Brak raportu');
     end;
     RadioGroup1.ItemIndex := -1;
end;

procedure TfrKreatorRaportow.JvImgBtn4Click(Sender: TObject);
var
   miesiac,kwartal,raporty: String;
   odbiorca,x,y,mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer;

begin
raporty  := ExtractFilePath(Application.ExeName) + '/raporty/importy dostawc�w ostateczna.fr3';
mi1 := 0;mi2 := 0;mi3 := 0;mi4 := 0;mi1 := 0;mi5 := 0;mi6 := 0;mi7 := 0;mi8 := 0;mi9 := 0;mi10 := 0;mi11 := 0;
mi12 := 0;
if CheckBox13.Checked then
   begin
        mi1 := 1;
        miesiac := miesiac + '1 ';
   end;
if CheckBox14.Checked then
   begin
        mi2 := 2;
        miesiac := miesiac + '2 ';
   end;
if CheckBox15.Checked then
   begin
        mi3 := 3;
        miesiac := miesiac + '3 ';
   end;
if CheckBox16.Checked then
   begin
        mi4 := 4;
        miesiac := miesiac + '4 ';
   end;
if CheckBox17.Checked then
   begin
        mi5 := 5;
        miesiac := miesiac + '5 ';
   end;
if CheckBox18.Checked then
   begin
        mi6 := 6;
        miesiac := miesiac + '6 ';
   end;
if CheckBox19.Checked then
   begin
        mi7 := 7;
        miesiac := miesiac + '7 ';
   end;
if CheckBox20.Checked then
   begin
        mi8 := 8;
        miesiac := miesiac + '8 ';
   end;
if CheckBox21.Checked then
   begin
        mi9 := 9;
        miesiac := miesiac + '9 ';
   end;
if CheckBox22.Checked then
   begin
        mi10 := 10;
        miesiac := miesiac + '10 ';
   end;
if CheckBox23.Checked then
   begin
        mi11 := 11;
        miesiac := miesiac + '11 ';
   end;
if CheckBox24.Checked then
   begin
        mi12 := 12;
        miesiac := miesiac + '12 ';
   end;
miesiac := Trim(miesiac);
miesiac := StringReplace(miesiac,' ',',',[rfReplaceAll]);
if FileExists(raporty) then
   begin
        rpImporty.DrukujObrotyDostawcy(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12);
        rpImporty.frxReport1.LoadFromFile(raporty,False);
        rpImporty.frxReport1.Variables['okres'] := '''' + 'Obroty za miesi�ce ' + miesiac + '''';
        rpImporty.frxReport1.PrepareReport(True);
        rpImporty.frxReport1.ShowReport(True);
   end;
end;

procedure TfrKreatorRaportow.JvImgBtn3Click(Sender: TObject);
begin
rpImporty.frxReport1.DesignReport(True,False);
end;

procedure TfrKreatorRaportow.FormCreate(Sender: TObject);
begin
PageControlEx1.ActivePageIndex := 0;
end;

procedure TfrKreatorRaportow.JvImgBtn7Click(Sender: TObject);
begin

rpImporty.frxReport1.LoadFromFile(JvFileListBox1.FileName);
rpImporty.frxReport1.ShowReport;
end;

procedure TfrKreatorRaportow.Edit1Change(Sender: TObject);
var
   zmienna: Integer;
begin
if TryStrToInt(Edit1.Text,zmienna) then
   dm.SzukajOdbiorcy('','',IntToStr(zmienna))
else
    dm.SzukajOdbiorcy('',Edit1.Text,'')
end;



procedure TfrKreatorRaportow.UstawSiatke(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa firmy';
          lColumn.Width := 300;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'IMIE_NAZWISKO';
          lColumn.Title.Caption := 'Imi� i nazwisko';
          lColumn.Width := 150;
     end;

end;

procedure TfrKreatorRaportow.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
Edit1.Text := '';
dm.SzukajOdbiorcy('','','');
PageControlEx1.Pages[3].TabVisible := False;
end;

procedure TfrKreatorRaportow.CheckBox25Click(Sender: TObject);
begin
if CheckBox25.Checked then
   begin
        CheckBox1.Checked := True;CheckBox2.Checked := True;CheckBox3.Checked := True;
        CheckBox4.Checked := True;CheckBox5.Checked := True;CheckBox6.Checked := True;
        CheckBox7.Checked := True;CheckBox8.Checked := True;CheckBox9.Checked := True;
        CheckBox10.Checked := True;CheckBox11.Checked := True;CheckBox12.Checked := True;
   end
else
    begin
        CheckBox1.Checked := False;CheckBox2.Checked := False;CheckBox3.Checked := False;
        CheckBox4.Checked := False;CheckBox5.Checked := False;CheckBox6.Checked := False;
        CheckBox7.Checked := False;CheckBox8.Checked := False;CheckBox9.Checked := False;
        CheckBox10.Checked := False;CheckBox11.Checked := False;CheckBox12.Checked := False;
    end;
end;

procedure TfrKreatorRaportow.CheckBox26Click(Sender: TObject);
begin
if CheckBox26.Checked then
   begin
        CheckBox13.Checked := True;CheckBox14.Checked := True;CheckBox15.Checked := True;
        CheckBox16.Checked := True;CheckBox17.Checked := True;CheckBox18.Checked := True;
        CheckBox19.Checked := True;CheckBox20.Checked := True;CheckBox21.Checked := True;
        CheckBox22.Checked := True;CheckBox23.Checked := True;CheckBox24.Checked := True;
   end
else
    begin
        CheckBox13.Checked := False;CheckBox14.Checked := False;CheckBox15.Checked := False;
        CheckBox16.Checked := False;CheckBox17.Checked := False;CheckBox18.Checked := False;
        CheckBox19.Checked := False;CheckBox20.Checked := False;CheckBox21.Checked := False;
        CheckBox22.Checked := False;CheckBox23.Checked := False;CheckBox24.Checked := False;
    end;
end;

procedure TfrKreatorRaportow.DrukujWszystkie(miesiac: String; mi1, mi2,
  mi3, mi4, mi5, mi6, mi7, mi8, mi9, mi10, mi11, mi12: Integer);
//DRUKUJE OBROTY WSZYSTKICH SKLEPOW W OKRESIE
var
   raporty: String;  
begin
raporty  := ExtractFilePath(Application.ExeName) + '/raporty/01zestawienieAll.fr3';
if FileExists(raporty) then
   begin
        rpImporty.DrukujObrotySklepowAll(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12);
        rpImporty.frxReport1.LoadFromFile(raporty,False);
        rpImporty.frxReport1.Variables['okres'] := '''' + miesiac + '''';
        rpImporty.frxReport1.PrepareReport(True);
        rpImporty.frxReport1.ShowReport(True);
   end;
end;

procedure TfrKreatorRaportow.DrukujWszystkieExcel(mi1, mi2, mi3, mi4, mi5,
  mi6, mi7, mi8, mi9, mi10, mi11, mi12: Integer);
//DRUKUJE OBROTY WSZYSTKICH SKLEPOW W OKRESIE EXCEL
var
   raporty: String;  
begin
raporty  := ExtractFilePath(Application.ExeName) + '/raporty/01zestawienieAll_Excel.fr3';
if FileExists(raporty) then
   begin
        rpImporty.DrukujObrotySklepowAllExcel(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12);
        rpImporty.frxReport1.LoadFromFile(raporty,False);
        rpImporty.frxReport1.PrepareReport(True);
        rpImporty.frxReport1.ShowReport(True);
   end;
end;

end.

