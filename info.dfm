object frInfo: TfrInfo
  Left = 720
  Top = 242
  Cursor = crArrow
  BorderIcons = [biSystemMenu, biHelp]
  BorderStyle = bsSingle
  Caption = #39#39
  ClientHeight = 383
  ClientWidth = 786
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pc: TPageControlEx
    Left = 0
    Top = 0
    Width = 786
    Height = 364
    ActivePage = WybierzKontrach
    Align = alClient
    TabOrder = 0
    HideHeader = False
    object WybierzKontrach: TTabSheet
      Caption = 'Wybierz dostawc'#281
      object ToolBar1: TToolBar
        Left = 0
        Top = 0
        Width = 778
        Height = 29
        ButtonHeight = 21
        Caption = 'ToolBar1'
        TabOrder = 0
        object eNazwaD: TEdit
          Left = 0
          Top = 2
          Width = 193
          Height = 21
          TabOrder = 0
          Text = 'Wpisz nazw'#281' dostawcy'
          OnChange = eNazwaDChange
          OnClick = eNazwaDClick
          OnDblClick = eNazwaDDblClick
        end
        object DBNavigator1: TDBNavigator
          Left = 193
          Top = 2
          Width = 224
          Height = 21
          DataSource = dsSzukajD
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          TabOrder = 1
        end
      end
      object dbSiatka: TDBGrid
        Left = 0
        Top = 29
        Width = 778
        Height = 307
        Align = alClient
        DataSource = dsSzukajD
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnCellClick = dbSiatkaCellClick
      end
    end
    object AnalizujSklepy: TTabSheet
      Caption = 'Analiza importu'
      ImageIndex = 1
      object ToolBar2: TToolBar
        Left = 0
        Top = 0
        Width = 778
        Height = 29
        ButtonHeight = 16
        Caption = 'ToolBar2'
        TabOrder = 0
        object lSuma: TLabel
          Left = 0
          Top = 2
          Width = 761
          Height = 16
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object mSiatka: TJvStringGrid
        Left = 0
        Top = 29
        Width = 778
        Height = 307
        Align = alClient
        ColCount = 6
        FixedColor = clSilver
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        TabOrder = 1
        Alignment = taLeftJustify
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = []
      end
    end
    object DodajKontrahenta: TTabSheet
      Caption = 'Dodawanie kontrahent'#243'w'
      ImageIndex = 2
      object GroupBox1: TGroupBox
        Left = 8
        Top = 15
        Width = 185
        Height = 249
        Caption = 'Lista NIP'#39#243'w bez powi'#261'za'#324
        TabOrder = 0
        object lbNIP: TListBox
          Left = 8
          Top = 32
          Width = 169
          Height = 201
          ExtendedSelect = False
          ItemHeight = 13
          TabOrder = 0
          OnClick = lbNIPClick
        end
      end
      object GroupBox2: TGroupBox
        Left = 200
        Top = 16
        Width = 569
        Height = 313
        Caption = 'Dane kontrahenta'
        TabOrder = 1
        object JvLabel1: TJvLabel
          Left = 144
          Top = 96
          Width = 92
          Height = 13
          Caption = 'Data przyst'#261'pienia'
        end
        object JvLabel2: TJvLabel
          Left = 336
          Top = 240
          Width = 31
          Height = 13
          Caption = 'Uwagi'
        end
        object Label28: TLabel
          Left = 144
          Top = 176
          Width = 111
          Height = 13
          Caption = 'Obs'#322'uguj'#261'cy pracownik'
        end
        object Label27: TLabel
          Left = 144
          Top = 136
          Width = 60
          Height = 13
          Caption = 'Nazwa trasy'
        end
        object leImieN: TLabeledEdit
          Left = 8
          Top = 32
          Width = 121
          Height = 21
          EditLabel.Width = 70
          EditLabel.Height = 13
          EditLabel.Caption = 'Nazwisko i imi'#281
          TabOrder = 0
        end
        object leNazwa: TLabeledEdit
          Left = 136
          Top = 32
          Width = 305
          Height = 21
          EditLabel.Width = 59
          EditLabel.Height = 13
          EditLabel.Caption = 'Nazwa firmy'
          TabOrder = 1
        end
        object leNIP: TLabeledEdit
          Left = 448
          Top = 32
          Width = 105
          Height = 21
          EditLabel.Width = 17
          EditLabel.Height = 13
          EditLabel.Caption = 'NIP'
          TabOrder = 2
        end
        object leUlica: TLabeledEdit
          Left = 8
          Top = 72
          Width = 121
          Height = 21
          EditLabel.Width = 22
          EditLabel.Height = 13
          EditLabel.Caption = 'Ulica'
          TabOrder = 3
        end
        object leMiasto: TLabeledEdit
          Left = 144
          Top = 72
          Width = 121
          Height = 21
          EditLabel.Width = 31
          EditLabel.Height = 13
          EditLabel.Caption = 'Miasto'
          TabOrder = 4
        end
        object leKodP: TLabeledEdit
          Left = 280
          Top = 72
          Width = 65
          Height = 21
          EditLabel.Width = 67
          EditLabel.Height = 13
          EditLabel.Caption = 'Kod pocztowy'
          TabOrder = 5
        end
        object leTelefon: TLabeledEdit
          Left = 352
          Top = 72
          Width = 89
          Height = 21
          EditLabel.Width = 36
          EditLabel.Height = 13
          EditLabel.Caption = 'Telefon'
          TabOrder = 6
        end
        object leKoncesja: TLabeledEdit
          Left = 448
          Top = 72
          Width = 105
          Height = 21
          EditLabel.Width = 43
          EditLabel.Height = 13
          EditLabel.Caption = 'Koncesja'
          TabOrder = 7
        end
        object leMetraz: TLabeledEdit
          Left = 8
          Top = 112
          Width = 41
          Height = 21
          EditLabel.Width = 33
          EditLabel.Height = 13
          EditLabel.Caption = 'Matra'#380
          TabOrder = 8
        end
        object leKategoria: TLabeledEdit
          Left = 56
          Top = 112
          Width = 65
          Height = 21
          EditLabel.Width = 46
          EditLabel.Height = 13
          EditLabel.Caption = 'Kategoria'
          TabOrder = 9
        end
        object dpeDataP: TJvDatePickerEdit
          Left = 144
          Top = 112
          Width = 121
          Height = 21
          AllowNoDate = True
          Checked = True
          TabOrder = 10
        end
        object mUwagi: TJvMemo
          Left = 8
          Top = 256
          Width = 361
          Height = 49
          Lines.Strings = (
            '')
          TabOrder = 11
        end
        object JvImgBtn1: TJvImgBtn
          Left = 384
          Top = 248
          Width = 169
          Height = 57
          Caption = 'Zapisz do bazy'
          TabOrder = 12
          OnClick = JvImgBtn1Click
          Flat = True
          Images = frGlowny.JvImageList1
          ImageIndex = 4
        end
        object GroupBox4: TGroupBox
          Left = 384
          Top = 104
          Width = 169
          Height = 137
          Caption = 'Harmonogram wizyt'
          TabOrder = 13
          object CheckBox13: TCheckBox
            Left = 8
            Top = 16
            Width = 97
            Height = 17
            Caption = 'I tydzie'#324
            TabOrder = 0
          end
          object CheckBox14: TCheckBox
            Left = 8
            Top = 39
            Width = 97
            Height = 17
            Caption = 'II tydzie'#324
            TabOrder = 1
          end
          object CheckBox15: TCheckBox
            Left = 8
            Top = 63
            Width = 97
            Height = 17
            Caption = 'III tydzie'#324
            TabOrder = 2
          end
          object CheckBox16: TCheckBox
            Left = 8
            Top = 88
            Width = 97
            Height = 17
            Caption = 'IV tydzie'#324
            TabOrder = 3
          end
          object CheckBox17: TCheckBox
            Left = 8
            Top = 115
            Width = 97
            Height = 17
            Caption = 'V tydzie'#324
            TabOrder = 4
          end
        end
        object ComboBox3: TComboBox
          Left = 144
          Top = 152
          Width = 193
          Height = 21
          ItemHeight = 13
          TabOrder = 14
        end
        object ComboBox4: TComboBox
          Left = 144
          Top = 192
          Width = 193
          Height = 21
          ItemHeight = 13
          TabOrder = 15
        end
        object GroupBox3: TGroupBox
          Left = 8
          Top = 144
          Width = 100
          Height = 105
          Caption = 'Rodzaj koncesji'
          TabOrder = 16
          object CheckBox4: TCheckBox
            Left = 8
            Top = 24
            Width = 70
            Height = 17
            Caption = 'typ A'
            TabOrder = 0
          end
          object CheckBox5: TCheckBox
            Left = 8
            Top = 48
            Width = 70
            Height = 17
            Caption = 'typ B'
            TabOrder = 1
          end
          object CheckBox6: TCheckBox
            Left = 8
            Top = 72
            Width = 70
            Height = 17
            Caption = 'typ C'
            TabOrder = 2
          end
        end
      end
    end
  end
  object sb: TStatusBar
    Left = 0
    Top = 364
    Width = 786
    Height = 19
    Panels = <>
  end
  object ibqSzukajD: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 652
    Top = 24
  end
  object dsSzukajD: TDataSource
    DataSet = dm.ibqDostawca
    Left = 692
    Top = 24
  end
  object dsNowyK: TDataSource
    DataSet = ibqNowyK
    Left = 744
    Top = 24
  end
  object ibqNowyK: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from KONTRAH')
    Left = 612
    Top = 24
  end
end
