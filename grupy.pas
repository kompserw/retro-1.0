unit grupy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ComCtrls, PageControlEx, ImgList, ToolWin,
  StdCtrls, JvExStdCtrls, JvButton, JvCtrls, ExtCtrls;

type
  TfrGrupy = class(TForm)
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    PageControlEx1: TPageControlEx;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    GroupBox1: TGroupBox;
    LabeledEdit1: TLabeledEdit;
    RadioGroup1: TRadioGroup;
    JvImgBtn1: TJvImgBtn;
    JvImgBtn2: TJvImgBtn;
    GroupBox2: TGroupBox;
    LabeledEdit2: TLabeledEdit;
    RadioGroup2: TRadioGroup;
    JvImgBtn3: TJvImgBtn;
    GroupBox3: TGroupBox;
    LabeledEdit3: TLabeledEdit;
    RadioGroup3: TRadioGroup;
    procedure JvImgBtn1Click(Sender: TObject);
    procedure CzyscPola;
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frGrupy: TfrGrupy;

implementation
uses baza;
{$R *.dfm}
procedure TfrGrupy.CzyscPola;
begin
    LabeledEdit1.Text := '';
    LabeledEdit2.Text := '';
    LabeledEdit3.Text := '';
    RadioGroup1.ItemIndex := -1;
    RadioGroup2.ItemIndex := -1;
    RadioGroup3.ItemIndex := -1;
    PageControlEx1.ActivePageIndex := 0;
    dm.PokazGrupy;
end;

procedure TfrGrupy.JvImgBtn1Click(Sender: TObject);
begin
case RadioGroup1.ItemIndex of
     0: dm.Grupy(1,0,1,0,0,0,0,LabeledEdit1.Text);
     1: dm.Grupy(1,0,0,1,0,0,0,LabeledEdit1.Text);
     2: dm.Grupy(1,0,0,0,1,0,0,LabeledEdit1.Text);
     3: dm.Grupy(1,0,0,0,0,1,0,LabeledEdit1.Text);
end;
CzyscPola;
end;

procedure TfrGrupy.ToolButton2Click(Sender: TObject);
begin
PageControlEx1.ActivePageIndex := 1;
end;

procedure TfrGrupy.ToolButton3Click(Sender: TObject);
begin
PageControlEx1.ActivePageIndex := 2;
end;

procedure TfrGrupy.ToolButton4Click(Sender: TObject);
begin
PageControlEx1.ActivePageIndex := 3;
end;

end.
