object frKreatorRaportow: TfrKreatorRaportow
  Left = 543
  Top = 235
  Width = 462
  Height = 656
  Caption = 'Kreator raport'#243'w'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControlEx1: TPageControlEx
    Left = 0
    Top = 0
    Width = 446
    Height = 598
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    HideHeader = False
    object TabSheet1: TTabSheet
      Caption = 'Raporty klient'#243'w z obrotami '
      object Label1: TLabel
        Left = 16
        Top = 24
        Width = 93
        Height = 13
        Caption = 'Szukaj kontrahenta'
      end
      object JvImgBtn1: TJvImgBtn
        Left = 8
        Top = 504
        Width = 137
        Height = 49
        Caption = 'Drukuj raport'
        TabOrder = 0
        OnClick = JvImgBtn1Click
        Images = frGlowny.JvImageList1
        ImageIndex = 23
      end
      object JvImgBtn2: TJvImgBtn
        Left = 152
        Top = 504
        Width = 137
        Height = 49
        Caption = 'Export Excel'
        TabOrder = 1
        Visible = False
        Images = frGlowny.JvImageList1
        ImageIndex = 30
      end
      object Edit1: TEdit
        Left = 16
        Top = 40
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 2
        OnChange = Edit1Change
      end
      object DBGrid1: TDBGrid
        Left = 16
        Top = 88
        Width = 401
        Height = 120
        DataSource = dm.dsKontrah
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 240
        Width = 401
        Height = 105
        Caption = 'Wybierz miesi'#261'ce do wydruku'
        TabOrder = 4
        object CheckBox1: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = 'stycze'#324
          TabOrder = 0
        end
        object CheckBox2: TCheckBox
          Left = 16
          Top = 48
          Width = 97
          Height = 17
          Caption = 'luty'
          TabOrder = 1
        end
        object CheckBox3: TCheckBox
          Left = 16
          Top = 72
          Width = 97
          Height = 17
          Caption = 'marzec'
          TabOrder = 2
        end
        object CheckBox4: TCheckBox
          Left = 96
          Top = 24
          Width = 97
          Height = 17
          Caption = 'kwiecie'#324
          TabOrder = 3
        end
        object CheckBox5: TCheckBox
          Left = 96
          Top = 48
          Width = 97
          Height = 17
          Caption = 'maj'
          TabOrder = 4
        end
        object CheckBox6: TCheckBox
          Left = 96
          Top = 72
          Width = 97
          Height = 17
          Caption = 'czerwiec'
          TabOrder = 5
        end
        object CheckBox7: TCheckBox
          Left = 176
          Top = 24
          Width = 97
          Height = 17
          Caption = 'lipiec'
          TabOrder = 6
        end
        object CheckBox8: TCheckBox
          Left = 176
          Top = 48
          Width = 97
          Height = 17
          Caption = 'sierpie'#324
          TabOrder = 7
        end
        object CheckBox9: TCheckBox
          Left = 176
          Top = 72
          Width = 97
          Height = 17
          Caption = 'wrzesie'#324
          TabOrder = 8
        end
        object CheckBox10: TCheckBox
          Left = 264
          Top = 24
          Width = 97
          Height = 17
          Caption = 'pa'#378'dziernik'
          TabOrder = 9
        end
        object CheckBox11: TCheckBox
          Left = 264
          Top = 48
          Width = 97
          Height = 17
          Caption = 'listopad'
          TabOrder = 10
        end
        object CheckBox12: TCheckBox
          Left = 264
          Top = 72
          Width = 97
          Height = 17
          Caption = 'grudzie'#324
          TabOrder = 11
        end
      end
      object CheckBox25: TCheckBox
        Left = 16
        Top = 216
        Width = 153
        Height = 17
        Caption = 'Zaznacz wszystkie miesi'#261'ce'
        TabOrder = 5
        OnClick = CheckBox25Click
      end
      object RadioGroup1: TRadioGroup
        Left = 16
        Top = 360
        Width = 401
        Height = 65
        Caption = 'Inne raporty'
        Columns = 2
        Items.Strings = (
          'Raport wszystkich sklep'#243'w w okresie'
          'Obroty sklep'#243'w do Excela')
        TabOrder = 6
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Raporty dostawc'#243'w'
      ImageIndex = 1
      object JvImgBtn4: TJvImgBtn
        Left = 8
        Top = 504
        Width = 137
        Height = 49
        Caption = 'Drukuj raport'
        TabOrder = 0
        OnClick = JvImgBtn4Click
        Images = frGlowny.JvImageList1
        ImageIndex = 23
      end
      object JvImgBtn5: TJvImgBtn
        Left = 152
        Top = 504
        Width = 137
        Height = 49
        Caption = 'Export Excel'
        TabOrder = 1
        Visible = False
        Images = frGlowny.JvImageList1
        ImageIndex = 30
      end
      object JvImgBtn6: TJvImgBtn
        Left = 296
        Top = 504
        Width = 137
        Height = 49
        Caption = 'Export pdf'
        TabOrder = 2
        Visible = False
        Images = frGlowny.JvImageList1
        ImageIndex = 31
      end
      object GroupBox2: TGroupBox
        Left = 16
        Top = 40
        Width = 401
        Height = 105
        Caption = 'Wybierz miesi'#261'ce do wydruku'
        TabOrder = 3
        object CheckBox13: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = 'stycze'#324
          TabOrder = 0
        end
        object CheckBox14: TCheckBox
          Left = 16
          Top = 48
          Width = 97
          Height = 17
          Caption = 'luty'
          TabOrder = 1
        end
        object CheckBox15: TCheckBox
          Left = 16
          Top = 72
          Width = 97
          Height = 17
          Caption = 'marzec'
          TabOrder = 2
        end
        object CheckBox16: TCheckBox
          Left = 96
          Top = 24
          Width = 97
          Height = 17
          Caption = 'kwiecie'#324
          TabOrder = 3
        end
        object CheckBox17: TCheckBox
          Left = 96
          Top = 48
          Width = 97
          Height = 17
          Caption = 'maj'
          TabOrder = 4
        end
        object CheckBox18: TCheckBox
          Left = 96
          Top = 72
          Width = 97
          Height = 17
          Caption = 'czerwiec'
          TabOrder = 5
        end
        object CheckBox19: TCheckBox
          Left = 176
          Top = 24
          Width = 97
          Height = 17
          Caption = 'lipiec'
          TabOrder = 6
        end
        object CheckBox20: TCheckBox
          Left = 176
          Top = 48
          Width = 97
          Height = 17
          Caption = 'sierpie'#324
          TabOrder = 7
        end
        object CheckBox21: TCheckBox
          Left = 176
          Top = 72
          Width = 97
          Height = 17
          Caption = 'wrzesie'#324
          TabOrder = 8
        end
        object CheckBox22: TCheckBox
          Left = 264
          Top = 24
          Width = 97
          Height = 17
          Caption = 'pa'#378'dziernik'
          TabOrder = 9
        end
        object CheckBox23: TCheckBox
          Left = 264
          Top = 48
          Width = 97
          Height = 17
          Caption = 'listopad'
          TabOrder = 10
        end
        object CheckBox24: TCheckBox
          Left = 264
          Top = 72
          Width = 97
          Height = 17
          Caption = 'grudzie'#324
          TabOrder = 11
        end
      end
      object CheckBox26: TCheckBox
        Left = 16
        Top = 16
        Width = 153
        Height = 17
        Caption = 'zaznacz wszystkie miesi'#261'ce'
        TabOrder = 4
        OnClick = CheckBox26Click
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      TabVisible = False
    end
    object TabSheet4: TTabSheet
      Caption = 'Lista raport'#243'w'
      ImageIndex = 3
      object JvDriveCombo1: TJvDriveCombo
        Left = 8
        Top = 16
        Width = 145
        Height = 22
        DriveTypes = [dtFixed, dtRemote, dtCDROM]
        Offset = 4
        TabOrder = 0
      end
      object JvDirectoryListBox1: TJvDirectoryListBox
        Left = 8
        Top = 56
        Width = 417
        Height = 233
        Directory = 'C:\Users\Marcin\Documents\Euro'
        FileList = JvFileListBox1
        DriveCombo = JvDriveCombo1
        ItemHeight = 17
        TabOrder = 1
      end
      object JvFileListBox1: TJvFileListBox
        Left = 8
        Top = 304
        Width = 417
        Height = 97
        ItemHeight = 13
        TabOrder = 2
        ForceFileExtensions = False
      end
      object JvImgBtn7: TJvImgBtn
        Left = 80
        Top = 464
        Width = 145
        Height = 57
        Caption = 'Uruchom wybrany raport'
        TabOrder = 3
        OnClick = JvImgBtn7Click
        Flat = True
        Images = frGlowny.JvImageList1
        ImageIndex = 23
      end
      object JvImgBtn3: TJvImgBtn
        Left = 240
        Top = 464
        Width = 137
        Height = 57
        Caption = 'Edytor raport'#243'w'
        TabOrder = 4
        OnClick = JvImgBtn3Click
        Flat = True
        Images = frGlowny.JvImageList1
        ImageIndex = 21
      end
    end
  end
  object sb: TStatusBar
    Left = 0
    Top = 598
    Width = 446
    Height = 19
    Panels = <>
  end
end
