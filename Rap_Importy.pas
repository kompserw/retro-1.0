unit Rap_Importy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass, frxDBSet, frxPreview, frxDBXComponents,
  frxIBXComponents, frxExportXLS, frxExportPDF, frxCross, frxDesgn, DB,
  IBCustomDataSet, IBQuery, Grids, DBGrids, JvComponentBase, JvDBGridExport,
  Menus, frxExportXLSX;

type
  TrpImporty = class(TForm)
    frxReport1: TfrxReport;
    frxIBXComponents1: TfrxIBXComponents;
    frxPDFExport1: TfrxPDFExport;
    frxCrossObject1: TfrxCrossObject;
    frxDesigner1: TfrxDesigner;
    frxDBDataset1: TfrxDBDataset;
    sklepy: TIBQuery;
    DataSource1: TDataSource;
    eksport: TJvDBGridExcelExport;
    frxXLSXExport1: TfrxXLSXExport;
    frxDBDataset2: TfrxDBDataset;
    ibqKartaKontrah: TIBQuery;
    frxDBDataset3: TfrxDBDataset;
    frxDBDataset4: TfrxDBDataset;
    frxDBDataset5: TfrxDBDataset;
    ibqExcelK: TIBQuery;
    frxDBDataset6: TfrxDBDataset;
    frxDBDataset7: TfrxDBDataset;
    procedure DrukujObrotyDostawcy(dostawca,miesiac: String);overload;
    procedure DrukujObrotyDostawcyK(dostawca,kwartal: String);
    procedure DrukujObrotyOdbiorcy(odbiorca,m01,m02,m03,m04,m05,m06,m07,m08,m09,m10,m11,m12: Integer);
    procedure DrukujObrotyOdbiorcyK(odbiorca: Integer; kwartal: String);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DrukujObrotyDostawcy(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer);overload;
    procedure DrukujObrotySklepowAll(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer);
    procedure DrukujObrotySklepowAllExcel(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer);
  end;

var
  rpImporty: TrpImporty;

implementation
uses baza, KreaotorRap;
{$R *.dfm}

{ TrpImporty }

procedure TrpImporty.DrukujObrotyDostawcy(dostawca,miesiac: String);
begin
with sklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,B.IMIE_NAZWISKO, sum(A.OBROTY) AS OBROTY,sum(A.PROWIZJA_KWOTA) AS PROWIZJA_SKLEPY,sum(A.PROW_E_KWOTA) AS PROWIZJE_EURO,sum(A.ZYSK) AS ZYSK from OBROTY A, KONTRAH B');
          SQL.Add('WHERE OKRES_M LIKE :A AND A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND A.NAZWA_D LIKE :B');
          SQL.Add('GROUP BY B.NAZWA,B.IMIE_NAZWISKO');
          Unprepare;
              ParamByName('B').AsString := '%'+dostawca+'%';
              ParamByName('A').AsString := miesiac;
          Prepare;
          Open;
     end;
end;

procedure TrpImporty.DrukujObrotyDostawcy(mi1,mi2,mi3,mi4,mi5,mi6,mi7,mi8,mi9,mi10,mi11,mi12: Integer);
begin
with sklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA_D,A.NR_PROWIZJE,B.OPIS,SUM(A.OBROTY) AS OBROTY,SUM(A.PROW_E_KWOTA) AS EURO FROM OBROTY A,PROWIZJE B');
          SQL.Add('WHERE A.NUMER_MIESIAC IN (:M1,:M2,:M3,:M4,:M5,:M6,:M7,:M8,:M9,:M10,:M11,:M12) AND A.NR_PROWIZJE = B.NR_PROWIZJE');
          SQL.Add('GROUP BY A.NAZWA_D,A.NR_PROWIZJE,B.OPIS');
          Unprepare;
              ParamByName('M1').AsInteger := mi1;
              ParamByName('M2').AsInteger := mi2;
              ParamByName('M3').AsInteger := mi3;
              ParamByName('M4').AsInteger := mi4;
              ParamByName('M5').AsInteger := mi5;
              ParamByName('M6').AsInteger := mi6;
              ParamByName('M7').AsInteger := mi7;
              ParamByName('M8').AsInteger := mi8;
              ParamByName('M9').AsInteger := mi9;
              ParamByName('M10').AsInteger := mi10;
              ParamByName('M11').AsInteger := mi11;
              ParamByName('M12').AsInteger := mi12;
          Prepare;
          Open;
     end;
end;

procedure TrpImporty.DrukujObrotyDostawcyK(dostawca, kwartal: String);
begin
with sklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,B.IMIE_NAZWISKO, sum(A.OBROTY) AS OBROTY,sum(A.PROWIZJA_KWOTA) AS PROWIZJA_SKLEPY,sum(A.PROW_E_KWOTA) AS PROWIZJE_EURO,sum(A.ZYSK) AS ZYSK from OBROTY A, KONTRAH B');
          SQL.Add('WHERE OKRES_Q IN (:A) AND A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND A.NAZWA_D LIKE :B');
          SQL.Add('GROUP BY B.NAZWA,B.IMIE_NAZWISKO');
          Unprepare;
              ParamByName('B').AsString := '%'+dostawca+'%';
              ParamByName('A').AsString := kwartal;
          Prepare;
          Open;
     end;
end;

procedure TrpImporty.DrukujObrotyOdbiorcy(odbiorca,m01,m02,m03,m04,m05,m06,m07,m08,m09,m10,m11,m12: Integer);
begin
odbiorca := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
with sklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM P_OBROTY_SKLEPY_MIESIAC(:A,:M1,:M2,:M3,:M4,:M5,:M6,:M7,:M8,:M9,:M10,:M11,:M12)');
          Unprepare;
              ParamByName('A').AsInteger := odbiorca;
              ParamByName('M1').AsInteger := m01;
              ParamByName('M2').AsInteger := m02;
              ParamByName('M3').AsInteger := m03;
              ParamByName('M4').AsInteger := m04;
              ParamByName('M5').AsInteger := m05;
              ParamByName('M6').AsInteger := m06;
              ParamByName('M7').AsInteger := m07;
              ParamByName('M8').AsInteger := m08;
              ParamByName('M9').AsInteger := m09;
              ParamByName('M10').AsInteger := m10;
              ParamByName('M11').AsInteger := m11;
              ParamByName('M12').AsInteger := m12;
          Prepare;
          Open;
     end;
end;

procedure TrpImporty.DrukujObrotyOdbiorcyK(odbiorca: Integer; kwartal: String);
begin
with sklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA_D, sum(A.OBROTY) AS OBROTY,sum(A.PROWIZJA_KWOTA) AS PROWIZJA_SKLEPY,sum(A.PROW_E_KWOTA) AS PROWIZJE_EURO,sum(A.ZYSK) AS ZYSK from OBROTY A, KONTRAH B');
          SQL.Add('WHERE OKRES_Q LIKE :A AND A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND B.NR_KONTRAH = :B');
          SQL.Add('GROUP BY A.NAZWA_D');
          SQL.Add('ORDER BY OBROTY DESC');
          Unprepare;
              ParamByName('B').AsInteger := odbiorca;
              ParamByName('A').AsString := kwartal;
          Prepare;
          Open;
     end;
end;

procedure TrpImporty.DrukujObrotySklepowAll(mi1, mi2, mi3, mi4, mi5, mi6,
  mi7, mi8, mi9, mi10, mi11, mi12: Integer);
begin
with sklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.IMIE_NAZWISKO,B.NAZWA,SUM(A.OBROTY) AS OBROTY,SUM(A.PROWIZJA_KWOTA) AS PROWIZJA_SKLEP,SUM(A.PROW_E_KWOTA) AS PROWIZJA_EURO FROM OBROTY A,KONTRAH B');
          SQL.Add('WHERE A.NUMER_MIESIAC IN (:M1,:M2,:M3,:M4,:M5,:M6,:M7,:M8,:M9,:M10,:M11,:M12) AND A.ROK = 2017 AND B.NR_KONTRAH = A.NR_KONTRAH_SKLEP');
          SQL.Add('GROUP BY B.IMIE_NAZWISKO,B.NAZWA');
          SQL.Add('ORDER BY B.IMIE_NAZWISKO');
          Unprepare;
              ParamByName('M1').AsInteger := mi1;
              ParamByName('M2').AsInteger := mi2;
              ParamByName('M3').AsInteger := mi3;
              ParamByName('M4').AsInteger := mi4;
              ParamByName('M5').AsInteger := mi5;
              ParamByName('M6').AsInteger := mi6;
              ParamByName('M7').AsInteger := mi7;
              ParamByName('M8').AsInteger := mi8;
              ParamByName('M9').AsInteger := mi9;
              ParamByName('M10').AsInteger := mi10;
              ParamByName('M11').AsInteger := mi11;
              ParamByName('M12').AsInteger := mi12;
          Prepare;
          Open;
     end;
end;

procedure TrpImporty.DrukujObrotySklepowAllExcel(mi1, mi2, mi3, mi4, mi5,
  mi6, mi7, mi8, mi9, mi10, mi11, mi12: Integer);
begin
with sklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM P_OBR_SKLEP_OKRESY_ALL');
          SQL.Add('WHERE OBROTY > 0 AND NUMER_MIESIAC IN (:M1,:M2,:M3,:M4,:M5,:M6,:M7,:M8,:M9,:M10,:M11,:M12) AND ROK = 2017');
          SQL.Add('ORDER BY NUMER_MIESIAC');
          Unprepare;
              ParamByName('M1').AsInteger := mi1;
              ParamByName('M2').AsInteger := mi2;
              ParamByName('M3').AsInteger := mi3;
              ParamByName('M4').AsInteger := mi4;
              ParamByName('M5').AsInteger := mi5;
              ParamByName('M6').AsInteger := mi6;
              ParamByName('M7').AsInteger := mi7;
              ParamByName('M8').AsInteger := mi8;
              ParamByName('M9').AsInteger := mi9;
              ParamByName('M10').AsInteger := mi10;
              ParamByName('M11').AsInteger := mi11;
              ParamByName('M12').AsInteger := mi12;
          Prepare;
          Open;
     end;
end;

end.



