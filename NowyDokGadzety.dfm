object frNowyDokG: TfrNowyDokG
  Left = 290
  Top = 245
  Width = 954
  Height = 517
  Caption = '{'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pc: TPageControlEx
    Left = 0
    Top = 0
    Width = 938
    Height = 478
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    HideHeader = False
    object TabSheet1: TTabSheet
      Caption = 'Lista kontrahent'#243'w'
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 930
        Height = 49
        Align = alTop
        TabOrder = 0
        object LabeledEdit14: TLabeledEdit
          Left = 104
          Top = 16
          Width = 121
          Height = 21
          CharCase = ecUpperCase
          EditLabel.Width = 94
          EditLabel.Height = 13
          EditLabel.Caption = 'Szukaj wg nazwiska'
          LabelPosition = lpLeft
          TabOrder = 0
          OnChange = LabeledEdit14Change
        end
        object LabeledEdit15: TLabeledEdit
          Left = 552
          Top = 16
          Width = 97
          Height = 21
          EditLabel.Width = 68
          EditLabel.Height = 13
          EditLabel.Caption = 'Szukaj wg NIP'
          LabelPosition = lpLeft
          TabOrder = 1
          OnChange = LabeledEdit15Change
        end
        object LabeledEdit1: TLabeledEdit
          Left = 352
          Top = 16
          Width = 121
          Height = 21
          CharCase = ecUpperCase
          EditLabel.Width = 109
          EditLabel.Height = 13
          EditLabel.Caption = 'Szukaj wg nazwy firmy'
          LabelPosition = lpLeft
          TabOrder = 2
          OnChange = LabeledEdit1Change
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 49
        Width = 930
        Height = 401
        Align = alClient
        TabOrder = 1
        object DBGrid5: TDBGrid
          Left = 1
          Top = 1
          Width = 928
          Height = 399
          Align = alClient
          DataSource = dm.dsKontrah
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnCellClick = DBGrid5CellClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Lista towar'#243'w'
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 425
        Top = 49
        Height = 401
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 930
        Height = 49
        Align = alTop
        TabOrder = 0
        object LabeledEdit2: TLabeledEdit
          Left = 104
          Top = 16
          Width = 121
          Height = 21
          CharCase = ecUpperCase
          EditLabel.Width = 82
          EditLabel.Height = 13
          EditLabel.Caption = 'Szukaj wg nazwy'
          LabelPosition = lpLeft
          TabOrder = 0
          OnChange = LabeledEdit2Change
        end
        object JvImgBtn4: TJvImgBtn
          Left = 280
          Top = -1
          Width = 145
          Height = 49
          Caption = 'Dodaj nowy towar'
          TabOrder = 1
          Visible = False
          OnClick = JvImgBtn4Click
          Flat = True
          Images = frTowary.ImageList1
          ImageIndex = 0
        end
        object JvImgBtn1: TJvImgBtn
          Left = 424
          Top = 0
          Width = 177
          Height = 49
          Caption = 'Dalej do podsumowania'
          TabOrder = 2
          OnClick = JvImgBtn1Click
          Flat = True
          Images = frTowary.ImageList1
          ImageIndex = 9
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 49
        Width = 425
        Height = 401
        Align = alLeft
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 423
          Height = 399
          Align = alClient
          DataSource = dm.dsTowary
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
        end
      end
      object Panel5: TPanel
        Left = 428
        Top = 49
        Width = 502
        Height = 401
        Align = alClient
        TabOrder = 2
        object DBGrid2: TDBGrid
          Left = 1
          Top = 1
          Width = 500
          Height = 399
          Align = alClient
          DataSource = dm.dsGadzetyP
          PopupMenu = pmPozycjeWydania
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Podsumowanie'
      ImageIndex = 2
      object GroupBox1: TGroupBox
        Left = 8
        Top = 32
        Width = 729
        Height = 105
        Caption = 'Dokument dla:'
        TabOrder = 0
        object Label1: TLabel
          Left = 24
          Top = 32
          Width = 54
          Height = 19
          Caption = 'Label1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 24
          Top = 72
          Width = 54
          Height = 19
          Caption = 'Label2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 336
          Top = 32
          Width = 54
          Height = 19
          Caption = 'Label3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 336
          Top = 16
          Width = 17
          Height = 13
          Caption = 'NIP'
        end
        object Label6: TLabel
          Left = 24
          Top = 16
          Width = 71
          Height = 13
          Caption = 'Imi'#281' i nazwisko'
        end
        object Label7: TLabel
          Left = 24
          Top = 56
          Width = 32
          Height = 13
          Caption = 'Nazwa'
        end
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 152
        Width = 729
        Height = 105
        Caption = 'Warto'#347#263' dokumentu'
        TabOrder = 1
        object Label4: TLabel
          Left = 24
          Top = 48
          Width = 54
          Height = 19
          Caption = 'Label4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 24
          Top = 32
          Width = 96
          Height = 13
          Caption = 'Warto'#347#263' dokumentu'
        end
      end
      object JvImgBtn2: TJvImgBtn
        Left = 752
        Top = 200
        Width = 161
        Height = 57
        Caption = 'Anuluj'
        TabOrder = 2
        OnClick = JvImgBtn2Click
        Flat = True
        Images = frTowary.ImageList1
        ImageIndex = 10
      end
      object JvImgBtn3: TJvImgBtn
        Left = 752
        Top = 144
        Width = 161
        Height = 57
        Caption = 'Zapisz'
        TabOrder = 3
        OnClick = JvImgBtn3Click
        Flat = True
        Images = frTowary.ImageList1
        ImageIndex = 5
      end
    end
  end
  object pmPozycjeWydania: TPopupMenu
    Images = frGlowny.JvImageList1
    Left = 792
    Top = 33
    object Usuwybranpozycj1: TMenuItem
      Caption = 'Usu'#324' wybran'#261' pozycj'#281
      OnClick = Usuwybranpozycj1Click
    end
    object Modyfikujwybranpozycj1: TMenuItem
      Caption = 'Modyfikuj wybran'#261' pozycj'#281
      OnClick = Modyfikujwybranpozycj1Click
    end
  end
end
