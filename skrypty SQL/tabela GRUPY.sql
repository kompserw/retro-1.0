CREATE TABLE GRUPY 
(
  NR_GRUPY                 INTEGER         NOT NULL,
  NAZWA_GRUPY              VARCHAR(    50) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  ROLA1                    INTEGER,
  ROLA2                    INTEGER,
  ROLA3                    INTEGER,
  ROLA4                    INTEGER,
  ROLA5                    INTEGER,
 CONSTRAINT PK_GRUPY PRIMARY KEY (NR_GRUPY)
);
