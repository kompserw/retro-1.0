/* Trigger GRUPY_ for table/view GRUPY
2016-09-13 20:30:37 */

CREATE TRIGGER GRUPY_ FOR GRUPY ACTIVE BEFORE INSERT POSITION 0 AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Trigger:

  Author   : , 
  Date     : 2016-09-13 20:30:13
  Purpose  :
*/
begin
  NEW.NR_GRUPY = GEN_ID(NR_GRUPY_GEN, 1);
end 


