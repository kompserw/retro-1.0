CREATE TABLE PRACOWNIK 
(
  NR_PRACOWNIK              INTEGER         NOT NULL,
  IMIE                      VARCHAR(    30) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  NAZWISKO                  VARCHAR(    30) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  LOGIN                     VARCHAR(    15) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  HASLO                     VARCHAR(    20) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
 CONSTRAINT PK_PRACOWNIK PRIMARY KEY (NR_PRACOWNIK)
);

CREATE TRIGGER PRACOWNIK_ FOR PRACOWNIK ACTIVE BEFORE INSERT POSITION 0 AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Trigger:

  Author   : , 
  Date     : 2016-08-31 22:37:20
  Purpose  :
*/
begin
    NEW.NR_PRACOWNIK = GEN_ID(NR_PRACOWNIK_GEN, 1);
end
