CREATE TRIGGER GADZETY_POZYCJE_ FOR GADZETY_POZYCJE ACTIVE BEFORE INSERT POSITION 0 AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Trigger:

  Author   : , 
  Date     : 2016-11-09 23:08:41
  Purpose  :
*/
begin
  NEW.NR_GADZETY_POZYCJE = GEN_ID(GADZETY_POZYCJE_GEN, 1);
end