CREATE PROCEDURE P_OBROT_PROW_OPIS (
  MIESIAC VarChar(20), 
  KONTRAHENT Integer)
 returns (
  OPIS VarChar(80), 
  OBROT Decimal(18,2), 
  PROWIZJA_SKLEP Decimal(18,2), 
  PROWIZJA_SIEC Decimal(18,2), 
  ZYSK Decimal(18,2))
AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Procedure:

  Author   : , 
  Date     : 2016-04-03 23:39:36
  Purpose  :
  Params
  ------
  <param>  : <purpose>
*/
begin
FOR SELECT B.OPIS AS OPIS,A.OBROT AS OBROT,A.PROWIZJA_SIEC AS PROWIZJA_SIEC,A.PROWIZJA_SKLEP AS PROWIZJA_SKLEP,A.ZYSK AS ZYSK FROM P_OBROT_PROWIZJE_NR_PROWIZJE(:MIESIAC,:KONTRAHENT) A,PROWIZJE B
WHERE A.NR_PROWIZJE = B.NR_PROWIZJE
ORDER BY B.OPIS
INTO :OPIS,:OBROT,:PROWIZJA_SIEC,:PROWIZJA_SKLEP,:ZYSK
DO
SUSPEND;
end