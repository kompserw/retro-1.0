
SET TERM ^^ ;
CREATE TRIGGER KONTRAH_ FOR KONTRAH ACTIVE BEFORE INSERT POSITION 0 AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Trigger:

  Author   : , 
  Date     : 2016-02-15 07:12:55
  Purpose  :
*/
begin
  NEW.NR_KONTRAH = GEN_ID(NR_KONTRAH_GEN, 1);
end ^^
SET TERM ; ^^

