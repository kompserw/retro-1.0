GRANT ALL ON CRM TO SZEF;
GRANT ALL ON FAKT_NAGLOWEK TO SZEF;
GRANT ALL ON FAKT_POZYCJE TO SZEF;
GRANT ALL ON IMPORT TO SZEF;
GRANT ALL ON KONTRAH TO SZEF;
GRANT ALL ON NAZWY TO SZEF;
GRANT ALL ON OBROTY TO SZEF;
GRANT ALL ON PRACOWNIK TO SZEF;
GRANT ALL ON PROWIZJE TO SZEF;

GRANT SELECT,INSERT ON CRM TO PRACOWNIK;
GRANT ALL ON FAKT_NAGLOWEK TO PRACOWNIK;
GRANT ALL ON FAKT_POZYCJE TO PRACOWNIK;
GRANT ALL ON IMPORT TO PRACOWNIK;
GRANT SELECT,INSERT,UPDATE ON KONTRAH TO PRACOWNIK;
GRANT ALL ON NAZWY TO PRACOWNIK;
GRANT SELECT,INSERT,UPDATE ON OBROTY TO PRACOWNIK;
GRANT SELECT,INSERT,UPDATE ON PRACOWNIK TO PRACOWNIK;
GRANT SELECT,INSERT,UPDATE ON PROWIZJE TO PRACOWNIK;
