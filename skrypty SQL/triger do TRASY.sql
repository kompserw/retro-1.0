/* Trigger TRASY_ for table/view TRASY
2016-09-13 20:29:00 */

CREATE TRIGGER TRASY_ FOR TRASY ACTIVE BEFORE INSERT POSITION 0 AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Trigger:

  Author   : , 
  Date     : 2016-09-13 20:27:57
  Purpose  :
*/
begin
  NEW.NR_TRASY = GEN_ID(NR_TRASY_GEN, 1);
end


