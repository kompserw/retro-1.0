/* Constraint FK_OBROTY_KONTRAH for table OBROTY
2016-10-01 07:46:39 */
ALTER TABLE OBROTY ADD CONSTRAINT FK_OBROTY_KONTRAH 
  FOREIGN KEY (NR_KONTRACH_DOST) REFERENCES KONTRAH
  (NR_KONTRAH) 
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
;
