CREATE TABLE CRM 
(
  NR_CRM                       INTEGER         NOT NULL,
  NR_KONTRAH                   INTEGER         NOT NULL,
  DATA_UTWORZENIA                 DATE         NOT NULL,
  KTO_UTWORZYL                 VARCHAR(    50) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  KANAL                        VARCHAR(    30) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  NOTATKA                      VARCHAR(   255) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
 CONSTRAINT PK_CRM PRIMARY KEY (NR_CRM)
);

CREATE TRIGGER CRM_ FOR CRM ACTIVE BEFORE INSERT POSITION 0 AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Trigger:

  Author   : , 
  Date     : 2016-08-31 22:31:30
  Purpose  :
*/
begin
    NEW.NR_CRM = GEN_ID(NR_CRM_GEN, 1);
    NEW.DATA_UTWORZENIA = CURRENT_TIMESTAMP;
end
