CREATE TABLE ADRESY 
(
  NR_ADRESY                 INTEGER         NOT NULL,
  NR_KONTRAH                INTEGER         NOT NULL,
  NAZWA_PUNKTU              VARCHAR(    50) CHARACTER SET WIN1250  COLLATE PXW_PLK,
  ULICA                     VARCHAR(    50) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  MIASTO                    VARCHAR(    30) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  KOD_P                     VARCHAR(     8) CHARACTER SET WIN1250 NOT NULL COLLATE PXW_PLK,
  TELEFON                   VARCHAR(    30),
  KONCESJA                  VARCHAR(     5),
  METRAZ                    VARCHAR(     5),
  KATEGORIA                 VARCHAR(     5),
  AKTYWNY                   INTEGER         DEFAULT 1 NOT NULL,
 CONSTRAINT PK_ADRESY PRIMARY KEY (NR_ADRESY)
);

CREATE GENERATOR NR_ADRESY_GEN;
SET GENERATOR NR_ADRESY_GEN TO 0;

SET TERM ^^ ;
CREATE TRIGGER ADRESY_ FOR ADRESY ACTIVE BEFORE INSERT POSITION 0 AS
/*
  You can change this template in the template editor:
  File | Preferences | Object Templates

  Trigger:

  Author   : , 
  Date     : 2016-04-11 22:40:58
  Purpose  :
*/
begin
NEW.NR_ADRESY = GEN_ID(NR_ADRESY_GEN, 1);
end ^^
SET TERM ; ^^


