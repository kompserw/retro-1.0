unit info;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DBGrids, DB, IBCustomDataSet,
  IBQuery, DBCtrls, ToolWin, ComCtrls, PageControlEx, JvExGrids,
  JvStringGrid, IBTable, JvExExtCtrls, JvExtComponent, JvDBRadioPanel,
  JvExMask, JvToolEdit, JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit,
  JvDBDatePickerEdit, Mask, JvExStdCtrls, JvButton, JvCtrls, JvExComCtrls,
  JvToolBar, JvExDBGrids, JvDBGrid, JvDBControls, JvMemo, JvRadioGroup,
  JvExControls, JvLabel;

type

  TfrInfo = class(TForm)
    pc: TPageControlEx;
    WybierzKontrach: TTabSheet;
    AnalizujSklepy: TTabSheet;
    ToolBar1: TToolBar;
    eNazwaD: TEdit;
    DBNavigator1: TDBNavigator;
    ibqSzukajD: TIBQuery;
    dsSzukajD: TDataSource;
    dbSiatka: TDBGrid;
    ToolBar2: TToolBar;
    lSuma: TLabel;
    mSiatka: TJvStringGrid;
    sb: TStatusBar;
    DodajKontrahenta: TTabSheet;
    dsNowyK: TDataSource;
    GroupBox1: TGroupBox;
    lbNIP: TListBox;
    GroupBox2: TGroupBox;
    ibqNowyK: TIBQuery;
    leImieN: TLabeledEdit;
    leNazwa: TLabeledEdit;
    leNIP: TLabeledEdit;
    leUlica: TLabeledEdit;
    leMiasto: TLabeledEdit;
    leKodP: TLabeledEdit;
    leTelefon: TLabeledEdit;
    leKoncesja: TLabeledEdit;
    leMetraz: TLabeledEdit;
    leKategoria: TLabeledEdit;
    dpeDataP: TJvDatePickerEdit;
    JvLabel1: TJvLabel;
    mUwagi: TJvMemo;
    JvImgBtn1: TJvImgBtn;
    JvLabel2: TJvLabel;
    GroupBox4: TGroupBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    Label28: TLabel;
    Label27: TLabel;
    GroupBox3: TGroupBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    procedure eNazwaDClick(Sender: TObject);
    procedure eNazwaDChange(Sender: TObject);
    procedure eNazwaDDblClick(Sender: TObject);
    procedure dbSiatkaCellClick(Column: TColumn);
    procedure SzukajNIP(NIP: String);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure lbNIPClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  frInfo: TfrInfo;

implementation
uses baza,glowny,kontrahent;
{$R *.dfm}


procedure TfrInfo.eNazwaDClick(Sender: TObject);
begin
eNazwaD.Text:='';
end;

procedure TfrInfo.eNazwaDChange(Sender: TObject);
begin
//
with dm.ibqDostawca do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_KONTRAH,NAZWA,NIP,ULICA,MIASTO FROM KONTRAH');
          SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = :B');
          UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(eNazwaD.Text) +'%';
                    ParamByName('B').AsString := 'D';
          Prepare;
          Open;
     end;
with dbSiatka do
     begin
          Columns.Items[0].Visible := False;
          Columns.Items[1].FieldName := 'NAZWA';
          Columns.Items[1].Width := 250;
          Columns.Items[2].FieldName := 'NIP';
          Columns.Items[2].Width := 70;
          Columns.Items[3].FieldName := 'ULICA';
          Columns.Items[3].Title.Caption := 'Ulica';
          Columns.Items[3].Width := 200;
          Columns.Items[4].FieldName := 'MIASTO';
          Columns.Items[4].Title.Caption := 'Miasto';
          Columns.Items[4].Width := 200;
     end;
end;

procedure TfrInfo.eNazwaDDblClick(Sender: TObject);
begin
eNazwaD.Text:='';
end;


procedure TfrInfo.SzukajNIP(NIP: String);        //
begin
with dm.ibqSQL do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT NR_KONTRAH,IMIE_NAZWISKO,NAZWA,NIP,ULICA FROM KONTRAH');
    SQL.Add('WHERE NIP = :A');
    UnPrepare;
      ParamByName('A').AsString := NIP;
    Prepare;
    Open;
  end;
end;

procedure TfrInfo.dbSiatkaCellClick(Column: TColumn);
var
  zakladka: TBookmark;
  ile,x,y,nr_import: Integer;                                           //TU TWORZY LIST� IMPORTU
  suma: Real;
  dostawca,nip: String;
begin
with dm.ibqZapisz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_IMPORTU FROM IMPORT');
          Open;
          Last;
          if FieldByName('NR_IMPORTU').IsNull then
             nr_import := 0
          else
              nr_import := FieldValues['NR_IMPORTU'];
     end;
nr_import := nr_import + 1;
y := 0;
pc.ActivePageIndex := 1;
with ibqSzukajD do
     begin
          Close;
          SQL.Clear;
            SQL.Add('SELECT NIP,SUM(OBROT) AS OBROT FROM IMPORT');
            SQL.Add('GROUP BY NIP');
          Open;
     end;

suma := 0;x := 0;
dm.ibqCSV2Firebird.First;
mSiatka.Cells[0,0] := 'Lp.';
mSiatka.Cells[1,0] := 'Nr NIP';
mSiatka.Cells[2,0] := 'Nazwa sklepu';
mSiatka.Cells[3,0] := 'Obroty';
mSiatka.ColWidths[0] := 50;
mSiatka.ColWidths[1] := 100;
mSiatka.ColWidths[2] := 550;
mSiatka.ColWidths[3] := 100;
ile := dm.ibqCSV2Firebird.RecordCount;
//for x := 1 to ile + 1 do
//for x := 1 to ile + 1 do
while not dm.ibqCSV2Firebird.Eof do
begin
x := x + 1;
if x <= ile then suma := suma + dm.ibqCSV2Firebird.FieldValues['OBROT'];
with dm.ibqSQL do
     begin
          if dm.ibqCSV2Firebird.FieldByName('NIP').IsNull then sb.SimpleText := 'Brak NIP'
          else
              nip := dm.ibqCSV2Firebird.FieldValues['NIP'];
          SzukajNIP(nip);        //dm.ibqCSV2Firebird.FieldValues['NIP'];
          if FieldByName('NR_KONTRAH').IsNull then
             begin
                   mSiatka.RowCount := x;
                   y := y + 1;
                   mSiatka.Cells[0,x] := IntToStr(x);
                   mSiatka.Cells[1,x] := dm.ibqCSV2Firebird.FieldValues['NIP'];
                   mSiatka.Cells[2,x] := 'BRAK KONTRAHENTA';
                   mSiatka.Cells[3,x] := FloatToStr(dm.ibqCSV2Firebird.FieldValues['OBROT']);
                   lbNIP.Items.Add(dm.ibqCSV2Firebird.FieldValues['NIP']);
             end
          else
              begin
                   mSiatka.RowCount := x;
                   mSiatka.Cells[0,x] := IntToStr(x);
                   mSiatka.Cells[1,x] := dm.ibqCSV2Firebird.FieldValues['NIP'];
                   mSiatka.Cells[2,x] := dm.ibqSQL.FieldValues['NAZWA'];
                   mSiatka.Cells[3,x] := FloatToStr(dm.ibqCSV2Firebird.FieldValues['OBROT']);
              end;
              //suma := suma + dm.ibqCSV2Firebird.FieldValues['OBROT'];
     end;
     dm.ibqCSV2Firebird.Next;
end;
dostawca := dm.ibqDostawca.FieldValues['NAZWA'];
frGlowny.nr_dost := dm.ibqDostawca.FieldValues['NR_KONTRAH'];
lSuma.Caption := 'Warto�� importu dostawcy ' + dostawca + ' wynosi: ' + FloatToStr(suma) + ' z�' + ' znalaz�em ' + IntToStr(y) + ' niezidentyfikowane NIP-y';
if y > 0 then pc.ActivePageIndex := 2;
dm.ibqCSV2Firebird.First;
with frGlowny.dbgDoObrotu.Columns do
begin
     Items[0].Visible := False;
     Items[1].FieldName := 'IMIE_NAZWISKO';
     Items[1].Title.Caption := 'Imi� i nazwisko';
     Items[1].Width := 100;
     Items[2].FieldName := 'NAZWA';
     Items[2].Width := 250;
     Items[3].Visible := False;
     Items[4].FieldName := 'ULICA';
     Items[4].Width := 100;
end;
frGlowny.JvGroupBox1.Visible := True;
end;

procedure TfrInfo.JvImgBtn1Click(Sender: TObject);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with ibqNowyK do
begin
     Close;
     SQL.Clear;
     SQL.Add('INSERT INTO KONTRAH(RODZAJ,IMIE_NAZWISKO,NAZWA,NIP,ULICA,MIASTO,KOD_P,DATA_PRZYSTAPIENIA,TELEFON,KONCESJA,METRAZ,KATEGORIA,UWAGI)');
     SQL.Add('VALUES(:RODZAJ,:IMIE_NAZWISKO,:NAZWA,:NIP,:ULICA,:MIASTO,:KOD_P,:DATA_PRZYSTAPIENIA,:TELEFON,:KONCESJA,:METRAZ,:KATEGORIA,:UWAGI)');
     UnPrepare;
          ParamByName('RODZAJ').AsString:='O';
          ParamByName('IMIE_NAZWISKO').AsString := UpperCase(leImieN.Text);
          ParamByName('NAZWA').AsString := UpperCase(leNazwa.Text);
          ParamByName('NIP').AsString := leNIP.Text;
          ParamByName('ULICA').AsString := UpperCase(leUlica.Text);
          ParamByName('MIASTO').AsString := UpperCase(leMiasto.Text);
          ParamByName('KOD_P').AsString := leKodP.Text;
          ParamByName('DATA_PRZYSTAPIENIA').AsDate := dpeDataP.Date;
          ParamByName('TELEFON').AsString := leTelefon.Text;
          ParamByName('KONCESJA').AsString := UpperCase(leKoncesja.Text);
          ParamByName('METRAZ').AsString := leMetraz.Text;
          ParamByName('KATEGORIA').AsString := UpperCase(leKategoria.Text);
          ParamByName('UWAGI').AsString := mUwagi.Text;
     Prepare;
     Open;
end;

if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
leImieN.Text := '';leKategoria.Text := '';leKodP.Text := '';leKoncesja.Text := '';
leMetraz.Text := '';leMiasto.Text := '';leNazwa.Text := '';leNIP.Text := '';
leTelefon.Text := '';leUlica.Text := '';mUwagi.Text := '';

end;

procedure TfrInfo.lbNIPClick(Sender: TObject);
var
   ile : Integer;
begin
for ile := 0 to lbNIP.Items.Count - 1 do
 if lbNIP.Selected[ile] = True then leNIP.Text := lbNIP.Items.Strings[ile];

end;

end.
