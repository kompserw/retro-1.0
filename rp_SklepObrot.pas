unit rp_SklepObrot;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery;

type
  TfrRaportObrSklepu = class(TForm)
    ibqPokazObrot: TIBQuery;
    dsPokazObrot: TDataSource;
    ibqPokazObrotDOSTAWCA: TIBStringField;
    ibqPokazObrotOBROTY: TIBBCDField;
    ibqPokazObrotPROWIZJA_PROC: TFloatField;
    ibqPokazObrotPROWIZJA_KWOTA: TFloatField;
    ibqPokazObrotOKRES_M: TIBStringField;
    ibqPokazObrotOKRES_Q: TIBStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frRaportObrSklepu: TfrRaportObrSklepu;

implementation
uses glowny,baza;
{$R *.dfm}

end.
