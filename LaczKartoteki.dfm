object frPolaczKartoteki: TfrPolaczKartoteki
  Left = 192
  Top = 125
  Width = 1142
  Height = 656
  Caption = #321#261'czenie kartotek kontrahent'#243'w w jedn'#261'.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object JvNetscapeSplitter1: TJvNetscapeSplitter
    Left = 393
    Top = 29
    Height = 588
    Align = alLeft
    Maximized = False
    Minimized = False
    ButtonCursor = crDefault
  end
  object JvToolBar1: TJvToolBar
    Left = 0
    Top = 0
    Width = 1126
    Height = 29
    Caption = 'JvToolBar1'
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Width = 8
      Caption = 'ToolButton1'
      Style = tbsSeparator
    end
    object JvEdit1: TJvEdit
      Left = 8
      Top = 2
      Width = 385
      Height = 22
      Hint = 'Wprowad'#378' nazw'#281' szukanej firmy'
      Flat = True
      ParentFlat = False
      AutoSize = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = JvEdit1Change
    end
  end
  object JvPanel1: TJvPanel
    Left = 0
    Top = 29
    Width = 393
    Height = 588
    Align = alLeft
    TabOrder = 1
    object JvDBUltimGrid1: TJvDBUltimGrid
      Left = 1
      Top = 26
      Width = 391
      Height = 561
      Align = alClient
      DataSource = dsPokazKontrah
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = JvDBUltimGrid1DblClick
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      WordWrap = True
      WordWrapAllFields = True
      Columns = <
        item
          Expanded = False
          FieldName = 'NAZWA'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NIP'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ULICA'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MIASTO'
          Width = 150
          Visible = True
        end>
    end
    object JvDBNavigator1: TJvDBNavigator
      Left = 1
      Top = 1
      Width = 391
      Height = 25
      DataSource = dsPokazKontrah
      Align = alTop
      Flat = True
      TabOrder = 1
    end
  end
  object JvPanel2: TJvPanel
    Left = 403
    Top = 29
    Width = 723
    Height = 588
    Align = alClient
    TabOrder = 2
    object JvDBUltimGrid2: TJvDBUltimGrid
      Left = 0
      Top = 24
      Width = 713
      Height = 329
      DataSource = dsPokazTMP
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'NR_KONTRAH'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'NAZWA_PUNKTU'
          Title.Caption = 'Nazwa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ULICA'
          Title.Caption = 'Ulica'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MIASTO'
          Title.Caption = 'Miasto'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KOD_P'
          Title.Caption = 'Kod pocztowy'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TELEFON'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'KONCESJA'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'METRAZ'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'KATEGORIA'
          Visible = False
        end>
    end
  end
  object dsPokazKontrah: TJvDataSource
    Left = 648
    Top = 8
  end
  object dsPokazTMP: TJvDataSource
    Left = 920
    Top = 8
  end
  object PokazKontrah: TIBQuery
    BufferChunks = 1000
    CachedUpdates = False
    Left = 555
    Top = 13
  end
  object wstawDoTMP: TIBQuery
    BufferChunks = 1000
    CachedUpdates = False
    Left = 840
    Top = 8
  end
  object pPokazTMP: TIBQuery
    BufferChunks = 1000
    CachedUpdates = False
    Left = 976
    Top = 8
  end
end
