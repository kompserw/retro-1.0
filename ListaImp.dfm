object frListaImp: TfrListaImp
  Left = 363
  Top = 183
  Width = 1019
  Height = 656
  Caption = 'Lista import'#243'w'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pc: TPageControlEx
    Left = 0
    Top = 0
    Width = 1003
    Height = 598
    ActivePage = tsObrotyLista
    Align = alClient
    TabOrder = 0
    HideHeader = True
    object tsObrotyLista: TTabSheet
      Caption = 'tsObrotyLista'
      object JvToolBar1: TJvToolBar
        Left = 0
        Top = 0
        Width = 1003
        Height = 40
        ButtonHeight = 38
        ButtonWidth = 39
        Caption = 'JvToolBar1'
        Images = frGlowny.JvImageList1
        TabOrder = 0
        object JvDBNavigator1: TJvDBNavigator
          Left = 0
          Top = 2
          Width = 224
          Height = 38
          DataSource = dm.dsSQL1
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          Flat = True
          TabOrder = 0
        end
        object ToolButton1: TToolButton
          Left = 224
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          Style = tbsSeparator
        end
        object JvImgBtn2: TJvImgBtn
          Left = 232
          Top = 2
          Width = 127
          Height = 38
          Caption = 'Usu'#324' wybrany import'
          TabOrder = 1
          OnClick = JvImgBtn2Click
          Flat = True
        end
      end
      object JvPanel1: TJvPanel
        Left = 0
        Top = 40
        Width = 1003
        Height = 556
        Align = alClient
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 1001
          Height = 554
          Align = alClient
          DataSource = dm.dsSQL1
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object JvToolBar2: TJvToolBar
        Left = 0
        Top = 0
        Width = 853
        Height = 29
        ButtonHeight = 23
        Caption = 'JvToolBar2'
        TabOrder = 0
        object JvDBNavigator2: TJvDBNavigator
          Left = 0
          Top = 2
          Width = 224
          Height = 23
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          TabOrder = 0
        end
        object JvImgBtn1: TJvImgBtn
          Left = 224
          Top = 2
          Width = 75
          Height = 23
          Caption = 'Wr'#243#263
          TabOrder = 1
          OnClick = JvImgBtn1Click
          Flat = True
          Images = frGlowny.JvImageList1
        end
      end
      object JvDBUltimGrid2: TJvDBUltimGrid
        Left = 0
        Top = 29
        Width = 853
        Height = 567
        Align = alClient
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'NAZWA'
            Title.Caption = 'Nazwa sklepu'
            Width = 400
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBROTY'
            Title.Alignment = taCenter
            Title.Caption = 'Obr'#243't sklepu'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PROWIZJA_SKLEPY'
            Title.Alignment = taCenter
            Title.Caption = 'Prowizja sklepu'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PROWIZJE_EURO'
            Title.Alignment = taCenter
            Title.Caption = 'Prowizja sieci'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ZYSK'
            Title.Alignment = taCenter
            Title.Caption = 'Zysk'
            Width = 80
            Visible = True
          end>
      end
    end
  end
  object sb: TStatusBar
    Left = 0
    Top = 598
    Width = 1003
    Height = 19
    Panels = <
      item
        Width = 400
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 80
      end>
  end
end
