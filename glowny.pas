unit glowny;

{$IFDEF FPC}
  {$MODE Delphi}
{$ENDIF}

interface

uses
{$IFnDEF FPC}
  JvExControls, JvDBLookupTreeView, JvExStdCtrls, JvMemo, JvExComCtrls, JvComCtrls, JvDBControls, JvToolBar,
  JvExExtCtrls, JvExtComponent, JvPanel, JvExDBGrids, JvDBGrid, JvDBUltimGrid, JvEdit, JvMonthCalendar,
  JvExButtons, JvBitBtn, JvRadioGroup, JvSplitter, JvExMask, JvToolEdit, JvDBLookup, JvDBLookupComboEdit,
  JvGroupBox, JvCombobox, JvNetscapeSplitter, JvCheckBox, Mask, Windows,DB,
{$ELSE}
  MaskEdit, LCLIntf, LCLType, LMessages,
{$ENDIF}
  Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, ToolWin, Menus, StdCtrls,
  DBCtrls, Grids, DBGrids, ExtCtrls,
  Buttons, JvRadioButton, PageControlEx, ShellApi, JvProgressBar, JvButton,
  JvCtrls, JvImageList, rlconsts, JvLabel, JvDateTimePicker, JvMaskEdit,
  JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit, IBCustomDataSet,
  IBQuery, JvListBox, JvDriveCtrls,
  FileCtrl, INIFiles, DateUtils;

type
  TfrGlowny = class(TForm)
    mm: TMainMenu;
    sb: TStatusBar;
    Plik1: TMenuItem;
    Raporty1: TMenuItem;
    Administracja1: TMenuItem;
    Koniec1: TMenuItem;
    Importsprzeday1: TMenuItem;
    ZplikuExcel1: TMenuItem;
    Zplikutxt1: TMenuItem;
    Po1: TMenuItem;
    Kartoteki1: TMenuItem;
    Dostawcy1: TMenuItem;
    Prowizje1: TMenuItem;
    KonfiguracjapoczeniazbazWapro1: TMenuItem;
    ImpExcel: TToolButton;
    Prowizje: TToolButton;
    ToolButton4: TToolButton;
    KatSklepy: TToolButton;
    KartDostwacy: TToolButton;
    ToolButton7: TToolButton;
    RaportyButton: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    img: TImageList;
    odTxt: TOpenDialog;
    tbGlowny: TToolBar;
    jpcGlowna: TJvPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    JvMemo1: TJvMemo;
    JvPanel1: TJvPanel;
    JvPanel2: TJvPanel;
    JvToolBar1: TJvToolBar;
    JvDBNavigator1: TJvDBNavigator;
    JvDBUltimGrid1: TJvDBUltimGrid;
    Importy1: TMenuItem;
    KontorahenciEur1: TMenuItem;
    DostawcyEuro1: TMenuItem;
    TuSzukaj: TDBEdit;
    dbImie_naz: TDBEdit;
    dbNazwa: TDBEdit;
    dbMiasto: TDBEdit;
    dbUlica: TDBEdit;
    dbKod_p: TDBEdit;
    dbNIP: TDBEdit;
    dbData_P: TDBEdit;
    dbMetraz: TDBEdit;
    dbKategoria: TDBEdit;
    dbTelefon: TDBEdit;
    dbKoncesja: TDBEdit;
    bZapisz: TButton;
    imie_naz: TJvEdit;
    kod_p: TJvEdit;
    Ulica: TJvEdit;
    Koncesja: TJvEdit;
    Nazwa: TJvEdit;
    Miasto: TJvEdit;
    NIP: TJvEdit;
    Metraz: TJvEdit;
    Kategoria: TJvEdit;
    Telefon: TJvEdit;
    bKopiuj: TButton;
    Uwagi: TMemo;
    Data_Prz: TJvMonthCalendar;
    JvDBNavigator2: TJvDBNavigator;
    JvDBGrid1: TJvDBGrid;
    JvPanel4: TJvPanel;
    JvGroupBox1: TJvGroupBox;
    Edit1: TEdit;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    JvGroupBox2: TJvGroupBox;
    dbgDoObrotu: TJvDBUltimGrid;
    Label2: TLabel;
    DBGrid2: TDBGrid;
    GroupBox1: TGroupBox;
    DBText1: TDBText;
    DBText2: TDBText;
    Label3: TLabel;
    DBText3: TDBText;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Button1: TButton;
    GroupBox2: TGroupBox;
    JvNetscapeSplitter1: TJvNetscapeSplitter;
    Panel3: TPanel;
    Panel2: TPanel;
    JvToolBar4: TJvToolBar;
    JvToolBar2: TJvToolBar;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    jPC: TPageControlEx;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    dbGridPokazK: TJvDBUltimGrid;
    JvToolBar5: TJvToolBar;
    JvDBNavigator5: TJvDBNavigator;
    jvdbKartaObrotow: TJvDBGrid;
    PC_KK: TPageControlEx;
    TabSheet8: TTabSheet;
    e10: TJvEdit;
    Label17: TLabel;
    e05: TJvEdit;
    e04: TJvEdit;
    Label16: TLabel;
    Label15: TLabel;
    e09: TJvEdit;
    Label14: TLabel;
    e07: TJvEdit;
    e02: TJvEdit;
    Label13: TLabel;
    Label12: TLabel;
    e03: TJvEdit;
    Label11: TLabel;
    e06: TJvEdit;
    e01: TJvEdit;
    Label9: TLabel;
    e08: TJvEdit;
    Label8: TLabel;
    rgTyp: TJvRadioGroup;
    JvBitBtn1: TJvBitBtn;
    Label10: TLabel;
    Ustawieniezakadek1: TMenuItem;
    Pierwsza1: TMenuItem;
    Druga1: TMenuItem;
    jpbPasek: TJvProgressBar;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    JvImageList1: TJvImageList;
    JvImgBtn1: TJvImgBtn;
    Czyszczenietabel1: TMenuItem;
    KasowanietabeliImport1: TMenuItem;
    KasowanietabeliObroty1: TMenuItem;
    Wydruksesjiimportu1: TMenuItem;
    Wydrukklientwdane1: TMenuItem;
    Wydrukklientwzobrotami1: TMenuItem;
    Wydrukdostawcwzobrotami1: TMenuItem;
    pmKontrah: TPopupMenu;
    Poprawdanekontrahenta1: TMenuItem;
    Usudanekontrahenta1: TMenuItem;
    Pokaobrotykontrahenta1: TMenuItem;
    KartaDostawcy: TTabSheet;
    JvToolBar6: TJvToolBar;
    JvDBNavigator6: TJvDBNavigator;
    JvDBGrid2: TJvDBGrid;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton18: TToolButton;
    ToolButton17: TToolButton;
    ToolButton19: TToolButton;
    cMiesiac: TJvComboBox;
    cKwartal: TJvComboBox;
    JvLabel2: TJvLabel;
    JvLabel3: TJvLabel;
    pmTabelaR: TMenuItem;
    eSzukajN: TEdit;
    ibqDostawcy: TIBQuery;
    dsDostawcy: TDataSource;
    eSzukajNIP: TEdit;
    pmDostawca: TPopupMenu;
    Pokaobrotydostawcy1: TMenuItem;
    Drukujobrotydostawcy1: TMenuItem;
    Wydrukklientwbezobrotw1: TMenuItem;
    wszystkie1: TMenuItem;
    stycze1: TMenuItem;
    luty1: TMenuItem;
    marzec1: TMenuItem;
    kwiecie1: TMenuItem;
    maj1: TMenuItem;
    czerwiec1: TMenuItem;
    lipiec1: TMenuItem;
    sierpie1: TMenuItem;
    wrzesie1: TMenuItem;
    padziernik1: TMenuItem;
    listopad1: TMenuItem;
    grudzie1: TMenuItem;
    wszystkie2: TMenuItem;
    stycze2: TMenuItem;
    luty2: TMenuItem;
    marzec2: TMenuItem;
    kwiecie2: TMenuItem;
    maj2: TMenuItem;
    czerwiec2: TMenuItem;
    lipiec2: TMenuItem;
    sierpie2: TMenuItem;
    wrzesie2: TMenuItem;
    padziernik2: TMenuItem;
    listopad2: TMenuItem;
    grudzie2: TMenuItem;
    wszyscy1: TMenuItem;
    luty3: TMenuItem;
    marzec3: TMenuItem;
    kwiecie3: TMenuItem;
    maj3: TMenuItem;
    czerwiec3: TMenuItem;
    lipiec3: TMenuItem;
    sierpie3: TMenuItem;
    wrzesie3: TMenuItem;
    padziernik3: TMenuItem;
    listopad3: TMenuItem;
    grudzie3: TMenuItem;
    wszystkie3: TMenuItem;
    stycze3: TMenuItem;
    luty4: TMenuItem;
    marzec4: TMenuItem;
    kwiecie4: TMenuItem;
    maj4: TMenuItem;
    czerwiec4: TMenuItem;
    lipiec4: TMenuItem;
    sierpie4: TMenuItem;
    wrzesie4: TMenuItem;
    padziernik4: TMenuItem;
    listopad4: TMenuItem;
    grudzie4: TMenuItem;
    pmKontrah2: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    wszystkie: TMenuItem;
    styczen: TMenuItem;
    luty: TMenuItem;
    marzec: TMenuItem;
    kwiecien: TMenuItem;
    maj: TMenuItem;
    czerwiec: TMenuItem;
    lipiec: TMenuItem;
    sierpien: TMenuItem;
    wrzesien: TMenuItem;
    pazdziernik: TMenuItem;
    listopad: TMenuItem;
    grudzien: TMenuItem;
    Pokalistimportw1: TMenuItem;
    stycze5: TMenuItem;
    Poczkontrahentw1: TMenuItem;
    N1: TMenuItem;
    Koniec2: TMenuItem;
    KontrahS: TToolButton;
    pmRaporty: TPopupMenu;
    Oprogramie1: TMenuItem;
    Koniec3: TMenuItem;
    raporty: TOpenDialog;
    TabSheet9: TTabSheet;
    DBGrid3: TDBGrid;
    DataSource1: TDataSource;
    ToolButton1: TToolButton;
    Pracownicy1: TMenuItem;
    DodajCMR1: TMenuItem;
    rasy1: TMenuItem;
    Grupy1: TMenuItem;
    ToolButton21: TToolButton;
    ToolButton20: TToolButton;
    ToolButton22: TToolButton;
    Wczprzyciski1: TMenuItem;
    Dodajgadety1: TMenuItem;
    Aktualizacja1: TMenuItem;
    cRok: TComboBox;
    Label18: TLabel;
    Panel1: TPanel;
    JvDBNavigator3: TJvDBNavigator;
    jrbTypD: TJvRadioButton;
    jrbTypO: TJvRadioButton;
    jeNazwa: TJvEdit;
    JvEdit1: TJvEdit;
    jeMiasto: TJvEdit;
    Drukujkartkontrahenta1: TMenuItem;
    Aktywnidostawcy1: TMenuItem;
    //nr_import : Integer;
    procedure ToolButton10Click(Sender: TObject);
    procedure ImpExcelClick(Sender: TObject);
    procedure KontrahDClick(Sender: TObject);
    procedure KontorahenciEur1Click(Sender: TObject);
    procedure bKopiujClick(Sender: TObject);
    procedure bZapiszClick(Sender: TObject);
    procedure bbZapiszClick(Sender: TObject);
    //procedure comboBChange(Sender: TObject);
    //procedure comboBClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure JvDBNavigator4Click(Sender: TObject; Button: TNavigateBtn);
    procedure DBGrid1Enter(Sender: TObject);
    procedure dbgImportCSVCellClick(Column: TColumn);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure Button1Click(Sender: TObject);
    procedure e08Change(Sender: TObject);
    procedure e01Change(Sender: TObject);
    procedure e06Change(Sender: TObject);
    procedure e03Change(Sender: TObject);
    procedure e02Change(Sender: TObject);
    procedure e07Change(Sender: TObject);
    procedure e04Change(Sender: TObject);
    procedure e05Change(Sender: TObject);
    procedure e10Change(Sender: TObject);
    procedure e09Change(Sender: TObject);
    procedure rgTypClick(Sender: TObject);
    procedure ustawSiatke(Sender: TObject);
    procedure dbGridPokazKEnter(Sender: TObject);
    procedure eNazwaChange(Sender: TObject);
    procedure jrbTypDClick(Sender: TObject);
    procedure jrbTypOClick(Sender: TObject);
    procedure jeNIPChange(Sender: TObject);
    procedure jeNazwaChange(Sender: TObject);
    procedure jeNazwaDblClick(Sender: TObject);
    procedure jeNIPDblClick(Sender: TObject);
    procedure JvBitBtn1Click(Sender: TObject);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure dbGridPokazKCellClick(Column: TColumn);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure jvdbKartaObrotowDrawDataCell(Sender: TObject;
      const Rect: TRect; Field: TField; State: TGridDrawState);
    procedure jpcGlownaChange(Sender: TObject);
    procedure Pierwsza1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SzukajNIP(NIP: String);
    procedure AutomatWstaw();
    procedure JvImgBtn1Click(Sender: TObject);
    procedure Raporty1Click(Sender: TObject);
    procedure Zplikutxt1Click(Sender: TObject);
    procedure KasowanietabeliImport1Click(Sender: TObject);
    procedure KasowanietabeliObroty1Click(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure jvbZapiszZmianyClick(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure Poprawdanekontrahenta1Click(Sender: TObject);
    procedure Usudanekontrahenta1Click(Sender: TObject);
    procedure KartaDostawcyShow(Sender: TObject);
    procedure Pokaobrotykontrahenta1Click(Sender: TObject);
    procedure pmKontrahPopup(Sender: TObject);
    procedure pmTabelaRClick(Sender: TObject);
    procedure eSzukajNChange(Sender: TObject);
    procedure eSzukajNIPChange(Sender: TObject);
    procedure Pokaobrotydostawcy1Click(Sender: TObject);
    procedure Wydruksesjiimportu1Click(Sender: TObject);
    procedure DlaMiesiacaObrotySklepy(miesiac: String);
    procedure DlaMiesiacaObrotySklepyExcel(miesiac,typ: String);
    procedure DlaMiesiacaObrotySklepyR(okres: String;nr_kontrahenta: Integer);
    procedure OdswiezBazy();
    procedure MenuItem3Click(Sender: TObject);
    procedure Pokalistimportw1Click(Sender: TObject);
    procedure Prowizje1Click(Sender: TObject);
    procedure Poczkontrahentw1Click(Sender: TObject);
    procedure Koniec3Click(Sender: TObject);
    procedure Koniec2Click(Sender: TObject);
    procedure listaPlikiClick(Sender: TObject);
    procedure KartDostwacyClick(Sender: TObject);
    procedure KatSklepyClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure KontrahSClick(Sender: TObject);
    procedure DodajCMR1Click(Sender: TObject);
    procedure rasy1Click(Sender: TObject);
    procedure Pracownicy1Click(Sender: TObject);
    procedure Grupy1Click(Sender: TObject);
    procedure jeMiastoClick(Sender: TObject);
    procedure jeMiastoChange(Sender: TObject);
    procedure ToolButton22Click(Sender: TObject);
    procedure Wczprzyciski1Click(Sender: TObject);
    procedure Dostawcy1Click(Sender: TObject);
    procedure Dodajgadety1Click(Sender: TObject);
    procedure Aktualizacja1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function NrMiesiaca(miesiac: String): Integer;
    procedure Drukujkartkontrahenta1Click(Sender: TObject);
    procedure Aktywnidostawcy1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nr_import,ile : Integer;
    nr_dost: Integer;
  end;

  type TMiesiace = (styczen,luty,marzec,kwiecien,maj,czerwiec,lipiec,sierpien,wrzesien,pazdziernik,listopad,grudzien);
       TKwartal = (Ikwartal,IIkwartal,IIIkwartal,IVkwartal);
var
  frGlowny: TfrGlowny;
  status,przyciski : boolean;
  nr_import,ile : Integer;
  zakladka_glowna: TBookmark;
  obrot_glowny: Real;


implementation
uses baza,kontrahent,dodajR, raport_n, info, obrotyK, rera, obrotyD,
  raport_imp, rp_SklepObrot, rp_ObrSklep, rp_ObrDost, ListaImp,
  LaczKartoteki, Rap_Importy, KreaotorRap, pracownicy, trasy,
  grupy, towary, wydaj_gadzety, update, aktywni_d;
{$IFnDEF FPC}
  {$R *.dfm}
{$ELSE}
  {$R *.lfm}
{$ENDIF}

procedure TfrGlowny.ToolButton10Click(Sender: TObject);
begin
Close;
end;

function StringNIP(parA: String ): String;
begin
while Pos('-',parA) > 0 do
      begin
           Delete(parA,Pos('-',parA),1);
           StringNIP(parA);
      end;
Result := parA;
end;

procedure TfrGlowny.ImpExcelClick(Sender: TObject);
var
      x,ile,licznik : Integer;
      parA, parBStr,plik,info,plik_nazwa,plik_rozszezenie : String;
      parB,suma : Real;
      TF : TextFile;
begin
KasowanietabeliImport1Click(Sender);
dm.PlikCSV.Refresh;
dm.PlikCSV.Close;
odTxt.Execute;
plik := odTxt.FileName;
if plik <> '' then                                                              //if od sprw czy wybrano plik
begin
dm.PlikCSV.FileName := plik;
dm.PlikCSV.Open;
ile := dm.PlikCSV.RecordCount;
nr_import := 0; x := 0;
with dm.ibqZapisz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_IMPORTU FROM IMPORT');
          Open;
          Last;
          if FieldByName('NR_IMPORTU').IsNull then
             nr_import := 0
          else
              nr_import := FieldValues['NR_IMPORTU'];
     end;
nr_import := nr_import + 1;
dm.PlikCSV.First;
jpbPasek.Max := ile;
if not dm.trCSV2Firebird.InTransaction then dm.trCSV2Firebird.StartTransaction;
//for x := 1 to ile do
while not dm.PlikCSV.Eof do
begin
x := x + 1;
jpbPasek.Position := x;
if not dm.PlikCSV.Fields.Fields[0].IsNull then
   begin
        parA := dm.PlikCSV.Fields.Fields[0].AsString;
        if Pos('-',parA) > 0 then parA := StringNIP(parA);
   end;
//else
    //parA := '0';
if not dm.PlikCSV.Fields.Fields[1].IsNull then
   begin
        parBStr := dm.PlikCSV.Fields.Fields[1].asString;
        Delete(parBStr,Pos(' ',parBStr),1);
        parB := StrToFloat(parBStr);
   end;
//else
    //parB := 0;
//if dm.PlikCSV.Fields.Fields[0].IsNull and (parB > 0) then break;
if (parA <> Null) or (parB <> Null) then
with dm.ibqCSV2Firebird do
      begin
            Close;
            SQL.Clear;
            SQL.Add('INSERT INTO IMPORT(NIP,OBROT,NR_IMPORTU,NR_DOSTAWCA)');
            SQL.Add('VALUES(:A,:B,:C,:D)');
            UnPrepare;
                  ParamByName('A').AsString := parA;
                  ParamByName('B').AsFloat := parB;
                  ParamByName('C').AsInteger := nr_import;
                  ParamByName('D').AsInteger := 1;
            Prepare;
            Open;
      end;
      dm.PlikCSV.Next;
end;
if dm.trCSV2Firebird.InTransaction then dm.trCSV2Firebird.Commit;

with dm.ibqCSV2Firebird do
      begin
            Close;
            SQL.Clear;
            SQL.Add('SELECT NIP,SUM(OBROT) AS OBROT FROM IMPORT');
            SQL.Add('WHERE NR_IMPORTU = :A');
            SQL.Add('GROUP BY NIP');
            UnPrepare;
                  ParamByName('A').AsInteger := nr_import;
            Prepare;
            Open;
      end;
      while not dm.ibqCSV2Firebird.Eof do
             begin
                  licznik := licznik + 1;
                  dm.ibqCSV2Firebird.Next;
             end;
      //info := 'Record count da� taki wynik: ' + IntToStr(dm.ibqCSV2Firebird.RecordCount) + ' licznik zliczy� ' + IntToStr(licznik);
      sb.SimpleText := 'Importujesz plik: ' + plik;
      jpcGlowna.ActivePageIndex := 3;
      jpbPasek.Max := 0;
      frInfo.pc.ActivePageIndex := 0;
      frInfo.lbNIP.Clear;
      frInfo.ShowModal;
end;                                                                            //koniec if od sprw czy wybrano plik
end;

procedure TfrGlowny.KontrahDClick(Sender: TObject);
begin
//
end;

procedure TfrGlowny.KontorahenciEur1Click(Sender: TObject);
begin
//Wstawianie kontrahentów Euro do bazy firebirda

end;

procedure TfrGlowny.bKopiujClick(Sender: TObject);
var
     dataR,dataM,dataD,dataN,S : String;
     Poz : Integer;
begin
imie_naz.Font.Style := imie_naz.Font.Style - [fsItalic];
imie_naz.Text := Trim(dbImie_naz.Text);
imie_naz.Font.Color := clDefault;

Nazwa.Font.Style := Nazwa.Font.Style - [fsItalic];
Nazwa.Text := Trim(dbNazwa.Text);
Nazwa.Font.Color := clDefault;

kod_p.Font.Style := kod_p.Font.Style - [fsItalic];
kod_p.Text := Trim(dbkod_p.Text);
kod_p.Font.Color := clDefault;

Miasto.Font.Style := Miasto.Font.Style - [fsItalic];
Miasto.Text := Trim(dbMiasto.Text);
Miasto.Font.Color := clDefault;

Telefon.Font.Style := Telefon.Font.Style - [fsItalic];
Telefon.Text := Trim(dbTelefon.Text);
Telefon.Font.Color := clDefault;

Ulica.Font.Style := Ulica.Font.Style - [fsItalic];
Ulica.Text := Trim(dbUlica.Text);
Ulica.Font.Color := clDefault;

S := Trim(dbNIP.Text);
while Pos('-', S) > 0 do
     begin
          Poz := Pos('-',S);
          Delete(S,Poz,1);
     end;

NIP.Font.Style := NIP.Font.Style - [fsItalic];
NIP.Text := S;
NIP.Font.Color := clDefault;

{dataD := Copy(dbData_P.Text,0,2);
dataM := Copy(dbData_P.Text,4,2);
dataR := Copy(dbData_P.Text,7,4);
dataN := dataR + '-' + dataM + '-' + dataD;
Data_Prz.Date := StrToDate(dataN);   }

Metraz.Font.Style := Metraz.Font.Style - [fsItalic];
Metraz.Text := Trim(dbMetraz.Text);
Metraz.Font.Color := clDefault;

Kategoria.Font.Style := Kategoria.Font.Style - [fsItalic];
Kategoria.Text := Trim(dbKategoria.Text);
Kategoria.Font.Color := clDefault;

Koncesja.Font.Style := Koncesja.Font.Style - [fsItalic];
Koncesja.Text := Trim(dbKoncesja.Text);
Koncesja.Font.Color := clDefault;

end;

procedure TfrGlowny.bZapiszClick(Sender: TObject);                               //ZAPIS KONTRAHENTA DO BAZY
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqZapisImportExcel do
begin
     Close;
     SQL.Clear;
     SQL.Add('INSERT INTO KONTRAH(RODZAJ,IMIE_NAZWISKO,NAZWA,NIP,ULICA,MIASTO,KOD_P,DATA_PRZYSTAPIENIA,TELEFON,KONCESJA,METRAZ,KATEGORIA,UWAGI)');
     SQL.Add('VALUES(:RODZAJ,:IMIE_NAZWISKO,:NAZWA,:NIP,:ULICA,:MIASTO,:KOD_P,:DATA_PRZYSTAPIENIA,:TELEFON,:KONCESJA,:METRAZ,:KATEGORIA,:UWAGI)');
     UnPrepare;
          ParamByName('RODZAJ').AsString:='D';
          ParamByName('IMIE_NAZWISKO').AsString:='BRAK';
          ParamByName('NAZWA').AsString:=UpperCase(Nazwa.Text);
          ParamByName('NIP').AsString:=NIP.Text;
          ParamByName('ULICA').AsString:=UpperCase(Ulica.Text);
          ParamByName('MIASTO').AsString:=UpperCase(Miasto.Text);
          ParamByName('KOD_P').AsString:=kod_p.Text;
          //ParamByName('DATA_PRZYSTAPIENIA').AsDate:=Data_Prz.Date;
          ParamByName('DATA_PRZYSTAPIENIA').AsDate:=now();
          ParamByName('TELEFON').AsString:=Telefon.Text;
          ParamByName('KONCESJA').AsString:='-';
          ParamByName('METRAZ').AsString:='-';
          ParamByName('KATEGORIA').AsString:='-';
          ParamByName('UWAGI').AsString:=Uwagi.Text;
     Prepare;
     Open;
end;

if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;

{with dm.IBQuery1 do
begin
     Close;
     SQL.Clear;
     SQL.Add('SELECT * FROM KONTRAH');
     Open;
end;

dm.TabelaKontrah.Active := True;

imie_naz.Font.Style := imie_naz.Font.Style + [fsItalic];
imie_naz.Text := 'imi� i nazwisko';
imie_naz.Font.Color := clMenuHighlight;

Nazwa.Font.Style := Nazwa.Font.Style + [fsItalic];
Nazwa.Text := 'nazwa firmy';
Nazwa.Font.Color := clMenuHighlight;

kod_p.Font.Style := kod_p.Font.Style + [fsItalic];
kod_p.Text := 'kod pocztowy';
kod_p.Font.Color := clMenuHighlight;

Miasto.Font.Style := Miasto.Font.Style + [fsItalic];
Miasto.Text := 'miasto';
Miasto.Font.Color := clMenuHighlight;

Telefon.Font.Style := Telefon.Font.Style + [fsItalic];
Telefon.Text := 'telefon';
Telefon.Font.Color := clMenuHighlight;

Ulica.Font.Style := Ulica.Font.Style + [fsItalic];
Ulica.Text := 'ulica';
Ulica.Font.Color := clMenuHighlight;

NIP.Font.Style := NIP.Font.Style + [fsItalic];
NIP.Text := 'NIP';
NIP.Font.Color := clMenuHighlight;

Metraz.Font.Style := Metraz.Font.Style + [fsItalic];
Metraz.Text := 'metra�';
Metraz.Font.Color := clMenuHighlight;

Kategoria.Font.Style := Kategoria.Font.Style + [fsItalic];
Kategoria.Text := 'kategoria';
Kategoria.Font.Color := clMenuHighlight;

Koncesja.Font.Style := Koncesja.Font.Style + [fsItalic];
Koncesja.Text := 'koncesja';
Koncesja.Font.Color := clMenuHighlight;  }
end;

procedure TfrGlowny.bbZapiszClick(Sender: TObject);
begin
{if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqSQL do
begin
     Close;
     SQL.Clear;
     SQL.Add('INSERT INTO KONTRAH(RODZAJ,IMIE_NAZWISKO,NAZWA,NIP,ULICA,MIASTO,KOD_P,DATA_PRZYSTAPIENIA,TELEFON,KONCESJA,METRAZ,KATEGORIA,UWAGI)');
     SQL.Add('VALUES(:RODZAJ,:IMIE_NAZWISKO,:NAZWA,:NIP,:ULICA,:MIASTO,:KOD_P,:DATA_PRZYSTAPIENIA,:TELEFON,:KONCESJA,:METRAZ,:KATEGORIA,:UWAGI)');
     UnPrepare;
          case rgTypK.ItemIndex of
               0: ParamByName('RODZAJ').AsString:='D';
               1: ParamByName('RODZAJ').AsString:='O';
          end;
          ParamByName('IMIE_NAZWISKO').AsString := UpperCase(edImieNaz.Text);
          ParamByName('NAZWA').AsString := UpperCase(edNazwa.Text);
          ParamByName('NIP').AsString := edNIP.Text;
          ParamByName('ULICA').AsString := UpperCase(edUlica.Text);
          ParamByName('MIASTO').AsString := UpperCase(edMiasto.Text);
          ParamByName('KOD_P').AsString := edKodP.Text;
          ParamByName('DATA_PRZYSTAPIENIA').AsDate := DataPrzystapienia.Date;
          ParamByName('TELEFON').AsString := edTelefon.Text;
          ParamByName('KONCESJA').AsString := UpperCase(edKoncesja.Text);
          ParamByName('METRAZ').AsString := edMetraz.Text;
          ParamByName('KATEGORIA').AsString := UpperCase(edKategoria.Text);
          ParamByName('UWAGI').AsString := mUwagi.Text;
     Prepare;
     Open;
end;

if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;  }

end;

{procedure TfrGlowny.comboBChange(Sender: TObject);
begin
with dm.ibqSQL do
begin
     Close;
     SQL.Clear;
     SQL.Add('SELECT NAZWA FROM KONTRAH');
     SQL.Add('WHERE NAZWA LIKE :A');
          ParamByName('A').AsString := '%' + comboB.Text;
     Open;
end;
while not dm.ibqSQL.Eof do
     begin
          if not dm.ibqSQL.FieldByName('NAZWA').IsNull then
          comboB.Items.Add(dm.ibqSQL.FieldValues['NAZWA']);
          dm.ibqSQL.Next;
     end;
end;

procedure TfrGlowny.comboBClick(Sender: TObject);
begin
with dm.ibqSQL do
begin
     Close;
     SQL.Clear;
     SQL.Add('SELECT NAZWA FROM KONTRAH');
     SQL.Add('WHERE NAZWA LIKE :A');
          ParamByName('A').AsString := '%' + comboB.Text;
     Open;
end;
while not dm.ibqSQL.Eof do
     begin
          if not dm.ibqSQL.FieldByName('NAZWA').IsNull then
          comboB.Items.Add(dm.ibqSQL.FieldValues['NAZWA']);
          dm.ibqSQL.Next;
     end;
end; }

procedure TfrGlowny.Edit1Change(Sender: TObject);
begin
with dm.ibqDostawca do
begin
     Close;
     SQL.Clear;
     SQL.Add('SELECT NR_KONTRAH,NAZWA FROM KONTRAH');
     SQL.Add('WHERE NAZWA LIKE :A');
          ParamByName('A').AsString := '%' + UpperCase(Edit1.Text) + '%';
     Open;
     end;
end;
procedure TfrGlowny.JvDBNavigator4Click(Sender: TObject;
  Button: TNavigateBtn);
var
  zakladka: TBookmark;

begin
with dm.ibqSQL do
     begin
          SzukajNIP(dm.ibqCSV2Firebird.FieldValues['NIP']);        //dm.ibqCSV2Firebird.FieldValues['NIP'];
          if FieldByName('NR_KONTRAH').IsNull then
            if MessageBox(Handle,'Brak kontrahenta w bazie kontrahent�w, czy chcesz dodac kontrahenta do bazy ?', 'Dodawanie kontrahenta', MB_YESNO + MB_ICONQUESTION) = IdYes then
              begin
                frDodajK.edNIP.Text := dm.ibqCSV2Firebird.FieldValues['NIP'];
                frDodajK.ShowModal;
                zakladka := dm.ibqCSV2Firebird.GetBookmark;
                dm.serwer.Close;
                dm.serwer.Open;

                Close;
                SQL.Clear;
                SQL.Add('SELECT NR_KONTRAH,IMIE_NAZWISKO,NAZWA,NIP,ULICA FROM KONTRAH');
                Open;

                with dm.ibqCSV2Firebird do
                  begin
                    Close;
                    SQL.Clear;
                    SQL.Add('SELECT NIP,SUM(OBROT) AS OBROT FROM IMPORT');
                    SQL.Add('GROUP BY NIP');
                    Open;
                  end;
                  dm.ibqCSV2Firebird.GotoBookmark(zakladka);
              end;
     end;

     dbgDoObrotu.Columns.Items[0].Visible := False;
     dbgDoObrotu.Columns.Items[1].FieldName := 'IMIE_NAZWISKO';
     dbgDoObrotu.Columns.Items[1].Title.Caption := 'Imi� i nazwisko';
     dbgDoObrotu.Columns.Items[1].Width := 100;
     dbgDoObrotu.Columns.Items[2].FieldName := 'NAZWA';
     dbgDoObrotu.Columns.Items[2].Width := 250;
     dbgDoObrotu.Columns.Items[3].Visible := False;
     dbgDoObrotu.Columns.Items[4].FieldName := 'ULICA';
     dbgDoObrotu.Columns.Items[4].Width := 100;
     Label3.Caption := FloatToStr(dm.ibqCSV2Firebird.FieldValues['OBROT']) + ' z�';
     Label5.Caption := '';
     Label6.Caption := '';
end;

procedure TfrGlowny.DBGrid1Enter(Sender: TObject);
begin
with dm.ibqProwizje do
begin
     Close;
          ParamByName('A').AsString := dm.ibqDostawca.FieldValues['NR_KONTRAH'];
     Open;
     end;
end;

procedure TfrGlowny.dbgImportCSVCellClick(Column: TColumn);                    //WYSZUKIWANIE KONTRAHENTA PO
var
  zakladka: TBookmark;
begin
with dm.ibqSQL do
     begin
          SzukajNIP(dm.ibqCSV2Firebird.FieldValues['NIP']);        //dm.ibqCSV2Firebird.FieldValues['NIP'];
          if FieldByName('NR_KONTRAH').IsNull then
            if MessageBox(Handle,'Brak kontrahenta w bazie kontrahent�w, czy chcesz dodac kontrahenta do bazy ?', 'Dodawanie kontrahenta', MB_YESNO + MB_ICONQUESTION) = IdYes then
              begin
                frDodajK.edNIP.Text := dm.ibqCSV2Firebird.FieldValues['NIP'];
                frDodajK.ShowModal;
                zakladka := dm.ibqCSV2Firebird.GetBookmark;
                dm.serwer.Close;
                dm.serwer.Open;

                Close;
                SQL.Clear;
                SQL.Add('SELECT NR_KONTRAH,IMIE_NAZWISKO,NAZWA,NIP,ULICA FROM KONTRAH');
                Open;

                with dm.ibqCSV2Firebird do
                  begin
                    Close;
                    SQL.Clear;
                    SQL.Add('SELECT NIP,SUM(OBROT) AS OBROT FROM IMPORT');
                    SQL.Add('GROUP BY NIP');
                    Open;
                  end;
                  dm.ibqCSV2Firebird.GotoBookmark(zakladka);
              end;
     end;

     dbgDoObrotu.Columns.Items[0].Visible := False;
     dbgDoObrotu.Columns.Items[1].FieldName := 'IMIE_NAZWISKO';
     dbgDoObrotu.Columns.Items[1].Title.Caption := 'Imi� i nazwisko';
     dbgDoObrotu.Columns.Items[1].Width := 100;
     dbgDoObrotu.Columns.Items[2].FieldName := 'NAZWA';
     dbgDoObrotu.Columns.Items[2].Width := 250;
     dbgDoObrotu.Columns.Items[3].Visible := False;
     dbgDoObrotu.Columns.Items[4].FieldName := 'ULICA';
     dbgDoObrotu.Columns.Items[4].Width := 100;
     Label3.Caption := FloatToStr(dm.ibqCSV2Firebird.FieldValues['OBROT']) + ' z�';
     Label5.Caption := '';
     Label6.Caption := '';
end;

procedure TfrGlowny.DBGrid2CellClick(Column: TColumn);
var
     obroty,retro,wynik,retroE,wynikE : Real;
     prowizja : PChar;
     nr_sklep,nr_dostawca,ile,x : Integer;
     nazwa_dostawca : String;
begin
dm.ibqCSV2Firebird.First;                                                       //WYWO�ANIE AUTOMATYZACJI ZAPISU
if MessageBox(Handle,'Czy zapisa� wybran� prowizj� do bazy ?', 'Dodawanie obrot�w', MB_YESNO + MB_ICONQUESTION) = IdYes then
  begin
    AutomatWstaw;
    nazwa_dostawca := dm.ibqDostawca.FieldValues['NAZWA'];
    obroty := obrot_glowny;
    ShowMessage('Dane o obrotach z dostawc� ' + nazwa_dostawca + ' w wysoko�ci: ' + FloatToStrF(obroty, ffCurrency, 7, 2) + ' zosta�y zapisane w bazie');
    JvGroupBox1.Visible := False;
  end
else
  begin
    obroty := dm.ibqCSV2Firebird.FieldValues['OBROT'];
    retro := dm.ibqProwizje.FieldValues['RETRO_KLIENT'];                        //OBLICZANIE OBROT�W SKLEPU I EURO
    wynik := obroty * (retro/100);
    retroE := StrToFloat(dm.ibqProwizje.FieldValues['RETRO']);
    wynikE := obroty * (retroE/100);
    Label5.Caption := FloatToStr(wynik) + ' z�';
    Label6.Caption := FloatToStr(wynikE) + ' z�';
  end;
  jpbPasek.Position := 0;
  obrot_glowny := 0;                                                                              //ZAMYKANIE IMPORTU
end;

procedure TfrGlowny.Button1Click(Sender: TObject);                               //POJEDY�CZY ZAPIS OBROTU
var                                                                              //SKLEPU
     nr_sklep,nr_dostawca : Integer;
     nazwa_dostawca,okres : String;
     obroty : Real;
begin
nr_sklep := dm.ibqSQL.FieldValues['NR_KONTRAH'];
nr_dostawca := dm.ibqDostawca.FieldValues['NR_KONTRAH'];
nazwa_dostawca := dm.ibqDostawca.FieldValues['NAZWA'];
obroty := dm.ibqCSV2Firebird.FieldValues['OBROT'];
okres := Trim(dm.ibqProwizje.FieldValues['OKRES']);
if not dm.ibtZapisz.InTransaction then dm.ibtZapisz.StartTransaction;
with dm.ibqZapisz do
     begin
              Close;
              SQL.Clear;
              SQL.Add('INSERT INTO OBROTY(NR_KONTRAH_SKLEP,NR_KONTRACH_DOST,NAZWA_D,DATA_ZAPISU,OBROTY,PROWIZJA_PROC,PROW_E_PROC,OKRES_M,OKRES_Q)');
              SQL.Add('VALUES(:NR_KONTRAH_SKLEP,:NR_KONTRACH_DOST,:NAZWA_D,CURRENT_DATE,:OBROTY,:PROWIZJA_PROC,:PROW_E_PROC,:OKRES_M,:OKRES_Q)');
              UnPrepare;
                ParamByName('NR_KONTRAH_SKLEP').AsInteger := nr_sklep;
                ParamByName('NR_KONTRACH_DOST').AsInteger := nr_dostawca;
                ParamByName('NAZWA_D').AsString := nazwa_dostawca;
                ParamByName('OBROTY').asFloat := obroty;
                ParamByName('PROWIZJA_PROC').asFloat := dm.ibqProwizje.FieldValues['RETRO_KLIENT'];
                ParamByName('PROW_E_PROC').asFloat := dm.ibqProwizje.FieldValues['RETRO'];
                if CompareText(okres,'M') = 0 then ParamByName('OKRES_M').AsString := cMiesiac.Text;
                if CompareText(okres,'Q') = 0 then ParamByName('OKRES_K').AsString := cKwartal.Text;
              Prepare;
              Open;
     end;
     if dm.ibtZapisz.InTransaction then dm.ibtZapisz.Commit;
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_KONTRAH,IMIE_NAZWISKO,NAZWA,NIP,ULICA FROM KONTRAH');
          SQL.Add('WHERE NR_KONTRAH = :A');
          UnPrepare;
               ParamByName('A').AsInteger := nr_sklep;
          Prepare;
          Open;
     end;
     Edit1.Text := '';
     dbgDoObrotu.Columns.Items[0].Visible := False;
     dbgDoObrotu.Columns.Items[1].FieldName := 'IMIE_NAZWISKO';
     dbgDoObrotu.Columns.Items[1].Title.Caption := 'Imi� i nazwisko';
     dbgDoObrotu.Columns.Items[1].Width := 100;
     dbgDoObrotu.Columns.Items[2].FieldName := 'NAZWA';
     dbgDoObrotu.Columns.Items[2].Width := 250;
     dbgDoObrotu.Columns.Items[3].Visible := False;
     dbgDoObrotu.Columns.Items[4].FieldName := 'ULICA';
     dbgDoObrotu.Columns.Items[4].Width := 100;
     Label5.Caption := '';
     Label6.Caption := '';
end;

procedure TfrGlowny.ustawSiatke(Sender: TObject);
begin
//parametry DBGrid
with dbGridPokazK do
     begin
          Columns.Items[0].Visible := False;
          Columns.Items[1].FieldName := 'RODZAJ';
          Columns.Items[1].Title.Caption := 'Rodzaj';
          Columns.Items[1].Width := 70;
          Columns.Items[2].FieldName := 'IMIE_NAZWISKO';
          Columns.Items[2].Title.Caption := 'Imi� i nazwisko';
          Columns.Items[2].Width := 150;
          Columns.Items[3].FieldName := 'NAZWA';
          Columns.Items[3].Width := 250;
          Columns.Items[4].FieldName := 'ULICA';
          Columns.Items[4].Width := 100;
     end;
end;

procedure TfrGlowny.e08Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');
          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE NAZWA LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e08.Text) + '%';
               end;
               1: begin
                    SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e08.Text) + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e08.Text) + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e01Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE IMIE_NAZWISKO LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
               end;
               1: begin
                    SQL.Add('WHERE IMIE_NAZWISKO LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE IMIE_NAZWISKO LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e06Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE NIP LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e06.Text + '%';
               end;
               1: begin
                    SQL.Add('WHERE NIP LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e06.Text + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE NIP LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e06.Text + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(e06.Text) + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e03Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE ULICA LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e03.Text) + '%';
               end;
               1: begin
                    SQL.Add('WHERE ULICA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e03.Text) + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE ULICA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e03.Text) + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(e03.Text) + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e02Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE KOD_P LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e02.Text + '%';
               end;
               1: begin
                    SQL.Add('WHERE KOD_P LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e02.Text + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE KOD_P LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e02.Text + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(e02.Text) + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e07Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE MIASTO LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e07.Text) + '%';
               end;
               1: begin
                    SQL.Add('WHERE MIASTO LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e07.Text) + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE MIASTO LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e07.Text) + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(e07.Text) + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e04Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE KONCESJA LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e04.Text) + '%';
               end;
               1: begin
                    SQL.Add('WHERE KONCESJA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e04.Text) + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE KONCESJA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e04.Text) + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(e04.Text) + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e05Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE METRAZ LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e05.Text) + '%';
               end;
               1: begin
                    SQL.Add('WHERE METRAZ LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e05.Text) + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE METRAZ LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e05.Text) + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := UpperCase(e05.Text);
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e10Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE KATEGORIA LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
               end;
               1: begin
                    SQL.Add('WHERE KATEGORIA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE KATEGORIA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(e01.Text) + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.e09Change(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');

          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE TELEFON LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e09.Text + '%';
               end;
               1: begin
                    SQL.Add('WHERE TELEFON LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e09.Text + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE TELEFON LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e09.Text + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          UnPrepare;
               ParamByName('A').AsString := '%' + e09.Text + '%';
          Prepare;
          Open;
     end;
     ustawSiatke(Sender);
end;

procedure TfrGlowny.rgTypClick(Sender: TObject);
begin
e01.Text := '';e02.Text := '';e03.Text := '';e04.Text := '';e05.Text := '';
e06.Text := '';e07.Text := '';e08.Text := '';e09.Text := '';e10.Text := '';
end;

procedure TfrGlowny.dbGridPokazKEnter(Sender: TObject);
begin
//zmiana := False;
end;

procedure TfrGlowny.eNazwaChange(Sender: TObject);
begin
with dm.ibqSQL do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM KONTRAH');
          case rgTyp.ItemIndex of
               0: begin
                    SQL.Add('WHERE NAZWA LIKE :A');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e08.Text + '%';
               end;
               1: begin
                    SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e08.Text + '%';
                    ParamByName('B').AsString := 'O';
               end;
               2: begin
                    SQL.Add('WHERE NAZWA LIKE :A AND RODZAJ = :B');
                    UnPrepare;
                    ParamByName('A').AsString := '%' + e08.Text + '%';
                    ParamByName('B').AsString := 'D';
               end;
               end;

          Prepare;
          Open;
     end;
end;

procedure TfrGlowny.jrbTypDClick(Sender: TObject);
begin
with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE RODZAJ = :A  AND AKTYWNY = 1');
    ParamByName('A').AsString := 'D';
    Open;
  end;
end;

procedure TfrGlowny.jrbTypOClick(Sender: TObject);
begin
//GroupBox3.Visible := False;
with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE RODZAJ = :A  AND AKTYWNY = 1');
    ParamByName('A').AsString := 'O';
    Open;
  end;
end;

procedure TfrGlowny.jeNIPChange(Sender: TObject);
begin

with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE RODZAJ = :A AND NIP LIKE :B AND AKTYWNY = 1');
    if jrbTypD.Checked then ParamByName('A').AsString := 'D';
    if jrbTypO.Checked then ParamByName('A').AsString := 'O';
    ParamByName('B').AsString := '%' + jvEdit1.Text + '%';
    Open;
  end;
end;

procedure TfrGlowny.jeNazwaChange(Sender: TObject);
begin

with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE RODZAJ = :A AND NAZWA LIKE :B AND AKTYWNY = 1');
    if jrbTypD.Checked then ParamByName('A').AsString := 'D';
    if jrbTypO.Checked then ParamByName('A').AsString := 'O';
    ParamByName('B').AsString := '%' + UpperCase(jeNazwa.Text) + '%';
    Open;
  end;
end;

procedure TfrGlowny.jeNazwaDblClick(Sender: TObject);
begin
jeNazwa.Text := '';
end;

procedure TfrGlowny.jeNIPDblClick(Sender: TObject);
begin
jvEdit1.Text := '';
end;

procedure TfrGlowny.JvBitBtn1Click(Sender: TObject);
begin
rgTypClick(Sender);
jPC.ActivePageIndex := 0;
end;

procedure TfrGlowny.JvDBGrid1CellClick(Column: TColumn);
var
   rodzaj : String;
begin
rodzaj := Trim(dm.ibqKontrahenci.FieldValues['RODZAJ']);
if  CompareText(rodzaj, 'D') = 0 then
   begin
        //GroupBox3.Visible := true;
        //leDostawcaProw.Text := dm.ibqKontrahenci.FieldValues['NAZWA'];
   end;
end;

procedure TfrGlowny.dbGridPokazKCellClick(Column: TColumn);
var nr_sklep : Integer;
begin
jPC.ActivePageIndex := 1;
//PC_KK.ActivePageIndex := 1;
nr_sklep := dm.ibqSQL.FieldValues['NR_KONTRAH'];
with dm.ibqKartaK do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM OBROTY');
    SQL.Add('WHERE NR_KONTRAH_SKLEP = :A');
    SQL.Add('ORDER BY DATA_ZAPISU');
    UnPrepare;
      ParamByName('A').AsInteger := nr_sklep;
    Prepare;
    Open;
    end;

with jvdbKartaObrotow do
     begin
          Columns.Items[0].Visible := False;
          Columns.Items[1].Visible := False;
          Columns.Items[2].Visible := False;
          Columns.Items[3].Visible := False;
          Columns.Items[4].FieldName := 'NAZWA_D';
          Columns.Items[4].Title.Caption := 'Nazwa dostawcy';
          Columns.Items[4].Width := 250;
          Columns.Items[5].FieldName := 'DATA_ZAPISU';
          Columns.Items[5].Title.Caption := 'Data';
          Columns.Items[5].Width := 80;
          Columns.Items[6].FieldName := 'OBROTY';
          Columns.Items[6].Title.Caption := 'Obroty';
          Columns.Items[6].Width := 80;
          Columns.Items[7].FieldName := 'PROWIZJA_PROC';
          Columns.Items[7].Title.Caption := 'Prowizja %';
          Columns.Items[7].Width := 100;
          Columns.Items[8].FieldName := 'PROWIZJA_KWOTA';
          Columns.Items[8].Title.Caption := 'Prowizja kwota';
          Columns.Items[8].Width := 100;
          Columns.Items[9].FieldName := 'PROW_E_PROC';
          Columns.Items[9].Title.Caption := 'Prowizja % Euro';
          Columns.Items[9].Width := 100;
          Columns.Items[10].FieldName := 'PROW_E_KWOTA';
          Columns.Items[10].Title.Caption := 'Prowizja Euro';
          Columns.Items[10].Width := 100;
          Columns.Items[11].FieldName := 'OKRES_M';
          Columns.Items[11].Title.Caption := 'Miesi�c';
          Columns.Items[11].Width := 100;
          Columns.Items[12].FieldName := 'OKRES_Q';
          Columns.Items[12].Title.Caption := 'Kwarta�';
          Columns.Items[12].Width := 100;
          Columns.Items[13].FieldName := 'ZYSK';
          Columns.Items[13].Title.Caption := 'Zysk';
          Columns.Items[13].Width := 100;
     end;
end;

procedure TfrGlowny.DBGrid1DblClick(Sender: TObject);
begin

DBGrid1Enter(Sender);
end;

procedure TfrGlowny.jvdbKartaObrotowDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  if Field.FieldName = 'OBROTY'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'PROWIZJA_KWOTA'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'PROWIZJA_PROC'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 %';
  if Field.FieldName = 'PROW_E_PROC'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 %';
  if Field.FieldName = 'PROW_E_KWOTA'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
  if Field.FieldName = 'ZYSK'  then  //Nazwa pola reprezentuj�cego cen�
    TFloatField(Field).DisplayFormat := '0.00 z�';
end;

procedure TfrGlowny.jpcGlownaChange(Sender: TObject);
begin
jPC.ActivePageIndex := 0;
end;

procedure TfrGlowny.Pierwsza1Click(Sender: TObject);
begin
if jpcGlowna.Pages[0].Visible then jpcGlowna.Pages[0].Visible := False
        else jpcGlowna.Pages[0].Visible := True;
end;

procedure TfrGlowny.FormCreate(Sender: TObject);
var
   dd,mm,yyyy : Word;
   indeks,rok : Integer;
   INI : TINIFile;
   miesiac,kwartal,aktualny_miesiac,tekst: String;
begin
jpcGlowna.ActivePageIndex := 3;
PC_KK.ActivePageIndex := 0;
DecodeDate(Now(),yyyy,mm,dd);
if not FileExists(ExtractFilePath(Application.ExeName) + 'retro.ini') then
   begin
        INI := TINIFile.Create(ExtractFilePath(Application.ExeName) + 'retro.ini');
        try
           INI.WriteString('Okres','miesi�c','stycze�');
           INI.WriteString('Okres','kwarta�','I kwarta�');
           INI.WriteInteger('Okres','rok',2017);

        finally
               INI.Free;
        end;
   end
else
    begin
         INI := TINIFile.Create(ExtractFilePath(Application.ExeName) + 'retro.ini');
         try
            miesiac := INI.ReadString('Okres','miesi�c','stycze�');
            kwartal := INI.ReadString('Okres','kwarta�','I kwarta�');
            rok := INI.ReadInteger('Okres','rok',2017);
         finally
            INI.Free;
         end;
    end;
case mm of
        1: aktualny_miesiac := 'stycze�';
        2: aktualny_miesiac := 'luty';
        3: aktualny_miesiac := 'marzec';
        4: aktualny_miesiac := 'kwiecie�';
        5: aktualny_miesiac := 'maj';
        6: aktualny_miesiac := 'czerwiec';
        7: aktualny_miesiac := 'lipiec';
        8: aktualny_miesiac := 'sierpie�';
        9: aktualny_miesiac := 'wrzesie�';
        10: aktualny_miesiac := 'pa�dziernik';
        11: aktualny_miesiac := 'listopad';
        12: aktualny_miesiac := 'grudzie�';
     end;
     cMiesiac.Text := miesiac;
     cKwartal.Text := kwartal;
     cRok.Text := IntToStr(rok);
     tekst := 'Importowa�a�(e�) dane do miesi�ca ';
     tekst := tekst + miesiac;
     tekst := tekst + ' czy dalej b�dziesz zapisywa� do tego miesi�ca, czy do miesi�ca: ';
     tekst := tekst + aktualny_miesiac;
     if aktualny_miesiac <> miesiac then
        if MessageDlg(tekst, mtConfirmation, [mbYes,mbNo], 0) = mrYes then
           begin
                cMiesiac.Text := miesiac;
                cKwartal.Text := kwartal;
                cRok.Text := IntToStr(rok);
           end
        else
            begin
                 cMiesiac.Text := aktualny_miesiac;
                 case mm of
                      1,2,3 : cKwartal.Text := 'I kwarta�';
                      4,5,6 : cKwartal.Text := 'II kwarta�';
                      7,8,9 : cKwartal.Text := 'III kwarta�';
                      10,11,12 : cKwartal.Text := 'IV kwarta�';
                 end;
                 cRok.Text := IntToStr(CurrentYear);
            end;
end;

procedure TfrGlowny.SzukajNIP(NIP: String);        //
begin
with dm.ibqSQL do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT NR_KONTRAH,IMIE_NAZWISKO,NAZWA,NIP,ULICA FROM KONTRAH');
    SQL.Add('WHERE NIP = :A');
    UnPrepare;
      ParamByName('A').AsString := NIP;
    Prepare;
    Open;
  end;
end;

procedure TfrGlowny.AutomatWstaw;                                                 //Funkcja AutomatWstaw
var
     obroty,retro,wynik,retroE,wynikE : Real;
     prowizja : PChar;
     nr_sklep,nr_dostawca,nr_prowizje,ile,x,rok,nr_miesiac : Integer;
     nazwa_dostawca,okres,miesiac,kwartal : String;
begin
    with dm.ibqPoliczImport do
        begin
            Close;
            SQL.Clear;
            SQL.Add('SELECT COUNT(NIP) AS ILE FROM V_ILE');
            Open;
            ile := FieldValues['ILE'];
        end;

        nr_dostawca := nr_dost;
        nazwa_dostawca := dm.ibqDostawca.FieldValues['NAZWA'];
        nr_prowizje := dm.ibqProwizje.FieldValues['NR_PROWIZJE'];
        //dm.ibqDostawca.Next;

    for x := 1 to ile do
      begin
        SzukajNIP(dm.ibqCSV2Firebird.FieldValues['NIP']);                         //dm.ibqCSV2Firebird.FieldValues['NIP'];
        nr_sklep := dm.ibqSQL.FieldValues['NR_KONTRAH'];
        dm.ibqSQL.Next;
        obroty := dm.ibqCSV2Firebird.FieldValues['OBROT'];
        dm.ibqCSV2Firebird.Next;
        retro := dm.ibqProwizje.FieldValues['RETRO_KLIENT'];
        //dm.ibqProwizje.Next;
        okres := Trim(dm.ibqProwizje.FieldValues['OKRES']);
        miesiac := cMiesiac.Text;
        kwartal := cKwartal.Text;
        rok := StrToInt(cRok.Text);
        nr_miesiac := NrMiesiaca(miesiac);
        if not dm.ibtZapisz.InTransaction then dm.ibtZapisz.StartTransaction;
          with dm.ibqZapisz do
            begin
              Close;
              SQL.Clear;
              SQL.Add('INSERT INTO OBROTY(NR_KONTRAH_SKLEP,NR_KONTRACH_DOST,NR_PROWIZJE,NAZWA_D,DATA_ZAPISU,CZAS_ZAPISU,OBROTY,PROWIZJA_PROC,PROW_E_PROC,OKRES_M,OKRES_Q,NUMER_MIESIAC,ROK)');
              SQL.Add('VALUES(:NR_KONTRAH_SKLEP,:NR_KONTRACH_DOST,:NR_PROWIZJE,:NAZWA_D,CURRENT_DATE,CURRENT_TIME,:OBROTY,:PROWIZJA_PROC,:PROW_E_PROC,:OKRES_M,:OKRES_Q,:NUMER_MIESIAC,:ROK)');
              UnPrepare;
                ParamByName('NR_KONTRAH_SKLEP').AsInteger := nr_sklep;
                ParamByName('NR_KONTRACH_DOST').AsInteger := nr_dostawca;
                ParamByName('NR_PROWIZJE').AsInteger := nr_prowizje;
                ParamByName('NAZWA_D').AsString := nazwa_dostawca;
                ParamByName('OBROTY').asFloat := obroty;
                obrot_glowny := obrot_glowny + obroty;
                ParamByName('PROWIZJA_PROC').asFloat := dm.ibqProwizje.FieldValues['RETRO_KLIENT'];
                ParamByName('PROW_E_PROC').asFloat := dm.ibqProwizje.FieldValues['RETRO'];
                if CompareText(okres,'M') = 0 then ParamByName('OKRES_M').AsString := miesiac;
                if CompareText(okres,'Q') = 0 then ParamByName('OKRES_Q').AsString := kwartal;
                ParamByName('OKRES_M').AsString := miesiac;
                ParamByName('OKRES_Q').AsString := kwartal;
                ParamByName('NUMER_MIESIAC').AsInteger := nr_miesiac;
                ParamByName('ROK').AsInteger := rok;
              Prepare;
              Open;
        end;
        if dm.ibtZapisz.InTransaction then dm.ibtZapisz.Commit;
        //OdswiezBazy;
     end;
end;

procedure TfrGlowny.JvImgBtn1Click(Sender: TObject);
begin
frReczny.pc.ActivePageIndex := 0;
frReczny.ShowModal;
end;

procedure TfrGlowny.Raporty1Click(Sender: TObject);
begin
//frRaport.RLReport1.Preview;
end;

procedure TfrGlowny.Zplikutxt1Click(Sender: TObject);
begin
odTxt.Execute;
end;

procedure TfrGlowny.KasowanietabeliImport1Click(Sender: TObject);               //kasowanie tabeli import
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqKasujTabele do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM IMPORT');
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
OdswiezBazy;
end;

procedure TfrGlowny.KasowanietabeliObroty1Click(Sender: TObject);                //kasowanie tabeli obroty
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqKasujTabele do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM OBROTY');
          Open;
     end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;
OdswiezBazy;
end;

procedure TfrGlowny.JvDBGrid1DblClick(Sender: TObject);
begin
{jvbZapiszZmiany.Enabled := True;
jvbDodajNowego.Enabled := False;
edImieNazClick(Sender);
edNazwaClick(Sender);
edKodPClick(Sender);
edMiastoClick(Sender);
edTelefonClick(Sender);
edKategoriaClick(Sender);
edUlicaClick(Sender);
edNIPClick(Sender);
edKoncesjaClick(Sender);
edMetrazClick(Sender);
mUwagiClick(Sender);
with dm.ibqKontrahenci do
begin
     edImieNaz.Text := FieldValues['IMIE_NAZWISKO'];
     edKategoria.Text := FieldValues['KATEGORIA'];
     edKodP.Text := FieldValues['KOD_P'];
     edKoncesja.Text := FieldValues['KONCESJA'];
     edMetraz.Text := FieldValues['METRAZ'];
     edMiasto.Text := FieldValues['MIASTO'];
     edNazwa.Text := FieldValues['NAZWA'];
     edNIP.Text := FieldValues['NIP'];
     edTelefon.Text := FieldValues['TELEFON'];
     edUlica.Text := FieldValues['ULICA'];
     if Trim(FieldValues['RODZAJ']) = 'O' then
        rgTypK.ItemIndex := 1
     else
         rgTypK.ItemIndex := 0;
     mUwagi.Lines.Add(FieldValues['TELEFON']);
     zakladka_glowna := GetBookmark;
end;  }

end;

procedure TfrGlowny.jvbZapiszZmianyClick(Sender: TObject);
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
with dm.ibqFormKontrah do
begin
     Close;
     SQL.Clear;
     SQL.Add('UPDATE KONTRAH');
     SQL.Add('SET RODZAJ=:RODZAJ,IMIE_NAZWISKO=:IMIE_NAZWISKO,NAZWA=:NAZWA,NIP=:NIP,ULICA=:ULICA,MIASTO=:MIASTO,');
     SQL.Add('KOD_P = :KOD_P,DATA_PRZYSTAPIENIA = :DATA_PRZYSTAPIENIA,TELEFON = :TELEFON,KONCESJA = :KONCESJA,');
     SQL.Add('METRAZ = :METRAZ,KATEGORIA = :KATEGORIA,UWAGI = :UWAGI,AKTYWNY = :AKTYWNY');
     SQL.Add('WHERE NR_KONTRAH = :A');
     UnPrepare;
          ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
          {case rgTypK.ItemIndex of
               0: ParamByName('RODZAJ').AsString:='D';
               1: ParamByName('RODZAJ').AsString:='O';
          end;
          ParamByName('IMIE_NAZWISKO').AsString := UpperCase(edImieNaz.Text);
          ParamByName('NAZWA').AsString := UpperCase(edNazwa.Text);
          ParamByName('NIP').AsString := edNIP.Text;
          ParamByName('ULICA').AsString := UpperCase(edUlica.Text);
          ParamByName('MIASTO').AsString := UpperCase(edMiasto.Text);
          ParamByName('KOD_P').AsString := edKodP.Text;
          ParamByName('DATA_PRZYSTAPIENIA').AsDate := DataPrzystapienia.Date;
          ParamByName('TELEFON').AsString := edTelefon.Text;
          ParamByName('KONCESJA').AsString := UpperCase(edKoncesja.Text);
          ParamByName('METRAZ').AsString := edMetraz.Text;
          ParamByName('KATEGORIA').AsString := UpperCase(edKategoria.Text);
          ParamByName('UWAGI').AsString := mUwagi.Text;
          if CzyAktywny.Checked = True then ParamByName('AKTYWNY').AsInteger := 1
          else ParamByName('AKTYWNY').AsInteger := 0;  }
     Prepare;
     Open;
end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;

end;

procedure TfrGlowny.JvImgBtn3Click(Sender: TObject);
begin

with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    Open;
    GotoBookmark(zakladka_glowna);
  end;
end;

procedure TfrGlowny.Poprawdanekontrahenta1Click(Sender: TObject);             //POPRAWIANIE DANYCH KONTRAHENTA
var
   imie,nazwisko: String;
begin
with frDodajK do
     begin
          if Pos('D',dm.ibqKontrahenci.FieldValues['RODZAJ']) > 0 then
             rgTypK.ItemIndex := 0
          else rgTypK.ItemIndex := 1;
          edImieNaz.Text  :=  dm.ibqKontrahenci.FieldValues['IMIE_NAZWISKO'];
          edNazwa.Text  :=   dm.ibqKontrahenci.FieldValues['NAZWA'];
          edNIP.Text  :=   dm.ibqKontrahenci.FieldValues['NIP'];
          edUlica.Text  :=   dm.ibqKontrahenci.FieldValues['ULICA'];
          edMiasto.Text  :=   dm.ibqKontrahenci.FieldValues['MIASTO'];
          edKodP.Text  :=   dm.ibqKontrahenci.FieldValues['KOD_P'];
          DataPrzystapienia.Date  :=   dm.ibqKontrahenci.FieldValues['DATA_PRZYSTAPIENIA'];
          edTelefon.Text  :=   dm.ibqKontrahenci.FieldValues['TELEFON'];
          if Pos('A',dm.ibqKontrahenci.FieldValues['KONCESJA']) > 0 then
             CheckBox1.Checked := True;
          if Pos('B',dm.ibqKontrahenci.FieldValues['KONCESJA']) > 0 then
             CheckBox2.Checked := True;
          if Pos('C',dm.ibqKontrahenci.FieldValues['KONCESJA']) > 0 then
             CheckBox3.Checked := True;
          if (not dm.ibqKontrahenci.FieldByName('TYDZIEN_1').IsNull) and (CompareText(dm.ibqKontrahenci.FieldValues['TYDZIEN_1'],'T') = 0) then CheckBox8.Checked := True;
          if (not dm.ibqKontrahenci.FieldByName('TYDZIEN_2').IsNull) and (CompareText(dm.ibqKontrahenci.FieldValues['TYDZIEN_2'],'T') = 0) then CheckBox9.Checked := True;
          if (not dm.ibqKontrahenci.FieldByName('TYDZIEN_3').IsNull) and (CompareText(dm.ibqKontrahenci.FieldValues['TYDZIEN_3'],'T') = 0) then CheckBox10.Checked := True;
          if (not dm.ibqKontrahenci.FieldByName('TYDZIEN_4').IsNull) and (CompareText(dm.ibqKontrahenci.FieldValues['TYDZIEN_4'],'T') = 0) then CheckBox11.Checked := True;
          if (not dm.ibqKontrahenci.FieldByName('TYDZIEN_5').IsNull) and (CompareText(dm.ibqKontrahenci.FieldValues['TYDZIEN_5'],'T') = 0) then CheckBox12.Checked := True;
          edMetraz.Text  :=   dm.ibqKontrahenci.FieldValues['METRAZ'];
          edKategoria.Text  :=   dm.ibqKontrahenci.FieldValues['KATEGORIA'];
          if not dm.ibqKontrahenci.FieldByName('UWAGI').IsNull then
             mUwagi.Text  :=   dm.ibqKontrahenci.FieldValues['UWAGI'];
          if dm.ibqKontrahenci.FieldValues['NR_TRASY'] > 0 then
             begin
                  dm.PokazTrase(dm.ibqKontrahenci.FieldValues['NR_TRASY'],0,'');
                  Label26.Caption := 'Aktualna trasa ' + dm.ibqSQL1.FieldValues['NAZWA_TRASY'];
                  Label29.Caption := 'Aktualna handlowiec ' + dm.PracownikF(dm.ibqKontrahenci.FieldValues['NR_TRASY']);
             end;
          dm.PokazTrase(0,0,'');
          while not dm.ibqSQL1.Eof do
                begin
                     ComboBox1.Items.Add(dm.ibqSQL1.FieldValues['NAZWA_TRASY']);
                     ComboBox2.Items.Add(dm.ibqSQL1.FieldValues['NAZWA_TRASY']);
                     dm.ibqSQL1.Next;
                end;
          dm.PokazPracownikow;

          pc.Pages[1].TabVisible := False;
          pc.Pages[2].TabVisible := False;
          pc.ActivePageIndex := 0;
          ShowModal;
     end;
end;

procedure TfrGlowny.Usudanekontrahenta1Click(Sender: TObject);
begin
if MessageBox(Handle,'Czy chcesz usun�� wybreanego kontrahenta?', 'Usuwanie pozycji', MB_YESNO + MB_ICONQUESTION) = IdYes then
begin
if not dm.ibTransakcje.InTransaction then dm.ibTransakcje.StartTransaction;
zakladka_glowna := dm.ibqKontrahenci.GetBookmark;
with dm.ibqFormKontrah do
begin
     Close;
     SQL.Clear;
     SQL.Add('DELETE FROM KONTRAH');
     SQL.Add('WHERE NR_KONTRAH = :A');
     UnPrepare;
          ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
     Prepare;
     Open;
end;
if dm.ibTransakcje.InTransaction then dm.ibTransakcje.Commit;

with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    Open;
    GotoBookmark(zakladka_glowna);
  end;
end;
end;

procedure TfrGlowny.KartaDostawcyShow(Sender: TObject);

begin
with ibqDostawcy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_KONTRAH,NAZWA,NIP,KOD_P,MIASTO,RODZAJ,TELEFON,ULICA,UWAGI FROM KONTRAH');
          SQL.Add('WHERE RODZAJ = ''D''');
          Open;
     end;
end;

procedure TfrGlowny.Pokaobrotykontrahenta1Click(Sender: TObject);
var
   rodzaj0,rodzaj1: String;
   nr_kontrah: Integer;
begin
nr_kontrah := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
rodzaj0 := Trim(dm.ibqKontrahenci.FieldValues['RODZAJ']);
rodzaj1 := 'O';
if CompareStr(rodzaj0,rodzaj1) = 0 then
   dm.PokazObrotySklep(nr_kontrah)
else
   dm.PokazObrotyDostawca(nr_kontrah);

if CompareStr(rodzaj0,rodzaj1) = 0 then
   frObrotyK.ShowModal
else
    frObrotyD.ShowModal;
end;

procedure TfrGlowny.pmKontrahPopup(Sender: TObject);
begin
if Trim(dm.ibqKontrahenci.FieldValues['RODZAJ']) = 'D' then pmTabelaR.Visible := True;
end;

procedure TfrGlowny.pmTabelaRClick(Sender: TObject);
begin
with frRetra do
     with ibqRetro do
          begin
               Close;
               SQL.Clear;
               SQL.Add('SELECT RETRO,OKRES,RETRO_KLIENT,OPIS,NR_PROWIZJE FROM PROWIZJE');
               SQL.Add('WHERE NR_KONTRAH = :A');
               UnPrepare;
                     ParamByName('A').AsInteger := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
               Prepare;
               Open;
          with dbRetra do
               begin
                    Columns.Items[0].FieldName := 'RETRO';
                    Columns.Items[0].Title.Caption := 'Retro';
                    Columns.Items[0].Width := 80;
                    Columns.Items[1].FieldName := 'OKRES';
                    Columns.Items[1].Title.Caption := 'Okres retra';
                    Columns.Items[1].Width := 80;
                    Columns.Items[2].FieldName := 'RETRO_KLIENT';
                    Columns.Items[2].Title.Caption := 'Retro sklepu';
                    Columns.Items[2].Width := 80;
                    Columns.Items[3].FieldName := 'OPIS';
                    Columns.Items[3].Title.Caption := 'Opis retra';
                    Columns.Items[3].Width := 250;
                    Columns.Items[4].Visible := False;
               end;

          ShowModal;
          end;
end;

procedure TfrGlowny.eSzukajNChange(Sender: TObject);
begin
with ibqDostawcy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_KONTRAH,NAZWA,NIP,KOD_P,MIASTO,RODZAJ,TELEFON,ULICA,UWAGI FROM KONTRAH');
          SQL.Add('WHERE RODZAJ = ''D'' AND NAZWA LIKE :A');
          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(eSzukajN.Text) + '%';
          Prepare;
          Open;
     end;
end;

procedure TfrGlowny.eSzukajNIPChange(Sender: TObject);
begin
with ibqDostawcy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_KONTRAH,NAZWA,NIP,KOD_P,MIASTO,RODZAJ,TELEFON,ULICA,UWAGI FROM KONTRAH');
          SQL.Add('WHERE RODZAJ = ''D'' AND NIP LIKE :A');
          UnPrepare;
               ParamByName('A').AsString := '%' + UpperCase(eSzukajNIP.Text) + '%';
          Prepare;
          Open;
     end;
end;

procedure TfrGlowny.Pokaobrotydostawcy1Click(Sender: TObject);
begin
with frObrotyD.ibqObrotyD do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT B.NAZWA,B.MIASTO,A.NAZWA_D,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.PROW_E_PROC,A.PROW_E_KWOTA,A.ZYSK,A.OKRES_M,A.OKRES_Q FROM OBROTY A,KONTRAH B');
          SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A');
          SQL.Add('ORDER BY A.NAZWA_D,B.NAZWA,A.OKRES_M');
          UnPrepare;
              if not ibqDostawcy.FieldByName('NAZWA').IsNull then ParamByName('A').AsString := ibqDostawcy.FieldValues['NAZWA'];
          Prepare;
          Open;
     end;

with frObrotyD.ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(A.OBROTY) AS OBROT,SUM(A.PROWIZJA_KWOTA) AS PROWIZJA_S,SUM(A.PROW_E_KWOTA) AS PROWIZJA_E,SUM(A.ZYSK) AS ZYSK FROM OBROTY A,KONTRAH B');
          SQL.Add('WHERE A.NR_KONTRAH_SKLEP = B.NR_KONTRAH AND NAZWA_D = :A');
          UnPrepare;
              if not ibqDostawcy.FieldByName('NAZWA').IsNull then ParamByName('A').AsString := ibqDostawcy.FieldValues['NAZWA'];
          Prepare;
          Open;
     end;

with frObrotyD do
     begin
          if not ibqObroty.FieldByName('OBROT').IsNull then lObroty.Caption := FloatToStrF(ibqObroty.FieldValues['OBROT'], ffCurrency, 7, 2);
          if not ibqObroty.FieldByName('PROWIZJA_S').IsNull then lProwizja.Caption := FloatToStrF(ibqObroty.FieldValues['PROWIZJA_S'], ffCurrency, 7, 2);
          if not ibqObroty.FieldByName('PROWIZJA_E').IsNull then lProw_E.Caption := FloatToStrF(ibqObroty.FieldValues['PROWIZJA_E'], ffCurrency, 7, 2);
          //if not ibqObroty.FieldByName('ZYSK').IsNull then lZysk.Caption := FloatToStrF(ibqObroty.FieldValues['ZYSK'], ffCurrency, 7, 2);
     end;

with frObrotyD do
     begin
          grObroty.Columns.Items[2].Visible := False;
          grObroty.Columns.Items[0].Title.Caption := 'Nazwa sklepu';
          grObroty.Columns.Items[1].Title.Caption := 'Miasto sklepu';
          grObroty.Columns.Items[3].Title.Caption := 'Obroty sklepu';
          grObroty.Columns.Items[4].Title.Caption := 'Prowizja %';
          grObroty.Columns.Items[5].Title.Caption := 'Prowizja sklepu z�';
          grObroty.Columns.Items[6].Title.Caption := 'Prowizja % Euro';
          grObroty.Columns.Items[7].Title.Caption := 'Prowizja Euro z�';
          grObroty.Columns.Items[8].Title.Caption := 'Zysk Euro-sklep';
          grObroty.Columns.Items[9].Title.Caption := 'Okres miesi�c';
          grObroty.Columns.Items[10].Title.Caption := 'Okres kwarta�';
          grObroty.Columns.Items[0].Width := 250;
          grObroty.Columns.Items[1].Width := 150;
          grObroty.Columns.Items[3].Width := 80;
          grObroty.Columns.Items[4].Width := 80;
          grObroty.Columns.Items[5].Width := 80;
          grObroty.Columns.Items[6].Width := 80;
          grObroty.Columns.Items[7].Width := 80;
          grObroty.Columns.Items[8].Width := 80;
          grObroty.Columns.Items[9].Width := 80;
          grObroty.Columns.Items[10].Width := 80; 
     end;
frObrotyD.ShowModal;
end;

procedure TfrGlowny.Wydruksesjiimportu1Click(Sender: TObject);
begin
//
end;

procedure TfrGlowny.OdswiezBazy;
begin
dm.serwer.Close;
dm.serwer.DatabaseName := ExtractFilePath(Application.ExeName) + 'euro.fdb';
dm.serwer.Connected := True;
if dm.serwer.TestConnected = False then ShowMessage('Baza jest nieaktywna');
dm.serwer.Open;
end;

procedure TfrGlowny.MenuItem3Click(Sender: TObject);
begin
dm.dsKontrah.DataSet := dm.ibqSQL;
with frObrotyK.ibqObroty do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA_D AS DOSTAWCA,A.OBROTY,A.PROWIZJA_PROC,A.PROWIZJA_KWOTA,A.OKRES_M,A.OKRES_Q,A.OKRES_Q FROM OBROTY A');
          SQL.Add('WHERE A.NR_KONTRAH_SKLEP = :A');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqSQL.FieldValues['NR_KONTRAH'];
          Prepare;
          Open;
     end;
with frObrotyK.ibqSuma do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(OBROTY) AS OBROTY,SUM(PROWIZJA_KWOTA) AS PROWIZJA FROM OBROTY');
          SQL.Add('WHERE NR_KONTRAH_SKLEP = :A');
          UnPrepare;
                    ParamByName('A').AsInteger := dm.ibqSQL.FieldValues['NR_KONTRAH'];
          Prepare;
          Open;
          if FieldByName('OBROTY').IsNull then
             begin
                  frObrotyK.lObroty.Caption := 'brak obrot�w';
                  frObrotyK.lProwizja.Caption := 'brak obrot�w';
             end
          else
              begin
                   frObrotyK.lObroty.Caption := FloatToStrF(FieldValues['OBROTY'], ffCurrency, 7, 2);
                   frObrotyK.lProwizja.Caption := FloatToStrF(FieldValues['PROWIZJA'], ffCurrency, 7, 2);
              end;
     end;

with frObrotyK.grObroty do
     begin
          Columns.Items[0].FieldName := 'DOSTAWCA';
          Columns.Items[0].Title.Caption := 'Dostawca';
          Columns.Items[0].Width := 200;
          Columns.Items[1].FieldName := 'OBROTY';
          Columns.Items[1].Title.Caption := 'Obr�t sklepu';
          Columns.Items[1].Width := 80;
          Columns.Items[2].FieldName := 'PROWIZJA_PROC';
          Columns.Items[2].Title.Caption := 'Procent prowizji';
          Columns.Items[2].Width := 80;
          Columns.Items[3].FieldName := 'PROWIZJA_KWOTA';
          Columns.Items[3].Title.Caption := 'Kwota prowizji';
          Columns.Items[3].Width := 80;
          Columns.Items[4].FieldName := 'OKRES_M';
          Columns.Items[4].Title.Caption := 'Okres miesi�ca';
          Columns.Items[4].Width := 80;
          Columns.Items[5].FieldName := 'OKRES_Q';
          Columns.Items[5].Title.Caption := 'Okres kwarta�';
          Columns.Items[5].Width := 80;
     end;

frObrotyK.ShowModal;
end;

procedure TfrGlowny.DlaMiesiacaObrotySklepy(miesiac: String);
begin
rpImporty.frxReport1.PrepareReport(True);
rpImporty.frxReport1.ShowPreparedReport;

end;

procedure TfrGlowny.DlaMiesiacaObrotySklepyExcel(miesiac,typ: String);
begin
raporty.DefaultExt := '*.fr3';
raporty.InitialDir := ExtractFilePath(Application.ExeName) + '\raporty\';
raporty.Execute;
rpImporty.frxReport1.LoadFromFile(raporty.FileName);
rpImporty.frxReport1.PrepareReport(True);
rpImporty.frxReport1.ShowPreparedReport;

end;

procedure TfrGlowny.Pokalistimportw1Click(Sender: TObject);                     //Poka� list� import�w
var
   t1,t2,t3,t4 : String;
begin

t1 := 'SELECT A.NAZWA_D,A.NR_PROWIZJE,A.DATA_ZAPISU,B.OPIS,SUM(A.OBROTY) AS OBROTY,NR_KONTRACH_DOST,NUMER_MIESIAC,ROK FROM OBROTY A,PROWIZJE B';
t2 := 'WHERE A.NR_PROWIZJE = B.NR_PROWIZJE';
t3 := 'GROUP BY A.DATA_ZAPISU,A.NAZWA_D,A.NR_PROWIZJE,B.OPIS,NR_KONTRACH_DOST,NUMER_MIESIAC,ROK';
t4 := 'ORDER BY A.DATA_ZAPISU DESC';
frListaImp.pc.ActivePageIndex := 0;
//frListaImp.cbData.Date := Now;
with dm.ibqSQL1 do
     begin
          Close;
          SQL.Clear;
          SQL.Add(t1);SQL.Add(t2);SQL.Add(t3);SQL.Add(t4);
          Open;
     end;
frListaImp.UstawSiatke(frListaImp.DBGrid1);
frListaImp.ShowModal;
end;

procedure TfrGlowny.Prowizje1Click(Sender: TObject);
begin
pmTabelaRClick(Sender);
end;

procedure TfrGlowny.Poczkontrahentw1Click(Sender: TObject);
begin
frPolaczKartoteki.ShowModal;
end;

procedure TfrGlowny.Koniec3Click(Sender: TObject);
begin
Close;
end;

procedure TfrGlowny.Koniec2Click(Sender: TObject);
begin
Close;
end;

procedure TfrGlowny.listaPlikiClick(Sender: TObject);
var
      x,ile,licznik : Integer;
      parA, parBStr,plik,info : String;
      parB,suma : Real;
begin
KasowanietabeliImport1Click(Sender);
dm.PlikCSV.Refresh;
dm.PlikCSV.Close;

if plik <> '' then                                                              //if od sprw czy wybrano plik
begin
dm.PlikCSV.FileName := plik;
dm.PlikCSV.Open;
ile := dm.PlikCSV.RecordCount;
nr_import := 0;
with dm.ibqZapisz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_IMPORTU FROM IMPORT');
          Open;
          Last;
          if FieldByName('NR_IMPORTU').IsNull then
             nr_import := 0
          else
              nr_import := FieldValues['NR_IMPORTU'];
     end;
nr_import := nr_import + 1;
dm.PlikCSV.First;
jpbPasek.Max := ile;
if not dm.trCSV2Firebird.InTransaction then dm.trCSV2Firebird.StartTransaction;
for x := 1 to ile do
begin
jpbPasek.Position := x;
if not dm.PlikCSV.Fields.Fields[0].IsNull then
   begin
        parA := dm.PlikCSV.Fields.Fields[0].AsString;
        if Pos('-',parA) > 0 then parA := StringNIP(parA);
   end
else
    parA := 'Spr.' + IntToStr(x) + ' wiersz';
if not dm.PlikCSV.Fields.Fields[1].IsNull then
   begin
        parBStr := dm.PlikCSV.Fields.Fields[1].asString;
        Delete(parBStr,Pos(' ',parBStr),1);
        parB := StrToFloat(parBStr);
   end
else
    parB := 0;
if (parA <> Null) or (parB <> Null) then
with dm.ibqCSV2Firebird do
      begin
            Close;
            SQL.Clear;
            SQL.Add('INSERT INTO IMPORT(NIP,OBROT,NR_IMPORTU,NR_DOSTAWCA)');
            SQL.Add('VALUES(:A,:B,:C,:D)');
            UnPrepare;
                  ParamByName('A').AsString := parA;
                  ParamByName('B').AsFloat := parB;
                  ParamByName('C').AsInteger := nr_import;
                  ParamByName('D').AsInteger := 1;
            Prepare;
            Open;
      end;
      dm.PlikCSV.Next;
end;
if dm.trCSV2Firebird.InTransaction then dm.trCSV2Firebird.Commit;

with dm.ibqCSV2Firebird do
      begin
            Close;
            SQL.Clear;
            SQL.Add('SELECT NIP,SUM(OBROT) AS OBROT FROM IMPORT');
            SQL.Add('WHERE NR_IMPORTU = :A');
            SQL.Add('GROUP BY NIP');
            UnPrepare;
                  ParamByName('A').AsInteger := nr_import;
            Prepare;
            Open;
      end;
      while not dm.ibqCSV2Firebird.Eof do
             begin
                  licznik := licznik + 1;
                  dm.ibqCSV2Firebird.Next;
             end;
      sb.SimpleText := 'Importujesz plik: ' + plik;
      jpcGlowna.ActivePageIndex := 3;
      jpbPasek.Max := 0;
      frInfo.pc.ActivePageIndex := 0;
      frInfo.lbNIP.Clear;
      frInfo.ShowModal;
end;                                                                            //koniec if od sprw czy wybrano plik
end;

procedure TfrGlowny.KartDostwacyClick(Sender: TObject);
begin
DlaMiesiacaObrotySklepyExcel('wszystkie','e');
end;

procedure TfrGlowny.KatSklepyClick(Sender: TObject);
var
   okres : String;
   nr : Integer;
begin
dm.PokazKontrah(0,'O','');
with frKreatorRaportow do
     begin
          PageControlEx1.ActivePageIndex := 0;
          if przyciski then
             PageControlEx1.Pages[3].TabVisible := True
          else
              PageControlEx1.Pages[3].TabVisible := False;
          UstawSiatke(frKreatorRaportow.DBGrid1);
          ShowModal;
     end;
end;

procedure TfrGlowny.DlaMiesiacaObrotySklepyR(okres: String;
  nr_kontrahenta: Integer);
begin
//
end;

procedure TfrGlowny.ToolButton1Click(Sender: TObject);
begin
//rpImporty.ShowModal;
end;

procedure TfrGlowny.KontrahSClick(Sender: TObject);                              //dodawanie nowego kontrahenta
begin
dm.PokazTrase(0,0,'');
while not dm.ibqSQL1.Eof do
      begin
           frDodajK.ComboBox3.Items.Add(dm.ibqSQL1.FieldValues['NAZWA_TRASY']);
           dm.ibqSQL1.Next;
      end;
dm.PokazPracownikow;
frDodajK.nr := 255;
frDodajK.pc.Pages[0].TabVisible := False;
frDodajK.pc.Pages[1].TabVisible := False;
frDodajK.pc.Pages[2].TabVisible := True;
frDodajK.pc.Pages[3].TabVisible := False;
frDodajK.ShowModal;
end;

procedure TfrGlowny.DodajCMR1Click(Sender: TObject);                             //wywolanie kontrahenta - zakladka CMR
var
   nr_kontrahenta: Integer;
begin
frDodajK.nr := 255;
frDodajK.pc.Pages[0].TabVisible := False;
frDodajK.pc.Pages[2].TabVisible := False;
frDodajK.pc.Pages[3].TabVisible := False;
frDodajK.pc.ActivePageIndex := 1;
nr_kontrahenta := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
dm.PokazCRM(nr_kontrahenta);
frDodajK.UstawSiatke;
frDodajK.ShowModal;
end;

procedure TfrGlowny.rasy1Click(Sender: TObject);
var
   nazwisko,imie: String;
begin
//TRASY
dm.PokazPracownikow;
dm.ibqSQL1.First;
while not dm.ibqSQL1.Eof do
      begin
           nazwisko := dm.ibqSQL1.FieldValues['NAZWISKO'];
           imie := dm.ibqSQL1.FieldValues['IMIE'];
           frTrasy.ComboBox1.Items.Add(nazwisko + ' ' + imie);
           frTrasy.ComboBox2.Items.Add(nazwisko + ' ' + imie);
           frTrasy.ComboBox3.Items.Add(nazwisko + ' ' + imie);
           dm.ibqSQL1.Next;
      end;
dm.PokazTrase(0,0,'');
with frTrasy do
    begin
         UstawSiatke;
         PageControlEx1.ActivePageIndex := 0;
         ToolButton2.Enabled := True;ToolButton3.Enabled := False;ToolButton4.Enabled := False;
         ToolButton6.Enabled := False;
         ShowModal;
    end;
end;

procedure TfrGlowny.Pracownicy1Click(Sender: TObject);
begin
dm.PokazGrupy;
dm.ibqSQL1.First;
while not dm.ibqSQL1.Eof do
      begin
           frPracownicy.ComboBox1.Items.Add(dm.ibqSQL1.FieldValues['NAZWA_GRUPY']);
           dm.ibqSQL1.Next;
      end;
dm.PokazPracownikow;
frPracownicy.PageControlEx1.ActivePageIndex := 0;
frPracownicy.ShowModal;
end;

procedure TfrGlowny.Grupy1Click(Sender: TObject);
begin
frGrupy.ShowModal;
end;

procedure TfrGlowny.jeMiastoClick(Sender: TObject);
begin
jeMiasto.Text := '';
end;

procedure TfrGlowny.jeMiastoChange(Sender: TObject);
begin
with dm.ibqKontrahenci do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM KONTRAH');
    SQL.Add('WHERE RODZAJ = :A AND MIASTO LIKE :B AND AKTYWNY = 1');
    if jrbTypD.Checked then ParamByName('A').AsString := 'D';
    if jrbTypO.Checked then ParamByName('A').AsString := 'O';
    ParamByName('B').AsString := '%' + UpperCase(jeMiasto.Text) + '%';
    Open;
  end;
end;

procedure TfrGlowny.ToolButton22Click(Sender: TObject);
begin
dm.SzukajTowar(0,'%');
frTowary.data_zakupu_i.Date := now;
frTowary.data_zakupu_e.Date := now;
frTowary.data_zakupu_d.Date := now;
frTowary.UstawSiatke(frTowary.DBGrid1);
frTowary.ShowModal;
end;

procedure TfrGlowny.Wczprzyciski1Click(Sender: TObject);
begin
frKreatorRaportow.PageControlEx1.Pages[3].TabVisible := True;
frTowary.ToolButton5.Visible := True;
frTowary.pc.Pages[4].TabVisible := True;
frTowary.pc.ActivePageIndex := 4;
if przyciski then
   przyciski := False
else
    przyciski := True;
end;

procedure TfrGlowny.Dostawcy1Click(Sender: TObject);                             //otwarcie kartoteki kontrahentow
begin
//
end;

procedure TfrGlowny.Dodajgadety1Click(Sender: TObject);
begin
dm.PokazTowary;
//frWydajGadzety.UstaNrNaglowka(dm.NrNaglowka);
dm.PokazGadzetyP(dm.NrNaglowka);
frWydajGadzety.UstalNrKontrah(dm.ibqKontrahenci.FieldValues['NR_KONTRAH']);
frWydajGadzety.UstawSiatke1;
frWydajGadzety.UstawSiatke2;
frWydajGadzety.LabeledEdit2.Text := '1';
frWydajGadzety.ShowModal;
end;

procedure TfrGlowny.Aktualizacja1Click(Sender: TObject);
begin
frUpdate.ShowModal;
end;

procedure TfrGlowny.FormClose(Sender: TObject; var Action: TCloseAction);
var
   INI : TINIFile;
   miesiac,kwartal: String;
   rok: Integer;
begin
miesiac := cMiesiac.Text;
kwartal := cKwartal.Text;
rok := StrToInt(cRok.Text);
INI := TINIFile.Create(ExtractFilePath(Application.ExeName) + 'retro.ini');
    try
       INI.WriteString('Okres','miesi�c',miesiac);
       INI.WriteString('Okres','kwarta�',kwartal);
       INI.WriteInteger('Okres','rok',rok);
    finally
       INI.Free;
    end;
end;

function TfrGlowny.NrMiesiaca(miesiac: String): Integer;
begin
if SameText(miesiac,'stycze�') then
   Result := 1;
if SameText(miesiac,'luty') then
   Result := 2;
if SameText(miesiac,'marzec') then
   Result := 3;
if SameText(miesiac,'kwiecie�') then
Result := 4;
if SameText(miesiac,'maj') then
   Result := 5;
if SameText(miesiac,'czerwiec') then
   Result := 6;
if SameText(miesiac,'lipiec') then
   Result := 7;
if SameText(miesiac,'sierpie�') then
   Result := 8;
if SameText(miesiac,'wrzesie�') then
   Result := 9;
if SameText(miesiac,'pa�dziernik') then
   Result := 10;
if SameText(miesiac,'listopad') then
   Result := 11;
if SameText(miesiac,'grudzie�') then
   Result := 12;
end;

procedure TfrGlowny.Drukujkartkontrahenta1Click(Sender: TObject);
var
   raporty: String;
begin
//DRUKOWANIE KARTY KONTRAHENTA
raporty  := ExtractFilePath(Application.ExeName) + '/raporty/karta_kontrahenta.fr3';
if FileExists(raporty) then
   begin
        rpImporty.frxReport1.LoadFromFile(raporty,False);
        rpImporty.frxReport1.PrepareReport(True);
        
        rpImporty.frxReport1.ShowReport(True);
   end;
end;

procedure TfrGlowny.Aktywnidostawcy1Click(Sender: TObject);
begin
//AKTYWNI DOSTAWCY
dm.AktywniD_Pokaz;
dm.AktywniDD_Pokaz;
frAktywniDostawcy.ShowModal;
end;

initialization
//rlconsts.SetVersion(3,72,'B');

end.
