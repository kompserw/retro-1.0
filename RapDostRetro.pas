unit RapDostRetro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IBStoredProc, DB, IBCustomDataSet, IBQuery, 
  ZAbstractRODataset, ZDataset, ZConnection, ADODB,
  JvADOQuery;

type
  TfrRapDostRetra = class(TForm)
    ibqDostawca: TIBQuery;
    ibqPokazObroty: TIBQuery;
    dsObrotOpisRetra: TDataSource;
    dsDostawca: TDataSource;
    ibqDostawcaNAZWA: TIBStringField;
    ibqDostawcaNR_KONTRAH: TIntegerField;
    ibqPokazObrotyOPIS: TIBStringField;
    ibqPokazObrotyNAZWA_D: TIBStringField;
    ibqPokazObrotyOBROT: TIBBCDField;
    ibqPokazObrotyPROWIZJA_SIEC: TIBBCDField;
    ibqPokazObrotyPROWIZJA_SKLEP: TIBBCDField;
    ibqPokazObrotyZYSK: TIBBCDField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    JvADODataSet1: TJvADODataSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frRapDostRetra: TfrRapDostRetra;

implementation
uses baza;
{$R *.dfm}

end.
