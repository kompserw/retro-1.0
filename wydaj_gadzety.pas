unit wydaj_gadzety;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, JvExStdCtrls, JvButton, JvCtrls, Grids,
  DBGrids, ExtCtrls, frxDesgn, frxClass, frxDBSet;

type
  TfrWydajGadzety = class(TForm)
    GroupBox1: TGroupBox;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    JvImgBtn1: TJvImgBtn;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    JvImgBtn2: TJvImgBtn;
    frxReport2: TfrxReport;
    frxDBDataset2: TfrxDBDataset;
    frxDBDataset3: TfrxDBDataset;
    procedure UstawSiatke1;
    procedure UstawSiatke2;
    procedure UstaNrNaglowka(nr: Integer);
    procedure UstalNrKontrah(nr: Integer);
    procedure LabeledEdit1Change(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure JvImgBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frWydajGadzety: TfrWydajGadzety;
  nr_naglowka,nr_kontrah: Integer;
implementation
uses baza,glowny, towary;
{$R *.dfm}

{ TfrWydajGadzety }

procedure TfrWydajGadzety.UstawSiatke1;
begin
with DBGrid1.Columns do
     begin
          Items[0].Visible := false;
          Items[1].Title.Caption := 'Nazwa towaru';
          Items[1].Width := 200;
          Items[2].Width := 70;
          Items[2].Title.Caption := 'Cena netto';
          Items[3].Width := 70;
          Items[3].Title.Caption := 'Cena brutto';
          Items[4].Visible := false;
          Items[5].Width := 50;
          Items[5].Title.Caption := 'Ilo��';
          Items[6].Visible := false;
          Items[7].Visible := false;
          Items[8].Visible := false;
          Items[9].Visible := false;
          Items[10].Visible := false;
     end;
end;

procedure TfrWydajGadzety.LabeledEdit1Change(Sender: TObject);
begin
dm.SzukajTowar(0,'%' + LabeledEdit1.Text + '%');
UstawSiatke1;
end;

procedure TfrWydajGadzety.JvImgBtn1Click(Sender: TObject);
var
   nr_towary,ilosc,ilosc_old,ilosc_new,nr_naglowek: Integer;
   nazwa: String;
   cena_n_zakup,wartosc_n_zakupu: Real;
begin
nr_naglowek := dm.NrNaglowka;
with dm.ibqTowary do
     begin
          nr_towary := FieldValues['NR_TOWARY'];
          nazwa := FieldValues['NAZWA'];
          ilosc_old := FieldValues['ILOSC'];
          ilosc := StrToInt(LabeledEdit2.Text);
          ilosc_new := ilosc_old - ilosc;
          cena_n_zakup := FieldValues['CENA_N_ZAKUP'];
          wartosc_n_zakupu := ilosc * cena_n_zakup;
          if ilosc_new < 0 then
             ShowMessage('Ilo�� wydanego towaru jest wi�ksza ni� ilo�� posiadanego towaru na stanie')
          else
              begin
                   dm.PozycjeG(4,nr_naglowek,0,0,nazwa,ilosc,cena_n_zakup,wartosc_n_zakupu);
                   dm.TowaryStany(nr_towary,ilosc_new,cena_n_zakup);
              end;
     end;

dm.PokazGadzetyP(nr_naglowek);
dm.PokazTowary;
dm.PokazKontrah(nr_kontrah,'O','');
LabeledEdit2.Text := '1';
UstawSiatke1;
UstawSiatke2;
end;

procedure TfrWydajGadzety.UstaNrNaglowka(nr: Integer);
begin
nr_naglowka:= nr;
end;

procedure TfrWydajGadzety.UstawSiatke2;
begin
with DBGrid2.Columns do
     begin
          Items[0].Visible := false;
          Items[1].Visible := false;
          Items[2].Title.Caption := 'Nazwa towaru';
          Items[2].Width := 200;
          Items[3].Width := 80;
          Items[3].Title.Caption := 'Ilo��';
          Items[4].Width := 80;
          Items[4].Title.Caption := 'Cena netto';
          Items[5].Width := 80;
          Items[5].Title.Caption := 'Warto�� netto';
     end;
end;

procedure TfrWydajGadzety.UstalNrKontrah(nr: Integer);
begin
nr_kontrah := nr;
end;

procedure TfrWydajGadzety.JvImgBtn2Click(Sender: TObject);
var
   nr_kontahenta,nr: Integer;
   komunikat: PAnsiChar;
begin
komunikat := 'Czy chcesz wydrukowa� dokument wydania ?';
nr_kontahenta := dm.ibqKontrahenci.FieldValues['NR_KONTRAH'];
nr := dm.NrNaglowka;
dm.NaglowekG(3,nr_kontahenta,nr,1,0,0);
LabeledEdit2.Text := '1';
if MessageBox(Handle,komunikat,'Drukowanie dokumentu', MB_YESNO + MB_ICONQUESTION) = IdYes then
   begin
        frxReport2.PrepareReport(True);
        frxReport2.ShowReport(True);
        Close;
   end
else
    Close;
end;

end.
