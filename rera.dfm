object frRetra: TfrRetra
  Left = 192
  Top = 125
  Width = 722
  Height = 441
  Caption = 'Tabela retra dostawcy'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pc: TPageControlEx
    Left = 8
    Top = 8
    Width = 689
    Height = 385
    ActivePage = tsEdycjaR
    TabOrder = 0
    HideHeader = False
    object tsTabelaR: TTabSheet
      Caption = 'Tabela retr'
      object GroupBox1: TGroupBox
        Left = 8
        Top = 40
        Width = 649
        Height = 289
        Caption = 'Retra'
        TabOrder = 0
        object dbRetra: TJvDBUltimGrid
          Left = 2
          Top = 15
          Width = 645
          Height = 272
          Align = alClient
          DataSource = dsRetro
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDblClick = dbRetraDblClick
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
        end
      end
      object DBNavigator1: TDBNavigator
        Left = 8
        Top = 8
        Width = 240
        Height = 25
        DataSource = dsRetro
        TabOrder = 1
      end
    end
    object tsEdycjaR: TTabSheet
      Caption = 'Edycja retr'
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 16
        Top = 32
        Width = 649
        Height = 153
        TabOrder = 0
        object leRetro: TLabeledEdit
          Left = 112
          Top = 48
          Width = 121
          Height = 21
          EditLabel.Width = 27
          EditLabel.Height = 13
          EditLabel.Caption = 'Retro'
          TabOrder = 0
        end
        object leRetroS: TLabeledEdit
          Left = 248
          Top = 48
          Width = 121
          Height = 21
          EditLabel.Width = 77
          EditLabel.Height = 13
          EditLabel.Caption = 'Retro dla sklepu'
          TabOrder = 1
        end
        object leOpis: TLabeledEdit
          Left = 112
          Top = 104
          Width = 377
          Height = 21
          EditLabel.Width = 48
          EditLabel.Height = 13
          EditLabel.Caption = 'Opis retra'
          TabOrder = 2
        end
        object rgOkres: TRadioGroup
          Left = 384
          Top = 24
          Width = 249
          Height = 57
          Caption = 'Retro'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'M - miesi'#281'czne'
            'Q- kwartalne')
          TabOrder = 3
        end
      end
      object JvImgBtn1: TJvImgBtn
        Left = 152
        Top = 216
        Width = 115
        Height = 41
        Caption = 'Zapisz zmian'#281
        TabOrder = 1
        OnClick = JvImgBtn1Click
        Flat = True
        Images = frGlowny.JvImageList1
        ImageIndex = 3
      end
      object JvImgBtn2: TJvImgBtn
        Left = 272
        Top = 216
        Width = 113
        Height = 41
        Caption = 'Zapisz nowe'
        TabOrder = 2
        OnClick = JvImgBtn2Click
        Flat = True
        Images = frGlowny.JvImageList1
        ImageIndex = 4
      end
      object JvImgBtn3: TJvImgBtn
        Left = 392
        Top = 216
        Width = 113
        Height = 41
        Caption = 'Usu'#324' retro'
        TabOrder = 3
        OnClick = JvImgBtn3Click
        Flat = True
        Images = frGlowny.JvImageList1
        ImageIndex = 9
      end
    end
  end
  object ibqRetro: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 408
    Top = 32
  end
  object dsRetro: TDataSource
    DataSet = ibqRetro
    Left = 448
    Top = 32
  end
  object ibqKasujR: TIBQuery
    Database = dm.serwer
    Transaction = dm.ibTransakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 492
    Top = 32
  end
end
